(function() {
	var mainClassName = "Main";
	var mainClassPath = mainClassName.replace(/\./g, "/");
	var workerSourcePath = "";//"worker/"+mainClassPath+"/";
	var baseLocation = "";
	self.loaderInfos = {};
	
	if (!self.importScripts) {
		var currentScript = document.currentScript || document.querySelector("script:last-of-type");
		self.loaderInfos.url = currentScript.src;
		baseLocation = currentScript.src.substring(0, currentScript.src.lastIndexOf("/")+1);
		self.importScripts = function() {
			for (var i=0; i<arguments.length; ++i) {
				document.write('<script type="text/javascript" src="' + arguments[i] + '"></script>');
			}
		};
	}
	
	if (self.document)
		initMainClass();
	else
		self.addEventListener("message", initWorker, false);

	function initWorker(e) {
		self.removeEventListener("message", initWorker, false);
		
		self["workerId"] = e.data.id;
		self["workerSharedProperties"] = e.data.sharedProperties;
		var loc = e.data.location;
		self.loaderInfos.url = e.data.location;
		baseLocation = loc.substring(0, loc.lastIndexOf("/")+1) + workerSourcePath;
		
		initMainClass();
	}
	
	function initMainClass() {
		self.importScripts(baseLocation + "bin/js-debug/library/closure/goog/base.js");
		if (!self.document) {
			goog.global.CLOSURE_IMPORT_SCRIPT = function(src, scriptText) {
				src = src.replace(/(\.\.\/)+/g, "");
				self.importScripts(baseLocation+"bin/js-debug/"+src);
			}
		}
		self.importScripts(baseLocation + "bin/js-debug/"+mainClassPath+"-dependencies.js");
		
		if (!self.document)
			launchApp();
		else
			self.onload = launchApp;
	}
	
	function launchApp() {
		var mainClass = self;
		mainClassName.split(".").forEach(function(el) {
			mainClass = mainClass[el];
		});
		new mainClass();
	}
})();