package {
	/**
	 *  The Date class represents date and time information. An instance of the Date class represents a particular point in time for which the properties such as month, day, hours, and seconds can be queried or modified. The Date class lets you retrieve date and time values relative to universal time (Greenwich mean time, now called universal time or UTC) or relative to local time, which is determined by the local time zone setting on the operating system that is running Flash Player. The methods of the Date class are not static but apply only to the individual Date object specified when the method is called. The <code>Date.UTC()</code> and <code>Date.parse()</code> methods are exceptions; they are static methods. <p>The Date class handles daylight saving time differently, depending on the operating system and runtime version. Flash Player 6 and later versions handle daylight saving time on the following operating systems in these ways:</p> <ul> 
	 *  <li>Windows - the Date object automatically adjusts its output for daylight saving time. The Date object detects whether daylight saving time is employed in the current locale, and if so, it detects the standard-to-daylight saving time transition date and times. However, the transition dates currently in effect are applied to dates in the past and the future, so the daylight saving time bias might calculate incorrectly for dates in the past when the locale had different transition dates.</li> 
	 *  <li>Mac OS X - the Date object automatically adjusts its output for daylight saving time. The time zone information database in Mac OS X is used to determine whether any date or time in the present or past should have a daylight saving time bias applied.</li> 
	 *  <li>Mac OS 9 - the operating system provides only enough information to determine whether the current date and time should have a daylight saving time bias applied. Accordingly, the date object assumes that the current daylight saving time bias applies to all dates and times in the past or future.</li> 
	 * </ul> <p>Flash Player 5 handles daylight saving time on the following operating systems as follows:</p> <ul> 
	 *  <li>Windows - the U.S. rules for daylight saving time are always applied, which leads to incorrect transitions in Europe and other areas that employ daylight saving time but have different transition times than the U.S. Flash correctly detects whether daylight saving time is used in the current locale.</li> 
	 * </ul> <p>To use the Date class, construct a Date instance using the <code>new</code> operator.</p> <p>ActionScript 3.0 adds several new accessor properties that can be used in place of many Date class methods that access or modify Date instances. ActionScript 3.0 also includes several new variations of the <code>toString()</code> method that are included for ECMA-262 3rd Edition compliance, including: <code>Date.toLocaleString()</code>, <code>Date.toTimeString()</code>, <code>Date.toLocaleTimeString()</code>, <code>Date.toDateString()</code>, and <code>Date.toLocaleDateString()</code>.</p> <p>To compute relative time or time elapsed, see the <code>getTimer()</code> method in the flash.utils package.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7f10.html" target="_blank">Managing calendar dates and times</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7f0c.html" target="_blank">Creating Date objects</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7f0b.html" target="_blank">Getting time unit values</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7f0a.html" target="_blank">Performing date and time arithmetic</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7f09.html" target="_blank">Converting between time zones</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7f0e.html" target="_blank">Date and time example: Simple analog clock</a>
	 * </div><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e52.html" target="_blank">Working with dates and times</a>
	 *  <br>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="flash/utils/package-detail.html#getTimer()" target="">flash.utils.getTimer()</a>
	 * </div><br><hr>
	 */
	public class Date {
		private var _date:Number;
		private var _dateUTC:Number;
		private var _fullYear:Number;
		private var _fullYearUTC:Number;
		private var _hours:Number;
		private var _hoursUTC:Number;
		private var _milliseconds:Number;
		private var _millisecondsUTC:Number;
		private var _minutes:Number;
		private var _minutesUTC:Number;
		private var _month:Number;
		private var _monthUTC:Number;
		private var _seconds:Number;
		private var _secondsUTC:Number;
		private var _time:Number;

		private var _day:Number;
		private var _dayUTC:Number;
		private var _timezoneOffset:Number;

		/**
		 * <p> Constructs a new Date object that holds the specified date and time. </p>
		 * <p>The <code>Date()</code> constructor takes up to seven parameters (year, month, ..., millisecond) to specify a date and time to the millisecond. The date that the newly constructed Date object contains depends on the number, and data type, of arguments passed. </p>
		 * <ul>
		 *  <li>If you pass no arguments, the Date object is assigned the current date and time.</li>
		 *  <li>If you pass one argument of data type Number, the Date object is assigned a time value based on the number of milliseconds since January 1, 1970 0:00:000 GMT, as specified by the lone argument.</li>
		 *  <li>If you pass one argument of data type String, and the string contains a valid date, the Date object contains a time value based on that date.</li>
		 *  <li>If you pass two or more arguments, the Date object is assigned a time value based on the argument values passed, which represent the date's year, month, date, hour, minute, second, and milliseconds.</li>
		 * </ul>
		 * <p>If you pass a string to the Date class constructor, the date can be in a variety of formats, but must at least include the month, date, and year. For example, <code>Feb 1 2005</code> is valid, but <code>Feb 2005</code> is not. The following list indicates some of the valid formats:</p>
		 * <ul>
		 *  <li>Day Month Date Hours:Minutes:Seconds GMT Year (for instance, "Tue Feb 1 00:00:00 GMT-0800 2005", which matches <code>toString()</code>)</li>
		 *  <li>Day Month Date Year Hours:Minutes:Seconds AM/PM (for instance, "Tue Feb 1 2005 12:00:00 AM", which matches <code>toLocaleString()</code>)</li>
		 *  <li>Day Month Date Year (for instance, "Tue Feb 1 2005", which matches <code>toDateString()</code>)</li>
		 *  <li>Month/Day/Year (for instance, "02/01/2005")</li>
		 *  <li>Month/Year (for instance, "02/2005")</li>
		 * </ul>
		 * 
		 * @param yearOrTimevalue  — If other parameters are specified, this number represents a year (such as 1965); otherwise, it represents a time value. If the number represents a year, a value of 0 to 99 indicates 1900 through 1999; otherwise all four digits of the year must be specified. If the number represents a time value (no other parameters are specified), it is the number of milliseconds before or after 0:00:00 GMT January 1, 1970; a negative values represents a time <i>before</i> 0:00:00 GMT January 1, 1970, and a positive value represents a time after. 
		 * @param month  — An integer from 0 (January) to 11 (December). 
		 * @param date  — An integer from 1 to 31. 
		 * @param hour  — An integer from 0 (midnight) to 23 (11 p.m.). 
		 * @param minute  — An integer from 0 to 59. 
		 * @param second  — An integer from 0 to 59. 
		 * @param millisecond  — An integer from 0 to 999 of milliseconds. 
		 */
		public function Date(yearOrTimevalue:Object=null, month:Number=0, date:Number = 1, hour:Number = 0, minute:Number = 0, second:Number = 0, millisecond:Number = 0) {
			throw new Error("Not implemented");
		}

		/**
		 * <p> The day of the month (an integer from 1 to 31) specified by a <code>Date</code> object according to local time. Local time is determined by the operating system on which the Flash runtimes are running. </p>
		 * 
		 * @return 
		 */
		public function get date():Number {
			return _date;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set date(value:Number):void {
			_date = value;
		}

		/**
		 * <p> The day of the month (an integer from 1 to 31) of a <code>Date</code> object according to universal time (UTC). </p>
		 * 
		 * @return 
		 */
		public function get dateUTC():Number {
			return _dateUTC;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set dateUTC(value:Number):void {
			_dateUTC = value;
		}

		/**
		 * <p> The day of the week (0 for Sunday, 1 for Monday, and so on) specified by this <code>Date</code> according to local time. Local time is determined by the operating system on which the Flash runtimes are running. </p>
		 * 
		 * @return 
		 */
		public function get day():Number {
			return _day;
		}

		/**
		 * <p> The day of the week (0 for Sunday, 1 for Monday, and so on) of this <code>Date </code> according to universal time (UTC). </p>
		 * 
		 * @return 
		 */
		public function get dayUTC():Number {
			return _dayUTC;
		}

		/**
		 * <p> The full year (a four-digit number, such as 2000) of a <code>Date</code> object according to local time. Local time is determined by the operating system on which the Flash runtimes are running. </p>
		 * 
		 * @return 
		 */
		public function get fullYear():Number {
			return _fullYear;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set fullYear(value:Number):void {
			_fullYear = value;
		}

		/**
		 * <p> The four-digit year of a <code>Date</code> object according to universal time (UTC). </p>
		 * 
		 * @return 
		 */
		public function get fullYearUTC():Number {
			return _fullYearUTC;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set fullYearUTC(value:Number):void {
			_fullYearUTC = value;
		}

		/**
		 * <p> The hour (an integer from 0 to 23) of the day portion of a <code>Date</code> object according to local time. Local time is determined by the operating system on which the Flash runtimes are running. </p>
		 * 
		 * @return 
		 */
		public function get hours():Number {
			return _hours;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set hours(value:Number):void {
			_hours = value;
		}

		/**
		 * <p> The hour (an integer from 0 to 23) of the day of a <code>Date</code> object according to universal time (UTC). </p>
		 * 
		 * @return 
		 */
		public function get hoursUTC():Number {
			return _hoursUTC;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set hoursUTC(value:Number):void {
			_hoursUTC = value;
		}

		/**
		 * <p> The milliseconds (an integer from 0 to 999) portion of a <code>Date</code> object according to local time. Local time is determined by the operating system on which the Flash runtimes are running. </p>
		 * 
		 * @return 
		 */
		public function get milliseconds():Number {
			return _milliseconds;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set milliseconds(value:Number):void {
			_milliseconds = value;
		}

		/**
		 * <p> The milliseconds (an integer from 0 to 999) portion of a <code>Date</code> object according to universal time (UTC). </p>
		 * 
		 * @return 
		 */
		public function get millisecondsUTC():Number {
			return _millisecondsUTC;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set millisecondsUTC(value:Number):void {
			_millisecondsUTC = value;
		}

		/**
		 * <p> The minutes (an integer from 0 to 59) portion of a <code>Date</code> object according to local time. Local time is determined by the operating system on which the Flash runtimes are running. </p>
		 * 
		 * @return 
		 */
		public function get minutes():Number {
			return _minutes;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set minutes(value:Number):void {
			_minutes = value;
		}

		/**
		 * <p> The minutes (an integer from 0 to 59) portion of a <code>Date</code> object according to universal time (UTC). </p>
		 * 
		 * @return 
		 */
		public function get minutesUTC():Number {
			return _minutesUTC;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set minutesUTC(value:Number):void {
			_minutesUTC = value;
		}

		/**
		 * <p> The month (0 for January, 1 for February, and so on) portion of a <code> Date</code> object according to local time. Local time is determined by the operating system on which the Flash runtimes are running. </p>
		 * 
		 * @return 
		 */
		public function get month():Number {
			return _month;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set month(value:Number):void {
			_month = value;
		}

		/**
		 * <p> The month (0 [January] to 11 [December]) portion of a <code>Date</code> object according to universal time (UTC). </p>
		 * 
		 * @return 
		 */
		public function get monthUTC():Number {
			return _monthUTC;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set monthUTC(value:Number):void {
			_monthUTC = value;
		}

		/**
		 * <p> The seconds (an integer from 0 to 59) portion of a <code>Date</code> object according to local time. Local time is determined by the operating system on which the Flash runtimes are running. </p>
		 * 
		 * @return 
		 */
		public function get seconds():Number {
			return _seconds;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set seconds(value:Number):void {
			_seconds = value;
		}

		/**
		 * <p> The seconds (an integer from 0 to 59) portion of a <code>Date</code> object according to universal time (UTC). </p>
		 * 
		 * @return 
		 */
		public function get secondsUTC():Number {
			return _secondsUTC;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set secondsUTC(value:Number):void {
			_secondsUTC = value;
		}

		/**
		 * <p> The number of milliseconds since midnight January 1, 1970, universal time, for a <code>Date</code> object. Use this method to represent a specific instant in time when comparing two or more <code>Date</code> objects. </p>
		 * 
		 * @return 
		 */
		public function get time():Number {
			return _time;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set time(value:Number):void {
			_time = value;
		}

		/**
		 * <p> The difference, in minutes, between universal time (UTC) and the computer's local time. Specifically, this value is the number of minutes you need to add to the computer's local time to equal UTC. If your computer's time is set later than UTC, the value will be negative. </p>
		 * 
		 * @return 
		 */
		public function get timezoneOffset():Number {
			return _timezoneOffset;
		}

		/**
		 * <p> Returns the day of the month (an integer from 1 to 31) specified by a <code>Date</code> object according to local time. Local time is determined by the operating system on which the Flash runtimes are running. </p>
		 * 
		 * @return  — The day of the month (1 - 31) a  object represents. 
		 */
		public function getDate():Number {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the day of the week (0 for Sunday, 1 for Monday, and so on) specified by this <code>Date</code> according to local time. Local time is determined by the operating system on which the Flash runtimes are running. </p>
		 * 
		 * @return  — A numeric version of the day of the week (0 - 6) a  object represents. 
		 */
		public function getDay():Number {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the full year (a four-digit number, such as 2000) of a <code>Date</code> object according to local time. Local time is determined by the operating system on which the Flash runtimes are running. </p>
		 * 
		 * @return  — The full year a  object represents. 
		 */
		public function getFullYear():Number {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the hour (an integer from 0 to 23) of the day portion of a <code>Date</code> object according to local time. Local time is determined by the operating system on which the Flash runtimes are running. </p>
		 * 
		 * @return  — The hour (0 - 23) of the day a  object represents. 
		 */
		public function getHours():Number {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the milliseconds (an integer from 0 to 999) portion of a <code>Date</code> object according to local time. Local time is determined by the operating system on which the Flash runtimes are running. </p>
		 * 
		 * @return  — The milliseconds portion of a  object. 
		 */
		public function getMilliseconds():Number {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the minutes (an integer from 0 to 59) portion of a <code>Date</code> object according to local time. Local time is determined by the operating system on which the Flash runtimes are running. </p>
		 * 
		 * @return  — The minutes portion of a  object. 
		 */
		public function getMinutes():Number {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the month (0 for January, 1 for February, and so on) portion of this <code> Date</code> according to local time. Local time is determined by the operating system on which the Flash runtimes are running. </p>
		 * 
		 * @return  — The month (0 - 11) portion of a  object. 
		 */
		public function getMonth():Number {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the seconds (an integer from 0 to 59) portion of a <code>Date</code> object according to local time. Local time is determined by the operating system on which the Flash runtimes are running. </p>
		 * 
		 * @return  — The seconds (0 to 59) portion of a  object. 
		 */
		public function getSeconds():Number {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the number of milliseconds since midnight January 1, 1970, universal time, for a <code>Date</code> object. Use this method to represent a specific instant in time when comparing two or more <code>Date</code> objects. </p>
		 * 
		 * @return  — The number of milliseconds since Jan 1, 1970 that a  object represents. 
		 */
		public function getTime():Number {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the difference, in minutes, between universal time (UTC) and the computer's local time. </p>
		 * 
		 * @return  — The minutes you need to add to the computer's local time value to equal UTC. If your computer's time is set later than UTC, the return value will be negative. 
		 */
		public function getTimezoneOffset():Number {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the day of the month (an integer from 1 to 31) of a <code>Date</code> object, according to universal time (UTC). </p>
		 * 
		 * @return  — The UTC day of the month (1 to 31) that a  object represents. 
		 */
		public function getUTCDate():Number {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the day of the week (0 for Sunday, 1 for Monday, and so on) of this <code>Date </code> according to universal time (UTC). </p>
		 * 
		 * @return  — The UTC day of the week (0 to 6) that a  object represents. 
		 */
		public function getUTCDay():Number {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the four-digit year of a <code>Date</code> object according to universal time (UTC). </p>
		 * 
		 * @return  — The UTC four-digit year a  object represents. 
		 */
		public function getUTCFullYear():Number {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the hour (an integer from 0 to 23) of the day of a <code>Date</code> object according to universal time (UTC). </p>
		 * 
		 * @return  — The UTC hour of the day (0 to 23) a  object represents. 
		 */
		public function getUTCHours():Number {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the milliseconds (an integer from 0 to 999) portion of a <code>Date</code> object according to universal time (UTC). </p>
		 * 
		 * @return  — The UTC milliseconds portion of a  object. 
		 */
		public function getUTCMilliseconds():Number {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the minutes (an integer from 0 to 59) portion of a <code>Date</code> object according to universal time (UTC). </p>
		 * 
		 * @return  — The UTC minutes portion of a  object. 
		 */
		public function getUTCMinutes():Number {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the month (0 [January] to 11 [December]) portion of a <code>Date</code> object according to universal time (UTC). </p>
		 * 
		 * @return  — The UTC month portion of a  object. 
		 */
		public function getUTCMonth():Number {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the seconds (an integer from 0 to 59) portion of a <code>Date</code> object according to universal time (UTC). </p>
		 * 
		 * @return  — The UTC seconds portion of a  object. 
		 */
		public function getUTCSeconds():Number {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets the day of the month, according to local time, and returns the new time in milliseconds. Local time is determined by the operating system on which the Flash runtimes are running. </p>
		 * 
		 * @param day  — An integer from 1 to 31. 
		 * @return  — The new time, in milliseconds. 
		 */
		public function setDate(day:Number):Number {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets the year, according to local time, and returns the new time in milliseconds. If the <code>month</code> and <code>day</code> parameters are specified, they are set to local time. Local time is determined by the operating system on which the Flash runtimes are running. </p>
		 * <p> Calling this method does not modify the other fields of the <code>Date</code> but <code>Date.getUTCDay()</code> and <code>Date.getDay()</code> can report a new value if the day of the week changes as a result of calling this method. </p>
		 * 
		 * @param year  — A four-digit number specifying a year. Two-digit numbers do not represent four-digit years; for example, 99 is not the year 1999, but the year 99. 
		 * @param month  — An integer from 0 (January) to 11 (December). 
		 * @param day  — A number from 1 to 31. 
		 * @return  — The new time, in milliseconds. 
		 */
		public function setFullYear(year:Number, month:Number, day:Number):Number {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets the hour, according to local time, and returns the new time in milliseconds. Local time is determined by the operating system on which the Flash runtimes are running. </p>
		 * 
		 * @param hour  — An integer from 0 (midnight) to 23 (11 p.m.). 
		 * @param minute  — An integer from 0 to 59. 
		 * @param second  — An integer from 0 to 59. 
		 * @param millisecond  — An integer from 0 to 999. 
		 * @return  — The new time, in milliseconds. 
		 */
		public function setHours(hour:Number, minute:Number, second:Number, millisecond:Number):Number {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets the milliseconds, according to local time, and returns the new time in milliseconds. Local time is determined by the operating system on which the Flash runtimes are running. </p>
		 * 
		 * @param millisecond  — An integer from 0 to 999. 
		 * @return  — The new time, in milliseconds. 
		 */
		public function setMilliseconds(millisecond:Number):Number {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets the minutes, according to local time, and returns the new time in milliseconds. Local time is determined by the operating system on which the Flash runtimes are running. </p>
		 * 
		 * @param minute  — An integer from 0 to 59. 
		 * @param second  — An integer from 0 to 59. 
		 * @param millisecond  — An integer from 0 to 999. 
		 * @return  — The new time, in milliseconds. 
		 */
		public function setMinutes(minute:Number, second:Number, millisecond:Number):Number {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets the month and optionally the day of the month, according to local time, and returns the new time in milliseconds. Local time is determined by the operating system on which the Flash runtimes are running. </p>
		 * 
		 * @param month  — An integer from 0 (January) to 11 (December). 
		 * @param day  — An integer from 1 to 31. 
		 * @return  — The new time, in milliseconds. 
		 */
		public function setMonth(month:Number, day:Number):Number {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets the seconds, according to local time, and returns the new time in milliseconds. Local time is determined by the operating system on which the Flash runtimes are running. </p>
		 * 
		 * @param second  — An integer from 0 to 59. 
		 * @param millisecond  — An integer from 0 to 999. 
		 * @return  — The new time, in milliseconds. 
		 */
		public function setSeconds(second:Number, millisecond:Number):Number {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets the date in milliseconds since midnight on January 1, 1970, and returns the new time in milliseconds. </p>
		 * 
		 * @param millisecond  — An integer value where 0 is midnight on January 1, universal time (UTC). 
		 * @return  — The new time, in milliseconds. 
		 */
		public function setTime(millisecond:Number):Number {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets the day of the month, in universal time (UTC), and returns the new time in milliseconds. Calling this method does not modify the other fields of a <code>Date </code> object, but the <code>Date.getUTCDay()</code> and <code>Date.getDay()</code> methods can report a new value if the day of the week changes as a result of calling this method. </p>
		 * 
		 * @param day  — A number; an integer from 1 to 31. 
		 * @return  — The new time, in milliseconds. 
		 */
		public function setUTCDate(day:Number):Number {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets the year, in universal time (UTC), and returns the new time in milliseconds. </p>
		 * <p> Optionally, this method can also set the month and day of the month. Calling this method does not modify the other fields, but the <code>Date.getUTCDay()</code> and <code>Date.getDay()</code> methods can report a new value if the day of the week changes as a result of calling this method. </p>
		 * 
		 * @param year  — An integer that represents the year specified as a full four-digit year, such as 2000. 
		 * @param month  — An integer from 0 (January) to 11 (December). 
		 * @param day  — An integer from 1 to 31. 
		 * @return  — An integer. 
		 */
		public function setUTCFullYear(year:Number, month:Number, day:Number):Number {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets the hour, in universal time (UTC), and returns the new time in milliseconds. Optionally, the minutes, seconds, and milliseconds can be specified. </p>
		 * 
		 * @param hour  — An integer from 0 (midnight) to 23 (11 p.m.). 
		 * @param minute  — An integer from 0 to 59. 
		 * @param second  — An integer from 0 to 59. 
		 * @param millisecond  — An integer from 0 to 999. 
		 * @return  — The new time, in milliseconds. 
		 */
		public function setUTCHours(hour:Number, minute:Number, second:Number, millisecond:Number):Number {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets the milliseconds, in universal time (UTC), and returns the new time in milliseconds. </p>
		 * 
		 * @param millisecond  — An integer from 0 to 999. 
		 * @return  — The new time, in milliseconds. 
		 */
		public function setUTCMilliseconds(millisecond:Number):Number {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets the minutes, in universal time (UTC), and returns the new time in milliseconds. Optionally, you can specify the seconds and milliseconds. </p>
		 * 
		 * @param minute  — An integer from 0 to 59. 
		 * @param second  — An integer from 0 to 59. 
		 * @param millisecond  — An integer from 0 to 999. 
		 * @return  — The new time, in milliseconds. 
		 */
		public function setUTCMinutes(minute:Number, second:Number, millisecond:Number):Number {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets the month, and optionally the day, in universal time(UTC) and returns the new time in milliseconds. Calling this method does not modify the other fields, but the <code>Date.getUTCDay()</code> and <code>Date.getDay()</code> methods might report a new value if the day of the week changes as a result of calling this method. </p>
		 * 
		 * @param month  — An integer from 0 (January) to 11 (December). 
		 * @param day  — An integer from 1 to 31. 
		 * @return  — The new time, in milliseconds. 
		 */
		public function setUTCMonth(month:Number, day:Number):Number {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets the seconds, and optionally the milliseconds, in universal time (UTC) and returns the new time in milliseconds. </p>
		 * 
		 * @param second  — An integer from 0 to 59. 
		 * @param millisecond  — An integer from 0 to 999. 
		 * @return  — The new time, in milliseconds. 
		 */
		public function setUTCSeconds(second:Number, millisecond:Number):Number {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a string representation of the day and date only, and does not include the time or timezone. Contrast with the following methods: </p>
		 * <ul>
		 *  <li><code>Date.toTimeString()</code>, which returns only the time and timezone</li>
		 *  <li><code>Date.toString()</code>, which returns not only the day and date, but also the time and timezone.</li>
		 * </ul>
		 * 
		 * @return  — The string representation of day and date only. 
		 */
		public function toDateString():String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Provides an overridable method for customizing the JSON encoding of values in an Date object. </p>
		 * <p>The <code>JSON.stringify()</code> method looks for a <code>toJSON()</code> method on each object that it traverses. If the <code>toJSON()</code> method is found, <code>JSON.stringify()</code> calls it for each value it encounters, passing in the key that is paired with the value.</p>
		 * <p>Date provides a default implementation of <code>toJSON()</code> that returns the output of <code>Date.toString()</code>. Clients that wish to export Date objects to JSON in any other format can provide their own implementations. You can do this by redefining the <code>toJSON()</code> method on the class prototype. </p>
		 * <p> The <code>toJSON()</code> method can return a value of any type. If it returns an object, <code>stringify()</code> recurses into that object. If <code>toJSON()</code> returns a string, <code>stringify()</code> does not recurse and continues its traversal.</p>
		 * 
		 * @param k  — The key of a key/value pair that <code>JSON.stringify()</code> has encountered in its traversal of this object 
		 * @return  — The object's value of Date.toString(). 
		 */
		/*public function toJSON(k:String):* {
			throw new Error("Not implemented");
		}*/

		/**
		 * <p> Returns a String representation of the day and date only, and does not include the time or timezone. This method returns the same value as <code>Date.toDateString</code>. Contrast with the following methods: </p>
		 * <ul>
		 *  <li><code>Date.toTimeString()</code>, which returns only the time and timezone</li>
		 *  <li><code>Date.toString()</code>, which returns not only the day and date, but also the time and timezone.</li>
		 * </ul>
		 * 
		 * @return  — The  representation of day and date only. 
		 */
		public function toLocaleDateString():String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a String representation of the day, date, time, given in local time. Contrast with the <code>Date.toString()</code> method, which returns the same information (plus the timezone) with the year listed at the end of the string. </p>
		 * 
		 * @return  — A string representation of a  object in the local timezone. 
		 */
		public function toLocaleString():String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a String representation of the time only, and does not include the day, date, year, or timezone. Contrast with the <code>Date.toTimeString()</code> method, which returns the time and timezone. </p>
		 * 
		 * @return  — The string representation of time and timezone only. 
		 */
		public function toLocaleTimeString():String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a String representation of the day, date, time, and timezone. The date format for the output is: </p>
		 * <pre>
		 *      Day Mon Date HH:MM:SS TZD YYYY
		 *      </pre>
		 * <p>For example:</p>
		 * <pre>
		 *      Wed Apr 12 15:30:17 GMT-0700 2006
		 *      </pre>
		 * 
		 * @return  — The string representation of a  object. 
		 */
		/*override public function toString():String {
			throw new Error("Not implemented");
		}*/

		/**
		 * <p> Returns a String representation of the time and timezone only, and does not include the day and date. Contrast with the <code>Date.toDateString()</code> method, which returns only the day and date. </p>
		 * 
		 * @return  — The string representation of time and timezone only. 
		 */
		public function toTimeString():String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a String representation of the day, date, and time in universal time (UTC). For example, the date February 1, 2005 is returned as <code>Tue Feb 1 00:00:00 2005 UTC</code>. </p>
		 * 
		 * @return  — The string representation of a  object in UTC time. 
		 */
		public function toUTCString():String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the number of milliseconds since midnight January 1, 1970, universal time, for a <code>Date</code> object. </p>
		 * 
		 * @return  — The number of milliseconds since January 1, 1970 that a  object represents. 
		 */
		public function valueOf():* {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Returns the number of milliseconds between midnight on January 1, 1970, universal time, and the time specified in the parameters. This method uses universal time, whereas the <code>Date</code> constructor uses local time. </p>
		 * <p>This method is useful if you want to pass a UTC date to the Date class constructor. Because the Date class constructor accepts the millisecond offset as an argument, you can use the Date.UTC() method to convert your UTC date into the corresponding millisecond offset, and send that offset as an argument to the Date class constructor:</p>
		 * 
		 * @param year  — A four-digit integer that represents the year (for example, 2000). 
		 * @param month  — An integer from 0 (January) to 11 (December). 
		 * @param date  — An integer from 1 to 31. 
		 * @param hour  — An integer from 0 (midnight) to 23 (11 p.m.). 
		 * @param minute  — An integer from 0 to 59. 
		 * @param second  — An integer from 0 to 59. 
		 * @param millisecond  — An integer from 0 to 999. 
		 * @return  — The number of milliseconds since January 1, 1970 and the specified date and time. 
		 */
		public static function UTC(year:Number, month:Number, date:Number = 1, hour:Number = 0, minute:Number = 0, second:Number = 0, millisecond:Number = 0):Number {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Converts a string representing a date into a number equaling the number of milliseconds elapsed since January 1, 1970, UTC. </p>
		 * 
		 * @param date  — A string representation of a date, which conforms to the format for the output of <code>Date.toString()</code>. The date format for the output of <code>Date.toString()</code> is: <pre>
		 *      Day Mon DD HH:MM:SS TZD YYYY
		 *      </pre> <p>For example: </p> <pre>
		 *      Wed Apr 12 15:30:17 GMT-0700 2006
		 *      </pre> <p>The Time Zone Designation (TZD) is always in the form <code>GMT-HHMM</code> or <code>UTC-HHMM</code> indicating the hour and minute offset relative to Greenwich Mean Time (GMT), which is now also called universal time (UTC). The year month and day terms can be separated by a forward slash (<code>/</code>) or by spaces, but never by a dash (<code>-</code>). Other supported formats include the following (you can include partial representations of these formats; that is, just the month, day, and year):</p> <pre>
		 *      MM/DD/YYYY HH:MM:SS TZD
		 *      HH:MM:SS TZD Day Mon/DD/YYYY 
		 *      Mon DD YYYY HH:MM:SS TZD
		 *      Day Mon DD HH:MM:SS TZD YYYY
		 *      Day DD Mon HH:MM:SS TZD YYYY
		 *      Mon/DD/YYYY HH:MM:SS TZD
		 *      YYYY/MM/DD HH:MM:SS TZD
		 *      </pre> 
		 * @return  — A number representing the milliseconds elapsed since January 1, 1970, UTC. 
		 */
		public static function parse(date:String):Number {
			throw new Error("Not implemented");
		}
	}
}
