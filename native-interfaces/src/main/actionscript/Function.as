package {
	/**
	 *  A function is the basic unit of code that can be invoked in ActionScript. Both user-defined and built-in functions in ActionScript are represented by Function objects, which are instances of the Function class. <p>Methods of a class are slightly different than Function objects. Unlike an ordinary function object, a method is tightly linked to its associated class object. Therefore, a method or property has a definition that is shared among all instances of the same class. Methods can be extracted from an instance and treated as "bound" methods (retaining the link to the original instance). For a bound method, the <code>this</code> keyword points to the original object that implemented the method. For a function, <code>this</code> points to the associated object at the time the function is invoked.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><br><hr>
	 */
	public dynamic class Function {

		/**
		 * <p> Specifies the value of <code>thisObject</code> to be used within any function that ActionScript calls. This method also specifies the parameters to be passed to any called function. Because <code>apply()</code> is a method of the Function class, it is also a method of every Function object in ActionScript. </p>
		 * <p>The parameters are specified as an Array object, unlike <code>Function.call()</code>, which specifies parameters as a comma-delimited list. This is often useful when the number of parameters to be passed is not known until the script actually executes.</p>
		 * <p>Returns the value that the called function specifies as the return value.</p>
		 * 
		 * @param thisArg  — The object to which the function is applied. 
		 * @param argArray  — An array whose elements are passed to the function as parameters. 
		 * @return  — Any value that the called function specifies. 
		 */
		public function apply(thisArg:* = NaN, argArray:* = NaN):* {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Invokes the function represented by a Function object. Every function in ActionScript is represented by a Function object, so all functions support this method. </p>
		 * <p>In almost all cases, the function call (<code>()</code>) operator can be used instead of this method. The function call operator produces code that is concise and readable. This method is primarily useful when the <code>thisObject</code> parameter of the function invocation needs to be explicitly controlled. Normally, if a function is invoked as a method of an object within the body of the function, <code>thisObject</code> is set to <code>myObject</code>, as shown in the following example:</p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *   myObject.myMethod(1, 2, 3);
		 *   </pre>
		 * </div>
		 * <p>In some situations, you might want <code>thisObject</code> to point somewhere else; for example, if a function must be invoked as a method of an object, but is not actually stored as a method of that object:</p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *   myObject.myMethod.call(myOtherObject, 1, 2, 3); 
		 *   </pre>
		 * </div>
		 * <p>You can pass the value <code>null</code> for the <code>thisObject</code> parameter to invoke a function as a regular function and not as a method of an object. For example, the following function invocations are equivalent:</p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *   Math.sin(Math.PI / 4)
		 *   Math.sin.call(null, Math.PI / 4)
		 *   </pre>
		 * </div>
		 * <p>Returns the value that the called function specifies as the return value.</p>
		 * 
		 * @param thisArg  — An object that specifies the value of <code>thisObject</code> within the function body. 
		 * @param args  — The parameter or parameters to be passed to the function. You can specify zero or more parameters. 
		 * @return 
		 */
		public function call(thisArg:* = NaN, ...args):* {
			throw new Error("Not implemented");
		}
	}
}
