package {

	/**
	 * <p> Returns <code>true</code> if the value is a finite number, or <code>false</code> if the value is <code>Infinity</code> or <code>-Infinity</code>. The presence of <code>Infinity</code> or <code>-Infinity</code> indicates a mathematical error condition such as division by 0. </p>
	 * 
	 * @param num  — A number to evaluate as finite or infinite. 
	 * @return  — Returns  if it is a finite number or  if it is infinity or negative infinity 
	 */
	public function isFinite(num:Number):Boolean {
		throw new Error("Not implemented");
	}
}
