package {
	/**
	 *  The Array class lets you access and manipulate arrays. Array indices are zero-based, which means that the first element in the array is <code>[0]</code>, the second element is <code>[1]</code>, and so on. To create an Array object, you use the <code>new Array()</code> constructor . <code>Array()</code> can also be invoked as a function. In addition, you can use the array access (<code>[]</code>) operator to initialize an array or access the elements of an array. <p>You can store a wide variety of data types in an array element, including numbers, strings, objects, and even other arrays. You can create a <i>multidimensional</i> array by creating an indexed array and assigning to each of its elements a different indexed array. Such an array is considered multidimensional because it can be used to represent data in a table.</p> <p> Arrays are <i>sparse arrays</i>, meaning there might be an element at index 0 and another at index 5, but nothing in the index positions between those two elements. In such a case, the elements in positions 1 through 4 are undefined, which indicates the absence of an element, not necessarily the presence of an element with the value <code>undefined</code>.</p> <p>Array assignment is by reference rather than by value. When you assign one array variable to another array variable, both refer to the same array:</p> <div class="listing">
	 *  <div class="clipcopy">
	 *   <a href="#" class="copyText">Copy</a>
	 *  </div>
	 *  <pre>
	 *  var oneArray:Array = new Array("a", "b", "c");
	 *  var twoArray:Array = oneArray; // Both array variables refer to the same array.
	 *  twoArray[0] = "z";             
	 *  trace(oneArray);               // Output: z,b,c.
	 *  </pre>
	 * </div> <p>Do not use the Array class to create <i>associative arrays</i> (also called <i>hashes</i>), which are data structures that contain named elements instead of numbered elements. To create associative arrays, use the Object class. Although ActionScript permits you to create associative arrays using the Array class, you cannot use any of the Array class methods or properties with associative arrays. </p> <p>You can extend the Array class and override or add methods. However, you must specify the subclass as <code>dynamic</code> or you will lose the ability to store data in an array.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7ee1.html" target="_blank">Creating arrays</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7ee0.html" target="_blank">Inserting array elements</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7edf.html" target="_blank">Retrieving values and removing array elements</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7fa4.html" target="_blank">Sorting an array</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7edd.html" target="_blank">Querying an array</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b8d829-7fde.html" target="_blank">Extending the Array class</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7ee8.html" target="_blank">Arrays example: PlayList</a>
	 * </div><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7fdc.html" target="_blank">Working with arrays</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7ee3.html" target="_blank">Basics of arrays</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7ee5.html" target="_blank">Indexed arrays</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7eea.html" target="_blank">Associative arrays</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7ee9.html" target="_blank">Multidimensional arrays</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7ee7.html" target="_blank">Cloning arrays</a>
	 *  <br>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="operators.html#array_access" target="">[] (array access)</a>
	 *  <br>
	 *  <a href="Object.html" target="">Object class</a>
	 * </div><br><hr>
	 */
	public dynamic class Array {
		/**
		 * <p> Specifies case-insensitive sorting for the Array class sorting methods. You can use this constant for the <code>options</code> parameter in the <code>sort()</code> or <code>sortOn()</code> method. </p>
		 * <p>The value of this constant is 1.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Array.html#sort()" target="">Array.sort()</a>
		 *  <br>
		 *  <a href="Array.html#sortOn()" target="">Array.sortOn()</a>
		 * </div>
		 */
		public static const CASEINSENSITIVE:uint = 1;
		/**
		 * <p> Specifies descending sorting for the Array class sorting methods. You can use this constant for the <code>options</code> parameter in the <code>sort()</code> or <code>sortOn()</code> method. </p>
		 * <p>The value of this constant is 2.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Array.html#sort()" target="">Array.sort()</a>
		 *  <br>
		 *  <a href="Array.html#sortOn()" target="">Array.sortOn()</a>
		 * </div>
		 */
		public static const DESCENDING:uint = 2;
		/**
		 * <p> Specifies numeric (instead of character-string) sorting for the Array class sorting methods. Including this constant in the <code>options</code> parameter causes the <code>sort()</code> and <code>sortOn()</code> methods to sort numbers as numeric values, not as strings of numeric characters. Without the <code>NUMERIC</code> constant, sorting treats each array element as a character string and produces the results in Unicode order. </p>
		 * <p>For example, given the array of values <code>[2005, 7, 35]</code>, if the <code>NUMERIC</code> constant is <b>not</b> included in the <code>options</code> parameter, the sorted array is <code>[2005, 35, 7]</code>, but if the <code>NUMERIC</code> constant <b>is</b> included, the sorted array is <code>[7, 35, 2005]</code>. </p>
		 * <p>This constant applies only to numbers in the array; it does not apply to strings that contain numeric data such as <code>["23", "5"]</code>.</p>
		 * <p>The value of this constant is 16.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Array.html#sort()" target="">Array.sort()</a>
		 *  <br>
		 *  <a href="Array.html#sortOn()" target="">Array.sortOn()</a>
		 * </div>
		 */
		public static const NUMERIC:uint = 16;
		/**
		 * <p> Specifies that a sort returns an array that consists of array indices. You can use this constant for the <code>options</code> parameter in the <code>sort()</code> or <code>sortOn()</code> method, so you have access to multiple views of the array elements while the original array is unmodified. </p>
		 * <p>The value of this constant is 8.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Array.html#sort()" target="">Array.sort()</a>
		 *  <br>
		 *  <a href="Array.html#sortOn()" target="">Array.sortOn()</a>
		 * </div>
		 */
		public static const RETURNINDEXEDARRAY:uint = 8;
		/**
		 * <p> Specifies the unique sorting requirement for the Array class sorting methods. You can use this constant for the <code>options</code> parameter in the <code>sort()</code> or <code>sortOn()</code> method. The unique sorting option terminates the sort if any two elements or fields being sorted have identical values. </p>
		 * <p>The value of this constant is 4.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Array.html#sort()" target="">Array.sort()</a>
		 *  <br>
		 *  <a href="Array.html#sortOn()" target="">Array.sortOn()</a>
		 * </div>
		 */
		public static const UNIQUESORT:uint = 4;

		private var _length:uint;

		/**
		 * <p> Lets you create an array of the specified number of elements. If you don't specify any parameters, an array containing 0 elements is created. If you specify a number of elements, an array is created with <code>numElements</code> number of elements. </p>
		 * <p><b>Note:</b> This class shows two constructor method entries because the constructor accepts variable types of arguments. The constructor behaves differently depending on the type and number of arguments passed, as detailed in each entry. ActionScript 3.0 does not support method or constructor overloading.</p>
		 * 
		 * @param numElements  — An integer that specifies the number of elements in the array. 
		 */
		/*public function Array(numElements:int = 0) {
			throw new Error("Not implemented");
		}*/

		/**
		 * <p> Lets you create an array that contains the specified elements. You can specify values of any type. The first element in an array always has an index (or position) of 0. </p>
		 * <p><b>Note:</b> This class shows two constructor entries because the constructor accepts variable types of arguments. The constructor behaves differently depending on the type and number of arguments passed, as detailed in each entry. ActionScript 3.0 does not support method or constructor overloading.</p>
		 * 
		 * @param values  — A comma-separated list of one or more arbitrary values. <p><b>Note: </b>If only a single numeric parameter is passed to the Array constructor, it is assumed to specify the array's <code>length</code> property.</p> 
		 */
		public function Array(...values) {
			throw new Error("Not implemented");
		}

		/**
		 * <p> A non-negative integer specifying the number of elements in the array. This property is automatically updated when new elements are added to the array. When you assign a value to an array element (for example, <code>my_array[index] = value</code>), if <code>index</code> is a number, and <code>index+1</code> is greater than the <code>length</code> property, the <code>length</code> property is updated to <code>index+1</code>. </p>
		 * <p><b>Note: </b>If you assign a value to the <code>length</code> property that is shorter than the existing length, the array will be truncated.</p>
		 * 
		 * @return 
		 */
		public function get length():uint {
			return _length;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set length(value:uint):void {
			_length = value;
		}

		/**
		 * <p> Concatenates the elements specified in the parameters with the elements in an array and creates a new array. If the parameters specify an array, the elements of that array are concatenated. If you don't pass any parameters, the new array is a duplicate (shallow clone) of the original array. </p>
		 * 
		 * @param args  — A value of any data type (such as numbers, elements, or strings) to be concatenated in a new array. 
		 * @return  — An array that contains the elements from this array followed by elements from the parameters. 
		 */
		public function concat(...args):Array {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Executes a test function on each item in the array until an item is reached that returns <code>false</code> for the specified function. You use this method to determine whether all items in an array meet a criterion, such as having values less than a particular number. </p>
		 * <p>For this method, the second parameter, <code>thisObject</code>, must be <code>null</code> if the first parameter, <code>callback</code>, is a method closure. Suppose you create a function in a movie clip called <code>me</code>:</p>
		 * <pre class="flash">
		 *      function myFunction(obj:Object):void {
		 *         //your code here
		 *      }
		 *      </pre>
		 * <p>Suppose you then use the <code>every()</code> method on an array called <code>myArray</code>:</p>
		 * <pre class="flash">
		 *      myArray.every(myFunction, me);
		 *      </pre>
		 * <p>Because <code>myFunction</code> is a member of the Timeline class, which cannot be overridden by <code>me</code>, the Flash runtime will throw an exception. You can avoid this runtime error by assigning the function to a variable, as follows:</p>
		 * <pre class="flash">
		 *      var myFunction:Function = function(obj:Object):void {
		 *          //your code here
		 *      };
		 *      myArray.every(myFunction, me);
		 *      </pre>
		 * 
		 * @param callback  — The function to run on each item in the array. This function can contain a simple comparison (for example, <code>item &lt; 20</code>) or a more complex operation, and is invoked with three arguments; the value of an item, the index of an item, and the Array object: <pre>function callback(item:*, index:int, array:Array):Boolean;</pre> 
		 * @param thisObject  — An object to use as <code>this</code> for the function. 
		 * @return  — A Boolean value of  if all items in the array return  for the specified function; otherwise, . 
		 */
		public function every(callback:Function, thisObject:* = null):Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Executes a test function on each item in the array and constructs a new array for all items that return <code>true</code> for the specified function. If an item returns <code>false</code>, it is not included in the new array. </p>
		 * <p>For this method, the second parameter, <code>thisObject</code>, must be <code>null</code> if the first parameter, <code>callback</code>, is a method closure. Suppose you create a function in a movie clip called <code>me</code>:</p>
		 * <pre class="flash">
		 *      function myFunction(obj:Object):void {
		 *         //your code here
		 *      }
		 *      </pre>
		 * <p>Suppose you then use the <code>filter()</code> method on an array called <code>myArray</code>:</p>
		 * <pre class="flash"> 
		 *      myArray.filter(myFunction, me);
		 *      </pre>
		 * <p>Because <code>myFunction</code> is a member of the Timeline class, which cannot be overridden by <code>me</code>, the Flash runtime will throw an exception. You can avoid this runtime error by assigning the function to a variable, as follows:</p>
		 * <pre class="flash">
		 *      var myFunction:Function = function(obj:Object):void {
		 *          //your code here
		 *          };
		 *      myArray.filter(myFunction, me);
		 *      </pre>
		 * 
		 * @param callback  — The function to run on each item in the array. This function can contain a simple comparison (for example, <code>item &lt; 20</code>) or a more complex operation, and is invoked with three arguments; the value of an item, the index of an item, and the Array object: <pre>    function callback(item:*, index:int, array:Array):Boolean;</pre> 
		 * @param thisObject  — An object to use as <code>this</code> for the function. 
		 * @return  — A new array that contains all items from the original array that returned . 
		 */
		public function filter(callback:Function, thisObject:* = null):Array {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Executes a function on each item in the array. </p>
		 * <p>For this method, the second parameter, <code>thisObject</code>, must be <code>null</code> if the first parameter, <code>callback</code>, is a method closure. Suppose you create a function in a movie clip called <code>me</code>:</p>
		 * <pre class="flash">
		 *      function myFunction(obj:Object):void {
		 *         //your code here
		 *      }
		 *      </pre>
		 * <p>Suppose you then use the <code>forEach()</code> method on an array called <code>myArray</code>:</p>
		 * <pre class="flash">
		 *      myArray.forEach(myFunction, me);
		 *      </pre>
		 * <p>Because <code>myFunction</code> is a member of the Timeline class, which cannot be overridden by <code>me</code>, the Flash runtime will throw an exception. You can avoid this runtime error by assigning the function to a variable, as follows:</p>
		 * <pre class="flash">
		 *      var myFunction:Function = function(obj:Object):void {
		 *          //your code here
		 *          };
		 *      myArray.forEach(myFunction, me);
		 *      </pre>
		 * 
		 * @param callback  — The function to run on each item in the array. This function can contain a simple command (for example, a <code>trace()</code> statement) or a more complex operation, and is invoked with three arguments; the value of an item, the index of an item, and the Array object: <pre>    function callback(item:*, index:int, array:Array):void;</pre> 
		 * @param thisObject  — An object to use as <code>this</code> for the function. 
		 */
		public function forEach(callback:Function, thisObject:* = null):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Searches for an item in an array by using strict equality (<code>===</code>) and returns the index position of the item. </p>
		 * 
		 * @param searchElement  — The item to find in the array. 
		 * @param fromIndex  — The location in the array from which to start searching for the item. 
		 * @return  — A zero-based index position of the item in the array. If the  argument is not found, the return value is -1. 
		 */
		public function indexOf(searchElement:*, fromIndex:int = 0):int {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Insert a single element into an array. This method modifies the array without making a copy. </p>
		 * 
		 * @param index  — An integer that specifies the position in the array where the element is to be inserted. You can use a negative integer to specify a position relative to the end of the array (for example, -1 is the last element of the array). 
		 * @param element
		 */
		public function insertAt(index:int, element:*):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Converts the elements in an array to strings, inserts the specified separator between the elements, concatenates them, and returns the resulting string. A nested array is always separated by a comma (,), not by the separator passed to the <code>join()</code> method. </p>
		 * 
		 * @param sep  — A character or string that separates array elements in the returned string. If you omit this parameter, a comma is used as the default separator. 
		 * @return  — A string consisting of the elements of an array converted to strings and separated by the specified parameter. 
		 */
		public function join(sep:* = NaN):String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Searches for an item in an array, working backward from the last item, and returns the index position of the matching item using strict equality (<code>===</code>). </p>
		 * 
		 * @param searchElement  — The item to find in the array. 
		 * @param fromIndex  — The location in the array from which to start searching for the item. The default is the maximum value allowed for an index. If you do not specify <code>fromIndex</code>, the search starts at the last item in the array. 
		 * @return  — A zero-based index position of the item in the array. If the  argument is not found, the return value is -1. 
		 */
		public function lastIndexOf(searchElement:*, fromIndex:int = 0x7fffffff):int {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Executes a function on each item in an array, and constructs a new array of items corresponding to the results of the function on each item in the original array. </p>
		 * <p>For this method, the second parameter, <code>thisObject</code>, must be <code>null</code> if the first parameter, <code>callback</code>, is a method closure. Suppose you create a function in a movie clip called <code>me</code>:</p>
		 * <pre class="flash">
		 *      function myFunction(obj:Object):void {
		 *         //your code here
		 *      }
		 *      </pre>
		 * <p>Suppose you then use the <code>map()</code> method on an array called <code>myArray</code>:</p>
		 * <pre class="flash">
		 *      myArray.map(myFunction, me);
		 *      </pre>
		 * <p>Because <code>myFunction</code> is a member of the Timeline class, which cannot be overridden by <code>me</code>, the Flash runtime will throw an exception. You can avoid this runtime error by assigning the function to a variable, as follows:</p>
		 * <pre class="flash">
		 *      var myFunction:Function = function(obj:Object):void {
		 *          //your code here
		 *          };
		 *      myArray.map(myFunction, me);
		 *      </pre>
		 * 
		 * @param callback  — The function to run on each item in the array. This function can contain a simple command (such as changing the case of an array of strings) or a more complex operation, and is invoked with three arguments; the value of an item, the index of an item, and the Array object: <pre>    function callback(item:*, index:int, array:Array):String;</pre> 
		 * @param thisObject  — An object to use as <code>this</code> for the function. 
		 * @return  — A new array that contains the results of the function on each item in the original array. 
		 */
		public function map(callback:Function, thisObject:* = null):Array {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Removes the last element from an array and returns the value of that element. </p>
		 * 
		 * @return  — The value of the last element (of any data type) in the specified array. 
		 */
		public function pop():* {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Adds one or more elements to the end of an array and returns the new length of the array. </p>
		 * 
		 * @param args  — One or more values to append to the array. 
		 * @return  — An integer representing the length of the new array. 
		 */
		public function push(...args):uint {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Remove a single element from an array. This method modifies the array without making a copy. </p>
		 * 
		 * @param index  — An integer that specifies the index of the element in the array that is to be deleted. You can use a negative integer to specify a position relative to the end of the array (for example, -1 is the last element of the array). 
		 * @return  — The element that was removed from the original array. 
		 */
		public function removeAt(index:int):* {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Reverses the array in place. </p>
		 * 
		 * @return  — The new array. 
		 */
		public function reverse():Array {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Removes the first element from an array and returns that element. The remaining array elements are moved from their original position, i, to i-1. </p>
		 * 
		 * @return  — The first element (of any data type) in an array. 
		 */
		public function shift():* {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a new array that consists of a range of elements from the original array, without modifying the original array. The returned array includes the <code>startIndex</code> element and all elements up to, but not including, the <code>endIndex</code> element. </p>
		 * <p>If you don't pass any parameters, the new array is a duplicate (shallow clone) of the original array.</p>
		 * 
		 * @param startIndex  — A number specifying the index of the starting point for the slice. If <code>startIndex</code> is a negative number, the starting point begins at the end of the array, where -1 is the last element. 
		 * @param endIndex  — A number specifying the index of the ending point for the slice. If you omit this parameter, the slice includes all elements from the starting point to the end of the array. If <code>endIndex</code> is a negative number, the ending point is specified from the end of the array, where -1 is the last element. 
		 * @return  — An array that consists of a range of elements from the original array. 
		 */
		public function slice(startIndex:int = 0, endIndex:int = 16777215):Array {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Executes a test function on each item in the array until an item is reached that returns <code>true</code>. Use this method to determine whether any items in an array meet a criterion, such as having a value less than a particular number. </p>
		 * <p>For this method, the second parameter, <code>thisObject</code>, must be <code>null</code> if the first parameter, <code>callback</code>, is a method closure. Suppose you create a function in a movie clip called <code>me</code>:</p>
		 * <pre class="flash">
		 *      function myFunction(obj:Object):void {
		 *         //your code here
		 *      }
		 *      </pre>
		 * <p>Suppose you then use the <code>some()</code> method on an array called <code>myArray</code>:</p>
		 * <pre class="flash">
		 *      myArray.some(myFunction, me);
		 *      </pre>
		 * <p>Because <code>myFunction</code> is a member of the Timeline class, which cannot be overridden by <code>me</code>, the Flash runtime will throw an exception. You can avoid this runtime error by assigning the function to a variable, as follows:</p>
		 * <pre class="flash">
		 *      var myFunction:Function = function(obj:Object):void {
		 *          //your code here
		 *          };
		 *      myArray.some(myFunction, me);
		 *      </pre>
		 * 
		 * @param callback  — The function to run on each item in the array. This function can contain a simple comparison (for example <code>item &lt; 20</code>) or a more complex operation, and is invoked with three arguments; the value of an item, the index of an item, and the Array object: <pre>    function callback(item:*, index:int, array:Array):Boolean;</pre> 
		 * @param thisObject  — An object to use as <code>this</code> for the function. 
		 * @return  — A Boolean value of  if any items in the array return  for the specified function; otherwise . 
		 */
		public function some(callback:Function, thisObject:* = null):Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sorts the elements in an array. This method sorts according to Unicode values. (ASCII is a subset of Unicode.) </p>
		 * <p>By default, <code>Array</code>.<code>sort()</code> works in the following way:</p>
		 * <ul>
		 *  <li>Sorting is case-sensitive (<i>Z</i> precedes <i>a</i>).</li>
		 *  <li>Sorting is ascending (<i>a</i> precedes <i>b</i>). </li>
		 *  <li>The array is modified to reflect the sort order; multiple elements that have identical sort fields are placed consecutively in the sorted array in no particular order.</li>
		 *  <li>All elements, regardless of data type, are sorted as if they were strings, so 100 precedes 99, because "1" is a lower string value than "9".</li>
		 * </ul>
		 * <p> To sort an array by using settings that deviate from the default settings, you can either use one of the sorting options described in the <code>sortOptions</code> portion of the <code>...args</code> parameter description, or you can create your own custom function to do the sorting. If you create a custom function, you call the <code>sort()</code> method, and use the name of your custom function as the first argument (<code>compareFunction</code>) </p>
		 * 
		 * @param args  — The arguments specifying a comparison function and one or more values that determine the behavior of the sort. <p>This method uses the syntax and argument order <code>Array.sort(compareFunction, sortOptions)</code> with the arguments defined as follows:</p> <ul>
		 *  <li><code>compareFunction</code> - A comparison function used to determine the sorting order of elements in an array. This argument is optional. A comparison function should take two arguments to compare. Given the elements A and B, the result of <code>compareFunction</code> can have a negative, 0, or positive value: 
		 *   <ul>
		 *    <li>A negative return value specifies that A appears before B in the sorted sequence.</li>
		 *    <li>A return value of 0 specifies that A and B have the same sort order.</li>
		 *    <li>A positive return value specifies that A appears after B in the sorted sequence.</li>
		 *   </ul> </li>
		 *  <li><code>sortOptions</code> - One or more numbers or defined constants, separated by the <code>|</code> (bitwise OR) operator, that change the behavior of the sort from the default. This argument is optional. The following values are acceptable for <code>sortOptions</code>: 
		 *   <ul>
		 *    <li>1 or <code>Array.CASEINSENSITIVE</code></li>
		 *    <li>2 or <code>Array.DESCENDING</code></li>
		 *    <li>4 or <code>Array.UNIQUESORT</code></li>
		 *    <li>8 or <code>Array.RETURNINDEXEDARRAY</code> </li>
		 *    <li>16 or <code>Array.NUMERIC</code></li>
		 *   </ul> For more information, see the <code>Array.sortOn()</code> method.</li>
		 * </ul> 
		 * @return  — The return value depends on whether you pass any arguments, as described in the following list: <ul>
		 *  <li>If you specify a value of 4 or <code>Array.UNIQUESORT</code> for the <code>sortOptions</code> argument of the <code>...args</code> parameter and two or more elements being sorted have identical sort fields, Flash returns a value of 0 and does not modify the array. </li>
		 *  <li>If you specify a value of 8 or <code>Array.RETURNINDEXEDARRAY</code> for the <code>sortOptions</code> argument of the <code>...args</code> parameter, Flash returns a sorted numeric array of the indices that reflects the results of the sort and does not modify the array. </li>
		 *  <li>Otherwise, Flash returns nothing and modifies the array to reflect the sort order.</li>
		 * </ul> 
		 */
		public function sort(...args):Array {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sorts the elements in an array according to one or more fields in the array. The array should have the following characteristics: </p>
		 * <ul>
		 *  <li>The array is an indexed array, not an associative array.</li>
		 *  <li>Each element of the array holds an object with one or more properties.</li>
		 *  <li>All of the objects have at least one property in common, the values of which can be used to sort the array. Such a property is called a <i>field</i>.</li>
		 * </ul>
		 * <p>If you pass multiple <code>fieldName</code> parameters, the first field represents the primary sort field, the second represents the next sort field, and so on. Flash sorts according to Unicode values. (ASCII is a subset of Unicode.) If either of the elements being compared does not contain the field that is specified in the <code>fieldName</code> parameter, the field is assumed to be set to <code>undefined</code>, and the elements are placed consecutively in the sorted array in no particular order.</p>
		 * <p>By default, <code>Array</code>.<code>sortOn()</code> works in the following way:</p>
		 * <ul>
		 *  <li>Sorting is case-sensitive (<i>Z</i> precedes <i>a</i>).</li>
		 *  <li>Sorting is ascending (<i>a</i> precedes <i>b</i>). </li>
		 *  <li>The array is modified to reflect the sort order; multiple elements that have identical sort fields are placed consecutively in the sorted array in no particular order.</li>
		 *  <li>Numeric fields are sorted as if they were strings, so 100 precedes 99, because "1" is a lower string value than "9".</li>
		 * </ul>
		 * <p>Flash Player 7 added the <code>options</code> parameter, which you can use to override the default sort behavior. To sort a simple array (for example, an array with only one field), or to specify a sort order that the <code>options</code> parameter doesn't support, use <code>Array.sort()</code>.</p>
		 * <p>To pass multiple flags, separate them with the bitwise OR (<code>|</code>) operator:</p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *      my_array.sortOn(someFieldName, Array.DESCENDING | Array.NUMERIC);
		 *      </pre>
		 * </div>
		 * <p>Flash Player 8 added the ability to specify a different sorting option for each field when you sort by more than one field. In Flash Player 8 and later, the <code>options</code> parameter accepts an array of sort options such that each sort option corresponds to a sort field in the <code>fieldName</code> parameter. The following example sorts the primary sort field, <code>a</code>, using a descending sort; the secondary sort field, <code>b</code>, using a numeric sort; and the tertiary sort field, <code>c</code>, using a case-insensitive sort:</p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *      Array.sortOn (["a", "b", "c"], [Array.DESCENDING, Array.NUMERIC, Array.CASEINSENSITIVE]);
		 *      </pre>
		 * </div>
		 * <p><b>Note: </b>The <code>fieldName</code> and <code>options</code> arrays must have the same number of elements; otherwise, the <code>options</code> array is ignored. Also, the <code>Array.UNIQUESORT</code> and <code>Array.RETURNINDEXEDARRAY</code> options can be used only as the first element in the array; otherwise, they are ignored.</p>
		 * 
		 * @param fieldName  — A string that identifies a field to be used as the sort value, or an array in which the first element represents the primary sort field, the second represents the secondary sort field, and so on. 
		 * @param options  — One or more numbers or names of defined constants, separated by the <code>bitwise OR (|)</code> operator, that change the sorting behavior. The following values are acceptable for the <code>options</code> parameter: <ul>
		 *  <li><code>Array.CASEINSENSITIVE</code> or 1</li>
		 *  <li><code>Array.DESCENDING</code> or 2</li>
		 *  <li><code>Array.UNIQUESORT</code> or 4</li>
		 *  <li><code>Array.RETURNINDEXEDARRAY</code> or 8</li>
		 *  <li><code>Array.NUMERIC</code> or 16</li>
		 * </ul> <p>Code hinting is enabled if you use the string form of the flag (for example, <code>DESCENDING</code>) rather than the numeric form (2).</p> 
		 * @return  — The return value depends on whether you pass any parameters: <ul>
		 *  <li>If you specify a value of 4 or <code>Array.UNIQUESORT</code> for the <code>options</code> parameter, and two or more elements being sorted have identical sort fields, a value of 0 is returned and the array is not modified. </li>
		 *  <li>If you specify a value of 8 or <code>Array.RETURNINDEXEDARRAY</code> for the <code>options</code> parameter, an array is returned that reflects the results of the sort and the array is not modified.</li>
		 *  <li>Otherwise, nothing is returned and the array is modified to reflect the sort order.</li>
		 * </ul> 
		 */
		public function sortOn(fieldName:Object, options:Object = null):Array {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Adds elements to and removes elements from an array. This method modifies the array without making a copy. </p>
		 * <p><b>Note:</b> To override this method in a subclass of Array, use <code>...args</code> for the parameters, as this example shows:</p>
		 * <pre>
		 *      public override function splice(...args) {
		 *        // your statements here
		 *      }
		 *      </pre>
		 * 
		 * @param startIndex  — An integer that specifies the index of the element in the array where the insertion or deletion begins. You can use a negative integer to specify a position relative to the end of the array (for example, -1 is the last element of the array). 
		 * @param deleteCount  — An integer that specifies the number of elements to be deleted. This number includes the element specified in the <code>startIndex</code> parameter. If you do not specify a value for the <code>deleteCount</code> parameter, the method deletes all of the values from the <code>startIndex</code> element to the last element in the array. If the value is 0, no elements are deleted. 
		 * @param values  — An optional list of one or more comma-separated values to insert into the array at the position specified in the <code>startIndex</code> parameter. If an inserted value is of type Array, the array is kept intact and inserted as a single element. For example, if you splice an existing array of length three with another array of length three, the resulting array will have only four elements. One of the elements, however, will be an array of length three. 
		 * @return  — An array containing the elements that were removed from the original array. 
		 */
		public function splice(startIndex:int, deleteCount:uint, ...values):Array {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a string that represents the elements in the specified array. Every element in the array, starting with index 0 and ending with the highest index, is converted to a concatenated string and separated by commas. In the ActionScript 3.0 implementation, this method returns the same value as the <code>Array.toString()</code> method. </p>
		 * 
		 * @return  — A string of array elements. 
		 */
		/*public function toLocaleString():String {
			throw new Error("Not implemented");
		}*/

		/**
		 * <p> Returns a string that represents the elements in the specified array. Every element in the array, starting with index 0 and ending with the highest index, is converted to a concatenated string and separated by commas. To specify a custom separator, use the <code>Array.join()</code> method. </p>
		 * 
		 * @return  — A string of array elements. 
		 */
		/*override public function toString():String {
			throw new Error("Not implemented");
		}*/

		/**
		 * <p> Adds one or more elements to the beginning of an array and returns the new length of the array. The other elements in the array are moved from their original position, i, to i+1. </p>
		 * 
		 * @param args  — One or more numbers, elements, or variables to be inserted at the beginning of the array. 
		 * @return  — An integer representing the new length of the array. 
		 */
		public function unshift(...args):uint {
			throw new Error("Not implemented");
		}
	}
}
