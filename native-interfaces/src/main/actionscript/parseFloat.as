package {

	/**
	 * <p> Converts a string to a floating-point number. The function reads, or <i>parses</i>, and returns the numbers in a string until it reaches a character that is not a part of the initial number. If the string does not begin with a number that can be parsed, <code>parseFloat()</code> returns <code>NaN</code>. White space preceding valid integers is ignored, as are trailing nonnumeric characters. </p>
	 * 
	 * @param str  — The string to read and convert to a floating-point number. 
	 * @return  — A number or  (not a number). 
	 */
	public function parseFloat(str:String):Number {
		throw new Error("Not implemented");
	}
}
