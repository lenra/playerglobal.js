package {

	/**
	 * <p> Decodes an encoded URI into a string. Returns a string in which all characters previously encoded by the <code>encodeURI</code> function are restored to their unencoded representation. </p>
	 * <p>The following table shows the set of escape sequences that are <i>not</i> decoded to characters by the <code>decodeURI</code> function. Use <code>decodeURIComponent()</code> to decode the escape sequences in this table.</p>
	 * <table class="innertable">
	 *  <tbody>
	 *   <tr>
	 *    <th>Escape sequences not decoded</th>
	 *    <th>Character equivalents</th>
	 *   </tr>
	 *   <tr>
	 *    <td><code>%23</code></td>
	 *    <td><code>#</code></td>
	 *   </tr>
	 *   <tr>
	 *    <td><code>%24</code></td>
	 *    <td><code>$</code></td>
	 *   </tr>
	 *   <tr>
	 *    <td><code>%26</code></td>
	 *    <td><code>&amp;</code></td>
	 *   </tr>
	 *   <tr>
	 *    <td><code>%2B</code></td>
	 *    <td><code>+</code></td>
	 *   </tr>
	 *   <tr>
	 *    <td><code>%2C</code></td>
	 *    <td><code>,</code></td>
	 *   </tr>
	 *   <tr>
	 *    <td><code>%2F</code></td>
	 *    <td><code>/</code></td>
	 *   </tr>
	 *   <tr>
	 *    <td><code>%3A</code></td>
	 *    <td><code>:</code></td>
	 *   </tr>
	 *   <tr>
	 *    <td><code>%3B</code></td>
	 *    <td><code>;</code></td>
	 *   </tr>
	 *   <tr>
	 *    <td><code>%3D</code></td>
	 *    <td><code>=</code></td>
	 *   </tr>
	 *   <tr>
	 *    <td><code>%3F</code></td>
	 *    <td><code>?</code></td>
	 *   </tr>
	 *   <tr>
	 *    <td><code>%40</code></td>
	 *    <td><code>@</code></td>
	 *   </tr>
	 *  </tbody>
	 * </table>
	 * 
	 * @param uri  — A string encoded with the <code>encodeURI</code> function. 
	 * @return  — A string in which all characters previously escaped by the  function are restored to their unescaped representation. 
	 */
	public function decodeURI(uri:String):String {
		throw new Error("Not implemented");
	}
}
