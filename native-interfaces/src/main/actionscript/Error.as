package {
	/**
	 *  The Error class contains information about an error that occurred in a script. In developing ActionScript 3.0 applications, when you run your compiled code in the debugger version of a Flash runtime, a dialog box displays exceptions of type Error, or of a subclass, to help you troubleshoot the code. You create an Error object by using the <code>Error</code> constructor function. Typically, you throw a new Error object from within a <code>try</code> code block that is caught by a <code>catch</code> code block. <p>You can also create a subclass of the Error class and throw instances of that subclass.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7ecc.html" target="_blank">Working with the debugger versions of Flash runtimes</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7ec5.html" target="_blank">Using try..catch..finally statements</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7ed0.html" target="_blank">Creating custom error classes</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7ecf.html" target="_blank">Responding to error events and status</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7ecd.html" target="_blank">Handling errors example: CustomErrors application</a>
	 * </div><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7ebd.html" target="_blank">Basics of error handling</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7ed2.html" target="_blank">Error handling in ActionScript 3.0</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7ebb.html" target="_blank">ActionScript 3.0 error-handling elements</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7eba.html" target="_blank">Error-handling strategies</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7ece.html" target="_blank">Comparing the Error classes</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7eb3.html" target="_blank">Core Error classes</a>
	 *  <br>
	 * </div><br><hr>
	 */
	public class Error {
		private var _message:String;
		private var _name:String;

		private var _errorID:int;

		/**
		 * <p> Creates a new Error object. If <code>message</code> is specified, its value is assigned to the object's <code>Error.message</code> property. </p>
		 * 
		 * @param message  — A string associated with the Error object; this parameter is optional. 
		 * @param id  — A reference number to associate with the specific error message. 
		 */
		public function Error(message:String = "", id:int = 0) {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Contains the reference number associated with the specific error message. For a custom Error object, this number is the value from the <code>id</code> parameter supplied in the constructor. </p>
		 * 
		 * @return 
		 */
		public function get errorID():int {
			return _errorID;
		}

		/**
		 * <p> Contains the message associated with the Error object. By default, the value of this property is "<code>Error</code>". You can specify a <code>message</code> property when you create an Error object by passing the error string to the <code>Error</code> constructor function. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="statements.html#throw" target="">statements.html#throw</a>
		 *  <br>
		 *  <a href="statements.html#try..catch..finally" target="">statements.html#try..catch..finally</a>
		 * </div>
		 * 
		 * @return 
		 */
		public function get message():String {
			return _message;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set message(value:String):void {
			_message = value;
		}

		/**
		 * <p> Contains the name of the Error object. By default, the value of this property is "<code>Error</code>". </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="statements.html#throw" target="">statements.html#throw</a>
		 *  <br>
		 *  <a href="statements.html#try..catch..finally" target="">statements.html#try..catch..finally</a>
		 * </div>
		 * 
		 * @return 
		 */
		public function get name():String {
			return _name;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set name(value:String):void {
			_name = value;
		}

		/**
		 * <p> Returns the call stack for an error at the time of the error's construction as a string. As shown in the following example, the first line of the return value is the string representation of the exception object, followed by the stack trace elements: </p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *      TypeError: Error #1009: Cannot access a property or method of a null object reference
		 *          at com.xyz::OrderEntry/retrieveData()[/src/com/xyz/OrderEntry.as:995]
		 *          at com.xyz::OrderEntry/init()[/src/com/xyz/OrderEntry.as:200]
		 *          at com.xyz::OrderEntry()[/src/com/xyz/OrderEntry.as:148]
		 *         </pre>
		 * </div>
		 * <p>The preceding listing shows the value of this method when called in a debugger version of Flash Player or code running in the AIR Debug Launcher (ADL). When code runs in a release version of Flash Player or AIR, the stack trace is provided without the file path and line number information, as in the following example:</p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *      TypeError: Error #1009: Cannot access a property or method of a null object reference
		 *          at com.xyz::OrderEntry/retrieveData()
		 *          at com.xyz::OrderEntry/init()
		 *          at com.xyz::OrderEntry()
		 *         </pre>
		 * </div>
		 * <p>For Flash Player 11.4 and earlier and AIR 3.4 and earlier, stack traces are <b>only</b> available when code is running in the debugger version of Flash Player or the AIR Debug Launcher (ADL). In non-debugger versions of those runtimes, calling this method returns <code>null</code>.</p>
		 * 
		 * @return  — A string representation of the call stack. 
		 */
		public function getStackTrace():String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the string <code>"Error"</code> by default or the value contained in the <code>Error.message</code> property, if defined. </p>
		 * 
		 * @return  — The error message. 
		 */
		/*override public function toString():String {
			throw new Error("Not implemented");
		}*/
	}
}
