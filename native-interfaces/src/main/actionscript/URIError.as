package {
	/**
	 *  A URIError exception is thrown when one of the global URI handling functions is used in a way that is incompatible with its definition. This exception is thrown when an invalid URI is specified to a function that expects a valid URI, such as the <code>Socket.connect()</code> method. <p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7ecf.html" target="_blank">Responding to error events and status</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="flash/net/Socket.html#connect()" target="">flash.net.Socket.connect()</a>
	 * </div><br><hr>
	 */
	public class URIError extends Error {

		/**
		 * <p> Creates a new URIError object. </p>
		 * 
		 * @param message  — Contains the message associated with the URIError object. 
		 */
		public function URIError(message:String = "") {
			super(message);
			throw new Error("Not implemented");
		}
	}
}
