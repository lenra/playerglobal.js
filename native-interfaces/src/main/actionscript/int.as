package {
	/**
	 *  The int class lets you work with the data type representing a 32-bit signed integer. The range of values represented by the int class is -2,147,483,648 (-2^31) to 2,147,483,647 (2^31-1). <p>The constant properties of the int class, <code>MAX_VALUE</code> and <code>MIN_VALUE</code>, are static, which means that you don't need an object to use them, so you don't need to use the constructor. The methods, however, are not static, which means that you do need an object to use them. You can create an int object by using the int class constructor or by declaring a variable of type int and assigning the variable a literal value.</p> <p>The int data type is useful for loop counters and other situations where a floating point number is not needed, and is similar to the int data type in Java and C++. The default value of a variable typed as int is <code>0</code> </p> <p>If you are working with numbers that exceed <code>int.MAX_VALUE</code>, consider using Number. </p> <p>The following example calls the <code>toString()</code> method of the int class, which returns the string <code>1234</code>: </p> <div class="listing" version="3.0">
	 *  <div class="clipcopy">
	 *   <a href="#" class="copyText">Copy</a>
	 *  </div>
	 *  <pre>
	 *  var myint:int = 1234;
	 *  myint.toString();
	 *  </pre>
	 * </div> <p>The following example assigns the value of the <code>MIN_VALUE</code> property to a variable declared without the use of the constructor:</p> <pre>
	 *  var smallest:int = int.MIN_VALUE;
	 *  </pre> <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="uint.html" target="">uint</a>
	 *  <br>
	 *  <a href="Number.html" target="">Number</a>
	 * </div><br><hr>
	 */
	public class int {
		/**
		 * <p> The largest representable 32-bit signed integer, which is 2,147,483,647. </p>
		 */
		public static const MAX_VALUE:int = 2147483647;
		/**
		 * <p> The smallest representable 32-bit signed integer, which is -2,147,483,648. </p>
		 */
		public static const MIN_VALUE:int = -2147483648;

		/**
		 * <p> Constructor; creates a new int object. You must use the int constructor when using <code>int.toString()</code> and <code>int.valueOf()</code>. You do not use a constructor when using the properties of an int object. The <code>new int</code> constructor is primarily used as a placeholder. An int object is not the same as the <code>int()</code> function that converts a parameter to a primitive value. </p>
		 * 
		 * @param num  — The numeric value of the int object being created or a value to be converted to a&nbsp;number. The default value is 0 if <code>value</code> is not provided. 
		 */
		public function int(num:Object) {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a string representation of the number in exponential notation. The string contains one digit before the decimal point and up to 20 digits after the decimal point, as specified by the <code>fractionDigits</code> parameter. </p>
		 * 
		 * @param fractionDigits  — An integer between 0 and 20, inclusive, that represents the desired number of decimal places. 
		 * @return 
		 */
		public function toExponential(fractionDigits:uint):String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a string representation of the number in fixed-point notation. Fixed-point notation means that the string will contain a specific number of digits after the decimal point, as specified in the <code>fractionDigits</code> parameter. The valid range for the <code>fractionDigits</code> parameter is from 0 to 20. Specifying a value outside this range throws an exception. </p>
		 * 
		 * @param fractionDigits  — An integer between 0 and 20, inclusive, that represents the desired number of decimal places. 
		 * @return 
		 */
		public function toFixed(fractionDigits:uint):String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a string representation of the number either in exponential notation or in fixed-point notation. The string will contain the number of digits specified in the <code>precision</code> parameter. </p>
		 * 
		 * @param precision  — An integer between 1 and 21, inclusive, that represents the desired number of digits to represent in the resulting string. 
		 * @return 
		 */
		public function toPrecision(precision:uint):String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the string representation of an <code>int</code> object. </p>
		 * 
		 * @param radix  — Specifies the numeric base (from 2 to 36) to use for the number-to-string conversion. If you do not specify the <code>radix</code> parameter, the default value is 10. 
		 * @return  — A string. 
		 */
		/*override public function toString(radix:uint):String {
			throw new Error("Not implemented");
		}*/

		/**
		 * <p> Returns the primitive value of the specified int object. </p>
		 * 
		 * @return  — An int value. 
		 */
		public function valueOf():int {
			throw new Error("Not implemented");
		}
	}
}
