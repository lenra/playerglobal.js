package {

	/**
	 * <p> Encodes a string into a valid URI (Uniform Resource Identifier). Converts a complete URI into a string in which all characters are encoded as UTF-8 escape sequences unless a character belongs to a small group of basic characters. </p>
	 * <p>The following table shows the entire set of basic characters that are <i>not</i> converted to UTF-8 escape sequences by the <code>encodeURI</code> function.</p>
	 * <table class="innertable">
	 *  <tbody>
	 *   <tr>
	 *    <th>Characters not encoded</th>
	 *   </tr>
	 *   <tr>
	 *    <td><code>0 1 2 3 4 5 6 7 8 9</code></td>
	 *   </tr>
	 *   <tr>
	 *    <td><code>a b c d e f g h i j k l m n o p q r s t u v w x y z</code></td>
	 *   </tr>
	 *   <tr>
	 *    <td><code>A B C D E F G H I J K L M N O P Q R S T U V W X Y Z</code></td>
	 *   </tr>
	 *   <tr>
	 *    <td><code>; / ? : @ &amp; = + $ , #</code></td>
	 *   </tr>
	 *   <tr>
	 *    <td><code>- _ . ! ~ * ' ( )</code></td>
	 *   </tr>
	 *  </tbody>
	 * </table>
	 * 
	 * @param uri  — A string representing a complete URI. 
	 * @return  — A string with certain characters encoded as UTF-8 escape sequences. 
	 */
	public function encodeURI(uri:String):String {
		throw new Error("Not implemented");
	}
}
