package {
	/**
	 *  The JSON class lets applications import and export data using JavaScript Object Notation (JSON) format. JSON is an industry-standard data interchange format that is described at <a href="http://www.json.org">http://www.json.org</a>. <p>As a rule, the ActionScript JSON class adheres to the ECMA-262 specification. The following exceptions occur where ActionScript provides greater flexibility than ECMAScript:</p> <ul> 
	 *  <li> <p>Unlike ECMAScript, the ActionScript JSON class encodes the following primitives data types as well as the objects that wrap them:</p> 
	 *   <table class="innertable">
	 *    <tbody>
	 *     <tr>
	 *      <th>Primitive type</th>
	 *      <th>Wrapper class</th>
	 *     </tr>
	 *     <tr>
	 *      <td>string</td>
	 *      <td>String</td>
	 *     </tr>
	 *     <tr>
	 *      <td>number</td>
	 *      <td>Number</td>
	 *     </tr>
	 *     <tr>
	 *      <td>boolean</td>
	 *      <td>Boolean</td>
	 *     </tr>
	 *    </tbody>
	 *   </table> </li> 
	 *  <li> <p> <b> <code>parse()</code> method:</b> When the argument passed to the <code>reviver</code> parameter is not a function, ActionScript throws a TypeError with error ID <code>kJSONInvalidReviver</code>. It also posts a localized error message.</p> </li> 
	 *  <li> <p> <b> <code>stringify()</code> method:</b> When the argument passed to the <code>replacer</code> parameter is neither an array or a function, ActionScript throws a TypeError with error ID <code>kJSONInvalidReplacer</code>. It also posts a localized error message.</p> </li> 
	 *  <li> <p> <b> <code>stringify()</code> method:</b> When the argument passed to the <code>space</code> parameter is not a String or a Number, it is converted to a String. This string serves as the gap in the output. ECMA-262 requires the gap to be an empty string.</p> </li> 
	 *  <li> <p> <b> <code>stringify()</code> method:</b> When the code encounters a cyclic structure, it throws a TypeError with error ID <code>kJSONCyclicStructure</code>. It also posts a localized error message.</p> </li> 
	 *  <li> <p>ECMA-262 specifies that JSON stringification enumerates the "own properties" of an object, meaning the object's dynamic properties. Because ActionScript classes can also declare fixed properties, the ActionScript <code>stringify()</code> method enumerates both dynamic properties and public non-transient properties on ActionScript objects. These properties include properties accessed by getters as well as properties accessed directly.</p> </li> 
	 * </ul> <p>For most ActionScript classes, the ActionScript <code>JSON.stringify()</code> method generates a default encoding. ActionScript classes can change this encoding by defining a <code>toJSON()</code> method in the class or its prototype. For a few ActionScript classes, the default JSON encoding is inappropriate. These classes provide a trivial, overridable implementation of <code>toJSON()</code> that returns the value described in the following table. You can override or redefine the <code>toJSON()</code> methods on these classes if you require a specific behavior. The following table describes these exceptions:</p> <table class="innertable">
	 *  <tbody>
	 *   <tr>
	 *    <th>Class</th>
	 *    <th>Comments</th>
	 *   </tr>
	 *   <tr>
	 *    <td>ByteArray</td>
	 *    <td>Overridable <code>toJSON()</code> method returns the string "ByteArray".</td>
	 *   </tr>
	 *   <tr>
	 *    <td>Dictionary</td>
	 *    <td>Overridable <code>toJSON()</code> method returns the string "Dictionary".</td>
	 *   </tr>
	 *   <tr>
	 *    <td>Date</td>
	 *    <td>Overridable <code>toJSON()</code> method returns <code>Date.toString()</code>. This exception guarantees that the ActionScript Date constructor can parse the JSON string.</td>
	 *   </tr>
	 *   <tr>
	 *    <td>XML</td>
	 *    <td>Overridable <code>toJSON()</code> method returns the string "XML".</td>
	 *   </tr>
	 *  </tbody>
	 * </table> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WSf3d65dd2c930a82b-59702cdf13201c7fa84-7ffc.html" target="_blank">Defining custom JSON behavior</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WSf3d65dd2c930a82b11338b8013201e2db2b-8000.html" target="_blank">Defining toJSON() on the prototype of a built-in class</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WSab2db1cbc17f4fe03862fd0a132e04d0ed0-8000.html" target="_blank">Defining or overriding toJSON() at the class level</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WSf3d65dd2c930a82b33ca443913202ba5d0a-8000.html" target="_blank">Using the JSON.stringify() replacer parameter</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WSf3d65dd2c930a82b43322d12132a75d91da-8000.html" target="_blank">Using the JSON.parse() reviver parameter</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WSde2d341a0d78278f173272d5132ae87b002-8000.html" target="_blank">Parsing example</a>
	 * </div><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WSf3d65dd2c930a82b-59702cdf13201c7fa84-8000.html" target="_blank">Overview of the JSON API</a>
	 *  <br>
	 * </div><br><hr>
	 */
	public class JSON {


		/**
		 * <p> Accepts a JSON-formatted String and returns an ActionScript Object that represents that value. JSON objects, arrays, strings, numbers, Booleans, and null map to corresponding ActionScript values, as shown below: </p>
		 * <p> </p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>JSON type</th>
		 *    <th>ActionScript type</th>
		 *   </tr>
		 *   <tr>
		 *    <td>array</td>
		 *    <td>Array</td>
		 *   </tr>
		 *   <tr>
		 *    <td>string</td>
		 *    <td>String</td>
		 *   </tr>
		 *   <tr>
		 *    <td>number</td>
		 *    <td>Number</td>
		 *   </tr>
		 *   <tr>
		 *    <td>boolean</td>
		 *    <td>Boolean</td>
		 *   </tr>
		 *   <tr>
		 *    <td>null</td>
		 *    <td>null</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p>The <code>reviver</code> parameter is a function that takes two parameters: a key and a value. You can use this function to transform or filter each key/value pair as it is parsed. If you supply a <code>reviver</code> function, your transformed or filtered value for each pair, rather than the default parsing, is returned in the <code>parse()</code> function output. If the <code>reviver</code> function returns <code>undefined</code> for any pair, the property is deleted from the final result. </p>
		 * <p>When the argument passed to the <code>reviver</code> parameter is not a function, ActionScript throws a TypeError with error ID <code>kJSONInvalidReviver</code>. It also posts a localized error message.</p>
		 * <p>If the <code>parse()</code> function encounters duplicate keys within an object, the duplicate key encountered last provides the value for all preceding occurrences of that key. </p>
		 * 
		 * @param text  — The JSON string to be parsed 
		 * @param reviver  — (Optional) A function that transforms each key/value pair that is parsed 
		 * @return 
		 */
		public static function parse(text:String, reviver:Function = null):Object {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Returns a String, in JSON format, that represents an ActionScript value. The <code>stringify</code> method can take three parameters. </p>
		 * <p>The <code>value</code> parameter is required. This parameter is an ActionScript value. Typically, it is an Object or Array, but it can also be a String, Boolean, Number, or null. </p>
		 * <p>The optional <code>replacer</code> parameter can be either a function or an array of strings or numbers. If it is a function, the function takes two parameters: a key and a value. You can use this function to transform or filter each key/value pair as it is parsed. If you supply a <code>replacer</code> function, your transformed or filtered value for each pair, rather than the default parsing, is returned in the <code>parse()</code> function output. If the <code>replacer</code> function returns <code>undefined</code> for any pair, the property is deleted from the final result. If <code>replacer</code> is an array, it is used as a filter for designating which properties are included in the output. </p>
		 * <p>When the argument passed to the <code>replacer</code> parameter is neither an array or a function, ActionScript throws a TypeError with error ID <code>kJSONInvalidReplacer</code>. It also posts a localized error message.</p>
		 * <p>The optional <code>space</code> parameter is a String or Number that allows white space to be injected into the returned string to improve human readability. Entries in generated JSON objects and JSON arrays are separated by a gap derived from the <code>space</code> parameter. This gap is always 0 to 10 characters wide. If space is a string, then the derived gap is the first ten characters of that string. If space is a non-negative number <i>x</i>, then the gap is <i>x</i> space characters, to a maximum of ten spaces. When the argument passed to the <code>space</code> parameter is not a String a Number, it is converted to a String for use as the gap in the output. Execution then proceeds.</p>
		 * <p>When the <code>stringify()</code> method encounters a cyclic structure, it throws a TypeError with error ID <code>kJSONCyclicStructure</code>. It also posts a localized error message.</p>
		 * 
		 * @param value  — The ActionScript value to be converted into a JSON string 
		 * @param replacer  — (Optional) A function or an array that transforms or filters key/value pairs in the <code>stringify</code> output 
		 * @param space  — (Optional) A string or number that controls added white space in the returned String 
		 * @return 
		 */
		public static function stringify(value:Object, replacer:* = null, space:* = null):String {
			throw new Error("Not implemented");
		}
	}
}
