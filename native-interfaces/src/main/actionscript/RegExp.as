package {
	/**
	 *  The RegExp class lets you work with regular expressions, which are patterns that you can use to perform searches in strings and to replace text in strings. <p>You can create a new RegExp object by using the <code>new RegExp()</code> constructor or by assigning a RegExp literal to a variable:</p> <div class="listing">
	 *  <div class="clipcopy">
	 *   <a href="#" class="copyText">Copy</a>
	 *  </div>
	 *  <pre> var pattern1:RegExp = new RegExp("test-\\d", "i");
	 *      var pattern2:RegExp = /test-\d/i;
	 *      </pre>
	 * </div> <p>For more information, see "Using Regular Expressions" in the <i>ActionScript 3.0 Developer's Guide</i>.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7ea9.html" target="_blank">Regular expression syntax</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="String.html#match()" target="">String.match()</a>
	 *  <br>
	 *  <a href="String.html#replace()" target="">String.replace()</a>
	 *  <br>
	 *  <a href="String.html#search()" target="">String.search()</a>
	 * </div><br><hr>
	 */
	public class RegExp {
		private var _lastIndex:Number;

		private var _dotall:Boolean;
		private var _extended:Boolean;
		private var _global:Boolean;
		private var _ignoreCase:Boolean;
		private var _multiline:Boolean;
		private var _source:String;

		/**
		 * <p> Lets you construct a regular expression from two strings. One string defines the pattern of the regular expression, and the other defines the flags used in the regular expression. </p>
		 * 
		 * @param re  — The pattern of the regular expression (also known as the <i>constructor string</i>). This is the main part of the regular expression (the part that goes within the "/" characters). <p><b>Notes:</b> </p> <ul>
		 *  <li>Do not include the starting and trailing "/" characters; use these only when defining a regular expression literal without using the constructor. For example, the following two regular expressions are equivalent: 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre> var re1:RegExp = new RegExp("bob", "i"); 
		 *         var re2:RegExp = /bob/i;</pre>
		 *   </div> </li>
		 *  <li>In a regular expression that is defined with the <code>RegExp()</code> constructor method, to use a metasequence that begins with the backslash (\) character, such as <code>\d</code> (which matches any digit), type the backslash character twice. For example, the following two regular expressions are equivalent: 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre> var re1:RegExp = new RegExp("\\d+", ""); 
		 *         var re2:RegExp = /\d/;</pre>
		 *   </div> <p>In the first expression, you must type the backlash character twice in this case, because the first parameter of the <code>RegExp() </code> constructor method is a string, and in a string literal you must type a backslash character twice to have it recognized as a single backslash character.</p> </li>
		 * </ul> 
		 * @param flags  — The modifiers of the regular expression. These can include the following: <ul>
		 *  <li> <code>g</code> — When using the <code>replace()</code> method of the String class, specify this modifier to replace all matches, rather than only the first one. This modifier corresponds to the <code>global</code> property of the RegExp instance.</li>
		 *  <li> <code>i</code> — The regular expression is evaluated <i>without</i> case sensitivity. This modifier corresponds to the <code>ignoreCase</code> property of the RegExp instance.</li>
		 *  <li> <code>s</code> — The dot (<code>.</code>) character matches new-line characters. Note This modifier corresponds to the <code>dotall</code> property of the RegExp instance.</li>
		 *  <li> <code>m</code> — The caret (<code>^</code>) character and dollar sign (<code>$</code>) match before <i>and</i> after new-line characters. This modifier corresponds to the <code>multiline</code> property of the RegExp instance.</li>
		 *  <li> <code>x</code> — White space characters in the <code>re</code> string are ignored, so that you can write more readable constructors. This modifier corresponds to the <code>extended</code> property of the RegExp instance.</li>
		 * </ul> <p>All other characters in the <code>flags</code> string are ignored. </p> 
		 */
		public function RegExp(re:String, flags:String) {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Specifies whether the dot character (.) in a regular expression pattern matches new-line characters. Use the <code>s</code> flag when constructing a regular expression to set <code>dotall = true</code>. </p>
		 * 
		 * @return 
		 */
		public function get dotall():Boolean {
			return _dotall;
		}

		/**
		 * <p> Specifies whether to use extended mode for the regular expression. When a RegExp object is in extended mode, white space characters in the constructor string are ignored. This is done to allow more readable constructors. </p>
		 * <p>Use the <code>x</code> flag when constructing a regular expression to set <code>extended = true</code>. </p>
		 * 
		 * @return 
		 */
		public function get extended():Boolean {
			return _extended;
		}

		/**
		 * <p> Specifies whether to use global matching for the regular expression. When <code>global == true</code>, the <code>lastIndex</code> property is set after a match is found. The next time a match is requested, the regular expression engine starts from the <code>lastIndex</code> position in the string. Use the <code>g</code> flag when constructing a regular expression to set <code>global</code> to <code>true</code>. </p>
		 * 
		 * @return 
		 */
		public function get global():Boolean {
			return _global;
		}

		/**
		 * <p> Specifies whether the regular expression ignores case sensitivity. Use the <code>i</code> flag when constructing a regular expression to set <code>ignoreCase = true</code>. </p>
		 * 
		 * @return 
		 */
		public function get ignoreCase():Boolean {
			return _ignoreCase;
		}

		/**
		 * <p> Specifies the index position in the string at which to start the next search. This property affects the <code>exec()</code> and <code>test()</code> methods of the RegExp class. However, the <code>match()</code>, <code>replace()</code>, and <code>search()</code> methods of the String class ignore the <code>lastIndex</code> property and start all searches from the beginning of the string. </p>
		 * <p>When the <code>exec()</code> or <code>test()</code> method finds a match and the <code>g</code> (<code>global</code>) flag is set to <code>true</code> for the regular expression, the method automatically sets the <code>lastIndex</code> property to the index position of the character <i>after</i> the last character in the matching substring of the last match. If the <code>g</code> (<code>global</code>) flag is set to <code>false</code>, the method does not set the <code>lastIndex</code>property.</p>
		 * <p>You can set the <code>lastIndex</code> property to adjust the starting position in the string for regular expression matching. </p>
		 * 
		 * @return 
		 */
		public function get lastIndex():Number {
			return _lastIndex;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set lastIndex(value:Number):void {
			_lastIndex = value;
		}

		/**
		 * <p> Specifies whether the <code>m</code> (<code>multiline</code>) flag is set. If it is set, the caret (<code>^</code>) and dollar sign (<code>$</code>) in a regular expression match before and after new lines. Use the <code>m</code> flag when constructing a regular expression to set <code>multiline = true</code>. </p>
		 * 
		 * @return 
		 */
		public function get multiline():Boolean {
			return _multiline;
		}

		/**
		 * <p> Specifies the pattern portion of the regular expression. </p>
		 * 
		 * @return 
		 */
		public function get source():String {
			return _source;
		}

		/**
		 * <p> Performs a search for the regular expression on the given string <code>str</code>. </p>
		 * <p>If the <code>g</code> (<code>global</code>) flag is <i>not</i> set for the regular expression, then the search starts at the beginning of the string (at index position 0); the search ignores the <code>lastIndex</code> property of the regular expression.</p>
		 * <p>If the <code>g</code> (<code>global</code>) flag <i>is</i> set for the regular expression, then the search starts at the index position specified by the <code>lastIndex</code> property of the regular expression. If the search matches a substring, the <code>lastIndex</code> property changes to match the position of the end of the match. </p>
		 * 
		 * @param str  — The string to search. 
		 * @return  — If there is no match, ; otherwise, an object with the following properties: <ul>
		 *  <li>An array, in which element 0 contains the complete matching substring, and other elements of the array (1 through <i>n</i>) contain substrings that match parenthetical groups in the regular expression </li>
		 *  <li><code>index</code> — The character position of the matched substring within the string</li>
		 *  <li><code>input</code> — The string (<code>str</code>)</li>
		 * </ul> 
		 */
		public function exec(str:String):Object {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Tests for the match of the regular expression in the given string <code>str</code>. </p>
		 * <p>If the <code>g</code> (<code>global</code>) flag is <i>not</i> set for the regular expression, then the search starts at the beginning of the string (at index position 0); the search ignores the <code>lastIndex</code> property of the regular expression.</p>
		 * <p>If the <code>g</code> (<code>global</code>) flag <i>is</i> set for the regular expression, then the search starts at the index position specified by the <code>lastIndex</code> property of the regular expression. If the search matches a substring, the <code>lastIndex</code> property changes to match the position of the end of the match. </p>
		 * 
		 * @param str  — The string to test. 
		 * @return  — If there is a match, ; otherwise, . 
		 */
		public function test(str:String):Boolean {
			throw new Error("Not implemented");
		}
	}
}
