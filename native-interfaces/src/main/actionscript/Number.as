package {
	/**
	 *  A data type representing an IEEE-754 double-precision floating-point number. You can manipulate primitive numeric values by using the methods and properties associated with the Number class. This class is identical to the JavaScript Number class. <p>The properties of the Number class are static, which means you do not need an object to use them, so you do not need to use the constructor.</p> <p>The Number data type adheres to the double-precision IEEE-754 standard. </p> <p>The Number data type is useful when you need to use floating-point values. Flash runtimes handle int and uint data types more efficiently than Number, but Number is useful in situations where the range of values required exceeds the valid range of the int and uint data types. The Number class can be used to represent integer values well beyond the valid range of the int and uint data types. The Number data type can use up to 53 bits to represent integer values, compared to the 32 bits available to int and uint. The default value of a variable typed as Number is <code>NaN</code> (Not a Number).</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="int.html" target="">int</a>
	 *  <br>
	 *  <a href="uint.html" target="">uint</a>
	 * </div><br><hr>
	 */
	public class Number {
		/**
		 * <p> The largest representable number (double-precision IEEE-754). This number is approximately 1.79e+308. </p>
		 */
		public static const MAX_VALUE:Number = 0;
		/**
		 * <p> The smallest (closest to zero, not most negative) representable IEEE-754 number that does not compare equal to zero. On most platforms this number is approximately 5e-324, but it can be larger on systems that do not support denormalized numbers. For example, on iOS this constant's value is 2.225e-308. </p>
		 * <p><b>Note:</b> The <i>absolute</i> smallest representable number overall is <code>-Number.MAX_VALUE</code>.</p>
		 */
		public static const MIN_VALUE:Number = 0;
		/**
		 * <p> Specifies the IEEE-754 value representing negative infinity. The value of this property is the same as that of the constant <code>-Infinity</code>. </p>
		 * <p> Negative infinity is a special numeric value that is returned when a mathematical operation or function returns a negative value larger than can be represented. </p>
		 */
		public static const NEGATIVE_INFINITY:Number = 0;
		/**
		 * <p> The IEEE-754 value representing Not a Number (<code>NaN</code>). </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="package.html#isNaN()" target="">isNaN() global function</a>
		 * </div>
		 */
		public static const NaN:Number = 0;
		/**
		 * <p> Specifies the IEEE-754 value representing positive infinity. The value of this property is the same as that of the constant <code>Infinity</code>. </p>
		 * <p> Positive infinity is a special numeric value that is returned when a mathematical operation or function returns a value larger than can be represented. </p>
		 */
		public static const POSITIVE_INFINITY:Number = 0;

		/**
		 * <p> Creates a Number object with the specified value. This constructor has the same effect as the <code>Number()</code> public native function that converts an object of a different type to a primitive numeric value. </p>
		 * 
		 * @param num  — The numeric value of the Number instance being created or a value to be converted to a Number. The default value is 0 if <code>num</code> is not specified. Using the constructor without specifying a <code>num</code> parameter is not the same as declaring a variable of type Number with no value assigned (such as <code>var myNumber:Number</code>), which defaults to <code>NaN</code>. A number with no value assigned is undefined and the equivalent of <code>new Number(undefined)</code>. 
		 */
		public function Number() {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a string representation of the number in exponential notation. The string contains one digit before the decimal point and up to 20 digits after the decimal point, as specified by the <code>fractionDigits</code> parameter. </p>
		 * 
		 * @param fractionDigits  — An integer between 0 and 20, inclusive, that represents the desired number of decimal places. 
		 * @return 
		 */
		public function toExponential(fractionDigits:uint):String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a string representation of the number in fixed-point notation. Fixed-point notation means that the string will contain a specific number of digits after the decimal point, as specified in the <code>fractionDigits</code> parameter. The valid range for the <code>fractionDigits</code> parameter is from 0 to 20. Specifying a value outside this range throws an exception. </p>
		 * 
		 * @param fractionDigits  — An integer between 0 and 20, inclusive, that represents the desired number of decimal places. 
		 * @return 
		 */
		public function toFixed(fractionDigits:uint):String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a string representation of the number either in exponential notation or in fixed-point notation. The string will contain the number of digits specified in the <code>precision</code> parameter. </p>
		 * 
		 * @param precision  — An integer between 1 and 21, inclusive, that represents the desired number of digits to represent in the resulting string. 
		 * @return 
		 */
		public function toPrecision(precision:uint):String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the string representation of the specified Number object (<code><i>myNumber</i></code>). If the value of the Number object is a decimal number without a leading zero (such as <code>.4</code>), <code>Number.toString()</code> adds a leading zero (<code>0.4</code>). </p>
		 * 
		 * @param radix  — Specifies the numeric base (from 2 to 36) to use for the number-to-string conversion. If you do not specify the <code>radix</code> parameter, the default value is 10. 
		 * @return  — The numeric representation of the Number object as a string. 
		 */
		public function toString(radix:uint=10):String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the primitive value type of the specified Number object. </p>
		 * 
		 * @return  — The primitive type value of the Number object. 
		 */
		public function valueOf():* {
			throw new Error("Not implemented");
		}
	}
}
