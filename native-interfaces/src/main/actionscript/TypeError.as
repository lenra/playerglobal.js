package {
	/**
	 *  A TypeError exception is thrown when the actual type of an operand is different from the expected type. <p> In addition, this exception is thrown when: </p><ul> 
	 *  <li>An actual parameter to a function or method could not be coerced to the formal parameter type.</li> 
	 *  <li>A value is assigned to a variable and cannot be coerced to the variable's type.</li> 
	 *  <li>The right side of the <code>is</code> or <code>instanceof</code> operator is not a valid type.</li> 
	 *  <li>The <code>super</code> keyword is used illegally.</li> 
	 *  <li>A property lookup results in more than one binding, and is therefore ambiguous.</li> 
	 *  <li>A method is invoked on an incompatible object. For example, a TypeError exception is thrown if a RegExp class method is "grafted" onto a generic object and then invoked.</li> 
	 * </ul>  <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7ecf.html" target="_blank">Responding to error events and status</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="operators.html#is" target="">is operator</a>
	 *  <br>
	 *  <a href="operators.html#instanceof" target="">instanceof operator</a>
	 *  <br>
	 *  <a href="statements.html#super" target="">super statement</a>
	 *  <br>
	 *  <a href="RegExp.html" target="">RegExp class</a>
	 * </div><br><hr>
	 */
	public class TypeError extends Error {

		/**
		 * <p> Creates a new TypeError object. </p>
		 * 
		 * @param message  — Contains the message associated with the TypeError object. 
		 */
		public function TypeError(message:String = "") {
			super(message);
			throw new Error("Not implemented");
		}
	}
}
