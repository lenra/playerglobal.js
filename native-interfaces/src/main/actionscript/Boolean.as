package {
	/**
	 *  A Boolean object is a data type that can have one of two values, either <code>true</code> or <code>false</code>, used for logical operations. Use the Boolean class to retrieve the primitive data type or string representation of a Boolean object. <p>To create a Boolean object, you can use the constructor or the global function, or assign a literal value. It doesn't matter which technique you use; in ActionScript 3.0, all three techniques are equivalent. (This is different from JavaScript, where a Boolean object is distinct from the Boolean primitive type.)</p> <p>The following lines of code are equivalent:</p> <div class="listing" version="3.0">
	 *  <div class="clipcopy">
	 *   <a href="#" class="copyText">Copy</a>
	 *  </div>
	 *  <pre>
	 * var flag:Boolean = true;
	 * var flag:Boolean = new Boolean(true);
	 * var flag:Boolean = Boolean(true);
	 * </pre>
	 * </div> <p><a href="#includeExamplesSummary">View the examples</a></p><br><hr>
	 */
	public class Boolean {

		/**
		 * <p> Creates a Boolean object with the specified value. If you omit the <code>expression</code> parameter, the Boolean object is initialized with a value of <code>false</code>. If you specify a value for the <code>expression</code> parameter, the method evaluates it and returns the result as a Boolean value according to the rules in the global <code>Boolean()</code> function. </p>
		 * 
		 * @param expression  — Any expression. 
		 */
		public function Boolean(expression:Object = false) {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the string representation (<code>"true"</code> or <code>"false"</code>) of the Boolean object. The output is not localized, and is <code>"true"</code> or <code>"false"</code> regardless of the system language. </p>
		 * 
		 * @return  — The string  or . 
		 */
		/*override public function toString():String {
			throw new Error("Not implemented");
		}*/

		/**
		 * <p> Returns <code>true</code> if the value of the specified Boolean object is true; <code>false</code> otherwise. </p>
		 * 
		 * @return  — A Boolean value. 
		 */
		public function valueOf():Boolean {
			throw new Error("Not implemented");
		}
	}
}
