package {
	/**
	 *  The EvalError class represents an error that occurs when user code calls the <code>eval()</code> function or attempts to use the <code>new</code> operator with the Function object. Calling <code>eval()</code> and calling <code>new</code> with the Function object are not supported. <p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7ed2.html" target="_blank">Error handling in ActionScript 3.0</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7ecf.html" target="_blank">Responding to error events and status</a>
	 * </div><br><hr>
	 */
	public class EvalError extends Error {

		/**
		 * <p> Creates a new EvalError object. </p>
		 * 
		 * @param message  — A string associated with the error. 
		 */
		public function EvalError(message:String = "") {
			super(message);
			throw new Error("Not implemented");
		}
	}
}
