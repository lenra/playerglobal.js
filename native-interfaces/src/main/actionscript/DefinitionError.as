package {
	/**
	 *  The DefinitionError class represents an error that occurs when user code attempts to define an identifier that is already defined. This error commonly occurs in redefining classes, interfaces, and functions. <br><hr>
	 */
	public class DefinitionError extends Error {

		/**
		 * <p> Creates a new DefinitionError object. </p>
		 * 
		 * @param message
		 */
		public function DefinitionError(message:String = "") {
			super(message);
			throw new Error("Not implemented");
		}
	}
}
