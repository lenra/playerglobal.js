package {

	/**
	 * <p> Evaluates the parameter <code>str</code> as a string, decodes the string from URL-encoded format (converting all hexadecimal sequences to ASCII characters), and returns the string. </p>
	 * 
	 * @param str  — A string with hexadecimal sequences to escape. 
	 * @return  — A string decoded from a URL-encoded parameter. 
	 */
	public function unescape(str:String):String {
		throw new Error("Not implemented");
	}
}
