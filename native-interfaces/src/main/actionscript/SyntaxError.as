package {
	/**
	 *  A SyntaxError exception is thrown when a parsing error occurs, for one of the following reasons:. <ul> 
	 *  <li>An invalid regular expression is parsed by the RegExp class.</li> 
	 *  <li>Invalid XML content is parsed by the XML class.</li> 
	 * </ul> <p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7ecf.html" target="_blank">Responding to error events and status</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="RegExp.html" target="">RegExp class</a>
	 *  <br>
	 *  <a href="XML.html" target="">XML class</a>
	 * </div><br><hr>
	 */
	public class SyntaxError extends Error {

		/**
		 * <p> Creates a new SyntaxError object. </p>
		 * 
		 * @param message  — Contains the message associated with the SyntaxError object. 
		 */
		public function SyntaxError(message:String = "") {
			super(message);
			throw new Error("Not implemented");
		}
	}
}
