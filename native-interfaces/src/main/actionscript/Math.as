package {
	/**
	 *  The Math class contains methods and constants that represent common mathematical functions and values. <p>Use the methods and properties of this class to access and manipulate mathematical constants and functions. All the properties and methods of the Math class are static and must be called using the syntax <code>Math.method(</code> <code> <i>parameter</i> </code> <code>)</code> or <code>Math.constant</code>. In ActionScript, constants are defined with the maximum precision of double-precision IEEE-754 floating-point numbers.</p> <p>Several Math class methods use the measure of an angle in radians as a parameter. You can use the following equation to calculate radian values before calling the method and then provide the calculated value as the parameter, or you can provide the entire right side of the equation (with the angle's measure in degrees in place of <code>degrees</code>) as the radian parameter.</p> <p>To calculate a radian value, use the following formula:</p> <pre>
	 *  radians = degrees * Math.PI/180
	 *  </pre> <p>To calculate degrees from radians, use the following formula:</p> <pre>
	 *  degrees = radians * 180/Math.PI
	 *  </pre> <p>The following is an example of passing the equation as a parameter to calculate the sine of a 45° angle:</p> <p> <code>Math.sin(45 * Math.PI/180)</code> is the same as <code>Math.sin(.7854)</code> </p> <p> <b>Note:</b> The Math functions acos, asin, atan, atan2, cos, exp, log, pow, sin, and sqrt may result in slightly different values depending on the algorithms used by the CPU or operating system. Flash runtimes call on the CPU (or operating system if the CPU doesn't support floating point calculations) when performing the calculations for the listed functions, and results have shown slight variations depending upon the CPU or operating system in use. </p> <p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7dd6.html" target="_blank">Using the Math class with drawing methods</a>
	 * </div><br><hr>
	 */
	public class Math {
		/**
		 * <p> A mathematical constant for the base of natural logarithms, expressed as <i>e</i>. The approximate value of <i>e</i><code> </code>is 2.71828182845905. </p>
		 */
		public static const E:Number = 2.71828182845905;
		/**
		 * <p> A mathematical constant for the natural logarithm of 10, expressed as log<sub>e</sub>10, with an approximate value of 2.302585092994046. </p>
		 */
		public static const LN10:Number = 2.302585092994046;
		/**
		 * <p> A mathematical constant for the natural logarithm of 2, expressed as log<sub>e</sub>2, with an approximate value of 0.6931471805599453. </p>
		 */
		public static const LN2:Number = 0.6931471805599453;
		/**
		 * <p> A mathematical constant for the base-10 logarithm of the constant <i>e</i> (<code>Math.E</code>), expressed as log<sub>10</sub>e, with an approximate value of 0.4342944819032518. </p>
		 * <p>The <code>Math.log()</code> method computes the natural logarithm of a number. Multiply the result of <code>Math.log()</code> by <code>Math.LOG10E</code> to obtain the base-10 logarithm.</p>
		 */
		public static const LOG10E:Number = 0.4342944819032518;
		/**
		 * <p> A mathematical constant for the base-2 logarithm of the constant <i>e</i>, expressed as log2e, with an approximate value of 1.442695040888963387. </p>
		 * <p>The <code>Math.log</code> method computes the natural logarithm of a number. Multiply the result of <code>Math.log()</code> by <code>Math.LOG2E</code> to obtain the base-2 logarithm.</p>
		 */
		public static const LOG2E:Number = 1.442695040888963387;
		/**
		 * <p> A mathematical constant for the ratio of the circumference of a circle to its diameter, expressed as pi, with a value of 3.141592653589793. </p>
		 */
		public static const PI:Number = 3.141592653589793;
		/**
		 * <p> A mathematical constant for the square root of one-half, with an approximate value of 0.7071067811865476. </p>
		 */
		public static const SQRT1_2:Number = 0.7071067811865476;
		/**
		 * <p> A mathematical constant for the square root of 2, with an approximate value of 1.4142135623730951. </p>
		 */
		public static const SQRT2:Number = 1.4142135623730951;


		/**
		 * <p> Computes and returns an absolute value for the number specified by the parameter <code>val</code>. </p>
		 * 
		 * @param val  — The number whose absolute value is returned. 
		 * @return  — The absolute value of the specified parameter. 
		 */
		public static function abs(val:Number):Number {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Computes and returns the arc cosine of the number specified in the parameter <code>val</code>, in radians. </p>
		 * 
		 * @param val  — A number from -1.0 to 1.0. 
		 * @return  — The arc cosine of the parameter . 
		 */
		public static function acos(val:Number):Number {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Computes and returns the arc sine for the number specified in the parameter <code>val</code>, in radians. </p>
		 * 
		 * @param val  — A number from -1.0 to 1.0. 
		 * @return  — A number between negative pi divided by 2 and positive pi divided by 2. 
		 */
		public static function asin(val:Number):Number {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Computes and returns the value, in radians, of the angle whose tangent is specified in the parameter <code>val</code>. The return value is between negative pi divided by 2 and positive pi divided by 2. </p>
		 * 
		 * @param val  — A number that represents the tangent of an angle. 
		 * @return  — A number between negative pi divided by 2 and positive pi divided by 2. 
		 */
		public static function atan(val:Number):Number {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Computes and returns the angle of the point <code>y</code>/<code>x</code> in radians, when measured counterclockwise from a circle's <i>x</i> axis (where 0,0 represents the center of the circle). The return value is between positive pi and negative pi. Note that the first parameter to atan2 is always the <i>y</i> coordinate. </p>
		 * 
		 * @param y  — The <i>y</i> coordinate of the point. 
		 * @param x  — The <i>x</i> coordinate of the point. 
		 * @return  — A number. 
		 */
		public static function atan2(y:Number, x:Number):Number {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Returns the ceiling of the specified number or expression. The ceiling of a number is the closest integer that is greater than or equal to the number. </p>
		 * 
		 * @param val  — A number or expression. 
		 * @return  — An integer that is both closest to, and greater than or equal to, the parameter . 
		 */
		public static function ceil(val:Number):Number {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Computes and returns the cosine of the specified angle in radians. To calculate a radian, see the overview of the Math class. </p>
		 * 
		 * @param angleRadians  — A number that represents an angle measured in radians. 
		 * @return  — A number from -1.0 to 1.0. 
		 */
		public static function cos(angleRadians:Number):Number {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Returns the value of the base of the natural logarithm (<i>e</i>), to the power of the exponent specified in the parameter <code>x</code>. The constant <code>Math.E</code> can provide the value of <i>e</i>. </p>
		 * 
		 * @param val  — The exponent; a number or expression. 
		 * @return  — <i>e</i> to the power of the parameter . 
		 */
		public static function exp(val:Number):Number {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Returns the floor of the number or expression specified in the parameter <code>val</code>. The floor is the closest integer that is less than or equal to the specified number or expression. </p>
		 * 
		 * @param val  — A number or expression. 
		 * @return  — The integer that is both closest to, and less than or equal to, the parameter . 
		 */
		public static function floor(val:Number):Number {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Returns the natural logarithm of the parameter <code>val</code>. </p>
		 * 
		 * @param val  — A number or expression with a value greater than 0. 
		 * @return  — The natural logarithm of parameter . 
		 */
		public static function log(val:Number):Number {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Evaluates <code>val1</code> and <code>val2</code> (or more values) and returns the largest value. </p>
		 * 
		 * @param val1  — A number or expression. 
		 * @param val2  — A number or expression. 
		 * @param rest  — A number or expression. <code>Math.max()</code> can accept multiple arguments. 
		 * @return  — The largest of the parameters  and  (or more values). 
		 */
		public static function max(val1:Number, val2:Number, ...rest):Number {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Evaluates <code>val1</code> and <code>val2</code> (or more values) and returns the smallest value. </p>
		 * 
		 * @param val1  — A number or expression. 
		 * @param val2  — A number or expression. 
		 * @param rest  — A number or expression. <code>Math.min()</code> can accept multiple arguments. 
		 * @return  — The smallest of the parameters  and  (or more values). 
		 */
		public static function min(val1:Number, val2:Number, ...rest):Number {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Computes and returns <code>base</code> to the power of <code>pow</code>. </p>
		 * 
		 * @param base  — A number to be raised by the power of the parameter <code>pow</code>. 
		 * @param pow  — A number specifying the power that the parameter <code>base</code> is raised by. 
		 * @return  — The value of  raised to the power of . 
		 */
		public static function pow(base:Number, pow:Number):Number {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Returns a pseudo-random number n, where 0 &lt;= n &lt; 1. The number returned is calculated in an undisclosed manner, and is "pseudo-random" because the calculation inevitably contains some element of non-randomness. </p>
		 * 
		 * @return  — A pseudo-random number. 
		 */
		public static function random():Number {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Rounds the value of the parameter <code>val</code> up or down to the nearest integer and returns the value. If <code>val</code> is equidistant from its two nearest integers (that is, if the number ends in .5), the value is rounded up to the next higher integer. </p>
		 * 
		 * @param val  — The number to round. 
		 * @return  — The parameter  rounded to the nearest whole number. 
		 */
		public static function round(val:Number):Number {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Computes and returns the sine of the specified angle in radians. To calculate a radian, see the overview of the Math class. </p>
		 * 
		 * @param angleRadians  — A number that represents an angle measured in radians. 
		 * @return  — A number; the sine of the specified angle (between -1.0 and 1.0). 
		 */
		public static function sin(angleRadians:Number):Number {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Computes and returns the square root of the specified number. </p>
		 * 
		 * @param val  — A number or expression greater than or equal to 0. 
		 * @return  — If the parameter  is greater than or equal to zero, a number; otherwise  (not a number). 
		 */
		public static function sqrt(val:Number):Number {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Computes and returns the tangent of the specified angle. To calculate a radian, see the overview of the Math class. </p>
		 * 
		 * @param angleRadians  — A number that represents an angle measured in radians. 
		 * @return  — The tangent of the parameter . 
		 */
		public static function tan(angleRadians:Number):Number {
			throw new Error("Not implemented");
		}
	}
}
