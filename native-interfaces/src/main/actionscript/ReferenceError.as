package {
	/**
	 *  A ReferenceError exception is thrown when a reference to an undefined property is attempted on a sealed (nondynamic) object. References to undefined variables will result in ReferenceError exceptions to inform you of potential bugs and help you troubleshoot application code. <p>However, you can refer to undefined properties of a dynamic class without causing a ReferenceError exception to be thrown. For more information, see the <code>dynamic</code> keyword.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7ed2.html" target="_blank">Error handling in ActionScript 3.0</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7ecf.html" target="_blank">Responding to error events and status</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="statements.html#dynamic" target="">dynamic keyword</a>
	 * </div><br><hr>
	 */
	public class ReferenceError extends Error {

		/**
		 * <p> Creates a new ReferenceError object. </p>
		 * 
		 * @param message  — Contains the message associated with the ReferenceError object. 
		 */
		public function ReferenceError(message:String = "") {
			super(message);
			throw new Error("Not implemented");
		}
	}
}
