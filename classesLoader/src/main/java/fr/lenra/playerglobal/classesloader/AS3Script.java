package fr.lenra.playerglobal.classesloader;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Logger;

import org.jdom2.Element;

public class AS3Script {
	private static final Logger LOGGER = Logger.getLogger("AS3Script");
	private static final String basePath = "D:/Lenra/playerglobal.js/generated/";
	private static final String AS3 = ".as3";
	
	private final String path;
	private final Set<Dependency> dependencies = new TreeSet<>();
	private final Map<String, List<Definition>> definitions = new HashMap<>();
	
	public AS3Script(Element script) {
		path = script.getAttributeValue("name");
		LOGGER.info("script "+path);
		
		/*Iterator<Element> it = script.getChildren("def", script.getNamespace()).iterator();
		while (it.hasNext()) {
			Definition d = new Definition(it.next());
			if (!definitions.containsKey(d.packageName))
				definitions.put(d.packageName, new ArrayList<Definition>());
			definitions.get(d.packageName).add(d);
		}
		
		it = script.getChildren("dep", script.getNamespace()).iterator();
		while (it.hasNext()) {
			try {
				dependencies.add(new Dependency(it.next()));
			}
			catch (WrongDependencyException e) {
				LOGGER.fine(e.getMessage());
			}
		}
		
		//executor.invokeAll(arg0);
		//TODO: charger la Javadoc de chaque classe*/
		
		buildFile();
	}
	
	public void buildFile() {
		//initialisation du répertoire d'écriture
		File f = new File(basePath+path+AS3);
		f.getParentFile().mkdirs();

		//création du stream d'écriture
		try (FileWriter w = new FileWriter(f)){
			//TODO: écriture de chaque package
			Set<Entry<String, List<Definition>>> entries = definitions.entrySet();
			for (Entry<String, List<Definition>> entry : entries) {
				//Definition du package
				String packageName = entry.getKey();
				w.append("package ");
				if (packageName!=null) {
					w.append(packageName);
					w.append(" ");
				}
				w.append("{");
				w.append("\n");
				
				//Ajout des dépendances
				String lastPackageName = null;
				for (Dependency d : dependencies) {
					//TODO: ne pas ajouter les dépendances du même package
					
					if (d.packageName!=null) {
						String packageBase = d.packageName.split("\\.")[0];
						if (!packageBase.equals(lastPackageName)) {
							w.append("\n");
							lastPackageName = packageBase;
						}
					}
					w.append("\t");
					w.append(d.getFullName());
					w.append(";");
					w.append("\n");
				}
				
				//TODO: Ajout des définitions
				List<Definition> defs = entry.getValue();
				
				//Fermeture du package
				w.append("}");
				w.append("\n");
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
}
