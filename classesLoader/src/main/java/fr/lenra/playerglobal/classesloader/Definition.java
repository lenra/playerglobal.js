package fr.lenra.playerglobal.classesloader;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.Future;
import java.util.logging.Logger;


public class Definition {
	static final Logger LOGGER = Logger.getLogger("Definition");
	
	//private static final Map<DefinitionName, Definition> definitions = new HashMap<>();
	
	private static final List<DefinitionName> ignoredDefinitions = new ArrayList<>();
	
	static {
		/*ignoredDefinitions.add(DefinitionName.OBJECT);
		ignoredDefinitions.add(DefinitionName.VECTOR);
		ignoredDefinitions.add(DefinitionName.getDefinitionName("Error"));
		ignoredDefinitions.add(DefinitionName.getDefinitionName("int"));
		ignoredDefinitions.add(DefinitionName.getDefinitionName("uint"));
		ignoredDefinitions.add(DefinitionName.getDefinitionName("Number"));
		ignoredDefinitions.add(DefinitionName.getDefinitionName("String"));*/
	}
	
	public final DefinitionName name;
	public final Type type;
	public final String path;
	public String documentation;
	private Future<Definition> contentResult;

	public boolean loaded;
	private final Set<DefinitionName> dependencies = new TreeSet<>();
	private DefinitionName extendedClass;
	private Definition extendedDefinition;
	private boolean extendedDefinitionLoaded;
	private final List<DefinitionName> inheritedInterfaces = new ArrayList<>();
	
	private Method constructor;
	
	private final Set<Property> properties = new TreeSet<>();
	private final Set<Method> methods = new TreeSet<>();
	private final Set<Event> events = new TreeSet<>();

	
	public Definition(DefinitionName name, Type type) {
		System.out.println("Create definition for "+name);
		this.name = name;
		this.type = type;
		//pas besoin de gérer les TypedVector
		this.path = name.getFullname().replaceAll("\\.", "/");
		
		this.name.setDefinition(this);
		
		System.out.println("new Definition -> "+name.getPackageName()+":"+name.getName()+"["+type+"]");
	}

	public void setDocumentation(String documentation) {
		this.documentation = documentation;
	}
	
	public DefinitionName getExtendedClass() {
		return extendedClass;
	}
	
	public Definition getExtendedDefinition() {
		if (!extendedDefinitionLoaded) {
			extendedDefinition = Definition.getDefinition(extendedClass);
			extendedDefinitionLoaded = true;
		}
		return extendedDefinition;
	}
	
	public Method getConstructor() {
		return constructor;
	}
	
	public void setExtendedClass(DefinitionName extendedClass) {
		if (this.extendedClass==null && extendedClass!=DefinitionName.OBJECT) {
			this.extendedClass = extendedClass;
			this.addDependency(extendedClass);
		}
	}
	
	public void setInheritedInterfaces(List<DefinitionName> inheritedInterfaces) {
		if (this.inheritedInterfaces.isEmpty()) {
			this.inheritedInterfaces.addAll(inheritedInterfaces);
			for (DefinitionName def : inheritedInterfaces) {
				addDependency(def);
			}
		}
	}
	
	public void addDependency(DefinitionName dependency) {
		if (dependency==null)
			return;
		if (dependencies.contains(dependency))
			return;
		if (dependency==DefinitionName.REST)
			return;
		if (dependency instanceof TypedVector) {
			this.addDependency(DefinitionName.VECTOR);
			this.addDependency(((TypedVector)dependency).type);
			return;
		}
		if (dependency.getPackageName()==null)
			return;
		if (dependency.getPackageName().equals(this.name.getPackageName()))
			return;
		dependencies.add(dependency);
	}
	
	public void addProperty(Property p) {
		boolean isReadOnly = (p instanceof ReadOnlyProperty);
		if (type==Type.CLASS) {
			properties.add(p);
//			if (isReadOnly)
//				addMethod(new Getter(this, p.name, p.staticContent, new ArrayList<Parameter>(), p.returnedType, p.documentation));
		}
		if (!(p instanceof Const)) {
			addMethod(new Getter(this, p.name, p.staticContent, new ArrayList<Parameter>(), p.returnedType, p.documentation));
			if (!isReadOnly) {
				ArrayList<Parameter> params = new ArrayList<>();
				params.add(new Parameter("value", p.returnedType, null));
				addMethod(new Setter(this, p.name, p.staticContent, params, DefinitionName.VOID));
			}
		}
		addDependency(p.returnedType);
	}
	
	public void addMethod(Method m) {
		if (m.isConstructor()) {
			if (this.constructor==null)
				this.constructor = m;
			else
				System.err.println("La définition "+this.name.getFullname()+" a plusieurs constructeurs");
		}
		methods.add(m);
		addDependency(m.returnedType);
		for (Parameter param : m.parameters) {
			addDependency(param.type);
		}
	}
	
	public void addEvent(Event e) {
		events.add(e);
		addDependency(e.type);
	}
	
	public boolean containsMethod(Method m) {
		return methods.contains(m);
	}
	
	public void writeDefinition(File basePath) throws IOException {
		if (ignoredDefinitions.contains(this.name))
			return;
		File f = new File(basePath.getAbsolutePath()+"/"+this.path+".as");
		f.getParentFile().mkdirs();
		FileWriter w = new FileWriter(f);
		w.append("package ");
		if (name.getPackageName()!=null) {
			w.append(name.getPackageName());
			w.append(" ");
		}
		w.append("{\n");
		
		//Ajout des dépendances
		String lastPackageName = null;
		for (DefinitionName d : dependencies) {
			String packageBase = d.getPackageName().split("\\.")[0];
			if (!packageBase.equals(lastPackageName)) {
				if (lastPackageName!=null)
					w.append("\n");
				lastPackageName = packageBase;
			}
			w.append("\timport ");
			w.append(d.getFullname());
			w.append(";\n");
		}
		if (lastPackageName!=null)
			w.append("\n");
		
		boolean directDefinition = (type==Type.PACKAGE || type==Type.FUNCTION);
		
		if (!directDefinition) {
			// ajout des événements
			for (Event event : this.events) {
				event.writeDefinition(w);
			}
			
			// ajout de la documentation de la classe
			DocumentedElement.writeDocumentation(this.documentation, w, "\t");
			
			// ajout de la définition
			w.append("\tpublic ");
			w.append(this.type.name().toLowerCase());
			w.append(' ');
			w.append(this.name.getName());
			if (this.extendedClass!=null) {
				// ajout de la classe étendue
				w.append(" extends ");
				w.append(this.extendedClass.getName());
			}
			if (this.inheritedInterfaces.size()>0) {
				boolean first = true;
				// ajout des interfaces implémentées
				for (DefinitionName inter : this.inheritedInterfaces) {
					if (first) {
						w.append(" implements ");
						first = false;
					}
					else
						w.append(", ");
					w.append(inter.getName());
				}
			}
			w.append(" {\n");
		}
		
		String linePrefix = "\t";
		if (!directDefinition)
			linePrefix += "\t";
		
		// ajout des propriétés
		Class<Property> lastClass = null;
		Boolean lastStatic = null;
		for (Property property : this.properties) {
			Class<Property> c = (Class<Property>) property.getClass();
			Boolean s = property.staticContent;
			if (lastClass!=null && !(c.equals(lastClass) && s.equals(lastStatic)))
				w.append('\n');

			property.writeDefinition(w, linePrefix);
			w.append('\n');
			
			lastClass = c;
			lastStatic = s;
		}
		
		// ajout des méthodes
		lastStatic = false;
		for (Method method : this.methods) {
			if (method.staticContent!=lastStatic) {
				w.append('\n');
			}
			w.append('\n');
			method.writeDefinition(w, linePrefix);
		}
		
		
		if (!directDefinition) {
			//Fermeture de la définition
			w.append("\t}\n");
		}
		
		//Fermeture du package
		w.append("}\n");
		
		w.close();
	}
	
	
	public static Definition getDefinition(DefinitionName name) {
		if (name==null)
			return null;
		
		/*Definition ret;
		synchronized (definitions) {
			ret = definitions.get(name);
		}
		
		while (ret==null) {
			
			synchronized (definitions) {
				System.out.println("Waiting for "+name);
				try {
					definitions.wait(10000);
				}
				catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				ret = definitions.get(name);
			}
		}*/
		
		return name.getDefinition();
	}
	
	public static enum Type {
		CLASS, INTERFACE, FUNCTION, PACKAGE;
	}
}
