package fr.lenra.playerglobal.classesloader;

import java.util.List;

public class Setter extends Method {

	public Setter(Definition definition, String name, boolean staticContent, List<Parameter> parameters, DefinitionName returnedType) {
		super(definition, name, staticContent, parameters, returnedType, "", "");
	}
	
	@Override
	public boolean isSetter() {
		return true;
	}
}
