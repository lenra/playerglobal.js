package fr.lenra.playerglobal.classesloader;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import fr.lenra.playerglobal.classesloader.Definition.Type;

public class PackageDocLoader implements Callable<List<Definition>> {
	

	private final Definition definition;
	private final List<String> constantIds;
	private final List<String> functionIds;

	public PackageDocLoader(Definition definition, List<String> constantIds, List<String> functionIds) {
		this.definition = definition;
		this.constantIds = constantIds;
		this.functionIds = functionIds;
	}

	@Override
	public List<Definition> call() throws Exception {
		Document doc = DocumentationLoader.loadHtml(definition.path);

		List<Definition> ret = new ArrayList<Definition>();
		
		if (!constantIds.isEmpty())
			ret.add(definition);
		
		// charger les docs de contantes
		for (String consId : constantIds) {
			Element constElement = doc.selectFirst("[id='"+consId+"'] ~ div.detailBody");
			Element def = constElement.selectFirst("code");
			String defStr = def.text();
			String name = defStr.substring(defStr.indexOf("const ")+"const ".length(), defStr.indexOf(":"));
			DefinitionName type = DefinitionName.linkToDefinitionName(def.selectFirst("a"), definition.name.getPackageName());
			int eqPos = defStr.indexOf('=');
			String value = null;
			if (eqPos!=-1)
				value = defStr.substring(eqPos+1);
			Elements documentationElements = constElement.select("> table + p + p ~ *:not(br ~ *, :empty)");
			StringBuilder documentation = new StringBuilder();
			for (Element docElement : documentationElements) {
				documentation.append(docElement.outerHtml());
				documentation.append("\n");
			}
			definition.addProperty(new Const(definition, name, false, type, value, documentation.toString()));
		}
		
		// charger les docs des méthodes
		for (String fxId : functionIds) {
			Element fxElement = doc.selectFirst("[id='"+fxId+"'] ~ div.detailBody");
			String defStr = fxElement.selectFirst("code").text();
			
			String name = defStr.substring(defStr.indexOf("function ")+"function ".length(), defStr.indexOf("("));
			Definition def = new Definition(DefinitionName.getDefinitionName(definition.name.getPackageName(), name), Type.FUNCTION);
			def.addMethod(Method.parseMethod(def, fxElement));
			ret.add(def);
		}
		
		return ret;
	}

}
