package fr.lenra.playerglobal.classesloader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import fr.lenra.playerglobal.classesloader.Definition.Type;

public class DefinitionListLoader {
	//private static final String PATH = "all-classes";
	
	private static final String PATH = "package-list";
	public static final ExecutorService executor = Executors.newCachedThreadPool();
	
	public static List<Future<?>> getDefinitions() throws IOException {
		//TODO: refaire le principe de chargement des définition pour récupérer que les définitions du FlashPlayer (function comprises)
		//TODO: charger package par package
		
		List<Future<?>> ret = new ArrayList<>();
	    
		Map<String, String> cookies = new HashMap<>();
		cookies.put("filter_flashplayer", "29.0");
		cookies.put("filter_air", "none");
		cookies.put("filter_blazeds", "none");
		cookies.put("filter_coldfusion", "none");
		cookies.put("filter_flashlite", "none");
		cookies.put("filter_flash", "none");
		cookies.put("filter_flexosmf", "none");
		cookies.put("filter_flex", "none");
		cookies.put("filter_livecyclees", "none");
		cookies.put("filter_livecycle", "none");
		cookies.put("filter_osmf", "none");
		cookies.put("filter_product", "flex");
		
		
	    Document doc = DocumentationLoader.loadHtml(PATH);
	    
	    Elements packages = doc.select("#packagelistWrapper > table:first-of-type tr:not([product]):not([runtime]:not([runtime*=Flash::])) a");
	    
	    for (Element pack : packages) {
	    	String packageName = pack.text();
	    	if ("Top Level".equals(packageName))
	    		packageName = "";
			String packageBaseUrl = packageName.replaceAll("\\.", "/")+"/";
			String packageUrl = packageBaseUrl+"package-detail";
			System.out.println(packageName+" => "+packageUrl);
			
			Definition packageDefinition = new Definition(DefinitionName.getDefinitionName(packageName, "package"), Type.PACKAGE);
			
			Document packageDoc = DocumentationLoader.loadHtml(packageUrl);
			
			List<String> constantIds = new ArrayList<>();
			List<String> functionIds = new ArrayList<>();
			
			Elements elements = packageDoc.select("#summaryTableIdConstant tr:not([product]):not([runtime]:not([runtime*=Flash::])) > td.summaryTableSecondCol > a");
			for (Element constElement : elements) {
				String name = constElement.attr("href").substring("package.html#".length());
				constantIds.add(name);
				System.out.println("const "+name);
			}
			
			elements = packageDoc.select("#summaryTableIdFunction tr:not([product]):not([runtime]:not([runtime*=Flash::])) > td.summaryTableSecondCol > a");
			for (Element functionElement : elements) {
				String name = functionElement.attr("href").substring("package.html#".length());
				functionIds.add(name);
				System.out.println("function "+name);
			}
			
			if (!constantIds.isEmpty() || !functionIds.isEmpty())
				ret.add(executor.submit(new PackageDocLoader(packageDefinition, constantIds, functionIds)));
			
			elements = packageDoc.select("#summaryTableIdInterface tr:not([product]):not([runtime]:not([runtime*=Flash::])) > td.summaryTableSecondCol > i > a");
			for (Element interfaceElement : elements) {
				Definition def = new Definition(DefinitionName.linkToDefinitionName(interfaceElement, packageName), Type.INTERFACE);
				ret.add(executor.submit(new DocumentationLoader(def)));
			}
			
			elements = packageDoc.select("#summaryTableIdClass tr:not([product]):not([runtime]:not([runtime*=Flash::])) > td.summaryTableSecondCol > a");
			for (Element classElement : elements) {
				Definition def = new Definition(DefinitionName.linkToDefinitionName(classElement, packageName), Type.CLASS);
				ret.add(executor.submit(new DocumentationLoader(def)));
			}
		}
		
		return ret;
	}
}
