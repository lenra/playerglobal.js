package fr.lenra.playerglobal.classesloader;

import java.util.HashMap;
import java.util.Map;

public class TypedVector extends DefinitionName {
	private static Map<DefinitionName, TypedVector> TYPES = new HashMap<>();
	public final DefinitionName type;

	public TypedVector(DefinitionName type) {
		super(null, null);
		this.type = type;
		TYPES.put(type, this);
	}
	
	@Override
	public String getPackageName() {
		return DefinitionName.VECTOR.getPackageName();
	}
	
	@Override
	public String getName() {
		return DefinitionName.VECTOR.getName()+".<"+type.getName()+">";
	}
	
	@Override
	public String getFullname() {
		//TODO: implémenter
		return super.getFullname();
	}

	public static TypedVector getTypedVector(DefinitionName type) {
		TypedVector ret = TYPES.get(type);
		if (ret==null)
			ret = new TypedVector(type);
		return ret;
	}
}
