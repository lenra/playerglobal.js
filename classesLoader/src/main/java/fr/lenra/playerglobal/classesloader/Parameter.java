package fr.lenra.playerglobal.classesloader;

public class Parameter extends DocumentedElement {
	public static final Parameter REST = new Parameter("args", null, null);
	
	public final String name;
	public final DefinitionName type;
	public final String value;
	
	public Parameter(String name, DefinitionName type, String documentation) {
		this(name, type, documentation, null);
	}
	
	public Parameter(String name, DefinitionName type, String documentation, String value) {
		super(documentation);
		this.name = name;
		this.type = type;
		this.value = value;
	}
}
