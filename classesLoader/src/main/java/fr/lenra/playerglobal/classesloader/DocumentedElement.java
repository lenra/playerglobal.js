package fr.lenra.playerglobal.classesloader;

import java.io.IOException;
import java.io.Writer;

public abstract class DocumentedElement {
	public final String documentation;

	public DocumentedElement(String documentation) {
		this.documentation = documentation;
	}
	
	protected void writeDocumentation(Writer w, String linePrefix) throws IOException {
		DocumentedElement.writeDocumentation(this.documentation, w, linePrefix);
	}

	public static void writeDocumentation(String documentation, Writer w, String linePrefix) throws IOException {
		if (documentation==null || documentation.isEmpty())
			return;
		String[] lines = documentation.split("\n");
		w.append(linePrefix);
		w.append("/**\n");
		for (String line : lines) {
			w.append(linePrefix);
			w.append(" * ");
			w.append(line);
			w.append("\n");
		}
		w.append(linePrefix);
		w.append(" */\n");
	}
}
