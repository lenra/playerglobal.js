package fr.lenra.playerglobal.classesloader;

import java.io.IOException;
import java.io.Writer;

import fr.lenra.playerglobal.classesloader.Definition.Type;

public class Property extends Component implements Comparable<Property> {
	
	public Property(Definition definition, String name, boolean staticContent, DefinitionName type, String documentation) {
		super(definition, name, staticContent, type, documentation);
	}
	
	@Override
	protected void writeAccess(Writer w) throws IOException {
		w.append("private ");
		if (staticContent)
			w.append("static ");
	}
	
	protected void writeNameAndType(Writer w) throws IOException {
		w.append(name);
		w.append(':');
		w.append(returnedType.getName());
	}
	
	public void writeDefinition(Writer w, String linePrefix) throws IOException {
		if (definition.type==Type.INTERFACE)
			return;
		w.append(linePrefix);
		writeAccess(w);
		w.append("var _");
		writeNameAndType(w);
		w.append(';');
		/*writeDocumentation(w, 2);
		w.append("\t\t");
		writeAccess(w);
		w.append("var ");
		writeNameAndType(w);
		w.append(';');*/
	}

	@Override
	public int compareTo(Property p) {
		if (this.staticContent!=p.staticContent)
			return this.staticContent ? -1 : 1;
		if (this instanceof Const!=p instanceof Const)
			return this instanceof Const ? -1 : 1;
		if (this instanceof ReadOnlyProperty!=p instanceof ReadOnlyProperty)
			return this instanceof ReadOnlyProperty ? 1 : -1;
		return this.name.compareTo(p.name);
	}
}
