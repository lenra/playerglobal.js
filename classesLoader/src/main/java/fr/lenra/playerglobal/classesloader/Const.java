package fr.lenra.playerglobal.classesloader;

import java.io.IOException;
import java.io.Writer;

public class Const extends Property {

	public final String value;

	public Const(Definition definition, String name, boolean staticContent, DefinitionName type, String value, String documentation) {
		super(definition, name, staticContent, type, documentation);
		this.value = value;
	}
	
	@Override
	protected void writeAccess(Writer w) throws IOException {
		w.append("public ");
		if (staticContent)
			w.append("static ");
	}
	
	@Override
	public void writeDefinition(Writer w, String linePrefix) throws IOException {
		writeDocumentation(w, linePrefix);
		w.append(linePrefix);
		writeAccess(w);
		w.append("const ");
		writeNameAndType(w);
		w.append(" = ");
		w.append(value);
		w.append(';');
	}
}
