package fr.lenra.playerglobal.classesloader;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

import fr.lenra.playerglobal.classesloader.Definition.Type;

public class Method extends Component implements Comparable<Method> {
	public final List<Parameter> parameters;

	public Method(Definition definition, String name, boolean staticContent, List<Parameter> parameters, DefinitionName returnedType, String documentation, String returnDocumentation) {
		super(definition, name, staticContent, returnedType, buildDocumentation(documentation, parameters, returnDocumentation));
		this.parameters = parameters;
	}
	
	public boolean isConstructor() {
		return this.name.equals(this.definition.name.getName());
	}
	
	public boolean isGetter() {
		return false;
	}
	
	public boolean isSetter() {
		return false;
	}
	
	@Override
	public void writeDefinition(Writer w, String linePrefix) throws IOException {
		writeDocumentation(w, linePrefix);
		w.append(linePrefix);
		
		// gérer l'override
		boolean override = false;
		Definition c = definition;
		//if (this.name.equals("toString") || this.name.equals("valueOf"))
		//	override = true;
		while (!override && c!=null && c.getExtendedClass()!=null) {
			c = Definition.getDefinition(c.getExtendedClass());
			if (c!=null && c.containsMethod(this))
				override = true;
		}
		if (override)
			w.append("override ");
		
		if (definition.type!=Type.INTERFACE)
			writeAccess(w);
		w.append("function ");
		if (this instanceof Getter) 
			w.append("get ");
		else if (this instanceof Setter) 
			w.append("set ");
		w.append(this.name);
		w.append('(');
		boolean first = true;
		for (Parameter param : this.parameters) {
			if (!first)
				w.append(", ");
			if (param.type==DefinitionName.REST) {
				w.append("...");
				w.append(param.name);
			}
			else {
				w.append(param.name);
				w.append(':');
				w.append(param.type.getName());
				if (param.value!=null) {
					w.append(" = ");
					w.append(param.value);
				}
			}
			first = false;
		}
		w.append(")");
		if (this.returnedType!=null) {//Constructor : null
			w.append(':');
			w.append(this.returnedType.getName());
		}
		if (definition.type!=Type.INTERFACE) {
			String contentLinePrefix = linePrefix + "\t";
			w.append(" {\n");
			if (this instanceof Getter) {
				w.append(contentLinePrefix);
				w.append("return _"+this.name+";\n");
			}
			else if (this instanceof Setter) {
				w.append(contentLinePrefix);
				w.append("_"+this.name+" = "+parameters.get(0).name+";\n");
			}
			else {
				if (this.isConstructor()) {
					Definition superClass = Definition.getDefinition(this.definition.getExtendedClass());
					Method constructor = superClass!=null ? superClass.getConstructor() : null;
					if (constructor!=null && !constructor.parameters.isEmpty()) {
						w.append(contentLinePrefix);
						w.append("super(");
						boolean firstParam = true;
						for (Parameter param : constructor.parameters) {
							if (firstParam)
								firstParam = false;
							else
								w.append(", ");
							w.append(param.name);
						}
						w.append(");\n");
					}
				}
				w.append(contentLinePrefix);
				w.append("throw new Error(\"Not implemented\");\n");
			}
			w.append(linePrefix);
			w.append("}\n");
		}
		else
			w.append(";\n");
	}

	@Override
	public int compareTo(Method m) {
		if (this.staticContent!=m.staticContent)
			return this.staticContent ? 1 : -1;
		if (this.isConstructor())
			return -1;
		if	(m.isConstructor())
			return 1;
		if ((this.isGetter() || this.isSetter()) && (m.isGetter() || m.isSetter())) {
			int res = this.name.compareTo(m.name);
			if (res==0)
				return this.isGetter() ? -1 : 1;
			return res;
		}
		if (this.isGetter()!=m.isGetter())
			return this.isGetter() ? -1 : 1;
		if (this.isSetter()!=m.isSetter())
			return this.isSetter() ? -1 : 1;
		return this.name.compareTo(m.name);
	}
	
	private static String buildDocumentation(String doc, List<Parameter> params, String returnDoc) {
		StringBuilder ret = new StringBuilder();
		if (doc!=null && !doc.trim().isEmpty()) {
			ret.append(doc);
			ret.append('\n');
		}
		for (Parameter parameter : params) {
			ret.append("@param ");
			ret.append(parameter.name);
			if (parameter.documentation!=null && !parameter.documentation.trim().isEmpty()) {
				ret.append(' ');
				ret.append(parameter.documentation);
			}
			ret.append('\n');
		}
		if (returnDoc!=null) {
			ret.append("@return ");
			ret.append(returnDoc);
			ret.append('\n');
		}
		
		return ret.toString();
	}
	
	public static Method parseMethod(Definition definition, Element fxElement) {
		Element def = fxElement.selectFirst("code");
		String defStr = def.text();
		
		String name = defStr.substring(defStr.indexOf("function ")+"function ".length(), defStr.indexOf("("));
		
		//Non applicable
		boolean staticProp = defStr.contains(" static ");
		
		List<Parameter> params = new ArrayList<>();
		DefinitionName returnedType = null;
		
		List<Node> childNodes;
		
		Elements paramElements = fxElement.select("> :not(:not(p:has(span.label), span.label)):matches(Parameters) + table > tbody > tr > td:last-child:not(.paramSpacer)");			
		
		for (Element paramElement : paramElements) {
			childNodes = paramElement.childNodes();
			int pos = 0;
			Element firstCode = (Element)childNodes.get(pos++);
			DefinitionName paramType;
			Element typeElement = firstCode.selectFirst("a");
			if (typeElement!=null)
				paramType = DefinitionName.linkToDefinitionName(typeElement, definition.name.getPackageName());
			else
				paramType = DefinitionName.OBJECT;
			String paramName = firstCode.selectFirst("span.label").text();
			String value = null;
			StringBuilder paramDoc = new StringBuilder();
			
			if (childNodes.size() > 1 && childNodes.get(1).outerHtml().contains("(default =")) {
				value = ((Element)childNodes.get(2)).text();
				pos += 4;
				if (childNodes.get(1).outerHtml().contains("\""))
					value = "\"" + value + "\"";
			}
			for (int i = pos; i < childNodes.size(); i++) {
				paramDoc.append(childNodes.get(i).outerHtml());
			}
			
			params.add(new Parameter(paramName, paramType, paramDoc.toString(), value));
		}

		// gérer la documentation du type de retour
		String returnDoc = null;
		Element returnDocElement = fxElement.selectFirst("> :not(:not(p:has(span.label), span.label)):matches(Returns) + table tr > td:last-child");			
		if (returnDocElement!=null) {
			returnDoc = "";
			childNodes = returnDocElement.childNodes();
			Element typeElement = ((Element)childNodes.get(0)).selectFirst("a");
			if (typeElement!=null)
				returnedType = DefinitionName.linkToDefinitionName(typeElement, definition.name.getPackageName());
			else
				returnedType = DefinitionName.OBJECT;
			for (Node node : childNodes) {
				if (!"code".equals(node.nodeName()))
					returnDoc += node.outerHtml();
			}
		}
		else if (!name.equals(definition.name.getName())) {
			returnedType = DefinitionName.VOID;
		}
		
		Elements documentationElements = fxElement.select("> table + p ~ *:not(br ~ *, :empty, :not(:not(p:has(span.label), span.label)):matches(Returns|Parameters), :not(:not(p:has(span.label), span.label)):matches(Returns|Parameters) ~ *)");
		StringBuilder documentation = new StringBuilder();
		for (Element docElement : documentationElements) {
			documentation.append(docElement.outerHtml());
			documentation.append("\n");
		}
		
		return new Method(definition, name, staticProp, params, returnedType, documentation.toString(), returnDoc);
	}
}
