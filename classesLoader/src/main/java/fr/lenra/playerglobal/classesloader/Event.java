package fr.lenra.playerglobal.classesloader;

import java.io.IOException;
import java.io.Writer;

public class Event extends DocumentedElement implements Comparable<Event> {
	public final String name;
	public final DefinitionName type;
	
	public Event(String name, DefinitionName type, String documentation) {
		super(documentation);
		this.name = name;
		this.type = type;
	}
	
	public void writeDefinition(Writer w) throws IOException {
		writeDocumentation(w, "\t");
		w.append("\t");
		w.append("[Event(");
		w.append("name=\"");
		w.append(name);
		w.append("\", ");
		w.append("type=\"");
		w.append(type.getFullname());
		w.append("\")]\n");
	}

	@Override
	public int compareTo(Event e) {
		return this.name.compareTo(e.name);
	}
}
