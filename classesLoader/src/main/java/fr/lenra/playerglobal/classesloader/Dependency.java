package fr.lenra.playerglobal.classesloader;

import org.jdom2.Element;

public class Dependency implements Comparable<Dependency> {
	public final String packageName;
	public final String name;
	
	public Dependency(Element dep) throws WrongDependencyException {
		String id = dep.getAttributeValue("id");
		if ("Object".equals(id))
			throw new WrongDependencyException("The dependency " + id + " is not correct");
		if (id.contains(":")) {
			String[] parts = id.split(":");
			packageName = parts[0];
			name = parts[1];
		}
		else {
			packageName = null;
			name = id;
		}
	}
	
	public String getFullName() {
		if (packageName!=null)
			return packageName + "." + name;
		return name;
	}
	
	@Override
	public int compareTo(Dependency dep) {
		if (packageName!=null==(dep.packageName!=null))
			return this.getFullName().compareTo(dep.getFullName());
		return packageName!=null ? 1 : -1;
	}
}
