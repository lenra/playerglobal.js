package fr.lenra.playerglobal.classesloader;

import java.util.HashMap;
import java.util.Map;

import org.jsoup.nodes.Element;


public class DefinitionName implements Comparable<DefinitionName>{
	private static final Map<String, Map<String, DefinitionName>> definitionNames = new HashMap<>();
	public static final DefinitionName OBJECT = getDefinitionName("Object");
	public static final DefinitionName VOID = getDefinitionName("void");
	public static final DefinitionName VECTOR = getDefinitionName("Vector");
	public static final DefinitionName REST = getDefinitionName("statements#..._(rest)_parameter");
	public static final DefinitionName EVENT = getDefinitionName("flash.events.Event");
	
	private final String packageName;
	private final String name;
	
	private Definition definition;
	
	protected DefinitionName(String packageName, String name) {
		this.packageName = packageName;
		this.name = name;
		if (!definitionNames.containsKey(packageName))
			definitionNames.put(packageName, new HashMap<String, DefinitionName>());
		definitionNames.get(packageName).put(name, this);
	}
	
	public String getPackageName() {
		return packageName;
	}

	public String getName() {
		if (this==EVENT)
			return this.packageName + "." + name;
		return name;
	}

	public String getFullname() {
		return getPackageName()!=null && this!=EVENT ? getPackageName() + "." + getName() : getName();
	}
	
	public synchronized Definition getDefinition() {
		if (definition==null) {
			try {
				this.wait(1000);
			}
			catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return definition;
	}
	
	public synchronized void setDefinition(Definition definition) {
		if (this.definition!=null || definition.name!=this)
			return;
		this.definition = definition;
		this.notifyAll();
	}
	
	@Override
	public int compareTo(DefinitionName dep) {
		if (packageName!=null==(dep.packageName!=null))
			return this.getFullname().compareTo(dep.getFullname());
		return packageName!=null ? 1 : -1;
	}
	
	@Override
	public boolean equals(Object o) {
		return compareTo((DefinitionName) o)==0;
	}
	
	@Override
	public String toString() {
		return getFullname();
	}
	
	public static DefinitionName getDefinitionName(String packageName, String name) {
		if (packageName!=null && packageName.isEmpty())
			packageName = null;
		if (definitionNames.containsKey(packageName) && definitionNames.get(packageName).containsKey(name))
			return definitionNames.get(packageName).get(name);
		return new DefinitionName(packageName, name);
	}
	
	public static DefinitionName getDefinitionName(String fullname) {
		String packageName = null;
		String name = fullname;
		int pos = name.lastIndexOf('.');
		if (pos!=-1) {
			packageName = name.substring(0, pos);
			name = name.substring(pos + 1);
		}
		else if (name.startsWith("specialTypes#")) {
			name = name.substring("specialTypes#".length());
		}
		return getDefinitionName(packageName, name);
	}
	
	public static DefinitionName linkToDefinitionName(Element aElement, String packageName) {
		String name = aElement.attr("href");
		String pName = packageName;
		if (pName==null)
			pName = "";
		while (!pName.isEmpty() && name.contains("../")) {
			name = name.replaceFirst("\\.\\.\\/", "");
			int pos = pName.lastIndexOf(".");
			if (pos!=-1)
				pName = pName.substring(0, pos);
			else
				pName = "";
		}
		name = name.replaceAll("\\.\\.\\/", "");
		name = name.replaceAll("\\.html", "");
		name = name.replaceAll("\\/", ".");
		if (!pName.isEmpty())
			pName += '.';
		
		DefinitionName ret = DefinitionName.getDefinitionName(pName + name);
		
		//gérer les vecteurs
		if (ret==DefinitionName.VECTOR && aElement.nextSibling()!=null){
			if (".&lt;".equals(aElement.nextSibling().outerHtml())) {
				//TODO: aller chercher les noeuds après le a jusqu'a la fermeture du vecteur
				//List<Node> nodes = aElement.parent().childNodes();
				DefinitionName type = linkToDefinitionName(aElement.nextElementSibling(), packageName);
				
				//TODO: récupérer le type du vecteur
				
				//TODO: récupérer le vecteur pour le type trouver et remplir ret avec
				ret = TypedVector.getTypedVector(type);
			}
			else {
				System.out.println("next sibling (not .&lt;) => "+aElement.nextSibling().outerHtml());
			}
		}
		
		return ret;
	}
}
