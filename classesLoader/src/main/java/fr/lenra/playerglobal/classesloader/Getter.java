package fr.lenra.playerglobal.classesloader;

import java.util.List;

public class Getter extends Method {

	public Getter(Definition definition, String name, boolean staticContent, List<Parameter> parameters, DefinitionName returnedType, String documentation) {
		super(definition, name, staticContent, parameters, returnedType, documentation, "");
	}
	
	@Override
	public boolean isGetter() {
		return true;
	}
}
