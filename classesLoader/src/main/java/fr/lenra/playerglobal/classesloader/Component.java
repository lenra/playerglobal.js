package fr.lenra.playerglobal.classesloader;

import java.io.IOException;
import java.io.Writer;

public abstract class Component extends DocumentedElement {
	public final Definition definition;
	public final String name;
	public final boolean staticContent;
	public final DefinitionName returnedType;
	
	public Component(Definition definition, String name, boolean staticContent, DefinitionName returnedType, String documentation) {
		super(documentation);
		this.definition = definition;
		this.name = name;
		this.staticContent = staticContent;
		this.returnedType = returnedType;
	}
	
	protected void writeAccess(Writer w) throws IOException {
		w.append("public ");
		if (staticContent)
			w.append("static ");
	}
	
	public abstract void writeDefinition(Writer w, String linePrefix) throws IOException;
	
	@Override
	public String toString() {
		StringBuilder ret = new StringBuilder();
		ret.append(name);
		ret.append(":");
		ret.append(returnedType);
		if (staticContent)
			ret.append("{static}");
		
		return ret.toString();
	}
}
