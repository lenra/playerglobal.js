package fr.lenra.playerglobal.classesloader;

public class WrongDependencyException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2261653600830588412L;

	public WrongDependencyException(String message) {
		super(message);
	}
}
