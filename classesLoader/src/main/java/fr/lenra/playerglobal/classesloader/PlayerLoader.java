package fr.lenra.playerglobal.classesloader;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.logging.Logger;

public class PlayerLoader {

	private static final Logger LOGGER = Logger.getLogger("PlayerLoader");
	private static final String URL = "https://fpdownload.macromedia.com/get/flashplayer/updaters/%v/playerglobal%v_0.swc";

	private final URL url;
	
	public PlayerLoader(int version) throws MalformedURLException {
		this.url = new URL(URL.replaceAll("%v", Integer.toString(version)));
	}
	
	public File load() {
		LOGGER.info("start loading "+url);
		File ret = null;
		try {
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			con.setDoInput(true);
			con.connect();
			int status = con.getResponseCode();
			LOGGER.info("status "+status);
			if (status==200) {
				ReadableByteChannel rbc = Channels.newChannel(con.getInputStream());
				ret = File.createTempFile("player_", ".swc");
				FileOutputStream fos = new FileOutputStream(ret);
				fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
				fos.close();
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		return ret;
	}
}
