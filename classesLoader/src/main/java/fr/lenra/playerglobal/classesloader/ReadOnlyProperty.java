package fr.lenra.playerglobal.classesloader;

import java.io.IOException;
import java.io.Writer;

import fr.lenra.playerglobal.classesloader.Definition.Type;

public class ReadOnlyProperty extends Property {

	public ReadOnlyProperty(Definition definition, String name, boolean staticContent, DefinitionName returnedType, String documentation) {
		super(definition, name, staticContent, returnedType, documentation);
	}
	
	/*@Override
	protected void writeAccess(Writer w) throws IOException {
		w.append("private ");
		if (staticContent)
			w.append("static ");
	}*/

	@Override
	public void writeDefinition(Writer w, String linePrefix) throws IOException {
		if (definition.type==Type.INTERFACE)
			return;
		w.append("\t\t");
		writeAccess(w);
		w.append("var _");
		writeNameAndType(w);
		w.append(';');
	}
}
