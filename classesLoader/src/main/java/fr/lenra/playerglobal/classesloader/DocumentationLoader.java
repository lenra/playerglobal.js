package fr.lenra.playerglobal.classesloader;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

public class DocumentationLoader implements Callable<Definition> {
	private static final String URL = "https://help.adobe.com/en_US/FlashPlatform/reference/actionscript/3/%def.html";
	private static final String CACHE = "D:/Lenra/playerglobal.js/cache/%def.html";
	
	private static final String charset = "UTF-8";
	private final Definition definition;
	
	public DocumentationLoader(Definition definition) {
		this.definition = definition;
	}
	
	@Override
	public Definition call() throws Exception {
		Document doc = loadHtml(definition.path);
		
		// récupérer la doc de la classe
		int emptyP = 0;
		StringBuilder classDocumentation = null;
		Element mainContent = doc.selectFirst("#content > .content > .MainContent");
		List<Node> nodes = mainContent.childNodes();
		for (Node node : nodes) {
			if ("p".equals(node.nodeName()) && ((Element)node).is(":empty")) {
				if (++emptyP==2) {
					if (classDocumentation==null) {
						classDocumentation = new StringBuilder();
						emptyP = 0;
					}
					else 
						break;
				}
			}
			else {
				emptyP = 0;
				if (classDocumentation!=null)
					classDocumentation.append(node.outerHtml());
			}
		}
		
		definition.setDocumentation(classDocumentation.toString());
		
		// l'héritage de classe et implémentation d'interfaces
		String extendedClass = null;
		Element extendedClassElement = doc.selectFirst(".inheritanceList > img.inheritArrow + a");
		if (extendedClassElement!=null) {
			this.definition.setExtendedClass(DefinitionName.linkToDefinitionName(extendedClassElement, definition.name.getPackageName()));
			Definition parentDef = Definition.getDefinition(this.definition.getExtendedClass());
			if (parentDef!=null) {
				synchronized (parentDef) {
					if (!parentDef.loaded)
						parentDef.wait();
				}
			}
		}
		
		Elements interfaceLinks = doc.select("td.classHeaderTableLabel:contains(Implements) + td > a");
		if (interfaceLinks!=null && !interfaceLinks.isEmpty()) {
			List<DefinitionName> inheritedInterfaces = new ArrayList<>();
			for (Element element : interfaceLinks) {
				inheritedInterfaces.add(DefinitionName.linkToDefinitionName(element, definition.name.getPackageName()));
			}
			this.definition.setInheritedInterfaces(inheritedInterfaces);
		}
		
		// récupérer les proprités (statics ou non)
		Elements propElements = doc.select("#summaryTableProperty tr > td.summaryTableInheritanceCol:not(:has(img)) + td.summaryTableSignatureCol");
		for (Element propElement : propElements) {
			Element nameElement = propElement.selectFirst("a.signatureLink");
			String name = nameElement.text();
			Element typeLink = propElement.selectFirst("a.signatureLink ~ a");
			
			DefinitionName type;
			if (typeLink!=null)
				type = DefinitionName.linkToDefinitionName(typeLink, definition.name.getPackageName());
			else {
				type = DefinitionName.OBJECT;
				System.err.println("ERRRRRRREURRR : Erreur de récupération du type de la propriété "+definition.name.getFullname()+"."+name);
			}
			boolean readOnly = propElement.selectFirst("div.summaryTableDescription:contains([read-only])")!=null;
			boolean staticProp = propElement.selectFirst("div.summaryTableDescription:contains([static])")!=null;
			Elements documentationElements = doc.select(nameElement.attr("href")+" ~ div.detailBody > table + p + p ~ *:not(br ~ *, :empty)");
			StringBuilder documentation = new StringBuilder();
			for (Element docElement : documentationElements) {
				documentation.append(docElement.outerHtml());
				documentation.append("\n");
			}
			if (readOnly)
				definition.addProperty(new ReadOnlyProperty(definition, name, staticProp, type, documentation.toString()));
			else
				definition.addProperty(new Property(definition, name, staticProp, type, documentation.toString()));
		}
		
		// récupérer les constantes
		propElements = doc.select("#summaryTableConstant tr > td.summaryTableInheritanceCol:not(:has(img)) + td.summaryTableSignatureCol");
		for (Element propElement : propElements) {
			Element nameElement = propElement.selectFirst("a.signatureLink");
			String name = nameElement.text();
			Element typeElement = propElement.selectFirst("a.signatureLink ~ a");
			DefinitionName type = DefinitionName.linkToDefinitionName(typeElement, definition.name.getPackageName());
			List<Node> nodeList = propElement.childNodes();
			boolean isReady = false;
			String value = null;
			for (Node node : nodeList) {
				if (isReady) {
					value = node.outerHtml().substring(3).trim();
					break;
				}
				if (node.equals(typeElement)) {
					isReady = true;
				}
			}
			boolean staticProp = propElement.selectFirst("div.summaryTableDescription:contains([static])")!=null;
			Elements documentationElements = doc.select(nameElement.attr("href")+" ~ div.detailBody > table + p + p ~ *:not(br ~ *, :empty)");
			StringBuilder documentation = new StringBuilder();
			for (Element docElement : documentationElements) {
				documentation.append(docElement.outerHtml());
				documentation.append("\n");
			}
			definition.addProperty(new Const(definition, name, staticProp, type, value, documentation.toString()));
		}
		
		// récupérer les méthodes
		Elements methodElements = doc.select("#summaryTableMethod tr > td.summaryTableInheritanceCol:not(:has(img)) + td.summaryTableSignatureCol > div.summarySignature");
		for (Element methodElement : methodElements) {
			String link = methodElement.selectFirst("a.signatureLink").attr("href").replaceAll("#", "");
			definition.addMethod(Method.parseMethod(definition, doc.selectFirst("[id='"+ link +"'] ~ div.detailBody")));
		}
		
		// récupérer les événements écoutés
		Elements eventElements = doc.select("#summaryTableEvent tr > td.summaryTableInheritanceCol:not(:has(img)) + td.summaryTableSignatureCol > div.summarySignature");
		for (Element eventElement : eventElements) {
			Element nameElement = eventElement.selectFirst("a.signatureLink");
			
			Element typeElement = doc.selectFirst("[id='"+ nameElement.attr("href").replaceAll("#", "") +"'] ~ div.detailBody > span.label + a");
			DefinitionName type;
			if (typeElement!=null)
				type = DefinitionName.linkToDefinitionName(typeElement, definition.name.getPackageName());
			else
				type = definition.name;
			String name = nameElement.text();/*typeElements.get(1).attr("href");
			int pos = name.indexOf('#');
			if (!type.equals(hrefToDefinitionName(name.substring(0, pos)))) {
				throw new Exception("Type différent...");
			}
			name = nameElement.text();*/
			
			Elements documentationElements = doc.select("[id='"+ nameElement.attr("href").replaceAll("#", "") +"'] ~ div.detailBody > table + p + p ~ *:not(br ~ *, :empty)");
			StringBuilder documentation = new StringBuilder();
			for (Element docElement : documentationElements) {
				documentation.append(docElement.outerHtml());
				documentation.append("\n");
			}
			definition.addEvent(new Event(name, type, documentation.toString()));
		}
		
		this.definition.loaded = true;
		
		synchronized (this.definition) {
			this.definition.notifyAll();
		}
		
		return this.definition;
	}
	
	public static Document loadHtml(String path) throws IOException {
		//gérer le cache
		Path filePath = Paths.get(CACHE.replaceAll("%def", path));
		
		String html;
		if (!ClassesLoader.isUseCache() || !Files.exists(filePath)) {
			//charger la page de documentation
			String url = URL.replaceAll("%def", path);
			Definition.LOGGER.info("Loading "+url);
			
			html = Jsoup.connect(url).execute().body();

			Files.createDirectories(filePath.getParent());
			Files.write(filePath, html.getBytes(charset));
		}
		else 
			html = new String(Files.readAllBytes(filePath), charset);
		
		return Jsoup.parse(html);
	}
}