package fr.lenra.playerglobal.classesloader;

import java.io.File;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Future;
import java.util.logging.Logger;

import org.xml.sax.SAXException;

public class ClassesLoader {

	private static final Logger LOGGER = Logger.getGlobal();
	
	private static Locale language = Locale.US;//Locale.FRANCE;
	
	private static int version = 29;
	
	private static final String basePath = "D:/Lenra/playerglobal.js/generated/";
	
	private static boolean useCache = true;
	
	
	/**
	 * @param args
	 * @throws Exception 
	 * @throws SAXException 
	 */
	public static void main(String[] args) throws Exception {
		LOGGER.info("ClassesLoader startup");
		
		//TODO: récupérer les valeurs des arguments
		
		//TODO: si il n'y a pas de version définie, aller chercher la dernière version disponible
		
		// Charger la liste des classes (et interfaces) à créer
		List<Future<?>> results = DefinitionListLoader.getDefinitions();
	    LOGGER.info("Definitions found : "+results.size());
		
	    File baseDir = new File(basePath);
		
	    for (Future<?> res : results) {
	    	Object resVal = res.get();
	    	if (resVal instanceof Definition)
	    		((Definition)resVal).writeDefinition(baseDir);
	    	else {
	    		List<Definition> l = (List<Definition>)resVal;
	    		for (Definition def : l) {
					def.writeDefinition(baseDir);
				}
	    	}
		}		
	    
		LOGGER.info("ClassesLoader end");
		DefinitionListLoader.executor.shutdown();
	}


	public static boolean isUseCache() {
		return useCache;
	}
}
