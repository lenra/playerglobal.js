(function () {
    ////////// Init of the flash.util.Proxy classes //////////
    
    var googInherits = goog.inherits;
    var googExportSymbol = goog.exportSymbol;
    var googProvide = goog.provide;
    var googGlobalCLOSURE_IMPORT_SCRIPT = goog.global.CLOSURE_IMPORT_SCRIPT;
    var proxies = [];
    //Proxy handlers definition
    var objectProxyHandler = {
		get: function(target, property) {
			return target.getProperty(property);
		},
		set: function(target, property, value) {
			target.setProperty(property, value);
		}
	};
    var classProxyHandler = {
        get: function(target, property) {
            return target[property];
        },
        set: function(target, property, value) {
            target[property] = value;
        },
        construct: function(target, argumentsList) {
            return new Proxy(new target(...argumentsList), objectProxyHandler);
        }
    };

    //Override of the goog methods
    goog.inherits = function (child, parent) {
        initProxy();
        //If the parent is a proxy, then the child is one two
        if (proxies.indexOf(parent.prototype.constructor)!=-1)
            proxies.push(child.prototype.constructor);
        //Call of the original goog method
        googInherits(child, parent);
    };
    goog.exportSymbol = function(className, constructor) {
        initProxy();
        //If this target class is a proxy
        if (proxies.indexOf(constructor.prototype.constructor)!=-1) {
            //Replacement of the target class by a Proxy
            var origine = constructor;
            constructor = new Proxy(origine, classProxyHandler);

            //Getting the package to replace the class definition
            var cNamePos = className.lastIndexOf(".")+1;
            var cName = className;
            var pack = self;
            if (cNamePos) {
                cName = className.substring(cNamePos);
                pack = getAsElementByName(className.substring(0, cNamePos-1));
            }
            pack[cName] = constructor;
        }
        //Call of the original goog method
        googExportSymbol(className, constructor);
    };
    // goog.provide = function(className) {
    //     googProvide(className);
    //     if (className=='org.apache.royale.utils.Language') {
    //         console.log(document.currentScript);
    //     }
    // };
    if (!self.document) {
        goog.global.CLOSURE_IMPORT_SCRIPT = function() {
            console.log(arguments, "loading");
            googGlobalCLOSURE_IMPORT_SCRIPT.apply(null, arguments);
            console.log(arguments, "loaded");
            if (arguments[0].endsWith("org/apache/royale/utils/Language.js")) {
				org.apache.royale.utils.Language.Vector.prototype.ROYALE_CLASS_INFO = { names: [{ name: 'Vector', qName: 'org.apache.royale.utils.Language.Vector', kind: 'class' }] };
                /*var c = getAsElementByName(className);
                if (c && c.prototype && !("ROYALE_CLASS_INFO" in c.prototype)) {
                    var cName = className.split(".");
                    cName = cName[cName.length-1];
                    c.prototype.ROYALE_CLASS_INFO = { names: [{ name: cName, qName: className, kind: 'class' }] };
                }*/
            }
        };
    }
    function initProxy() {
        //If the proxy class is not in the proxy list and exists, it's added to the list
        if (!proxies.length && 'flash' in self && flash.utils && flash.utils.Proxy)
            proxies.push(flash.utils.Proxy.prototype.constructor);
    }


    ////////// Object class polyfill //////////

    Boolean.prototype.ROYALE_CLASS_INFO = { names: [{ name: 'Boolean', qName: 'Boolean', kind: 'class' }] };
    Number.prototype.ROYALE_CLASS_INFO = { names: [{ name: 'Number', qName: 'Number', kind: 'class' }] };
    String.prototype.ROYALE_CLASS_INFO = { names: [{ name: 'String', qName: 'String', kind: 'class' }] };
    //org.apache.royale.utils.Language.Vector.prototype.ROYALE_CLASS_INFO = { names: [{ name: 'Vector', qName: 'org.apache.royale.utils.Language.Vector', kind: 'class' }] };

})();

function getAsElementByName(name) {
    var parts = name.split(".");
    if (parts.length==1 && parts[0]=="")
        parts.pop();
    var p = self;
    for (var i = 0; i < parts.length; i++) {
        if (!(parts[i] in p))
            return null;
        p = p[parts[i]];
    }
    return p;
}