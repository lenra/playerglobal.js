package {

	/**
	 * <p> Displays expressions, or writes to log files, while debugging. A single trace statement can support multiple arguments. If any argument in a trace statement includes a data type other than a String, the trace function invokes the associated <code>toString()</code> method for that data type. For example, if the argument is a Boolean value the trace function invokes <code>Boolean.toString()</code> and displays the return value. </p>
	 * 
	 * @param arguments  — One or more (comma separated) expressions to evaluate. For multiple expressions, a space is inserted between each expression in the output. 
	 */
	public function trace(...arguments):void {
		throw new Error("Not implemented");
	}
}
