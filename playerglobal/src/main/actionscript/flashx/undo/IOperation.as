package flashx.undo {
	/**
	 *  IOperation defines the interface for operations that can be undone and redone. <br><hr>
	 */
	public interface IOperation {

		/**
		 * <p> Reperforms the operation. </p>
		 * <p>The operation is also responsible for pushing itself onto the undo stack.</p>
		 */
		//function performRedo():void;

		/**
		 * <p> Reverses the operation. </p>
		 * <p>The operation is also responsible for pushing itself onto the redo stack.</p>
		 */
		//function performUndo():void;
	}
}
