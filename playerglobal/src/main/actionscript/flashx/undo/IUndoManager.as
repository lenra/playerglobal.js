package flashx.undo {
	/**
	 *  IUndoManager defines the interface for managing the undo and redo stacks. <p>An undo manager maintains a stack of operations that can be undone and redone.</p> <br><hr>
	 */
	public interface IUndoManager {

		/**
		 * <p> The maximum number of undoable or redoable operations to track. </p>
		 * <p>To disable the undo function, set this value to 0.</p>
		 * 
		 * @return 
		 */
		function get undoAndRedoItemLimit():int;

		/**
		 * @param value
		 * @return 
		 */
		function set undoAndRedoItemLimit(value:int):void;

		/**
		 * <p> Indicates whether there is currently an operation that can be redone. </p>
		 * 
		 * @return  — Boolean , if there is an operation on the redo stack that can be redone. Otherwise, . 
		 */
		function canRedo():Boolean;

		/**
		 * <p> Indicates whether there is currently an operation that can be undone. </p>
		 * 
		 * @return  — Boolean , if there is an operation on the undo stack that can be reversed. Otherwise, . 
		 */
		function canUndo():Boolean;

		/**
		 * <p> Clears both the undo and the redo histories. </p>
		 */
		function clearAll():void;

		/**
		 * <p> Clears the redo stack. </p>
		 */
		function clearRedo():void;

		/**
		 * <p> Returns the next operation to be redone. </p>
		 * 
		 * @return  — The redoable IOperation object, or , if no redoable operation is on the stack. 
		 */
		function peekRedo():IOperation;

		/**
		 * <p> Returns the next operation to be undone. </p>
		 * 
		 * @return  — The undoable IOperation object, or , if no undoable operation is on the stack. 
		 */
		function peekUndo():IOperation;

		/**
		 * <p> Removes the next operation to be redone from the redo stack, and returns it. </p>
		 * 
		 * @return  — The redoable IOperation object, or , if no redoable operation is on the stack. 
		 */
		function popRedo():IOperation;

		/**
		 * <p> Removes the next operation to be undone from the undo stack, and returns it. </p>
		 * 
		 * @return  — The undoable IOperation object, or , if no undoable operation is on the stack. 
		 */
		function popUndo():IOperation;

		/**
		 * <p> Adds a redoable operation to the redo stack. </p>
		 * 
		 * @param operation
		 */
		function pushRedo(operation:IOperation):void;

		/**
		 * <p> Adds an undoable operation to the undo stack. </p>
		 * 
		 * @param operation
		 */
		function pushUndo(operation:IOperation):void;

		function redo():void;

		function undo():void;
	}
}
