package flashx.undo {
	/**
	 *  The UndoManager class manages the history of editing operations on a text flow so that these operations can be undone and redone. <p>The undo manager maintains two stacks of IOperation objects. When a reversible operation is executed, it is placed on the undo stack. If that operation is undone, it is removed from the undo stack, reversed, and placed on the redo stack. Likewise, if that operation is then redone, it is removed from the redo stack, re-executed, and then placed onto the undo stack again. If another operation is executed first, the redo stack is cleared.</p> <p>If the TextFlow is modified directly (not via calls to the edit manager, but directly via calls to the managed FlowElement objects), then the edit manager clears the undo stack to prevent the stack from getting out of sync with the current state.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../flashx/textLayout/edit/EditManager.html" target="">flashx.textLayout.edit.EditManager</a>
	 * </div><br><hr>
	 */
	public class UndoManager implements IUndoManager {
		private var _undoAndRedoItemLimit:int;

		/**
		 * <p> Creates an UndoManager object. </p>
		 */
		public function UndoManager() {
			throw new Error("Not implemented");
		}

		/**
		 * <p> The maximum number of undoable or redoable operations to track. </p>
		 * <p>To disable the undo function, set this value to 0.</p>
		 * 
		 * @return 
		 */
		public function get undoAndRedoItemLimit():int {
			return _undoAndRedoItemLimit;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set undoAndRedoItemLimit(value:int):void {
			_undoAndRedoItemLimit = value;
		}

		/**
		 * <p> Indicates whether there is currently an operation that can be redone. </p>
		 * 
		 * @return  — Boolean , if there is an operation on the redo stack that can be redone. Otherwise, . 
		 */
		public function canRedo():Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Indicates whether there is currently an operation that can be undone. </p>
		 * 
		 * @return  — Boolean , if there is an operation on the undo stack that can be reversed. Otherwise, . 
		 */
		public function canUndo():Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Clears both the undo and the redo histories. </p>
		 */
		public function clearAll():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Clears the redo stack. </p>
		 */
		public function clearRedo():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the next operation to be redone. </p>
		 * 
		 * @return  — The redoable IOperation object, or , if no redoable operation is on the stack. 
		 */
		public function peekRedo():IOperation {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the next operation to be undone. </p>
		 * 
		 * @return  — The undoable IOperation object, or , if no undoable operation is on the stack. 
		 */
		public function peekUndo():IOperation {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Removes the next operation to be redone from the redo stack, and returns it. </p>
		 * 
		 * @return  — The redoable IOperation object, or , if no redoable operation is on the stack. 
		 */
		public function popRedo():IOperation {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Removes the next operation to be undone from the undo stack, and returns it. </p>
		 * 
		 * @return  — The undoable IOperation object, or , if no undoable operation is on the stack. 
		 */
		public function popUndo():IOperation {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Adds a redoable operation to the redo stack. </p>
		 * 
		 * @param operation
		 */
		public function pushRedo(operation:IOperation):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Adds an undoable operation to the undo stack. </p>
		 * 
		 * @param operation
		 */
		public function pushUndo(operation:IOperation):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Removes the next IOperation object from the redo stack and calls the performRedo() function of that object. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flashx/textLayout/edit/IEditManager.html#redo()" target="">flashx.textLayout.edit.IEditManager.redo()</a>
		 * </div>
		 */
		public function redo():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Removes the next IOperation object from the undo stack and calls the performUndo() function of that object. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flashx/textLayout/edit/IEditManager.html#undo()" target="">flashx.textLayout.edit.IEditManager.undo()</a>
		 * </div>
		 */
		public function undo():void {
			throw new Error("Not implemented");
		}
	}
}
