package flashx.textLayout.edit {
	import flashx.textLayout.elements.TextFlow;
	import flashx.textLayout.elements.TextRange;
	import flashx.textLayout.formats.ITextLayoutFormat;

	/**
	 *  The SelectionState class represents a selection in a text flow. <p>A selection range has an anchor point, representing the point at which the selection of text began, and an active point, representing the point to which the selection is extended. The active point can be before or after the anchor point in the text. If a selection is modified (for example, by a user shift-clicking with the mouse), the active point changes while the anchor point always remains in the same position.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="ISelectionManager.html#getSelectionState()" target="">flashx.textLayout.edit.ISelectionManager.getSelectionState()</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/elements/TextFlow.html" target="">flashx.textLayout.elements.TextFlow</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/elements/TextRange.html" target="">flashx.textLayout.elements.TextRange</a>
	 * </div><br><hr>
	 */
	public class SelectionState extends TextRange {
		private var _pointFormat:ITextLayoutFormat;

		/**
		 * <p> Creates a SelectionState object. </p>
		 * <p><b>Note:</b> Do not construct a SelectionState object in order to create a selection. To create a selection in a text flow, call the <code>setSelection()</code> method of the relevant ISelectionManager instance (which is the SelectionManager or EditManager object assigned to the <code>interactionManager</code> property of the text flow).</p>
		 * 
		 * @param root  — The TextFlow associated with the selection. 
		 * @param anchorPosition  — The anchor index of the selection. 
		 * @param activePosition  — The active index of the selection. 
		 * @param format  — The TextLayoutFormat to be applied on next character typed when a point selection 
		 */
		public function SelectionState(root:TextFlow, anchorPosition:int, activePosition:int, format:ITextLayoutFormat = null) {
			super(root, anchorPosition, activePosition);
			this.pointFormat = format;
		}

		/**
		 * <p> The format attributes applied to inserted text. </p>
		 * <p><b>Note:</b> The <code>pointFormat</code> object does not include inherited styles. To get all the applicable style definitions, use the <code>getCommonCharacterFormat()</code> method of the ISelectionManager class.</p>
		 * 
		 * @return 
		 */
		public function get pointFormat():ITextLayoutFormat {
			return _pointFormat;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set pointFormat(value:ITextLayoutFormat):void {
			_pointFormat = value;
		}

		/**
		 * @param newAnchorPosition  — the anchor index of the selection. 
		 * @param newActivePosition  — the active index of the selection. 
		 * @return  — true if selection is changed 
		 */
		override public function updateRange(newAnchorPosition:int, newActivePosition:int):Boolean {
			throw new Error("Not implemented");
		}
	}
}
