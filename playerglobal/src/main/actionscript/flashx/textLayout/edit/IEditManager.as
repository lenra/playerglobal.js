package flashx.textLayout.edit {
	import flashx.textLayout.elements.DivElement;
	import flashx.textLayout.elements.FlowElement;
	import flashx.textLayout.elements.FlowGroupElement;
	import flashx.textLayout.elements.InlineGraphicElement;
	import flashx.textLayout.elements.LinkElement;
	import flashx.textLayout.elements.ListElement;
	import flashx.textLayout.elements.ParagraphElement;
	import flashx.textLayout.elements.SubParagraphGroupElement;
	import flashx.textLayout.elements.TCYElement;
	import flashx.textLayout.formats.ITextLayoutFormat;
	import flashx.textLayout.operations.FlowOperation;
	import flashx.undo.IUndoManager;

	/**
	 *  IEditManager defines the interface for handling edit operations of a text flow. <p>To enable text flow editing, assign an IEditManager instance to the <code>interactionManager</code> property of the TextFlow object. The edit manager handles changes to the text (such as insertions, deletions, and format changes). Changes are reversible if the edit manager has an undo manager. The edit manager triggers the recomposition and display of the text flow, as necessary.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="EditManager.html" target="">EditManager</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/elements/TextFlow.html" target="">flashx.textLayout.elements.TextFlow</a>
	 *  <br>
	 *  <a href="../../../flashx/undo/UndoManager.html" target="">flashx.undo.UndoManager</a>
	 * </div><br><hr>
	 */
	public interface IEditManager {

		/**
		 * <p> Controls whether operations can be queued up for later execution. </p>
		 * <p>Execution of some operations might be delayed as a performance optimization. For example, it is convenient to be able to combine multiple keystrokes into a single insert operation. If <code>allowDelayedOperations</code> is <code>true</code>, then operations may be queued up. If <code>false</code>, all operations are executed immediately. By default, it is <code>true</code>.</p>
		 * 
		 * @return 
		 */
		function get allowDelayedOperations():Boolean;

		/**
		 * @param value
		 * @return 
		 */
		function set allowDelayedOperations(value:Boolean):void;

		/**
		 * <p> By default, calls into IEditManager handle updates synchronously, so the requested change is made and the text recomposed and added to the display list within the IEditManager method. To get a delayed redraw, set <code>delayUpdates</code> to <code>true</code>. This causes the IEditManager to only update the model, and recompose and redraw on the next <code>enter_frame</code> event. </p>
		 * 
		 * @return 
		 */
		function get delayUpdates():Boolean;

		/**
		 * @param value
		 * @return 
		 */
		function set delayUpdates(value:Boolean):void;

		/**
		 * <p> The UndoManager object assigned to this EditManager instance, if there is one. </p>
		 * <p>An undo manager handles undo and redo operations.</p>
		 * 
		 * @return 
		 */
		function get undoManager():IUndoManager;

		/**
		 * <p> Applies container styles to any containers in the selection. </p>
		 * <p>Any style properties in the format object that are <code>null</code> are left unchanged.</p>
		 * 
		 * @param format  — The format to apply to the containers in the range 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 */
		function applyContainerFormat(format:ITextLayoutFormat, operationState:SelectionState = null):void;

		/**
		 * <p> Changes the formats of the specified (or current) selection. </p>
		 * <p>Executes an undoable operation that applies the new formats. Only style attributes set for the TextLayoutFormat objects are applied. Undefined attributes in the format objects are not changed. </p>
		 * 
		 * @param leafFormat  — The format to apply to leaf elements such as spans and inline graphics. 
		 * @param paragraphFormat  — The format to apply to paragraph elements. 
		 * @param containerFormat  — The format to apply to the containers. 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 */
		function applyFormat(leafFormat:ITextLayoutFormat, paragraphFormat:ITextLayoutFormat, containerFormat:ITextLayoutFormat, operationState:SelectionState = null):void;

		/**
		 * <p> Applies styles to the specified element. </p>
		 * <p>Any style properties in the format object that are <code>null</code> are left unchanged. Only styles that are relevant to the specified element are applied.</p>
		 * 
		 * @param targetElement  — The element to which the styles are applied. 
		 * @param format  — The format containing the styles to apply. 
		 * @param relativeStart  — An offset from the beginning of the element at which to split the element when assigning the new formatting. 
		 * @param relativeEnd  — An offset from the beginning of the element at which to split the element when applying the new formatting. 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 */
		function applyFormatToElement(targetElement:FlowElement, format:ITextLayoutFormat, relativeStart:int = 0, relativeEnd:int = -1, operationState:SelectionState = null):void;

		/**
		 * <p> Changes the format applied to the leaf elements in the specified (or current) selection. </p>
		 * <p>Executes an undoable operation that applies the new format to leaf elements such as SpanElement and InlineGraphicElement objects. Only style attributes set for the TextLayoutFormat objects are applied. Undefined attributes in the format object are changed.</p>
		 * 
		 * @param format  — The format to apply. 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 */
		function applyLeafFormat(format:ITextLayoutFormat, operationState:SelectionState = null):void;

		/**
		 * <p> Transforms a selection into a link, or a link into normal text. </p>
		 * <p>Executes an undoable operation that creates or removes the link.</p>
		 * <p>If a <code>target</code> parameter is specified, it must be one of the following values:</p>
		 * <ul>
		 *  <li>"_self"</li>
		 *  <li>"_blank"</li>
		 *  <li>"_parent"</li>
		 *  <li>"_top"</li>
		 * </ul>
		 * <p>In browser-hosted runtimes, a target of "_self" replaces the current html page. So, if the SWF content containing the link is in a page within a frame or frameset, the linked content loads within that frame. If the page is at the top level, the linked content opens to replace the original page. A target of "_blank" opens a new browser window with no name. A target of "_parent" replaces the parent of the html page containing the SWF content. A target of "_top" replaces the top-level page in the current browser window.</p>
		 * <p>In other runtimes, such as Adobe AIR, the link opens in the user's default browser and the <code>target</code> parameter is ignored.</p>
		 * <p>The <code>extendToLinkBoundary</code> parameter determines how the edit manager treats a selection that intersects with one or more existing links. If the parameter is <code>true</code>, then the operation is applied as a unit to the selection and the whole text of the existing links. Thus, a single link is created that spans from the beginning of the first link intersected to the end of the last link intersected. In contrast, if <code>extendToLinkBoundary</code> were <code>false</code> in this situation, the existing partially selected links would be split into two links.</p>
		 * 
		 * @param href  — The uri referenced by the link. 
		 * @param target  — The target browser window of the link. 
		 * @param extendToLinkBoundary  — Specifies whether to consolidate selection with any overlapping existing links, and then apply the change. 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 * @return  — The LinkElement that was created. 
		 */
		function applyLink(href:String, target:String = null, extendToLinkBoundary:Boolean = false, operationState:SelectionState = null):LinkElement;

		/**
		 * <p> Applies paragraph styles to any paragraphs in the selection. </p>
		 * <p>Any style properties in the format object that are <code>null</code> are left unchanged.</p>
		 * 
		 * @param format  — The format to apply to the selected paragraphs. 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 */
		function applyParagraphFormat(format:ITextLayoutFormat, operationState:SelectionState = null):void;

		/**
		 * <p> Transforms text into a TCY run, or a TCY run into non-TCY text. </p>
		 * <p>TCY, or tate-chu-yoko, causes text to draw horizontally within a vertical line, and is used to make small blocks of non-Japanese text or numbers, such as dates, more readable in vertical text.</p>
		 * 
		 * @param tcyOn  — Set to <code>true</code> to apply TCY to a text range, <code>false</code> to remove TCY. 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 * @return  — The TCYElement that was created. 
		 */
		function applyTCY(tcyOn:Boolean, operationState:SelectionState = null):TCYElement;

		/**
		 * <p> Begins a new group of operations. </p>
		 * <p>All operations executed after the call to <code>beginCompositeOperation()</code>, and before the matching call to <code>endCompositeOperation()</code> are executed and grouped together as a single operation that can be undone as a unit.</p>
		 * <p>A <code>beginCompositeOperation</code>/<code>endCompositeOperation</code> block can be nested inside another <code>beginCompositeOperation</code>/<code>endCompositeOperation</code> block.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  flashx.textLayout.edit.IEditManager.endCompositeOperation
		 * </div>
		 * <span id="pageFilter"><br><span class="label"> Example &nbsp;( <span class="usage"><a href="http://www.adobe.com/go/learn_as3_usingexamples_en"> How to use this example </a></span>) </span><br><br>
		 *  <div class="detailBody">
		 *    The following example defines a function that inserts a graphic object in its own paragraph. If the 
		 *   <code>beginCompositeOperation()</code> and 
		 *   <code>endCompositeOperation()</code> functions were not used, then each of the suboperations would have to be undone separately rather than as a group. 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>
		 * package flashx.textLayout.edit.examples
		 * {
		 *     import flashx.textLayout.edit.IEditManager;
		 *     import flashx.textLayout.edit.SelectionState;
		 *     import flashx.textLayout.elements.TextFlow;
		 *     
		 *     public class EditManager_beginCompositeOperation
		 *     {
		 *         static public function insertGraphic( source:Object, width:Object, height:Object, float:String, selection:SelectionState ):void
		 *         {
		 *             var editManager:IEditManager = selection.textFlow.interactionManager as IEditManager;
		 *             
		 *             editManager.beginCompositeOperation();
		 *             
		 *             editManager.deleteText( selection );
		 *             var changedSelection:SelectionState = 
		 *                 new SelectionState( selection.textFlow, selection.anchorPosition, selection.anchorPosition );
		 *             editManager.splitParagraph( changedSelection );
		 *             changedSelection = 
		 *                 new SelectionState( changedSelection.textFlow, changedSelection.anchorPosition + 1, changedSelection.anchorPosition + 1);
		 *             editManager.insertInlineGraphic( source, width, height, float, changedSelection );
		 *             changedSelection = 
		 *                 new SelectionState( changedSelection.textFlow, changedSelection.anchorPosition + 1, changedSelection.anchorPosition + 1);
		 *             editManager.splitParagraph( changedSelection );
		 *             
		 *             editManager.endCompositeOperation();                
		 *         }
		 *     }
		 * }
		 * </pre>
		 *   </div>
		 *  </div></span>
		 */
		function beginCompositeOperation():void;

		/**
		 * <p> Changes the ID of an element. </p>
		 * <p>If the <code>relativeStart</code> or <code>relativeEnd</code> parameters are set (to anything other than the default values), then the element is split. The parts of the element outside this range retain the original ID. Setting both the <code>relativeStart</code> and <code>relativeEnd</code> parameters creates elements with duplicate IDs.</p>
		 * 
		 * @param newID  — The new ID value. 
		 * @param targetElement  — The element to modify. 
		 * @param relativeStart  — An offset from the beginning of the element at which to split the element when assigning the new ID. 
		 * @param relativeEnd  — An offset from the beginning of the element at which to split the element when assigning the new ID. 
		 * @param operationState  — Specifies the selection to restore when undoing this operation; if <code>null</code>, the operation saves the current selection. 
		 */
		function changeElementID(newID:String, targetElement:FlowElement, relativeStart:int = 0, relativeEnd:int = -1, operationState:SelectionState = null):void;

		/**
		 * <p> Changes the styleName of an element or part of an element. </p>
		 * <p>If the <code>relativeStart</code> or <code>relativeEnd</code> parameters are set (to anything other than the default values), then the element is split. The parts of the element outside this range retain the original style.</p>
		 * 
		 * @param newName  — The name of the new style. 
		 * @param targetElement  — Specifies the element to change. 
		 * @param relativeStart  — An offset from the beginning of the element at which to split the element when assigning the new style. 
		 * @param relativeEnd  — An offset from the end of the element at which to split the element when assigning the new style. 
		 * @param operationState  — Specifies the selection to restore when undoing this operation; if <code>null</code>, the operation saves the current selection. 
		 */
		function changeStyleName(newName:String, targetElement:FlowElement, relativeStart:int = 0, relativeEnd:int = -1, operationState:SelectionState = null):void;

		/**
		 * <p> Changes the typeName of an element or part of an element. </p>
		 * <p>If the <code>relativeStart</code> or <code>relativeEnd</code> parameters are set (to anything other than the default values), then the element is split. The parts of the element outside this range retain the original style.</p>
		 * 
		 * @param newName  — The name of the new type. 
		 * @param targetElement  — Specifies the element to change. 
		 * @param relativeStart  — An offset from the beginning of the element at which to split the element when assigning the new style 
		 * @param relativeEnd  — An offset from the end of the element at which to split the element when assigning the new style 
		 * @param operationState  — Specifies the selection to restore when undoing this operation; if <code>null</code>, the operation saves the current selection. 
		 */
		function changeTypeName(newName:String, targetElement:FlowElement, relativeStart:int = 0, relativeEnd:int = -1, operationState:SelectionState = null):void;

		/**
		 * <p> Undefines formats of the specified (or current) selection. </p>
		 * <p>Executes an undoable operation that undefines the specified formats. Only style attributes set for the TextLayoutFormat objects are applied. Undefined attributes in the format objects are not changed. </p>
		 * 
		 * @param leafFormat  — The format whose set values indicate properties to undefine to LeafFlowElement objects in the selected range. 
		 * @param paragraphFormat  — The format whose set values indicate properties to undefine to ParagraphElement objects in the selected range. 
		 * @param containerFormat  — The format whose set values indicate properties to undefine to ContainerController objects in the selected range. 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 */
		function clearFormat(leafFormat:ITextLayoutFormat, paragraphFormat:ITextLayoutFormat, containerFormat:ITextLayoutFormat, operationState:SelectionState = null):void;

		/**
		 * <p> Undefines styles to the specified element. </p>
		 * <p>Any style properties in the format object that are <code>undefined</code> are left unchanged. Any styles that are defined in the specififed format are undefined on the specified element.</p>
		 * 
		 * @param targetElement  — The element to which the styles are applied. 
		 * @param format  — The format containing the styles to undefine. 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 */
		function clearFormatOnElement(targetElement:FlowElement, format:ITextLayoutFormat, operationState:SelectionState = null):void;

		/**
		 * <p> Creates a new DivElement that contains the entire range specified in the operationState at the lowest common parent element that contains both the start and end points of the range. If the start and end points are the same, a new DivElement is created at that position with a single child paragraph. </p>
		 * 
		 * @param parent  — Specifies a parent element for the new DivElement. If <code>null</code> the new parent will be lowest level that contains the SelectionState. 
		 * @param format  — Formatting attributes to apply to the new DivElement. 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 * @return  — The new DivElement that was created. 
		 */
		function createDiv(parent:FlowGroupElement = null, format:ITextLayoutFormat = null, operationState:SelectionState = null):DivElement;

		/**
		 * <p> Creates a new ListElement that contains the entire range specified in the operationState at the lowest common parent element that contains both the start and end points of the range. Each paragraph within the range will become a ListItemElement in the new ListElement. If the start and end points are the same, a new ListElement is created at that position with a single ListItemElement child. </p>
		 * 
		 * @param parent  — Optionally specifies a parent element for the new ListElement. If <code>null</code> the new parent will be lowest level that contains the SelectionState. 
		 * @param format  — Formatting attributes to apply to the new ListElement. 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 * @return  — The new ListElement that was created. 
		 */
		function createList(parent:FlowGroupElement = null, format:ITextLayoutFormat = null, operationState:SelectionState = null):ListElement;

		/**
		 * <p> Creates a new SubParagraphGroupElement that contains the entire range specified in the operationState at the lowest common parent element that contains both the start and end points of the range. If the start and end points are the same, nothing is done. </p>
		 * 
		 * @param parent  — Specifies a parent element for the new SubParagraphGroupElement element. If <code>null</code> the new parent will be lowest level that contains the SelectionState. 
		 * @param format  — Formatting attributes to apply to the new SubParagraphGroupElement 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 * @return  — The new SubParagraphGroupElement that was created. 
		 */
		function createSubParagraphGroup(parent:FlowGroupElement = null, format:ITextLayoutFormat = null, operationState:SelectionState = null):SubParagraphGroupElement;

		/**
		 * <p> Deletes the selected area and returns the deleted area in a TextScrap object. </p>
		 * <p>The resulting TextScrap can be posted to the system clipboard or used in a subsequent <code>pasteTextOperation()</code> operation.</p>
		 * 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 * @return  — The TextScrap that was cut. 
		 */
		function cutTextScrap(operationState:SelectionState = null):TextScrap;

		/**
		 * <p> Deletes a range of text, or, if a point selection is given, deletes the next character. </p>
		 * 
		 * @param operationState  — specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 */
		function deleteNextCharacter(operationState:SelectionState = null):void;

		/**
		 * <p> Deletes the next word. </p>
		 * <p>If a range is selected, the first word of the range is deleted.</p>
		 * 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 */
		function deleteNextWord(operationState:SelectionState = null):void;

		/**
		 * <p> Deletes a range of text, or, if a point selection is given, deletes the previous character. </p>
		 * 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 */
		function deletePreviousCharacter(operationState:SelectionState = null):void;

		/**
		 * <p> Deletes the previous word. </p>
		 * <p>If a range is selected, the first word of the range is deleted.</p>
		 * 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 */
		function deletePreviousWord(operationState:SelectionState = null):void;

		/**
		 * <p> Deletes a range of text. </p>
		 * 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 */
		function deleteText(operationState:SelectionState = null):void;

		/**
		 * <p> Executes a FlowOperation. </p>
		 * <p>The <code>doOperation()</code> method is called by IEditManager functions that update the text flow. You do not typically need to call this function directly unless you create your own custom operations.</p>
		 * <p>This function proceeds in the following steps:</p>
		 * <ol>
		 *  <li>Flush any pending operations before performing this operation.</li>
		 *  <li>Send a cancelable flowOperationBegin event. If canceled this method returns immediately.</li>
		 *  <li>Execute the operation. The operation returns <code>true</code> or <code>false</code>. <code>False</code> indicates that no changes were made.</li>
		 *  <li>Push the operation onto the undo stack.</li>
		 *  <li>Clear the redo stack.</li>
		 *  <li>Update the display.</li>
		 *  <li>Send a cancelable flowOperationEnd event.</li>
		 * </ol>
		 * <p>Exception handling: If the operation throws an exception, it is caught and the error is attached to the flowOperationEnd event. If the event is not canceled the error is rethrown.</p>
		 * 
		 * @param operation  — a FlowOperation object 
		 */
		function doOperation(operation:FlowOperation):void;

		/**
		 * <p> Ends a group of operations. </p>
		 * <p>All operations executed since the last call to <code>beginCompositeOperation()</code> are grouped as a CompositeOperation that is then completed. This CompositeOperation object is added to the undo stack or, if this composite operation is nested inside another composite operation, added to the parent operation.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  flashx.textLayout.edit.IEditManager.beginCompositeOperation
		 * </div>
		 */
		function endCompositeOperation():void;

		/**
		 * <p> Inserts an image. </p>
		 * <p>The source of the image can be a string containing a URI, URLRequest object, a Class object representing an embedded asset, or a DisplayObject instance.</p>
		 * <p>The width and height values can be the number of pixels, a percent, or the string, 'auto', in which case the actual dimension of the graphic is used.</p>
		 * <p>Set the <code>float</code> to one of the constants defined in the Float class to specify whether the image should be displayed to the left or right of any text or inline with the text.</p>
		 * 
		 * @param source  — Can be either a String interpreted as a uri, a Class interpreted as the class of an Embed DisplayObject, a DisplayObject instance or a URLRequest. 
		 * @param width  — The width of the image to insert (number, percent, or 'auto'). 
		 * @param height  — The height of the image to insert (number, percent, or 'auto'). 
		 * @param options  — None supported. 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 * @return 
		 */
		function insertInlineGraphic(source:Object, width:Object, height:Object, options:Object = null, operationState:SelectionState = null):InlineGraphicElement;

		/**
		 * <p> Inserts text. </p>
		 * <p>Inserts the text at a position or range in the text. If the location supplied in the <code>operationState</code> parameter is a range (or the parameter is <code>null</code> and the current selection is a range), then the text currently in the range is replaced by the inserted text.</p>
		 * 
		 * @param text  — The string to insert. 
		 * @param operationState  — Specifies the text in the flow to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 */
		function insertText(text:String, operationState:SelectionState = null):void;

		/**
		 * <p> Modifies an existing inline graphic. </p>
		 * <p>Set unchanging properties to the values in the original graphic. (Modifying an existing graphic object is typically more efficient than deleting and recreating one.)</p>
		 * 
		 * @param source  — Can be either a String interpreted as a uri, a Class interpreted as the class of an Embed DisplayObject, a DisplayObject instance or a URLRequest. 
		 * @param width  — The new width for the image (number or percent). 
		 * @param height  — The new height for the image (number or percent). 
		 * @param options  — None supported. 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 */
		function modifyInlineGraphic(source:Object, width:Object, height:Object, options:Object = null, operationState:SelectionState = null):void;

		/**
		 * <p> Move a set of FlowElements from one FlowGroupElement to another. The desinationElement must be a legal parent type for the children being moved, or an exception is thrown. </p>
		 * 
		 * @param source  — The orginal parent of the elements to be moved. 
		 * @param sourceIndex  — The child index within the source of the first element to be moved. 
		 * @param numChildren  — The number of children being moved. 
		 * @param destination  — The new parent of elements after move. 
		 * @param destinationIndex  — The child index within the destination to where elements are moved to. 
		 * @param operationState  — Specifies the text to which this operation applies, and to which selection returns to upon undo. If <code>null</code>, the operation applies to the current selection. If there is no current selection, this parameter must be non-null. 
		 */
		function moveChildren(source:FlowGroupElement, sourceIndex:int, numChildren:int, destination:FlowGroupElement, destinationIndex:int, operationState:SelectionState = null):void;

		/**
		 * <p> Overwrites the selected text. </p>
		 * <p>If the selection is a point selection, the first character is overwritten by the new text.</p>
		 * 
		 * @param text  — The string to insert. 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 */
		function overwriteText(text:String, operationState:SelectionState = null):void;

		/**
		 * <p> Pastes the TextScrap into the selected area. </p>
		 * <p>If a range of text is specified, the text in the range is deleted.</p>
		 * 
		 * @param scrapToPaste  — The TextScrap to paste. 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 */
		function pasteTextScrap(scrapToPaste:TextScrap, operationState:SelectionState = null):void;

		/**
		 * <p> Reperforms the previous undone operation. </p>
		 * <p><b>Note:</b> If the IUndoManager associated with this IEditManager is also associated with another IEditManager, then it is possible that the redo operation associated with the other IEditManager is the one redone. This can happen if the FlowOperation of another IEditManager is on top of the redo stack.</p>
		 * <p>This function does nothing if undo is not turned on.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../../flashx/undo/IUndoManager.html#redo()" target="">flashx.undo.IUndoManager.redo()</a>
		 * </div>
		 * <span id="pageFilter"><br><span class="label"> Example &nbsp;( <span class="usage"><a href="http://www.adobe.com/go/learn_as3_usingexamples_en"> How to use this example </a></span>) </span><br><br>
		 *  <div class="detailBody">
		 *    The following example defines a function that reperforms the last undone operation on a text flow: 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>
		 * package flashx.textLayout.edit.examples
		 * {
		 *     import flash.display.Sprite;
		 *     
		 *     import flashx.textLayout.edit.IEditManager;
		 *     import flashx.textLayout.elements.TextFlow;
		 *     
		 *     public class EditManager_redo
		 *     {
		 *         static public function redo( textFlow:TextFlow ):void
		 *         {
		 *             if( textFlow.interactionManager is IEditManager )
		 *             {
		 *                 IEditManager( textFlow.interactionManager ).redo();
		 *             }
		 *         }
		 *     }
		 * }
		 * </pre>
		 *   </div>
		 *  </div></span>
		 */
		function redo():void;

		/**
		 * <p> Splits the target element at the location specified, creating a new element after the current one. If the operationState is a range, the text within the range is deleted. The new element is created after the text position specified by operationState. Note that splitting a SubParagraphGroupElement will have no effect because they will automatically remerge with the adejacent elements. </p>
		 * <p>An example where you might want to use this is if you have a list, and you want to divide it into two lists.</p>
		 * 
		 * @param target  — The element to be split. 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 * @return  — The new paragraph that was created. 
		 */
		function splitElement(target:FlowGroupElement, operationState:SelectionState = null):FlowGroupElement;

		/**
		 * <p> Splits the paragraph at the current position, creating a new paragraph after the current one. </p>
		 * <p>If a range of text is specified, the text in the range is deleted.</p>
		 * 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 * @return  — The new paragraph that was created. 
		 */
		function splitParagraph(operationState:SelectionState = null):ParagraphElement;

		/**
		 * <p> Reverses the previous operation. </p>
		 * <p><b>Note:</b> If the IUndoManager associated with this IEditManager is also associated with another IEditManager, then it is possible that the undo operation associated with the other IEditManager is the one undone. This can happen if the FlowOperation of another IEditManager is on top of the undo stack.</p>
		 * <p>This function does nothing if undo is not turned on.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../../flashx/undo/IUndoManager.html#undo()" target="">flashx.undo.IUndoManager.undo()</a>
		 * </div>
		 * <span id="pageFilter"><br><span class="label"> Example &nbsp;( <span class="usage"><a href="http://www.adobe.com/go/learn_as3_usingexamples_en"> How to use this example </a></span>) </span><br><br>
		 *  <div class="detailBody">
		 *    The following example defines a function that undoes the last operation on a text flow: 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>
		 * package flashx.textLayout.edit.examples
		 * {
		 *     import flashx.textLayout.edit.IEditManager;
		 *     import flashx.textLayout.elements.TextFlow;
		 *     
		 *     public class EditManager_undo
		 *     {
		 *         static public function undo( textFlow:TextFlow ):void
		 *         {
		 *             if( textFlow.interactionManager is IEditManager )
		 *             {
		 *                 IEditManager( textFlow.interactionManager ).undo();
		 *             }
		 *         }
		 *     }
		 * }
		 * </pre>
		 *   </div>
		 *  </div></span>
		 */
		function undo():void;

		/**
		 * <p> Updates the display after an operation has modified it. Normally this is handled automatically, but call this method if <code>delayUpdates</code> is on, and the display should be updated before the next <code>enter_frame</code> event. </p>
		 */
		function updateAllControllers():void;
	}
}
