package flashx.textLayout.edit {
	import flashx.textLayout.elements.DivElement;
	import flashx.textLayout.elements.FlowElement;
	import flashx.textLayout.elements.FlowGroupElement;
	import flashx.textLayout.elements.InlineGraphicElement;
	import flashx.textLayout.elements.LinkElement;
	import flashx.textLayout.elements.ListElement;
	import flashx.textLayout.elements.ParagraphElement;
	import flashx.textLayout.elements.SubParagraphGroupElement;
	import flashx.textLayout.elements.TCYElement;
	import flashx.textLayout.formats.ITextLayoutFormat;
	import flashx.textLayout.operations.FlowOperation;
	import flashx.undo.IUndoManager;

	/**
	 *  The EditManager class manages editing changes to a TextFlow. <p>To enable text flow editing, assign an EditManager object to the <code>interactionManager</code> property of the TextFlow object. The edit manager handles changes to the text (such as insertions, deletions, and format changes). Changes are reversible if the edit manager has an undo manager. The edit manager triggers the recomposition and display of the text flow, as necessary.</p> <p>The EditManager class supports the following keyboard shortcuts:</p> <table class="innertable">
	 *  <tbody>
	 *   <tr>
	 *    <th>Keys</th>
	 *    <th>Result</th>
	 *   </tr>
	 *   <tr>
	 *    <td>ctrl-z</td>
	 *    <td>undo</td>
	 *   </tr>
	 *   <tr>
	 *    <td>ctrl-y</td>
	 *    <td>redo</td>
	 *   </tr>
	 *   <tr>
	 *    <td>ctrl-backspace</td>
	 *    <td>deletePreviousWord</td>
	 *   </tr>
	 *   <tr>
	 *    <td>ctrl-delete</td>
	 *    <td>deleteNextWord</td>
	 *   </tr>
	 *   <tr>
	 *    <td>alt+delete</td>
	 *    <td>deleteNextWord</td>
	 *   </tr>
	 *   <tr>
	 *    <td>ctrl+alt-delete</td>
	 *    <td>deleteNextWord</td>
	 *   </tr>
	 *   <tr>
	 *    <td>ctrl-shift-hyphen</td>
	 *    <td>insert discretionary hyphen</td>
	 *   </tr>
	 *   <tr>
	 *    <td>ctrl+backspace</td>
	 *    <td>deletePreviousWord</td>
	 *   </tr>
	 *   <tr>
	 *    <td>alt+backspace</td>
	 *    <td>deletePreviousWord</td>
	 *   </tr>
	 *   <tr>
	 *    <td>ctrl+alt-backspace</td>
	 *    <td>deletePreviousWord</td>
	 *   </tr>
	 *   <tr>
	 *    <td>INSERT</td>
	 *    <td>toggles overWriteMode</td>
	 *   </tr>
	 *   <tr>
	 *    <td>backspace</td>
	 *    <td>deletePreviousCharacter</td>
	 *   </tr>
	 *   <tr>
	 *    <td>ENTER</td>
	 *    <td>if textFlow.configuration.manageEnterKey in a list it creates a new list item, otherwise creates a new paragraph</td>
	 *   </tr>
	 *   <tr>
	 *    <td>shift-ENTER</td>
	 *    <td>if textFlow.configuration.manageEnterKey creates a new paragraph</td>
	 *   </tr>
	 *   <tr>
	 *    <td>TAB</td>
	 *    <td>if textFlow.configuration.manageTabKey in a list it creates nested list, otherwise inserts a TAB or overwrites next character with a TAB</td>
	 *   </tr>
	 *   <tr>
	 *    <td>shift-TAB</td>
	 *    <td>if textFlow.configuration.manageTabKey in the first item of a list it moves the item out of the list (promotes it)</td>
	 *   </tr>
	 *  </tbody>
	 * </table> <p> <b>Note:</b> The following keys do not work on Windows: alt-backspace, alt-delete, ctrl+alt-backspace, and ctrl+alt-delete. These keys do not generate an event for the runtime.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flashx/textLayout/elements/TextFlow.html" target="">flashx.textLayout.elements.TextFlow</a>
	 *  <br>
	 *  <a href="../../../flashx/undo/UndoManager.html" target="">flashx.undo.UndoManager</a>
	 * </div><br><hr>
	 */
	public class EditManager extends SelectionManager implements IEditManager {
		private static var _overwriteMode:Boolean;

		private var _allowDelayedOperations:Boolean;
		private var _delayUpdates:Boolean;

		private var _undoManager:IUndoManager;

		/**
		 * <p> Creates an EditManager object. </p>
		 * <p>Assign an EditManager object to the <code>interactionManager</code> property of a text flow to enable editing of that text flow. </p>
		 * <p>To enable support for undoing and redoing changes, pass an IUndoManager instance to the EditManager constructor. You can use the <code>flashx.undo.UndoManager</code> class or create a custom IUndoManager instance. Use a custom IUndoManager instance to integrate Text Layout Framework changes with an existing undo manager that is not an instance of the UndoManager class. To create a custom IUndoManager instance, ensure that the class you use to define the undo manager implements the IUndoManager interface.</p>
		 * 
		 * @param undoManager  — The UndoManager for the application 
		 */
		public function EditManager(undoManager:IUndoManager = null) {
			throw new Error("Not implemented");
		}

		/**
		 * @return 
		 */
		public function get allowDelayedOperations():Boolean {
			return _allowDelayedOperations;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set allowDelayedOperations(value:Boolean):void {
			_allowDelayedOperations = value;
		}

		/**
		 * @return 
		 */
		public function get delayUpdates():Boolean {
			return _delayUpdates;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set delayUpdates(value:Boolean):void {
			_delayUpdates = value;
		}

		/**
		 * <p> The IUndoManager assigned to this edit manager. </p>
		 * <p>To allow edits to be undone (and redone), pass an IUndoManager instance to the EditManager constructor. The undo manager maintains a stack of operations that have been executed, and it can undo or redo individual operations. </p>
		 * <p><b>Note:</b> If the TextFlow is modified directly (not via calls to the EditManager, but directly via calls to the managed FlowElement objects), then the EditManager clears the undo stack to prevent the stack from getting out of sync with the current state.</p>
		 * 
		 * @return 
		 */
		public function get undoManager():IUndoManager {
			return _undoManager;
		}

		/**
		 * <p> Applies container styles to any containers in the selection. </p>
		 * <p>Any style properties in the format object that are <code>null</code> are left unchanged.</p>
		 * 
		 * @param containerFormat  — The format to apply to the containers in the range 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 */
		public function applyContainerFormat(containerFormat:ITextLayoutFormat, operationState:SelectionState = null):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Changes the formats of the specified (or current) selection. </p>
		 * <p>Executes an undoable operation that applies the new formats. Only style attributes set for the TextLayoutFormat objects are applied. Undefined attributes in the format objects are not changed. </p>
		 * 
		 * @param leafFormat  — The format to apply to leaf elements such as spans and inline graphics. 
		 * @param paragraphFormat  — The format to apply to paragraph elements. 
		 * @param containerFormat  — The format to apply to the containers. 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 */
		public function applyFormat(leafFormat:ITextLayoutFormat, paragraphFormat:ITextLayoutFormat, containerFormat:ITextLayoutFormat, operationState:SelectionState = null):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Applies styles to the specified element. </p>
		 * <p>Any style properties in the format object that are <code>null</code> are left unchanged. Only styles that are relevant to the specified element are applied.</p>
		 * 
		 * @param targetElement  — The element to which the styles are applied. 
		 * @param format  — The format containing the styles to apply. 
		 * @param relativeStart  — An offset from the beginning of the element at which to split the element when assigning the new formatting. 
		 * @param relativeEnd  — An offset from the beginning of the element at which to split the element when applying the new formatting. 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 */
		public function applyFormatToElement(targetElement:FlowElement, format:ITextLayoutFormat, relativeStart:int = 0, relativeEnd:int = -1, operationState:SelectionState = null):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Changes the format applied to the leaf elements in the specified (or current) selection. </p>
		 * <p>Executes an undoable operation that applies the new format to leaf elements such as SpanElement and InlineGraphicElement objects. Only style attributes set for the TextLayoutFormat objects are applied. Undefined attributes in the format object are changed.</p>
		 * 
		 * @param characterFormat  — The format to apply. 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 */
		public function applyLeafFormat(characterFormat:ITextLayoutFormat, operationState:SelectionState = null):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Transforms a selection into a link, or a link into normal text. </p>
		 * <p>Executes an undoable operation that creates or removes the link.</p>
		 * <p>If a <code>target</code> parameter is specified, it must be one of the following values:</p>
		 * <ul>
		 *  <li>"_self"</li>
		 *  <li>"_blank"</li>
		 *  <li>"_parent"</li>
		 *  <li>"_top"</li>
		 * </ul>
		 * <p>In browser-hosted runtimes, a target of "_self" replaces the current html page. So, if the SWF content containing the link is in a page within a frame or frameset, the linked content loads within that frame. If the page is at the top level, the linked content opens to replace the original page. A target of "_blank" opens a new browser window with no name. A target of "_parent" replaces the parent of the html page containing the SWF content. A target of "_top" replaces the top-level page in the current browser window.</p>
		 * <p>In other runtimes, such as Adobe AIR, the link opens in the user's default browser and the <code>target</code> parameter is ignored.</p>
		 * <p>The <code>extendToLinkBoundary</code> parameter determines how the edit manager treats a selection that intersects with one or more existing links. If the parameter is <code>true</code>, then the operation is applied as a unit to the selection and the whole text of the existing links. Thus, a single link is created that spans from the beginning of the first link intersected to the end of the last link intersected. In contrast, if <code>extendToLinkBoundary</code> were <code>false</code> in this situation, the existing partially selected links would be split into two links.</p>
		 * 
		 * @param href  — The uri referenced by the link. 
		 * @param targetString  — The target browser window of the link. 
		 * @param extendToLinkBoundary  — Specifies whether to consolidate selection with any overlapping existing links, and then apply the change. 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 * @return  — The LinkElement that was created. 
		 */
		public function applyLink(href:String, targetString:String = null, extendToLinkBoundary:Boolean = false, operationState:SelectionState = null):LinkElement {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Applies paragraph styles to any paragraphs in the selection. </p>
		 * <p>Any style properties in the format object that are <code>null</code> are left unchanged.</p>
		 * 
		 * @param paragraphFormat  — The format to apply to the selected paragraphs. 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 */
		public function applyParagraphFormat(paragraphFormat:ITextLayoutFormat, operationState:SelectionState = null):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Transforms text into a TCY run, or a TCY run into non-TCY text. </p>
		 * <p>TCY, or tate-chu-yoko, causes text to draw horizontally within a vertical line, and is used to make small blocks of non-Japanese text or numbers, such as dates, more readable in vertical text.</p>
		 * 
		 * @param tcyOn  — Set to <code>true</code> to apply TCY to a text range, <code>false</code> to remove TCY. 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 * @return  — The TCYElement that was created. 
		 */
		public function applyTCY(tcyOn:Boolean, operationState:SelectionState = null):TCYElement {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Begins a new group of operations. </p>
		 * <p>All operations executed after the call to <code>beginCompositeOperation()</code>, and before the matching call to <code>endCompositeOperation()</code> are executed and grouped together as a single operation that can be undone as a unit.</p>
		 * <p>A <code>beginCompositeOperation</code>/<code>endCompositeOperation</code> block can be nested inside another <code>beginCompositeOperation</code>/<code>endCompositeOperation</code> block.</p>
		 * <span id="pageFilter"><br><span class="label"> Example &nbsp;( <span class="usage"><a href="http://www.adobe.com/go/learn_as3_usingexamples_en"> How to use this example </a></span>) </span><br><br>
		 *  <div class="detailBody">
		 *    The following example defines a function that inserts a graphic object in its own paragraph. If the 
		 *   <code>beginCompositeOperation()</code> and 
		 *   <code>endCompositeOperation()</code> functions were not used, then each of the suboperations would have to be undone separately rather than as a group. 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>
		 * package flashx.textLayout.edit.examples
		 * {
		 *     import flashx.textLayout.edit.IEditManager;
		 *     import flashx.textLayout.edit.SelectionState;
		 *     import flashx.textLayout.elements.TextFlow;
		 *     
		 *     public class EditManager_beginCompositeOperation
		 *     {
		 *         static public function insertGraphic( source:Object, width:Object, height:Object, float:String, selection:SelectionState ):void
		 *         {
		 *             var editManager:IEditManager = selection.textFlow.interactionManager as IEditManager;
		 *             
		 *             editManager.beginCompositeOperation();
		 *             
		 *             editManager.deleteText( selection );
		 *             var changedSelection:SelectionState = 
		 *                 new SelectionState( selection.textFlow, selection.anchorPosition, selection.anchorPosition );
		 *             editManager.splitParagraph( changedSelection );
		 *             changedSelection = 
		 *                 new SelectionState( changedSelection.textFlow, changedSelection.anchorPosition + 1, changedSelection.anchorPosition + 1);
		 *             editManager.insertInlineGraphic( source, width, height, float, changedSelection );
		 *             changedSelection = 
		 *                 new SelectionState( changedSelection.textFlow, changedSelection.anchorPosition + 1, changedSelection.anchorPosition + 1);
		 *             editManager.splitParagraph( changedSelection );
		 *             
		 *             editManager.endCompositeOperation();                
		 *         }
		 *     }
		 * }
		 * </pre>
		 *   </div>
		 *  </div></span>
		 */
		public function beginCompositeOperation():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Changes the ID of an element. </p>
		 * <p>If the <code>relativeStart</code> or <code>relativeEnd</code> parameters are set (to anything other than the default values), then the element is split. The parts of the element outside this range retain the original ID. Setting both the <code>relativeStart</code> and <code>relativeEnd</code> parameters creates elements with duplicate IDs.</p>
		 * 
		 * @param newID  — The new ID value. 
		 * @param targetElement  — The element to modify. 
		 * @param relativeStart  — An offset from the beginning of the element at which to split the element when assigning the new ID. 
		 * @param relativeEnd  — An offset from the beginning of the element at which to split the element when assigning the new ID. 
		 * @param operationState  — Specifies the selection to restore when undoing this operation; if <code>null</code>, the operation saves the current selection. 
		 */
		public function changeElementID(newID:String, targetElement:FlowElement, relativeStart:int = 0, relativeEnd:int = -1, operationState:SelectionState = null):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Changes the styleName of an element or part of an element. </p>
		 * <p>If the <code>relativeStart</code> or <code>relativeEnd</code> parameters are set (to anything other than the default values), then the element is split. The parts of the element outside this range retain the original style.</p>
		 * 
		 * @param newName  — The name of the new style. 
		 * @param targetElement  — Specifies the element to change. 
		 * @param relativeStart  — An offset from the beginning of the element at which to split the element when assigning the new style. 
		 * @param relativeEnd  — An offset from the end of the element at which to split the element when assigning the new style. 
		 * @param operationState  — Specifies the selection to restore when undoing this operation; if <code>null</code>, the operation saves the current selection. 
		 */
		public function changeStyleName(newName:String, targetElement:FlowElement, relativeStart:int = 0, relativeEnd:int = -1, operationState:SelectionState = null):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Changes the typeName of an element or part of an element. </p>
		 * <p>If the <code>relativeStart</code> or <code>relativeEnd</code> parameters are set (to anything other than the default values), then the element is split. The parts of the element outside this range retain the original style.</p>
		 * 
		 * @param newName  — The name of the new type. 
		 * @param targetElement  — Specifies the element to change. 
		 * @param relativeStart  — An offset from the beginning of the element at which to split the element when assigning the new style 
		 * @param relativeEnd  — An offset from the end of the element at which to split the element when assigning the new style 
		 * @param operationState  — Specifies the selection to restore when undoing this operation; if <code>null</code>, the operation saves the current selection. 
		 */
		public function changeTypeName(newName:String, targetElement:FlowElement, relativeStart:int = 0, relativeEnd:int = -1, operationState:SelectionState = null):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Undefines formats of the specified (or current) selection. </p>
		 * <p>Executes an undoable operation that undefines the specified formats. Only style attributes set for the TextLayoutFormat objects are applied. Undefined attributes in the format objects are not changed. </p>
		 * 
		 * @param leafFormat  — The format whose set values indicate properties to undefine to LeafFlowElement objects in the selected range. 
		 * @param paragraphFormat  — The format whose set values indicate properties to undefine to ParagraphElement objects in the selected range. 
		 * @param containerFormat  — The format whose set values indicate properties to undefine to ContainerController objects in the selected range. 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 */
		public function clearFormat(leafFormat:ITextLayoutFormat, paragraphFormat:ITextLayoutFormat, containerFormat:ITextLayoutFormat, operationState:SelectionState = null):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Undefines styles to the specified element. </p>
		 * <p>Any style properties in the format object that are <code>undefined</code> are left unchanged. Any styles that are defined in the specififed format are undefined on the specified element.</p>
		 * 
		 * @param targetElement  — The element to which the styles are applied. 
		 * @param format  — The format containing the styles to undefine. 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 */
		public function clearFormatOnElement(targetElement:FlowElement, format:ITextLayoutFormat, operationState:SelectionState = null):void {
			throw new Error("Not implemented");
		}

		/**
		 * @param parent  — Specifies a parent element for the new DivElement. If <code>null</code> the new parent will be lowest level that contains the SelectionState. 
		 * @param format  — Formatting attributes to apply to the new DivElement. 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 * @return  — The new DivElement that was created. 
		 */
		public function createDiv(parent:FlowGroupElement = null, format:ITextLayoutFormat = null, operationState:SelectionState = null):DivElement {
			throw new Error("Not implemented");
		}

		/**
		 * @param parent  — Optionally specifies a parent element for the new ListElement. If <code>null</code> the new parent will be lowest level that contains the SelectionState. 
		 * @param format  — Formatting attributes to apply to the new ListElement. 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 * @return  — The new ListElement that was created. 
		 */
		public function createList(parent:FlowGroupElement = null, format:ITextLayoutFormat = null, operationState:SelectionState = null):ListElement {
			throw new Error("Not implemented");
		}

		/**
		 * @param parent  — Specifies a parent element for the new SubParagraphGroupElement element. If <code>null</code> the new parent will be lowest level that contains the SelectionState. 
		 * @param format  — Formatting attributes to apply to the new SubParagraphGroupElement 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 * @return  — The new SubParagraphGroupElement that was created. 
		 */
		public function createSubParagraphGroup(parent:FlowGroupElement = null, format:ITextLayoutFormat = null, operationState:SelectionState = null):SubParagraphGroupElement {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Deletes the selected area and returns the deleted area in a TextScrap object. </p>
		 * <p>The resulting TextScrap can be posted to the system clipboard or used in a subsequent <code>pasteTextOperation()</code> operation.</p>
		 * 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 * @return  — The TextScrap that was cut. 
		 */
		public function cutTextScrap(operationState:SelectionState = null):TextScrap {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Deletes a range of text, or, if a point selection is given, deletes the next character. </p>
		 * 
		 * @param operationState  — specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 */
		public function deleteNextCharacter(operationState:SelectionState = null):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Deletes the next word. </p>
		 * <p>If a range is selected, the first word of the range is deleted.</p>
		 * 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 */
		public function deleteNextWord(operationState:SelectionState = null):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Deletes a range of text, or, if a point selection is given, deletes the previous character. </p>
		 * 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 */
		public function deletePreviousCharacter(operationState:SelectionState = null):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Deletes the previous word. </p>
		 * <p>If a range is selected, the first word of the range is deleted.</p>
		 * 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 */
		public function deletePreviousWord(operationState:SelectionState = null):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Deletes a range of text. </p>
		 * 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 */
		public function deleteText(operationState:SelectionState = null):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Executes a FlowOperation. </p>
		 * <p>The <code>doOperation()</code> method is called by IEditManager functions that update the text flow. You do not typically need to call this function directly unless you create your own custom operations.</p>
		 * <p>This function proceeds in the following steps:</p>
		 * <ol>
		 *  <li>Flush any pending operations before performing this operation.</li>
		 *  <li>Send a cancelable flowOperationBegin event. If canceled this method returns immediately.</li>
		 *  <li>Execute the operation. The operation returns <code>true</code> or <code>false</code>. <code>False</code> indicates that no changes were made.</li>
		 *  <li>Push the operation onto the undo stack.</li>
		 *  <li>Clear the redo stack.</li>
		 *  <li>Update the display.</li>
		 *  <li>Send a cancelable flowOperationEnd event.</li>
		 * </ol>
		 * <p>Exception handling: If the operation throws an exception, it is caught and the error is attached to the flowOperationEnd event. If the event is not canceled the error is rethrown.</p>
		 * 
		 * @param operation  — a FlowOperation object 
		 */
		override public function doOperation(operation:FlowOperation):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Ends a group of operations. </p>
		 * <p>All operations executed since the last call to <code>beginCompositeOperation()</code> are grouped as a CompositeOperation that is then completed. This CompositeOperation object is added to the undo stack or, if this composite operation is nested inside another composite operation, added to the parent operation.</p>
		 * <span id="pageFilter"><br><span class="label"> Example &nbsp;( <span class="usage"><a href="http://www.adobe.com/go/learn_as3_usingexamples_en"> How to use this example </a></span>) </span><br><br>
		 *  <div class="detailBody">
		 *    The following example defines a function that inserts a graphic object in its own paragraph. If the 
		 *   <code>beginCompositeOperation()</code> and 
		 *   <code>endCompositeOperation()</code> functions were not used, then each of the suboperations would have to be undone separately rather than as a group. 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>
		 * package flashx.textLayout.edit.examples
		 * {
		 *     import flashx.textLayout.edit.IEditManager;
		 *     import flashx.textLayout.edit.SelectionState;
		 *     import flashx.textLayout.elements.TextFlow;
		 *     
		 *     public class EditManager_beginCompositeOperation
		 *     {
		 *         static public function insertGraphic( source:Object, width:Object, height:Object, float:String, selection:SelectionState ):void
		 *         {
		 *             var editManager:IEditManager = selection.textFlow.interactionManager as IEditManager;
		 *             
		 *             editManager.beginCompositeOperation();
		 *             
		 *             editManager.deleteText( selection );
		 *             var changedSelection:SelectionState = 
		 *                 new SelectionState( selection.textFlow, selection.anchorPosition, selection.anchorPosition );
		 *             editManager.splitParagraph( changedSelection );
		 *             changedSelection = 
		 *                 new SelectionState( changedSelection.textFlow, changedSelection.anchorPosition + 1, changedSelection.anchorPosition + 1);
		 *             editManager.insertInlineGraphic( source, width, height, float, changedSelection );
		 *             changedSelection = 
		 *                 new SelectionState( changedSelection.textFlow, changedSelection.anchorPosition + 1, changedSelection.anchorPosition + 1);
		 *             editManager.splitParagraph( changedSelection );
		 *             
		 *             editManager.endCompositeOperation();                
		 *         }
		 *     }
		 * }
		 * </pre>
		 *   </div>
		 *  </div></span>
		 */
		public function endCompositeOperation():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Inserts an image. </p>
		 * <p>The source of the image can be a string containing a URI, URLRequest object, a Class object representing an embedded asset, or a DisplayObject instance.</p>
		 * <p>The width and height values can be the number of pixels, a percent, or the string, 'auto', in which case the actual dimension of the graphic is used.</p>
		 * <p>Set the <code>float</code> to one of the constants defined in the Float class to specify whether the image should be displayed to the left or right of any text or inline with the text.</p>
		 * 
		 * @param source  — Can be either a String interpreted as a uri, a Class interpreted as the class of an Embed DisplayObject, a DisplayObject instance or a URLRequest. 
		 * @param width  — The width of the image to insert (number, percent, or 'auto'). 
		 * @param height  — The height of the image to insert (number, percent, or 'auto'). 
		 * @param options  — None supported. 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 * @return 
		 */
		public function insertInlineGraphic(source:Object, width:Object, height:Object, options:Object = null, operationState:SelectionState = null):InlineGraphicElement {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Inserts text. </p>
		 * <p>Inserts the text at a position or range in the text. If the location supplied in the <code>operationState</code> parameter is a range (or the parameter is <code>null</code> and the current selection is a range), then the text currently in the range is replaced by the inserted text.</p>
		 * 
		 * @param text  — The string to insert. 
		 * @param origOperationState  — Specifies the text in the flow to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 */
		public function insertText(text:String, origOperationState:SelectionState = null):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Modifies an existing inline graphic. </p>
		 * <p>Set unchanging properties to the values in the original graphic. (Modifying an existing graphic object is typically more efficient than deleting and recreating one.)</p>
		 * 
		 * @param source  — Can be either a String interpreted as a uri, a Class interpreted as the class of an Embed DisplayObject, a DisplayObject instance or a URLRequest. 
		 * @param width  — The new width for the image (number or percent). 
		 * @param height  — The new height for the image (number or percent). 
		 * @param options  — None supported. 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 */
		public function modifyInlineGraphic(source:Object, width:Object, height:Object, options:Object = null, operationState:SelectionState = null):void {
			throw new Error("Not implemented");
		}

		/**
		 * @param source  — The orginal parent of the elements to be moved. 
		 * @param sourceIndex  — The child index within the source of the first element to be moved. 
		 * @param numChildren  — The number of children being moved. 
		 * @param destination  — The new parent of elements after move. 
		 * @param destinationIndex  — The child index within the destination to where elements are moved to. 
		 * @param selectionState  — Specifies the text to which this operation applies, and to which selection returns to upon undo. If <code>null</code>, the operation applies to the current selection. If there is no current selection, this parameter must be non-null. 
		 */
		public function moveChildren(source:FlowGroupElement, sourceIndex:int, numChildren:int, destination:FlowGroupElement, destinationIndex:int, selectionState:SelectionState = null):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Overwrites the selected text. </p>
		 * <p>If the selection is a point selection, the first character is overwritten by the new text.</p>
		 * 
		 * @param text  — The string to insert. 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 */
		public function overwriteText(text:String, operationState:SelectionState = null):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Pastes the TextScrap into the selected area. </p>
		 * <p>If a range of text is specified, the text in the range is deleted.</p>
		 * 
		 * @param scrapToPaste  — The TextScrap to paste. 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 */
		public function pasteTextScrap(scrapToPaste:TextScrap, operationState:SelectionState = null):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Reperforms the previous undone operation. </p>
		 * <p><b>Note:</b> If the IUndoManager associated with this IEditManager is also associated with another IEditManager, then it is possible that the redo operation associated with the other IEditManager is the one redone. This can happen if the FlowOperation of another IEditManager is on top of the redo stack.</p>
		 * <p>This function does nothing if undo is not turned on.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../../flashx/undo/IUndoManager.html#redo()" target="">flashx.undo.IUndoManager.redo()</a>
		 * </div>
		 * <span id="pageFilter"><br><span class="label"> Example &nbsp;( <span class="usage"><a href="http://www.adobe.com/go/learn_as3_usingexamples_en"> How to use this example </a></span>) </span><br><br>
		 *  <div class="detailBody">
		 *    The following example defines a function that reperforms the last undone operation on a text flow: 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>
		 * package flashx.textLayout.edit.examples
		 * {
		 *     import flash.display.Sprite;
		 *     
		 *     import flashx.textLayout.edit.IEditManager;
		 *     import flashx.textLayout.elements.TextFlow;
		 *     
		 *     public class EditManager_redo
		 *     {
		 *         static public function redo( textFlow:TextFlow ):void
		 *         {
		 *             if( textFlow.interactionManager is IEditManager )
		 *             {
		 *                 IEditManager( textFlow.interactionManager ).redo();
		 *             }
		 *         }
		 *     }
		 * }
		 * </pre>
		 *   </div>
		 *  </div></span>
		 */
		public function redo():void {
			throw new Error("Not implemented");
		}

		/**
		 * @param target  — The element to be split. 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 * @return  — The new paragraph that was created. 
		 */
		public function splitElement(target:FlowGroupElement, operationState:SelectionState = null):FlowGroupElement {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Splits the paragraph at the current position, creating a new paragraph after the current one. </p>
		 * <p>If a range of text is specified, the text in the range is deleted.</p>
		 * 
		 * @param operationState  — Specifies the text to which this operation applies; if <code>null</code>, the operation applies to the current selection. 
		 * @return  — The new paragraph that was created. 
		 */
		public function splitParagraph(operationState:SelectionState = null):ParagraphElement {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Reverses the previous operation. </p>
		 * <p><b>Note:</b> If the IUndoManager associated with this IEditManager is also associated with another IEditManager, then it is possible that the undo operation associated with the other IEditManager is the one undone. This can happen if the FlowOperation of another IEditManager is on top of the undo stack.</p>
		 * <p>This function does nothing if undo is not turned on.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../../flashx/undo/IUndoManager.html#undo()" target="">flashx.undo.IUndoManager.undo()</a>
		 * </div>
		 * <span id="pageFilter"><br><span class="label"> Example &nbsp;( <span class="usage"><a href="http://www.adobe.com/go/learn_as3_usingexamples_en"> How to use this example </a></span>) </span><br><br>
		 *  <div class="detailBody">
		 *    The following example defines a function that undoes the last operation on a text flow: 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>
		 * package flashx.textLayout.edit.examples
		 * {
		 *     import flashx.textLayout.edit.IEditManager;
		 *     import flashx.textLayout.elements.TextFlow;
		 *     
		 *     public class EditManager_undo
		 *     {
		 *         static public function undo( textFlow:TextFlow ):void
		 *         {
		 *             if( textFlow.interactionManager is IEditManager )
		 *             {
		 *                 IEditManager( textFlow.interactionManager ).undo();
		 *             }
		 *         }
		 *     }
		 * }
		 * </pre>
		 *   </div>
		 *  </div></span>
		 */
		public function undo():void {
			throw new Error("Not implemented");
		}

		public function updateAllControllers():void {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Indicates whether overwrite mode is on or off. </p>
		 * <p>If <code>true</code>, then a keystroke overwrites the character following the cursor. If <code>false</code>, then a keystroke is inserted at the cursor location.</p>
		 * 
		 * @return 
		 */
		public static function get overwriteMode():Boolean {
			return _overwriteMode;
		}


		/**
		 * @param value
		 * @return 
		 */
		public static function set overwriteMode(value:Boolean):void {
			_overwriteMode = value;
		}
	}
}
