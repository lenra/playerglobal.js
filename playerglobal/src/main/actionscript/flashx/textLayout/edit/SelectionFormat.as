package flashx.textLayout.edit {
	/**
	 *  The SelectionFormat class defines the properties of a selection highlight. <p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS02f7d8d4857b1677-165a04e1126951a2d98-7ff2.html" target="_blank">Styling TLF-based text controls</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="ISelectionManager.html" target="">flashx.textLayout.edit.ISelectionManager</a>
	 *  <br>
	 *  <a href="SelectionManager.html" target="">flashx.textLayout.edit.SelectionManager</a>
	 * </div><br><hr>
	 */
	public class SelectionFormat {
		private var _pointAlpha:Number;
		private var _pointBlendMode:String;
		private var _pointBlinkRate:Number;
		private var _pointColor:uint;
		private var _rangeAlpha:Number;
		private var _rangeBlendMode:String;
		private var _rangeColor:uint;

		/**
		 * <p> Creates a SelectionFormat object with the specified properties. </p>
		 * <p>A SelectionFormat created with the default values will use black for the highlight colors, 1.0 for the alphas, and BlendMode.DIFFERENCE for the blending modes. The cursor blink rate is 500 milliseconds.</p>
		 * <p>Setting the <code>pointAlpha</code> and <code>rangeAlpha</code> properties to zero disables selection highlighting.</p>
		 * <p>Non-zero blink rate is only used when an EditManager is attached to the TextFlow.</p>
		 * 
		 * @param rangeColor  — The color for drawing the highlight. 
		 * @param rangeAlpha  — The transparency value for drawing the highlight. Valid values are between 0 (completely transparent) and 1 (completely opaque, which is the default). 
		 * @param rangeBlendMode  — The blend mode for drawing the highlight. Use constants defined in the BlendMode class to set this parameter. 
		 * @param pointColor  — The color for the drawing cursor. 
		 * @param pointAlpha  — The transparency value for drawing the cursor. Valid values are between 0 (completely transparent) and 1 (completely opaque, which is the default). 
		 * @param pointBlendMode  — The blend mode for drawing the cursor. Use constants defined in the BlendMode class to set this parameter. 
		 * @param pointBlinkRate  — The rate at which the cursor blinks, in milliseconds. 
		 */
		public function SelectionFormat(rangeColor:uint = 0xffffff, rangeAlpha:Number = 1.0, rangeBlendMode:String = "difference", pointColor:uint = 0xffffff, pointAlpha:Number = 1.0, pointBlendMode:String = "difference", pointBlinkRate:Number = 500) {
			throw new Error("Not implemented");
		}

		/**
		 * <p> The alpha for drawing the cursor. Valid values are between 0 (completely transparent) and 1 (completely opaque, which is the default). </p>
		 * <p>Setting the <code>pointAlpha</code> and <code>rangeAlpha</code> properties to zero disables selection highlighting.</p>
		 * 
		 * @return 
		 */
		public function get pointAlpha():Number {
			return _pointAlpha;
		}

		/**
		 * <p> The blend mode for drawing the cursor. </p>
		 * 
		 * @return 
		 */
		public function get pointBlendMode():String {
			return _pointBlendMode;
		}

		/**
		 * <p> The rate at which the cursor blinks, in milliseconds. </p>
		 * 
		 * @return 
		 */
		public function get pointBlinkRate():Number {
			return _pointBlinkRate;
		}

		/**
		 * <p> The color for drawing the cursor. </p>
		 * 
		 * @return 
		 */
		public function get pointColor():uint {
			return _pointColor;
		}

		/**
		 * <p> The alpha for drawing the highlight of a range selection. Valid values are between 0 (completely transparent) and 1 (completely opaque, which is the default). </p>
		 * <p>Setting the <code>pointAlpha</code> and <code>rangeAlpha</code> properties to zero disables selection highlighting.</p>
		 * 
		 * @return 
		 */
		public function get rangeAlpha():Number {
			return _rangeAlpha;
		}

		/**
		 * <p> The blending mode for drawing the highlight of a range selection. </p>
		 * 
		 * @return 
		 */
		public function get rangeBlendMode():String {
			return _rangeBlendMode;
		}

		/**
		 * <p> The color for drawing the highlight of a range selection. </p>
		 * 
		 * @return 
		 */
		public function get rangeColor():uint {
			return _rangeColor;
		}

		/**
		 * <p> Determines whether this SelectionFormat object has the same property values as another SelectionFormat object. </p>
		 * 
		 * @param selectionFormat  — the SelectionFormat to compare against. 
		 * @return  — , if the property values are identical; , otherwise. 
		 */
		public function equals(selectionFormat:SelectionFormat):Boolean {
			throw new Error("Not implemented");
		}
	}
}
