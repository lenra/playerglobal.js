package flashx.textLayout.edit {
	import flash.events.ContextMenuEvent;
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.IMEEvent;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.TextEvent;

	/**
	 *  The IInteractionEventHandler interface defines the event handler functions that are handled by a Text Layout Framework selection or edit manager. <br><hr>
	 */
	public interface IInteractionEventHandler {

		/**
		 * <p> Processes an activate event. </p>
		 * 
		 * @param event
		 */
		function activateHandler(event:flash.events.Event):void;

		/**
		 * <p> Processes a deactivate event. </p>
		 * 
		 * @param event
		 */
		function deactivateHandler(event:flash.events.Event):void;

		/**
		 * <p> Processes an edit event. </p>
		 * <p>Edit events are dispatched for cut, copy, paste, and selectAll commands.</p>
		 * 
		 * @param event
		 */
		function editHandler(event:flash.events.Event):void;

		/**
		 * <p> Processes a focusChange event. </p>
		 * 
		 * @param event
		 */
		function focusChangeHandler(event:FocusEvent):void;

		/**
		 * <p> Processes a focusIn event. </p>
		 * 
		 * @param event
		 */
		function focusInHandler(event:FocusEvent):void;

		/**
		 * <p> Processes a focusOut event. </p>
		 * 
		 * @param event
		 */
		function focusOutHandler(event:FocusEvent):void;

		/**
		 * <p> Processes an imeStartComposition event </p>
		 * 
		 * @param event
		 */
		function imeStartCompositionHandler(event:IMEEvent):void;

		/**
		 * <p> Processes a keyDown event. </p>
		 * 
		 * @param event
		 */
		function keyDownHandler(event:KeyboardEvent):void;

		/**
		 * <p> Processes a keyFocusChange event. </p>
		 * 
		 * @param event
		 */
		function keyFocusChangeHandler(event:FocusEvent):void;

		/**
		 * <p> Processes a keyUp event. </p>
		 * 
		 * @param event
		 */
		function keyUpHandler(event:KeyboardEvent):void;

		/**
		 * <p> Processes a menuSelect event. </p>
		 * 
		 * @param event
		 */
		function menuSelectHandler(event:ContextMenuEvent):void;

		/**
		 * <p> Processes a mouseDoubleClick event. </p>
		 * 
		 * @param event
		 */
		function mouseDoubleClickHandler(event:MouseEvent):void;

		/**
		 * <p> Processes a mouseDown event. </p>
		 * 
		 * @param event
		 */
		function mouseDownHandler(event:MouseEvent):void;

		/**
		 * <p> Processes a mouseMove event. </p>
		 * 
		 * @param event
		 */
		function mouseMoveHandler(event:MouseEvent):void;

		/**
		 * <p> Processes a mouseOut event. </p>
		 * 
		 * @param event
		 */
		function mouseOutHandler(event:MouseEvent):void;

		/**
		 * <p> Processes a mouseOver event. </p>
		 * 
		 * @param event
		 */
		function mouseOverHandler(event:MouseEvent):void;

		/**
		 * <p> Processes a mouseUp event. </p>
		 * 
		 * @param event
		 */
		function mouseUpHandler(event:MouseEvent):void;

		/**
		 * <p> Processes a mouseWheel event. </p>
		 * 
		 * @param event
		 */
		function mouseWheelHandler(event:MouseEvent):void;

		/**
		 * <p> Processes an softKeyboardActivating event </p>
		 * 
		 * @param event
		 */
		function softKeyboardActivatingHandler(event:flash.events.Event):void;

		/**
		 * <p> Processes a TextEvent. </p>
		 * 
		 * @param event
		 */
		function textInputHandler(event:TextEvent):void;
	}
}
