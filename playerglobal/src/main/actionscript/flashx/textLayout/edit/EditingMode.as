package flashx.textLayout.edit {
	/**
	 *  The EditingMode class defines constants used with EditManager class to represent the read, select, and edit permissions of a document. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="EditManager.html" target="">flashx.textLayout.edit.EditManager</a>
	 * </div><br><hr>
	 */
	public class EditingMode {
		/**
		 * <p> The document is read-only. </p>
		 * <p>Neither selection nor editing is allowed.</p>
		 */
		public static const READ_ONLY:String = "readOnly";
		/**
		 * <p> The text in the document can be selected and copied, but not edited. </p>
		 */
		public static const READ_SELECT:String = "readSelect";
		/**
		 * <p> The document can be edited. </p>
		 */
		public static const READ_WRITE:String = "readWrite";
	}
}
