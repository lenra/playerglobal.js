package flashx.textLayout.edit {
	import flashx.textLayout.elements.TextFlow;
	import flashx.textLayout.elements.TextRange;
	import flashx.textLayout.formats.TextLayoutFormat;

	/**
	 *  The ISelectionManager interface defines the interface for handling text selection. <p>A SelectionManager keeps track of the selected text range and handles events for a TextFlow.</p> <p>A selection can be either a point selection or a range selection. A point selection is the insertion point and is indicated visually by drawing a cursor. A range selection includes the text between an anchor point and an active point.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="SelectionManager.html" target="">flashx.textLayout.edit.SelectionManager</a>
	 *  <br>
	 *  <a href="TextScrap.html" target="">flashx.textLayout.edit.TextScrap</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/elements/TextFlow.html" target="">flashx.textLayout.elements.TextFlow</a>
	 * </div><br><hr>
	 */
	public interface ISelectionManager {

		/**
		 * <p> The text position of the end of the selection, as an offset from the start of the text flow. </p>
		 * <p>The absolute end is the same as either the active or the anchor point of the selection, whichever comes last in the text flow.</p>
		 * 
		 * @return 
		 */
		function get absoluteEnd():int;

		/**
		 * <p> The text position of the start of the selection, as an offset from the start of the text flow. </p>
		 * <p>The absolute start is the same as either the active or the anchor point of the selection, whichever comes first in the text flow.</p>
		 * 
		 * @return 
		 */
		function get absoluteStart():int;

		/**
		 * <p> The active point of the selection. </p>
		 * <p>The <i>active</i> point is the volatile end of the selection. The active point is changed when the selection is modified. The active point can be at either the beginning or the end of the selection.</p>
		 * 
		 * @return 
		 */
		function get activePosition():int;

		/**
		 * <p> The anchor point of the selection. </p>
		 * <p>An <i>anchor</i> point is the stable end of the selection. When the selection is extended, the anchor point does not change. The anchor point can be at either the beginning or the end of the selection.</p>
		 * 
		 * @return 
		 */
		function get anchorPosition():int;

		/**
		 * <p> The current SelectionFormat object. </p>
		 * <p>The current SelectionFormat object is chosen from the SelectionFormat objects assigned to the <code>unfocusedSelectionFormat</code>, <code>inactiveSelectionFormat</code> and <code>focusedSelectionFormat</code> properties based on the current state of the <code>windowActive</code> and <code>focused</code> properties.</p>
		 * 
		 * @return 
		 */
		function get currentSelectionFormat():SelectionFormat;

		/**
		 * <p> The editing mode. </p>
		 * <p>The editing mode indicates whether the text flow supports selection, editing, or only reading. A text flow is made selectable by assigning a selection manager and editable by assigning an edit manager. Constants representing the editing modes are defined in the EditingMode class.</p>
		 * 
		 * @return 
		 */
		function get editingMode():String;

		/**
		 * <p> Indicates whether a container in the text flow has the focus. </p>
		 * <p>The <code>focused</code> property is <code>true</code> if any of the containers in the text flow has key focus.</p>
		 * 
		 * @return 
		 */
		function get focused():Boolean;

		/**
		 * <p> The SelectionFormat object used to draw the selection in a focused container. </p>
		 * 
		 * @return 
		 */
		function get focusedSelectionFormat():SelectionFormat;

		/**
		 * @param value
		 * @return 
		 */
		function set focusedSelectionFormat(value:SelectionFormat):void;

		/**
		 * <p> The SelectionFormat object used to draw the selection when it is not in the active window. </p>
		 * 
		 * @return 
		 */
		function get inactiveSelectionFormat():SelectionFormat;

		/**
		 * @param value
		 * @return 
		 */
		function set inactiveSelectionFormat(value:SelectionFormat):void;

		/**
		 * <p> The TextFlow object managed by this selection manager. </p>
		 * <p>A selection manager manages a single text flow. A selection manager can also be assigned to a text flow by setting the <code>interactionManager</code> property of the TextFlow object.</p>
		 * 
		 * @return 
		 */
		function get textFlow():TextFlow;

		/**
		 * @param value
		 * @return 
		 */
		function set textFlow(value:TextFlow):void;

		/**
		 * <p> The SelectionFormat object used to draw the selection when it is not in a focused container, but is in the active window. </p>
		 * 
		 * @return 
		 */
		function get unfocusedSelectionFormat():SelectionFormat;

		/**
		 * @param value
		 * @return 
		 */
		function set unfocusedSelectionFormat(value:SelectionFormat):void;

		/**
		 * <p> Indicates whether the window associated with the text flow is active. </p>
		 * <p>The <code>windowActive</code> property is <code>true</code> if the window displaying with the text flow is the active window.</p>
		 * 
		 * @return 
		 */
		function get windowActive():Boolean;

		/**
		 * <p> Executes any pending FlowOperations. </p>
		 * <p>The execution of some editing operations, such as text insertion, is delayed until the next enterFrame event. Calling <code>flushPendingOperations()</code> causes any deferred operations to be executed immediately.</p>
		 */
		function flushPendingOperations():void;

		/**
		 * <p> Gets the character format attributes that are common to all characters in the specified text range or current selection. </p>
		 * <p>Format attributes that do not have the same value for all characters in the specified element range or selection are set to <code>null</code> in the returned TextLayoutFormat instance.</p>
		 * 
		 * @param range  — The optional range of text for which common attributes are requested. If null, the current selection is used. 
		 * @return  — The common character style settings 
		 */
		function getCommonCharacterFormat(range:TextRange = null):TextLayoutFormat;

		/**
		 * <p> Gets the container format attributes that are common to all containers in the specified text range or current selection. </p>
		 * <p>Format attributes that do not have the same value for all containers in the specified element range or selection are set to <code>null</code> in the returned TextLayoutFormat instance.</p>
		 * 
		 * @param range  — The optional range of text for which common attributes are requested. If null, the current selection is used. 
		 * @return  — The common container style settings 
		 */
		function getCommonContainerFormat(range:TextRange = null):TextLayoutFormat;

		/**
		 * <p> Gets the paragraph format attributes that are common to all paragraphs in the specified text range or current selection. </p>
		 * <p>Format attributes that do not have the same value for all paragraphs in the specified element range or selection are set to <code>null</code> in the returned TextLayoutFormat instance.</p>
		 * 
		 * @param range  — The optional range of text for which common attributes are requested. If null, the current selection is used. 
		 * @return  — The common paragraph style settings 
		 */
		function getCommonParagraphFormat(range:TextRange = null):TextLayoutFormat;

		/**
		 * <p> Gets the SelectionState object of the current selection. </p>
		 * 
		 * @return 
		 */
		function getSelectionState():SelectionState;

		/**
		 * <p> Indicates whether there is a selection. </p>
		 * <p>Returns <code>true</code> if there is either a range selection or a point selection. By default, when a selection manager is first set up, there is no selection (the start and end are -1).</p>
		 * 
		 * @return 
		 */
		function hasSelection():Boolean;

		/**
		 * <p> Indicates whether the selection covers a range of text. </p>
		 * <p>Returns <code>true</code> if there is a selection that extends past a single position.</p>
		 * 
		 * @return 
		 */
		function isRangeSelection():Boolean;

		/**
		 * <p> Updates the selection manager when text is inserted or deleted. </p>
		 * <p>Operations must call <code>notifyInsertOrDelete</code> when changing the text in the text flow. The selection manager adjusts index-based position indicators accordingly. If you create a new Operation class that changes text in a text flow directly (not using another operation) your operation must call this function to keep the selection up to date.</p>
		 * 
		 * @param absolutePosition  — The point in the text where the change was made. 
		 * @param length  — A positive or negative number indicating how many characters were inserted or deleted. 
		 */
		function notifyInsertOrDelete(absolutePosition:int, length:int):void;

		/**
		 * <p> Redisplays the selection shapes. </p>
		 * <p><b>Note:</b> You do not need to call this method directly. It is called automatically.</p>
		 */
		function refreshSelection():void;

		/**
		 * <p> Selects the entire flow. </p>
		 */
		function selectAll():void;

		/**
		 * <p> Selects a range of text. </p>
		 * <p>If a negative number is passed as either of the parameters, then any existing selection is removed.</p>
		 * 
		 * @param anchorPosition  — The anchor point for the new selection, as an absolute position in the TextFlow 
		 * @param activePosition  — The active end of the new selection, as an absolute position in the TextFlow 
		 */
		function selectRange(anchorPosition:int, activePosition:int):void;

		/**
		 * <p> Gives the focus to the first container in the selection. </p>
		 * <span id="pageFilter"><br><span class="label"> Example &nbsp;( <span class="usage"><a href="http://www.adobe.com/go/learn_as3_usingexamples_en"> How to use this example </a></span>) </span><br><br>
		 *  <div class="detailBody">
		 *    The following example sets the focus to the first container in the current selection of a text flow. (The textFlow variable in the example is a TextFlow object.) 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>
		 *  textFlow.interactionManager.setFocus();
		 * </pre>
		 *   </div>
		 *  </div></span>
		 */
		function setFocus():void;

		/**
		 * <p> Sets the SelectionState object of the current selection. </p>
		 * 
		 * @param state
		 */
		function setSelectionState(state:SelectionState):void;
	}
}
