package flashx.textLayout.edit {
	import flashx.textLayout.elements.FlowLeafElement;
	import flashx.textLayout.elements.ParagraphElement;
	import flashx.textLayout.elements.TextFlow;
	import flashx.textLayout.formats.ITextLayoutFormat;
	import flashx.textLayout.formats.TextLayoutFormat;

	/**
	 *  The ElementRange class represents the range of objects selected within a text flow. <p>The beginning elements (such as <code>firstLeaf</code>) are always less than or equal to the end elements (in this case, <code>lastLeaf</code>) for each pair of values in an element range.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flashx/textLayout/elements/TextFlow.html" target="">flashx.textLayout.elements.TextFlow</a>
	 * </div><br><hr>
	 */
	public class ElementRange {
		private var _absoluteEnd:int;
		private var _absoluteStart:int;
		private var _firstLeaf:FlowLeafElement;
		private var _firstParagraph:ParagraphElement;
		private var _lastLeaf:FlowLeafElement;
		private var _lastParagraph:ParagraphElement;
		private var _textFlow:TextFlow;

		private var _characterFormat:ITextLayoutFormat;
		private var _containerFormat:ITextLayoutFormat;
		private var _paragraphFormat:ITextLayoutFormat;

		/**
		 * <p> The absolute text position of the FlowLeafElement object that contains the end of the range. </p>
		 * 
		 * @return 
		 */
		public function get absoluteEnd():int {
			return _absoluteEnd;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set absoluteEnd(value:int):void {
			_absoluteEnd = value;
		}

		/**
		 * <p> The absolute text position of the FlowLeafElement object that contains the start of the range. </p>
		 * 
		 * @return 
		 */
		public function get absoluteStart():int {
			return _absoluteStart;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set absoluteStart(value:int):void {
			_absoluteStart = value;
		}

		/**
		 * <p> The format attributes of the characters in the range. </p>
		 * <p>If the range spans more than one FlowElement object, which means that more than one character format may exist within the range, the format of the first FlowElement object is returned.</p>
		 * 
		 * @return 
		 */
		public function get characterFormat():ITextLayoutFormat {
			return _characterFormat;
		}

		/**
		 * <p> The format attributes of the container displaying the range. </p>
		 * <p>If the range spans more than one container, the format of the first container is returned.</p>
		 * 
		 * @return 
		 */
		public function get containerFormat():ITextLayoutFormat {
			return _containerFormat;
		}

		/**
		 * <p> The FlowLeafElement object that contains the start of the range. </p>
		 * 
		 * @return 
		 */
		public function get firstLeaf():FlowLeafElement {
			return _firstLeaf;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set firstLeaf(value:FlowLeafElement):void {
			_firstLeaf = value;
		}

		/**
		 * <p> The ParagraphElement object that contains the start of the range. </p>
		 * 
		 * @return 
		 */
		public function get firstParagraph():ParagraphElement {
			return _firstParagraph;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set firstParagraph(value:ParagraphElement):void {
			_firstParagraph = value;
		}

		/**
		 * <p> The FlowLeafElement object that contains the end of the range. </p>
		 * 
		 * @return 
		 */
		public function get lastLeaf():FlowLeafElement {
			return _lastLeaf;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set lastLeaf(value:FlowLeafElement):void {
			_lastLeaf = value;
		}

		/**
		 * <p> The ParagraphElement object that contains the end of the range. </p>
		 * 
		 * @return 
		 */
		public function get lastParagraph():ParagraphElement {
			return _lastParagraph;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set lastParagraph(value:ParagraphElement):void {
			_lastParagraph = value;
		}

		/**
		 * <p> The format attributes of the paragraph containing the range. </p>
		 * <p>If the range spans more than one paragraph, the format of the first paragraph is returned.</p>
		 * 
		 * @return 
		 */
		public function get paragraphFormat():ITextLayoutFormat {
			return _paragraphFormat;
		}

		/**
		 * <p> The TextFlow object that contains the range. </p>
		 * 
		 * @return 
		 */
		public function get textFlow():TextFlow {
			return _textFlow;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set textFlow(value:TextFlow):void {
			_textFlow = value;
		}

		/**
		 * <p> Gets the character format attributes that are common to all characters in the text range or current selection. </p>
		 * <p>Format attributes that do not have the same value for all characters in the element range are set to <code>null</code> in the returned TextLayoutFormat instance.</p>
		 * 
		 * @return  — The common character style settings 
		 */
		public function getCommonCharacterFormat():TextLayoutFormat {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Gets the container format attributes that are common to all containers in the element range. </p>
		 * <p>Format attributes that do not have the same value for all containers in the element range are set to <code>null</code> in the returned TextLayoutFormat instance.</p>
		 * 
		 * @return  — The common paragraph style settings 
		 */
		public function getCommonContainerFormat():TextLayoutFormat {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Gets the paragraph format attributes that are common to all paragraphs in the element range. </p>
		 * <p>Format attributes that do not have the same value for all paragraphs in the element range are set to <code>null</code> in the returned TextLayoutFormat instance.</p>
		 * 
		 * @return  — The common paragraph style settings 
		 */
		public function getCommonParagraphFormat():TextLayoutFormat {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Creates an ElementRange object. </p>
		 * 
		 * @param textFlow  — the text flow 
		 * @param absoluteStart  — absolute text position of the first character in the text range 
		 * @param absoluteEnd  — one beyond the absolute text position of the last character in the text range 
		 * @return 
		 */
		public static function createElementRange(textFlow:TextFlow, absoluteStart:int, absoluteEnd:int):ElementRange {
			throw new Error("Not implemented");
		}
	}
}
