package flashx.textLayout.edit {
	import flash.events.ContextMenuEvent;
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.IMEEvent;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.TextEvent;

	import flashx.textLayout.elements.TextFlow;
	import flashx.textLayout.elements.TextRange;
	import flashx.textLayout.formats.TextLayoutFormat;
	import flashx.textLayout.operations.FlowOperation;

	/**
	 *  The SelectionManager class manages text selection in a text flow. <p>The selection manager keeps track of the selected text range, manages its formatting, and can handle events affecting the selection. To allow a user to make selections in a text flow, assign a SelectionManager object to the <code>interactionManager</code> property of the flow. (To allow editing, assign an instance of the EditManager class, which extends SelectionManager.)</p> <p>The following table describes how the SelectionManager class handles keyboard shortcuts:</p> <table class="innertable">
	 *  <tbody>
	 *   <tr></tr>
	 *   <tr>
	 *    <th></th>
	 *    <th></th>
	 *    <th align="center">TB,LTR</th>
	 *    <th align="right"></th>
	 *    <th></th>
	 *    <th align="center">TB,RTL</th>
	 *    <th></th>
	 *    <th></th>
	 *    <th align="center">TL,LTR</th>
	 *    <th></th>
	 *    <th></th>
	 *    <th align="center">RL,RTL</th>
	 *    <th></th>
	 *   </tr>
	 *   <tr>
	 *    <th></th>
	 *    <th>none</th>
	 *    <th>ctrl</th>
	 *    <th>alt|ctrl+alt</th>
	 *    <th>none</th>
	 *    <th>ctrl</th>
	 *    <th>alt|ctrl+alt</th>
	 *    <th>none</th>
	 *    <th>ctrl</th>
	 *    <th>alt|ctrl+alt</th>
	 *    <th>none</th>
	 *    <th>ctrl</th>
	 *    <th>alt|ctrl+alt</th>
	 *   </tr>
	 *   <tr>
	 *    <td>leftarrow</td>
	 *    <td>previousCharacter</td>
	 *    <td>previousWord</td>
	 *    <td>previousWord</td>
	 *    <td>nextCharacter</td>
	 *    <td>nextWord</td>
	 *    <td>nextWord</td>
	 *    <td>nextLine</td>
	 *    <td>endOfDocument</td>
	 *    <td>endOfParagraph</td>
	 *    <td>nextLine</td>
	 *    <td>endOfDocument</td>
	 *    <td>endOfParagraph</td>
	 *   </tr>
	 *   <tr>
	 *    <td>uparrow</td>
	 *    <td>previousLine</td>
	 *    <td>startOfDocument</td>
	 *    <td>startOfParagraph</td>
	 *    <td>previousLine</td>
	 *    <td>startOfDocument</td>
	 *    <td>startOfParagraph</td>
	 *    <td>previousCharacter</td>
	 *    <td>previousWord</td>
	 *    <td>previousWord</td>
	 *    <td>nextCharacter</td>
	 *    <td>nextWord</td>
	 *    <td>nextWord</td>
	 *   </tr>
	 *   <tr>
	 *    <td>rightarrow</td>
	 *    <td>nextCharacter</td>
	 *    <td>nextWord</td>
	 *    <td>nextWord</td>
	 *    <td>previousCharacter</td>
	 *    <td>previousWord</td>
	 *    <td>previousWord</td>
	 *    <td>previousLine</td>
	 *    <td>startOfDocument</td>
	 *    <td>startOfParagraph</td>
	 *    <td>previousLine</td>
	 *    <td>startOfDocument</td>
	 *    <td>startOfParagraph</td>
	 *   </tr>
	 *   <tr>
	 *    <td>downarrow</td>
	 *    <td>nextLine</td>
	 *    <td>endOfDocument</td>
	 *    <td>endOfParagraph</td>
	 *    <td>nextLine</td>
	 *    <td>endOfDocument</td>
	 *    <td>endOfParagraph</td>
	 *    <td>nextCharacter</td>
	 *    <td>nextWord</td>
	 *    <td>nextWord</td>
	 *    <td>previousCharacter</td>
	 *    <td>previousWord</td>
	 *    <td>previousWord</td>
	 *   </tr>
	 *   <tr>
	 *    <td>home</td>
	 *    <td>startOfLine</td>
	 *    <td>startOfDocument</td>
	 *    <td>startOfLine</td>
	 *    <td>startOfLine</td>
	 *    <td>startOfDocument</td>
	 *    <td>startOfLine</td>
	 *    <td>startOfLine</td>
	 *    <td>startOfDocument</td>
	 *    <td>startOfLine</td>
	 *    <td>startOfLine</td>
	 *    <td>startOfDocument</td>
	 *    <td>startOfLine</td>
	 *   </tr>
	 *   <tr>
	 *    <td>end</td>
	 *    <td>endOfLine</td>
	 *    <td>endOfDocument</td>
	 *    <td>endOfLine</td>
	 *    <td>endOfLine</td>
	 *    <td>endOfDocument</td>
	 *    <td>endOfLine</td>
	 *    <td>endOfLine</td>
	 *    <td>endOfDocument</td>
	 *    <td>endOfLine</td>
	 *    <td>endOfLine</td>
	 *    <td>endOfDocument</td>
	 *    <td>endOfLine</td>
	 *   </tr>
	 *   <tr>
	 *    <td>pagedown</td>
	 *    <td>nextPage</td>
	 *    <td>nextPage</td>
	 *    <td>nextPage</td>
	 *    <td>nextPage</td>
	 *    <td>nextPage</td>
	 *    <td>nextPage</td>
	 *    <td>nextPage</td>
	 *    <td>nextPage</td>
	 *    <td>nextPage</td>
	 *    <td>nextPage</td>
	 *    <td>nextPage</td>
	 *    <td>nextPage</td>
	 *   </tr>
	 *   <tr>
	 *    <td>pageup</td>
	 *    <td>previousPage</td>
	 *    <td>previousPage</td>
	 *    <td>previousPage</td>
	 *    <td>previousPage</td>
	 *    <td>previousPage</td>
	 *    <td>previousPage</td>
	 *    <td>previousPage</td>
	 *    <td>previousPage</td>
	 *    <td>previousPage</td>
	 *    <td>previousPage</td>
	 *    <td>previousPage</td>
	 *    <td>previousPage</td>
	 *   </tr>
	 *  </tbody>
	 * </table> <p> <b>Key:</b> </p><ul> 
	 *  <li>none = no modifier</li> 
	 *  <li>ctrl, shift, alt = modifiers</li> 
	 *  <li>alt-key and ctrl+alt-key are the same on all platforms (on some platforms alt-key does not get to the Text Layout Framework (TLF)</li> 
	 *  <li>shift key modifes to extend the active end of the selection in the specified manner</li> 
	 *  <li>TB (top-to-bottom),RL (right-to-left) are textFlow level <code>blockProgression</code> settings</li> 
	 *  <li>LTR (left-to-right),RTL (right-to-left) are textFlow level <code>direction</code> settings</li> 
	 *  <li>next and prev in logical order in the textFlow - the effect in RTL text is that the selection moves in the physical direction</li> 
	 * </ul>  <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="EditManager.html" target="">EditManager</a>
	 *  <br>flashx.elements.TextFlow
	 * </div><br><hr>
	 */
	public class SelectionManager implements ISelectionManager {
		private var _focusedSelectionFormat:SelectionFormat;
		private var _inactiveSelectionFormat:SelectionFormat;
		private var _textFlow:TextFlow;
		private var _unfocusedSelectionFormat:SelectionFormat;

		private var _absoluteEnd:int;
		private var _absoluteStart:int;
		private var _activePosition:int;
		private var _anchorPosition:int;
		private var _currentSelectionFormat:SelectionFormat;
		private var _editingMode:String;
		private var _focused:Boolean;
		private var _windowActive:Boolean;

		/**
		 * <p> Creates a SelectionManager object. </p>
		 * <p>Assign a SelectionManager object to the <code>interactionManager</code> property of a text flow to enable text selection.</p>
		 */
		public function SelectionManager() {
			throw new Error("Not implemented");
		}

		/**
		 * <p> The text position of the end of the selection, as an offset from the start of the text flow. </p>
		 * <p>The absolute end is the same as either the active or the anchor point of the selection, whichever comes last in the text flow.</p>
		 * 
		 * @return 
		 */
		public function get absoluteEnd():int {
			return _absoluteEnd;
		}

		/**
		 * <p> The text position of the start of the selection, as an offset from the start of the text flow. </p>
		 * <p>The absolute start is the same as either the active or the anchor point of the selection, whichever comes first in the text flow.</p>
		 * 
		 * @return 
		 */
		public function get absoluteStart():int {
			return _absoluteStart;
		}

		/**
		 * <p> The active point of the selection. </p>
		 * <p>The <i>active</i> point is the volatile end of the selection. The active point is changed when the selection is modified. The active point can be at either the beginning or the end of the selection.</p>
		 * 
		 * @return 
		 */
		public function get activePosition():int {
			return _activePosition;
		}

		/**
		 * @return 
		 */
		public function get anchorPosition():int {
			return _anchorPosition;
		}

		/**
		 * <p> The current SelectionFormat object. </p>
		 * <p>The current SelectionFormat object is chosen from the SelectionFormat objects assigned to the <code>unfocusedSelectionFormat</code>, <code>inactiveSelectionFormat</code> and <code>focusedSelectionFormat</code> properties based on the current state of the <code>windowActive</code> and <code>focused</code> properties.</p>
		 * 
		 * @return 
		 */
		public function get currentSelectionFormat():SelectionFormat {
			return _currentSelectionFormat;
		}

		/**
		 * <p> The editing mode. </p>
		 * <p>The editing mode indicates whether the text flow supports selection, editing, or only reading. A text flow is made selectable by assigning a selection manager and editable by assigning an edit manager. Constants representing the editing modes are defined in the EditingMode class.</p>
		 * 
		 * @return 
		 */
		public function get editingMode():String {
			return _editingMode;
		}

		/**
		 * <p> Indicates whether a container in the text flow has the focus. </p>
		 * <p>The <code>focused</code> property is <code>true</code> if any of the containers in the text flow has key focus.</p>
		 * 
		 * @return 
		 */
		public function get focused():Boolean {
			return _focused;
		}

		/**
		 * <p> The SelectionFormat object used to draw the selection in a focused container. </p>
		 * 
		 * @return 
		 */
		public function get focusedSelectionFormat():SelectionFormat {
			return _focusedSelectionFormat;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set focusedSelectionFormat(value:SelectionFormat):void {
			_focusedSelectionFormat = value;
		}

		/**
		 * <p> The SelectionFormat object used to draw the selection when it is not in the active window. </p>
		 * 
		 * @return 
		 */
		public function get inactiveSelectionFormat():SelectionFormat {
			return _inactiveSelectionFormat;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set inactiveSelectionFormat(value:SelectionFormat):void {
			_inactiveSelectionFormat = value;
		}

		/**
		 * <p> The TextFlow object managed by this selection manager. </p>
		 * <p>A selection manager manages a single text flow. A selection manager can also be assigned to a text flow by setting the <code>interactionManager</code> property of the TextFlow object.</p>
		 * 
		 * @return 
		 */
		public function get textFlow():TextFlow {
			return _textFlow;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set textFlow(value:TextFlow):void {
			_textFlow = value;
		}

		/**
		 * <p> The SelectionFormat object used to draw the selection when it is not in a focused container, but is in the active window. </p>
		 * 
		 * @return 
		 */
		public function get unfocusedSelectionFormat():SelectionFormat {
			return _unfocusedSelectionFormat;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set unfocusedSelectionFormat(value:SelectionFormat):void {
			_unfocusedSelectionFormat = value;
		}

		/**
		 * <p> Indicates whether the window associated with the text flow is active. </p>
		 * <p>The <code>windowActive</code> property is <code>true</code> if the window displaying with the text flow is the active window.</p>
		 * 
		 * @return 
		 */
		public function get windowActive():Boolean {
			return _windowActive;
		}

		/**
		 * <p> Processes an activate event. </p>
		 * 
		 * @param event
		 */
		public function activateHandler(event:flash.events.Event):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes a deactivate event. </p>
		 * 
		 * @param event
		 */
		public function deactivateHandler(event:flash.events.Event):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Perform a SelectionManager operation - these may never modify the flow but clients still are able to cancel them. </p>
		 * 
		 * @param op
		 */
		public function doOperation(op:FlowOperation):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes an edit event. </p>
		 * <p>Edit events are dispatched for cut, copy, paste, and selectAll commands.</p>
		 * 
		 * @param event
		 */
		public function editHandler(event:flash.events.Event):void {
			throw new Error("Not implemented");
		}

		public function flushPendingOperations():void {
			throw new Error("Not implemented");
		}

		/**
		 * @param event
		 */
		public function focusChangeHandler(event:FocusEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes a focusIn event. </p>
		 * 
		 * @param event
		 */
		public function focusInHandler(event:FocusEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes a focusOut event. </p>
		 * 
		 * @param event
		 */
		public function focusOutHandler(event:FocusEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Gets the character format attributes that are common to all characters in the specified text range or current selection. </p>
		 * <p>Format attributes that do not have the same value for all characters in the specified element range or selection are set to <code>null</code> in the returned TextLayoutFormat instance.</p>
		 * 
		 * @param range  — The optional range of text for which common attributes are requested. If null, the current selection is used. 
		 * @return  — The common character style settings 
		 */
		public function getCommonCharacterFormat(range:TextRange = null):TextLayoutFormat {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Gets the container format attributes that are common to all containers in the specified text range or current selection. </p>
		 * <p>Format attributes that do not have the same value for all containers in the specified element range or selection are set to <code>null</code> in the returned TextLayoutFormat instance.</p>
		 * 
		 * @param range  — The optional range of text for which common attributes are requested. If null, the current selection is used. 
		 * @return  — The common container style settings 
		 */
		public function getCommonContainerFormat(range:TextRange = null):TextLayoutFormat {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Gets the paragraph format attributes that are common to all paragraphs in the specified text range or current selection. </p>
		 * <p>Format attributes that do not have the same value for all paragraphs in the specified element range or selection are set to <code>null</code> in the returned TextLayoutFormat instance.</p>
		 * 
		 * @param range  — The optional range of text for which common attributes are requested. If null, the current selection is used. 
		 * @return  — The common paragraph style settings 
		 */
		public function getCommonParagraphFormat(range:TextRange = null):TextLayoutFormat {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Gets the SelectionState object of the current selection. </p>
		 * 
		 * @return 
		 */
		public function getSelectionState():SelectionState {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Indicates whether there is a selection. </p>
		 * <p>Returns <code>true</code> if there is either a range selection or a point selection. By default, when a selection manager is first set up, there is no selection (the start and end are -1).</p>
		 * 
		 * @return 
		 */
		public function hasSelection():Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes an imeStartComposition event </p>
		 * 
		 * @param event
		 */
		public function imeStartCompositionHandler(event:IMEEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Indicates whether the selection covers a range of text. </p>
		 * <p>Returns <code>true</code> if there is a selection that extends past a single position.</p>
		 * 
		 * @return 
		 */
		public function isRangeSelection():Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes a keyDown event. </p>
		 * 
		 * @param event
		 */
		public function keyDownHandler(event:KeyboardEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes a keyFocusChange event. </p>
		 * 
		 * @param event
		 */
		public function keyFocusChangeHandler(event:FocusEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes a keyUp event. </p>
		 * 
		 * @param event
		 */
		public function keyUpHandler(event:KeyboardEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * @param event
		 */
		public function menuSelectHandler(event:ContextMenuEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes a mouseDoubleClick event. </p>
		 * 
		 * @param event
		 */
		public function mouseDoubleClickHandler(event:MouseEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes a mouseDown event. </p>
		 * 
		 * @param event
		 */
		public function mouseDownHandler(event:MouseEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes a mouseMove event. </p>
		 * 
		 * @param event
		 */
		public function mouseMoveHandler(event:MouseEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes a mouseOut event. </p>
		 * 
		 * @param event
		 */
		public function mouseOutHandler(event:MouseEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes a mouseOver event. </p>
		 * 
		 * @param event
		 */
		public function mouseOverHandler(event:MouseEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes a mouseUp event. </p>
		 * 
		 * @param event
		 */
		public function mouseUpHandler(event:MouseEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * @param event
		 */
		public function mouseWheelHandler(event:MouseEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Updates the selection manager when text is inserted or deleted. </p>
		 * <p>Operations must call <code>notifyInsertOrDelete</code> when changing the text in the text flow. The selection manager adjusts index-based position indicators accordingly. If you create a new Operation class that changes text in a text flow directly (not using another operation) your operation must call this function to keep the selection up to date.</p>
		 * 
		 * @param absolutePosition  — The point in the text where the change was made. 
		 * @param length  — A positive or negative number indicating how many characters were inserted or deleted. 
		 */
		public function notifyInsertOrDelete(absolutePosition:int, length:int):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Redisplays the selection shapes. </p>
		 * <p><b>Note:</b> You do not need to call this method directly. It is called automatically.</p>
		 */
		public function refreshSelection():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../../flashx/textLayout/compose/IFlowComposer.html" target="">flashx.textLayout.compose.IFlowComposer</a>
		 * </div>
		 */
		public function selectAll():void {
			throw new Error("Not implemented");
		}

		/**
		 * @param anchorPosition
		 * @param activePosition
		 */
		public function selectRange(anchorPosition:int, activePosition:int):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Gives the focus to the first container in the selection. </p>
		 * <span id="pageFilter"><br><span class="label"> Example &nbsp;( <span class="usage"><a href="http://www.adobe.com/go/learn_as3_usingexamples_en"> How to use this example </a></span>) </span><br><br>
		 *  <div class="detailBody">
		 *    The following example sets the focus to the first container in the current selection of a text flow. (The textFlow variable in the example is a TextFlow object.) 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>
		 *  textFlow.interactionManager.setFocus();
		 * </pre>
		 *   </div>
		 *  </div></span>
		 */
		public function setFocus():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets the SelectionState object of the current selection. </p>
		 * 
		 * @param sel
		 */
		public function setSelectionState(sel:SelectionState):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes an softKeyboardActivating event </p>
		 * 
		 * @param event
		 */
		public function softKeyboardActivatingHandler(event:flash.events.Event):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes a TextEvent. </p>
		 * 
		 * @param event
		 */
		public function textInputHandler(event:TextEvent):void {
			throw new Error("Not implemented");
		}
	}
}
