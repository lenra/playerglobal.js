package flashx.textLayout.edit {
	import flashx.textLayout.elements.TextFlow;
	import flashx.textLayout.elements.TextRange;

	/**
	 *  The TextScrap class represents a fragment of a text flow. <p>A TextScrap is a holding place for all or part of a TextFlow. A range of text can be copied from a TextFlow into a TextScrap, and pasted from the TextScrap into another TextFlow.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flashx/textLayout/elements/TextFlow.html" target="">flashx.textLayout.elements.TextFlow</a>
	 *  <br>
	 *  <a href="SelectionManager.html" target="">flashx.textLayout.edit.SelectionManager</a>
	 * </div><br><hr>
	 */
	public class TextScrap {
		private var _textFlow:TextFlow;

		/**
		 * <p> Creates a TextScrap object. </p>
		 * <p>Use the <code>createTextScrap()</code> method to create a TextScrap object from a range of text represented by a TextRange object.</p>
		 * 
		 * @param textFlow  — if set, the new TextScrap object contains the entire text flow. Otherwise, the TextScrap object is empty. 
		 */
		public function TextScrap(textFlow:TextFlow = null) {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Gets the TextFlow that is currently in the TextScrap. </p>
		 * 
		 * @return 
		 */
		public function get textFlow():TextFlow {
			return _textFlow;
		}

		/**
		 * <p> Creates a duplicate copy of this TextScrap object. </p>
		 * 
		 * @return  — TextScrap A copy of this TextScrap. 
		 */
		public function clone():TextScrap {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Creates a TextScrap object from a range of text represented by a TextRange object. </p>
		 * 
		 * @param range  — the TextRange object representing the range of text to copy. 
		 * @return 
		 */
		public static function createTextScrap(range:TextRange):TextScrap {
			throw new Error("Not implemented");
		}
	}
}
