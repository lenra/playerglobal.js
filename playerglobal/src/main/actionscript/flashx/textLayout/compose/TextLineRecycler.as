package flashx.textLayout.compose {
	import flash.text.engine.TextLine;

	/**
	 *  The TextLineRecycler class provides support for recycling of TextLines. Some player versions support a recreateTextLine. Passing TextLines to the recycler makes them available for reuse. This improves Player performance. <br><hr>
	 */
	public class TextLineRecycler {
		private static var _textLineRecyclerEnabled:Boolean;


		/**
		 * <p> Controls if the TLF recycler enabled. It can only be enabled in 10.1 or later players. </p>
		 * 
		 * @return 
		 */
		public static function get textLineRecyclerEnabled():Boolean {
			return _textLineRecyclerEnabled;
		}


		/**
		 * @param value
		 * @return 
		 */
		public static function set textLineRecyclerEnabled(value:Boolean):void {
			_textLineRecyclerEnabled = value;
		}


		/**
		 * <p> Add a TextLine to the pool for reuse. TextLines for reuse should have null userData and null parent. </p>
		 * 
		 * @param textLine
		 */
		public static function addLineForReuse(textLine:TextLine):void {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Return a TextLine from the pool for reuse. </p>
		 * 
		 * @return 
		 */
		public static function getLineForReuse():TextLine {
			//TODO: implement to optimise
			return null;
		}
	}
}
