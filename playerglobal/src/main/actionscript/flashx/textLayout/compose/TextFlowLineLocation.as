package flashx.textLayout.compose {
	/**
	 *  The TextFlowLineLocation class is an enumeration class that defines constants for specifying the location of a line within a paragraph. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  ParagraphElement
	 *  <br>TextFlow
	 * </div><br><hr>
	 */
	public class TextFlowLineLocation {
		/**
		 * <p> Specifies the first line in a paragraph. </p>
		 */
		public static const FIRST:uint = 1;
		/**
		 * <p> Specifies the last line in a paragraph. </p>
		 */
		public static const LAST:uint = 4;
		/**
		 * <p> Specifies a middle line in a paragraph - neither the first nor the last line. </p>
		 */
		public static const MIDDLE:uint = 2;
		/**
		 * <p> Specifies both the first and last lines in a paragraph. </p>
		 */
		public static const ONLY:uint = 5;
	}
}
