package flashx.textLayout.compose {
	/**
	 *  The IVerticalJustificationLine interface defines the methods and properties required to allow the vertical justification of text lines. <br><hr>
	 */
	public interface IVerticalJustificationLine {

		/**
		 * <p> Specifies the number of pixels from the baseline to the top of the tallest characters in the line. For a TextLine that contains only a graphic element, <code>ascent</code> is set to 0. </p>
		 * 
		 * @return 
		 */
		function get ascent():Number;

		/**
		 * <p> Specifies the number of pixels from the baseline to the bottom of the lowest-descending characters in the line. For a TextLine that contains only a graphic element, <code>descent</code> is set to 0. </p>
		 * 
		 * @return 
		 */
		function get descent():Number;

		/**
		 * <p> The height of the line in pixels. </p>
		 * 
		 * @return 
		 */
		function get height():Number;

		/**
		 * <p> The horizontal position of the line relative to its container, expressed as the offset in pixels from the left of the container. </p>
		 * 
		 * @return 
		 */
		function get x():Number;

		/**
		 * @param value
		 * @return 
		 */
		function set x(value:Number):void;

		/**
		 * <p> The vertical position of the line relative to its container, expressed as the offset in pixels from the top of the container. </p>
		 * 
		 * @return 
		 */
		function get y():Number;

		/**
		 * @param value
		 * @return 
		 */
		function set y(value:Number):void;
	}
}
