package flashx.textLayout.compose {
	import flash.geom.Rectangle;
	import flash.text.engine.TextLine;

	import flashx.textLayout.container.ContainerController;
	import flashx.textLayout.elements.ParagraphElement;

	/**
	 *  The TextFlowLine class represents a single line of text in a text flow. <p>Use this class to access information about how a line of text has been composed: its position, height, width, and so on. When the text flow (TextFlow) is modified, the lines immediately before and at the site of the modification are marked as invalid because they need to be recomposed. Lines after the site of the modification might not be damaged immediately, but they might be regenerated once the text is composed. You can access a TextFlowLine that has been damaged, but any values you access reflect the old state of the TextFlow. When the TextFlow is recomposed, it generates new lines and you can get the new line for a given position by calling <code>TextFlow.flowComposer.findLineAtPosition()</code>.</p> <br><hr>
	 */
	public class TextFlowLine implements IVerticalJustificationLine {
		private var _x:Number;
		private var _y:Number;

		private var _absoluteStart:int;
		private var _ascent:Number;
		private var _columnIndex:int;
		private var _controller:ContainerController;
		private var _descent:Number;
		private var _height:Number;
		private var _lineOffset:Number;
		private var _location:int;
		private var _paragraph:ParagraphElement;
		private var _spaceAfter:Number;
		private var _spaceBefore:Number;
		private var _textHeight:Number;
		private var _textLength:int;
		private var _textLineExists:Boolean;
		private var _unjustifiedTextWidth:Number;
		private var _validity:String;

		/**
		 * <p> Constructor - creates a new TextFlowLine instance. </p>
		 * <p><b>Note</b>: No client should call this. It's exposed for writing your own composer.</p>
		 * 
		 * @param textLine  — The TextLine display object to use for this line. 
		 * @param paragraph  — The paragraph (ParagraphElement) in which to place the line. 
		 * @param outerTargetWidth  — The width the line is composed to, excluding indents. 
		 * @param lineOffset  — The line's offset in pixels from the appropriate container inset (as dictated by paragraph direction and container block progression), prior to alignment of lines in the paragraph. 
		 * @param absoluteStart  — The character position in the text flow at which the line begins. 
		 * @param numChars  — The number of characters in the line. 
		 */
		public function TextFlowLine(textLine:TextLine, paragraph:ParagraphElement, outerTargetWidth:Number = 0, lineOffset:Number = 0, absoluteStart:int = 0, numChars:int = 0) {
			throw new Error("Not implemented");
		}

		/**
		 * <p> The location of the line as an absolute character position in the TextFlow object. </p>
		 * 
		 * @return 
		 */
		public function get absoluteStart():int {
			return _absoluteStart;
		}

		/**
		 * <p> Specifies the number of pixels from the baseline to the top of the tallest characters in the line. For a TextLine that contains only a graphic element, <code>ascent</code> is set to 0. </p>
		 * 
		 * @return 
		 */
		public function get ascent():Number {
			return _ascent;
		}

		/**
		 * <p> The number of the column in which the line has been placed, with the first column being 0. </p>
		 * 
		 * @return 
		 */
		public function get columnIndex():int {
			return _columnIndex;
		}

		/**
		 * <p> The controller (ContainerController object) for the container in which the line has been placed. </p>
		 * 
		 * @return 
		 */
		public function get controller():ContainerController {
			return _controller;
		}

		/**
		 * <p> Specifies the number of pixels from the baseline to the bottom of the lowest-descending characters in the line. For a TextLine that contains only a graphic element, <code>descent</code> is set to 0. </p>
		 * 
		 * @return 
		 */
		public function get descent():Number {
			return _descent;
		}

		/**
		 * <p> The height of the line in pixels. </p>
		 * 
		 * @return 
		 */
		public function get height():Number {
			return _height;
		}

		/**
		 * <p> The line's offset in pixels from the appropriate container inset (as dictated by paragraph direction and container block progression), prior to alignment of lines in the paragraph. </p>
		 * 
		 * @return 
		 */
		public function get lineOffset():Number {
			return _lineOffset;
		}

		/**
		 * <p> One of the values from TextFlowLineLocation for specifying a line's location within a paragraph. </p>
		 * 
		 * @return 
		 */
		public function get location():int {
			return _location;
		}

		/**
		 * <p> The paragraph (ParagraphElement) in which the line resides. </p>
		 * 
		 * @return 
		 */
		public function get paragraph():ParagraphElement {
			return _paragraph;
		}

		/**
		 * <p> The amount of space to leave after the line. </p>
		 * <p>If the line is the last line of a paragraph that has a space-after, the line will have a <code>spaceAfter</code> value. If the line comes at the bottom of a column, then the <code>spaceAfter</code> is ignored. Otherwise, the line comes before another line in the column, and the following line must be positioned vertically to insure that there is at least this much space left between this last line of the paragraph and the first line of the following paragraph.</p>
		 * 
		 * @return 
		 */
		public function get spaceAfter():Number {
			return _spaceAfter;
		}

		/**
		 * <p> The amount of space to leave before the line. </p>
		 * <p>If the line is the first line of a paragraph that has a space-before applied, the line will have a <code>spaceBefore</code> value. If the line comes at the top of a column, <code>spaceBefore</code> is ignored. Otherwise, the line follows another line in the column, and it is positioned vertically to insure that there is at least this much space left between this line and the last line of the preceding paragraph.</p>
		 * 
		 * @return 
		 */
		public function get spaceBefore():Number {
			return _spaceBefore;
		}

		/**
		 * <p> The height of the text line, which is equal to <code>ascent</code> plus <code>descent</code>. The value is calculated based on the difference between the baselines that bound the line, either ideographic top and bottom or ascent and descent depending on whether the baseline at y=0 is ideographic (for example, TextBaseline.IDEOGRAPHIC_TOP) or not. </p>
		 * 
		 * @return 
		 */
		public function get textHeight():Number {
			return _textHeight;
		}

		/**
		 * <p> The number of characters to the next line, including trailing spaces. </p>
		 * 
		 * @return 
		 */
		public function get textLength():int {
			return _textLength;
		}

		/**
		 * <p> Indicates whether the <code>flash.text.engine.TextLine</code> object for this TextFlowLine exists. The value is <code>true</code> if the TextLine object has <i>not</i> been garbage collected and <code>false</code> if it has been. </p>
		 * 
		 * @return 
		 */
		public function get textLineExists():Boolean {
			return _textLineExists;
		}

		/**
		 * <p> The width of the line if it was not justified. For unjustified text, this value is the same as <code>textLength</code>. For justified text, this value is what the length would have been without justification, and <code>textLength</code> represents the actual line width. For example, when the following String is justified and assigned a width of 500, it has an actual width of 500 but an unjustified width of 268.9921875. </p>
		 * 
		 * @return 
		 */
		public function get unjustifiedTextWidth():Number {
			return _unjustifiedTextWidth;
		}

		/**
		 * <p> The validity of the line. </p>
		 * <p>A line can be invalid if the text, the attributes applied to it, or the controller settings have changed since the line was created. An invalid line can still be displayed, and you can use it, but the values used will be the values at the time it was created. The line returned by <code>getTextLine()</code> also will be in an invalid state. </p>
		 * 
		 * @return 
		 */
		public function get validity():String {
			return _validity;
		}

		/**
		 * <p> The horizontal position of the line relative to its container, expressed as the offset in pixels from the left of the container. </p>
		 * <p><b>Note: </b>Although this property is technically <code>read-write</code>, you should treat it as <code>read-only</code>. The setter exists only to satisfy the requirements of the IVerticalJustificationLine interface that defines both a getter and setter for this property. Use of the setter, though possible, will lead to unpredictable results. </p>
		 * 
		 * @return 
		 */
		public function get x():Number {
			return _x;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set x(value:Number):void {
			_x = value;
		}

		/**
		 * <p> The vertical position of the line relative to its container, expressed as the offset in pixels from the top of the container. </p>
		 * <p><b>Note: </b>Although this property is technically <code>read-write</code>, you should treat it as <code>read-only</code>. The setter exists only to satisfy the requirements of the IVerticalJustificationLine interface that defines both a getter and setter for this property. Use of the setter, though possible, will lead to unpredictable results. </p>
		 * 
		 * @return 
		 */
		public function get y():Number {
			return _y;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set y(value:Number):void {
			_y = value;
		}

		/**
		 * <p> Returns the bounds of the line as a rectangle. </p>
		 * 
		 * @return  — a rectangle that represents the boundaries of the line. 
		 */
		public function getBounds():Rectangle {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the <code>flash.text.engine.TextLine</code> object for this line, which might be recreated if it does not exist due to garbage collection. Set <code>forceValid</code> to <code>true</code> to cause the TextLine to be regenerated. Returns null if the TextLine cannot be recreated. </p>
		 * 
		 * @param forceValid  — if true, the TextLine is regenerated, if it exists but is invalid. 
		 * @return  — object for this line or  if the TextLine object cannot be recreated. 
		 */
		public function getTextLine(forceValid:Boolean = false):TextLine {
			throw new Error("Not implemented");
		}
	}
}
