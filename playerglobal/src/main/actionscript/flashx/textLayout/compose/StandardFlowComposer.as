package flashx.textLayout.compose {
	import flashx.textLayout.container.ContainerController;
	import flashx.textLayout.edit.ISelectionManager;
	import flashx.textLayout.elements.ContainerFormattedElement;

	/**
	 *  The StandardFlowComposer class provides a standard composer and container manager. <p>Each call to <code>compose()</code> or <code>updateAllControllers()</code> normalizes the text flow as a first step. The normalizing process checks the parts of the TextFlow object that were modified and takes the following steps: </p><ol> 
	 *  <li> Deletes empty FlowLeafElement and SubParagraphGroupElement objects.</li> 
	 *  <li> Merges sibling spans that have identical attributes.</li> 
	 *  <li> Adds an empty paragraph if a flow is empty.</li> 
	 * </ol>  <p>To use a StandardFlowComposer, assign it to the <code>flowComposer</code> property of a TextFlow object. Call the <code>updateAllControllers()</code> method to lay out and display the text in the containers attached to the flow composer.</p> <p> <b>Note:</b> For simple, static text flows, you can also use the one of the text line factory classes. These factory classes will typically create lines with less overhead than a flow composer, but do not support editing, dynamic changes, or user interaction.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS14c3067b34b57c6d4a97343b122ab36a52f-7ff2.html" target="_blank">Managing text containers with TLF</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flashx/textLayout/elements/TextFlow.html#flowComposer" target="">flashx.textLayout.elements.TextFlow.flowComposer</a>
	 * </div><br><hr>
	 */
	public class StandardFlowComposer extends FlowComposerBase implements IFlowComposer {
		private var _composing:Boolean;
		private var _numControllers:int;
		private var _rootElement:ContainerFormattedElement;

		/**
		 * <p> Creates a StandardFlowComposer object. </p>
		 * <p>To use an StandardFlowComposer object, assign it to the <code>flowComposer</code> property of a TextFlow object. Call the <code>updateAllControllers()</code> method to lay out and display the text in the containers attached to the flow composer.</p>
		 * <span id="pageFilter"><br><span class="label"> Example &nbsp;( <span class="usage"><a href="http://www.adobe.com/go/learn_as3_usingexamples_en"> How to use this example </a></span>) </span><br><br>
		 *  <div class="detailBody">
		 *    The following example composes a text flow up to and including the container holding the 345th position: 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>
		 * package flashx.textLayout.compose.examples
		 * {
		 *     import flashx.textLayout.elements.TextFlow;
		 *     import flashx.textLayout.compose.StandardFlowComposer;
		 * 
		 *     public class StandardFlowComposer_constructor
		 *     {
		 *         public function createComposer(textFlow:TextFlow):void
		 *         {
		 *              textFlow.flowComposer = new StandardFlowComposer();
		 *         }
		 *     }
		 * }
		 * </pre>
		 *   </div>
		 *  </div></span>
		 */
		public function StandardFlowComposer() {
			throw new Error("Not implemented");
		}

		/**
		 * <p> True, if the flow composer is currently performing a composition operation. </p>
		 * 
		 * @return 
		 */
		public function get composing():Boolean {
			return _composing;
		}

		/**
		 * <p> The number of containers assigned to this IFlowComposer instance. </p>
		 * 
		 * @return 
		 */
		public function get numControllers():int {
			return _numControllers;
		}

		/**
		 * <p> The root element associated with this IFlowComposer instance. </p>
		 * <p>Only a TextFlow object can be a root element.</p>
		 * 
		 * @return 
		 */
		public function get rootElement():ContainerFormattedElement {
			return _rootElement;
		}

		/**
		 * <p> Adds a controller to this IFlowComposer instance. </p>
		 * <p>The container is added to the end of the container list.</p>
		 * 
		 * @param controller  — The ContainerController object to add. 
		 */
		public function addController(controller:ContainerController):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Adds a controller to this IFlowComposer instance at the specified index. </p>
		 * <p>The list of controllers is 0-based (the first controller has an index of 0).</p>
		 * 
		 * @param controller  — The ContainerController object to add. 
		 * @param index  — A numeric index that specifies the position in the controller list at which to insert the ContainerController object. 
		 */
		public function addControllerAt(controller:ContainerController, index:int):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Calculates how many lines are necessary to display the content in the root element of the flow and the positions of these lines in the flow's display containers. </p>
		 * <p>The <code>compose()</code> method only composes content if it has changed since the last composition operation. Results are saved so that subsequent calls to <code>compose()</code> or <code>updateAllControllers()</code> do not perform an additional recomposition if the flow content has not changed.</p>
		 * <p>If the contents of any container have changed, the method returns <code>true</code>.</p>
		 * 
		 * @return  — true if anything changed. 
		 */
		public function compose():Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Composes the content of the root element up to and including the container at the specified index. </p>
		 * <p>If the contents of any container up to and including the container at the specified index has changed, the method returns <code>true</code>. If <code>index</code> is greater than the number of controllers (or not specified), then all containers are composed.</p>
		 * 
		 * @param index  — compose at least up to this container in the TextFlow. If controllerIndex is greater than the number of controllers, compose to the end of the last container. 
		 * @return  — true if anything changed. 
		 */
		public function composeToController(index:int = NaN):Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Composes the content of the root element up to the specified position. </p>
		 * <p>If the contents of any container up to and including the container holding the content at the specified position has changed, the method returns <code>true</code>. If <code>absolutePosition</code> is greater than the length of the TextFlow (or not specified), then the entire flow is composed.</p>
		 * 
		 * @param absolutePosition  — compose at least up to this position in the TextFlow. By default or if absolutePosition is past the end of the flow compose to the end of the flow. 
		 * @return  — true if anything changed. 
		 */
		public function composeToPosition(absolutePosition:int = NaN):Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the index of the controller containing the content at the specified position. </p>
		 * <p>A position can be considered to be the division between two characters or other elements of a text flow. If the value in <code>absolutePosition</code> is a position between the last character of one container and the first character of the next, then the preceding container is returned if the <code>preferPrevious</code> parameter is set to <code>true</code> and the later container is returned if the <code>preferPrevious</code> parameter is set to <code>false</code>.</p>
		 * <p>The method returns -1 if the content at the specified position is not in any container or is outside the range of positions in the text flow.</p>
		 * 
		 * @param absolutePosition  — The position of the content for which the container index is sought. 
		 * @param preferPrevious  — Specifies which container index to return when the position is between the last element in one container and the first element in the next. 
		 * @return  — the index of the container controller or -1 if not found. 
		 */
		public function findControllerIndexAtPosition(absolutePosition:int, preferPrevious:Boolean = false):int {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the absolute position of the first content element in the specified ContainerController object. </p>
		 * <p>A position is calculated by counting the division between two characters or other elements of a text flow. The position preceding the first element of a flow is zero. An absolute position is the position counting from the beginning of the flow.</p>
		 * 
		 * @param controller  — A ContainerController object associated with this flow composer. 
		 * @return  — the position before the first character or graphic in the ContainerController. 
		 */
		public function getAbsoluteStart(controller:ContainerController):int {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the ContainerController object at the specified index. </p>
		 * 
		 * @param index  — The index of the ContainerController object to return. 
		 * @return  — the ContainerController object at the specified position. 
		 */
		public function getControllerAt(index:int):ContainerController {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the index of the specified ContainerController object. </p>
		 * 
		 * @param controller  — A reference to the ContainerController object to find. 
		 * @return  — the index of the specified ContainerController object or -1 if the controller is not attached to this flow composer. 
		 */
		public function getControllerIndex(controller:ContainerController):int {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Called by the TextFlow when the interaction manager changes. </p>
		 * <p>This function is called automatically. Your code does not typically need to call this method. Classes that extend StandardFlowComposer can override this method to update event listeners and other properties that depend on the interaction manager.</p>
		 * 
		 * @param newInteractionManager  — The new ISelectionManager instance. 
		 */
		public function interactionManagerChanged(newInteractionManager:ISelectionManager):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Removes all controllers from this IFlowComposer instance. </p>
		 */
		public function removeAllControllers():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Removes a controller from this IFlowComposer instance. </p>
		 * 
		 * @param controller  — The ContainerController instance to remove. 
		 */
		public function removeController(controller:ContainerController):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Removes the controller at the specified index from this IFlowComposer instance. </p>
		 * 
		 * @param index  — The index of the ContainerController object to remove. 
		 */
		public function removeControllerAt(index:int):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets the focus to the container that contains the location specified by the <code>absolutePosition</code> parameter. </p>
		 * <p>The StandardFlowComposer calls the <code>setFocus()</code> method of the ContainerController object containing the specified text flow position.</p>
		 * 
		 * @param absolutePosition  — Specifies the position in the text flow of the container to receive focus. 
		 * @param leanLeft  — If true and the position is before the first character in a container, sets focus to the end of the previous container. 
		 */
		public function setFocus(absolutePosition:int, leanLeft:Boolean = false):void {
			throw new Error("Not implemented");
		}

		/**
		 * @param newRootElement
		 */
		public function setRootElement(newRootElement:ContainerFormattedElement):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Composes the content of the root element and updates the display. </p>
		 * <p>Text layout is conducted in two phases: composition and display. In the composition phase, the flow composer calculates how many lines are necessary to display the content as well as the position of these lines in the flow's display containers. In the display phase, the flow composer updates the display object children of its containers. The <code>updateAllControllers()</code> method initiates both phases in sequence. The StandardFlowComposer keeps track of changes to content so that a full cycle of composition and display is only performed when necessary.</p>
		 * <p>This method updates all the text lines and the display list immediately and synchronously.</p>
		 * <p>If the contents of any container is changed, the method returns <code>true</code>.</p>
		 * 
		 * @return  — true if anything changed. 
		 */
		public function updateAllControllers():Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Composes and updates the display up to and including the container at the specified index. </p>
		 * <p>The <code>updateToController()</code> method composes the content and updates the display of all containers up to and including the container at the specified index. For example, if you have a chain of 20 containers and specify an index of 10, <code>updateToController()</code> ensures that the first through the tenth (indexes 0-9) containers are composed and displayed. Composition stops at that point. If <code>controllerIndex</code> is -1 (or not specified), then all containers are updated.</p>
		 * <p>This method updates all the text lines and the display list immediately and synchronously.</p>
		 * <p>If the contents of any container is changed, the method returns <code>true</code>.</p>
		 * 
		 * @param index  — index of the last container to update (by default updates all containers) 
		 * @return  — , if anything changed. 
		 */
		public function updateToController(index:int = NaN):Boolean {
			throw new Error("Not implemented");
		}
	}
}
