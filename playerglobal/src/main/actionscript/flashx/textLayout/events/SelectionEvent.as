package flashx.textLayout.events {
	import flash.events.Event;

	import flashx.textLayout.edit.SelectionState;

	/**
	 *  A TextFlow instance dispatches a SelectionEvent object when an EditManager or SelectionManager changes or selects a range of text. For example, this event is dispatched not only when a range of text is selected, but also when the selection changes because the user clicks elsewhere in the text flow. Moreover, this event is also dispatched when an EditManager changes the text or text formatting within a range of text. <p><a href="#includeExamplesSummary">View the examples</a></p><br><hr>
	 */
	public class SelectionEvent extends flash.events.Event {
		/**
		 * <p> The SelectionEvent.SELECTION_CHANGE constant defines the value of the type property of the event object for a selection event. </p>
		 */
		public static const SELECTION_CHANGE:String = "selectionChange";

		private var _selectionState:SelectionState;

		/**
		 * <p> Creates an event object that contains information about a flow operation. </p>
		 * 
		 * @param type  — The type of the event. Event listeners can access this information through the inherited <code>type</code> property. There is only one type of SelectionEvent: <code>SelectionEvent.SELECTION_CHANGE</code>; 
		 * @param bubbles  — Indicates whether an event is a bubbling event.This event does not bubble. 
		 * @param cancelable  — Indicates whether the behavior associated with the event can be prevented. 
		 * @param selectionState  — An object of type ElementRange that describes the range of text selected. 
		 */
		public function SelectionEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, selectionState:SelectionState = null) {
			super(type, bubbles, cancelable);
			throw new Error("Not implemented");
		}

		/**
		 * <p> An object of type SelectionState that represents the selected range associated with this SelectionEvent. </p>
		 * <p>You can use this property, along with the ElementRange class, to create an ElementRange instance that represents the range of selected text. You can use the following line of code to create an instance of the ElementRange class that represents the range of selected text (the <code>ev</code> variable represents the event object, and the conditional operator is used to guard against a <code>null</code> value for the <code>selectionState</code> property):</p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *          // Find selected element range
		 *          var range:ElementRange = ev.selectionState ?  
		 *              ElementRange.createElementRange(ev.selectionState.textFlow,
		 *              ev.selectionState.absoluteStart, ev.selectionState.absoluteEnd) : null;</pre>
		 * </div>
		 * 
		 * @return 
		 */
		public function get selectionState():SelectionState {
			return _selectionState;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set selectionState(value:SelectionState):void {
			_selectionState = value;
		}
	}
}
