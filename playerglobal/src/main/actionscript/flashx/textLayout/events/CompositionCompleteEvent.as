package flashx.textLayout.events {
	import flash.events.Event;

	import flashx.textLayout.elements.TextFlow;

	/**
	 *  A TextFlow instance dispatches this event after a compose operation completes. Each text container has two states: composition and display. This event notifies you when the composition phase has ended. This provides an opportunity to make any necessary and appropriate changes to the container before you display the text. For example, you can use this event to add highlighting of certain words or characters in the text flow before the text is displayed. <p>The three main methods that dispatch this event are <code>compose()</code>, <code>updateToController()</code>, and <code>updateAllControllers()</code>. All three of these methods are in the StandardFlowComposer class.</p> <p> <b>Note: </b>If the event is dispatched by the <code>updateAllControllers()</code> method, do not call <code>updateAllControllers()</code> again in your event handler function. Such a call would be a recursive call because the <code>updateAllControllers()</code> method executes both the composition and display steps. The <code>updateAllControllers()</code> dispatches this event after composition completes, but before the display step executes. The same reasoning applies to the <code>updateToController()</code> method. </p> <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flashx/textLayout/elements/TextFlow.html" target="">flashx.textLayout.elements.TextFlow</a>
	 * </div><br><hr>
	 */
	public class CompositionCompleteEvent extends flash.events.Event {
		/**
		 * <p> Defines the value of the <code>type</code> property of a <code>compositionComplete</code> event object </p>
		 */
		public static const COMPOSITION_COMPLETE:String = "compositionComplete";

		private var _compositionLength:int;
		private var _compositionStart:int;
		private var _textFlow:TextFlow;

		/**
		 * <p> Constructor </p>
		 * 
		 * @param type  — event type - use the static property COMPOSITION_COMPLETE. 
		 * @param bubbles  — Indicates whether an event is a bubbling event. This event does not bubble. 
		 * @param cancelable  — Indicates whether the behavior associated with the event can be prevented. This event cannot be cancelled. 
		 * @param textFlow  — The TextFlow which was composed 
		 * @param compositionStart  — start of composition, in terms of an index into the text flow. 
		 * @param compositionLength  — length number of characters composed 
		 */
		public function CompositionCompleteEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, textFlow:TextFlow = null, compositionStart:int = 0, compositionLength:int = 0) {
			super(type, bubbles, cancelable);
			throw new Error("Not implemented");
		}

		/**
		 * <p> The number of characters composed. </p>
		 * 
		 * @return 
		 */
		public function get compositionLength():int {
			return _compositionLength;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set compositionLength(value:int):void {
			_compositionLength = value;
		}

		/**
		 * <p> The start location of the text range affected by the composition, expressed as an index into the text flow. </p>
		 * 
		 * @return 
		 */
		public function get compositionStart():int {
			return _compositionStart;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set compositionStart(value:int):void {
			_compositionStart = value;
		}

		/**
		 * <p> TextFlow on which composition has been completed. </p>
		 * 
		 * @return 
		 */
		public function get textFlow():TextFlow {
			return _textFlow;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set textFlow(value:TextFlow):void {
			_textFlow = value;
		}
	}
}
