package flashx.textLayout.events {
	import flash.events.Event;

	import flashx.textLayout.elements.TextFlow;

	/**
	 *  A TextFlow instance dispatches this each time it is marked as damaged. Damage can be caused by changes to the model or changes to the layout. <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flashx/textLayout/elements/TextFlow.html" target="">flashx.textLayout.elements.TextFlow</a>
	 * </div><br><hr>
	 */
	public class DamageEvent extends flash.events.Event {
		public static const DAMAGE:String = "damage";

		private var _damageAbsoluteStart:int;
		private var _damageLength:int;
		private var _textFlow:TextFlow;

		/**
		 * <p> Constructor </p>
		 * 
		 * @param type  — text index of the start of the damage 
		 * @param bubbles  — length of text that was damaged 
		 * @param cancelable
		 * @param textFlow
		 * @param damageAbsoluteStart
		 * @param damageLength
		 */
		public function DamageEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, textFlow:TextFlow = null, damageAbsoluteStart:int = 0, damageLength:int = 0) {
			super(type, bubbles, cancelable);
			throw new Error("Not implemented");
		}

		/**
		 * <p> Absolute start of the damage </p>
		 * 
		 * @return 
		 */
		public function get damageAbsoluteStart():int {
			return _damageAbsoluteStart;
		}

		/**
		 * <p> Length of the damage </p>
		 * 
		 * @return 
		 */
		public function get damageLength():int {
			return _damageLength;
		}

		/**
		 * <p> TextFlow owning the damage </p>
		 * 
		 * @return 
		 */
		public function get textFlow():TextFlow {
			return _textFlow;
		}
	}
}
