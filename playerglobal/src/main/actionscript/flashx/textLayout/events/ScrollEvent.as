package flashx.textLayout.events {
	/**
	 *  Represents events that are dispatched when the TextFlow does automatic scrolling. <br><hr>
	 */
	public class ScrollEvent extends TextLayoutEvent {
		private var _delta:Number;
		private var _direction:String;

		/**
		 * @param type  — The event type; indicates the action that caused the event. 
		 * @param bubbles  — Specifies whether the event can bubble up the display list hierarchy. 
		 * @param cancelable  — Specifies whether the behavior associated with the event can be prevented. 
		 * @param direction  — The change in scroll position, expressed in pixels. 
		 * @param delta
		 */
		public function ScrollEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, direction:String = null, delta:Number = NaN) {
			super(type, bubbles, cancelable);
			throw new Error("Not implemented");
		}

		/**
		 * <p> The change in the scroll position value that resulted from the scroll. The value is expressed in pixels. A positive value indicates the scroll was down or to the right. A negative value indicates the scroll was up or to the left. </p>
		 * 
		 * @return 
		 */
		public function get delta():Number {
			return _delta;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set delta(value:Number):void {
			_delta = value;
		}

		/**
		 * <p> The direction of motion. The possible values are <code>ScrollEventDirection.VERTICAL</code> or <code>ScrollEventDirection.HORIZONTAL</code>. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="ScrollEventDirection.html" target="">flashx.textLayout.events.ScrollEventDirection</a>
		 * </div>
		 * 
		 * @return 
		 */
		public function get direction():String {
			return _direction;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set direction(value:String):void {
			_direction = value;
		}
	}
}
