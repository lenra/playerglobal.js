package flashx.textLayout.events {
	import flash.events.Event;

	/**
	 *  A TextLayoutEvent instance represents an event, such as the <code>TextLayoutEvent.SCROLL</code> event, that does not require custom properties. <p>A scroll event is represented by a TextLayoutEvent instance with its <code>type</code> property set to <code>TextLayoutEvent.SCROLL</code>. A class specifically for scroll events is not necessary because there are no custom properties for a scroll event, as there are for the other events that have specific event classes. If a new text layout event is needed, and the event does not require custom properties, the new event will also be represented by a TextLayoutEvent object, but with its <code>type</code> property set to a new static constant. </p> <p><a href="#includeExamplesSummary">View the examples</a></p><br><hr>
	 */
	public class TextLayoutEvent extends flash.events.Event {
		/**
		 * <p> The <code>TextLayoutEvent.SCROLL</code> constant defines the value of the <code>type</code> property of the event object for a <code>scroll</code> event. </p>
		 */
		public static const SCROLL:String = "scroll";

		/**
		 * <p> The TextLayoutEvent class represents the event object passed to the event listener for many Text Layout events. </p>
		 * 
		 * @param type
		 * @param bubbles
		 * @param cancelable
		 */
		public function TextLayoutEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false) {
			super(type, bubbles, cancelable);
			throw new Error("Not implemented");
		}
	}
}
