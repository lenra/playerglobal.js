package flashx.textLayout.events {
	/**
	 *  Constants for the values of the <code>direction</code> property of a ScrollEvent. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="ScrollEvent.html" target="">flashx.textLayout.events.ScrollEvent</a>
	 * </div><br><hr>
	 */
	public class ScrollEventDirection {
		/**
		 * <p> The user scrolled horizontally. </p>
		 */
		public static const HORIZONTAL:String = "horizontal";
		/**
		 * <p> The user scrolled vertically. </p>
		 */
		public static const VERTICAL:String = "vertical";
	}
}
