package flashx.textLayout.events {
	import flash.events.Event;

	import flashx.textLayout.container.ContainerController;
	import flashx.textLayout.elements.TextFlow;

	/**
	 *  A TextFlow instance dispatches this event after any of its containers completes an update. Each text container has two states: composition and display. This event notifies you when the display phase has ended. This provides an opportunity to make any necessary changes to the container when it is ready to be displayed, but hasn't yet been painted to the screen. <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flashx/textLayout/elements/TextFlow.html" target="">flashx.textLayout.elements.TextFlow</a>
	 * </div><br><hr>
	 */
	public class UpdateCompleteEvent extends flash.events.Event {
		/**
		 * <p> Defines the value of the <code>type</code> property of an <code>UpdateCompleteEvent</code> object </p>
		 */
		public static const UPDATE_COMPLETE:String = "updateComplete";

		private var _controller:ContainerController;
		private var _textFlow:TextFlow;

		/**
		 * <p> Constructor </p>
		 * 
		 * @param type  — event type - use the static property UPDATE_COMPLETE. 
		 * @param bubbles  — Indicates whether an event is a bubbling event. This event does not bubble. 
		 * @param cancelable  — Indicates whether the behavior associated with the event can be prevented. This event cannot be cancelled. 
		 * @param textFlow  — The ContainerController whose container was updated 
		 * @param controller  — The TextFlow which was updated 
		 */
		public function UpdateCompleteEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, textFlow:TextFlow = null, controller:ContainerController = null) {
			super(type, bubbles, cancelable);
			throw new Error("Not implemented");
		}

		/**
		 * <p> The controller of the container being updated </p>
		 * 
		 * @return 
		 */
		public function get controller():ContainerController {
			return _controller;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set controller(value:ContainerController):void {
			_controller = value;
		}

		/**
		 * <p> TextFlow which has been updated. </p>
		 * 
		 * @return 
		 */
		public function get textFlow():TextFlow {
			return _textFlow;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set textFlow(value:TextFlow):void {
			_textFlow = value;
		}
	}
}
