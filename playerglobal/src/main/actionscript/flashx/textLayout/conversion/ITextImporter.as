package flashx.textLayout.conversion {
	import flashx.textLayout.elements.IConfiguration;
	import flashx.textLayout.elements.TextFlow;

	/**
	 *  Interface for importing text content into a TextFlow from an external source. The TextConverter class creates importers with no constructor arguments. <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flashx/textLayout/elements/TextFlow.html" target="">flashx.textLayout.elements.TextFlow</a>
	 * </div><br><hr>
	 */
	public interface ITextImporter {

		/**
		 * <p> The <code>configuration</code> property contains the IConfiguration instance that the importer needs when creating new TextFlow instances. This property is initially set to <code>null</code>. </p>
		 * 
		 * @return 
		 */
		function get configuration():IConfiguration;

		/**
		 * @param value
		 * @return 
		 */
		function set configuration(value:IConfiguration):void;

		/**
		 * <p> This property contains a vector of error messages as strings after a call to an importer method is the <code>throwOnError</code> property is set to <code>false</code>, which is the default. If there were no errors, the property returns <code>null</code>. The property is reset on each method call. </p>
		 * 
		 * @return 
		 */
		function get errors():Vector.<String>;

		/**
		 * <p> The <code>throwOnError</code> property controls how the importer handles errors. If set to <code>true</code>, methods throw an Error instance on errors. If set to <code>false</code>, which is the default, errors are collected into a vector of strings and stored in the <code>errors</code> property, and the importer does not throw. </p>
		 * 
		 * @return 
		 */
		function get throwOnError():Boolean;

		/**
		 * @param value
		 * @return 
		 */
		function set throwOnError(value:Boolean):void;

		/**
		 * <p> Controls whether or not the importer should handle the extra information necessary for the clipboard. When data comes in from the clipboard, it might contain partial paragraphs; paragraphs that are missing the terminator or newline character. If <code>useClipboardAnnotations</code> is <code>true</code>, the importer marks these partial paragraphs with a <code>ConverterBase.MERGE_TO_NEXT_ON_PASTE</code> attribute. This causes the paste operation to correctly handle merging of the pasted paragraph (and any list or div elements that may include the paragraph) into the text. </p>
		 * 
		 * @return 
		 */
		function get useClipboardAnnotations():Boolean;

		/**
		 * @param value
		 * @return 
		 */
		function set useClipboardAnnotations(value:Boolean):void;

		/**
		 * <p> Import text content from an external source and convert it into a TextFlow. </p>
		 * 
		 * @param source  — The data to convert. 
		 * @return  — TextFlow Created from the source. 
		 */
		function importToFlow(source:Object):TextFlow;
	}
}
