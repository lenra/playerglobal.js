package flashx.textLayout.conversion {
	/**
	 *  This interface should be implemented by converters that import HTML or HTML-structured data. Clients that have explicitly created an importer using TextConverter.getImporter may control the import process by calling into these methods on the importer. <br><hr>
	 */
	public interface IHTMLImporter {

		/**
		 * <p> This property allows specification of a function to modify the source property supplied to an <code>&lt;img&gt;</code> element. Sample use would be to modify relative paths to some caller specified root path. The function takes the string set in the markup and returns the actual string to be used. </p>
		 * <p>Note that by default relative paths are relative to the loaded SWF. One use of this function is to make relative paths relative to some other location.</p>
		 * <p>The resolver function should look like this:</p>
		 * <code>function resolver(src:String):String</code>
		 * <p>It takes as an input parameter the value of src on the incoming img element, and returns the adjusted value.</p>
		 * 
		 * @return 
		 */
		function get imageSourceResolveFunction():Function;

		/**
		 * @param value
		 * @return 
		 */
		function set imageSourceResolveFunction(value:Function):void;

		/**
		 * <p> This property tells the importer to create an element for the <code>BODY</code> tag in HTML markup. </p>
		 * <p>The element will normally be a <code>DivElement</code> with <code>typeName</code> set to <code>BODY</code>.</p>
		 * <p>This will also trigger parsing of <code>class</code> and <code>id</code> on the element.</p>
		 * 
		 * @return 
		 */
		function get preserveBodyElement():Boolean;

		/**
		 * @param value
		 * @return 
		 */
		function set preserveBodyElement(value:Boolean):void;

		/**
		 * <p> This property tells the importer to create an element for the <code>HTML</code> tag in HTML markup. </p>
		 * <p>The element will normally be the top-level <code>TextFlow</code> element with <code>typeName</code> set to <code>HTML</code>.</p>
		 * <p>This will also trigger parsing of <code>class</code> and <code>id</code> on the element.</p>
		 * 
		 * @return 
		 */
		function get preserveHTMLElement():Boolean;

		/**
		 * @param value
		 * @return 
		 */
		function set preserveHTMLElement(value:Boolean):void;
	}
}
