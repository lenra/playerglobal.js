package flashx.textLayout.conversion {
	import flashx.textLayout.elements.TextFlow;

	/**
	 *  Interface for exporting text content from a TextFlow instance to a given format, which may for example be String or XML format, or a user-defined format. Exporters support the getting and setting of properties to control the export of data. These properties are implemented as public properties, but the direct access of these properties should be avoided, since a user might replace the converter class in the TextConverter registry, causing a downcast to fail. <br><hr>
	 */
	public interface ITextExporter {

		/**
		 * <p> This property contains a vector of error messages as strings after a call to an exporter method is the <code>throwOnError</code> property is set to <code>false</code>, which is the default. If there were no errors, the property returns <code>null</code>. The property is reset on each method call. </p>
		 * 
		 * @return 
		 */
		function get errors():Vector.<String>;

		/**
		 * <p> The throwOnError property controls how the exporter handles errors. If set to <code>true</code>, methods throw an Error instance on errors. If set to <code>false</code>, which is the default, errors are collected into a vector of strings and stored in the <code>errors</code> property, and the exporter does not throw. </p>
		 * 
		 * @return 
		 */
		function get throwOnError():Boolean;

		/**
		 * @param value
		 * @return 
		 */
		function set throwOnError(value:Boolean):void;

		/**
		 * <p> The <code>useClipboardAnnotations</code> property controls whether or not the importer should handle the extra information necessary for the clipboard. When data is in a TextFlow, paragraphs are always complete, and include a terminator character. When a range of text is pasted to the clipboard, it will form paragraphs, but the range may not include in the final terminator. In this case, the paragraph needs to be marked as a partial paragraph if it is intended for the clipboard, so that if it is later pasted it will merge into the new text correctly. If the content is intended for the clipboard, useClipboardAnnotations will be true. </p>
		 * 
		 * @return 
		 */
		function get useClipboardAnnotations():Boolean;

		/**
		 * @param value
		 * @return 
		 */
		function set useClipboardAnnotations(value:Boolean):void;

		/**
		 * <p> Export text content from a TextFlow instance in String, or XML, or a user defined format. </p>
		 * <p>Set the <code>conversionType</code> parameter to either of the following values, or a user defined format in user-defined exporters. </p>
		 * <ul>
		 *  <li><code>flashx.textLayout.conversion.ConversionType.STRING_TYPE</code>;</li>
		 *  <li><code>flashx.textLayout.conversion.ConversionType.XML_TYPE</code>.</li>
		 * </ul>
		 * 
		 * @param source  — The TextFlow to export 
		 * @param conversionType  — Return a String (STRING_TYPE) or XML (XML_TYPE), or any user defined format. 
		 * @return  — Object The exported content 
		 */
		function export(source:TextFlow, conversionType:String):Object;
	}
}
