package flashx.textLayout.conversion {
	import flashx.textLayout.elements.TextFlow;

	/**
	 *  Export converter for plain text format. This class provides an alternative to the <code>TextConverter.export()</code> static method for exporting plain text. The PlainTextExporter class's <code>export()</code> method results in the same output string as the <code>TextConverter.export()</code> static method if the two properties of the PlainTextExporter class, the <code>PARAGRAPH_SEPARATOR_PROPERTY</code> and the <code>STRIP_DISCRETIONARY_HYPHENS_PROPERTY</code> properties, contain their default values of <code>"\n"</code> and <code>true</code>, respectively. <p><a href="#includeExamplesSummary">View the examples</a></p><br><hr>
	 */
	public class PlainTextExporter extends ConverterBase implements IPlainTextExporter {
		private var _paragraphSeparator:String;
		private var _stripDiscretionaryHyphens:Boolean;

		/**
		 * <p> Constructor </p>
		 */
		public function PlainTextExporter() {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Specifies the character sequence used (in a text flow's plain-text equivalent) to separate paragraphs. The paragraph separator is not added after the last paragraph. </p>
		 * <p>This property applies to the <code>PLAIN_TEXT_FORMAT</code> exporter.</p>
		 * <p>The default value is "\n".</p>
		 * 
		 * @return 
		 */
		public function get paragraphSeparator():String {
			return _paragraphSeparator;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set paragraphSeparator(value:String):void {
			_paragraphSeparator = value;
		}

		/**
		 * <p> This property indicates whether discretionary hyphens in the text should be stripped during the export process. Discretionary hyphens, also known as "soft hyphens", indicate where to break a word in case the word must be split between two lines. The Unicode character for discretionary hyphens is <code>\u00AD</code>. </p>
		 * <p>If this property is set to <code>true</code>, discretionary hyphens that are in the original text will not be in the exported text, even if they are part of the original text. If <code>false</code>, discretionary hyphens will be in the exported text.</p>
		 * 
		 * @return 
		 */
		public function get stripDiscretionaryHyphens():Boolean {
			return _stripDiscretionaryHyphens;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set stripDiscretionaryHyphens(value:Boolean):void {
			_stripDiscretionaryHyphens = value;
		}

		/**
		 * @param source  — The TextFlow to export 
		 * @param conversionType  — Return a String (STRING_TYPE) or XML (XML_TYPE), or any user defined format. 
		 * @return  — Object The exported content 
		 */
		public function export(source:TextFlow, conversionType:String):Object {
			throw new Error("Not implemented");
		}
	}
}
