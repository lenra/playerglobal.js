package flashx.textLayout.conversion {
	/**
	 *  Contains information about a format. <br><hr>
	 */
	public class FormatDescriptor {
		private var _clipboardFormat:String;
		private var _exporterClass:Class;
		private var _format:String;
		private var _importerClass:Class;

		/**
		 * <p> Constructor. </p>
		 * 
		 * @param format
		 * @param importerClass
		 * @param exporterClass
		 * @param clipboardFormat
		 */
		public function FormatDescriptor(format:String, importerClass:Class, exporterClass:Class, clipboardFormat:String) {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Descriptor used when matching this format to the formats posted on the external clipboard. If the format supports importing, (it's importerClass is not null), it will be called when pasting from the clipboard, if the clipboard contents include data in this format. If the format supports exporting, it will be called when copying to the clipboard, and the output it creates will be posted to the clipboard with this clipboardFormat. </p>
		 * 
		 * @return 
		 */
		public function get clipboardFormat():String {
			return _clipboardFormat;
		}

		/**
		 * <p> Returns the class used for converting to the format. </p>
		 * 
		 * @return 
		 */
		public function get exporterClass():Class {
			return _exporterClass;
		}

		/**
		 * <p> Returns the data format used by the converter. </p>
		 * 
		 * @return 
		 */
		public function get format():String {
			return _format;
		}

		/**
		 * <p> Returns the class used for converting data from the format. </p>
		 * 
		 * @return 
		 */
		public function get importerClass():Class {
			return _importerClass;
		}
	}
}
