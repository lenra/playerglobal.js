package flashx.textLayout.conversion {
	/**
	 *  This interface should be implemented by converters that export plain text. Clients that have explicitly created an exporter using TextConverter.getExporter may control the export process by calling into these methods on the exporter. <br><hr>
	 */
	public interface IPlainTextExporter {

		/**
		 * <p> Specifies the character sequence used (in a text flow's plain-text equivalent) to separate paragraphs. The paragraph separator is not added after the last paragraph. </p>
		 * <p>This property applies to the <code>PLAIN_TEXT_FORMAT</code> exporter.</p>
		 * <p>The default value is "\n".</p>
		 * 
		 * @return 
		 */
		function get paragraphSeparator():String;

		/**
		 * @param value
		 * @return 
		 */
		function set paragraphSeparator(value:String):void;

		/**
		 * <p> This property indicates whether discretionary hyphens in the text should be stripped during the export process. Discretionary hyphens, also known as "soft hyphens", indicate where to break a word in case the word must be split between two lines. The Unicode character for discretionary hyphens is <code>\u00AD</code>. </p>
		 * <p>If this property is set to <code>true</code>, discretionary hyphens that are in the original text will not be in the exported text, even if they are part of the original text. If <code>false</code>, discretionary hyphens will be in the exported text.</p>
		 * 
		 * @return 
		 */
		function get stripDiscretionaryHyphens():Boolean;

		/**
		 * @param value
		 * @return 
		 */
		function set stripDiscretionaryHyphens(value:Boolean):void;
	}
}
