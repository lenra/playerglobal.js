package flashx.textLayout.conversion {
	/**
	 *  Values for the format of exported text. The values <code>STRING_TYPE</code> and <code>XML_TYPE</code> can be used for the <code>conversionType</code> parameter for the export() method in the ITextExporter interface and the TextConverter class. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="ITextExporter.html#export()" target="">flashx.textLayout.conversion.ITextExporter.export()</a>
	 *  <br>
	 *  <a href="TextConverter.html#export()" target="">flashx.textLayout.conversion.TextConverter.export()</a>
	 * </div><br><hr>
	 */
	public class ConversionType {
		/**
		 * <p> Export as type String. </p>
		 */
		public static const STRING_TYPE:String = "stringType";
		/**
		 * <p> Export as type XML. </p>
		 */
		public static const XML_TYPE:String = "xmlType";
	}
}
