package flashx.textLayout.conversion {
	/**
	 *  This interface should be implemented by converters that import TextLayout structured data. Clients that have explicitly created an importer using TextConverter.getImporter may control the import process by calling into these methods on the importer. <br><hr>
	 */
	public interface ITextLayoutImporter {

		/**
		 * <p> This property allows specification of a function to modify the source property supplied to an <code>&lt;img&gt;</code> element. Sample use would be to modify relative paths to some caller specified root path. The function takes the string set in the markup and returns the actual string to be used. </p>
		 * <p>Note that by default relative paths are relative to the loaded SWF. One use of this function is to make relative paths relative to some other location.</p>
		 * <p>The resolver function should look like this:</p>
		 * <code>function resolver(src:String):String</code>
		 * <p>It takes as an input parameter the value of src on the incoming img element, and returns the adjusted value.</p>
		 * 
		 * @return 
		 */
		function get imageSourceResolveFunction():Function;

		/**
		 * @param value
		 * @return 
		 */
		function set imageSourceResolveFunction(value:Function):void;
	}
}
