package flashx.textLayout {
	/**
	 *  This class controls the backward-compatibility of the framework. With every new release, some aspects of the framework are changed which can affect your application. <br><hr>
	 */
	public class TextLayoutVersion {
		/**
		 * <p> The current released version of the Text Layout Framework, encoded as a uint. </p>
		 */
		public static const CURRENT_VERSION:uint = 0x02000000;
		/**
		 * <p> The version number value of TLF 1.0, encoded numerically as a <code>uint</code>. </p>
		 */
		public static const VERSION_1_0:uint = 0x01000000;
		/**
		 * <p> The version number value of TLF 1.1, encoded numerically as a <code>uint</code>. </p>
		 */
		public static const VERSION_1_1:uint = 0x01010000;
		/**
		 * <p> The version number value of TLF 2.0, encoded numerically as a <code>uint</code>. </p>
		 */
		public static const VERSION_2_0:uint = 0x02000000;
	}
}
