package flashx.textLayout.utils {
	import flashx.textLayout.elements.TextFlow;
	import flashx.textLayout.elements.TextRange;

	/**
	 *  Utilities for manipulating a TextRange The methods of this class are static and must be called using the syntax <code>NavigationUtil.method(<i>parameter</i>)</code>. <br><hr>
	 */
	public class NavigationUtil {


		/**
		 * <p> Sets the TextRange at the end of the document. </p>
		 * 
		 * @param range  — Indicates that only activeIndex should move 
		 * @param extendSelection
		 * @return  — true if selection changed. 
		 */
		public static function endOfDocument(range:TextRange, extendSelection:Boolean = false):Boolean {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Sets the TextRange at the end of the line. </p>
		 * 
		 * @param range  — Indicates that only activeIndex should move 
		 * @param extendSelection
		 * @return  — true if selection changed. 
		 */
		public static function endOfLine(range:TextRange, extendSelection:Boolean = false):Boolean {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Sets the TextRange at the end of the paragraph. </p>
		 * 
		 * @param range  — Indicates that only activeIndex should move 
		 * @param extendSelection
		 * @return  — true if selection changed. 
		 */
		public static function endOfParagraph(range:TextRange, extendSelection:Boolean = false):Boolean {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Returns the absolute position of the next atom. </p>
		 * 
		 * @param flowRoot
		 * @param absolutePos
		 * @return 
		 */
		public static function nextAtomPosition(flowRoot:TextFlow, absolutePos:int):int {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Sets the TextRange forward by one character. </p>
		 * 
		 * @param range  — Indicates that only activeIndex should move 
		 * @param extendSelection
		 * @return  — true if selection changed. 
		 */
		public static function nextCharacter(range:TextRange, extendSelection:Boolean = false):Boolean {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Sets the TextRange down one line </p>
		 * 
		 * @param range  — Indicates that only activeIndex should move 
		 * @param extendSelection
		 * @return  — true if selection changed. 
		 */
		public static function nextLine(range:TextRange, extendSelection:Boolean = false):Boolean {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Sets the TextRange down one page. </p>
		 * 
		 * @param range  — Indicates that only activeIndex should move 
		 * @param extendSelection
		 * @return  — true if selection changed. 
		 */
		public static function nextPage(range:TextRange, extendSelection:Boolean = false):Boolean {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Sets the TextRange forward by one word. </p>
		 * 
		 * @param range  — Indicates that only activeIndex should move 
		 * @param extendSelection
		 * @return  — true if selection changed. 
		 */
		public static function nextWord(range:TextRange, extendSelection:Boolean = false):Boolean {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Returns the absolute position of the beginning of the next word. </p>
		 * 
		 * @param flowRoot
		 * @param absolutePos
		 * @return 
		 */
		public static function nextWordPosition(flowRoot:TextFlow, absolutePos:int):int {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Returns the absolute position of the previous atom. </p>
		 * 
		 * @param flowRoot
		 * @param absolutePos
		 * @return 
		 */
		public static function previousAtomPosition(flowRoot:TextFlow, absolutePos:int):int {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Sets the TextRange backward by one character. </p>
		 * 
		 * @param range  — Indicates that only activeIndex should move 
		 * @param extendSelection
		 * @return  — true if selection changed. 
		 */
		public static function previousCharacter(range:TextRange, extendSelection:Boolean = false):Boolean {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Sets the TextRange up one line. </p>
		 * 
		 * @param range  — Indicates that only activeIndex should move 
		 * @param extendSelection
		 * @return  — true if selection changed. 
		 */
		public static function previousLine(range:TextRange, extendSelection:Boolean = false):Boolean {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Sets the TextRange up one page. </p>
		 * 
		 * @param range  — Indicates that only activeIndex should move 
		 * @param extendSelection
		 * @return  — true if selection changed. 
		 */
		public static function previousPage(range:TextRange, extendSelection:Boolean = false):Boolean {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Sets the TextRange backward by one word. </p>
		 * 
		 * @param range  — Indicates that only activeIndex should move 
		 * @param extendSelection
		 * @return  — true if selection changed. 
		 */
		public static function previousWord(range:TextRange, extendSelection:Boolean = false):Boolean {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Returns absolute position of the beginning of the previous word. </p>
		 * 
		 * @param flowRoot
		 * @param absolutePos
		 * @return 
		 */
		public static function previousWordPosition(flowRoot:TextFlow, absolutePos:int):int {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Sets the TextRange at the beginning of the document. </p>
		 * 
		 * @param range  — Indicates that only activeIndex should move 
		 * @param extendSelection
		 * @return  — true if selection changed. 
		 */
		public static function startOfDocument(range:TextRange, extendSelection:Boolean = false):Boolean {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Sets the TextRange at the beginning of the line. </p>
		 * 
		 * @param range  — Indicates that only activeIndex should move 
		 * @param extendSelection
		 * @return  — true if selection changed. 
		 */
		public static function startOfLine(range:TextRange, extendSelection:Boolean = false):Boolean {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Sets the TextRange at the beginning of the paragraph. </p>
		 * 
		 * @param range  — Indicates that only activeIndex should move 
		 * @param extendSelection
		 * @return  — true if selection changed. 
		 */
		public static function startOfParagraph(range:TextRange, extendSelection:Boolean = false):Boolean {
			throw new Error("Not implemented");
		}
	}
}
