package flashx.textLayout.formats {
	/**
	 *  Defines values for setting the <code>textJustify</code> property of the TextLayoutFormat class. Default value is INTER_WORD, meaning that extra space in justification is added to the space characters. DISTRIBUTE specifies that extra space is added both to space characters and between individual letters. Use these values only when setting <code>justificationRule</code> to SPACE. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="TextLayoutFormat.html#textJustify" target="">TextLayoutFormat.textJustify</a>
	 *  <br>
	 *  <a href="TextLayoutFormat.html#justificationRule" target="">TextLayoutFormat.justificationRule</a>
	 * </div><br><hr>
	 */
	public class TextJustify {
		/**
		 * <p> Specifies that justification is to add space both to space characters and between individual letters. </p>
		 */
		public static const DISTRIBUTE:String = "distribute";
		/**
		 * <p> Specifies that justification is to add space to space characters. </p>
		 */
		public static const INTER_WORD:String = "interWord";
	}
}
