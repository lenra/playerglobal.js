package flashx.textLayout.formats {
	/**
	 *  This interface provides read access to tab stop-related properties. <br><hr>
	 */
	public interface ITabStopFormat {

		/**
		 * <p> The tab alignment for this tab stop. </p>
		 * <p>Legal values are TabAlignment.START, TabAlignment.CENTER, TabAlignment.END, TabAlignment.DECIMAL, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will have a value of TabAlignment.START.</p>
		 * 
		 * @return 
		 */
		function get alignment():*;

		/**
		 * <p> The alignment token to be used if the alignment is DECIMAL. </p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will have a value of null.</p>
		 * 
		 * @return 
		 */
		function get decimalAlignmentToken():*;

		/**
		 * <p> The position of the tab stop, in pixels, relative to the start edge of the column. </p>
		 * <p>Legal values are numbers from 0 to 10000 and FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will have a value of 0.</p>
		 * 
		 * @return 
		 */
		function get position():*;

		/**
		 * <p> Return the value of the style specified by the <code>styleProp</code> parameter which specifies the style name. </p>
		 * 
		 * @param styleName  — The name of the style whose value is to be retrieved. 
		 * @return  — The value of the specified style. The type varies depending on the type of the style being accessed. Returns  if the style is not set. 
		 */
		function getStyle(styleName:String):*;
	}
}
