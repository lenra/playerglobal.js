package flashx.textLayout.formats {
	/**
	 *  The TabStopFormat class represents the properties of a tab stop in a paragraph. You can set the <code>TextLayoutFormat.tabstops</code> property to an array of TabStopFormat objects. <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flashx/textLayout/elements/TabElement.html" target="">flashx.textLayout.elements.TabElement</a>
	 *  <br>
	 *  <a href="TextLayoutFormat.html#tabStops" target="">flashx.textLayout.formats.TextLayoutFormat.tabStops</a>
	 * </div><br><hr>
	 */
	public class TabStopFormat implements ITabStopFormat {
		private static var _defaultFormat:ITabStopFormat;

		private var _alignment:*;
		private var _decimalAlignmentToken:*;
		private var _position:*;

		/**
		 * <p> Creates a new TabStopFormat object. All settings are empty or, optionally, are initialized from the supplied <code>initialValues</code> object. </p>
		 * 
		 * @param initialValues  — optional instance from which to copy initial values. 
		 */
		public function TabStopFormat(initialValues:ITabStopFormat = null) {
			throw new Error("Not implemented");
		}

		/**
		 * <p> The tab alignment for this tab stop. </p>
		 * <p>Legal values are TabAlignment.START, TabAlignment.CENTER, TabAlignment.END, TabAlignment.DECIMAL, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will have a value of TabAlignment.START.</p>
		 * 
		 * @return 
		 */
		public function get alignment():* {
			return _alignment;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set alignment(value:*):void {
			_alignment = value;
		}

		/**
		 * <p> The alignment token to be used if the alignment is DECIMAL. </p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will have a value of null.</p>
		 * 
		 * @return 
		 */
		public function get decimalAlignmentToken():* {
			return _decimalAlignmentToken;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set decimalAlignmentToken(value:*):void {
			_decimalAlignmentToken = value;
		}

		/**
		 * <p> The position of the tab stop, in pixels, relative to the start edge of the column. </p>
		 * <p>Legal values are numbers from 0 to 10000 and FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will have a value of 0.</p>
		 * 
		 * @return 
		 */
		public function get position():* {
			return _position;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set position(value:*):void {
			_position = value;
		}

		/**
		 * <p> Replaces property values in this TabStopFormat object with the values of properties that are set in the <code>incoming</code> ITabStopFormat instance. Properties that are <code>undefined</code> in the <code>incoming</code> ITabStopFormat instance are not changed in this object. </p>
		 * 
		 * @param incoming  — instance whose property values are applied to this TabStopFormat object. 
		 */
		public function apply(incoming:ITabStopFormat):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Concatenates the values of properties in the <code>incoming</code> ITabStopFormat instance with the values of this TabStopFormat object. In this (the receiving) TabStopFormat object, properties whose values are <code>FormatValue.INHERIT</code>, and inheriting properties whose values are <code>undefined</code> will get new values from the <code>incoming</code> object. Non-inheriting properties whose values are <code>undefined</code> will get their default values. All other property values will remain unmodified. </p>
		 * 
		 * @param incoming  — instance from which values are concatenated. 
		 */
		public function concat(incoming:ITabStopFormat):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Concatenates the values of properties in the <code>incoming</code> ITabStopFormat instance with the values of this TabStopFormat object. In this (the receiving) TabStopFormat object, properties whose values are <code>FormatValue.INHERIT</code>, and inheriting properties whose values are <code>undefined</code> will get new values from the <code>incoming</code> object. All other property values will remain unmodified. </p>
		 * 
		 * @param incoming  — instance from which values are concatenated. 
		 */
		public function concatInheritOnly(incoming:ITabStopFormat):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Copies TabStopFormat settings from the <code>values</code> ITabStopFormat instance into this TabStopFormat object. If <code>values</code> is <code>null</code>, this TabStopFormat object is initialized with undefined values for all properties. </p>
		 * 
		 * @param values  — optional instance from which to copy values. 
		 */
		public function copy(values:ITabStopFormat):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Return the value of the style specified by the <code>styleProp</code> parameter which specifies the style name. </p>
		 * 
		 * @param styleName  — The name of the style whose value is to be retrieved. 
		 * @return  — The value of the specified style. The type varies depending on the type of the style being accessed. Returns  if the style is not set. 
		 */
		public function getStyle(styleName:String):* {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets properties in this TabStopFormat object to <code>undefined</code> if they do not match those in the <code>incoming</code> ITabStopFormat instance. </p>
		 * 
		 * @param incoming  — instance against which to compare this TabStopFormat object's property values. 
		 */
		public function removeClashing(incoming:ITabStopFormat):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets properties in this TabStopFormat object to <code>undefined</code> if they match those in the <code>incoming</code> ITabStopFormat instance. </p>
		 * 
		 * @param incoming  — instance against which to compare this TabStopFormat object's property values. 
		 */
		public function removeMatching(incoming:ITabStopFormat):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Set the value of the style specified by the <code>styleProp</code> parameter which specifies the style name to <code>value</code>. </p>
		 * 
		 * @param styleName  — The name of the style whose value is to be set. 
		 * @param value  — The value to set. 
		 */
		public function setStyle(styleName:String, value:*):void {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Returns a TabStopFormat object with default settings. This function always returns the same object. </p>
		 * 
		 * @return 
		 */
		public static function get defaultFormat():ITabStopFormat {
			return _defaultFormat;
		}


		/**
		 * <p> Compares properties in ITabStopFormat instance <code>p1</code> with properties in ITabStopFormat instance <code>p2</code> and returns <code>true</code> if all properties match. </p>
		 * 
		 * @param p1  — instance to compare to <code>p2</code>. 
		 * @param p2  — instance to compare to <code>p1</code>. 
		 * @return  — true if all properties match, false otherwise. 
		 */
		public static function isEqual(p1:ITabStopFormat, p2:ITabStopFormat):Boolean {
			throw new Error("Not implemented");
		}
	}
}
