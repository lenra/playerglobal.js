package flashx.textLayout.formats {
	/**
	 *  Defines constants for specifying subscript or superscript in the <code>baselineShift</code> property of the <code>TextLayoutFormat</code> class. You can specify baseline shift as an absolute pixel offset, a percentage of the current point size, or the constants SUPERSCRIPT or SUBSCRIPT. Positive values shift the line up for horizontal text (right for vertical) and negative values shift it down for horizontal (left for vertical). <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="TextLayoutFormat.html#baselineShift" target="">TextLayoutFormat.baselineShift</a>
	 * </div><br><hr>
	 */
	public class BaselineShift {
		/**
		 * <p> Shifts baseline to the current subscript position. </p>
		 */
		public static const SUBSCRIPT:String = "subscript";
		/**
		 * <p> Shifts baseline to the current superscript position. </p>
		 */
		public static const SUPERSCRIPT:String = "superscript";
	}
}
