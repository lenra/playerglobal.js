package flashx.textLayout.formats {
	/**
	 *  Defines values for setting the <code>suffix</code> property of a ListMarkerFormat. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="ListMarkerFormat.html#suffix" target="">ListMarkerFormat.suffix</a>
	 * </div><br><hr>
	 */
	public class Suffix {
		/**
		 * <p> Specifies that the auto suffix is appended to the list marker after the content. See <a href="http://www.w3.org/TR/css3-lists/">http://www.w3.org/TR/css3-lists/</a> for the default suffixes. </p>
		 */
		public static const AUTO:String = "auto";
		/**
		 * <p> Specifies that no suffix is appended to a list marker after the content. </p>
		 */
		public static const NONE:String = "none";
	}
}
