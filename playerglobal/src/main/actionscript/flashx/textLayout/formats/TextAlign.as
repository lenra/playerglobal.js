package flashx.textLayout.formats {
	/**
	 *  Defines values for setting the <code>textAlign</code> and <code>textAlignLast</code> properties of the TextLayoutFormat class. The values describe the alignment of lines in the paragraph relative to the container. <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="TextLayoutFormat.html#textAlign" target="">TextLayoutFormat.textAlign</a>
	 *  <br>
	 *  <a href="TextLayoutFormat.html#textAlignLast" target="">TextLayoutFormat.textAlignLast</a>
	 * </div><br><hr>
	 */
	public class TextAlign {
		/**
		 * <p> Specifies center alignment within the container. </p>
		 */
		public static const CENTER:String = "center";
		/**
		 * <p> Specifies end edge alignment - text is aligned opposite from the writing order. Equivalent to specifying right in left-to-right text, or left in right-to-left text. </p>
		 */
		public static const END:String = "end";
		/**
		 * <p> Specifies that text is justified within the lines so they fill the container space. </p>
		 */
		public static const JUSTIFY:String = "justify";
		/**
		 * <p> Specifies left edge alignment. </p>
		 */
		public static const LEFT:String = "left";
		/**
		 * <p> Specifies right edge alignment. </p>
		 */
		public static const RIGHT:String = "right";
		/**
		 * <p> Specifies start edge alignment - text is aligned to match the writing order. Equivalent to setting left in left-to-right text, or right in right-to-left text. </p>
		 */
		public static const START:String = "start";
	}
}
