package flashx.textLayout.formats {
	/**
	 *  Defines values for the <code>float</code> property of the InlineGraphicElement class. <p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS19f279b149e7481c2028ff4112df207af8b-8000.html" target="_blank">Positioning images inline</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS19f279b149e7481c-36a59aa412decda6f20-8000.html" target="_blank">Positioning images within text</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  flashx.textLayout.elements.InlineGrapicElement
	 * </div><br><hr>
	 */
	public class Float {
		public static const END:String = "end";
		public static const LEFT:String = "left";
		public static const NONE:String = "none";
		public static const RIGHT:String = "right";
		public static const START:String = "start";
	}
}
