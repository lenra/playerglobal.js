package flashx.textLayout.formats {
	/**
	 *  Defines values for the <code>textDecoration</code> property of the TextLayoutFormat class. The values specify either normal text, with no decoration, or underline. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="TextLayoutFormat.html#textDecoration" target="">TextLayoutFormat.textDecoration</a>
	 * </div><br><hr>
	 */
	public class TextDecoration {
		/**
		 * <p> Specifies normal text - no decoration applied </p>
		 */
		public static const NONE:String = "none";
		/**
		 * <p> Specifies that text is underlined. </p>
		 */
		public static const UNDERLINE:String = "underline";
	}
}
