package flashx.textLayout.formats {
	/**
	 *  Defines values for setting the <code>whiteSpaceCollapse</code> property of the <code>TextLayoutFormat</code> class. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="TextLayoutFormat.html#whiteSpaceCollapse" target="">TextLayoutFormat.whiteSpaceCollapse</a>
	 * </div><br><hr>
	 */
	public class WhiteSpaceCollapse {
		/**
		 * <p> Collapse whitespace when importing text (default). Within a block of imported text, removes newlines, tabs, and leading and trailing spaces. Retains line break tags (br/) and Unicode line separator characters. </p>
		 */
		public static const COLLAPSE:String = "collapse";
		/**
		 * <p> Preserves whitespace when importing text. </p>
		 */
		public static const PRESERVE:String = "preserve";
	}
}
