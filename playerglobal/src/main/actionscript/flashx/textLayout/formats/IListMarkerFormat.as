package flashx.textLayout.formats {
	/**
	 *  This interface provides read access to ListMarkerFormat properties. <br><hr>
	 */
	public interface IListMarkerFormat {

		/**
		 * @return 
		 */
		function get afterContent():*;

		/**
		 * @return 
		 */
		function get beforeContent():*;

		/**
		 * @return 
		 */
		function get content():*;

		/**
		 * @return 
		 */
		function get counterIncrement():*;

		/**
		 * @return 
		 */
		function get counterReset():*;

		/**
		 * @return 
		 */
		function get suffix():*;
	}
}
