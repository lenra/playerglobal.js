package flashx.textLayout.formats {
	/**
	 *  Defines values for setting the <code>leadingModel</code> property of the <code>TextLayoutFormat</code> class, composed of valid combinations of leading basis and leading direction. Leading basis describes which baselines determine the leading (or <code>lineHeight</code>) of lines in a paragraph. Leading direction specifies whether the <code>lineHeight</code> property refers to the distance of a line's baseline from that of the line before it or the line after it. <p> <img src="../../../images/textLayout_baselines.jpg" alt="baselines"> <img src="../../../images/textLayout_LD1.jpg" alt="leadingDirection_1"> <img src="../../../images/textLayout_LD2.jpg" alt="leadingDirection_2"> <img src="../../../images/textLayout_LD3.jpg" alt="leadingDirection_3"> </p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="TextLayoutFormat.html#leadingModel" target="">TextLayoutFormat.leadingModel</a>
	 *  <br>
	 *  <a href="TextLayoutFormat.html#lineHeight" target="">TextLayoutFormat.lineHeight</a>
	 *  <br>
	 *  <a href="../../../flash/text/TextField.html" target="">flash.text.TextField</a>
	 *  <br>
	 *  <a href="../../../flash/text/engine/TextLine.html" target="">flash.text.engine.TextLine</a>
	 * </div><br><hr>
	 */
	public class LeadingModel {
		/**
		 * <p> Specifies a leading model that approximates the line spacing behavior of <code>TextField</code>. It is similar to <code>ASCENT_DESCENT_UP</code> in that <code>lineHeight</code> refers to the distance of a line's ascent baseline from the previous line's descent baseline. However, baseline positions approximate those determined by <code>TextField</code>, rather than using metrics offered by <code>TextLine</code>. </p>
		 */
		public static const APPROXIMATE_TEXT_FIELD:String = "approximateTextField";
		/**
		 * <p> Specifies that leading basis is ASCENT/DESCENT and leading direction is UP. In other words, <code>lineHeight</code> refers to the distance of a line's ascent baseline from the previous line's descent baseline. </p>
		 */
		public static const ASCENT_DESCENT_UP:String = "ascentDescentUp";
		/**
		 * <p> Specifies that leading model is chosen automatically based on the paragraph's <code>locale</code> property. For Japanese and Chinese, it is IDEOGRAPHIC_TOP_DOWN and for all others it is ROMAN_UP. </p>
		 */
		public static const AUTO:String = "auto";
		/**
		 * <p> Specifies a leading model based on the CSS inline formatting model, which involves stacking <i>line boxes</i> contiguously. A <i>line box</i> is defined as the bounding box around <i>inline boxes</i> for all leaf elements on the text line, after they have been aligned using <code>baselineShift</code>, <code>dominantBaseline</code>, <code>alignmentBaseline</code> etc. For a span, the <i>inline box</i> is obtained by applying leading equally above and below the text content such that its height equals <code>lineHeight</code>. For an inline graphic, <code>lineHeight</code> is ignored; the <i>inline box</i> is derived from its specified dimensions and padding values. Also, <code>firstBaselineOffset</code> is ignored with this leading model. </p>
		 * <p>For more information, see <a href="http://blogs.adobe.com/tlf/2010/11/box-leading-model.html">Box leading model</a>.</p>
		 */
		public static const BOX:String = "box";
		/**
		 * <p> Specifies that leading basis is IDEOGRAPHIC_CENTER and leading direction is down. In other words, <code>lineHeight</code> refers to the distance of a line's ideographic center baseline from the next line's ideographic center baseline. </p>
		 */
		public static const IDEOGRAPHIC_CENTER_DOWN:String = "ideographicCenterDown";
		/**
		 * <p> Specifies that leading basis is IDEOGRAPHIC_CENTER and leading direction is UP. In other words, <code>lineHeight</code> refers to the distance of a line's ideographic center baseline from the previous line's ideographic center baseline. </p>
		 */
		public static const IDEOGRAPHIC_CENTER_UP:String = "ideographicCenterUp";
		/**
		 * <p> Specifies that leading basis is IDEOGRAPHIC_TOP and leading direction is DOWN. In other words, <code>lineHeight</code> refers to the distance of a line's ideographic top baseline from the next line's ideographic top baseline. </p>
		 */
		public static const IDEOGRAPHIC_TOP_DOWN:String = "ideographicTopDown";
		/**
		 * <p> Specifies that leading basis is IDEOGRAPHIC_TOP and leading direction is UP. In other words, <code>lineHeight</code> refers to the distance of a line's ideographic top baseline from the previous line's ideographic top baseline. </p>
		 */
		public static const IDEOGRAPHIC_TOP_UP:String = "ideographicTopUp";
		/**
		 * <p> Specifies that leading basis is ROMAN and leading direction is UP. In other words, <code>lineHeight</code> refers to the distance of a line's Roman baseline from the previous line's Roman baseline. </p>
		 */
		public static const ROMAN_UP:String = "romanUp";
	}
}
