package flashx.textLayout.formats {
	/**
	 *  Defines values for setting the <code>lineBreak</code> property of <code>TextLayoutFormat</code> to specify how lines are broken within wrapping text. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="TextLayoutFormat.html#linebreak" target="">TextLayoutFormat.linebreak</a>
	 * </div><br><hr>
	 */
	public class LineBreak {
		/**
		 * <p> Specifies that lines break only at explicit return or line feed characters. </p>
		 */
		public static const EXPLICIT:String = "explicit";
		/**
		 * <p> Specifies that lines wrap to fit the container width. </p>
		 */
		public static const TO_FIT:String = "toFit";
	}
}
