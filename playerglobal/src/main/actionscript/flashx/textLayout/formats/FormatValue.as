package flashx.textLayout.formats {
	/**
	 *  Defines values for specifying that a formatting property is to inherit its parent's value or have it's value generated automatically. The <code>INHERIT</code> constant specifies that a property inherits its parent's value while the <code>AUTO</code> constant specifies that an internal algorithm automatically determine the property's value. As one example, you can set <code>TextLayoutFormat.columnWidth</code> using these values. Typically, a property's description indicates whether it accepts these constants. <p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS19f279b149e7481c-1f6e470812df1478929-8000.html" target="_blank">Using padding in TLF</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="TextLayoutFormat.html" target="">TextLayoutFormat</a>
	 * </div><br><hr>
	 */
	public class FormatValue {
		/**
		 * <p> Specifies that a property's value is automatically generated. </p>
		 */
		public static const AUTO:String = "auto";
		/**
		 * <p> Specifies that a property is to inherit its parent's value. </p>
		 */
		public static const INHERIT:String = "inherit";
		/**
		 * <p> Specifies that a property's value is none. </p>
		 */
		public static const NONE:String = "none";
	}
}
