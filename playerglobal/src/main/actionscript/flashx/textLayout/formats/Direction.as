package flashx.textLayout.formats {
	/**
	 *  Defines values for setting the <code>direction</code> property of the <code>TextLayoutFormat</code> class. Left-to-right reading order is used in Latin-style scripts. Right-to-left reading order is used with scripts such as Arabic or Hebrew. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="TextLayoutFormat.html#direction" target="">TextLayoutFormat.direction</a>
	 * </div><br><hr>
	 */
	public class Direction {
		/**
		 * <p> Specifies left-to-right direction for text. </p>
		 */
		public static const LTR:String = "ltr";
		/**
		 * <p> Specifies right-to-left direction for text. </p>
		 */
		public static const RTL:String = "rtl";
	}
}
