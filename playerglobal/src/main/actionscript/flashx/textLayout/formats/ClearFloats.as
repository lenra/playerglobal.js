package flashx.textLayout.formats {
	/**
	 *  Defines values for setting the <code>clear</code> property of the <code>TextLayoutFormat</code> class. This property controls how text wraps around floats. <p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS19f279b149e7481c2028ff4112df207af8b-8000.html" target="_blank">Positioning images inline</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS19f279b149e7481c-36a59aa412decda6f20-8000.html" target="_blank">Positioning images within text</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flashx/textLayout/elements/InlineGraphicElement.html#float" target="">flashx.textLayout.elements.InlineGraphicElement.float</a>
	 *  <br>
	 *  <a href="TextLayoutFormat.html#clear" target="">TextLayoutFormat.clear</a>
	 * </div><br><hr>
	 */
	public class ClearFloats {
		/**
		 * <p> Specifies that text skips over any float. </p>
		 */
		public static const BOTH:String = "both";
		/**
		 * <p> Specifies that text skips over floats on the start side in reading order (left if direction is "ltr", right if direction is "rtl"). </p>
		 */
		public static const END:String = "end";
		/**
		 * <p> Specifies that text skips over left floats. </p>
		 */
		public static const LEFT:String = "left";
		/**
		 * <p> Specifies that text wraps closely around floats. </p>
		 */
		public static const NONE:String = "none";
		/**
		 * <p> Specifies that text skips over right floats. </p>
		 */
		public static const RIGHT:String = "right";
		/**
		 * <p> Specifies that text skips over floats on the start side in reading order (left if direction is "ltr", right if direction is "rtl"). </p>
		 */
		public static const START:String = "start";
	}
}
