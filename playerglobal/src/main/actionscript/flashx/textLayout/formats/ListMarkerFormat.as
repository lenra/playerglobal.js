package flashx.textLayout.formats {
	/**
	 *  Defines the marker format in a ListItemElement. <p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS19f279b149e7481c7fdc883d12de39353f1-8000.html" target="_blank">Using numbered and bulleted lists with TLF-based text controls</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS19f279b149e7481c6944fe6612e01ed0f26-8000.html" target="_blank">Using numbered and bulleted lists</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flashx/textLayout/elements/ListItemElement.html" target="">flashx.textLayout.elements.ListItemElement</a>
	 * </div><br><hr>
	 */
	public class ListMarkerFormat extends TextLayoutFormat implements IListMarkerFormat {
		private var _afterContent:*;
		private var _beforeContent:*;
		private var _content:*;
		private var _counterIncrement:*;
		private var _counterReset:*;
		private var _suffix:*;

		/**
		 * <p> Create a ListMarkerFormat that holds all the properties possible for a list marker. </p>
		 * 
		 * @param initialValues  — An optional instance from which to copy initial values. 
		 */
		public function ListMarkerFormat(initialValues:IListMarkerFormat = null) {
			super(initialValues);
			throw new Error("Not implemented");
		}

		/**
		 * <p> Specifies a string that goes after the marker. Default is the empty string. </p>
		 * 
		 * @return 
		 */
		public function get afterContent():* {
			return _afterContent;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set afterContent(value:*):void {
			_afterContent = value;
		}

		/**
		 * <p> Specifies a string that goes before the marker. Default is the empty string. </p>
		 * 
		 * @return 
		 */
		public function get beforeContent():* {
			return _beforeContent;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set beforeContent(value:*):void {
			_beforeContent = value;
		}

		/**
		 * <p> Controls the content of the marker. </p>
		 * <p>Legal values for this string are: </p>
		 * <ul>
		 *  <li><code>none</code> - No marker.</li>
		 *  <li><code>counter(ordered)</code> - Display the marker.</li>
		 *  <li><code>counter(ordered,ListStyleType)</code> - Display the marker but change the listStyleType to the specified value.</li>
		 *  <li><code>counters(ordered)</code> - Starting from the top-most parent ListElement creating a string of values of the ordered counter in each counters specified listStyleType separated by the suffix for each. This is used for outline number - for example I.1., I.2. etc.</li>
		 *  <li><code>counters(ordered,"&lt;string&gt;")</code> - Similar to the previous value, except the suffix for each ordered counter is replaced by &lt;string&gt;.</li>
		 *  <li><code>counters(ordered,"&lt;string&gt;",ListStyleType)</code> - Similar to the previous value, except each counter's <code>listStyleType</code> is replaced with the specified value.</li>
		 * </ul>
		 * <p>If undefined, the default vaule of this property is <code>"counter(ordered)"</code>.</p>
		 * 
		 * @return 
		 */
		public function get content():* {
			return _content;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set content(value:*):void {
			_content = value;
		}

		/**
		 * <p> Controls incrementing the value of the counter. </p>
		 * <p>Legal values for this string are: </p>
		 * <ul>
		 *  <li><code>none</code> - No increment.</li>
		 *  <li><code>ordered</code> - Increment the counter by one.</li>
		 *  <li><code>ordered <i>integer</i></code> - Increment the counter by <code><i>integer</i></code>.</li>
		 * </ul>
		 * <p>If undefined, the default vaule of this property is <code>"ordered 1"</code>.</p>
		 * <p>Note: The <code>counterIncrement</code> property is applied before the <code>counterReset</code> property.</p>
		 * 
		 * @return 
		 */
		public function get counterIncrement():* {
			return _counterIncrement;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set counterIncrement(value:*):void {
			_counterIncrement = value;
		}

		/**
		 * <p> Controls resetting the value of the counter. </p>
		 * <p>Legal values for this property are: </p>
		 * <ul>
		 *  <li><code>none</code> - No reset.</li>
		 *  <li><code>ordered</code> - Reset the counter to zero.</li>
		 *  <li><code>ordered <i>integer</i></code> - Reset the counter to <code><i>integer</i></code>.</li>
		 * </ul>
		 * <p>If <code>undefined</code>, the default value of this property is "none".</p>
		 * <p>Note: The <code>counterReset</code> property is applied before the <code>counterIncrement</code> property.</p>
		 * 
		 * @return 
		 */
		public function get counterReset():* {
			return _counterReset;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set counterReset(value:*):void {
			_counterReset = value;
		}

		/**
		 * <p> Controls the application of the suffix in the generated text in the ListItemElement. </p>
		 * <p>Legal values are: </p>
		 * <ul>
		 *  <li><code>flashx.textLayout.formats.Suffix.NONE</code> - No suffix.</li>
		 *  <li><code>flashx.textLayout.formats.Suffix.AUTO</code> - Follow CSS rules for adding a suffix.</li>
		 * </ul>
		 * <p>Default value is <code>Suffix.AUTO</code>.</p>
		 * 
		 * @return 
		 */
		public function get suffix():* {
			return _suffix;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set suffix(value:*):void {
			_suffix = value;
		}


		/**
		 * <p> Creates a new ListMarkerFormat object. All settings are empty or, optionally, are initialized from the supplied <code>initialValues</code> object. </p>
		 * 
		 * @param initialValues  — Optional instance from which to copy initial values. If the object is of type IListMarkerFormat or ITextLayoutFormat, the values are copied. Otherwise the <code>initialValues</code> parameter is treated like a Dictionary or Object and iterated over. 
		 * @return  — The new ListMarkerFormat object. 
		 */
		public static function createListMarkerFormat(initialValues:Object):ListMarkerFormat {
			throw new Error("Not implemented");
		}
	}
}
