package flashx.textLayout.formats {
	/**
	 *  Defines values for setting the <code>listStylePosition</code> property. These values control the placement of a list item marker relative to the list item. <p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS19f279b149e7481c7fdc883d12de39353f1-8000.html" target="_blank">Using numbered and bulleted lists with TLF-based text controls</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS19f279b149e7481c6944fe6612e01ed0f26-8000.html" target="_blank">Using numbered and bulleted lists</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="TextLayoutFormat.html#listStylePosition" target="">TextLayoutFormat.listStylePosition</a>
	 * </div><br><hr>
	 */
	public class ListStylePosition {
		/**
		 * <p> Marker will appear inline with the list item. This style position lets you include trailing spaces. </p>
		 */
		public static const INSIDE:String = "inside";
		/**
		 * <p> Marker will appear in the margin of the list. This style position does not recognize trailing spaces. </p>
		 */
		public static const OUTSIDE:String = "outside";
	}
}
