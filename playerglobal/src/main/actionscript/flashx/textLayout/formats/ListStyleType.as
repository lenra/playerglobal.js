package flashx.textLayout.formats {
	/**
	 *  Defines values for setting the <code>listStyleType</code> property of a TextLayoutFormat object. These values are used for controlling the appearance of items in a list. <p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS19f279b149e7481c7fdc883d12de39353f1-8000.html" target="_blank">Using numbered and bulleted lists with TLF-based text controls</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS19f279b149e7481c6944fe6612e01ed0f26-8000.html" target="_blank">Using numbered and bulleted lists</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="TextLayoutFormat.html#listStyleType" target="">TextLayoutFormat.listStyleType</a>
	 * </div><br><hr>
	 */
	public class ListStyleType {
		/**
		 * <p> Numbering using Arabic script. </p>
		 */
		public static const ARABIC_INDIC:String = "arabicIndic";
		/**
		 * <p> Numbering using Bengali script. </p>
		 */
		public static const BENGALI:String = "bengali";
		/**
		 * <p> A square marker that is not filled. </p>
		 */
		public static const BOX:String = "box";
		/**
		 * <p> A check mark. </p>
		 */
		public static const CHECK:String = "check";
		/**
		 * <p> A circle character marker that is not filled. </p>
		 */
		public static const CIRCLE:String = "circle";
		/**
		 * <p> Numbering for CJK. </p>
		 */
		public static const CJK_EARTHLY_BRANCH:String = "cjkEarthlyBranch";
		/**
		 * <p> Numbering for CJK. </p>
		 */
		public static const CJK_HEAVENLY_STEM:String = "cjkHeavenlyStem";
		/**
		 * <p> Numbering using decimals: 1, 2, 3, and so on. </p>
		 */
		public static const DECIMAL:String = "decimal";
		/**
		 * <p> Numbering using decimal with a leading zero: 01, 02, 03, and so on. </p>
		 */
		public static const DECIMAL_LEADING_ZERO:String = "decimalLeadingZero";
		/**
		 * <p> Numbering using Devangari. </p>
		 */
		public static const DEVANAGARI:String = "devanagari";
		/**
		 * <p> A filled diamond marker. </p>
		 */
		public static const DIAMOND:String = "diamond";
		/**
		 * <p> A bullet character marker (filled circle). </p>
		 */
		public static const DISC:String = "disc";
		/**
		 * <p> Numbering using Gujarati. </p>
		 */
		public static const GUJARATI:String = "gujarati";
		/**
		 * <p> Numbering using Gurmukhi. </p>
		 */
		public static const GURMUKHI:String = "gurmukhi";
		/**
		 * <p> Numbering for Hangul. </p>
		 */
		public static const HANGUL:String = "hangul";
		/**
		 * <p> Numbering for Hangul. </p>
		 */
		public static const HANGUL_CONSTANT:String = "hangulConstant";
		/**
		 * <p> Numbering for Hiragana. </p>
		 */
		public static const HIRAGANA:String = "hiragana";
		/**
		 * <p> Numbering for Hiragana. </p>
		 */
		public static const HIRAGANA_IROHA:String = "hiraganaIroha";
		/**
		 * <p> A dash mark. </p>
		 */
		public static const HYPHEN:String = "hyphen";
		/**
		 * <p> Numbering using Kannada. </p>
		 */
		public static const KANNADA:String = "kannada";
		/**
		 * <p> Numbering for Katagana. </p>
		 */
		public static const KATAKANA:String = "katakana";
		/**
		 * <p> Numbering for Katagana. </p>
		 */
		public static const KATAKANA_IROHA:String = "katakanaIroha";
		/**
		 * <p> Lower-case alphabetic "numbering": a-z, aa-zz, and so on. </p>
		 */
		public static const LOWER_ALPHA:String = "lowerAlpha";
		/**
		 * <p> Lower-case Greek alphabetic "numering". </p>
		 */
		public static const LOWER_GREEK:String = "lowerGreek";
		/**
		 * <p> Lower-case alphabetic "numbering": a-z, aa-zz, and so on. </p>
		 */
		public static const LOWER_LATIN:String = "lowerLatin";
		/**
		 * <p> Lower-case Roman numbering: i, ii, iii, iv, and so on. </p>
		 */
		public static const LOWER_ROMAN:String = "lowerRoman";
		/**
		 * <p> No content is generated for the marker. </p>
		 */
		public static const NONE:String = "none";
		/**
		 * <p> Numbering using Persian. </p>
		 */
		public static const PERSIAN:String = "persian";
		/**
		 * <p> A filled square marker. </p>
		 */
		public static const SQUARE:String = "square";
		/**
		 * <p> Numbering using Thai. </p>
		 */
		public static const THAI:String = "thai";
		/**
		 * <p> Upper-case alphabetic "numbering": A-Z, AA-ZZ, and so on. </p>
		 */
		public static const UPPER_ALPHA:String = "upperAlpha";
		/**
		 * <p> Upper-case Greek alphabetic "numering". </p>
		 */
		public static const UPPER_GREEK:String = "upperGreek";
		/**
		 * <p> Upper-case alphabetic "numbering": A-Z, AA-ZZ, and so on. </p>
		 */
		public static const UPPER_LATIN:String = "upperLatin";
		/**
		 * <p> Upper-case Roman numbering: I, II, III, IV, and so on. </p>
		 */
		public static const UPPER_ROMAN:String = "upperRoman";
		/**
		 * <p> Numbering using Urdu. </p>
		 */
		public static const URDU:String = "urdu";
	}
}
