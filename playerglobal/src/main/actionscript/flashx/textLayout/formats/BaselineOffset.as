package flashx.textLayout.formats {
	/**
	 *  Defines values for the <code>firstBaselineOffset</code> property of the <code>TextLayoutFormat</code> and <code>ContainerFormattedElement</code> classes. Determines the offset from the top inset of the container to the baseline of the first line. Baseline offset may be specified as the ascent of the line, the height of the line, or an auto generated amount. <p> <img src="../../../images/textLayout_FBO1.jpg" alt="firstBaselineOffset_1"> <img src="../../../images/textLayout_FBO2.jpg" alt="firstBaselineOffset_2"> <img src="../../../images/textLayout_FBO3.jpg" alt="firstBaselineOffset_3"> <img src="../../../images/textLayout_FBO4.jpg" alt="firstBaselineOffset_4"> </p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="TextLayoutFormat.html#firstBaselineOffset" target="">TextLayoutFormat.firstBaselineOffset</a>
	 * </div><br><hr>
	 */
	public class BaselineOffset {
		/**
		 * <p> Specifies an offset equal to the ascent of the line, that is, the ascent of the tallest font in the line, accounting for inline graphics as having the bottom of the graphic on the baseline. </p>
		 */
		public static const ASCENT:String = "ascent";
		/**
		 * <p> Aligns the ascent of the line with the container top inset. </p>
		 */
		public static const AUTO:String = "auto";
		/**
		 * <p> Specifies an offset equal to the height of the line. </p>
		 */
		public static const LINE_HEIGHT:String = "lineHeight";
	}
}
