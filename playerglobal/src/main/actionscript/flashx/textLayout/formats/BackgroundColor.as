package flashx.textLayout.formats {
	/**
	 *  Defines a constant for specifying that the value of the <code>backgroundColor</code> property of the <code>TextLayoutFormat</code> class is "transparent". <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="TextLayoutFormat.html#backgroundColor" target="">TextLayoutFormat.backgroundColor</a>
	 * </div><br><hr>
	 */
	public class BackgroundColor {
		/**
		 * <p> Transparent - no background color is applied. </p>
		 */
		public static const TRANSPARENT:String = "transparent";
	}
}
