package flashx.textLayout.formats {
	/**
	 *  Defines values for the <code>verticalAlign</code> property of the TextLayoutFormat class. Specifies how TextFlow elements align with their containers. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="TextLayoutFormat.html#verticalAlign" target="">TextLayoutFormat.verticalAlign</a>
	 * </div><br><hr>
	 */
	public class VerticalAlign {
		/**
		 * <p> Specifies alignment with the bottom edge of the frame. </p>
		 */
		public static const BOTTOM:String = "bottom";
		/**
		 * <p> Specifies vertical line justification within the frame </p>
		 */
		public static const JUSTIFY:String = "justify";
		/**
		 * <p> Specifies alignment with the middle of the frame. </p>
		 */
		public static const MIDDLE:String = "middle";
		/**
		 * <p> Specifies alignment with the top edge of the frame. </p>
		 */
		public static const TOP:String = "top";
	}
}
