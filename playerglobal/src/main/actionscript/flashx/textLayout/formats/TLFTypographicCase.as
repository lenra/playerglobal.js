package flashx.textLayout.formats {
	/**
	 *  Defines values for the <code>typographicCase</code> property of the TextLayoutFormat class. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="TextLayoutFormat.html#typographicCase" target="">TextLayoutFormat.typographicCase</a>
	 * </div><br><hr>
	 */
	public class TLFTypographicCase {
		/**
		 * <p> Specifies that uppercase characters use small-caps glyphs on output. </p>
		 */
		public static const CAPS_TO_SMALL_CAPS:String = "capsToSmallCaps";
		/**
		 * <p> Specifies default typographic case -- no special features applied. </p>
		 */
		public static const DEFAULT:String = "default";
		/**
		 * <p> Specifies that all characters use lowercase glyphs on output. </p>
		 */
		public static const LOWERCASE:String = "lowercase";
		/**
		 * <p> Converts all lowercase characters to uppercase, then applies small caps to only the characters that the conversion changed. </p>
		 */
		public static const LOWERCASE_TO_SMALL_CAPS:String = "lowercaseToSmallCaps";
		/**
		 * <p> Specifies that all characters use uppercase glyphs on output. </p>
		 */
		public static const UPPERCASE:String = "uppercase";
	}
}
