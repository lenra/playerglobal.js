package flashx.textLayout.elements {
	/**
	 *  The FlowGroupElement class is the base class for FlowElement objects that can have an array of children. These classes include TextFlow, ParagraphElement, DivElement, and LinkElement. <p>You cannot create a FlowGroupElement object directly. Invoking <code>new FlowGroupElement()</code> throws an error exception.</p> <p><span class="classHeaderTableLabel">Default MXML Property</span><code>mxmlChildren</code></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="DivElement.html" target="">DivElement</a>
	 *  <br>
	 *  <a href="LinkElement.html" target="">LinkElement</a>
	 *  <br>
	 *  <a href="ParagraphElement.html" target="">ParagraphElement</a>
	 *  <br>
	 *  <a href="TextFlow.html" target="">TextFlow</a>
	 * </div><br><hr>
	 */
	public class FlowGroupElement extends FlowElement {
		private var _mxmlChildren:Array;

		private var _numChildren:int;

		/**
		 * <p> Base class - invoking <code>new FlowGroupElement()</code> throws an error exception. </p>
		 */
		public function FlowGroupElement() {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Appends an array of children to this object. Uses the <code>replaceChildren()</code> method to append each element in the array. Intended for use during an mxml compiled import. </p>
		 * 
		 * @return 
		 */
		public function get mxmlChildren():Array {
			return _mxmlChildren;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set mxmlChildren(value:Array):void {
			_mxmlChildren = value;
		}

		/**
		 * <p> Returns the number of FlowElement children that this FlowGroupElement object has. </p>
		 * 
		 * @return 
		 */
		public function get numChildren():int {
			return _numChildren;
		}

		/**
		 * <p> Appends a child FlowElement object. The new child is added to the end of the children list. </p>
		 * 
		 * @param child  — The child element to append. 
		 * @return  — the added child FlowElement 
		 */
		public function addChild(child:FlowElement):FlowElement {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Adds a child FlowElement object at the specified index position. </p>
		 * 
		 * @param index  — index of the position at which to add the child element, with the first position being 0. 
		 * @param child  — The child element to add. 
		 * @return  — the added child FlowElement 
		 */
		public function addChildAt(index:uint, child:FlowElement):FlowElement {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Given a relative text position, find the index of the first child FlowElement that contains the relative position. More than one child can contain relative position because of zero length FlowElements. </p>
		 * <p>Examine the children to find the FlowElement that contains the relative position. The supplied relative position is relative to this FlowElement.</p>
		 * 
		 * @param relativePosition  — the position relative to this element 
		 * @return  — index of first child element containing  
		 */
		public function findChildIndexAtPosition(relativePosition:int):int {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Given a relative text position, find the leaf element that contains the position. </p>
		 * <p>Looks down the flow element hierarchy to find the FlowLeafElement that contains the specified position. The specified position is relative to this FlowElement object.</p>
		 * 
		 * @param relativePosition  — relative text index to look up. 
		 * @return  — the leaf element containing the relative position. 
		 */
		public function findLeaf(relativePosition:int):FlowLeafElement {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the FlowElement child at the specified index. </p>
		 * 
		 * @param index  — the position at which to find the FlowElement object. 
		 * @return  — the child FlowElement object at the specified position. 
		 */
		public function getChildAt(index:int):FlowElement {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Searches in children for the specified FlowElement object and returns its index position. </p>
		 * 
		 * @param child  — The FlowElement object item to locate among the children. 
		 * @return  — The index position of the specified chilc. If  is not found, returns -1. 
		 */
		public function getChildIndex(child:FlowElement):int {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the first FlowLeafElement descendant of this group. </p>
		 * 
		 * @return  — the first FlowLeafElement object. 
		 */
		public function getFirstLeaf():FlowLeafElement {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the last FlowLeafElement descendent of this group. </p>
		 * 
		 * @return  — the last FlowLeafElement object. 
		 */
		public function getLastLeaf():FlowLeafElement {
			throw new Error("Not implemented");
		}

		/**
		 * @param relativeStart
		 * @param relativeEnd
		 * @param paragraphSeparator
		 * @return 
		 */
		override public function getText(relativeStart:int = 0, relativeEnd:int = -1, paragraphSeparator:String = ""):String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Removes the specified child FlowElement object from the group. </p>
		 * 
		 * @param child  — The child element to remove. 
		 * @return  — the removed child FlowElement object 
		 */
		public function removeChild(child:FlowElement):FlowElement {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Removes the child FlowElement object at the specified index position. </p>
		 * 
		 * @param index  — position at which to remove the child element. 
		 * @return  — the child FlowElement object removed from the specified position. 
		 */
		public function removeChildAt(index:uint):FlowElement {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Replaces child elements in the group with the specified new elements. Use the <code>beginChildIndex</code> and <code>endChildIndex</code> parameters to govern the operation as follows: </p>
		 * <ul>
		 *  <li>To delete elements, do not pass any replacement elements.</li>
		 *  <li>To insert elements, pass the same value for <code>beginChildIndex</code> and <code>endChildIndex</code>. The new elements is inserted before the specified index.</li>
		 *  <li>To append elements, pass <code>numChildren</code> for <code>beginChildIndex</code> and <code>endChildIndex</code>.</li>
		 * </ul>
		 * <p>Otherwise, this method replaces the specified elements, starting with the element at <code>beginChildIndex</code> and up to but not including <code>endChildIndex</code>.</p>
		 * 
		 * @param beginChildIndex  — The index value for the start position of the replacement range in the children array. 
		 * @param endChildIndex  — The index value following the end position of the replacement range in the children array. 
		 * @param rest  — The elements to replace the specified range of elements. Can be a sequence containing flow elements or arrays or vectors thereof. 
		 */
		public function replaceChildren(beginChildIndex:int, endChildIndex:int, ...rest):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Splits this object at the position specified by the <code>childIndex</code> parameter. If this group element has a parent, creates a shallow copy of this object and replaces its children with the elements up to the index. Moves elements following <code>childIndex</code> into the copy. </p>
		 * 
		 * @param childIndex
		 * @return  — the new FlowGroupElement object. 
		 */
		public function splitAtIndex(childIndex:int):FlowGroupElement {
			throw new Error("Not implemented");
		}
	}
}
