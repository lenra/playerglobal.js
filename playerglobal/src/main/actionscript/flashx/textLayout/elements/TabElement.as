package flashx.textLayout.elements {
	/**
	 *  The TabElement class represents a &lt;tab/&gt; in the text flow. You assign tab stops as an array of TabStopFormat objects to the <code>ParagraphElement.tabStops</code> property. <p> <b>Note</b>:This class exists primarily to support &lt;tab/&gt; in MXML markup. You can add tab characters (\t) directly into the text like this:</p> <div class="listing" version="3.0">
	 *  <div class="clipcopy">
	 *   <a href="#" class="copyText">Copy</a>
	 *  </div>
	 *  <pre>
	 *      spanElement1.text += '\t';
	 *      </pre>
	 * </div> <p><span class="classHeaderTableLabel">Default MXML Property</span><code>mxmlChildren</code></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS02f7d8d4857b1677-165a04e1126951a2d98-7ffc.html" target="_blank">Creating TextFlow objects</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flashx/textLayout/formats/TabStopFormat.html" target="">flashx.textLayout.formats.TabStopFormat</a>
	 *  <br>
	 *  <a href="FlowElement.html#tabStops" target="">FlowElement.tabStops</a>
	 *  <br>
	 *  <a href="SpanElement.html" target="">SpanElement</a>
	 * </div><br><hr>
	 */
	public class TabElement extends SpecialCharacterElement {

		/**
		 * <p> Constructor - creates a new TabElement instance. </p>
		 */
		public function TabElement() {
			throw new Error("Not implemented");
		}
	}
}
