package flashx.textLayout.elements {
	/**
	 *  A read only class that describes a range of contiguous text. Such a range occurs when you select a section of text. The range consists of the anchor point of the selection, <code>anchorPosition</code>, and the point that is to be modified by actions, <code>activePosition</code>. As block selections are modified and extended <code>anchorPosition</code> remains fixed and <code>activePosition</code> is modified. The anchor position may be placed in the text before or after the active position. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="TextFlow.html" target="">TextFlow</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/edit/SelectionState.html" target="">SelectionState</a>
	 * </div><br><hr>
	 */
	public class TextRange {
		private var _absoluteEnd:int;
		private var _absoluteStart:int;
		private var _activePosition:int;
		private var _anchorPosition:int;
		private var _textFlow:TextFlow;

		/**
		 * <p> Constructor - creates a new TextRange instance. A TextRange can be (-1,-1), indicating no range, or a pair of values from 0 to <code>TextFlow.textLength</code>. </p>
		 * 
		 * @param root  — the TextFlow associated with the selection. 
		 * @param anchorIndex  — the index position of the anchor in the selection. The first position in the text is position 0. 
		 * @param activeIndex  — the index position of the active location in the selection. The first position in the text is position 0. 
		 */
		public function TextRange(root:TextFlow, anchorIndex:int, activeIndex:int) {
			throw new Error("Not implemented");
		}

		/**
		 * <p> End of the selection, as an absolute position in the TextFlow. </p>
		 * 
		 * @return 
		 */
		public function get absoluteEnd():int {
			return _absoluteEnd;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set absoluteEnd(value:int):void {
			_absoluteEnd = value;
		}

		/**
		 * <p> Start of the selection, as an absolute position in the TextFlow. </p>
		 * 
		 * @return 
		 */
		public function get absoluteStart():int {
			return _absoluteStart;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set absoluteStart(value:int):void {
			_absoluteStart = value;
		}

		/**
		 * <p> Active position of the selection, as an absolute position in the TextFlow. </p>
		 * 
		 * @return 
		 */
		public function get activePosition():int {
			return _activePosition;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set activePosition(value:int):void {
			_activePosition = value;
		}

		/**
		 * <p> Anchor position of the selection, as an absolute position in the TextFlow. </p>
		 * 
		 * @return 
		 */
		public function get anchorPosition():int {
			return _anchorPosition;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set anchorPosition(value:int):void {
			_anchorPosition = value;
		}

		/**
		 * <p> Returns the TextFlow associated with the selection. </p>
		 * 
		 * @return 
		 */
		public function get textFlow():TextFlow {
			return _textFlow;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set textFlow(value:TextFlow):void {
			_textFlow = value;
		}

		/**
		 * <p> Update the range with new anchor or active position values. </p>
		 * 
		 * @param newAnchorPosition  — the anchor index of the selection. 
		 * @param newActivePosition  — the active index of the selection. 
		 * @return  — true if selection is changed. 
		 */
		public function updateRange(newAnchorPosition:int, newActivePosition:int):Boolean {
			throw new Error("Not implemented");
		}
	}
}
