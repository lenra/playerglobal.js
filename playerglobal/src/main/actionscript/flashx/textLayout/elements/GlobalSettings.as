package flashx.textLayout.elements {
	/**
	 *  Configuration that applies to all TextFlow objects. <br><hr>
	 */
	public class GlobalSettings {
		private static var _enableSearch:Boolean;
		private static var _fontMapperFunction:Function;
		private static var _resolveFontLookupFunction:Function;
		private static var _resourceStringFunction:Function;


		/**
		 * <p> Controls whether the text will be visible to a search engine indexer. Defaults to <code>true</code>. </p>
		 * 
		 * @return 
		 */
		public static function get enableSearch():Boolean {
			return _enableSearch;
		}


		/**
		 * @param value
		 * @return 
		 */
		public static function set enableSearch(value:Boolean):void {
			_enableSearch = value;
		}


		/**
		 * <p> Specifies the callback used for font mapping. The callback takes a <code>flash.text.engine.FontDescription</code> object and updates it as needed. After setting a new font mapping callback, or changing the behavior of the exisiting font mapping callback, the client must explicitly call <code>flashx.textLayout.elements.TextFlow.invalidateAllFormats</code> for each impacted text flow. This ensures that whenever a leaf element in the text flow is next recomposed, the FontDescription applied to it is recalculated, and the the callback is invoked. </p>
		 * 
		 * @return 
		 */
		public static function get fontMapperFunction():Function {
			return _fontMapperFunction;
		}


		/**
		 * @param value
		 * @return 
		 */
		public static function set fontMapperFunction(value:Function):void {
			_fontMapperFunction = value;
		}


		/**
		 * <p> Specifies the callback used for changing the FontLookup based on swfcontext. The function will be called each time an ElementFormat is computed. It gives the client the opportunity to modify the FontLookup setting. The function is called with two parameters an ISWFContext and an ITextLayoutFormat. It must return a valid FontLookup. </p>
		 * 
		 * @return 
		 */
		public static function get resolveFontLookupFunction():Function {
			return _resolveFontLookupFunction;
		}


		/**
		 * @param value
		 * @return 
		 */
		public static function set resolveFontLookupFunction(value:Function):void {
			_resolveFontLookupFunction = value;
		}


		/**
		 * <p> Function that takes two parameters, a resource id and an optional array of parameters to substitute into the string. The string is of form "Content {0} more content {1}". The parameters are read from the optional array and substituted for the bracketed substrings. TLF provides a default implementation with default strings. Clients may replace this function with their own implementation for localization. </p>
		 * 
		 * @return 
		 */
		public static function get resourceStringFunction():Function {
			return _resourceStringFunction;
		}


		/**
		 * @param value
		 * @return 
		 */
		public static function set resourceStringFunction(value:Function):void {
			_resourceStringFunction = value;
		}
	}
}
