package flashx.textLayout.elements {
	/**
	 *  The SpanElement class represents a run of text that has a single set of formatting attributes applied. SpanElement objects contain the text in a paragraph. A simple paragraph (ParagraphElement) includes one or more SpanElement objects. <p>A ParagraphElement will have a single SpanElement object if all the text in the paragraph shares the same set of attributes. It has multiple SpanElement objects if the text in the paragraph has multiple formats.</p> <p><span class="classHeaderTableLabel">Default MXML Property</span><code>mxmlChildren</code></p><p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS02f7d8d4857b1677-165a04e1126951a2d98-7ffc.html" target="_blank">Creating TextFlow objects</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="FlowElement.html" target="">FlowElement</a>
	 *  <br>
	 *  <a href="ParagraphElement.html" target="">ParagraphElement</a>
	 *  <br>
	 *  <a href="TextFlow.html" target="">TextFlow</a>
	 * </div><br><hr>
	 */
	public class SpanElement extends FlowLeafElement {
		private var _mxmlChildren:Array;
		private var _text:String;

		/**
		 * <p> Constructor - creates a SpanElement object to hold a run of text in a paragraph. </p>
		 */
		public function SpanElement() {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets text based on content within span tags; always deletes existing children. This property is intended for use during MXML compiled import in Flex. Flash Professional ignores this property. When TLF markup elements have other TLF markup elements as children, the children are assigned to this property. </p>
		 * 
		 * @return 
		 */
		public function get mxmlChildren():Array {
			return _mxmlChildren;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set mxmlChildren(value:Array):void {
			_mxmlChildren = value;
		}

		/**
		 * <p> Receives the String of text that this SpanElement object holds. </p>
		 * <p>The text of a span does not include the carriage return (CR) at the end of the paragraph but it is included in the value of <code>textLength</code>.</p>
		 * 
		 * @return 
		 */
		override public function get text():String {
			return _text;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set text(value:String):void {
			_text = value;
		}

		/**
		 * <p> Updates the text in text span based on the specified start and end positions. To insert text, set the end position equal to the start position. To append text to the existing text in the span, set the start position and the end position equal to the length of the existing text. </p>
		 * <p>The replaced text includes the start character and up to but not including the end character.</p>
		 * 
		 * @param relativeStartPosition  — The index position of the beginning of the text to be replaced, relative to the start of the span. The first character in the span is at position 0. 
		 * @param relativeEndPosition  — The index one position after the last character of the text to be replaced, relative to the start of the span. Set this value equal to <code>relativeStartPos</code> for an insert. 
		 * @param textValue  — The replacement text or the text to add, as the case may be. 
		 */
		public function replaceText(relativeStartPosition:int, relativeEndPosition:int, textValue:String):void {
			throw new Error("Not implemented");
		}
	}
}
