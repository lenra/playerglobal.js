package flashx.textLayout.elements {
	/**
	 *  The SubParagraphGroupElementBase class groups FlowLeafElements together. A SubParagraphGroupElementBase is a child of a ParagraphElement object and it can contain one or more FlowLeafElement objects as children. <p><span class="classHeaderTableLabel">Default MXML Property</span><code>mxmlChildren</code></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="FlowLeafElement.html" target="">FlowLeafElement</a>
	 *  <br>
	 *  <a href="LinkElement.html" target="">LinkElement</a>
	 *  <br>
	 *  <a href="ParagraphElement.html" target="">ParagraphElement</a>
	 *  <br>
	 *  <a href="TCYElement.html" target="">TCYElement</a>
	 * </div><br><hr>
	 */
	public class SubParagraphGroupElementBase extends FlowGroupElement {

		/**
		 * <p> Constructor - creates a new SubParagraphGroupElementBase instance. </p>
		 */
		public function SubParagraphGroupElementBase() {
			throw new Error("Not implemented");
		}
	}
}
