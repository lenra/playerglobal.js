package flashx.textLayout.elements {
	/**
	 *  ContainerFormattedElement is the root class for all container-level block elements, such as DivElement and TextFlow objects. Container-level block elements are grouping elements for other FlowElement objects. <p><span class="classHeaderTableLabel">Default MXML Property</span><code>mxmlChildren</code></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="DivElement.html" target="">DivElement</a>
	 *  <br>
	 *  <a href="TextFlow.html" target="">TextFlow</a>
	 * </div><br><hr>
	 */
	public class ContainerFormattedElement extends ParagraphFormattedElement {
	}
}
