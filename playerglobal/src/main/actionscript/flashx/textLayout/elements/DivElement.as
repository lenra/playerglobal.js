package flashx.textLayout.elements {
	/**
	 *  The DivElement class defines an element for grouping paragraphs (ParagraphElement objects). If you want a group of paragraphs to share the same formatting attributes, you can group them in a DivElement object and apply the attributes to it. The paragraphs will inherit the attributes from the DivElement object. <p><span class="classHeaderTableLabel">Default MXML Property</span><code>mxmlChildren</code></p><p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS02f7d8d4857b1677-165a04e1126951a2d98-7ffc.html" target="_blank">Creating TextFlow objects</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="ParagraphElement.html" target="">ParagraphElement</a>
	 *  <br>
	 *  <a href="TextFlow.html" target="">TextFlow</a>
	 * </div><br><hr>
	 */
	public class DivElement extends ContainerFormattedElement {
	}
}
