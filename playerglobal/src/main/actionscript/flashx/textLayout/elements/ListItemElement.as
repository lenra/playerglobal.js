package flashx.textLayout.elements {
	/**
	 *  <p> ListItemElement is an item in a list. It most commonly contains one or more ParagraphElement objects, but could also have children of type DivElement or ListElement. A ListItemElement always appears within a ListElement.</p> <p>A ListItemElement has automatically generated content that appears before the regular content of the list. This is called the <i>marker</i>, and it is what visually distinguishes the list item. The listStyleType property governs how the marker is generated and allows the user to control whether the list item is marked with a bullet, a number, or alphabetically. The listStylePosition governs where the marker appears relative to the list item; specifically it may appear outside, in the margin of the list, or inside, beside the list item itself. The ListMarkerFormat defines the TextLayoutFormat of the marker (by default this will be the same as the list item), as well as an optional suffix that goes at the end of the marker. For instance, for a numbered list, it is common to have a "." as a suffix that appears after the number. The ListMarkerFormat also allows specification of text that goes at the start of the marker, and for numbered lists allows control over the numbering.</p> <p><span class="classHeaderTableLabel">Default MXML Property</span><code>mxmlChildren</code></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS19f279b149e7481c7fdc883d12de39353f1-8000.html" target="_blank">Using numbered and bulleted lists with TLF-based text controls</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS19f279b149e7481c6944fe6612e01ed0f26-8000.html" target="_blank">Using numbered and bulleted lists</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="ParagraphElement.html" target="">ParagraphElement</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/formats/ITextLayoutFormat.html#listStyleType" target="">flashx.textLayout.formats.ITextLayoutFormat.listStyleType</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/formats/ITextLayoutFormat.html#listStylePosition" target="">flashx.textLayout.formats.ITextLayoutFormat.listStylePosition</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/formats/ListMarkerFormat.html" target="">flashx.textLayout.formats.ListMarkerFormat</a>
	 * </div><br><hr>
	 */
	public class ListItemElement extends ContainerFormattedElement {
	}
}
