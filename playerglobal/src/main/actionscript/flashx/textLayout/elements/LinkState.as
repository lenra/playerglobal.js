package flashx.textLayout.elements {
	/**
	 *  The LinkState class defines a set of constants for the <code>linkState</code> property of the LinkElement class. <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="LinkElement.html#linkState" target="">LinkElement.linkState</a>
	 * </div><br><hr>
	 */
	public class LinkState {
		/**
		 * <p> Value for the active state, which occurs when you hold the mouse down over a link. </p>
		 */
		public static const ACTIVE:String = "active";
		/**
		 * <p> Value for the hover state, which occurs when you drag the mouse over a link. </p>
		 */
		public static const HOVER:String = "hover";
		/**
		 * <p> Value for the normal, default link state. </p>
		 */
		public static const LINK:String = "link";
	}
}
