package flashx.textLayout.elements {
	import flashx.textLayout.edit.SelectionFormat;
	import flashx.textLayout.formats.IListMarkerFormat;
	import flashx.textLayout.formats.ITextLayoutFormat;

	/**
	 *  The Configuration class is a primary point of integration between the Text Layout Framework and an application. You can include a Configuration object as a parameter to the <code>TextFlow()</code> constructor when you create a new TextFlow instance. It allows the application to initially control how the Text Layout Framework behaves. <p>The Configuration class allows you to specify initial, paragraph and container formats for the text flow through the <code>textFlowInitialFormat</code> property. It also allows you to specify initial format attributes for links, selection, scrolling, and for handling the Tab and Enter keys.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS02f7d8d4857b1677-165a04e1126951a2d98-7fef.html" target="_blank">Applying styles with the Configuration class</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flashx/textLayout/formats/ITextLayoutFormat.html" target="">ITextLayoutFormat</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/edit/SelectionFormat.html" target="">SelectionFormat</a>
	 *  <br>
	 *  <a href="TextFlow.html" target="">TextFlow</a>
	 * </div><br><hr>
	 */
	public class Configuration implements IConfiguration {
		private var _defaultLinkActiveFormat:ITextLayoutFormat;
		private var _defaultLinkHoverFormat:ITextLayoutFormat;
		private var _defaultLinkNormalFormat:ITextLayoutFormat;
		private var _defaultListMarkerFormat:IListMarkerFormat;
		private var _enableAccessibility:Boolean;
		private var _flowComposerClass:Class;
		private var _focusedSelectionFormat:SelectionFormat;
		private var _inactiveSelectionFormat:SelectionFormat;
		private var _inlineGraphicResolverFunction:Function;
		private var _manageEnterKey:Boolean;
		private var _manageTabKey:Boolean;
		private var _overflowPolicy:String;
		private var _releaseLineCreationData:Boolean;
		private var _scrollDragDelay:Number;
		private var _scrollDragPixels:Number;
		private var _scrollMouseWheelMultiplier:Number;
		private var _scrollPagePercentage:Number;
		private var _textFlowInitialFormat:ITextLayoutFormat;
		private var _unfocusedSelectionFormat:SelectionFormat;

		/**
		 * <p> Constructor - creates a default configuration. </p>
		 * 
		 * @param initializeWithDefaults  — Specifies whether to initialize the configuration with the default values. Default is <code>true</code>. If set to <code>false</code>, initializes without default values, thereby saving some objects. The <code>clone()</code> method sets this to <code>false</code> and copies the properties from the original object. 
		 */
		public function Configuration(initializeWithDefaults:Boolean = true) {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Specifies the active character format attributes that initially apply for all links (LinkElement objects) in the text flow. These are defaults for new LinkElement objects that don't specify values for these attributes. </p>
		 * <p>Default is <code>null</code>.</p>
		 * 
		 * @return 
		 */
		public function get defaultLinkActiveFormat():ITextLayoutFormat {
			return _defaultLinkActiveFormat;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set defaultLinkActiveFormat(value:ITextLayoutFormat):void {
			_defaultLinkActiveFormat = value;
		}

		/**
		 * <p> Specifies the initial character format attributes that apply to a link (LinkElement) in the text flow when the cursor hovers over it. These are defaults for new LinkElement objects that don't specify values for these attributes. </p>
		 * <p>Default is <code>null</code>.</p>
		 * 
		 * @return 
		 */
		public function get defaultLinkHoverFormat():ITextLayoutFormat {
			return _defaultLinkHoverFormat;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set defaultLinkHoverFormat(value:ITextLayoutFormat):void {
			_defaultLinkHoverFormat = value;
		}

		/**
		 * <p> Specifies the initial link attributes for all LinkElement objects in the text flow. These are default values for new LinkElement objects that don't specify values for these attributes. The default normal format displays the link in blue with underlining. </p>
		 * 
		 * @return 
		 */
		public function get defaultLinkNormalFormat():ITextLayoutFormat {
			return _defaultLinkNormalFormat;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set defaultLinkNormalFormat(value:ITextLayoutFormat):void {
			_defaultLinkNormalFormat = value;
		}

		/**
		 * <p> Specifies the active character format attributes that initially apply for all ListItems in the text flow. These are defaults for new ListItemElements objects that don't specify values for these attributes. </p>
		 * <p>Default is <code>null</code>.</p>
		 * 
		 * @return 
		 */
		public function get defaultListMarkerFormat():IListMarkerFormat {
			return _defaultListMarkerFormat;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set defaultListMarkerFormat(value:IListMarkerFormat):void {
			_defaultListMarkerFormat = value;
		}

		/**
		 * <p> Specifies whether accessibility support is turned on or not. If <code>true</code>, screen readers can read the TextFlow contents. </p>
		 * <p>Default value is <code>false</code>.</p>
		 * 
		 * @return 
		 */
		public function get enableAccessibility():Boolean {
			return _enableAccessibility;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set enableAccessibility(value:Boolean):void {
			_enableAccessibility = value;
		}

		/**
		 * <p> Specifies the type of flow composer to attach to a new TextFlow object by default. Default value is StandardFlowComposer. </p>
		 * 
		 * @return 
		 */
		public function get flowComposerClass():Class {
			return _flowComposerClass;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set flowComposerClass(value:Class):void {
			_flowComposerClass = value;
		}

		/**
		 * <p> The initial selection format (SelectionFormat) for a text flow (TextFlow) when its window has focus. Text Layout Framework uses <code>focusedSelectionFormat</code> to draw the selection when the window is active and one of the containers in the TextFlow has focus. You can override this format using <code>SelectionManager.focusedSelectionFormat</code>, if desired. </p>
		 * <p>The SelectionFormat class specifies the default values, which invert the color of the text and its background.</p>
		 * 
		 * @return 
		 */
		public function get focusedSelectionFormat():SelectionFormat {
			return _focusedSelectionFormat;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set focusedSelectionFormat(value:SelectionFormat):void {
			_focusedSelectionFormat = value;
		}

		/**
		 * <p> The initial selection format (SelectionFormat) for a text flow (TextFlow) when its window is inactive. Text Layout Framework uses <code>inactiveSelectionFormat</code> for drawing the selection when the window is inactive. You can override this format using <code>SelectionManager.inactiveSelectionFormat</code>, if desired. </p>
		 * <p>If you do not override <code>unfocusedSelectionFormat</code> the SelectionFormat values used are:</p>
		 * <ul>
		 *  <li><code>color = 0xffffff</code> (white)</li>
		 *  <li><code>alpha = 0</code></li>
		 *  <li><code>blendMode = flash.display.BlendMode.DIFFERENCE</code></li>
		 * </ul>
		 * <p>The result is no selection is displayed.</p>
		 * 
		 * @return 
		 */
		public function get inactiveSelectionFormat():SelectionFormat {
			return _inactiveSelectionFormat;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set inactiveSelectionFormat(value:SelectionFormat):void {
			_inactiveSelectionFormat = value;
		}

		/**
		 * <p> Specifies the callback used for resolving an inline graphic element. The callback takes a <code>flashx.textLayout.elements.InlineGraphicElement</code> object and returns the value to be used as the element's <code>flashx.textLayout.elements.InlineGraphicElement#source</code>. This callback provides the mechanism to delay providing an inline graphic element's source until just before it is composed. </p>
		 * <p><b>Note:</b> this callback will be invoked only if a placeholder source of String type is already set. Moreover, it may be invoked multiple times. </p>
		 * 
		 * @return 
		 */
		public function get inlineGraphicResolverFunction():Function {
			return _inlineGraphicResolverFunction;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set inlineGraphicResolverFunction(value:Function):void {
			_inlineGraphicResolverFunction = value;
		}

		/**
		 * <p> Specifies whether the Enter / Return key is entered as text by Text Layout Framework, to split a paragraph for example, or the client code handles it. The client code might handle it by committing a form that has a default button for that purpose, for example. </p>
		 * <p>Default value is <code>true</code>.</p>
		 * 
		 * @return 
		 */
		public function get manageEnterKey():Boolean {
			return _manageEnterKey;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set manageEnterKey(value:Boolean):void {
			_manageEnterKey = value;
		}

		/**
		 * <p> Specifies whether the TAB key is entered as text by Text Layout Framework, or Flash Player or AIR handles it and turns it into a tabbed panel event. </p>
		 * <p>Default value is <code>false</code>.</p>
		 * 
		 * @return 
		 */
		public function get manageTabKey():Boolean {
			return _manageTabKey;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set manageTabKey(value:Boolean):void {
			_manageTabKey = value;
		}

		/**
		 * <p> Policy used for deciding whether the last line of a container fits in the container, or whether it overflows. Use the constants of the OverflowPolicy class to set this property. </p>
		 * <p>Default value is OverflowPolicy.FIT_DESCENDERS, which fits the line in the composition area if the area from the top to the baseline fits.</p>
		 * 
		 * @return 
		 */
		public function get overflowPolicy():String {
			return _overflowPolicy;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set overflowPolicy(value:String):void {
			_overflowPolicy = value;
		}

		/**
		 * <p> Requests that the process of composing text release line creation data after composing each paragraph. This request saves memory but slows down the composing process. </p>
		 * <p>Default value is <code>false</code>.</p>
		 * 
		 * @return 
		 */
		public function get releaseLineCreationData():Boolean {
			return _releaseLineCreationData;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set releaseLineCreationData(value:Boolean):void {
			_releaseLineCreationData = value;
		}

		/**
		 * <p> Specifies a timed delay between one scroll and the next to prevent scrolling from going too fast. This value specifies what the delay is in milliseconds. The default value is 35. </p>
		 * 
		 * @return 
		 */
		public function get scrollDragDelay():Number {
			return _scrollDragDelay;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set scrollDragDelay(value:Number):void {
			_scrollDragDelay = value;
		}

		/**
		 * <p> Specifies the default number of pixels to scroll when the user initiates auto scrolling by dragging the selection. Default value is 20. </p>
		 * 
		 * @return 
		 */
		public function get scrollDragPixels():Number {
			return _scrollDragPixels;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set scrollDragPixels(value:Number):void {
			_scrollDragPixels = value;
		}

		/**
		 * <p> Specifies the default number of pixels to scroll for Mouse wheel events. Default value is 20. </p>
		 * 
		 * @return 
		 */
		public function get scrollMouseWheelMultiplier():Number {
			return _scrollMouseWheelMultiplier;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set scrollMouseWheelMultiplier(value:Number):void {
			_scrollMouseWheelMultiplier = value;
		}

		/**
		 * <p> Specifies the default percentage of the text flow to scroll for page scrolls. Default value is 7.0 / 8.0, or .875. </p>
		 * 
		 * @return 
		 */
		public function get scrollPagePercentage():Number {
			return _scrollPagePercentage;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set scrollPagePercentage(value:Number):void {
			_scrollPagePercentage = value;
		}

		/**
		 * <p> Specifies the initial format TextLayoutFormat configuration for a text flow (TextFlow object). </p>
		 * <p>Default is <code>null</code>.</p>
		 * 
		 * @return 
		 */
		public function get textFlowInitialFormat():ITextLayoutFormat {
			return _textFlowInitialFormat;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set textFlowInitialFormat(value:ITextLayoutFormat):void {
			_textFlowInitialFormat = value;
		}

		/**
		 * <p> The initial selection format that Text Layout Framework uses to draw the selection when the window is active but none of the containers in the TextFlow have focus. You can override this format using <code>SelectionManager.unfocusedSelectionFormat</code>, if desired. </p>
		 * <p>If you do not override <code>unfocusedSelectionFormat</code> the SelectionFormat values used are:</p>
		 * <ul>
		 *  <li><code>color = 0xffffff</code> (white)</li>
		 *  <li><code>alpha = 0</code></li>
		 *  <li><code>blendMode = flash.display.BlendMode.DIFFERENCE</code></li>
		 * </ul>
		 * <p>The result is no selection is displayed.</p>
		 * 
		 * @return 
		 */
		public function get unfocusedSelectionFormat():SelectionFormat {
			return _unfocusedSelectionFormat;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set unfocusedSelectionFormat(value:SelectionFormat):void {
			_unfocusedSelectionFormat = value;
		}

		/**
		 * <p> Creates a clone of the Configuration object. </p>
		 * 
		 * @return 
		 */
		public function clone():Configuration {
			throw new Error("Not implemented");
		}
	}
}
