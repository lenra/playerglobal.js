package flashx.textLayout.elements {
	import flash.display.DisplayObject;

	/**
	 *  The InlineGraphicElement class handles graphic objects that display inline in the text. <p>You can embed a graphic or any DisplayObject or specify a URl for the location of the graphic. The <code>height</code> and <code>width</code> properties of InlineGraphicElement control the actual size of the graphic to display. These values also control how much space to allocate for the graphic in the TextLine object that contains the graphic. The <code>height</code> and <code>width</code> properties each can be one of:</p> <ol> 
	 *  <li>A number of pixels</li> 
	 *  <li>A percent of the measured size of the image</li> 
	 *  <li>The constant, "auto", which computes the size (Default value)</li> 
	 * </ol> There are three properties, or accessors, pertaining to the width and height of a graphic: <ul> 
	 *  <li>The <code>width</code> and <code>height</code> properties</li> 
	 *  <li>The <code>measuredWidth</code> and <code>measuredHeight</code> properties, which are the width or height of the graphic at load time</li> 
	 *  <li>The <code>actualWidth</code> and <code>actualHeight</code> properties, which are the actual display and compose width and height of the graphic as computed from <code>width</code> or <code>height</code> and <code>measuredWidth</code> or <code>measuredHeight</code> </li> 
	 * </ul> <p>The values of the <code>actualWidth</code> and <code>actualHeight</code> properties are always zero until the graphic is loaded.</p> <p>If <code>source</code> is specified as a URI, the graphic is loaded asynchronously. If it's a DisplayObject, TextLayout uses the <code>width</code> and <code>height</code> at the time the graphic is set into the InlineGraphicElement object as <code>measuredHeight</code> and <code>measuredWidth</code>; its width and height are read immediately.</p> <p> <b>Notes</b>: For graphics that are loaded asynchronously the user must listen for a StatusChangeEvent.INLINE_GRAPHIC_STATUS_CHANGE event on the TextFlow and call <code>IFlowComposer.updateAllControllers()</code> to have the graphic appear. The value of <code>measuredWidth</code> and <code>measuredHeight</code> for graphics that are in the process of loading is zero.</p> <p>Some inline graphics are animations or videos that possibly have audio. They begin to run the first time they are composed after they finish loading. They don't stop running until the flowComposer on the TextFlow is set to null. At that time they are stopped and unloaded.</p> The following restrictions apply to InLineGraphicElement objects: <ol> 
	 *  <li>On export of TLFMarkup, source is converted to a string. If the graphic element is a class, the Text Layout Framework can't export it properly</li>.
	 *  <li>When doing a copy/paste operation of an InlineGraphicElement, if source can't be used to create a new InLineGraphicElement, it won't be pasted. For example if source is a DisplayObject, or if the graphic is set directly, it can't be duplicated. Best results are obtained if the source is the class of an embedded graphic though that doesn't export/import.</li> 
	 *  <li>InLineGraphicElement objects work in the factory (TextFlowTextLineFactory) only if the source is a class or if you explicitly set the graphic to a loaded graphic. InlineGraphic objects that require delayed loads generally do not show up.</li> 
	 * </ol> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS02f7d8d4857b1677-165a04e1126951a2d98-7ffc.html" target="_blank">Creating TextFlow objects</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS02f7d8d4857b1677-165a04e1126951a2d98-7ff4.html" target="_blank">Adding images with TLF</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="InlineGraphicElement.html#actualHeight" target="">actualHeight</a>
	 *  <br>
	 *  <a href="InlineGraphicElement.html#actualWidth" target="">actualWidth</a>
	 *  <br>
	 *  <a href="../../../flash/display/DisplayObject.html" target="">DisplayObject</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/compose/IFlowComposer.html#updateAllControllers()" target="">flashx.textLayout.compose.IFlowComposer.updateAllControllers()</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/events/StatusChangeEvent.html" target="">StatusChangeEvent</a>
	 *  <br>
	 *  <a href="TextFlow.html" target="">TextFlow</a>
	 * </div><br><hr>
	 */
	public class InlineGraphicElement extends FlowLeafElement {
		private var _float:*;
		private var _height:*;
		private var _source:Object;
		private var _width:*;

		private var _actualHeight:Number;
		private var _actualWidth:Number;
		private var _graphic:DisplayObject;
		private var _measuredHeight:Number;
		private var _measuredWidth:Number;
		private var _status:String;

		/**
		 * <p> Constructor - create new InlineGraphicElement object </p>
		 */
		public function InlineGraphicElement() {
			throw new Error("Not implemented");
		}

		/**
		 * <p> The actual height in effect. This is the display and compose height that's computed from the <code>height</code> and <code>measuredHeight</code> properties. </p>
		 * <p>The values of the <code>actualHeight</code> property are computed according to the following table:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>height property</th>
		 *    <th>actualHeight</th>
		 *   </tr>
		 *   <tr>
		 *    <td>auto</td>
		 *    <td>measuredheight</td>
		 *   </tr>
		 *   <tr>
		 *    <td>h a Percent</td>
		 *    <td>h percent of measuredheight</td>
		 *   </tr>
		 *   <tr>
		 *    <td>h a Number</td>
		 *    <td>h</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><b>Notes</b>: If the inline graphic is a DisplayObject, its width and height are read immmediately. If <code>measuredWidth</code> or <code>measuredHeight</code> are zero, then any auto calculations that would cause a divide by zero sets the result to zero.</p>
		 * 
		 * @return 
		 */
		public function get actualHeight():Number {
			return _actualHeight;
		}

		/**
		 * <p> The actual width in effect. This is the display and compose width that's computed from the <code>width</code> and <code>measuredWidth</code> properties. </p>
		 * <p>The values of the <code>actualWidth</code>property are computed according to the following table:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>width property</th>
		 *    <th>actualWidth</th>
		 *   </tr>
		 *   <tr>
		 *    <td>auto</td>
		 *    <td>measuredWidth</td>
		 *   </tr>
		 *   <tr>
		 *    <td>w a Percent</td>
		 *    <td>w percent of measuredWidth</td>
		 *   </tr>
		 *   <tr>
		 *    <td>w a Number</td>
		 *    <td>w</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><b>Notes</b>: If the inline graphic is a DisplayObject, its width and height are read immediately. If <code>measuredWidth</code> or <code>measuredHeight</code> are zero, then any auto calculations that would cause a divide by zero sets the result to zero.</p>
		 * 
		 * @return 
		 */
		public function get actualWidth():Number {
			return _actualWidth;
		}

		/**
		 * <p> Controls the placement of the graphic relative to the text. It can be part of the line, or can be beside the line with the text wrapped around it. </p>
		 * <p>Legal values are <code>flashx.textLayout.formats.Float.NONE</code>, <code>flashx.textLayout.formats.Float.LEFT</code>, <code>flashx.textLayout.formats.Float.RIGHT</code>, <code>flashx.textLayout.formats.Float.START</code>, and <code>flashx.textLayout.formats.Float.END</code>.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined will be treated as <code>Float.NONE</code>.</p>
		 * 
		 * @return 
		 */
		public function get float():* {
			return _float;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set float(value:*):void {
			_float = value;
		}

		/**
		 * <p> The embedded graphic. </p>
		 * 
		 * @return 
		 */
		public function get graphic():DisplayObject {
			return _graphic;
		}

		/**
		 * <p> The height of the image. May be 'auto', a number of pixels or a percent of the measured height. </p>
		 * <p>Legal values are flashx.textLayout.formats.FormatValue.AUTO and flashx.textLayout.formats.FormatValue.INHERIT.</p>
		 * <p>Legal values as a number are from 0 to 32000.</p>
		 * <p>Legal values as a percent are numbers from 0 to 1000000.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined or "inherit" the InlineGraphicElement will use the default value of "auto".</p>
		 * 
		 * @return 
		 */
		public function get height():* {
			return _height;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set height(value:*):void {
			_height = value;
		}

		/**
		 * <p> The natural height of the graphic. This is the height of the graphic at load time. </p>
		 * 
		 * @return 
		 */
		public function get measuredHeight():Number {
			return _measuredHeight;
		}

		/**
		 * <p> The natural width of the graphic. This is the width of the graphic at load time. </p>
		 * 
		 * @return 
		 */
		public function get measuredWidth():Number {
			return _measuredWidth;
		}

		/**
		 * <p> Sets the source for the graphic. The value can be either a String that is interpreted as a URI, a Class that's interpreted as the class of an embeddded DisplayObject, a DisplayObject instance, or a URLRequest. Creates a DisplayObject and, if the InlineGraphicElement object is added into a ParagraphElement in a TextFlow object, causes it to appear inline in the text. </p>
		 * 
		 * @return 
		 */
		public function get source():Object {
			return _source;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set source(value:Object):void {
			_source = value;
		}

		/**
		 * <p> The current status of the image. On each status change the owning TextFlow sends a StatusChangeEvent. </p>
		 * 
		 * @return 
		 */
		public function get status():String {
			return _status;
		}

		/**
		 * <p> The width of the graphic. The value can be 'auto', a number of pixels or a percent of the measured width of the image. </p>
		 * <p>Legal values are flashx.textLayout.formats.FormatValue.AUTO and flashx.textLayout.formats.FormatValue.INHERIT.</p>
		 * <p>Legal values as a number are from 0 to 32000.</p>
		 * <p>Legal values as a percent are numbers from 0 to 1000000.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined or "inherit" the InlineGraphicElement will use the default value of "auto".</p>
		 * 
		 * @return 
		 */
		public function get width():* {
			return _width;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set width(value:*):void {
			_width = value;
		}
	}
}
