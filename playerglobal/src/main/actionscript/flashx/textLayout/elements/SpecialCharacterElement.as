package flashx.textLayout.elements {
	/**
	 *  The SpecialCharacterElement class is an abstract base class for elements that represent special characters. <p>You cannot create a SpecialCharacterElement object directly. Invoking <code>new SpecialCharacterElement()</code> throws an error exception.</p> <p><span class="classHeaderTableLabel">Default MXML Property</span><code>mxmlChildren</code></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="BreakElement.html" target="">BreakElement</a>
	 *  <br>
	 *  <a href="TabElement.html" target="">TabElement</a>
	 * </div><br><hr>
	 */
	public class SpecialCharacterElement extends SpanElement {

		/**
		 * <p> Base class - invoking <code>new SpecialCharacterElement()</code> throws an error exception. </p>
		 */
		public function SpecialCharacterElement() {
			throw new Error("Not implemented");
		}
	}
}
