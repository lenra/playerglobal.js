package flashx.textLayout.elements {
	import flashx.textLayout.edit.SelectionFormat;
	import flashx.textLayout.formats.IListMarkerFormat;
	import flashx.textLayout.formats.ITextLayoutFormat;

	/**
	 *  Read-only interface to a configuration object. Used by TextFlow to guarantee it has an unchangeable configuration once its constructed. <br><hr>
	 */
	public interface IConfiguration {

		/**
		 * <p> Specifies the active character format attributes that initially apply for all links (LinkElement objects) in the text flow. These are defaults for new LinkElement objects that don't specify values for these attributes. </p>
		 * <p>Default is <code>null</code>.</p>
		 * 
		 * @return 
		 */
		function get defaultLinkActiveFormat():ITextLayoutFormat;

		/**
		 * <p> Specifies the initial character format attributes that apply to a link (LinkElement) in the text flow when the cursor hovers over it. These are defaults for new LinkElement objects that don't specify values for these attributes. </p>
		 * <p>Default is <code>null</code>.</p>
		 * 
		 * @return 
		 */
		function get defaultLinkHoverFormat():ITextLayoutFormat;

		/**
		 * <p> Specifies the initial link attributes for all LinkElement objects in the text flow. These are default values for new LinkElement objects that don't specify values for these attributes. The default normal format displays the link in blue with underlining. </p>
		 * 
		 * @return 
		 */
		function get defaultLinkNormalFormat():ITextLayoutFormat;

		/**
		 * <p> Specifies the active character format attributes that initially apply for all ListItems in the text flow. These are defaults for new ListItemElements objects that don't specify values for these attributes. </p>
		 * <p>Default is <code>null</code>.</p>
		 * 
		 * @return 
		 */
		function get defaultListMarkerFormat():IListMarkerFormat;

		/**
		 * <p> Specifies whether accessibility support is turned on or not. If <code>true</code>, screen readers can read the TextFlow contents. </p>
		 * <p>Default value is <code>false</code>.</p>
		 * 
		 * @return 
		 */
		function get enableAccessibility():Boolean;

		/**
		 * <p> Specifies the type of flow composer to attach to a new TextFlow object by default. Default value is StandardFlowComposer. </p>
		 * 
		 * @return 
		 */
		function get flowComposerClass():Class;

		/**
		 * <p> The initial selection format (SelectionFormat) for a text flow (TextFlow) when its window has focus. Text Layout Framework uses <code>focusedSelectionFormat</code> to draw the selection when the window is active and one of the containers in the TextFlow has focus. You can override this format using <code>SelectionManager.focusedSelectionFormat</code>, if desired. </p>
		 * <p>The SelectionFormat class specifies the default values, which invert the color of the text and its background.</p>
		 * 
		 * @return 
		 */
		function get focusedSelectionFormat():SelectionFormat;

		/**
		 * <p> The initial selection format (SelectionFormat) for a text flow (TextFlow) when its window is inactive. Text Layout Framework uses <code>inactiveSelectionFormat</code> for drawing the selection when the window is inactive. You can override this format using <code>SelectionManager.inactiveSelectionFormat</code>, if desired. </p>
		 * <p>If you do not override <code>unfocusedSelectionFormat</code> the SelectionFormat values used are:</p>
		 * <ul>
		 *  <li><code>color = 0xffffff</code> (white)</li>
		 *  <li><code>alpha = 0</code></li>
		 *  <li><code>blendMode = flash.display.BlendMode.DIFFERENCE</code></li>
		 * </ul>
		 * <p>The result is no selection is displayed.</p>
		 * 
		 * @return 
		 */
		function get inactiveSelectionFormat():SelectionFormat;

		/**
		 * <p> Specifies the callback used for resolving an inline graphic element. The callback takes a <code>flashx.textLayout.elements.InlineGraphicElement</code> object and returns the value to be used as the element's <code>flashx.textLayout.elements.InlineGraphicElement#source</code>. This callback provides the mechanism to delay providing an inline graphic element's source until just before it is composed. </p>
		 * <p><b>Note:</b> this callback will be invoked only if a placeholder source of String type is already set. Moreover, it may be invoked multiple times. </p>
		 * 
		 * @return 
		 */
		function get inlineGraphicResolverFunction():Function;

		/**
		 * <p> Specifies whether the Enter / Return key is entered as text by Text Layout Framework, to split a paragraph for example, or the client code handles it. The client code might handle it by committing a form that has a default button for that purpose, for example. </p>
		 * <p>Default value is <code>true</code>.</p>
		 * 
		 * @return 
		 */
		function get manageEnterKey():Boolean;

		/**
		 * <p> Specifies whether the TAB key is entered as text by Text Layout Framework, or Flash Player or AIR handles it and turns it into a tabbed panel event. </p>
		 * <p>Default value is <code>false</code>.</p>
		 * 
		 * @return 
		 */
		function get manageTabKey():Boolean;

		/**
		 * <p> Policy used for deciding whether the last line of a container fits in the container, or whether it overflows. Use the constants of the OverflowPolicy class to set this property. </p>
		 * <p>Default value is OverflowPolicy.FIT_DESCENDERS, which fits the line in the composition area if the area from the top to the baseline fits.</p>
		 * 
		 * @return 
		 */
		function get overflowPolicy():String;

		/**
		 * <p> Requests that the process of composing text release line creation data after composing each paragraph. This request saves memory but slows down the composing process. </p>
		 * <p>Default value is <code>false</code>.</p>
		 * 
		 * @return 
		 */
		function get releaseLineCreationData():Boolean;

		/**
		 * <p> Specifies a timed delay between one scroll and the next to prevent scrolling from going too fast. This value specifies what the delay is in milliseconds. The default value is 35. </p>
		 * 
		 * @return 
		 */
		function get scrollDragDelay():Number;

		/**
		 * <p> Specifies the default number of pixels to scroll when the user initiates auto scrolling by dragging the selection. Default value is 20. </p>
		 * 
		 * @return 
		 */
		function get scrollDragPixels():Number;

		/**
		 * <p> Specifies the default number of pixels to scroll for Mouse wheel events. Default value is 20. </p>
		 * 
		 * @return 
		 */
		function get scrollMouseWheelMultiplier():Number;

		/**
		 * <p> Specifies the default percentage of the text flow to scroll for page scrolls. Default value is 7.0 / 8.0, or .875. </p>
		 * 
		 * @return 
		 */
		function get scrollPagePercentage():Number;

		/**
		 * <p> Specifies the initial format TextLayoutFormat configuration for a text flow (TextFlow object). </p>
		 * <p>Default is <code>null</code>.</p>
		 * 
		 * @return 
		 */
		function get textFlowInitialFormat():ITextLayoutFormat;

		/**
		 * <p> The initial selection format that Text Layout Framework uses to draw the selection when the window is active but none of the containers in the TextFlow have focus. You can override this format using <code>SelectionManager.unfocusedSelectionFormat</code>, if desired. </p>
		 * <p>If you do not override <code>unfocusedSelectionFormat</code> the SelectionFormat values used are:</p>
		 * <ul>
		 *  <li><code>color = 0xffffff</code> (white)</li>
		 *  <li><code>alpha = 0</code></li>
		 *  <li><code>blendMode = flash.display.BlendMode.DIFFERENCE</code></li>
		 * </ul>
		 * <p>The result is no selection is displayed.</p>
		 * 
		 * @return 
		 */
		function get unfocusedSelectionFormat():SelectionFormat;

		/**
		 * <p> Creates a writeable clone of the IConfiguration object. </p>
		 * 
		 * @return 
		 */
		function clone():Configuration;
	}
}
