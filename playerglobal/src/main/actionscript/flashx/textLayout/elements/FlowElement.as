package flashx.textLayout.elements {
	import flashx.textLayout.formats.ITextLayoutFormat;

	/**
	 *  The text in a flow is stored in tree form with the elements of the tree representing logical divisions within the text. The FlowElement class is the abstract base class of all the objects in this tree. FlowElement objects represent paragraphs, spans of text within paragraphs, and groups of paragraphs. <p>The root of a composable FlowElement tree is always a TextFlow object. Leaf elements of the tree are always subclasses of the FlowLeafElement class. All leaves arranged in a composable TextFlow have a ParagraphElement ancestor. </p> <p>You cannot create a FlowElement object directly. Invoking <code>new FlowElement()</code> throws an error exception.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="FlowGroupElement.html" target="">FlowGroupElement</a>
	 *  <br>
	 *  <a href="FlowLeafElement.html" target="">FlowLeafElement</a>
	 *  <br>
	 *  <a href="InlineGraphicElement.html" target="">InlineGraphicElement</a>
	 *  <br>
	 *  <a href="ParagraphElement.html" target="">ParagraphElement</a>
	 *  <br>
	 *  <a href="SpanElement.html" target="">SpanElement</a>
	 *  <br>
	 *  <a href="TextFlow.html" target="">TextFlow</a>
	 * </div><br><hr>
	 */
	public class FlowElement implements ITextLayoutFormat {
		private var _alignmentBaseline:*;
		private var _backgroundAlpha:*;
		private var _backgroundColor:*;
		private var _baselineShift:*;
		private var _blockProgression:*;
		private var _breakOpportunity:*;
		private var _cffHinting:*;
		private var _clearFloats:*;
		private var _color:*;
		private var _columnCount:*;
		private var _columnGap:*;
		private var _columnWidth:*;
		private var _digitCase:*;
		private var _digitWidth:*;
		private var _direction:*;
		private var _dominantBaseline:*;
		private var _firstBaselineOffset:*;
		private var _fontFamily:*;
		private var _fontLookup:*;
		private var _fontSize:*;
		private var _fontStyle:*;
		private var _fontWeight:*;
		private var _format:ITextLayoutFormat;
		private var _id:String;
		private var _justificationRule:*;
		private var _justificationStyle:*;
		private var _kerning:*;
		private var _leadingModel:*;
		private var _ligatureLevel:*;
		private var _lineBreak:*;
		private var _lineHeight:*;
		private var _lineThrough:*;
		private var _linkActiveFormat:*;
		private var _linkHoverFormat:*;
		private var _linkNormalFormat:*;
		private var _listAutoPadding:*;
		private var _listMarkerFormat:*;
		private var _listStylePosition:*;
		private var _listStyleType:*;
		private var _locale:*;
		private var _paddingBottom:*;
		private var _paddingLeft:*;
		private var _paddingRight:*;
		private var _paddingTop:*;
		private var _paragraphEndIndent:*;
		private var _paragraphSpaceAfter:*;
		private var _paragraphSpaceBefore:*;
		private var _paragraphStartIndent:*;
		private var _renderingMode:*;
		private var _styleName:*;
		private var _tabStops:*;
		private var _textAlign:*;
		private var _textAlignLast:*;
		private var _textAlpha:*;
		private var _textDecoration:*;
		private var _textIndent:*;
		private var _textJustify:*;
		private var _textRotation:*;
		private var _tracking:Object;
		private var _trackingLeft:*;
		private var _trackingRight:*;
		private var _typeName:String;
		private var _typographicCase:*;
		private var _userStyles:Object;
		private var _verticalAlign:*;
		private var _whiteSpaceCollapse:*;
		private var _wordSpacing:*;

		private var _computedFormat:ITextLayoutFormat;
		private var _coreStyles:Object;
		private var _parent:FlowGroupElement;
		private var _parentRelativeEnd:int;
		private var _parentRelativeStart:int;
		private var _styles:Object;
		private var _textLength:int;

		/**
		 * <p> Base class - invoking <code>new FlowElement()</code> throws an error exception. </p>
		 */
		public function FlowElement() {
			throw new Error("Not implemented");
		}

		/**
		 * <p> TextLayoutFormat: Specifies the baseline to which the dominant baseline aligns. For example, if you set <code>dominantBaseline</code> to ASCENT, setting <code>alignmentBaseline</code> to DESCENT aligns the top of the text with the DESCENT baseline, or below the line. The largest element in the line generally determines the baselines.</p>
		 * <p><img src="../../../images/textLayout_baselines.jpg" alt="baselines"></p>
		 * <p>Legal values are TextBaseline.ROMAN, TextBaseline.ASCENT, TextBaseline.DESCENT, TextBaseline.IDEOGRAPHIC_TOP, TextBaseline.IDEOGRAPHIC_CENTER, TextBaseline.IDEOGRAPHIC_BOTTOM, TextBaseline.USE_DOMINANT_BASELINE, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of TextBaseline.USE_DOMINANT_BASELINE.</p>
		 * 
		 * @return 
		 */
		public function get alignmentBaseline():* {
			return _alignmentBaseline;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set alignmentBaseline(value:*):void {
			_alignmentBaseline = value;
		}

		/**
		 * <p> TextLayoutFormat: Alpha (transparency) value for the background (adopts default value if undefined during cascade). A value of 0 is fully transparent, and a value of 1 is fully opaque. Display objects with alpha set to 0 are active, even though they are invisible. </p>
		 * <p>Legal values are numbers from 0 to 1 and FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will have a value of 1.</p>
		 * 
		 * @return 
		 */
		public function get backgroundAlpha():* {
			return _backgroundAlpha;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set backgroundAlpha(value:*):void {
			_backgroundAlpha = value;
		}

		/**
		 * <p> TextLayoutFormat: Background color of the text (adopts default value if undefined during cascade). Can be either the constant value <code>BackgroundColor.TRANSPARENT</code>, or a hexadecimal value that specifies the three 8-bit RGB (red, green, blue) values; for example, 0xFF0000 is red and 0x00FF00 is green. </p>
		 * <p>Legal values as a string are BackgroundColor.TRANSPARENT, FormatValue.INHERIT and uints from 0x0 to 0xffffffff.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will have a value of BackgroundColor.TRANSPARENT.</p>
		 * 
		 * @return 
		 */
		public function get backgroundColor():* {
			return _backgroundColor;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set backgroundColor(value:*):void {
			_backgroundColor = value;
		}

		/**
		 * <p> TextLayoutFormat: Amount to shift the baseline from the <code>dominantBaseline</code> value. Units are in pixels, or a percentage of <code>fontSize</code> (in which case, enter a string value, like 140%). Positive values shift the line up for horizontal text (right for vertical) and negative values shift it down for horizontal (left for vertical). </p>
		 * <p>Legal values are BaselineShift.SUPERSCRIPT, BaselineShift.SUBSCRIPT, FormatValue.INHERIT.</p>
		 * <p>Legal values as a number are from -1000 to 1000.</p>
		 * <p>Legal values as a percent are numbers from -1000 to 1000.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of 0.0.</p>
		 * 
		 * @return 
		 */
		public function get baselineShift():* {
			return _baselineShift;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set baselineShift(value:*):void {
			_baselineShift = value;
		}

		/**
		 * <p> TextLayoutFormat: Specifies a vertical or horizontal progression of line placement. Lines are either placed top-to-bottom (<code>BlockProgression.TB</code>, used for horizontal text) or right-to-left (<code>BlockProgression.RL</code>, used for vertical text). </p>
		 * <p>Legal values are BlockProgression.RL, BlockProgression.TB, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of BlockProgression.TB.</p>
		 * 
		 * @return 
		 */
		public function get blockProgression():* {
			return _blockProgression;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set blockProgression(value:*):void {
			_blockProgression = value;
		}

		/**
		 * <p> TextLayoutFormat: Controls where lines are allowed to break when breaking wrapping text into multiple lines. Set to <code>BreakOpportunity.AUTO</code> to break text normally. Set to <code>BreakOpportunity.NONE</code> to <i>not</i> break the text unless the text would overrun the measure and there are no other places to break the line. Set to <code>BreakOpportunity.ANY</code> to allow the line to break anywhere, rather than just between words. Set to <code>BreakOpportunity.ALL</code> to have each typographic cluster put on a separate line (useful for text on a path). </p>
		 * <p>Legal values are BreakOpportunity.ALL, BreakOpportunity.ANY, BreakOpportunity.AUTO, BreakOpportunity.NONE, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of BreakOpportunity.AUTO.</p>
		 * 
		 * @return 
		 */
		public function get breakOpportunity():* {
			return _breakOpportunity;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set breakOpportunity(value:*):void {
			_breakOpportunity = value;
		}

		/**
		 * <p> TextLayoutFormat: The type of CFF hinting used for this text. CFF hinting determines whether the Flash runtime forces strong horizontal stems to fit to a sub pixel grid or not. This property applies only if the <code>renderingMode</code> property is set to <code>RenderingMode.CFF</code>, and the font is embedded (<code>fontLookup</code> property is set to <code>FontLookup.EMBEDDED_CFF</code>). At small screen sizes, hinting produces a clear, legible text for human readers. </p>
		 * <p>Legal values are CFFHinting.NONE, CFFHinting.HORIZONTAL_STEM, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of CFFHinting.HORIZONTAL_STEM.</p>
		 * 
		 * @return 
		 */
		public function get cffHinting():* {
			return _cffHinting;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set cffHinting(value:*):void {
			_cffHinting = value;
		}

		/**
		 * <p> TextLayoutFormat: Controls how text wraps around a float. A value of none will allow the text to wrap most closely around a float. A value of left will cause the text to skip over any portion of the container that has a left float, and a value of right will cause the text to skip over any portion of the container that has a right float. A value of both will cause the text to skip over any floats. </p>
		 * <p>Legal values are ClearFloats.START, ClearFloats.END, ClearFloats.LEFT, ClearFloats.RIGHT, ClearFloats.BOTH, ClearFloats.NONE, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will have a value of ClearFloats.NONE.</p>
		 * 
		 * @return 
		 */
		public function get clearFloats():* {
			return _clearFloats;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set clearFloats(value:*):void {
			_clearFloats = value;
		}

		/**
		 * <p> TextLayoutFormat: Color of the text. A hexadecimal number that specifies three 8-bit RGB (red, green, blue) values; for example, 0xFF0000 is red and 0x00FF00 is green. </p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of 0.</p>
		 * 
		 * @return 
		 */
		public function get color():* {
			return _color;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set color(value:*):void {
			_color = value;
		}

		/**
		 * <p> TextLayoutFormat: Number of text columns (adopts default value if undefined during cascade). The column number overrides the other column settings. Value is an integer, or <code>FormatValue.AUTO</code> if unspecified. If <code>columnCount</code> is not specified,<code>columnWidth</code> is used to create as many columns as can fit in the container. </p>
		 * <p>Legal values as a string are FormatValue.AUTO, FormatValue.INHERIT and from ints from 1 to 50.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will have a value of FormatValue.AUTO.</p>
		 * 
		 * @return 
		 */
		public function get columnCount():* {
			return _columnCount;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set columnCount(value:*):void {
			_columnCount = value;
		}

		/**
		 * <p> TextLayoutFormat: Specifies the amount of gutter space, in pixels, to leave between the columns (adopts default value if undefined during cascade). Value is a Number </p>
		 * <p>Legal values are numbers from 0 to 1000 and FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will have a value of 20.</p>
		 * 
		 * @return 
		 */
		public function get columnGap():* {
			return _columnGap;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set columnGap(value:*):void {
			_columnGap = value;
		}

		/**
		 * <p> TextLayoutFormat: Column width in pixels (adopts default value if undefined during cascade). If you specify the width of the columns, but not the count, TextLayout will create as many columns of that width as possible, given the container width and <code>columnGap</code> settings. Any remainder space is left after the last column. Value is a Number. </p>
		 * <p>Legal values as a string are FormatValue.AUTO, FormatValue.INHERIT and numbers from 0 to 8000.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will have a value of FormatValue.AUTO.</p>
		 * 
		 * @return 
		 */
		public function get columnWidth():* {
			return _columnWidth;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set columnWidth(value:*):void {
			_columnWidth = value;
		}

		/**
		 * <p> Returns the computed format attributes that are in effect for this element. Takes into account the inheritance of attributes from parent elements. </p>
		 * 
		 * @return 
		 */
		public function get computedFormat():ITextLayoutFormat {
			return _computedFormat;
		}

		/**
		 * <p> Returns the <code>coreStyles</code> on this FlowElement. Note that the getter makes a copy of the core styles dictionary. The coreStyles object encapsulates the formats that are defined by TextLayoutFormat and are in TextLayoutFormat.description. The <code>coreStyles</code> object consists of an array of <i>stylename-value</i> pairs. </p>
		 * 
		 * @return 
		 */
		public function get coreStyles():Object {
			return _coreStyles;
		}

		/**
		 * <p> TextLayoutFormat: The type of digit case used for this text. Setting the value to <code>DigitCase.OLD_STYLE</code> approximates lowercase letterforms with varying ascenders and descenders. The figures are proportionally spaced. This style is only available in selected typefaces, most commonly in a supplemental or expert font. The <code>DigitCase.LINING</code> setting has all-cap height and is typically monospaced to line up in charts.</p>
		 * <p><img src="../../../images/textLayout_digitcase.gif" alt="digitCase"></p>
		 * <p>Legal values are DigitCase.DEFAULT, DigitCase.LINING, DigitCase.OLD_STYLE, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of DigitCase.DEFAULT.</p>
		 * 
		 * @return 
		 */
		public function get digitCase():* {
			return _digitCase;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set digitCase(value:*):void {
			_digitCase = value;
		}

		/**
		 * <p> TextLayoutFormat: Type of digit width used for this text. This can be <code>DigitWidth.PROPORTIONAL</code>, which looks best for individual numbers, or <code>DigitWidth.TABULAR</code>, which works best for numbers in tables, charts, and vertical rows.</p>
		 * <p><img src="../../../images/textLayout_digitwidth.gif" alt="digitWidth"></p>
		 * <p>Legal values are DigitWidth.DEFAULT, DigitWidth.PROPORTIONAL, DigitWidth.TABULAR, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of DigitWidth.DEFAULT.</p>
		 * 
		 * @return 
		 */
		public function get digitWidth():* {
			return _digitWidth;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set digitWidth(value:*):void {
			_digitWidth = value;
		}

		/**
		 * <p> TextLayoutFormat: Specifies the default bidirectional embedding level of the text in the text block. Left-to-right reading order, as in Latin-style scripts, or right-to-left reading order, as in Arabic or Hebrew. This property also affects column direction when it is applied at the container level. Columns can be either left-to-right or right-to-left, just like text. Below are some examples:</p>
		 * <p><img src="../../../images/textLayout_direction.gif" alt="direction"></p>
		 * <p>Legal values are Direction.LTR, Direction.RTL, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of Direction.LTR.</p>
		 * 
		 * @return 
		 */
		public function get direction():* {
			return _direction;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set direction(value:*):void {
			_direction = value;
		}

		/**
		 * <p> TextLayoutFormat: Specifies which element baseline snaps to the <code>alignmentBaseline</code> to determine the vertical position of the element on the line. A value of <code>TextBaseline.AUTO</code> selects the dominant baseline based on the <code>locale</code> property of the parent paragraph. For Japanese and Chinese, the selected baseline value is <code>TextBaseline.IDEOGRAPHIC_CENTER</code>; for all others it is <code>TextBaseline.ROMAN</code>. These baseline choices are determined by the choice of font and the font size.</p>
		 * <p><img src="../../../images/textLayout_baselines.jpg" alt="baselines"></p>
		 * <p>Legal values are FormatValue.AUTO, TextBaseline.ROMAN, TextBaseline.ASCENT, TextBaseline.DESCENT, TextBaseline.IDEOGRAPHIC_TOP, TextBaseline.IDEOGRAPHIC_CENTER, TextBaseline.IDEOGRAPHIC_BOTTOM, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of FormatValue.AUTO.</p>
		 * 
		 * @return 
		 */
		public function get dominantBaseline():* {
			return _dominantBaseline;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set dominantBaseline(value:*):void {
			_dominantBaseline = value;
		}

		/**
		 * <p> TextLayoutFormat: Specifies the baseline position of the first line in the container. Which baseline this property refers to depends on the container-level locale. For Japanese and Chinese, it is <code>TextBaseline.IDEOGRAPHIC_BOTTOM</code>; for all others it is <code>TextBaseline.ROMAN</code>. The offset from the top inset (or right inset if <code>blockProgression</code> is RL) of the container to the baseline of the first line can be either <code>BaselineOffset.ASCENT</code>, meaning equal to the ascent of the line, <code>BaselineOffset.LINE_HEIGHT</code>, meaning equal to the height of that first line, or any fixed-value number to specify an absolute distance. <code>BaselineOffset.AUTO</code> aligns the ascent of the line with the container top inset.</p>
		 * <p><img src="../../../images/textLayout_FBO1.png" alt="firstBaselineOffset1"><img src="../../../images/textLayout_FBO2.png" alt="firstBaselineOffset2"><img src="../../../images/textLayout_FBO3.png" alt="firstBaselineOffset3"><img src="../../../images/textLayout_FBO4.png" alt="firstBaselineOffset4"></p>
		 * <p>Legal values as a string are BaselineOffset.AUTO, BaselineOffset.ASCENT, BaselineOffset.LINE_HEIGHT, FormatValue.INHERIT and numbers from 0 to 1000.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of BaselineOffset.AUTO.</p>
		 * 
		 * @return 
		 */
		public function get firstBaselineOffset():* {
			return _firstBaselineOffset;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set firstBaselineOffset(value:*):void {
			_firstBaselineOffset = value;
		}

		/**
		 * <p> TextLayoutFormat: The name of the font to use, or a comma-separated list of font names. The Flash runtime renders the element with the first available font in the list. For example Arial, Helvetica, _sans causes the player to search for Arial, then Helvetica if Arial is not found, then _sans if neither is found. </p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of Arial.</p>
		 * 
		 * @return 
		 */
		public function get fontFamily():* {
			return _fontFamily;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set fontFamily(value:*):void {
			_fontFamily = value;
		}

		/**
		 * <p> TextLayoutFormat: Font lookup to use. Specifying <code>FontLookup.DEVICE</code> uses the fonts installed on the system that is running the SWF file. Device fonts result in a smaller movie size, but text is not always rendered the same across different systems and platforms. Specifying <code>FontLookup.EMBEDDED_CFF</code> uses font outlines embedded in the published SWF file. Embedded fonts increase the size of the SWF file (sometimes dramatically), but text is consistently displayed in the chosen font. </p>
		 * <p>Legal values are FontLookup.DEVICE, FontLookup.EMBEDDED_CFF, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of FontLookup.DEVICE.</p>
		 * 
		 * @return 
		 */
		public function get fontLookup():* {
			return _fontLookup;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set fontLookup(value:*):void {
			_fontLookup = value;
		}

		/**
		 * <p> TextLayoutFormat: The size of the text in pixels. </p>
		 * <p>Legal values are numbers from 1 to 720 and FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of 12.</p>
		 * 
		 * @return 
		 */
		public function get fontSize():* {
			return _fontSize;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set fontSize(value:*):void {
			_fontSize = value;
		}

		/**
		 * <p> TextLayoutFormat: Style of text. May be <code>FontPosture.NORMAL</code>, for use in plain text, or <code>FontPosture.ITALIC</code> for italic. This property applies only to device fonts (<code>fontLookup</code> property is set to flash.text.engine.FontLookup.DEVICE). </p>
		 * <p>Legal values are FontPosture.NORMAL, FontPosture.ITALIC, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of FontPosture.NORMAL.</p>
		 * 
		 * @return 
		 */
		public function get fontStyle():* {
			return _fontStyle;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set fontStyle(value:*):void {
			_fontStyle = value;
		}

		/**
		 * <p> TextLayoutFormat: Weight of text. May be <code>FontWeight.NORMAL</code> for use in plain text, or <code>FontWeight.BOLD</code>. Applies only to device fonts (<code>fontLookup</code> property is set to flash.text.engine.FontLookup.DEVICE). </p>
		 * <p>Legal values are FontWeight.NORMAL, FontWeight.BOLD, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of FontWeight.NORMAL.</p>
		 * 
		 * @return 
		 */
		public function get fontWeight():* {
			return _fontWeight;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set fontWeight(value:*):void {
			_fontWeight = value;
		}

		/**
		 * @return 
		 */
		public function get format():ITextLayoutFormat {
			return _format;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set format(value:ITextLayoutFormat):void {
			_format = value;
		}

		/**
		 * <p> Assigns an identifying name to the element, making it possible to set a style for the element by referencing the <code>id</code>. For example, the following line sets the color for a SpanElement object that has an id of span1: </p>
		 * <div class="listing" version="3.0">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *          textFlow.getElementByID("span1").setStyle("color", 0xFF0000);
		 *          </pre>
		 * </div>
		 * 
		 * @return 
		 */
		public function get id():String {
			return _id;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set id(value:String):void {
			_id = value;
		}

		/**
		 * <p> TextLayoutFormat: Rule used to justify text in a paragraph. Default value is <code>FormatValue.AUTO</code>, which justifies text based on the paragraph's <code>locale</code> property. For all languages except Japanese and Chinese, <code>FormatValue.AUTO</code> becomes <code>JustificationRule.SPACE</code>, which adds extra space to the space characters. For Japanese and Chinese, <code>FormatValue.AUTO</code> becomes <code>JustficationRule.EAST_ASIAN</code>. In part, justification changes the spacing of punctuation. In Roman text the comma and Japanese periods take a full character's width but in East Asian text only half of a character's width. Also, in the East Asian text the spacing between sequential punctuation marks becomes tighter, obeying traditional East Asian typographic conventions. Note, too, in the example below the leading that is applied to the second line of the paragraphs. In the East Asian version, the last two lines push left. In the Roman version, the second and following lines push left.</p>
		 * <p><img src="../../../images/textLayout_justificationrule.png" alt="justificationRule"></p>
		 * <p>Legal values are JustificationRule.EAST_ASIAN, JustificationRule.SPACE, FormatValue.AUTO, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of FormatValue.AUTO.</p>
		 * 
		 * @return 
		 */
		public function get justificationRule():* {
			return _justificationRule;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set justificationRule(value:*):void {
			_justificationRule = value;
		}

		/**
		 * <p> TextLayoutFormat: The style used for justification of the paragraph. Used only in conjunction with a <code>justificationRule</code> setting of <code>JustificationRule.EAST_ASIAN</code>. Default value of <code>FormatValue.AUTO</code> is resolved to <code>JustificationStyle.PUSH_IN_KINSOKU</code> for all locales. The constants defined by the JustificationStyle class specify options for handling kinsoku characters, which are Japanese characters that cannot appear at either the beginning or end of a line. If you want looser text, specify <code>JustificationStyle.PUSH-OUT-ONLY</code>. If you want behavior that is like what you get with the <code>justificationRule</code> of <code>JustificationRule.SPACE</code>, use <code>JustificationStyle.PRIORITIZE-LEAST-ADJUSTMENT</code>. </p>
		 * <p>Legal values are JustificationStyle.PRIORITIZE_LEAST_ADJUSTMENT, JustificationStyle.PUSH_IN_KINSOKU, JustificationStyle.PUSH_OUT_ONLY, FormatValue.AUTO, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of FormatValue.AUTO.</p>
		 * 
		 * @return 
		 */
		public function get justificationStyle():* {
			return _justificationStyle;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set justificationStyle(value:*):void {
			_justificationStyle = value;
		}

		/**
		 * <p> TextLayoutFormat: Kerning adjusts the pixels between certain character pairs to improve readability. Kerning is supported for all fonts with kerning tables. </p>
		 * <p>Legal values are Kerning.ON, Kerning.OFF, Kerning.AUTO, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of Kerning.AUTO.</p>
		 * 
		 * @return 
		 */
		public function get kerning():* {
			return _kerning;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set kerning(value:*):void {
			_kerning = value;
		}

		/**
		 * <p> TextLayoutFormat: Specifies the leading model, which is a combination of leading basis and leading direction. Leading basis is the baseline to which the <code>lineHeight</code> property refers. Leading direction determines whether the <code>lineHeight</code> property refers to the distance of a line's baseline from that of the line before it or the line after it. The default value of <code>FormatValue.AUTO</code> is resolved based on the paragraph's <code>locale</code> property. For Japanese and Chinese, it is <code>LeadingModel.IDEOGRAPHIC_TOP_DOWN</code> and for all others it is <code>LeadingModel.ROMAN_UP</code>.</p>
		 * <p><b>Leading Basis:</b></p>
		 * <p><img src="../../../images/textLayout_LB1.png" alt="leadingBasis1"> <img src="../../../images/textLayout_LB2.png" alt="leadingBasis2"> <img src="../../../images/textLayout_LB3.png" alt="leadingBasis3"></p>
		 * <p><b>Leading Direction:</b></p>
		 * <p><img src="../../../images/textLayout_LD1.png" alt="leadingDirection1"> <img src="../../../images/textLayout_LD2.png" alt="leadingDirection2"> <img src="../../../images/textLayout_LD3.png" alt="leadingDirection3"></p>
		 * <p>Legal values are LeadingModel.ROMAN_UP, LeadingModel.IDEOGRAPHIC_TOP_UP, LeadingModel.IDEOGRAPHIC_CENTER_UP, LeadingModel.IDEOGRAPHIC_TOP_DOWN, LeadingModel.IDEOGRAPHIC_CENTER_DOWN, LeadingModel.APPROXIMATE_TEXT_FIELD, LeadingModel.ASCENT_DESCENT_UP, LeadingModel.BOX, LeadingModel.AUTO, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of LeadingModel.AUTO.</p>
		 * 
		 * @return 
		 */
		public function get leadingModel():* {
			return _leadingModel;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set leadingModel(value:*):void {
			_leadingModel = value;
		}

		/**
		 * <p> TextLayoutFormat: Controls which of the ligatures that are defined in the font may be used in the text. The ligatures that appear for each of these settings is dependent on the font. A ligature occurs where two or more letter-forms are joined as a single glyph. Ligatures usually replace consecutive characters sharing common components, such as the letter pairs 'fi', 'fl', or 'ae'. They are used with both Latin and Non-Latin character sets. The ligatures enabled by the values of the LigatureLevel class - <code>MINIMUM</code>, <code>COMMON</code>, <code>UNCOMMON</code>, and <code>EXOTIC</code> - are additive. Each value enables a new set of ligatures, but also includes those of the previous types.</p>
		 * <p><b>Note: </b>When working with Arabic or Syriac fonts, <code>ligatureLevel</code> must be set to MINIMUM or above.</p>
		 * <p><img src="../../../images/textLayout_ligatures.png" alt="ligatureLevel"></p>
		 * <p>Legal values are LigatureLevel.MINIMUM, LigatureLevel.COMMON, LigatureLevel.UNCOMMON, LigatureLevel.EXOTIC, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of LigatureLevel.COMMON.</p>
		 * 
		 * @return 
		 */
		public function get ligatureLevel():* {
			return _ligatureLevel;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set ligatureLevel(value:*):void {
			_ligatureLevel = value;
		}

		/**
		 * <p> TextLayoutFormat: Controls word wrapping within the container (adopts default value if undefined during cascade). Text in the container may be set to fit the width of the container (<code>LineBreak.TO_FIT</code>), or can be set to break only at explicit return or line feed characters (<code>LineBreak.EXPLICIT</code>). </p>
		 * <p>Legal values are LineBreak.EXPLICIT, LineBreak.TO_FIT, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will have a value of LineBreak.TO_FIT.</p>
		 * 
		 * @return 
		 */
		public function get lineBreak():* {
			return _lineBreak;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set lineBreak(value:*):void {
			_lineBreak = value;
		}

		/**
		 * <p> TextLayoutFormat: Leading controls for the text. The distance from the baseline of the previous or the next line (based on <code>LeadingModel</code>) to the baseline of the current line is equal to the maximum amount of the leading applied to any character in the line. This is either a number or a percent. If specifying a percent, enter a string value, like 140%.</p>
		 * <p><img src="../../../images/textLayout_lineHeight1.jpg" alt="lineHeight1"><img src="../../../images/textLayout_lineHeight2.jpg" alt="lineHeight2"></p>
		 * <p>Legal values as a number are from -720 to 720.</p>
		 * <p>Legal values as a percent are numbers from -1000% to 1000%.</p>
		 * <p>Legal values include FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of 120%.</p>
		 * 
		 * @return 
		 */
		public function get lineHeight():* {
			return _lineHeight;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set lineHeight(value:*):void {
			_lineHeight = value;
		}

		/**
		 * <p> TextLayoutFormat: If <code>true</code>, applies strikethrough, a line drawn through the middle of the text. </p>
		 * <p>Legal values are true, false and FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of false.</p>
		 * 
		 * @return 
		 */
		public function get lineThrough():* {
			return _lineThrough;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set lineThrough(value:*):void {
			_lineThrough = value;
		}

		/**
		 * <p> TextLayoutFormat: Defines the formatting attributes used for links in normal state. This value will cascade down the hierarchy and apply to any links that are descendants. Accepts <code>inherit</code>, an <code>ITextLayoutFormat</code> or converts an array of objects with key and value as members to a TextLayoutFormat. </p>
		 * <p>Legal values include FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of null.</p>
		 * 
		 * @return 
		 */
		public function get linkActiveFormat():* {
			return _linkActiveFormat;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set linkActiveFormat(value:*):void {
			_linkActiveFormat = value;
		}

		/**
		 * <p> TextLayoutFormat: Defines the formatting attributes used for links in hover state, when the mouse is within the bounds (rolling over) a link. This value will cascade down the hierarchy and apply to any links that are descendants. Accepts <code>inherit</code>, an <code>ITextLayoutFormat</code> or converts an array of objects with key and value as members to a TextLayoutFormat. </p>
		 * <p>Legal values include FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of null.</p>
		 * 
		 * @return 
		 */
		public function get linkHoverFormat():* {
			return _linkHoverFormat;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set linkHoverFormat(value:*):void {
			_linkHoverFormat = value;
		}

		/**
		 * <p> TextLayoutFormat: Defines the formatting attributes used for links in normal state. This value will cascade down the hierarchy and apply to any links that are descendants. Accepts <code>inherit</code>, an <code>ITextLayoutFormat</code> or converts an array of objects with key and value as members to a TextLayoutFormat. </p>
		 * <p>Legal values include FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of null.</p>
		 * 
		 * @return 
		 */
		public function get linkNormalFormat():* {
			return _linkNormalFormat;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set linkNormalFormat(value:*):void {
			_linkNormalFormat = value;
		}

		/**
		 * <p> TextLayoutFormat: This specifies an auto indent for the start edge of lists when the padding value of the list on that side is <code>auto</code>. </p>
		 * <p>Legal values are numbers from -1000 to 1000 and FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of 40.</p>
		 * 
		 * @return 
		 */
		public function get listAutoPadding():* {
			return _listAutoPadding;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set listAutoPadding(value:*):void {
			_listAutoPadding = value;
		}

		/**
		 * <p> TextLayoutFormat: Defines the formatting attributes list markers. This value will cascade down the hierarchy and apply to any links that are descendants. Accepts <code>inherit</code>, an <code>IListMarkerFormat</code> or converts an array of objects with key and value as members to a ListMarkerFormat. </p>
		 * <p>Legal values include FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of null.</p>
		 * 
		 * @return 
		 */
		public function get listMarkerFormat():* {
			return _listMarkerFormat;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set listMarkerFormat(value:*):void {
			_listMarkerFormat = value;
		}

		/**
		 * <p> TextLayoutFormat: </p>
		 * <p>Legal values are ListStylePosition.INSIDE, ListStylePosition.OUTSIDE, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of ListStylePosition.OUTSIDE.</p>
		 * 
		 * @return 
		 */
		public function get listStylePosition():* {
			return _listStylePosition;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set listStylePosition(value:*):void {
			_listStylePosition = value;
		}

		/**
		 * <p> TextLayoutFormat: </p>
		 * <p>Legal values are ListStyleType.UPPER_ALPHA, ListStyleType.LOWER_ALPHA, ListStyleType.UPPER_ROMAN, ListStyleType.LOWER_ROMAN, ListStyleType.NONE, ListStyleType.DISC, ListStyleType.CIRCLE, ListStyleType.SQUARE, ListStyleType.BOX, ListStyleType.CHECK, ListStyleType.DIAMOND, ListStyleType.HYPHEN, ListStyleType.ARABIC_INDIC, ListStyleType.BENGALI, ListStyleType.DECIMAL, ListStyleType.DECIMAL_LEADING_ZERO, ListStyleType.DEVANAGARI, ListStyleType.GUJARATI, ListStyleType.GURMUKHI, ListStyleType.KANNADA, ListStyleType.PERSIAN, ListStyleType.THAI, ListStyleType.URDU, ListStyleType.CJK_EARTHLY_BRANCH, ListStyleType.CJK_HEAVENLY_STEM, ListStyleType.HANGUL, ListStyleType.HANGUL_CONSTANT, ListStyleType.HIRAGANA, ListStyleType.HIRAGANA_IROHA, ListStyleType.KATAKANA, ListStyleType.KATAKANA_IROHA, ListStyleType.LOWER_ALPHA, ListStyleType.LOWER_GREEK, ListStyleType.LOWER_LATIN, ListStyleType.UPPER_ALPHA, ListStyleType.UPPER_GREEK, ListStyleType.UPPER_LATIN, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of ListStyleType.DISC.</p>
		 * 
		 * @return 
		 */
		public function get listStyleType():* {
			return _listStyleType;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set listStyleType(value:*):void {
			_listStyleType = value;
		}

		/**
		 * <p> TextLayoutFormat: The locale of the text. Controls case transformations and shaping. Standard locale identifiers as described in Unicode Technical Standard #35 are used. For example en, en_US and en-US are all English, ja is Japanese. </p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of en.</p>
		 * 
		 * @return 
		 */
		public function get locale():* {
			return _locale;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set locale(value:*):void {
			_locale = value;
		}

		/**
		 * <p> TextLayoutFormat: Bottom inset in pixels. Default of auto is zero except in lists which get a start side padding of 45. (adopts default value if undefined during cascade). Space between the bottom edge of the container and the text. Value is a Number or auto. </p>
		 * <p> With horizontal text, in scrollable containers with multiple columns, the first and following columns will show the padding as blank space at the bottom of the container, but for the last column, if the text doesn't all fit, you may have to scroll in order to see the padding.</p>
		 * <p>Legal values as a string are FormatValue.AUTO, FormatValue.INHERIT and numbers from -8000 to 8000.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will have a value of FormatValue.AUTO.</p>
		 * 
		 * @return 
		 */
		public function get paddingBottom():* {
			return _paddingBottom;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set paddingBottom(value:*):void {
			_paddingBottom = value;
		}

		/**
		 * <p> TextLayoutFormat: Left inset in pixels. Default of auto is zero except in lists which get a start side padding of 45. (adopts default value if undefined during cascade). Space between the left edge of the container and the text. Value is a Number or auto.</p>
		 * <p> With vertical text, in scrollable containers with multiple columns, the first and following columns will show the padding as blank space at the end of the container, but for the last column, if the text doesn't all fit, you may have to scroll in order to see the padding.</p>
		 * <p>Legal values as a string are FormatValue.AUTO, FormatValue.INHERIT and numbers from -8000 to 8000.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will have a value of FormatValue.AUTO.</p>
		 * 
		 * @return 
		 */
		public function get paddingLeft():* {
			return _paddingLeft;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set paddingLeft(value:*):void {
			_paddingLeft = value;
		}

		/**
		 * <p> TextLayoutFormat: Right inset in pixels. Default of auto is zero except in lists which get a start side padding of 45. (adopts default value if undefined during cascade). Space between the right edge of the container and the text. Value is a Number or auto. </p>
		 * <p>Legal values as a string are FormatValue.AUTO, FormatValue.INHERIT and numbers from -8000 to 8000.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will have a value of FormatValue.AUTO.</p>
		 * 
		 * @return 
		 */
		public function get paddingRight():* {
			return _paddingRight;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set paddingRight(value:*):void {
			_paddingRight = value;
		}

		/**
		 * <p> TextLayoutFormat: Top inset in pixels. Default of auto is zero except in lists which get a start side padding of 45. (adopts default value if undefined during cascade). Space between the top edge of the container and the text. Value is a Number or auto. </p>
		 * <p>Legal values as a string are FormatValue.AUTO, FormatValue.INHERIT and numbers from -8000 to 8000.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will have a value of FormatValue.AUTO.</p>
		 * 
		 * @return 
		 */
		public function get paddingTop():* {
			return _paddingTop;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set paddingTop(value:*):void {
			_paddingTop = value;
		}

		/**
		 * <p> TextLayoutFormat: A Number that specifies, in pixels, the amount to indent the paragraph's end edge. Refers to the right edge in left-to-right text and the left edge in right-to-left text. </p>
		 * <p>Legal values are numbers from 0 to 8000 and FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of 0.</p>
		 * 
		 * @return 
		 */
		public function get paragraphEndIndent():* {
			return _paragraphEndIndent;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set paragraphEndIndent(value:*):void {
			_paragraphEndIndent = value;
		}

		/**
		 * <p> TextLayoutFormat: A Number that specifies the amount of space, in pixels, to leave after the paragraph. Collapses in tandem with <code>paragraphSpaceBefore</code>. </p>
		 * <p>Legal values are numbers from 0 to 8000 and FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of 0.</p>
		 * 
		 * @return 
		 */
		public function get paragraphSpaceAfter():* {
			return _paragraphSpaceAfter;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set paragraphSpaceAfter(value:*):void {
			_paragraphSpaceAfter = value;
		}

		/**
		 * <p> TextLayoutFormat: A Number that specifies the amount of space, in pixels, to leave before the paragraph. Collapses in tandem with <code>paragraphSpaceAfter</code>. </p>
		 * <p>Legal values are numbers from 0 to 8000 and FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of 0.</p>
		 * 
		 * @return 
		 */
		public function get paragraphSpaceBefore():* {
			return _paragraphSpaceBefore;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set paragraphSpaceBefore(value:*):void {
			_paragraphSpaceBefore = value;
		}

		/**
		 * <p> TextLayoutFormat: A Number that specifies, in pixels, the amount to indent the paragraph's start edge. Refers to the left edge in left-to-right text and the right edge in right-to-left text. </p>
		 * <p>Legal values are numbers from 0 to 8000 and FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of 0.</p>
		 * 
		 * @return 
		 */
		public function get paragraphStartIndent():* {
			return _paragraphStartIndent;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set paragraphStartIndent(value:*):void {
			_paragraphStartIndent = value;
		}

		/**
		 * <p> Returns the parent of this FlowElement object. Every FlowElement has at most one parent. </p>
		 * 
		 * @return 
		 */
		public function get parent():FlowGroupElement {
			return _parent;
		}

		/**
		 * <p> Returns the relative end of this FlowElement object in the parent. If the parent is null this is always equal to <code>textLength</code>. If the parent is not null, the value is the sum of the text lengths of this and all previous siblings, which is effectively the first character in the next FlowElement object. </p>
		 * 
		 * @return 
		 */
		public function get parentRelativeEnd():int {
			return _parentRelativeEnd;
		}

		/**
		 * <p> Returns the relative start of this FlowElement object in the parent. If parent is null, this value is always zero. If parent is not null, the value is the sum of the text lengths of all previous siblings. </p>
		 * 
		 * @return 
		 */
		public function get parentRelativeStart():int {
			return _parentRelativeStart;
		}

		/**
		 * <p> TextLayoutFormat: The rendering mode used for this text. Applies only to embedded fonts (<code>fontLookup</code> property is set to <code>FontLookup.EMBEDDED_CFF</code>). </p>
		 * <p>Legal values are RenderingMode.NORMAL, RenderingMode.CFF, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of RenderingMode.CFF.</p>
		 * 
		 * @return 
		 */
		public function get renderingMode():* {
			return _renderingMode;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set renderingMode(value:*):void {
			_renderingMode = value;
		}

		/**
		 * <p> TextLayoutFormat: Assigns an identifying class to the element, making it possible to set a style for the element by referencing the <code>styleName</code>. </p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will have a value of null.</p>
		 * 
		 * @return 
		 */
		public function get styleName():* {
			return _styleName;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set styleName(value:*):void {
			_styleName = value;
		}

		/**
		 * <p> Returns the styles on this FlowElement. Note that the getter makes a copy of the styles dictionary. The returned object encapsulates all styles set in the format property including core and user styles. The returned object consists of an array of <i>stylename-value</i> pairs. </p>
		 * 
		 * @return 
		 */
		public function get styles():Object {
			return _styles;
		}

		/**
		 * <p> TextLayoutFormat: Specifies the tab stops associated with the paragraph. Setters can take an array of TabStopFormat, a condensed string representation, undefined, or <code>FormatValue.INHERIT</code>. The condensed string representation is always converted into an array of TabStopFormat. </p>
		 * <p>The string-based format is a list of tab stops, where each tab stop is delimited by one or more spaces.</p>
		 * <p>A tab stop takes the following form: &lt;alignment type&gt;&lt;alignment position&gt;|&lt;alignment token&gt;.</p>
		 * <p>The alignment type is a single character, and can be S, E, C, or D (or lower-case equivalents). S or s for start, E or e for end, C or c for center, D or d for decimal. The alignment type is optional, and if its not specified will default to S.</p>
		 * <p>The alignment position is a Number, and is specified according to FXG spec for Numbers (decimal or scientific notation). The alignment position is required.</p>
		 * <p>The vertical bar is used to separate the alignment position from the alignment token, and should only be present if the alignment token is present.</p>
		 * <p> The alignment token is optional if the alignment type is D, and should not be present if the alignment type is anything other than D. The alignment token may be any sequence of characters terminated by the space that ends the tab stop (for the last tab stop, the terminating space is optional; end of alignment token is implied). A space may be part of the alignment token if it is escaped with a backslash (\ ). A backslash may be part of the alignment token if it is escaped with another backslash (\\). If the alignment type is D, and the alignment token is not specified, it will take on the default value of null.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of null.</p>
		 * 
		 * @return 
		 */
		public function get tabStops():* {
			return _tabStops;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set tabStops(value:*):void {
			_tabStops = value;
		}

		/**
		 * <p> TextLayoutFormat: Alignment of lines in the paragraph relative to the container. <code>TextAlign.LEFT</code> aligns lines along the left edge of the container. <code>TextAlign.RIGHT</code> aligns on the right edge. <code>TextAlign.CENTER</code> positions the line equidistant from the left and right edges. <code>TextAlign.JUSTIFY</code> spreads the lines out so they fill the space. <code>TextAlign.START</code> is equivalent to setting left in left-to-right text, or right in right-to-left text. <code>TextAlign.END</code> is equivalent to setting right in left-to-right text, or left in right-to-left text. </p>
		 * <p>Legal values are TextAlign.LEFT, TextAlign.RIGHT, TextAlign.CENTER, TextAlign.JUSTIFY, TextAlign.START, TextAlign.END, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of TextAlign.START.</p>
		 * 
		 * @return 
		 */
		public function get textAlign():* {
			return _textAlign;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set textAlign(value:*):void {
			_textAlign = value;
		}

		/**
		 * <p> TextLayoutFormat: Alignment of the last (or only) line in the paragraph relative to the container in justified text. If <code>textAlign</code> is set to <code>TextAlign.JUSTIFY</code>, <code>textAlignLast</code> specifies how the last line (or only line, if this is a one line block) is aligned. Values are similar to <code>textAlign</code>. </p>
		 * <p>Legal values are TextAlign.LEFT, TextAlign.RIGHT, TextAlign.CENTER, TextAlign.JUSTIFY, TextAlign.START, TextAlign.END, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of TextAlign.START.</p>
		 * 
		 * @return 
		 */
		public function get textAlignLast():* {
			return _textAlignLast;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set textAlignLast(value:*):void {
			_textAlignLast = value;
		}

		/**
		 * <p> TextLayoutFormat: Alpha (transparency) value for the text. A value of 0 is fully transparent, and a value of 1 is fully opaque. Display objects with <code>textAlpha</code> set to 0 are active, even though they are invisible. </p>
		 * <p>Legal values are numbers from 0 to 1 and FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of 1.</p>
		 * 
		 * @return 
		 */
		public function get textAlpha():* {
			return _textAlpha;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set textAlpha(value:*):void {
			_textAlpha = value;
		}

		/**
		 * <p> TextLayoutFormat: Decoration on text. Use to apply underlining; default is none. </p>
		 * <p>Legal values are TextDecoration.NONE, TextDecoration.UNDERLINE, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of TextDecoration.NONE.</p>
		 * 
		 * @return 
		 */
		public function get textDecoration():* {
			return _textDecoration;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set textDecoration(value:*):void {
			_textDecoration = value;
		}

		/**
		 * <p> TextLayoutFormat: A Number that specifies, in pixels, the amount to indent the first line of the paragraph. A negative indent will push the line into the margin, and possibly out of the container. </p>
		 * <p>Legal values are numbers from -8000 to 8000 and FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of 0.</p>
		 * 
		 * @return 
		 */
		public function get textIndent():* {
			return _textIndent;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set textIndent(value:*):void {
			_textIndent = value;
		}

		/**
		 * <p> TextLayoutFormat: Specifies options for justifying text. Default value is <code>TextJustify.INTER_WORD</code>, meaning that extra space is added to the space characters. <code>TextJustify.DISTRIBUTE</code> adds extra space to space characters and between individual letters. Used only in conjunction with a <code>justificationRule</code> value of <code>JustificationRule.SPACE</code>. </p>
		 * <p>Legal values are TextJustify.INTER_WORD, TextJustify.DISTRIBUTE, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of TextJustify.INTER_WORD.</p>
		 * 
		 * @return 
		 */
		public function get textJustify():* {
			return _textJustify;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set textJustify(value:*):void {
			_textJustify = value;
		}

		/**
		 * <p> Returns the total length of text owned by this FlowElement object and its children. If an element has no text, the value of <code>textLength</code> is usually zero. </p>
		 * <p>ParagraphElement objects have a final span with a paragraph terminator character for the last SpanElement object.The paragraph terminator is included in the value of the <code>textLength</code> of that SpanElement object and all its parents. It is not included in <code>text</code> property of the SpanElement object.</p>
		 * 
		 * @return 
		 */
		public function get textLength():int {
			return _textLength;
		}

		/**
		 * <p> TextLayoutFormat: Determines the number of degrees to rotate this text. </p>
		 * <p>Legal values are TextRotation.ROTATE_0, TextRotation.ROTATE_180, TextRotation.ROTATE_270, TextRotation.ROTATE_90, TextRotation.AUTO, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of TextRotation.AUTO.</p>
		 * 
		 * @return 
		 */
		public function get textRotation():* {
			return _textRotation;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set textRotation(value:*):void {
			_textRotation = value;
		}

		/**
		 * <p> Sets the tracking and is synonymous with the <code>trackingRight</code> property. Specified as a number of pixels or a percent of <code>fontSize</code>. </p>
		 * 
		 * @return 
		 */
		public function get tracking():Object {
			return _tracking;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set tracking(value:Object):void {
			_tracking = value;
		}

		/**
		 * <p> TextLayoutFormat: Number in pixels (or percent of <code>fontSize</code>, like 120%) indicating the amount of tracking (manual kerning) to be applied to the left of each character. If kerning is enabled, the <code>trackingLeft</code> value is added to the values in the kerning table for the font. If kerning is disabled, the <code>trackingLeft</code> value is used as a manual kerning value. Supports both positive and negative values. </p>
		 * <p>Legal values as a number are from -1000 to 1000.</p>
		 * <p>Legal values as a percent are numbers from -1000% to 1000%.</p>
		 * <p>Legal values include FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of 0.</p>
		 * 
		 * @return 
		 */
		public function get trackingLeft():* {
			return _trackingLeft;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set trackingLeft(value:*):void {
			_trackingLeft = value;
		}

		/**
		 * <p> TextLayoutFormat: Number in pixels (or percent of <code>fontSize</code>, like 120%) indicating the amount of tracking (manual kerning) to be applied to the right of each character. If kerning is enabled, the <code>trackingRight</code> value is added to the values in the kerning table for the font. If kerning is disabled, the <code>trackingRight</code> value is used as a manual kerning value. Supports both positive and negative values. </p>
		 * <p>Legal values as a number are from -1000 to 1000.</p>
		 * <p>Legal values as a percent are numbers from -1000% to 1000%.</p>
		 * <p>Legal values include FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of 0.</p>
		 * 
		 * @return 
		 */
		public function get trackingRight():* {
			return _trackingRight;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set trackingRight(value:*):void {
			_trackingRight = value;
		}

		/**
		 * <p> Each FlowElement has a <code>typeName</code>. <code>typeName</code> defaults to the string the <code>textLayoutFormat</code> TextConverter uses. This API can be used to set a different <code>typeName</code> to a <code>FlowElement</code>. Typically this is done to support <code>type</code> selectors in CSS. </p>
		 * <p>See the <code>TEXT_FIELD_HTML_FORMAT</code> documentation for how this used..</p>
		 * 
		 * @return 
		 */
		public function get typeName():String {
			return _typeName;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set typeName(value:String):void {
			_typeName = value;
		}

		/**
		 * <p> TextLayoutFormat: The type of typographic case used for this text. Here are some examples:</p>
		 * <p><img src="../../../images/textLayout_typographiccase.png" alt="typographicCase"></p>
		 * <p>Legal values are TLFTypographicCase.DEFAULT, TLFTypographicCase.CAPS_TO_SMALL_CAPS, TLFTypographicCase.UPPERCASE, TLFTypographicCase.LOWERCASE, TLFTypographicCase.LOWERCASE_TO_SMALL_CAPS, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of TLFTypographicCase.DEFAULT.</p>
		 * 
		 * @return 
		 */
		public function get typographicCase():* {
			return _typographicCase;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set typographicCase(value:*):void {
			_typographicCase = value;
		}

		/**
		 * <p> Allows you to read and write user styles on a FlowElement object. Note that reading this property makes a copy of the userStyles set in the format of this element. </p>
		 * 
		 * @return 
		 */
		public function get userStyles():Object {
			return _userStyles;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set userStyles(value:Object):void {
			_userStyles = value;
		}

		/**
		 * <p> TextLayoutFormat: Vertical alignment or justification (adopts default value if undefined during cascade). Determines how TextFlow elements align within the container. </p>
		 * <p>Legal values are VerticalAlign.TOP, VerticalAlign.MIDDLE, VerticalAlign.BOTTOM, VerticalAlign.JUSTIFY, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will have a value of VerticalAlign.TOP.</p>
		 * 
		 * @return 
		 */
		public function get verticalAlign():* {
			return _verticalAlign;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set verticalAlign(value:*):void {
			_verticalAlign = value;
		}

		/**
		 * <p> TextLayoutFormat: Collapses or preserves whitespace when importing text into a TextFlow. <code>WhiteSpaceCollapse.PRESERVE</code> retains all whitespace characters. <code>WhiteSpaceCollapse.COLLAPSE</code> removes newlines, tabs, and leading or trailing spaces within a block of imported text. Line break tags () and Unicode line separator characters are retained. </p>
		 * <p>Legal values are WhiteSpaceCollapse.PRESERVE, WhiteSpaceCollapse.COLLAPSE, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of WhiteSpaceCollapse.COLLAPSE.</p>
		 * 
		 * @return 
		 */
		public function get whiteSpaceCollapse():* {
			return _whiteSpaceCollapse;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set whiteSpaceCollapse(value:*):void {
			_whiteSpaceCollapse = value;
		}

		/**
		 * <p> TextLayoutFormat: Specifies the optimum, minimum, and maximum spacing (as a multiplier of the width of a normal space) between words to use during justification. The optimum space is used to indicate the desired size of a space, as a fraction of the value defined in the font. The minimum and maximum values are the used when textJustify is distribute to determine how wide or narrow the spaces between the words may grow before letter spacing is used to justify the line. </p>
		 * <p>Legal values as a percent are numbers from -1000% to 1000%.</p>
		 * <p>Legal values include FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of 100%, 50%, 150%.</p>
		 * 
		 * @return 
		 */
		public function get wordSpacing():* {
			return _wordSpacing;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set wordSpacing(value:*):void {
			_wordSpacing = value;
		}

		/**
		 * <p> Clears the style specified by the <code>styleProp</code> parameter from this FlowElement object. Sets the value to <code>undefined</code>. </p>
		 * 
		 * @param styleProp  — The name of the style to clear. 
		 */
		public function clearStyle(styleProp:String):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Makes a deep copy of this FlowElement object, including any children, copying the content between the two specified character positions and returning the copy as a FlowElement object. </p>
		 * <p>With no arguments, <code>deepCopy()</code> defaults to copying the entire element.</p>
		 * 
		 * @param relativeStart  — relative text position of the first character to copy. First position is 0. 
		 * @param relativeEnd  — relative text position of the last character to copy. A value of -1 indicates copy to end. 
		 * @return  — the object created by the deep copy operation. 
		 */
		public function deepCopy(relativeStart:int = 0, relativeEnd:int = -1):FlowElement {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Compare the userStyles of this with otherElement's userStyles. </p>
		 * 
		 * @param otherElement  — the FlowElement object with which to compare user styles 
		 * @return  — true if the user styles are equal; false otherwise. 
		 */
		public function equalUserStyles(otherElement:FlowElement):Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the start location of the element in the text flow as an absolute index. The first character in the flow is position 0. </p>
		 * 
		 * @return  — The index of the start of the element from the start of the TextFlow object. 
		 */
		public function getAbsoluteStart():int {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the character at the specified position, relative to this FlowElement object. The first character is at relative position 0. </p>
		 * 
		 * @param relativePosition  — The relative position of the character in this FlowElement object. 
		 * @return  — String containing the character. 
		 */
		public function getCharAtPosition(relativePosition:int):String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the character code at the specified position, relative to this FlowElement. The first character is at relative position 0. </p>
		 * 
		 * @param relativePosition  — The relative position of the character in this FlowElement object. 
		 * @return  — the Unicode value for the character at the specified position. 
		 */
		public function getCharCodeAtPosition(relativePosition:int):int {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the start of this element relative to an ancestor element. Assumes that the ancestor element is in the parent chain. If the ancestor element is the parent, this is the same as <code>this.parentRelativeStart</code>. If the ancestor element is the grandparent, this is the same as <code>parentRelativeStart</code> plus <code>parent.parentRelativeStart</code> and so on. </p>
		 * 
		 * @param ancestorElement  — The element from which you want to find the relative start of this element. 
		 * @return  — the offset of this element. 
		 */
		public function getElementRelativeStart(ancestorElement:FlowElement):int {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the next FlowElement sibling in the text flow hierarchy. </p>
		 * 
		 * @return  — the next FlowElement object of the same type, or null if there is no sibling. 
		 */
		public function getNextSibling():FlowElement {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the ParagraphElement object associated with this element. It looks up the text flow hierarchy and returns the first ParagraphElement object. </p>
		 * 
		 * @return  — the ParagraphElement object that's associated with this FlowElement object. 
		 */
		public function getParagraph():ParagraphElement {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the previous FlowElement sibling in the text flow hierarchy. </p>
		 * 
		 * @return  — the previous FlowElement object of the same type, or null if there is no previous sibling. 
		 */
		public function getPreviousSibling():FlowElement {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the value of the style specified by the <code>styleProp</code> parameter, which specifies the style name and can include any user style name. Accesses an existing span, paragraph, text flow, or container style. Searches the parent tree if the style's value is <code>undefined</code> on a particular element. </p>
		 * 
		 * @param styleProp  — The name of the style whose value is to be retrieved. 
		 * @return  — The value of the specified style. The type varies depending on the type of the style being accessed. Returns  if the style is not set. 
		 */
		public function getStyle(styleProp:String):* {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Gets the specified range of text from a flow element. </p>
		 * 
		 * @param relativeStart  — The starting position of the range of text to be retrieved, relative to the start of the FlowElement 
		 * @param relativeEnd  — The ending position of the range of text to be retrieved, relative to the start of the FlowElement, -1 for up to the end of the element 
		 * @param paragraphSeparator  — character to put between paragraphs 
		 * @return  — The requested text. 
		 */
		public function getText(relativeStart:int = 0, relativeEnd:int = -1, paragraphSeparator:String = ""):String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Climbs the text flow hierarchy to return the root TextFlow object for the element. </p>
		 * 
		 * @return  — The root TextFlow object for this FlowElement object. 
		 */
		public function getTextFlow():TextFlow {
			throw new Error("Not implemented");
		}

		/**
		 * @param document  — The MXML document that created the object. 
		 * @param id  — The identifier used by document to refer to this object. 
		 */
		public function initialized(document:Object, id:String):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets the style specified by the <code>styleProp</code> parameter to the value specified by the <code>newValue</code> parameter. You can set a span, paragraph, text flow, or container style, including any user name-value pair. </p>
		 * <p><b>Note:</b> If you assign a custom style, Text Layout Framework can import and export it but compiled MXML cannot support it.</p>
		 * 
		 * @param styleProp  — The name of the style to set. 
		 * @param newValue  — The value to which to set the style. 
		 */
		public function setStyle(styleProp:String, newValue:*):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Makes a copy of this FlowElement object, copying the content between two specified character positions. It returns the copy as a new FlowElement object. Unlike <code>deepCopy()</code>, <code>shallowCopy()</code> does not copy any of the children of this FlowElement object. </p>
		 * <p>With no arguments, <code>shallowCopy()</code> defaults to copying all of the content.</p>
		 * 
		 * @param relativeStart  — The relative text position of the first character to copy. First position is 0. 
		 * @param relativeEnd  — The relative text position of the last character to copy. A value of -1 indicates copy to end. 
		 * @return  — the object created by the copy operation. 
		 */
		public function shallowCopy(relativeStart:int = 0, relativeEnd:int = -1):FlowElement {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Splits this FlowElement object at the position specified by the <code>relativePosition</code> parameter, which is a relative position in the text for this element. This method splits only SpanElement and FlowGroupElement objects. </p>
		 * 
		 * @param relativePosition  — the position at which to split the content of the original object, with the first position being 0. 
		 * @return  — the new object, which contains the content of the original object, starting at the specified position. 
		 */
		public function splitAtPosition(relativePosition:int):FlowElement {
			throw new Error("Not implemented");
		}
	}
}
