package flashx.textLayout.elements {
	/**
	 *  The ParagraphFormattedElement class is an abstract base class for FlowElement classes that have paragraph properties. <p>You cannot create a ParagraphFormattedElement object directly. Invoking <code>new ParagraphFormattedElement()</code> throws an error exception.</p> <p><span class="classHeaderTableLabel">Default MXML Property</span><code>mxmlChildren</code></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="ContainerFormattedElement.html" target="">ContainerFormattedElement</a>
	 *  <br>
	 *  <a href="ParagraphElement.html" target="">ParagraphElement</a>
	 * </div><br><hr>
	 */
	public class ParagraphFormattedElement extends FlowGroupElement {
	}
}
