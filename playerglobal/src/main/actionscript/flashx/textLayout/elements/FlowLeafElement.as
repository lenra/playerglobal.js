package flashx.textLayout.elements {
	import flash.text.engine.FontMetrics;

	import flashx.textLayout.formats.ITextLayoutFormat;

	/**
	 *  Base class for FlowElements that appear at the lowest level of the flow hierarchy. FlowLeafElement objects have no children and include InlineGraphicElement objects and SpanElement objects. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="InlineGraphicElement.html" target="">InlineGraphicElement</a>
	 *  <br>
	 *  <a href="SpanElement.html" target="">SpanElement</a>
	 * </div><br><hr>
	 */
	public class FlowLeafElement extends FlowElement {
		private var _computedFormat:ITextLayoutFormat;
		private var _text:String;

		/**
		 * <p> Base class - invoking new FlowLeafElement() throws an error exception. </p>
		 */
		public function FlowLeafElement() {
			throw new Error("Not implemented");
		}

		/**
		 * <p> The computed text format attributes that are in effect for this element. Takes into account the inheritance of attributes. </p>
		 * 
		 * @return 
		 */
		override public function get computedFormat():ITextLayoutFormat {
			return _computedFormat;
		}

		/**
		 * <p> The text associated with the FlowLeafElement: </p>
		 * <ul>
		 *  <li>The value for SpanElement subclass will be one character less than <code>textLength</code> if this is the last span in a ParagraphELement.</li>
		 *  <li>The value for BreakElement subclass is a U+2028</li>
		 *  <li>The value for TabElement subclass is a tab</li>
		 *  <li>The value for InlineGraphicElement subclass is U+FDEF</li>
		 * </ul>
		 * 
		 * @return 
		 */
		public function get text():String {
			return _text;
		}

		/**
		 * <p> Returns the FontMetrics object for the span. The properties of the FontMetrics object describe the emBox, strikethrough position, strikethrough thickness, underline position, and underline thickness for the specified font. </p>
		 * 
		 * @return  — font metrics associated with the span 
		 */
		public function getComputedFontMetrics():FontMetrics {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the next FlowLeafElement object. </p>
		 * 
		 * @param limitElement  — Specifies FlowGroupElement on whose last leaf to stop looking. A value of null (default) means search till no more elements. 
		 * @return  — next FlowLeafElement or null if at the end 
		 */
		public function getNextLeaf(limitElement:FlowGroupElement = null):FlowLeafElement {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the previous FlowLeafElement object. </p>
		 * 
		 * @param limitElement  — Specifies the FlowGroupElement on whose first leaf to stop looking. null (default) means search till no more elements. 
		 * @return  — previous leafElement or null if at the end 
		 */
		public function getPreviousLeaf(limitElement:FlowGroupElement = null):FlowLeafElement {
			throw new Error("Not implemented");
		}
	}
}
