package flashx.textLayout.elements {
	/**
	 *  The List class is used for grouping together items into a numbered or unnumbered list. A ListElement's children may be of type ListItemElement, ListElement, ParagraphElement, or DivElement. <p>Each ListElement creates a scope with an implicit counter 'ordered'.</p> <p><span class="classHeaderTableLabel">Default MXML Property</span><code>mxmlChildren</code></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS19f279b149e7481c7fdc883d12de39353f1-8000.html" target="_blank">Using numbered and bulleted lists with TLF-based text controls</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS19f279b149e7481c6944fe6612e01ed0f26-8000.html" target="_blank">Using numbered and bulleted lists</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flashx/textLayout/formats/ITextLayoutFormat.html#listStyleType" target="">flashx.textLayout.formats.ITextLayoutFormat.listStyleType</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/formats/ITextLayoutFormat.html#listStylePosition" target="">flashx.textLayout.formats.ITextLayoutFormat.listStylePosition</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/formats/ListMarkerFormat.html" target="">flashx.textLayout.formats.ListMarkerFormat</a>
	 * </div><br><hr>
	 */
	public class ListElement extends ContainerFormattedElement {
	}
}
