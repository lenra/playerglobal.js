package flashx.textLayout.elements {
	/**
	 *  The SubParagraphGroupElement is a grouping element for FlowLeafElements and other classes that extend SubParagraphGroupElementBase. <p><span class="classHeaderTableLabel">Default MXML Property</span><code>mxmlChildren</code></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="SubParagraphGroupElement.html" target="">flashx.textLayout.elements.SubParagraphGroupElement</a>
	 *  <br>
	 *  <a href="SubParagraphGroupElementBase.html" target="">flashx.textLayout.elements.SubParagraphGroupElementBase</a>
	 *  <br>
	 *  <a href="FlowLeafElement.html" target="">flashx.textLayout.elements.FlowLeafElement</a>
	 * </div><br><hr>
	 */
	public class SubParagraphGroupElement extends SubParagraphGroupElementBase {

		/**
		 * <p> Constructor. For information on using this class, see <a href="http://blogs.adobe.com/tlf/2011/01/tlf-2-0-changes-subparagraphgroupelements-and-typename-applied-to-textfieldhtmlimporter-and-cssformatresolver.html">TLF 2.0 SubParagraphGroupElement and typeName</a>. </p>
		 */
		public function SubParagraphGroupElement() {
			throw new Error("Not implemented");
		}
	}
}
