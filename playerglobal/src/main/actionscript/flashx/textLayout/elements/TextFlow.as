package flashx.textLayout.elements {
	import flash.events.Event;
	import flash.events.IEventDispatcher;

	import flashx.textLayout.compose.IFlowComposer;
	import flashx.textLayout.edit.ISelectionManager;
	import flashx.textLayout.events.CompositionCompleteEvent;
	import flashx.textLayout.events.DamageEvent;
	import flashx.textLayout.events.FlowElementMouseEvent;
	import flashx.textLayout.events.FlowOperationEvent;
	import flashx.textLayout.events.SelectionEvent;
	import flashx.textLayout.events.StatusChangeEvent;
	import flashx.textLayout.events.TextLayoutEvent;
	import flashx.textLayout.events.UpdateCompleteEvent;
	import flashx.textLayout.formats.ITextLayoutFormat;

	[Event(name="click", type="flashx.textLayout.events.FlowElementMouseEvent")]
	[Event(name="compositionComplete", type="flashx.textLayout.events.CompositionCompleteEvent")]
	[Event(name="damage", type="flashx.textLayout.events.DamageEvent")]
	[Event(name="flowOperationBegin", type="flashx.textLayout.events.FlowOperationEvent")]
	[Event(name="flowOperationComplete", type="flashx.textLayout.events.FlowOperationEvent")]
	[Event(name="flowOperationEnd", type="flashx.textLayout.events.FlowOperationEvent")]
	[Event(name="inlineGraphicStatusChanged", type="flashx.textLayout.events.StatusChangeEvent")]
	[Event(name="mouseDown", type="flashx.textLayout.events.FlowElementMouseEvent")]
	[Event(name="mouseMove", type="flashx.textLayout.events.FlowElementMouseEvent")]
	[Event(name="mouseUp", type="flashx.textLayout.events.FlowElementMouseEvent")]
	[Event(name="rollOut", type="flashx.textLayout.events.FlowElementMouseEvent")]
	[Event(name="rollOver", type="flashx.textLayout.events.FlowElementMouseEvent")]
	[Event(name="scroll", type="flashx.textLayout.events.TextLayoutEvent")]
	[Event(name="selectionChange", type="flashx.textLayout.events.SelectionEvent")]
	[Event(name="updateComplete", type="flashx.textLayout.events.UpdateCompleteEvent")]
	/**
	 *  The TextFlow class is responsible for managing all the text content of a story. In TextLayout, text is stored in a hierarchical tree of elements. TextFlow is the root object of the element tree. All elements on the tree derive from the base class, FlowElement. <p>A TextFlow object can have ParagraphElement and DivElement objects as children. A div (DivElement object) represents a group of paragraphs (ParagraphElement objects). A paragraph can have SpanElement, InlineGraphicElement, LinkElement, and TCYElement objects as children.</p> <p>A span (SpanElement) is a range of text in a paragraph that has the same attributes. An image (InlineGraphicElement) represents an arbitrary graphic that appears as a single character in a line of text. A LinkElement represents a hyperlink, or HTML <code>a</code> tag, and it can contain multiple spans. A TCYElement object is used in Japanese text when there is a small run of text that appears perpendicular to the line, as in a horizontal run within a vertical line. A TCYElement also can contain multiple spans.</p> <p>TextFlow also derives from the ContainerFormattedElement class, which is the root class for all container-level block elements.</p> <p>The following illustration shows the relationship of other elements, such as spans and paragraphs, to the TextFlow object.</p> <p> <img src="../../../images/textLayout_textFlowHierarchy.gif" alt="example TextFlow hierarchy"> </p> <p>Each TextFlow object has a corresponding Configuration object that allows you to specify initial character and paragraph formats and the initial container format. It also allows you to specify attributes for selection, links, focus, and scrolling. When you supply a Configuration object as parameter to the <code>TextFlow()</code> constructor, it creates a read-only snapshot that you can access through the <code>TextFlow.configuration</code> property. After creation, you can't change the TextFlow's configuration. If you do not specify a Configuration, you can access the default configuration through the <code>TextFlow.defaultConfiguration</code> property.</p> <p><span class="classHeaderTableLabel">Default MXML Property</span><code>mxmlChildren</code></p><p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS02f7d8d4857b1677-165a04e1126951a2d98-7ffc.html" target="_blank">Creating TextFlow objects</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="TextFlow.html#configuration" target="">configuration</a>
	 *  <br>
	 *  <a href="IConfiguration.html" target="">IConfiguration</a>
	 *  <br>
	 *  <a href="DivElement.html" target="">DivElement</a>
	 *  <br>
	 *  <a href="FlowElement.html" target="">FlowElement</a>
	 *  <br>
	 *  <a href="FlowGroupElement.html" target="">FlowGroupElement</a>
	 *  <br>
	 *  <a href="FlowLeafElement.html" target="">FlowLeafElement</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/compose/IFlowComposer.html" target="">IFlowComposer</a>
	 *  <br>
	 *  <a href="ParagraphElement.html" target="">ParagraphElement</a>
	 *  <br>
	 *  <a href="SpanElement.html" target="">SpanElement</a>
	 * </div><br><hr>
	 */
	public class TextFlow extends ContainerFormattedElement implements IEventDispatcher {
		private static var _defaultConfiguration:Configuration;

		private var _flowComposer:IFlowComposer;
		private var _formatResolver:IFormatResolver;
		private var _hostFormat:ITextLayoutFormat;
		private var _interactionManager:ISelectionManager;

		private var _configuration:IConfiguration;
		private var _generation:uint;

		/**
		 * <p> Constructor - creates a new TextFlow instance. </p>
		 * <p>If you provide a <code>config</code> parameter, the contents of the Configuration object are copied and you cannot make changes. You can access configuration settings, however, through the <code>configuration</code> property. If the <code>config</code> parameter is null, you can access the default configuration settings through the <code>defaultConfiguration</code> property.</p>
		 * <p>The Configuration object provides a mechanism for setting configurable default attributes on a TextFlow. While you can't make changes to the Configuration object, you can override default attributes, if necessary, by setting the attributes of TextFlow and its children.</p>
		 * 
		 * @param config  — Specifies the configuration to use for this TextFlow object. If it's null, use <code>TextFlow.defaultConfiguration</code> to access configuration values. 
		 */
		public function TextFlow(config:IConfiguration = null) {
			throw new Error("Not implemented");
		}

		/**
		 * <p> The Configuration object for this TextFlow object. The Configuration object specifies the initial character and paragraph formats, the initial container format, and attributes for selection highlighting, links, focus, and scrolling. </p>
		 * <p>If you do not specify a Configuration object, Text Layout Framework uses a default Configuration object, which is referenced by the <code>defaultConfiguration</code> property.</p>
		 * 
		 * @return 
		 */
		public function get configuration():IConfiguration {
			return _configuration;
		}

		/**
		 * <p> Manages the containers for this element. </p>
		 * <p>The TextLines that are created from the element appear as children of the container. The flowComposer manages the containers, and as the text is edited it adds lines to and removes lines from the containers. The flowComposer also keeps track of some critical attributes, such as the width and height to compose to, whether scrolling is on, and so on.</p>
		 * <p>The container and <code>flowComposer</code> are closely related. If you reset <code>flowComposer</code>, the container is reset to the new flowComposer's container. Likewise if the container is reset, <code>flowComposer</code> is reset to the container's new flowComposer.</p>
		 * 
		 * @return 
		 */
		public function get flowComposer():IFlowComposer {
			return _flowComposer;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set flowComposer(value:IFlowComposer):void {
			_flowComposer = value;
		}

		/**
		 * <p> A callback function for resolving element styles. You can use this to provide styling using CSS or named styles, for example. </p>
		 * 
		 * @return 
		 */
		public function get formatResolver():IFormatResolver {
			return _formatResolver;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set formatResolver(value:IFormatResolver):void {
			_formatResolver = value;
		}

		/**
		 * <p> The generation number for this TextFlow object. The undo and redo operations use the generation number to validate that it's legal to undo or redo an operation. The generation numbers must match. </p>
		 * <p>Each model change increments <code>generation</code> so if the generation number changes, you know the TextFlow model has changed.</p>
		 * 
		 * @return 
		 */
		public function get generation():uint {
			return _generation;
		}

		/**
		 * <p> The TextLayoutFormat object for this TextFlow object. This enables several optimizations for reusing host formats. For example; </p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *         textFlowA.hostFormat = textFlowB.hostFormat
		 *         </pre>
		 * </div>
		 * <code>hostFormat</code>
		 * <i>not</i>
		 * <i>after</i>
		 * <code>hostFormat</code>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *         format = new TextLayoutFormat()
		 *         textFlow.hostFormat = format
		 *         format.fontSize = 24;
		 *         </pre>
		 * </div>
		 * 
		 * @return 
		 */
		public function get hostFormat():ITextLayoutFormat {
			return _hostFormat;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set hostFormat(value:ITextLayoutFormat):void {
			_hostFormat = value;
		}

		/**
		 * <p> The InteractionManager associated with this TextFlow object. </p>
		 * <p>Controls all selection and editing on the text. If the TextFlow is not selectable, the interactionManager is null. To make the TextFlow editable, assign a interactionManager that is both an ISelectionManager and an IEditManager. To make a TextFlow that is read-only and allows selection, assign a interactionManager that is an ISelectionManager only. </p>
		 * 
		 * @return 
		 */
		public function get interactionManager():ISelectionManager {
			return _interactionManager;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set interactionManager(value:ISelectionManager):void {
			_interactionManager = value;
		}

		/**
		 * <p> Registers an event listener object with an EventDispatcher object so that the listener receives notification of an event. You can register event listeners on all nodes in the display list for a specific type of event, phase, and priority. </p>
		 * <p>After you successfully register an event listener, you cannot change its priority through additional calls to <code>addEventListener()</code>. To change a listener's priority, you must first call <code>removeEventListener()</code>. Then you can register the listener again with the new priority level.</p>
		 * <p>After the listener is registered, subsequent calls to <code>addEventListener()</code> with a different value for either <code>type</code> or <code>useCapture</code> result in the creation of a separate listener registration. For example, if you first register a listener with <code>useCapture</code> set to <code>true</code>, it listens only during the capture phase. If you call <code>addEventListener()</code> again using the same listener object, but with <code>useCapture</code> set to <code>false</code>, you have two separate listeners: one that listens during the capture phase, and another that listens during the target and bubbling phases.</p>
		 * <p>You cannot register an event listener for only the target phase or the bubbling phase. Those phases are coupled during registration because bubbling applies only to the ancestors of the target node.</p>
		 * <p>When you no longer need an event listener, remove it by calling <code>EventDispatcher.removeEventListener()</code>; otherwise, memory problems might result. Objects with registered event listeners are not automatically removed from memory because the garbage collector does not remove objects that still have references.</p>
		 * <p>Copying an EventDispatcher instance does not copy the event listeners attached to it. (If your newly created node needs an event listener, you must attach the listener after creating the node.) However, if you move an EventDispatcher instance, the event listeners attached to it move along with it.</p>
		 * <p>If the event listener is being registered on a node while an event is also being processed on this node, the event listener is not triggered during the current phase but may be triggered during a later phase in the event flow, such as the bubbling phase.</p>
		 * <p>If an event listener is removed from a node while an event is being processed on the node, it is still triggered by the current actions. After it is removed, the event listener is never invoked again (unless it is registered again for future processing). </p>
		 * 
		 * @param type  — The type of event. 
		 * @param listener  — The listener function that processes the event. This function must accept an event object as its only parameter and must return nothing, as this example shows: <p><code>function(evt:Event):void</code></p> The function can have any name. 
		 * @param useCapture  — Determines whether the listener works in the capture phase or the target and bubbling phases. If <code>useCapture</code> is set to <code>true</code>, the listener processes the event only during the capture phase and not in the target or bubbling phase. If <code>useCapture</code> is <code>false</code>, the listener processes the event only during the target or bubbling phase. To listen for the event in all three phases, call <code>addEventListener()</code> twice, once with <code>useCapture</code> set to <code>true</code>, then again with <code>useCapture</code> set to <code>false</code>. 
		 * @param priority  — The priority level of the event listener. Priorities are designated by a 32-bit integer. The higher the number, the higher the priority. All listeners with priority <i>n</i> are processed before listeners of priority <i>n-1</i>. If two or more listeners share the same priority, they are processed in the order in which they were added. The default priority is 0. 
		 * @param useWeakReference  — Determines whether the reference to the listener is strong or weak. A strong reference (the default) prevents your listener from being garbage-collected. A weak reference does not. <p>Class-level member functions are not subject to garbage collection, so you can set <code>useWeakReference</code> to <code>true</code> for class-level member functions without subjecting them to garbage collection. If you set <code>useWeakReference</code> to <code>true</code> for a listener that is a nested inner function, the function will be garbge-collected and no longer persistent. If you create references to the inner function (save it in another variable) then it is not garbage-collected and stays persistent.</p> 
		 */
		public function addEventListener(type:String, listener:Function, useCapture:Boolean = false, priority:int = 0, useWeakReference:Boolean = false):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Dispatches an event into the event flow. The event target is the EventDispatcher object upon which <code>dispatchEvent()</code> is called. </p>
		 * 
		 * @param event  — The event object dispatched into the event flow. 
		 * @return  — A value of  unless  is called on the event, in which case it returns . 
		 */
		public function dispatchEvent(event:flash.events.Event):Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns an element whose <code>id</code> property matches the <code>idName</code> parameter. Provides the ability to apply a style based on the <code>id</code>. </p>
		 * <p>For example, the following line sets the style "color" to 0xFF0000 (red), for the element having the <code>id</code> span1.</p>
		 * <div class="listing" version="3.0">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *          textFlow.getElementByID("span1").setStyle("color", 0xFF0000);
		 *          </pre>
		 * </div>
		 * <p><b>Note:</b> In the following code, <code>p.addChild(s)</code> <i>removes</i> <code>s</code> from its original parent and adds it to <code>p</code>, the new parent.</p>
		 * <div class="listing" version="3.0">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *          var s:SpanElement = new SpanElement();
		 *          var p:ParagraphElement = new ParagraphElement();
		 *          ...
		 *          s = textFlow.getElementByID("span3") as SpanElement;
		 *          p.addChild(s);
		 *          textFlow.addChild(p);
		 *          </pre>
		 * </div>
		 * 
		 * @param idName  — The <code>id</code> value of the element to find. 
		 * @return  — The element whose id matches . 
		 */
		public function getElementByID(idName:String):FlowElement {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns all elements that have <code>styleName</code> set to <code>styleNameValue</code>. </p>
		 * 
		 * @param styleNameValue  — The name of the style for which to find elements that have it set. 
		 * @return  — An array of the elements whose  value matches . For example, all elements that have the style name "color". 
		 */
		public function getElementsByStyleName(styleNameValue:String):Array {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns all elements that have <code>typeName</code> set to <code>typeNameValue</code>. </p>
		 * 
		 * @param typeNameValue  — The name of the style for which to find elements that have it set. 
		 * @return  — An array of the elements whose  value matches . For example, all elements that have the type name "foo". 
		 */
		public function getElementsByTypeName(typeNameValue:String):Array {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Checks whether the EventDispatcher object has any listeners registered for a specific type of event. This allows you to determine where an EventDispatcher object has altered handling of an event type in the event flow hierarchy. To determine whether a specific event type will actually trigger an event listener, use <code>IEventDispatcher.willTrigger()</code>. </p>
		 * <p>The difference between <code>hasEventListener()</code> and <code>willTrigger()</code> is that <code>hasEventListener()</code> examines only the object to which it belongs, whereas <code>willTrigger()</code> examines the entire event flow for the event specified by the <code>type</code> parameter.</p>
		 * 
		 * @param type  — The type of event. 
		 * @return  — A value of  if a listener of the specified type is registered;  otherwise. 
		 */
		public function hasEventListener(type:String):Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Invalidates all formatting information for the TextFlow, forcing it to be recomputed. Call this method when styles have changed. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="IFormatResolver.html#invalidateAll()" target="">IFormatResolver.invalidateAll()</a>
		 * </div>
		 */
		public function invalidateAllFormats():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Removes a listener from the EventDispatcher object. If there is no matching listener registered with the EventDispatcher object, a call to this method has no effect. </p>
		 * 
		 * @param type  — The type of event. 
		 * @param listener  — The listener object to remove. 
		 * @param useCapture  — Specifies whether the listener was registered for the capture phase or the target and bubbling phases. If the listener was registered for both the capture phase and the target and bubbling phases, two calls to <code>removeEventListener()</code> are required to remove both: one call with <code>useCapture</code> set to <code>true</code>, and another call with <code>useCapture</code> set to <code>false</code>. 
		 */
		public function removeEventListener(type:String, listener:Function, useCapture:Boolean = false):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Checks whether an event listener is registered with this EventDispatcher object or any of its ancestors for the specified event type. This method returns <code>true</code> if an event listener is triggered during any phase of the event flow when an event of the specified type is dispatched to this EventDispatcher object or any of its descendants. </p>
		 * <p>The difference between <code>hasEventListener()</code> and <code>willTrigger()</code> is that <code>hasEventListener()</code> examines only the object to which it belongs, whereas <code>willTrigger()</code> examines the entire event flow for the event specified by the <code>type</code> parameter.</p>
		 * 
		 * @param type  — The type of event. 
		 * @return  — A value of  if a listener of the specified type will be triggered;  otherwise. 
		 */
		public function willTrigger(type:String):Boolean {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Default configuration for all new TextFlow objects if the configuration is not specified. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Configuration.html" target="">Configuration</a>
		 * </div>
		 * 
		 * @return 
		 */
		public static function get defaultConfiguration():Configuration {
			return _defaultConfiguration;
		}


		/**
		 * @param value
		 * @return 
		 */
		public static function set defaultConfiguration(value:Configuration):void {
			_defaultConfiguration = value;
		}
	}
}
