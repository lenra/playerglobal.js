package flashx.textLayout.elements {
	/**
	 *  The BreakElement class defines a line break, which provides for creating a line break in the text without creating a new paragraph. It inserts a U+2028 character in the text of the paragraph. <p> <b>Note</b>: This class exists primarily to support break tags in MXML markup. To create line breaks, you can add newline characters (\n) directly into the text like this:</p> <div class="listing" version="3.0">
	 *  <div class="clipcopy">
	 *   <a href="#" class="copyText">Copy</a>
	 *  </div>
	 *  <pre>
	 *     spanElement1.text += '\n';
	 *     </pre>
	 * </div> In markup, either FXG, TEXT_LAYOUT_FORMAT or MXML, you can simply insert a where you want the break. <p><span class="classHeaderTableLabel">Default MXML Property</span><code>mxmlChildren</code></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS02f7d8d4857b1677-165a04e1126951a2d98-7ffc.html" target="_blank">Creating TextFlow objects</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="ParagraphElement.html" target="">ParagraphElement</a>
	 *  <br>
	 *  <a href="SpanElement.html" target="">SpanElement</a>
	 * </div><br><hr>
	 */
	public class BreakElement extends SpecialCharacterElement {

		/**
		 * <p> Constructor. </p>
		 */
		public function BreakElement() {
			throw new Error("Not implemented");
		}
	}
}
