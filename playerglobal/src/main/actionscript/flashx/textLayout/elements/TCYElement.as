package flashx.textLayout.elements {
	/**
	 *  The TCYElement (Tatechuuyoko - ta-tae-chu-yo-ko) class is a subclass of SubParagraphGroupElementBase that causes text to draw horizontally within a vertical line. Traditionally, it is used to make small blocks of non-Japanese text or numbers, such as dates, more readable. TCY can be applied to horizontal text, but has no effect on drawing style unless and until it is turned vertically. TCY blocks which contain no text will be removed from the text flow during the normalization process. <p> In the example below, the image on the right shows TCY applied to the number 57, while the image on the left has no TCY formatting.</p> <p> <img src="../../../images/textLayout_TCYElement.png" alt="TCYElement"> </p> <p><span class="classHeaderTableLabel">Default MXML Property</span><code>mxmlChildren</code></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS02f7d8d4857b1677-165a04e1126951a2d98-7ffc.html" target="_blank">Creating TextFlow objects</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="TextFlow.html" target="">TextFlow</a>
	 *  <br>
	 *  <a href="ParagraphElement.html" target="">ParagraphElement</a>
	 *  <br>
	 *  <a href="SpanElement.html" target="">SpanElement</a>
	 * </div><br><hr>
	 */
	public class TCYElement extends SubParagraphGroupElementBase {

		/**
		 * <p> Constructor - creates a new TCYElement instance. </p>
		 */
		public function TCYElement() {
			throw new Error("Not implemented");
		}
	}
}
