package flashx.textLayout.elements {
	/**
	 *  The InlineGraphicElementStatus class defines a set of constants for checking the value of <code>InlineGraphicElement.status</code>. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="InlineGraphicElement.html#status" target="">InlineGraphicElement.status</a>
	 * </div><br><hr>
	 */
	public class InlineGraphicElementStatus {
		/**
		 * <p> An error occurred during loading of a referenced graphic. </p>
		 */
		public static const ERROR:String = "error";
		/**
		 * <p> Load has been initiated (but not completed) on a graphic element that is a URL. </p>
		 */
		public static const LOADING:String = "loading";
		/**
		 * <p> Graphic element is an URL that has not been loaded. </p>
		 */
		public static const LOAD_PENDING:String = "loadPending";
		/**
		 * <p> Graphic is completely loaded and properly sized. </p>
		 */
		public static const READY:String = "ready";
		/**
		 * <p> Graphic element with auto or percentage width/height has completed loading but has not been recomposed. At the next recompose the actual size of the graphic element is calculated. </p>
		 */
		public static const SIZE_PENDING:String = "sizePending";
	}
}
