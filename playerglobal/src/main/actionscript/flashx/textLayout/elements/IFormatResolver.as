package flashx.textLayout.elements {
	import flashx.textLayout.formats.ITextLayoutFormat;

	/**
	 *  Interface to a format resolver. An implementation allows you to attach a styling mechanism of your choosing, such as Flex CSS styling and named styles, to a TextFlow. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="TextFlow.html#formatResolver" target="">TextFlow.formatResolver</a>
	 * </div><br><hr>
	 */
	public interface IFormatResolver {

		/**
		 * <p> Returns the format resolver when a TextFlow is copied. </p>
		 * 
		 * @param oldFlow
		 * @param newFlow
		 * @return  — the format resolver for the copy of the TextFlow. 
		 */
		function getResolverForNewFlow(oldFlow:TextFlow, newFlow:TextFlow):IFormatResolver;

		/**
		 * <p> Invalidates cached formatting information on this element because, for example, the <code>parent</code> changed, or the <code>id</code> or the <code>styleName</code> changed or the <code>typeName</code> changed. </p>
		 * 
		 * @param target
		 */
		function invalidate(target:Object):void;

		/**
		 * <p> Invalidates any cached formatting information for a TextFlow so that formatting must be recomputed. </p>
		 * 
		 * @param textFlow
		 */
		function invalidateAll(textFlow:TextFlow):void;

		/**
		 * <p> Given a FlowElement or ContainerController object, return any format settings for it. </p>
		 * 
		 * @param target
		 * @return  — format settings for the specified object. 
		 */
		function resolveFormat(target:Object):ITextLayoutFormat;

		/**
		 * <p> Given a FlowElement or ContainerController object and the name of a format property, return the format value or <code>undefined</code> if the value is not found. </p>
		 * 
		 * @param target
		 * @param userFormat
		 * @return  — the value of the specified format for the specified object. 
		 */
		function resolveUserFormat(target:Object, userFormat:String):*;
	}
}
