package flashx.textLayout.elements {
	/**
	 *  The ParagraphElement class represents a paragraph in the text flow hierarchy. Its parent is a ParagraphFormattedElement, and its children can include spans (SpanElement), images (inLineGraphicElement), links (LinkElement) and TCY (Tatechuuyoko - ta-tae-chu-yo-ko) elements (TCYElement). The paragraph text is stored in one or more SpanElement objects, which define ranges of text that share the same attributes. A TCYElement object defines a small run of Japanese text that runs perpendicular to the line, as in a horizontal run of text in a vertical line. A TCYElement can also contain multiple spans. <p><span class="classHeaderTableLabel">Default MXML Property</span><code>mxmlChildren</code></p><p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS02f7d8d4857b1677-165a04e1126951a2d98-7ffc.html" target="_blank">Creating TextFlow objects</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="InlineGraphicElement.html" target="">InlineGraphicElement</a>
	 *  <br>
	 *  <a href="LinkElement.html" target="">LinkElement</a>
	 *  <br>
	 *  <a href="SpanElement.html" target="">SpanElement</a>
	 *  <br>
	 *  <a href="TCYElement.html" target="">TCYElement</a>
	 *  <br>
	 *  <a href="TextFlow.html" target="">TextFlow</a>
	 * </div><br><hr>
	 */
	public class ParagraphElement extends ParagraphFormattedElement {

		/**
		 * <p> Constructor - represents a paragraph in a text flow. </p>
		 */
		public function ParagraphElement() {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Scans ahead from the supplied position to find the location in the text of the next atom and returns the index. The term atom refers to both graphic elements and characters (including groups of combining characters), the indivisible entities that make up a text line. </p>
		 * 
		 * @param relativePosition  — position in the text to start from, counting from 0 
		 * @return  — index in the text of the following atom 
		 */
		public function findNextAtomBoundary(relativePosition:int):int {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the index of the next word boundary in the text. </p>
		 * <p>Scans ahead from the supplied position to find the next position in the text that starts or ends a word. </p>
		 * 
		 * @param relativePosition  — position in the text to start from, counting from 0 
		 * @return  — index in the text of the next word boundary 
		 */
		public function findNextWordBoundary(relativePosition:int):int {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Scans backward from the supplied position to find the location in the text of the previous atom and returns the index. The term atom refers to both graphic elements and characters (including groups of combining characters), the indivisible entities that make up a text line. </p>
		 * 
		 * @param relativePosition  — position in the text to start from, counting from 0 
		 * @return  — index in the text of the previous cluster 
		 */
		public function findPreviousAtomBoundary(relativePosition:int):int {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the index of the previous word boundary in the text. </p>
		 * <p>Scans backward from the supplied position to find the previous position in the text that starts or ends a word. </p>
		 * 
		 * @param relativePosition  — position in the text to start from, counting from 0 
		 * @return  — index in the text of the previous word boundary 
		 */
		public function findPreviousWordBoundary(relativePosition:int):int {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the paragraph that follows this one, or null if there are no more paragraphs. </p>
		 * 
		 * @return  — the next paragraph or null if there are no more paragraphs. 
		 */
		public function getNextParagraph():ParagraphElement {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the paragraph that precedes this one, or null, if this paragraph is the first one in the TextFlow. </p>
		 * 
		 * @return 
		 */
		public function getPreviousParagraph():ParagraphElement {
			throw new Error("Not implemented");
		}
	}
}
