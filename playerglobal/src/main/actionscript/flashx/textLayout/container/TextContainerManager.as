package flashx.textLayout.container {
	import flash.display.Sprite;
	import flash.events.ContextMenuEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.FocusEvent;
	import flash.events.IMEEvent;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.TextEvent;
	import flash.geom.Rectangle;
	import flash.text.engine.TextLine;
	
	import flashx.textLayout.compose.ISWFContext;
	import flashx.textLayout.edit.IInteractionEventHandler;
	import flashx.textLayout.edit.ISelectionManager;
	import flashx.textLayout.elements.IConfiguration;
	import flashx.textLayout.elements.TextFlow;
	import flashx.textLayout.events.CompositionCompleteEvent;
	import flashx.textLayout.events.DamageEvent;
	import flashx.textLayout.events.FlowElementMouseEvent;
	import flashx.textLayout.events.FlowOperationEvent;
	import flashx.textLayout.events.SelectionEvent;
	import flashx.textLayout.events.StatusChangeEvent;
	import flashx.textLayout.events.TextLayoutEvent;
	import flashx.textLayout.events.UpdateCompleteEvent;
	import flashx.textLayout.formats.ITextLayoutFormat;

	[Event(name="click", type="flashx.textLayout.events.FlowElementMouseEvent")]
	[Event(name="compositionComplete", type="flashx.textLayout.events.CompositionCompleteEvent")]
	[Event(name="damage", type="flashx.textLayout.events.DamageEvent")]
	[Event(name="flowOperationBegin", type="flashx.textLayout.events.FlowOperationEvent")]
	[Event(name="flowOperationComplete", type="flashx.textLayout.events.FlowOperationEvent")]
	[Event(name="flowOperationEnd", type="flashx.textLayout.events.FlowOperationEvent")]
	[Event(name="inlineGraphicStatusChanged", type="flashx.textLayout.events.StatusChangeEvent")]
	[Event(name="mouseDown", type="flashx.textLayout.events.FlowElementMouseEvent")]
	[Event(name="mouseMove", type="flashx.textLayout.events.FlowElementMouseEvent")]
	[Event(name="mouseUp", type="flashx.textLayout.events.FlowElementMouseEvent")]
	[Event(name="rollOut", type="flashx.textLayout.events.FlowElementMouseEvent")]
	[Event(name="rollOver", type="flashx.textLayout.events.FlowElementMouseEvent")]
	[Event(name="scroll", type="flashx.textLayout.events.TextLayoutEvent")]
	[Event(name="selectionChange", type="flashx.textLayout.events.SelectionEvent")]
	[Event(name="updateComplete", type="flashx.textLayout.events.UpdateCompleteEvent")]
	/**
	 *  Manages text in a container. Assumes that it manages all children of the container. Consider using TextContainerManager for better performance in cases where there is a one container per TextFlow, and the TextFlow is not the main focus, is static text, or is infrequently selected. Good for text in form fields, for example. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="ContainerController.html" target="">ContainerController</a>
	 * </div><br><hr>
	 */
	public class TextContainerManager extends EventDispatcher implements ISWFContext, IInteractionEventHandler, ISandboxSupport {
		private static var _defaultConfiguration:IConfiguration;

		private var _compositionHeight:Number;
		private var _compositionWidth:Number;
		private var _editingMode:String;
		private var _horizontalScrollPolicy:String;
		private var _horizontalScrollPosition:Number;
		private var _hostFormat:ITextLayoutFormat;
		private var _swfContext:ISWFContext;
		private var _verticalScrollPolicy:String;
		private var _verticalScrollPosition:Number;

		private var _configuration:IConfiguration;
		private var _container:Sprite;
		private var _numLines:int;

		/**
		 * <p> Constructor function - creates a TextContainerManager instance. For best results: </p>
		 * <ol>
		 *  <li>Start with TextContainerManager.defaultConfiguration and modify it</li>
		 *  <li>Share the same Configuration among many InputManagers</li>
		 * </ol>
		 * 
		 * @param container  — The DisplayObjectContainer in which to manage the text lines. 
		 * @param configuration  — - The IConfiguration instance to use with this TextContainerManager instance. 
		 */
		public function TextContainerManager(container:Sprite, configuration:IConfiguration = null) {
			super(container);
			this._configuration = configuration;
		}

		/**
		 * <p> Returns the vertical extent allowed for text inside the container. The value is specified in pixels. </p>
		 * <p>After setting this property, the text in the container is damaged and requires composing.</p>
		 * 
		 * @return 
		 */
		public function get compositionHeight():Number {
			return _compositionHeight;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set compositionHeight(value:Number):void {
			_compositionHeight = value;
		}

		/**
		 * <p> Returns the horizontal extent allowed for text inside the container. The value is specified in pixels. </p>
		 * <p>After setting this property, the text in the container is damaged and requires composing.</p>
		 * 
		 * @return 
		 */
		public function get compositionWidth():Number {
			return _compositionWidth;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set compositionWidth(value:Number):void {
			_compositionWidth = value;
		}

		/**
		 * <p> The Configuration object for this TextContainerManager. </p>
		 * 
		 * @return 
		 */
		public function get configuration():IConfiguration {
			return _configuration;
		}

		/**
		 * <p> Returns the container (DisplayObjectContainer) that holds the text that this TextContainerManager manages. </p>
		 * 
		 * @return 
		 */
		public function get container():Sprite {
			return _container;
		}

		/**
		 * <p> Editing mode of this TextContainerManager. Modes are reading only, reading and selection permitted, and editing (reading, selection, and writing) permitted. Use the constant values of the EditingMode class to set this property. </p>
		 * <p>Default value is READ_WRITE.</p>
		 * 
		 * @return 
		 */
		public function get editingMode():String {
			return _editingMode;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set editingMode(value:String):void {
			_editingMode = value;
		}

		/**
		 * <p> Controls whether the factory generates all text lines or stops when the container bounds are filled. </p>
		 * 
		 * @return 
		 */
		public function get horizontalScrollPolicy():String {
			return _horizontalScrollPolicy;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set horizontalScrollPolicy(value:String):void {
			_horizontalScrollPolicy = value;
		}

		/**
		 * <p> Specifies the current horizontal scroll location on the stage. The value specifies the number of pixels from the left. </p>
		 * 
		 * @return 
		 */
		public function get horizontalScrollPosition():Number {
			return _horizontalScrollPosition;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set horizontalScrollPosition(value:Number):void {
			_horizontalScrollPosition = value;
		}

		/**
		 * @return 
		 */
		public function get hostFormat():ITextLayoutFormat {
			return _hostFormat;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set hostFormat(value:ITextLayoutFormat):void {
			_hostFormat = value;
		}

		/**
		 * <p> The total number of lines composed in the flow. By default TLF does not compose the entire flow and this value may be innacruate. Use composeToPosition to get all lines composed. </p>
		 * 
		 * @return 
		 */
		public function get numLines():int {
			return _numLines;
		}

		/**
		 * <p> Optional ISWFContext instance used to make FTE calls as needed in the proper swf context. </p>
		 * 
		 * @return 
		 */
		public function get swfContext():ISWFContext {
			return _swfContext;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set swfContext(value:ISWFContext):void {
			_swfContext = value;
		}

		/**
		 * <p> Controls whether the factory generates all text lines or stops when the container bounds are filled. </p>
		 * 
		 * @return 
		 */
		public function get verticalScrollPolicy():String {
			return _verticalScrollPolicy;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set verticalScrollPolicy(value:String):void {
			_verticalScrollPolicy = value;
		}

		/**
		 * <p> Specifies the current vertical scroll location on the stage. The value specifies the number of pixels from the top. </p>
		 * 
		 * @return 
		 */
		public function get verticalScrollPosition():Number {
			return _verticalScrollPosition;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set verticalScrollPosition(value:Number):void {
			_verticalScrollPosition = value;
		}

		/**
		 * <p> Processes the <code>Event.ACTIVATE</code> event when the client manages events. </p>
		 * 
		 * @param event  — The Event object. 
		 */
		public function activateHandler(event:flash.events.Event):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the current ISelectionManager instance. Converts to TextFlow instance and creates one if necessary. </p>
		 * 
		 * @return  — the interaction manager for this TextContainerManager instance. 
		 */
		public function beginInteraction():ISelectionManager {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Called to request clients to begin the forwarding of mouseup and mousemove events from outside a security sandbox. </p>
		 */
		public function beginMouseCapture():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Composes the container text; calls either the factory or <code>updateAllControllers()</code>. </p>
		 */
		public function compose():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes the <code>Event.DEACTIVATE</code> event when the client manages events. </p>
		 * 
		 * @param event  — The Event object. 
		 */
		public function deactivateHandler(event:flash.events.Event):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns <code>true</code> if it has filled in the container's scrollRect property. This method enables you to test whether <code>scrollRect</code> is set without actually accessing the <code>scrollRect</code> property which can possibly create a performance issue. </p>
		 * <p>Override this method to draw a background or a border. Overriding this method can be tricky as the scrollRect <b>must</b> be set as specified.</p>
		 * 
		 * @param scrollX  — The starting horizontal position of the scroll rectangle. 
		 * @param scrollY  — The starting vertical position of the scroll rectangle. 
		 * @return  —  if it has created the  object. 
		 */
		public function drawBackgroundAndSetScrollRect(scrollX:Number, scrollY:Number):Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes an edit event (CUT, COPY, PASTE, SELECT_ALL) when the client manages events. </p>
		 * 
		 * @param event  — Event object. 
		 */
		public function editHandler(event:flash.events.Event):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Terminates interaction. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../../flashx/textLayout/edit/ISelectionManager.html" target="">ISelectionManager</a>
		 * </div>
		 */
		public function endInteraction():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Called to inform clients that the the forwarding of mouseup and mousemove events from outside a security sandbox is no longer needed. </p>
		 */
		public function endMouseCapture():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes the <code>FocusEvent.KEY_FOCUS_CHANGE</code> and <code>FocusEvent.MOUSE_FOCUS_CHANGE</code> events when the client manages events. </p>
		 * 
		 * @param event  — The FocusEvent object. 
		 */
		public function focusChangeHandler(event:FocusEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Process a focusIn event. </p>
		 * 
		 * @param event
		 */
		public function focusInHandler(event:FocusEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes the <code>FocusEvent.FOCUS_OUT</code> event when the client manages events. </p>
		 * 
		 * @param event  — The FocusEvent object. 
		 */
		public function focusOutHandler(event:FocusEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Creates a rectangle that shows where the last call to either the <code>compose()</code> method or the <code>updateContainer()</code> method placed the text. </p>
		 * 
		 * @return  — the bounds of the content 
		 */
		public function getContentBounds():Rectangle {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Return the TextLine at the index from array of composed lines. </p>
		 * 
		 * @param index  — Finds the line at this index position in the text. 
		 * @return  — the TextLine that occurs at the specified index. 
		 */
		public function getLineAt(index:int):TextLine {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Figure out the scroll distance required to scroll up or down by the specified number of lines. Negative numbers scroll upward, bringing more of the top of the TextFlow into view. Positive numbers scroll downward, bringing the next line from the bottom into full view. </p>
		 * <p>When scrolling up, for example, the method makes the next line fully visible. If the next line is partially obscured and the number of lines specified is 1, the partially obscured line becomes fully visible.</p>
		 * 
		 * @param numLines  — The number of lines to scroll. 
		 * @return  — the delta amount of space to scroll 
		 */
		public function getScrollDelta(numLines:int):Number {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the current text using a separator between paragraphs. The separator can be specified with the <code>separator</code> argument. The default value of the <code>separator</code> argument is the Unicode character <code>'PARAGRAPH SEPARATOR' (U+2029)</code>. </p>
		 * <p>Calling the setter discards any attached TextFlow. Any selection is lost.</p>
		 * 
		 * @param separator  — String to set between paragraphs. 
		 * @return 
		 */
		public function getText(separator:String =""):String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> The current TextFlow. Converts this to a full TextFlow representation if it isn't already one. </p>
		 * 
		 * @return  — the current TextFlow object 
		 */
		public function getTextFlow():TextFlow {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes the <code>IME_START_COMPOSITION</code> event when the client manages events. </p>
		 * 
		 * @param event  — The IMEEvent object. 
		 */
		public function imeStartCompositionHandler(event:IMEEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Call this if you change the selection formats (SelectionFormat) and want the interactionManager to update. </p>
		 */
		public function invalidateSelectionFormats():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Call this if you are editing, and want to reset the undo manager used for editing. </p>
		 */
		public function invalidateUndoManager():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns <code>true</code> if the content needs composing. </p>
		 * 
		 * @return  —  if the content needs composing;  otherwise. 
		 */
		public function isDamaged():Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes the <code>KeyboardEvent.KEY_DOWN</code> event when the client manages events. </p>
		 * 
		 * @param event  — KeyboardEvent object. 
		 */
		public function keyDownHandler(event:KeyboardEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes the <code>FocusEvent.KEY_FOCUS_CHANGE</code> event when the client manages events. </p>
		 * 
		 * @param event  — The FocusEvent object. 
		 */
		public function keyFocusChangeHandler(event:FocusEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes the <code>Keyboard.KEY_UP</code> event when the client manages events. </p>
		 * 
		 * @param event  — The KeyboardEvent object. 
		 */
		public function keyUpHandler(event:KeyboardEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes the <code>ContextMenuEvent.MENU_SELECT</code> event when the client manages events. </p>
		 * 
		 * @param event  — ContextMenuEvent object. 
		 */
		public function menuSelectHandler(event:ContextMenuEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes the <code>MouseEvent.DOUBLE_CLICK</code> event when the client manages events. </p>
		 * 
		 * @param event  — The MouseEvent object. 
		 */
		public function mouseDoubleClickHandler(event:MouseEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes the <code>MouseEvent.MOUSE_DOWN</code> event when the client manages events. </p>
		 * 
		 * @param event  — The MouseEvent object. 
		 */
		public function mouseDownHandler(event:MouseEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes the <code>MouseEvent.MOUSE_MOVE</code> event when the client manages events. </p>
		 * 
		 * @param event  — The MouseEvent object. 
		 */
		public function mouseMoveHandler(event:MouseEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Client call to forward a mouseMove event from outside a security sandbox. Coordinates of the mouse move are not needed. </p>
		 * 
		 * @param e
		 */
		public function mouseMoveSomewhere(e:flash.events.Event):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes the <code>MouseEvent.MOUSE_OUT</code> event when the client manages events. </p>
		 * 
		 * @param event  — The MouseEvent object. 
		 */
		public function mouseOutHandler(event:MouseEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Process a mouseOver event. </p>
		 * 
		 * @param event
		 */
		public function mouseOverHandler(event:MouseEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes the <code>MouseEvent.MOUSE_UP</code> event when the client manages events. </p>
		 * 
		 * @param event  — The MouseEvent object. 
		 */
		public function mouseUpHandler(event:MouseEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Client call to forward a mouseUp event from outside a security sandbox. Coordinates of the mouse up are not needed. </p>
		 * 
		 * @param e
		 */
		public function mouseUpSomewhere(e:flash.events.Event):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes the <code>MouseEvent.MOUSE_WHEEL</code> event when the client manages events. </p>
		 * 
		 * @param event  — The MouseEvent object. 
		 */
		public function mouseWheelHandler(event:MouseEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * @param textLine
		 */
		public function resetLine(textLine:TextLine):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Scrolls so that the text range is visible in the container. </p>
		 * 
		 * @param activePosition  — The end of the selection that is changed when you extend the selection. It can be either the start or the end of the selection, expressed as an offset from the start of the text flow. 
		 * @param anchorPosition  — The stable end of the selection when you extend the selection. It can be either the start or the end of the selection. 
		 */
		public function scrollToRange(activePosition:int, anchorPosition:int):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets the <code>text</code> property to the specified String. Discards any attached TextFlow. Any selection is lost. </p>
		 * 
		 * @param text  — the String to set 
		 */
		public function setText(text:String):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets a TextFlow into this TextContainerManager replacing any existing TextFlow and discarding the current text. </p>
		 * 
		 * @param textFlow
		 */
		public function setTextFlow(textFlow:TextFlow):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes the <code>SOFT_KEYBOARD_ACTIVATING</code> event when the client manages events. </p>
		 * 
		 * @param event  — The SoftKeyboardEvent object. 
		 */
		public function softKeyboardActivatingHandler(event:flash.events.Event):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes the <code>TextEvent.TEXT_INPUT</code> event when the client manages events. </p>
		 * 
		 * @param event  — The TextEvent object. 
		 */
		public function textInputHandler(event:TextEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Updates the display; calls either the factory or updateAllControllers(). </p>
		 * 
		 * @return  — true if anything changed. 
		 */
		public function updateContainer():Boolean {
			throw new Error("Not implemented");
		}


		/**
		 * <p> The default configuration for this TextContainerManager. Column and padding attributes are set to <code>FormatValue.INHERIT</code>. </p>
		 * 
		 * @return 
		 */
		public static function get defaultConfiguration():IConfiguration {
			return _defaultConfiguration;
		}
		
		public function callInContext(fn:Function, thisArg:Object, argArray:Array, returns:Boolean = true):* {
			
		}
	}
}