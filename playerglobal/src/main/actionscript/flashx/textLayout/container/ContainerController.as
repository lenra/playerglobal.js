package flashx.textLayout.container {
	import flash.display.Sprite;
	import flash.events.ContextMenuEvent;
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.IMEEvent;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.TextEvent;
	import flash.geom.Rectangle;

	import flashx.textLayout.compose.IFlowComposer;
	import flashx.textLayout.edit.IInteractionEventHandler;
	import flashx.textLayout.edit.ISelectionManager;
	import flashx.textLayout.elements.ContainerFormattedElement;
	import flashx.textLayout.elements.TextFlow;
	import flashx.textLayout.formats.ITextLayoutFormat;

	/**
	 *  The ContainerController class defines the relationship between a TextFlow object and a container. A TextFlow may have one or more rectangular areas that can hold text; the text is said to be flowing through the containers. Each container is a Sprite that is the parent DisplayObject for the TextLines. Each container has a ContainerController that manages the container; the controller holds the target width and height for the text area, populates the container with TextLines, and handles scrolling. A controller also has a format associated with it that allows some formatting attributes to be applied to the text in the container. This allows, for instance, a TextFlow to have one container where the text appears in a single column, and a second container in the same TextFlow with two column text. Not all formatting attributes that can be applied to the container will affect the text; only the ones that affect container-level layout. The diagram below illustrates the relationship between the TextFlow, its flowComposer, and the display list. <p> <img src="../../../images/textLayout_multiController.gif" alt="IContainerController"> </p> <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flashx/textLayout/compose/IFlowComposer.html" target="">flashx.textLayout.compose.IFlowComposer</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/elements/TextFlow.html" target="">flashx.textLayout.elements.TextFlow</a>
	 *  <br>
	 *  <a href="../../../flash/text/engine/TextLine.html" target="">flash.text.engine.TextLine</a>
	 * </div><br><hr>
	 */
	public class ContainerController implements IInteractionEventHandler, ITextLayoutFormat, ISandboxSupport {
		private static var _containerControllerInitialFormat:ITextLayoutFormat;

		private var _alignmentBaseline:*;
		private var _backgroundAlpha:*;
		private var _backgroundColor:*;
		private var _baselineShift:*;
		private var _blockProgression:*;
		private var _breakOpportunity:*;
		private var _cffHinting:*;
		private var _clearFloats:*;
		private var _color:*;
		private var _columnCount:*;
		private var _columnGap:*;
		private var _columnWidth:*;
		private var _digitCase:*;
		private var _digitWidth:*;
		private var _direction:*;
		private var _dominantBaseline:*;
		private var _firstBaselineOffset:*;
		private var _fontFamily:*;
		private var _fontLookup:*;
		private var _fontSize:*;
		private var _fontStyle:*;
		private var _fontWeight:*;
		private var _format:ITextLayoutFormat;
		private var _horizontalScrollPolicy:String;
		private var _horizontalScrollPosition:Number;
		private var _justificationRule:*;
		private var _justificationStyle:*;
		private var _kerning:*;
		private var _leadingModel:*;
		private var _ligatureLevel:*;
		private var _lineBreak:*;
		private var _lineHeight:*;
		private var _lineThrough:*;
		private var _linkActiveFormat:*;
		private var _linkHoverFormat:*;
		private var _linkNormalFormat:*;
		private var _listAutoPadding:*;
		private var _listMarkerFormat:*;
		private var _listStylePosition:*;
		private var _listStyleType:*;
		private var _locale:*;
		private var _paddingBottom:*;
		private var _paddingLeft:*;
		private var _paddingRight:*;
		private var _paddingTop:*;
		private var _paragraphEndIndent:*;
		private var _paragraphSpaceAfter:*;
		private var _paragraphSpaceBefore:*;
		private var _paragraphStartIndent:*;
		private var _renderingMode:*;
		private var _styleName:*;
		private var _tabStops:*;
		private var _textAlign:*;
		private var _textAlignLast:*;
		private var _textAlpha:*;
		private var _textDecoration:*;
		private var _textIndent:*;
		private var _textJustify:*;
		private var _textRotation:*;
		private var _trackingLeft:*;
		private var _trackingRight:*;
		private var _typographicCase:*;
		private var _userStyles:Object;
		private var _verticalAlign:*;
		private var _verticalScrollPolicy:String;
		private var _verticalScrollPosition:Number;
		private var _whiteSpaceCollapse:*;
		private var _wordSpacing:*;

		private var _absoluteStart:int;
		private var _columnState:ColumnState;
		private var _compositionHeight:Number;
		private var _compositionWidth:Number;
		private var _computedFormat:ITextLayoutFormat;
		private var _container:Sprite;
		private var _coreStyles:Object;
		private var _flowComposer:IFlowComposer;
		private var _interactionManager:ISelectionManager;
		private var _rootElement:ContainerFormattedElement;
		private var _styles:Object;
		private var _textFlow:TextFlow;
		private var _textLength:int;

		/**
		 * <p> Constructor - creates a ContainerController instance. The ContainerController has a default <code>compositionWidth</code> and <code>compositionHeight</code> so that some text appears in the container if you don't specify its width height. </p>
		 * 
		 * @param container  — The DisplayObjectContainer in which to manage the text lines. 
		 * @param compositionWidth  — The initial width for composing text in the container. 
		 * @param compositionHeight  — The initial height for composing text in the container. 
		 */
		public function ContainerController(container:Sprite, compositionWidth:Number = 100, compositionHeight:Number = 100) {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the first character in the container. If this is not the first container in the flow, this value is updated when the text is composed, that is when the IFlowComposer's <code>compose()</code> or <code>updateAllControllers()</code> methods are called. </p>
		 * 
		 * @return 
		 */
		public function get absoluteStart():int {
			return _absoluteStart;
		}

		/**
		 * <p> TextLayoutFormat: Specifies the baseline to which the dominant baseline aligns. For example, if you set <code>dominantBaseline</code> to ASCENT, setting <code>alignmentBaseline</code> to DESCENT aligns the top of the text with the DESCENT baseline, or below the line. The largest element in the line generally determines the baselines.</p>
		 * <p><img src="../../../images/textLayout_baselines.jpg" alt="baselines"></p>
		 * <p>Legal values are TextBaseline.ROMAN, TextBaseline.ASCENT, TextBaseline.DESCENT, TextBaseline.IDEOGRAPHIC_TOP, TextBaseline.IDEOGRAPHIC_CENTER, TextBaseline.IDEOGRAPHIC_BOTTOM, TextBaseline.USE_DOMINANT_BASELINE, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of TextBaseline.USE_DOMINANT_BASELINE.</p>
		 * 
		 * @return 
		 */
		public function get alignmentBaseline():* {
			return _alignmentBaseline;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set alignmentBaseline(value:*):void {
			_alignmentBaseline = value;
		}

		/**
		 * <p> TextLayoutFormat: Alpha (transparency) value for the background (adopts default value if undefined during cascade). A value of 0 is fully transparent, and a value of 1 is fully opaque. Display objects with alpha set to 0 are active, even though they are invisible. </p>
		 * <p>Legal values are numbers from 0 to 1 and FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will have a value of 1.</p>
		 * 
		 * @return 
		 */
		public function get backgroundAlpha():* {
			return _backgroundAlpha;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set backgroundAlpha(value:*):void {
			_backgroundAlpha = value;
		}

		/**
		 * <p> TextLayoutFormat: Background color of the text (adopts default value if undefined during cascade). Can be either the constant value <code>BackgroundColor.TRANSPARENT</code>, or a hexadecimal value that specifies the three 8-bit RGB (red, green, blue) values; for example, 0xFF0000 is red and 0x00FF00 is green. </p>
		 * <p>Legal values as a string are BackgroundColor.TRANSPARENT, FormatValue.INHERIT and uints from 0x0 to 0xffffffff.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will have a value of BackgroundColor.TRANSPARENT.</p>
		 * 
		 * @return 
		 */
		public function get backgroundColor():* {
			return _backgroundColor;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set backgroundColor(value:*):void {
			_backgroundColor = value;
		}

		/**
		 * <p> TextLayoutFormat: Amount to shift the baseline from the <code>dominantBaseline</code> value. Units are in pixels, or a percentage of <code>fontSize</code> (in which case, enter a string value, like 140%). Positive values shift the line up for horizontal text (right for vertical) and negative values shift it down for horizontal (left for vertical). </p>
		 * <p>Legal values are BaselineShift.SUPERSCRIPT, BaselineShift.SUBSCRIPT, FormatValue.INHERIT.</p>
		 * <p>Legal values as a number are from -1000 to 1000.</p>
		 * <p>Legal values as a percent are numbers from -1000 to 1000.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of 0.0.</p>
		 * 
		 * @return 
		 */
		public function get baselineShift():* {
			return _baselineShift;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set baselineShift(value:*):void {
			_baselineShift = value;
		}

		/**
		 * <p> TextLayoutFormat: Specifies a vertical or horizontal progression of line placement. Lines are either placed top-to-bottom (<code>BlockProgression.TB</code>, used for horizontal text) or right-to-left (<code>BlockProgression.RL</code>, used for vertical text). </p>
		 * <p>Legal values are BlockProgression.RL, BlockProgression.TB, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of BlockProgression.TB.</p>
		 * 
		 * @return 
		 */
		public function get blockProgression():* {
			return _blockProgression;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set blockProgression(value:*):void {
			_blockProgression = value;
		}

		/**
		 * <p> TextLayoutFormat: Controls where lines are allowed to break when breaking wrapping text into multiple lines. Set to <code>BreakOpportunity.AUTO</code> to break text normally. Set to <code>BreakOpportunity.NONE</code> to <i>not</i> break the text unless the text would overrun the measure and there are no other places to break the line. Set to <code>BreakOpportunity.ANY</code> to allow the line to break anywhere, rather than just between words. Set to <code>BreakOpportunity.ALL</code> to have each typographic cluster put on a separate line (useful for text on a path). </p>
		 * <p>Legal values are BreakOpportunity.ALL, BreakOpportunity.ANY, BreakOpportunity.AUTO, BreakOpportunity.NONE, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of BreakOpportunity.AUTO.</p>
		 * 
		 * @return 
		 */
		public function get breakOpportunity():* {
			return _breakOpportunity;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set breakOpportunity(value:*):void {
			_breakOpportunity = value;
		}

		/**
		 * <p> TextLayoutFormat: The type of CFF hinting used for this text. CFF hinting determines whether the Flash runtime forces strong horizontal stems to fit to a sub pixel grid or not. This property applies only if the <code>renderingMode</code> property is set to <code>RenderingMode.CFF</code>, and the font is embedded (<code>fontLookup</code> property is set to <code>FontLookup.EMBEDDED_CFF</code>). At small screen sizes, hinting produces a clear, legible text for human readers. </p>
		 * <p>Legal values are CFFHinting.NONE, CFFHinting.HORIZONTAL_STEM, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of CFFHinting.HORIZONTAL_STEM.</p>
		 * 
		 * @return 
		 */
		public function get cffHinting():* {
			return _cffHinting;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set cffHinting(value:*):void {
			_cffHinting = value;
		}

		/**
		 * <p> TextLayoutFormat: Controls how text wraps around a float. A value of none will allow the text to wrap most closely around a float. A value of left will cause the text to skip over any portion of the container that has a left float, and a value of right will cause the text to skip over any portion of the container that has a right float. A value of both will cause the text to skip over any floats. </p>
		 * <p>Legal values are ClearFloats.START, ClearFloats.END, ClearFloats.LEFT, ClearFloats.RIGHT, ClearFloats.BOTH, ClearFloats.NONE, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will have a value of ClearFloats.NONE.</p>
		 * 
		 * @return 
		 */
		public function get clearFloats():* {
			return _clearFloats;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set clearFloats(value:*):void {
			_clearFloats = value;
		}

		/**
		 * <p> TextLayoutFormat: Color of the text. A hexadecimal number that specifies three 8-bit RGB (red, green, blue) values; for example, 0xFF0000 is red and 0x00FF00 is green. </p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of 0.</p>
		 * 
		 * @return 
		 */
		public function get color():* {
			return _color;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set color(value:*):void {
			_color = value;
		}

		/**
		 * <p> TextLayoutFormat: Number of text columns (adopts default value if undefined during cascade). The column number overrides the other column settings. Value is an integer, or <code>FormatValue.AUTO</code> if unspecified. If <code>columnCount</code> is not specified,<code>columnWidth</code> is used to create as many columns as can fit in the container. </p>
		 * <p>Legal values as a string are FormatValue.AUTO, FormatValue.INHERIT and from ints from 1 to 50.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will have a value of FormatValue.AUTO.</p>
		 * 
		 * @return 
		 */
		public function get columnCount():* {
			return _columnCount;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set columnCount(value:*):void {
			_columnCount = value;
		}

		/**
		 * <p> TextLayoutFormat: Specifies the amount of gutter space, in pixels, to leave between the columns (adopts default value if undefined during cascade). Value is a Number </p>
		 * <p>Legal values are numbers from 0 to 1000 and FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will have a value of 20.</p>
		 * 
		 * @return 
		 */
		public function get columnGap():* {
			return _columnGap;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set columnGap(value:*):void {
			_columnGap = value;
		}

		/**
		 * <p> Returns a ColumnState object, which describes the number and characteristics of columns in the container. These values are updated when the text is recomposed, either as a result of <code>IFlowComposer.compose()</code> or <code>IFlowComposer.updateAllControllers()</code>. </p>
		 * 
		 * @return 
		 */
		public function get columnState():ColumnState {
			return _columnState;
		}

		/**
		 * <p> TextLayoutFormat: Column width in pixels (adopts default value if undefined during cascade). If you specify the width of the columns, but not the count, TextLayout will create as many columns of that width as possible, given the container width and <code>columnGap</code> settings. Any remainder space is left after the last column. Value is a Number. </p>
		 * <p>Legal values as a string are FormatValue.AUTO, FormatValue.INHERIT and numbers from 0 to 8000.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will have a value of FormatValue.AUTO.</p>
		 * 
		 * @return 
		 */
		public function get columnWidth():* {
			return _columnWidth;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set columnWidth(value:*):void {
			_columnWidth = value;
		}

		/**
		 * <p> Returns the vertical extent allowed for text inside the container. The value is specified in pixels. </p>
		 * 
		 * @return 
		 */
		public function get compositionHeight():Number {
			return _compositionHeight;
		}

		/**
		 * <p> Returns the horizontal extent allowed for text inside the container. The value is specified in pixels. </p>
		 * 
		 * @return 
		 */
		public function get compositionWidth():Number {
			return _compositionWidth;
		}

		/**
		 * <p> Returns an ITextLayoutFormat instance with the attributes applied to this container, including the attributes inherited from its root element. </p>
		 * 
		 * @return 
		 */
		public function get computedFormat():ITextLayoutFormat {
			return _computedFormat;
		}

		/**
		 * <p> Returns the container display object that holds the text lines for this ContainerController instance. </p>
		 * 
		 * @return 
		 */
		public function get container():Sprite {
			return _container;
		}

		/**
		 * <p> Returns the <code>coreStyles</code> on this ContainerController. Note that the getter makes a copy of the core styles dictionary. The returned object includes the formats that are defined by TextLayoutFormat and are in TextLayoutFormat.description. The returned object consists of an array of <i>stylename-value</i> pairs. </p>
		 * 
		 * @return 
		 */
		public function get coreStyles():Object {
			return _coreStyles;
		}

		/**
		 * <p> TextLayoutFormat: The type of digit case used for this text. Setting the value to <code>DigitCase.OLD_STYLE</code> approximates lowercase letterforms with varying ascenders and descenders. The figures are proportionally spaced. This style is only available in selected typefaces, most commonly in a supplemental or expert font. The <code>DigitCase.LINING</code> setting has all-cap height and is typically monospaced to line up in charts.</p>
		 * <p><img src="../../../images/textLayout_digitcase.gif" alt="digitCase"></p>
		 * <p>Legal values are DigitCase.DEFAULT, DigitCase.LINING, DigitCase.OLD_STYLE, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of DigitCase.DEFAULT.</p>
		 * 
		 * @return 
		 */
		public function get digitCase():* {
			return _digitCase;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set digitCase(value:*):void {
			_digitCase = value;
		}

		/**
		 * <p> TextLayoutFormat: Type of digit width used for this text. This can be <code>DigitWidth.PROPORTIONAL</code>, which looks best for individual numbers, or <code>DigitWidth.TABULAR</code>, which works best for numbers in tables, charts, and vertical rows.</p>
		 * <p><img src="../../../images/textLayout_digitwidth.gif" alt="digitWidth"></p>
		 * <p>Legal values are DigitWidth.DEFAULT, DigitWidth.PROPORTIONAL, DigitWidth.TABULAR, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of DigitWidth.DEFAULT.</p>
		 * 
		 * @return 
		 */
		public function get digitWidth():* {
			return _digitWidth;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set digitWidth(value:*):void {
			_digitWidth = value;
		}

		/**
		 * <p> TextLayoutFormat: Specifies the default bidirectional embedding level of the text in the text block. Left-to-right reading order, as in Latin-style scripts, or right-to-left reading order, as in Arabic or Hebrew. This property also affects column direction when it is applied at the container level. Columns can be either left-to-right or right-to-left, just like text. Below are some examples:</p>
		 * <p><img src="../../../images/textLayout_direction.gif" alt="direction"></p>
		 * <p>Legal values are Direction.LTR, Direction.RTL, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of Direction.LTR.</p>
		 * 
		 * @return 
		 */
		public function get direction():* {
			return _direction;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set direction(value:*):void {
			_direction = value;
		}

		/**
		 * <p> TextLayoutFormat: Specifies which element baseline snaps to the <code>alignmentBaseline</code> to determine the vertical position of the element on the line. A value of <code>TextBaseline.AUTO</code> selects the dominant baseline based on the <code>locale</code> property of the parent paragraph. For Japanese and Chinese, the selected baseline value is <code>TextBaseline.IDEOGRAPHIC_CENTER</code>; for all others it is <code>TextBaseline.ROMAN</code>. These baseline choices are determined by the choice of font and the font size.</p>
		 * <p><img src="../../../images/textLayout_baselines.jpg" alt="baselines"></p>
		 * <p>Legal values are FormatValue.AUTO, TextBaseline.ROMAN, TextBaseline.ASCENT, TextBaseline.DESCENT, TextBaseline.IDEOGRAPHIC_TOP, TextBaseline.IDEOGRAPHIC_CENTER, TextBaseline.IDEOGRAPHIC_BOTTOM, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of FormatValue.AUTO.</p>
		 * 
		 * @return 
		 */
		public function get dominantBaseline():* {
			return _dominantBaseline;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set dominantBaseline(value:*):void {
			_dominantBaseline = value;
		}

		/**
		 * <p> TextLayoutFormat: Specifies the baseline position of the first line in the container. Which baseline this property refers to depends on the container-level locale. For Japanese and Chinese, it is <code>TextBaseline.IDEOGRAPHIC_BOTTOM</code>; for all others it is <code>TextBaseline.ROMAN</code>. The offset from the top inset (or right inset if <code>blockProgression</code> is RL) of the container to the baseline of the first line can be either <code>BaselineOffset.ASCENT</code>, meaning equal to the ascent of the line, <code>BaselineOffset.LINE_HEIGHT</code>, meaning equal to the height of that first line, or any fixed-value number to specify an absolute distance. <code>BaselineOffset.AUTO</code> aligns the ascent of the line with the container top inset.</p>
		 * <p><img src="../../../images/textLayout_FBO1.png" alt="firstBaselineOffset1"><img src="../../../images/textLayout_FBO2.png" alt="firstBaselineOffset2"><img src="../../../images/textLayout_FBO3.png" alt="firstBaselineOffset3"><img src="../../../images/textLayout_FBO4.png" alt="firstBaselineOffset4"></p>
		 * <p>Legal values as a string are BaselineOffset.AUTO, BaselineOffset.ASCENT, BaselineOffset.LINE_HEIGHT, FormatValue.INHERIT and numbers from 0 to 1000.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of BaselineOffset.AUTO.</p>
		 * 
		 * @return 
		 */
		public function get firstBaselineOffset():* {
			return _firstBaselineOffset;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set firstBaselineOffset(value:*):void {
			_firstBaselineOffset = value;
		}

		/**
		 * <p> Returns the flow composer object that composes and highlights text into the container that this controller manages. </p>
		 * 
		 * @return 
		 */
		public function get flowComposer():IFlowComposer {
			return _flowComposer;
		}

		/**
		 * <p> TextLayoutFormat: The name of the font to use, or a comma-separated list of font names. The Flash runtime renders the element with the first available font in the list. For example Arial, Helvetica, _sans causes the player to search for Arial, then Helvetica if Arial is not found, then _sans if neither is found. </p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of Arial.</p>
		 * 
		 * @return 
		 */
		public function get fontFamily():* {
			return _fontFamily;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set fontFamily(value:*):void {
			_fontFamily = value;
		}

		/**
		 * <p> TextLayoutFormat: Font lookup to use. Specifying <code>FontLookup.DEVICE</code> uses the fonts installed on the system that is running the SWF file. Device fonts result in a smaller movie size, but text is not always rendered the same across different systems and platforms. Specifying <code>FontLookup.EMBEDDED_CFF</code> uses font outlines embedded in the published SWF file. Embedded fonts increase the size of the SWF file (sometimes dramatically), but text is consistently displayed in the chosen font. </p>
		 * <p>Legal values are FontLookup.DEVICE, FontLookup.EMBEDDED_CFF, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of FontLookup.DEVICE.</p>
		 * 
		 * @return 
		 */
		public function get fontLookup():* {
			return _fontLookup;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set fontLookup(value:*):void {
			_fontLookup = value;
		}

		/**
		 * <p> TextLayoutFormat: The size of the text in pixels. </p>
		 * <p>Legal values are numbers from 1 to 720 and FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of 12.</p>
		 * 
		 * @return 
		 */
		public function get fontSize():* {
			return _fontSize;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set fontSize(value:*):void {
			_fontSize = value;
		}

		/**
		 * <p> TextLayoutFormat: Style of text. May be <code>FontPosture.NORMAL</code>, for use in plain text, or <code>FontPosture.ITALIC</code> for italic. This property applies only to device fonts (<code>fontLookup</code> property is set to flash.text.engine.FontLookup.DEVICE). </p>
		 * <p>Legal values are FontPosture.NORMAL, FontPosture.ITALIC, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of FontPosture.NORMAL.</p>
		 * 
		 * @return 
		 */
		public function get fontStyle():* {
			return _fontStyle;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set fontStyle(value:*):void {
			_fontStyle = value;
		}

		/**
		 * <p> TextLayoutFormat: Weight of text. May be <code>FontWeight.NORMAL</code> for use in plain text, or <code>FontWeight.BOLD</code>. Applies only to device fonts (<code>fontLookup</code> property is set to flash.text.engine.FontLookup.DEVICE). </p>
		 * <p>Legal values are FontWeight.NORMAL, FontWeight.BOLD, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of FontWeight.NORMAL.</p>
		 * 
		 * @return 
		 */
		public function get fontWeight():* {
			return _fontWeight;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set fontWeight(value:*):void {
			_fontWeight = value;
		}

		/**
		 * <p> Stores the ITextLayoutFormat object that contains the attributes for this container. The controller inherits the container properties from the TextFlow of which it is part. This property allows different controllers in the same text flow to have, for example, different column settings or padding. </p>
		 * 
		 * @return 
		 */
		public function get format():ITextLayoutFormat {
			return _format;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set format(value:ITextLayoutFormat):void {
			_format = value;
		}

		/**
		 * <p> Specifies the horizontal scrolling policy, which you can set by assigning one of the constants of the ScrollPolicy class: ON, OFF, or AUTO. </p>
		 * 
		 * @return 
		 */
		public function get horizontalScrollPolicy():String {
			return _horizontalScrollPolicy;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set horizontalScrollPolicy(value:String):void {
			_horizontalScrollPolicy = value;
		}

		/**
		 * <p> Specifies the current horizontal scroll location on the stage. The value specifies the number of pixels from the left. </p>
		 * 
		 * @return 
		 */
		public function get horizontalScrollPosition():Number {
			return _horizontalScrollPosition;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set horizontalScrollPosition(value:Number):void {
			_horizontalScrollPosition = value;
		}

		/**
		 * <p> The InteractionManager associated with this TextFlow object. </p>
		 * <p>Controls all selection and editing on the text. If the TextFlow is not selectable, the interactionManager is null. To make the TextFlow editable, assign a interactionManager that is both an ISelectionManager and an IEditManager. To make a TextFlow that is read-only and allows selection, assign a interactionManager that is an ISelectionManager only. </p>
		 * 
		 * @return 
		 */
		public function get interactionManager():ISelectionManager {
			return _interactionManager;
		}

		/**
		 * <p> TextLayoutFormat: Rule used to justify text in a paragraph. Default value is <code>FormatValue.AUTO</code>, which justifies text based on the paragraph's <code>locale</code> property. For all languages except Japanese and Chinese, <code>FormatValue.AUTO</code> becomes <code>JustificationRule.SPACE</code>, which adds extra space to the space characters. For Japanese and Chinese, <code>FormatValue.AUTO</code> becomes <code>JustficationRule.EAST_ASIAN</code>. In part, justification changes the spacing of punctuation. In Roman text the comma and Japanese periods take a full character's width but in East Asian text only half of a character's width. Also, in the East Asian text the spacing between sequential punctuation marks becomes tighter, obeying traditional East Asian typographic conventions. Note, too, in the example below the leading that is applied to the second line of the paragraphs. In the East Asian version, the last two lines push left. In the Roman version, the second and following lines push left.</p>
		 * <p><img src="../../../images/textLayout_justificationrule.png" alt="justificationRule"></p>
		 * <p>Legal values are JustificationRule.EAST_ASIAN, JustificationRule.SPACE, FormatValue.AUTO, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of FormatValue.AUTO.</p>
		 * 
		 * @return 
		 */
		public function get justificationRule():* {
			return _justificationRule;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set justificationRule(value:*):void {
			_justificationRule = value;
		}

		/**
		 * <p> TextLayoutFormat: The style used for justification of the paragraph. Used only in conjunction with a <code>justificationRule</code> setting of <code>JustificationRule.EAST_ASIAN</code>. Default value of <code>FormatValue.AUTO</code> is resolved to <code>JustificationStyle.PUSH_IN_KINSOKU</code> for all locales. The constants defined by the JustificationStyle class specify options for handling kinsoku characters, which are Japanese characters that cannot appear at either the beginning or end of a line. If you want looser text, specify <code>JustificationStyle.PUSH-OUT-ONLY</code>. If you want behavior that is like what you get with the <code>justificationRule</code> of <code>JustificationRule.SPACE</code>, use <code>JustificationStyle.PRIORITIZE-LEAST-ADJUSTMENT</code>. </p>
		 * <p>Legal values are JustificationStyle.PRIORITIZE_LEAST_ADJUSTMENT, JustificationStyle.PUSH_IN_KINSOKU, JustificationStyle.PUSH_OUT_ONLY, FormatValue.AUTO, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of FormatValue.AUTO.</p>
		 * 
		 * @return 
		 */
		public function get justificationStyle():* {
			return _justificationStyle;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set justificationStyle(value:*):void {
			_justificationStyle = value;
		}

		/**
		 * <p> TextLayoutFormat: Kerning adjusts the pixels between certain character pairs to improve readability. Kerning is supported for all fonts with kerning tables. </p>
		 * <p>Legal values are Kerning.ON, Kerning.OFF, Kerning.AUTO, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of Kerning.AUTO.</p>
		 * 
		 * @return 
		 */
		public function get kerning():* {
			return _kerning;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set kerning(value:*):void {
			_kerning = value;
		}

		/**
		 * <p> TextLayoutFormat: Specifies the leading model, which is a combination of leading basis and leading direction. Leading basis is the baseline to which the <code>lineHeight</code> property refers. Leading direction determines whether the <code>lineHeight</code> property refers to the distance of a line's baseline from that of the line before it or the line after it. The default value of <code>FormatValue.AUTO</code> is resolved based on the paragraph's <code>locale</code> property. For Japanese and Chinese, it is <code>LeadingModel.IDEOGRAPHIC_TOP_DOWN</code> and for all others it is <code>LeadingModel.ROMAN_UP</code>.</p>
		 * <p><b>Leading Basis:</b></p>
		 * <p><img src="../../../images/textLayout_LB1.png" alt="leadingBasis1"> <img src="../../../images/textLayout_LB2.png" alt="leadingBasis2"> <img src="../../../images/textLayout_LB3.png" alt="leadingBasis3"></p>
		 * <p><b>Leading Direction:</b></p>
		 * <p><img src="../../../images/textLayout_LD1.png" alt="leadingDirection1"> <img src="../../../images/textLayout_LD2.png" alt="leadingDirection2"> <img src="../../../images/textLayout_LD3.png" alt="leadingDirection3"></p>
		 * <p>Legal values are LeadingModel.ROMAN_UP, LeadingModel.IDEOGRAPHIC_TOP_UP, LeadingModel.IDEOGRAPHIC_CENTER_UP, LeadingModel.IDEOGRAPHIC_TOP_DOWN, LeadingModel.IDEOGRAPHIC_CENTER_DOWN, LeadingModel.APPROXIMATE_TEXT_FIELD, LeadingModel.ASCENT_DESCENT_UP, LeadingModel.BOX, LeadingModel.AUTO, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of LeadingModel.AUTO.</p>
		 * 
		 * @return 
		 */
		public function get leadingModel():* {
			return _leadingModel;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set leadingModel(value:*):void {
			_leadingModel = value;
		}

		/**
		 * <p> TextLayoutFormat: Controls which of the ligatures that are defined in the font may be used in the text. The ligatures that appear for each of these settings is dependent on the font. A ligature occurs where two or more letter-forms are joined as a single glyph. Ligatures usually replace consecutive characters sharing common components, such as the letter pairs 'fi', 'fl', or 'ae'. They are used with both Latin and Non-Latin character sets. The ligatures enabled by the values of the LigatureLevel class - <code>MINIMUM</code>, <code>COMMON</code>, <code>UNCOMMON</code>, and <code>EXOTIC</code> - are additive. Each value enables a new set of ligatures, but also includes those of the previous types.</p>
		 * <p><b>Note: </b>When working with Arabic or Syriac fonts, <code>ligatureLevel</code> must be set to MINIMUM or above.</p>
		 * <p><img src="../../../images/textLayout_ligatures.png" alt="ligatureLevel"></p>
		 * <p>Legal values are LigatureLevel.MINIMUM, LigatureLevel.COMMON, LigatureLevel.UNCOMMON, LigatureLevel.EXOTIC, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of LigatureLevel.COMMON.</p>
		 * 
		 * @return 
		 */
		public function get ligatureLevel():* {
			return _ligatureLevel;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set ligatureLevel(value:*):void {
			_ligatureLevel = value;
		}

		/**
		 * <p> TextLayoutFormat: Controls word wrapping within the container (adopts default value if undefined during cascade). Text in the container may be set to fit the width of the container (<code>LineBreak.TO_FIT</code>), or can be set to break only at explicit return or line feed characters (<code>LineBreak.EXPLICIT</code>). </p>
		 * <p>Legal values are LineBreak.EXPLICIT, LineBreak.TO_FIT, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will have a value of LineBreak.TO_FIT.</p>
		 * 
		 * @return 
		 */
		public function get lineBreak():* {
			return _lineBreak;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set lineBreak(value:*):void {
			_lineBreak = value;
		}

		/**
		 * <p> TextLayoutFormat: Leading controls for the text. The distance from the baseline of the previous or the next line (based on <code>LeadingModel</code>) to the baseline of the current line is equal to the maximum amount of the leading applied to any character in the line. This is either a number or a percent. If specifying a percent, enter a string value, like 140%.</p>
		 * <p><img src="../../../images/textLayout_lineHeight1.jpg" alt="lineHeight1"><img src="../../../images/textLayout_lineHeight2.jpg" alt="lineHeight2"></p>
		 * <p>Legal values as a number are from -720 to 720.</p>
		 * <p>Legal values as a percent are numbers from -1000% to 1000%.</p>
		 * <p>Legal values include FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of 120%.</p>
		 * 
		 * @return 
		 */
		public function get lineHeight():* {
			return _lineHeight;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set lineHeight(value:*):void {
			_lineHeight = value;
		}

		/**
		 * <p> TextLayoutFormat: If <code>true</code>, applies strikethrough, a line drawn through the middle of the text. </p>
		 * <p>Legal values are true, false and FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of false.</p>
		 * 
		 * @return 
		 */
		public function get lineThrough():* {
			return _lineThrough;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set lineThrough(value:*):void {
			_lineThrough = value;
		}

		/**
		 * <p> TextLayoutFormat: Defines the formatting attributes used for links in normal state. This value will cascade down the hierarchy and apply to any links that are descendants. Accepts <code>inherit</code>, an <code>ITextLayoutFormat</code> or converts an array of objects with key and value as members to a TextLayoutFormat. </p>
		 * <p>Legal values include FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of null.</p>
		 * 
		 * @return 
		 */
		public function get linkActiveFormat():* {
			return _linkActiveFormat;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set linkActiveFormat(value:*):void {
			_linkActiveFormat = value;
		}

		/**
		 * <p> TextLayoutFormat: Defines the formatting attributes used for links in hover state, when the mouse is within the bounds (rolling over) a link. This value will cascade down the hierarchy and apply to any links that are descendants. Accepts <code>inherit</code>, an <code>ITextLayoutFormat</code> or converts an array of objects with key and value as members to a TextLayoutFormat. </p>
		 * <p>Legal values include FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of null.</p>
		 * 
		 * @return 
		 */
		public function get linkHoverFormat():* {
			return _linkHoverFormat;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set linkHoverFormat(value:*):void {
			_linkHoverFormat = value;
		}

		/**
		 * <p> TextLayoutFormat: Defines the formatting attributes used for links in normal state. This value will cascade down the hierarchy and apply to any links that are descendants. Accepts <code>inherit</code>, an <code>ITextLayoutFormat</code> or converts an array of objects with key and value as members to a TextLayoutFormat. </p>
		 * <p>Legal values include FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of null.</p>
		 * 
		 * @return 
		 */
		public function get linkNormalFormat():* {
			return _linkNormalFormat;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set linkNormalFormat(value:*):void {
			_linkNormalFormat = value;
		}

		/**
		 * <p> TextLayoutFormat: This specifies an auto indent for the start edge of lists when the padding value of the list on that side is <code>auto</code>. </p>
		 * <p>Legal values are numbers from -1000 to 1000 and FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of 40.</p>
		 * 
		 * @return 
		 */
		public function get listAutoPadding():* {
			return _listAutoPadding;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set listAutoPadding(value:*):void {
			_listAutoPadding = value;
		}

		/**
		 * <p> TextLayoutFormat: Defines the formatting attributes list markers. This value will cascade down the hierarchy and apply to any links that are descendants. Accepts <code>inherit</code>, an <code>IListMarkerFormat</code> or converts an array of objects with key and value as members to a ListMarkerFormat. </p>
		 * <p>Legal values include FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of null.</p>
		 * 
		 * @return 
		 */
		public function get listMarkerFormat():* {
			return _listMarkerFormat;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set listMarkerFormat(value:*):void {
			_listMarkerFormat = value;
		}

		/**
		 * <p> TextLayoutFormat: </p>
		 * <p>Legal values are ListStylePosition.INSIDE, ListStylePosition.OUTSIDE, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of ListStylePosition.OUTSIDE.</p>
		 * 
		 * @return 
		 */
		public function get listStylePosition():* {
			return _listStylePosition;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set listStylePosition(value:*):void {
			_listStylePosition = value;
		}

		/**
		 * <p> TextLayoutFormat: </p>
		 * <p>Legal values are ListStyleType.UPPER_ALPHA, ListStyleType.LOWER_ALPHA, ListStyleType.UPPER_ROMAN, ListStyleType.LOWER_ROMAN, ListStyleType.NONE, ListStyleType.DISC, ListStyleType.CIRCLE, ListStyleType.SQUARE, ListStyleType.BOX, ListStyleType.CHECK, ListStyleType.DIAMOND, ListStyleType.HYPHEN, ListStyleType.ARABIC_INDIC, ListStyleType.BENGALI, ListStyleType.DECIMAL, ListStyleType.DECIMAL_LEADING_ZERO, ListStyleType.DEVANAGARI, ListStyleType.GUJARATI, ListStyleType.GURMUKHI, ListStyleType.KANNADA, ListStyleType.PERSIAN, ListStyleType.THAI, ListStyleType.URDU, ListStyleType.CJK_EARTHLY_BRANCH, ListStyleType.CJK_HEAVENLY_STEM, ListStyleType.HANGUL, ListStyleType.HANGUL_CONSTANT, ListStyleType.HIRAGANA, ListStyleType.HIRAGANA_IROHA, ListStyleType.KATAKANA, ListStyleType.KATAKANA_IROHA, ListStyleType.LOWER_ALPHA, ListStyleType.LOWER_GREEK, ListStyleType.LOWER_LATIN, ListStyleType.UPPER_ALPHA, ListStyleType.UPPER_GREEK, ListStyleType.UPPER_LATIN, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of ListStyleType.DISC.</p>
		 * 
		 * @return 
		 */
		public function get listStyleType():* {
			return _listStyleType;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set listStyleType(value:*):void {
			_listStyleType = value;
		}

		/**
		 * <p> TextLayoutFormat: The locale of the text. Controls case transformations and shaping. Standard locale identifiers as described in Unicode Technical Standard #35 are used. For example en, en_US and en-US are all English, ja is Japanese. </p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of en.</p>
		 * 
		 * @return 
		 */
		public function get locale():* {
			return _locale;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set locale(value:*):void {
			_locale = value;
		}

		/**
		 * <p> TextLayoutFormat: Bottom inset in pixels. Default of auto is zero except in lists which get a start side padding of 45. (adopts default value if undefined during cascade). Space between the bottom edge of the container and the text. Value is a Number or auto. </p>
		 * <p> With horizontal text, in scrollable containers with multiple columns, the first and following columns will show the padding as blank space at the bottom of the container, but for the last column, if the text doesn't all fit, you may have to scroll in order to see the padding.</p>
		 * <p>Legal values as a string are FormatValue.AUTO, FormatValue.INHERIT and numbers from -8000 to 8000.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will have a value of FormatValue.AUTO.</p>
		 * 
		 * @return 
		 */
		public function get paddingBottom():* {
			return _paddingBottom;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set paddingBottom(value:*):void {
			_paddingBottom = value;
		}

		/**
		 * <p> TextLayoutFormat: Left inset in pixels. Default of auto is zero except in lists which get a start side padding of 45. (adopts default value if undefined during cascade). Space between the left edge of the container and the text. Value is a Number or auto.</p>
		 * <p> With vertical text, in scrollable containers with multiple columns, the first and following columns will show the padding as blank space at the end of the container, but for the last column, if the text doesn't all fit, you may have to scroll in order to see the padding.</p>
		 * <p>Legal values as a string are FormatValue.AUTO, FormatValue.INHERIT and numbers from -8000 to 8000.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will have a value of FormatValue.AUTO.</p>
		 * 
		 * @return 
		 */
		public function get paddingLeft():* {
			return _paddingLeft;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set paddingLeft(value:*):void {
			_paddingLeft = value;
		}

		/**
		 * <p> TextLayoutFormat: Right inset in pixels. Default of auto is zero except in lists which get a start side padding of 45. (adopts default value if undefined during cascade). Space between the right edge of the container and the text. Value is a Number or auto. </p>
		 * <p>Legal values as a string are FormatValue.AUTO, FormatValue.INHERIT and numbers from -8000 to 8000.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will have a value of FormatValue.AUTO.</p>
		 * 
		 * @return 
		 */
		public function get paddingRight():* {
			return _paddingRight;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set paddingRight(value:*):void {
			_paddingRight = value;
		}

		/**
		 * <p> TextLayoutFormat: Top inset in pixels. Default of auto is zero except in lists which get a start side padding of 45. (adopts default value if undefined during cascade). Space between the top edge of the container and the text. Value is a Number or auto. </p>
		 * <p>Legal values as a string are FormatValue.AUTO, FormatValue.INHERIT and numbers from -8000 to 8000.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will have a value of FormatValue.AUTO.</p>
		 * 
		 * @return 
		 */
		public function get paddingTop():* {
			return _paddingTop;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set paddingTop(value:*):void {
			_paddingTop = value;
		}

		/**
		 * <p> TextLayoutFormat: A Number that specifies, in pixels, the amount to indent the paragraph's end edge. Refers to the right edge in left-to-right text and the left edge in right-to-left text. </p>
		 * <p>Legal values are numbers from 0 to 8000 and FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of 0.</p>
		 * 
		 * @return 
		 */
		public function get paragraphEndIndent():* {
			return _paragraphEndIndent;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set paragraphEndIndent(value:*):void {
			_paragraphEndIndent = value;
		}

		/**
		 * <p> TextLayoutFormat: A Number that specifies the amount of space, in pixels, to leave after the paragraph. Collapses in tandem with <code>paragraphSpaceBefore</code>. </p>
		 * <p>Legal values are numbers from 0 to 8000 and FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of 0.</p>
		 * 
		 * @return 
		 */
		public function get paragraphSpaceAfter():* {
			return _paragraphSpaceAfter;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set paragraphSpaceAfter(value:*):void {
			_paragraphSpaceAfter = value;
		}

		/**
		 * <p> TextLayoutFormat: A Number that specifies the amount of space, in pixels, to leave before the paragraph. Collapses in tandem with <code>paragraphSpaceAfter</code>. </p>
		 * <p>Legal values are numbers from 0 to 8000 and FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of 0.</p>
		 * 
		 * @return 
		 */
		public function get paragraphSpaceBefore():* {
			return _paragraphSpaceBefore;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set paragraphSpaceBefore(value:*):void {
			_paragraphSpaceBefore = value;
		}

		/**
		 * <p> TextLayoutFormat: A Number that specifies, in pixels, the amount to indent the paragraph's start edge. Refers to the left edge in left-to-right text and the right edge in right-to-left text. </p>
		 * <p>Legal values are numbers from 0 to 8000 and FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of 0.</p>
		 * 
		 * @return 
		 */
		public function get paragraphStartIndent():* {
			return _paragraphStartIndent;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set paragraphStartIndent(value:*):void {
			_paragraphStartIndent = value;
		}

		/**
		 * <p> TextLayoutFormat: The rendering mode used for this text. Applies only to embedded fonts (<code>fontLookup</code> property is set to <code>FontLookup.EMBEDDED_CFF</code>). </p>
		 * <p>Legal values are RenderingMode.NORMAL, RenderingMode.CFF, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of RenderingMode.CFF.</p>
		 * 
		 * @return 
		 */
		public function get renderingMode():* {
			return _renderingMode;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set renderingMode(value:*):void {
			_renderingMode = value;
		}

		/**
		 * <p> Returns the root element that appears in the container. The root element could be a DivElement or TextFlow instance, for example. </p>
		 * 
		 * @return 
		 */
		public function get rootElement():ContainerFormattedElement {
			return _rootElement;
		}

		/**
		 * <p> TextLayoutFormat: Assigns an identifying class to the element, making it possible to set a style for the element by referencing the <code>styleName</code>. </p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will have a value of null.</p>
		 * 
		 * @return 
		 */
		public function get styleName():* {
			return _styleName;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set styleName(value:*):void {
			_styleName = value;
		}

		/**
		 * <p> Returns the styles on this ContainerController. Note that the getter makes a copy of the styles dictionary. The returned object includes all styles set in the format property including core and user styles. The returned object consists of an array of <i>stylename-value</i> pairs. </p>
		 * 
		 * @return 
		 */
		public function get styles():Object {
			return _styles;
		}

		/**
		 * <p> TextLayoutFormat: Specifies the tab stops associated with the paragraph. Setters can take an array of TabStopFormat, a condensed string representation, undefined, or <code>FormatValue.INHERIT</code>. The condensed string representation is always converted into an array of TabStopFormat. </p>
		 * <p>The string-based format is a list of tab stops, where each tab stop is delimited by one or more spaces.</p>
		 * <p>A tab stop takes the following form: &lt;alignment type&gt;&lt;alignment position&gt;|&lt;alignment token&gt;.</p>
		 * <p>The alignment type is a single character, and can be S, E, C, or D (or lower-case equivalents). S or s for start, E or e for end, C or c for center, D or d for decimal. The alignment type is optional, and if its not specified will default to S.</p>
		 * <p>The alignment position is a Number, and is specified according to FXG spec for Numbers (decimal or scientific notation). The alignment position is required.</p>
		 * <p>The vertical bar is used to separate the alignment position from the alignment token, and should only be present if the alignment token is present.</p>
		 * <p> The alignment token is optional if the alignment type is D, and should not be present if the alignment type is anything other than D. The alignment token may be any sequence of characters terminated by the space that ends the tab stop (for the last tab stop, the terminating space is optional; end of alignment token is implied). A space may be part of the alignment token if it is escaped with a backslash (\ ). A backslash may be part of the alignment token if it is escaped with another backslash (\\). If the alignment type is D, and the alignment token is not specified, it will take on the default value of null.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of null.</p>
		 * 
		 * @return 
		 */
		public function get tabStops():* {
			return _tabStops;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set tabStops(value:*):void {
			_tabStops = value;
		}

		/**
		 * <p> TextLayoutFormat: Alignment of lines in the paragraph relative to the container. <code>TextAlign.LEFT</code> aligns lines along the left edge of the container. <code>TextAlign.RIGHT</code> aligns on the right edge. <code>TextAlign.CENTER</code> positions the line equidistant from the left and right edges. <code>TextAlign.JUSTIFY</code> spreads the lines out so they fill the space. <code>TextAlign.START</code> is equivalent to setting left in left-to-right text, or right in right-to-left text. <code>TextAlign.END</code> is equivalent to setting right in left-to-right text, or left in right-to-left text. </p>
		 * <p>Legal values are TextAlign.LEFT, TextAlign.RIGHT, TextAlign.CENTER, TextAlign.JUSTIFY, TextAlign.START, TextAlign.END, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of TextAlign.START.</p>
		 * 
		 * @return 
		 */
		public function get textAlign():* {
			return _textAlign;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set textAlign(value:*):void {
			_textAlign = value;
		}

		/**
		 * <p> TextLayoutFormat: Alignment of the last (or only) line in the paragraph relative to the container in justified text. If <code>textAlign</code> is set to <code>TextAlign.JUSTIFY</code>, <code>textAlignLast</code> specifies how the last line (or only line, if this is a one line block) is aligned. Values are similar to <code>textAlign</code>. </p>
		 * <p>Legal values are TextAlign.LEFT, TextAlign.RIGHT, TextAlign.CENTER, TextAlign.JUSTIFY, TextAlign.START, TextAlign.END, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of TextAlign.START.</p>
		 * 
		 * @return 
		 */
		public function get textAlignLast():* {
			return _textAlignLast;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set textAlignLast(value:*):void {
			_textAlignLast = value;
		}

		/**
		 * <p> TextLayoutFormat: Alpha (transparency) value for the text. A value of 0 is fully transparent, and a value of 1 is fully opaque. Display objects with <code>textAlpha</code> set to 0 are active, even though they are invisible. </p>
		 * <p>Legal values are numbers from 0 to 1 and FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of 1.</p>
		 * 
		 * @return 
		 */
		public function get textAlpha():* {
			return _textAlpha;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set textAlpha(value:*):void {
			_textAlpha = value;
		}

		/**
		 * <p> TextLayoutFormat: Decoration on text. Use to apply underlining; default is none. </p>
		 * <p>Legal values are TextDecoration.NONE, TextDecoration.UNDERLINE, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of TextDecoration.NONE.</p>
		 * 
		 * @return 
		 */
		public function get textDecoration():* {
			return _textDecoration;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set textDecoration(value:*):void {
			_textDecoration = value;
		}

		/**
		 * <p> Returns the TextFlow object whose content appears in the container. Either the <code>textFlow</code> and <code>rootElement</code> values are the same, or this is the root element's TextFlow object. For example, if the container's root element is a DivElement, the value would be the TextFlow object to which the DivElement belongs. </p>
		 * 
		 * @return 
		 */
		public function get textFlow():TextFlow {
			return _textFlow;
		}

		/**
		 * <p> TextLayoutFormat: A Number that specifies, in pixels, the amount to indent the first line of the paragraph. A negative indent will push the line into the margin, and possibly out of the container. </p>
		 * <p>Legal values are numbers from -8000 to 8000 and FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of 0.</p>
		 * 
		 * @return 
		 */
		public function get textIndent():* {
			return _textIndent;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set textIndent(value:*):void {
			_textIndent = value;
		}

		/**
		 * <p> TextLayoutFormat: Specifies options for justifying text. Default value is <code>TextJustify.INTER_WORD</code>, meaning that extra space is added to the space characters. <code>TextJustify.DISTRIBUTE</code> adds extra space to space characters and between individual letters. Used only in conjunction with a <code>justificationRule</code> value of <code>JustificationRule.SPACE</code>. </p>
		 * <p>Legal values are TextJustify.INTER_WORD, TextJustify.DISTRIBUTE, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of TextJustify.INTER_WORD.</p>
		 * 
		 * @return 
		 */
		public function get textJustify():* {
			return _textJustify;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set textJustify(value:*):void {
			_textJustify = value;
		}

		/**
		 * <p> Returns the total number of characters in the container. This can include text that is not currently in view, if the container is scrollable. This value is updated when the text is composed (when the IFlowComposer's <code>compose()</code> or <code>updateAllControllers()</code> methods are called). </p>
		 * 
		 * @return 
		 */
		public function get textLength():int {
			return _textLength;
		}

		/**
		 * <p> TextLayoutFormat: Determines the number of degrees to rotate this text. </p>
		 * <p>Legal values are TextRotation.ROTATE_0, TextRotation.ROTATE_180, TextRotation.ROTATE_270, TextRotation.ROTATE_90, TextRotation.AUTO, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of TextRotation.AUTO.</p>
		 * 
		 * @return 
		 */
		public function get textRotation():* {
			return _textRotation;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set textRotation(value:*):void {
			_textRotation = value;
		}

		/**
		 * <p> TextLayoutFormat: Number in pixels (or percent of <code>fontSize</code>, like 120%) indicating the amount of tracking (manual kerning) to be applied to the left of each character. If kerning is enabled, the <code>trackingLeft</code> value is added to the values in the kerning table for the font. If kerning is disabled, the <code>trackingLeft</code> value is used as a manual kerning value. Supports both positive and negative values. </p>
		 * <p>Legal values as a number are from -1000 to 1000.</p>
		 * <p>Legal values as a percent are numbers from -1000% to 1000%.</p>
		 * <p>Legal values include FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of 0.</p>
		 * 
		 * @return 
		 */
		public function get trackingLeft():* {
			return _trackingLeft;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set trackingLeft(value:*):void {
			_trackingLeft = value;
		}

		/**
		 * <p> TextLayoutFormat: Number in pixels (or percent of <code>fontSize</code>, like 120%) indicating the amount of tracking (manual kerning) to be applied to the right of each character. If kerning is enabled, the <code>trackingRight</code> value is added to the values in the kerning table for the font. If kerning is disabled, the <code>trackingRight</code> value is used as a manual kerning value. Supports both positive and negative values. </p>
		 * <p>Legal values as a number are from -1000 to 1000.</p>
		 * <p>Legal values as a percent are numbers from -1000% to 1000%.</p>
		 * <p>Legal values include FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of 0.</p>
		 * 
		 * @return 
		 */
		public function get trackingRight():* {
			return _trackingRight;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set trackingRight(value:*):void {
			_trackingRight = value;
		}

		/**
		 * <p> TextLayoutFormat: The type of typographic case used for this text. Here are some examples:</p>
		 * <p><img src="../../../images/textLayout_typographiccase.png" alt="typographicCase"></p>
		 * <p>Legal values are TLFTypographicCase.DEFAULT, TLFTypographicCase.CAPS_TO_SMALL_CAPS, TLFTypographicCase.UPPERCASE, TLFTypographicCase.LOWERCASE, TLFTypographicCase.LOWERCASE_TO_SMALL_CAPS, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of TLFTypographicCase.DEFAULT.</p>
		 * 
		 * @return 
		 */
		public function get typographicCase():* {
			return _typographicCase;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set typographicCase(value:*):void {
			_typographicCase = value;
		}

		/**
		 * <p> Allows you to read and write user styles on a ContainerController object. Note that reading this property makes a copy of the userStyles set in the format of this element. </p>
		 * 
		 * @return 
		 */
		public function get userStyles():Object {
			return _userStyles;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set userStyles(value:Object):void {
			_userStyles = value;
		}

		/**
		 * <p> TextLayoutFormat: Vertical alignment or justification (adopts default value if undefined during cascade). Determines how TextFlow elements align within the container. </p>
		 * <p>Legal values are VerticalAlign.TOP, VerticalAlign.MIDDLE, VerticalAlign.BOTTOM, VerticalAlign.JUSTIFY, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will have a value of VerticalAlign.TOP.</p>
		 * 
		 * @return 
		 */
		public function get verticalAlign():* {
			return _verticalAlign;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set verticalAlign(value:*):void {
			_verticalAlign = value;
		}

		/**
		 * <p> Specifies the vertical scrolling policy, which you can set by assigning one of the constants of the ScrollPolicy class: ON, OFF, or, AUTO. </p>
		 * 
		 * @return 
		 */
		public function get verticalScrollPolicy():String {
			return _verticalScrollPolicy;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set verticalScrollPolicy(value:String):void {
			_verticalScrollPolicy = value;
		}

		/**
		 * <p> Specifies the current vertical scroll location on the stage. The value specifies the number of pixels from the top. </p>
		 * 
		 * @return 
		 */
		public function get verticalScrollPosition():Number {
			return _verticalScrollPosition;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set verticalScrollPosition(value:Number):void {
			_verticalScrollPosition = value;
		}

		/**
		 * <p> TextLayoutFormat: Collapses or preserves whitespace when importing text into a TextFlow. <code>WhiteSpaceCollapse.PRESERVE</code> retains all whitespace characters. <code>WhiteSpaceCollapse.COLLAPSE</code> removes newlines, tabs, and leading or trailing spaces within a block of imported text. Line break tags () and Unicode line separator characters are retained. </p>
		 * <p>Legal values are WhiteSpaceCollapse.PRESERVE, WhiteSpaceCollapse.COLLAPSE, FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of WhiteSpaceCollapse.COLLAPSE.</p>
		 * 
		 * @return 
		 */
		public function get whiteSpaceCollapse():* {
			return _whiteSpaceCollapse;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set whiteSpaceCollapse(value:*):void {
			_whiteSpaceCollapse = value;
		}

		/**
		 * <p> TextLayoutFormat: Specifies the optimum, minimum, and maximum spacing (as a multiplier of the width of a normal space) between words to use during justification. The optimum space is used to indicate the desired size of a space, as a fraction of the value defined in the font. The minimum and maximum values are the used when textJustify is distribute to determine how wide or narrow the spaces between the words may grow before letter spacing is used to justify the line. </p>
		 * <p>Legal values as a percent are numbers from -1000% to 1000%.</p>
		 * <p>Legal values include FormatValue.INHERIT.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined during the cascade this property will inherit its value from an ancestor. If no ancestor has set this property, it will have a value of 100%, 50%, 150%.</p>
		 * 
		 * @return 
		 */
		public function get wordSpacing():* {
			return _wordSpacing;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set wordSpacing(value:*):void {
			_wordSpacing = value;
		}

		/**
		 * <p> Processes the <code>Event.ACTIVATE</code> event when the client manages events. </p>
		 * 
		 * @param event  — The Event object. 
		 */
		public function activateHandler(event:flash.events.Event):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Handle a scroll event during a "drag" selection. </p>
		 * 
		 * @param mouseX  — The horizontal position of the mouse cursor on the stage. 
		 * @param mouseY  — The vertical position of the mouse cursor on the stage. 
		 */
		public function autoScrollIfNecessary(mouseX:int, mouseY:int):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Called to request clients to begin the forwarding of mouseup and mousemove events from outside a security sandbox. </p>
		 */
		public function beginMouseCapture():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Clears the style specified by <code>styleProp</code> from this FlowElement. Sets the value to <code>undefined</code>. </p>
		 * 
		 * @param styleProp
		 */
		public function clearStyle(styleProp:String):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes the <code>Event.DEACTIVATE</code> event when the client manages events. </p>
		 * 
		 * @param event  — The Event object. 
		 */
		public function deactivateHandler(event:flash.events.Event):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes an edit event (CUT, COPY, PASTE, SELECT_ALL) when the client manages events. </p>
		 * 
		 * @param event  — Event object. 
		 */
		public function editHandler(event:flash.events.Event):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Called to inform clients that the the forwarding of mouseup and mousemove events from outside a security sandbox is no longer needed. </p>
		 */
		public function endMouseCapture():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes the <code>FocusEvent.KEY_FOCUS_CHANGE</code> and <code>FocusEvent.MOUSE_FOCUS_CHANGE</code> events when the client manages events. </p>
		 * 
		 * @param event  — The FocusEvent object. 
		 */
		public function focusChangeHandler(event:FocusEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes the <code>FocusEvent.FOCUS_IN</code> event when the client manages events. </p>
		 * 
		 * @param event  — The FocusEvent object. 
		 */
		public function focusInHandler(event:FocusEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes the <code>FocusEvent.FOCUS_OUT</code> event when the client manages events. </p>
		 * 
		 * @param event  — The FocusEvent object. 
		 */
		public function focusOutHandler(event:FocusEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the area that the text occupies, as reflected by the last compose or update operation. The width and the height might be estimated, if the container is scrollable and the text exceeds the visible area. </p>
		 * 
		 * @return  — describes the area that the text occupies. 
		 */
		public function getContentBounds():Rectangle {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Figure out the scroll distance required to scroll up or down by the specified number of lines. Negative numbers scroll upward, bringing more of the top of the TextFlow into view. Positive numbers scroll downward, bringing the next line from the bottom into full view. </p>
		 * <p>When scrolling up, for example, the method makes the next line fully visible. If the next line is partially obscured and the number of lines specified is 1, the partially obscured line becomes fully visible.</p>
		 * 
		 * @param numLines  — The number of lines to scroll. 
		 * @return  — the delta amount of space to scroll 
		 */
		public function getScrollDelta(numLines:int):Number {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the value of the style specified by the <code>styleProp</code> parameter. </p>
		 * 
		 * @param styleProp  — The name of the style property whose value you want. 
		 * @return  — The current value for the specified style. 
		 */
		public function getStyle(styleProp:String):* {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes the <code>IMEEvent.IME_START_COMPOSITION</code> event when the client manages events. </p>
		 * 
		 * @param event  — The IMEEvent object. 
		 */
		public function imeStartCompositionHandler(event:IMEEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Marks all the text in this container as needing composing. </p>
		 * <span id="pageFilter"><br><span class="label"> Example &nbsp;( <span class="usage"><a href="http://www.adobe.com/go/learn_as3_usingexamples_en"> How to use this example </a></span>) </span><br><br>
		 *  <div class="detailBody">
		 *    This example checks whether invalidateContents() causes the text flow to be damaged. 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>
		 * 
		 * package flashx.textLayout.container.examples 
		 * {
		 *     import flash.display.Sprite;
		 *     import flashx.textLayout.container.ContainerController;
		 *     import flashx.textLayout.elements.ParagraphElement;
		 *     import flashx.textLayout.elements.SpanElement;
		 *     import flashx.textLayout.elements.TextFlow;
		 *     import flashx.textLayout.conversion.TextConverter;
		 *     
		 *     public class ContainerController_invalidateContentsExample extends Sprite
		 *     {
		 *         public function ContainerController_invalidateContentsExample()
		 *         {
		 *             // create container, text flow, composer, paragraph, and span
		 *             var container:Sprite = new Sprite();
		 *             var textFlow:TextFlow = new TextFlow();
		 *             var paragraph:ParagraphElement = new ParagraphElement();
		 *             var span:SpanElement = new SpanElement;
		 *             // add container to the stage; create controller and add it to the text flow
		 *             addChild(container);
		 *             container.x = 100;
		 *             container.y = 50;
		 *             var controller:ContainerController = new ContainerController(container, 200, 200);
		 *             textFlow.flowComposer.addController(controller);
		 *             // set font size and add text to the span; add the span to the paragraph
		 *             textFlow.fontSize = 16;
		 *             span.text = "Does invalidateContents() cause this text to be damaged?";
		 *             paragraph.addChild(span);
		 *             textFlow.addChild(paragraph);  
		 *             //update controller to display text
		 *             textFlow.flowComposer.updateAllControllers(); 
		 *             // invalidate the controller contents and check to see if they are damaged
		 *             controller.invalidateContents();
		 *             if(controller.isDamaged())
		 *                 span.text += "\nYes, it does.";
		 *             else
		 *                 span.text += "\nNo, it doesn't";
		 *             textFlow.flowComposer.updateAllControllers();   
		 *         }
		 *     }
		 * }
		 * </pre>
		 *   </div>
		 *  </div></span>
		 */
		public function invalidateContents():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Determines whether the container has text that requires composing. </p>
		 * 
		 * @return  — true if the container requires composing. 
		 */
		public function isDamaged():Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes the <code>KeyboardEvent.KEY_DOWN</code> event when the client manages events. </p>
		 * 
		 * @param event  — KeyboardEvent object. 
		 */
		public function keyDownHandler(event:KeyboardEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes the <code>FocusEvent.KEY_FOCUS_CHANGE</code> event when the client manages events. </p>
		 * 
		 * @param event  — The FocusEvent object. 
		 */
		public function keyFocusChangeHandler(event:FocusEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes the <code>Keyboard.KEY_UP</code> event when the client manages events. </p>
		 * 
		 * @param event  — The KeyboardEvent object. 
		 */
		public function keyUpHandler(event:KeyboardEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes the <code>ContextMenuEvent.MENU_SELECT</code> event when the client manages events. </p>
		 * 
		 * @param event  — ContextMenuEvent object. 
		 */
		public function menuSelectHandler(event:ContextMenuEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes the <code>MouseEvent.DOUBLE_CLICK</code> event when the client manages events. </p>
		 * 
		 * @param event  — The MouseEvent object. 
		 */
		public function mouseDoubleClickHandler(event:MouseEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes the <code>MouseEvent.MOUSE_DOWN</code> event when the client manages events. </p>
		 * 
		 * @param event  — The MouseEvent object. 
		 */
		public function mouseDownHandler(event:MouseEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes the <code>MouseEvent.MOUSE_MOVE</code> event when the client manages events. </p>
		 * 
		 * @param event  — The MouseEvent object. 
		 */
		public function mouseMoveHandler(event:MouseEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Client call to forward a mouseMove event from outside a security sandbox. Coordinates of the mouse move are not needed. </p>
		 * 
		 * @param event
		 */
		public function mouseMoveSomewhere(event:flash.events.Event):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes the <code>MouseEvent.MOUSE_OUT</code> event when the client manages events. </p>
		 * 
		 * @param event  — The MouseEvent object. 
		 */
		public function mouseOutHandler(event:MouseEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes the <code>MouseEvent.MOUSE_OVER</code> event when the client manages events. </p>
		 * 
		 * @param event  — The MouseEvent object. 
		 */
		public function mouseOverHandler(event:MouseEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes the <code>MouseEvent.MOUSE_UP</code> event when the client manages events. </p>
		 * 
		 * @param event  — The MouseEvent object. 
		 */
		public function mouseUpHandler(event:MouseEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Client call to forward a mouseUp event from outside a security sandbox. Coordinates of the mouse up are not needed. </p>
		 * 
		 * @param event
		 */
		public function mouseUpSomewhere(event:flash.events.Event):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes the <code>MouseEvent.MOUSE_WHEEL</code> event when the client manages events. </p>
		 * 
		 * @param event  — The MouseEvent object. 
		 */
		public function mouseWheelHandler(event:MouseEvent):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Scrolls so that the text range is visible in the container. </p>
		 * 
		 * @param activePosition  — The end of the selection that is changed when you extend the selection. It can be either the start or the end of the selection, expressed as an offset from the start of the text flow. 
		 * @param anchorPosition  — The stable end of the selection when you extend the selection. It can be either the start or the end of the selection. 
		 */
		public function scrollToRange(activePosition:int, anchorPosition:int):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets the range of selected text in a component implementing ITextSupport. If either of the arguments is out of bounds the selection should not be changed. Components which wish to support inline IME should call into this method. </p>
		 * 
		 * @param anchorIndex  — The zero-based index value of the character at the anchor end of the selection 
		 * @param activeIndex  — The zero-based index value of the character at the active end of the selection. 
		 */
		public function selectRange(anchorIndex:int, activeIndex:int):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets the width and height allowed for text in the container. Width and height can be specified in pixels or <code>NaN</code> can be used for either value. <code>NaN</code> indicates measure that value. This can be used to find the widest line and/or the total height of all the content. When NaN is specified as the width lines are broken with a maximum width of <code>TextLine.MAX_LINE_WIDTH</code>. When <code>NaN</code> is specified as the height the container is assumed to have unlimited height. The actual measured values can be ready back in <code>getContentBounds</code>. When the computed <code>blockProgression</code> property of <code>TextFlow</code> is <code>BlockProgression.RL</code> the meanings of width and height are exchanged. </p>
		 * 
		 * @param w  — The width in pixels that's available for text in the container. <code>NaN</code> indicates no specified width. 
		 * @param h  — The height in pixels that's available for text in the container. <code>NaN</code> indicates no specified height. 
		 */
		public function setCompositionSize(w:Number, h:Number):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets the value of the style specified by the <code>styleProp</code> parameter to the value specified by the <code>newValue</code> parameter. </p>
		 * 
		 * @param styleProp  — The name of the style property whose value you want to set. 
		 * @param newValue  — The value that you want to assign to the style. 
		 */
		public function setStyle(styleProp:String, newValue:*):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes the <code>SoftKeyboardEvent.SOFT_KEYBOARD_ACTIVATING</code> event when the client manages events. </p>
		 * 
		 * @param event  — The SoftKeyboardEvent object. 
		 */
		public function softKeyboardActivatingHandler(event:flash.events.Event):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Processes the <code>TextEvent.TEXT_INPUT</code> event when the client manages events. </p>
		 * 
		 * @param event  — The TextEvent object. 
		 */
		public function textInputHandler(event:TextEvent):void {
			throw new Error("Not implemented");
		}


		/**
		 * @return 
		 */
		public static function get containerControllerInitialFormat():ITextLayoutFormat {
			return _containerControllerInitialFormat;
		}


		/**
		 * @param value
		 * @return 
		 */
		public static function set containerControllerInitialFormat(value:ITextLayoutFormat):void {
			_containerControllerInitialFormat = value;
		}
	}
}
