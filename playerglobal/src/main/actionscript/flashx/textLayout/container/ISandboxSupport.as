package flashx.textLayout.container {
	import flash.events.Event;

	/**
	 *  Interface to support TLF content in a sub-application. When an application is loaded in an untrusted context, mouse events that occur outside of the untrusted application's bounds are not delivered. Clients can handle this by implementing ISandboxSupport. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="ContainerController.html" target="">flashx.textLayout.container.ContainerController</a>
	 *  <br>
	 *  <a href="TextContainerManager.html" target="">flashx.textLayout.container.TextContainerManager</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/edit/SelectionManager.html" target="">flashx.textLayout.edit.SelectionManager</a>
	 *  <br>
	 *  <a href="../../../flash/system/SecurityDomain.html" target="">flash.system.SecurityDomain</a>
	 * </div><br><hr>
	 */
	public interface ISandboxSupport {

		/**
		 * <p> Called to request clients to begin the forwarding of mouseup and mousemove events from outside a security sandbox. </p>
		 */
		function beginMouseCapture():void;

		/**
		 * <p> Called to inform clients that the the forwarding of mouseup and mousemove events from outside a security sandbox is no longer needed. </p>
		 */
		function endMouseCapture():void;

		/**
		 * <p> Client call to forward a mouseMove event from outside a security sandbox. Coordinates of the mouse move are not needed. </p>
		 * 
		 * @param event
		 */
		function mouseMoveSomewhere(event:flash.events.Event):void;

		/**
		 * <p> Client call to forward a mouseUp event from outside a security sandbox. Coordinates of the mouse up are not needed. </p>
		 * 
		 * @param event
		 */
		function mouseUpSomewhere(event:flash.events.Event):void;
	}
}
