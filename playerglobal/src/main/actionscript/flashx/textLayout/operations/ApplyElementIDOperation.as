package flashx.textLayout.operations {
	import flashx.textLayout.edit.SelectionState;
	import flashx.textLayout.elements.FlowElement;

	/**
	 *  The ChangeElementIDOperation class encapsulates an element ID change. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flashx/textLayout/elements/FlowElement.html" target="">flashx.textLayout.elements.FlowElement</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/edit/EditManager.html" target="">flashx.textLayout.edit.EditManager</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/events/FlowOperationEvent.html" target="">flashx.textLayout.events.FlowOperationEvent</a>
	 * </div><br><hr>
	 */
	public class ApplyElementIDOperation extends FlowElementOperation {
		private var _newID:String;

		/**
		 * <p> Creates a ChangeElementIDOperation object. </p>
		 * <p>If the <code>relativeStart</code> or <code>relativeEnd</code> parameters are set, then the existing element is split into two elements, one using the existing ID and the other using the new ID. If both parameters are set, then the existing element is split into three elements. The first and last elements of the set are both assigned the original ID.</p>
		 * 
		 * @param operationState  — Specifies the selection state before the operation 
		 * @param targetElement  — Specifies the element to change 
		 * @param newID  — The ID to assign 
		 * @param relativeStart  — An offset from the beginning of the target element. 
		 * @param relativeEnd  — An offset from the end of the target element. 
		 */
		public function ApplyElementIDOperation(operationState:SelectionState, targetElement:FlowElement, newID:String, relativeStart:int = 0, relativeEnd:int = -1) {
			super(operationState, targetElement, relativeStart, relativeEnd);
			throw new Error("Not implemented");
		}

		/**
		 * <p> The ID assigned by this operation. </p>
		 * 
		 * @return 
		 */
		public function get newID():String {
			return _newID;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set newID(value:String):void {
			_newID = value;
		}
	}
}
