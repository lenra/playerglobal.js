package flashx.textLayout.operations {
	/**
	 *  The RedoOperation class encapsulates a redo operation. <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flashx/textLayout/edit/EditManager.html" target="">flashx.textLayout.edit.EditManager</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/events/FlowOperationEvent.html" target="">flashx.textLayout.events.FlowOperationEvent</a>
	 * </div><br><hr>
	 */
	public class RedoOperation extends FlowOperation {
		private var _operation:FlowOperation;

		/**
		 * <p> Creates a RedoOperation object. </p>
		 * 
		 * @param operation  — The operation to redo. 
		 */
		public function RedoOperation(operation:FlowOperation) {
			super(textFlow);
			throw new Error("Not implemented");
		}

		/**
		 * <p> The operation to redo. </p>
		 * 
		 * @return 
		 */
		public function get operation():FlowOperation {
			return _operation;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set operation(value:FlowOperation):void {
			_operation = value;
		}
	}
}
