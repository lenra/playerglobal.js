package flashx.textLayout.operations {
	import flashx.textLayout.edit.SelectionState;
	import flashx.textLayout.elements.DivElement;
	import flashx.textLayout.elements.FlowGroupElement;
	import flashx.textLayout.formats.ITextLayoutFormat;

	/**
	 *  The CreateDivOperation class encapsulates creating DivElement <br><hr>
	 */
	public class CreateDivOperation extends FlowTextOperation {
		private var _format:ITextLayoutFormat;
		private var _parent:FlowGroupElement;

		private var _newDivElement:DivElement;

		/**
		 * <p> Creates an CreateDivOperation object. </p>
		 * 
		 * @param operationState
		 * @param parent
		 * @param format
		 */
		public function CreateDivOperation(operationState:SelectionState, parent:FlowGroupElement = null, format:ITextLayoutFormat = null) {
			super(operationState);
			throw new Error("Not implemented");
		}

		/**
		 * <p> TextLayoutFormat to be applied to the new DivElement. </p>
		 * 
		 * @return 
		 */
		public function get format():ITextLayoutFormat {
			return _format;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set format(value:ITextLayoutFormat):void {
			_format = value;
		}

		/**
		 * <p> The new DivElement. </p>
		 * 
		 * @return 
		 */
		public function get newDivElement():DivElement {
			return _newDivElement;
		}

		/**
		 * <p> Specifies the parent element for the new DivElement </p>
		 * 
		 * @return 
		 */
		public function get parent():FlowGroupElement {
			return _parent;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set parent(value:FlowGroupElement):void {
			_parent = value;
		}
	}
}
