package flashx.textLayout.operations {
	import flashx.textLayout.edit.SelectionState;
	import flashx.textLayout.formats.ITextLayoutFormat;

	/**
	 *  The InsertTextOperation class encapsulates a text insertion operation. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flashx/textLayout/edit/EditManager.html" target="">flashx.textLayout.edit.EditManager</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/events/FlowOperationEvent.html" target="">flashx.textLayout.events.FlowOperationEvent</a>
	 * </div><br><hr>
	 */
	public class InsertTextOperation extends FlowTextOperation {
		private var _characterFormat:ITextLayoutFormat;
		private var _deleteSelectionState:SelectionState;
		private var _text:String;

		/**
		 * <p> Creates an InsertTextOperation object. </p>
		 * 
		 * @param operationState  — Describes the insertion point or range of text. 
		 * @param text  — The string to insert. 
		 * @param deleteSelectionState  — Describes the range of text to delete before doing insertion, if different than the range described by <code>operationState</code>. 
		 */
		public function InsertTextOperation(operationState:SelectionState, text:String, deleteSelectionState:SelectionState = null) {
			super(operationState);
			throw new Error("Not implemented");
		}

		/**
		 * <p> The character format applied to the inserted text. </p>
		 * 
		 * @return 
		 */
		public function get characterFormat():ITextLayoutFormat {
			return _characterFormat;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set characterFormat(value:ITextLayoutFormat):void {
			_characterFormat = value;
		}

		/**
		 * <p> The text deleted by this operation, if any. </p>
		 * <p><code>null</code> if no text is deleted.</p>
		 * 
		 * @return 
		 */
		public function get deleteSelectionState():SelectionState {
			return _deleteSelectionState;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set deleteSelectionState(value:SelectionState):void {
			_deleteSelectionState = value;
		}

		/**
		 * <p> The text inserted by this operation. </p>
		 * 
		 * @return 
		 */
		public function get text():String {
			return _text;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set text(value:String):void {
			_text = value;
		}

		/**
		 * <p> Re-executes the operation after it has been undone. </p>
		 * <p>This function is called by the edit manager, when necessary.</p>
		 * 
		 * @return 
		 */
		override public function redo():SelectionState {
			throw new Error("Not implemented");
		}
	}
}
