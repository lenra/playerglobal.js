package flashx.textLayout.operations {
	import flashx.textLayout.edit.SelectionState;
	import flashx.textLayout.elements.FlowElement;
	import flashx.textLayout.formats.ITextLayoutFormat;

	/**
	 *  The ApplyFormatToElementOperation class encapsulates a style change to an element. <p>This operation applies one or more formats to a flow element.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flashx/textLayout/edit/EditManager.html" target="">flashx.textLayout.edit.EditManager</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/formats/TextLayoutFormat.html" target="">flashx.textLayout.formats.TextLayoutFormat</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/events/FlowOperationEvent.html" target="">flashx.textLayout.events.FlowOperationEvent</a>
	 * </div><br><hr>
	 */
	public class ApplyFormatToElementOperation extends FlowElementOperation {
		private var _format:ITextLayoutFormat;

		/**
		 * <p> Creates an ApplyFormatToElementOperation object. </p>
		 * 
		 * @param operationState  — Specifies the text flow containing the element to which this operation is applied. 
		 * @param targetElement  — specifies the element to which this operation is applied. 
		 * @param format  — The formats to apply in this operation. 
		 * @param relativeStart
		 * @param relativeEnd
		 */
		public function ApplyFormatToElementOperation(operationState:SelectionState, targetElement:FlowElement, format:ITextLayoutFormat, relativeStart:int = 0, relativeEnd:int = -1) {
			super(operationState, targetElement, relativeStart, relativeEnd);
			throw new Error("Not implemented");
		}

		/**
		 * <p> The character formats applied in this operation. </p>
		 * <p>If <code>null</code> no character formats are changed.</p>
		 * 
		 * @return 
		 */
		public function get format():ITextLayoutFormat {
			return _format;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set format(value:ITextLayoutFormat):void {
			_format = value;
		}
	}
}
