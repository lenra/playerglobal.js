package flashx.textLayout.operations {
	import flashx.textLayout.edit.SelectionState;
	import flashx.textLayout.elements.FlowGroupElement;

	/**
	 *  The SplitElementOperation class encapsulates a change that splits any FlowGroupElement into two elements. This operation splits target at operationState.absoluteStart. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flashx/textLayout/elements/ParagraphElement.html" target="">flashx.textLayout.elements.ParagraphElement</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/edit/EditManager.html" target="">flashx.textLayout.edit.EditManager</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/events/FlowOperationEvent.html" target="">flashx.textLayout.events.FlowOperationEvent</a>
	 * </div><br><hr>
	 */
	public class SplitElementOperation extends FlowTextOperation {
		private var _targetElement:FlowGroupElement;

		private var _newElement:FlowGroupElement;

		/**
		 * <p> Creates a SplitElementOperation object. This operation deletes a block selection and then splits the target at absoluteStart. The block selection should not cause target to be deleted. Target is a FlowGroupElement but may not be a LinkElement, TCYElement or SubParagraphGroupElement. </p>
		 * 
		 * @param operationState  — Describes the point at which to split the element. If a range of text is specified, the contents of the range are deleted. 
		 * @param targetElement
		 */
		public function SplitElementOperation(operationState:SelectionState, targetElement:FlowGroupElement) {
			super(operationState);
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the new element created by doOperation. </p>
		 * 
		 * @return 
		 */
		public function get newElement():FlowGroupElement {
			return _newElement;
		}

		/**
		 * <p> Specifies the element this operation modifies. </p>
		 * 
		 * @return 
		 */
		public function get targetElement():FlowGroupElement {
			return _targetElement;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set targetElement(value:FlowGroupElement):void {
			_targetElement = value;
		}
	}
}
