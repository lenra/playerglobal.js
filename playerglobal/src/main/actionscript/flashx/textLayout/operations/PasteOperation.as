package flashx.textLayout.operations {
	import flashx.textLayout.edit.SelectionState;
	import flashx.textLayout.edit.TextScrap;

	/**
	 *  The PasteOperation class encapsulates a paste operation. <p>The specified range is replaced by the new content.</p> <p> <b>Note:</b> The edit manager is responsible for copying the contents of the clipboard.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flashx/textLayout/edit/EditManager.html" target="">flashx.textLayout.edit.EditManager</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/events/FlowOperationEvent.html" target="">flashx.textLayout.events.FlowOperationEvent</a>
	 * </div><br><hr>
	 */
	public class PasteOperation extends FlowTextOperation {
		private var _textScrap:TextScrap;

		/**
		 * <p> Creates a PasteOperation object. </p>
		 * 
		 * @param operationState  — Describes the insertion point or a range of text to replace. 
		 * @param textScrap  — The content to paste into the text flow. 
		 */
		public function PasteOperation(operationState:SelectionState, textScrap:TextScrap) {
			super(operationState);
			throw new Error("Not implemented");
		}

		/**
		 * <p> textScrap the text being pasted </p>
		 * 
		 * @return 
		 */
		public function get textScrap():TextScrap {
			return _textScrap;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set textScrap(value:TextScrap):void {
			_textScrap = value;
		}
	}
}
