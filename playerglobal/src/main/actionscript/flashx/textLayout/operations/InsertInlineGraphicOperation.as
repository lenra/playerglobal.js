package flashx.textLayout.operations {
	import flashx.textLayout.edit.SelectionState;
	import flashx.textLayout.elements.InlineGraphicElement;

	/**
	 *  The InsertInlineGraphicOperation class encapsulates the insertion of an inline graphic into a text flow. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flashx/textLayout/elements/InlineGraphicElement.html" target="">flashx.textLayout.elements.InlineGraphicElement</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/edit/EditManager.html" target="">flashx.textLayout.edit.EditManager</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/events/FlowOperationEvent.html" target="">flashx.textLayout.events.FlowOperationEvent</a>
	 * </div><br><hr>
	 */
	public class InsertInlineGraphicOperation extends FlowTextOperation {
		private var _height:Object;
		private var _options:Object;
		private var _source:Object;
		private var _width:Object;

		private var _newInlineGraphicElement:InlineGraphicElement;

		/**
		 * <p> Creates an InsertInlineGraphicsOperation object. </p>
		 * 
		 * @param operationState  — Describes the insertion point. If a range is selected, the operation deletes the contents of that range. 
		 * @param source  — The graphic source (uri string, URLRequest, DisplayObject, or Class of an embedded asset). 
		 * @param width  — The width to assign (number of pixels, percent, or the string 'auto') 
		 * @param height  — The height to assign (number of pixels, percent, or the string 'auto') 
		 * @param options  — The float to assign (String value, none for inline with text, left/right/start/end for float) 
		 */
		public function InsertInlineGraphicOperation(operationState:SelectionState, source:Object, width:Object, height:Object, options:Object = null) {
			super(operationState);
			throw new Error("Not implemented");
		}

		/**
		 * <p> The height of the image. May be 'auto', a number of pixels or a percent of the measured height. </p>
		 * <p>Legal values are flashx.textLayout.formats.FormatValue.AUTO and flashx.textLayout.formats.FormatValue.INHERIT.</p>
		 * <p>Legal values as a number are from 0 to 32000.</p>
		 * <p>Legal values as a percent are numbers from 0 to 1000000.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined or "inherit" the InlineGraphicElement will use the default value of "auto".</p>
		 * 
		 * @return 
		 */
		public function get height():Object {
			return _height;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set height(value:Object):void {
			_height = value;
		}

		/**
		 * <p> The InlineGraphicElement that was created by doOperation. </p>
		 * 
		 * @return 
		 */
		public function get newInlineGraphicElement():InlineGraphicElement {
			return _newInlineGraphicElement;
		}

		/**
		 * <p> Controls the placement of the graphic relative to the text. It can be part of the line, or can be beside the line with the text wrapped around it. </p>
		 * <p>Legal values are <code>flashx.textLayout.formats.Float.NONE</code>, <code>flashx.textLayout.formats.Float.LEFT</code>, <code>flashx.textLayout.formats.Float.RIGHT</code>, <code>flashx.textLayout.formats.Float.START</code>, and <code>flashx.textLayout.formats.Float.END</code>.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined will be treated as <code>Float.NONE</code>.</p>
		 * 
		 * @return 
		 */
		public function get options():Object {
			return _options;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set options(value:Object):void {
			_options = value;
		}

		/**
		 * <p> Sets the source for the graphic. The value can be either a String that is interpreted as a URI, a Class that's interpreted as the class of an embeddded DisplayObject, a DisplayObject instance, or a URLRequest. Creates a DisplayObject and, if the InlineGraphicElement object is added into a ParagraphElement in a TextFlow object, causes it to appear inline in the text. </p>
		 * 
		 * @return 
		 */
		public function get source():Object {
			return _source;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set source(value:Object):void {
			_source = value;
		}

		/**
		 * <p> The width of the graphic. The value can be 'auto', a number of pixels or a percent of the measured width of the image. </p>
		 * <p>Legal values are flashx.textLayout.formats.FormatValue.AUTO and flashx.textLayout.formats.FormatValue.INHERIT.</p>
		 * <p>Legal values as a number are from 0 to 32000.</p>
		 * <p>Legal values as a percent are numbers from 0 to 1000000.</p>
		 * <p>Default value is undefined indicating not set.</p>
		 * <p>If undefined or "inherit" the InlineGraphicElement will use the default value of "auto".</p>
		 * 
		 * @return 
		 */
		public function get width():Object {
			return _width;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set width(value:Object):void {
			_width = value;
		}

		/**
		 * <p> Re-executes the operation after it has been undone. </p>
		 * <p>This function is called by the edit manager, when necessary.</p>
		 * 
		 * @return 
		 */
		override public function redo():SelectionState {
			throw new Error("Not implemented");
		}
	}
}
