package flashx.textLayout.operations {
	import flashx.textLayout.edit.SelectionState;
	import flashx.textLayout.edit.TextScrap;

	/**
	 *  The CutOperation class encapsulates a cut operation. <p>The specified range is removed from the text flow.</p> <p> <b>Note:</b> The edit manager is responsible for copying the text scrap to the clipboard. Undoing a cut operation does not restore the original clipboard state.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flashx/textLayout/edit/EditManager.html" target="">flashx.textLayout.edit.EditManager</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/events/FlowOperationEvent.html" target="">flashx.textLayout.events.FlowOperationEvent</a>
	 * </div><br><hr>
	 */
	public class CutOperation extends FlowTextOperation {
		private var _scrapToCut:TextScrap;

		/**
		 * <p> Creates a CutOperation object. </p>
		 * 
		 * @param operationState  — The range of text to be cut. 
		 * @param scrapToCut  — A copy of the deleted text. 
		 */
		public function CutOperation(operationState:SelectionState, scrapToCut:TextScrap) {
			super(operationState);
			throw new Error("Not implemented");
		}

		/**
		 * <p> scrapToCut the original removed text </p>
		 * 
		 * @return 
		 */
		public function get scrapToCut():TextScrap {
			return _scrapToCut;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set scrapToCut(value:TextScrap):void {
			_scrapToCut = value;
		}
	}
}
