package flashx.textLayout.operations {
	import flashx.textLayout.edit.SelectionState;

	/**
	 *  The SplitParagraphOperation class encapsulates a change that splits a paragraph into two elements. <p>The operation creates a new paragraph containing the text from the specified position to the end of the paragraph. If a range of text is specified, the text in the range is deleted first.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flashx/textLayout/elements/ParagraphElement.html" target="">flashx.textLayout.elements.ParagraphElement</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/edit/EditManager.html" target="">flashx.textLayout.edit.EditManager</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/events/FlowOperationEvent.html" target="">flashx.textLayout.events.FlowOperationEvent</a>
	 * </div><br><hr>
	 */
	public class SplitParagraphOperation extends SplitElementOperation {

		/**
		 * <p> Creates a SplitParagraphOperation object. </p>
		 * 
		 * @param operationState  — Describes the point at which to split the paragraph. If a range of text is specified, the contents of the range are deleted. 
		 */
		public function SplitParagraphOperation(operationState:SelectionState) {
			super(operationState, targetElement);
			throw new Error("Not implemented");
		}
	}
}
