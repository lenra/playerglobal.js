package flashx.textLayout.operations {
	import flashx.textLayout.edit.SelectionState;

	/**
	 *  The FlowTextOperation is the base class for operations that transform a range of text. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flashx/textLayout/edit/EditManager.html" target="">flashx.textLayout.edit.EditManager</a>
	 * </div><br><hr>
	 */
	public class FlowTextOperation extends FlowOperation {
		private var _absoluteEnd:int;
		private var _absoluteStart:int;
		private var _originalSelectionState:SelectionState;

		/**
		 * <p> Creates the FlowTextOperation object. </p>
		 * 
		 * @param operationState  — Specifies the relevant selection. If relevant to the operation, the <code>operationState</code> describes the text range to which this operation applies. Otherwise, <code>operationState</code> is used to save the current selection state so that it can be restored when the operation is undone. 
		 */
		public function FlowTextOperation(operationState:SelectionState) {
			super(textFlow);
			throw new Error("Not implemented");
		}

		/**
		 * <p> The absolute end point of the range of text to which this operation is applied. </p>
		 * 
		 * @return 
		 */
		public function get absoluteEnd():int {
			return _absoluteEnd;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set absoluteEnd(value:int):void {
			_absoluteEnd = value;
		}

		/**
		 * <p> The absolute start point of the range of text to which this operation is applied. </p>
		 * 
		 * @return 
		 */
		public function get absoluteStart():int {
			return _absoluteStart;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set absoluteStart(value:int):void {
			_absoluteStart = value;
		}

		/**
		 * <p> The selection state at the start of the operation. </p>
		 * 
		 * @return 
		 */
		public function get originalSelectionState():SelectionState {
			return _originalSelectionState;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set originalSelectionState(value:SelectionState):void {
			_originalSelectionState = value;
		}

		/**
		 * <p> Re-executes the operation. </p>
		 * <p>This method must be overridden in derived classes. The base class method does nothing. You should not call <code>redo()</code> directly. The edit manager calls the method when it re-executes the operation. </p>
		 * 
		 * @return  — The SelectionState object passed to the operation when it was performed. This SelectionState object can be the current selection or a selection created specifically for the operation. 
		 */
		override public function redo():SelectionState {
			throw new Error("Not implemented");
		}
	}
}
