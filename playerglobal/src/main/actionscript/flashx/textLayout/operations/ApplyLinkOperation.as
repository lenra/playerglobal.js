package flashx.textLayout.operations {
	import flashx.textLayout.edit.SelectionState;
	import flashx.textLayout.elements.LinkElement;

	/**
	 *  The ApplyLinkOperation class encapsulates a link creation or modification operation. <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flashx/textLayout/elements/LinkElement.html" target="">flashx.textLayout.elements.LinkElement</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/edit/EditManager.html" target="">flashx.textLayout.edit.EditManager</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/events/FlowOperationEvent.html" target="">flashx.textLayout.events.FlowOperationEvent</a>
	 * </div><br><hr>
	 */
	public class ApplyLinkOperation extends FlowTextOperation {
		private var _extendToLinkBoundary:Boolean;
		private var _href:String;
		private var _target:String;

		private var _newLinkElement:LinkElement;

		/**
		 * <p> Creates an ApplyLinkOperation object. </p>
		 * 
		 * @param operationState  — The text range to which the operation is applied. 
		 * @param href  — The URI to be associated with the link. If href is an empty string, the URI of links in the selection are removed. 
		 * @param target  — The target of the link. 
		 * @param extendToLinkBoundary  — Whether to extend the selection to include the entire text of any existing links overlapped by the selection, and then apply the change. 
		 */
		public function ApplyLinkOperation(operationState:SelectionState, href:String, target:String, extendToLinkBoundary:Boolean) {
			super(operationState);
			throw new Error("Not implemented");
		}

		/**
		 * <p> Whether to extend the selection to include the entire text of any existing links overlapped by the selection, and then apply the change. </p>
		 * 
		 * @return 
		 */
		public function get extendToLinkBoundary():Boolean {
			return _extendToLinkBoundary;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set extendToLinkBoundary(value:Boolean):void {
			_extendToLinkBoundary = value;
		}

		/**
		 * <p> The URI to be associated with the link. If href is an empty string, the URI of links in the selection are removed. </p>
		 * 
		 * @return 
		 */
		public function get href():String {
			return _href;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set href(value:String):void {
			_href = value;
		}

		/**
		 * <p> The LinkElement that was created by doOperation. </p>
		 * 
		 * @return 
		 */
		public function get newLinkElement():LinkElement {
			return _newLinkElement;
		}

		/**
		 * <p> The target of the link. </p>
		 * 
		 * @return 
		 */
		public function get target():String {
			return _target;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set target(value:String):void {
			_target = value;
		}
	}
}
