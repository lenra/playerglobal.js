package flashx.textLayout.operations {
	import flashx.textLayout.edit.SelectionState;
	import flashx.textLayout.elements.TextFlow;
	import flashx.undo.IOperation;

	/**
	 *  The FlowOperation class is the base class for all Text Layout Framework operations. <p>Operations are transformations of a text flow. An Operation class defines the logic for performing and undoing the transformation. Operations are executed by an edit manager. Most applications do not need to create or manage operations directly (unless implementing a custom edit manager).</p> <p>When an operation is performed, the edit manager dispatches an Operation object within the FlowOperationEvent object. You can query this Operation object to decide whether or not to allow the operation, to decide whether to perform some other operation as well, or to update related user-interface elements.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flashx/textLayout/events/FlowOperationEvent.html" target="">flashx.textLayout.events.FlowOperationEvent</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/edit/EditManager.html" target="">flashx.textLayout.edit.EditManager</a>
	 * </div><br><hr>
	 */
	public class FlowOperation implements IOperation {
		private var _textFlow:TextFlow;
		private var _userData:*;

		private var _beginGeneration:uint;
		private var _endGeneration:uint;

		/**
		 * <p> Creates the FlowOperation object. </p>
		 * 
		 * @param textFlow  — The text flow to which this operation is applied. 
		 */
		public function FlowOperation(textFlow:TextFlow) {
			throw new Error("Not implemented");
		}

		/**
		 * <p> The text flow generation before the operation. </p>
		 * <p>A generation of 0 indicates that the operation did not complete.</p>
		 * 
		 * @return 
		 */
		public function get beginGeneration():uint {
			return _beginGeneration;
		}

		/**
		 * <p> The text flow generation after the operation. </p>
		 * <p>A generation of 0 indicates that the operation did not complete.</p>
		 * 
		 * @return 
		 */
		public function get endGeneration():uint {
			return _endGeneration;
		}

		/**
		 * <p> The TextFlow object to which this operation is applied. </p>
		 * 
		 * @return 
		 */
		public function get textFlow():TextFlow {
			return _textFlow;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set textFlow(value:TextFlow):void {
			_textFlow = value;
		}

		/**
		 * <p> Arbitrary data associated with an element. </p>
		 * 
		 * @return 
		 */
		public function get userData():* {
			return _userData;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set userData(value:*):void {
			_userData = value;
		}

		/**
		 * <p> Test if this operation be placed on the undo stack. </p>
		 * 
		 * @return  — true means to push the operation onto the undo stack. false means do not push this operation. 
		 */
		public function canUndo():Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Executes the operation. </p>
		 * <p>This method must be overridden in derived classes. The base class method does nothing. You should not call <code>doOperation()</code> directly. The edit manager calls the method when it executes the operation. </p>
		 * 
		 * @return  — Boolean , if the operation succeeded. Otherwise, . 
		 */
		public function doOperation():Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Re-executes the operation. </p>
		 * <p>This method must be overridden in derived classes. The base class method does nothing. You should not call <code>redo()</code> directly. The edit manager calls the method when it re-executes the operation. </p>
		 * 
		 * @return  — The SelectionState object passed to the operation when it was performed. This SelectionState object can be the current selection or a selection created specifically for the operation. 
		 */
		public function redo():SelectionState {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Reverses the operation. </p>
		 * <p>This method must be overridden in derived classes. The base class method does nothing. You should not call <code>undo()</code> directly. The edit manager calls the method when it reverses the operation. </p>
		 * 
		 * @return  — The SelectionState object passed to the operation when it was performed. This SelectionState object can be the current selection or a selection created specifically for the operation. 
		 */
		public function undo():SelectionState {
			throw new Error("Not implemented");
		}
	}
}
