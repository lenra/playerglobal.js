package flashx.textLayout.operations {
	import flashx.textLayout.edit.SelectionState;
	import flashx.textLayout.formats.ITextLayoutFormat;

	/**
	 *  The ClearFormatOperation class encapsulates a way to undefine formats. <p>An UndefineFormatOperation undefines properties set in the leaf format to the text in the specified range (no change is made if the specified range is a single point). It undefines properties set in the paragraph format to any paragraphs at least partially within the range (or a single paragraph if the range is a single point). And it undefines properties set in the container format to any containers at least partially within the range (or a single container if the range is a single point).</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flashx/textLayout/edit/EditManager.html" target="">flashx.textLayout.edit.EditManager</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/events/FlowOperationEvent.html" target="">flashx.textLayout.events.FlowOperationEvent</a>
	 * </div><br><hr>
	 */
	public class ClearFormatOperation extends FlowTextOperation {
		private var _containerFormat:ITextLayoutFormat;
		private var _leafFormat:ITextLayoutFormat;
		private var _paragraphFormat:ITextLayoutFormat;

		/**
		 * <p> Creates an ClearFormatOperation object. </p>
		 * 
		 * @param operationState  — Defines the text range to which the format is applied. 
		 * @param leafFormat  — The format whose set values indicate properties to undefine to LeafFlowElement objects in the selected range. 
		 * @param paragraphFormat  — The format whose set values indicate properties to undefine to ParagraphElement objects in the selected range. 
		 * @param containerFormat  — The format whose set values indicate properties to undefine to ContainerController objects in the selected range. 
		 */
		public function ClearFormatOperation(operationState:SelectionState, leafFormat:ITextLayoutFormat, paragraphFormat:ITextLayoutFormat, containerFormat:ITextLayoutFormat = null) {
			super(operationState);
			throw new Error("Not implemented");
		}

		/**
		 * <p> The format properties to undefine on the containers in the range. </p>
		 * <p>The formats of any containers at least partially within the range are updated. If the range of this operation is a point, then a single container is updated. If <code>containerFormat</code> is <code>null</code>, then no container formats are changed.</p>
		 * 
		 * @return 
		 */
		public function get containerFormat():ITextLayoutFormat {
			return _containerFormat;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set containerFormat(value:ITextLayoutFormat):void {
			_containerFormat = value;
		}

		/**
		 * <p> The format properties to undefine on the leaf elements in the range. </p>
		 * <p>If the range of this operation is a point, or if <code>leafFormat</code> is <code>null</code>, then no leaf element formats are changed.</p>
		 * 
		 * @return 
		 */
		public function get leafFormat():ITextLayoutFormat {
			return _leafFormat;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set leafFormat(value:ITextLayoutFormat):void {
			_leafFormat = value;
		}

		/**
		 * <p> The format properties to undefine on the paragraphs in the range. </p>
		 * <p>The formats of any paragraphs at least partially within the range are updated. If the range of this operation is a point, then a single paragraph is updated. If <code>paragraphFormat</code> is <code>null</code>, then no paragraph formats are changed.</p>
		 * 
		 * @return 
		 */
		public function get paragraphFormat():ITextLayoutFormat {
			return _paragraphFormat;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set paragraphFormat(value:ITextLayoutFormat):void {
			_paragraphFormat = value;
		}

		/**
		 * @return 
		 */
		override public function redo():SelectionState {
			throw new Error("Not implemented");
		}
	}
}
