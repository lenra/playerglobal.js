package flashx.textLayout.operations {
	/**
	 *  The CompositeOperation class encapsulates a group of transformations managed as a unit. <p>The CompositeOperation class provides a grouping mechanism for combining multiple FlowOperations into a single atomic operation. Grouping operations allows them to be undone and redone as a unit. For example, several single character inserts followed by several backspaces can be undone together as if they were a single operation. Grouping also provides a mechanism for representing complex operations. For example, a replace operation that modifies more than one text ranges can be represented and managed as a single composite operation.</p> <p> <b>Note:</b> It can be more efficient to merge individual atomic operations rather than to combine separate operations into a group. For example, several sequential character inserts can easily be represented as a single insert operation, and undoing or redoing that single operation is more efficient than undoing or redoing a group of insert operations.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flashx/textLayout/edit/EditManager.html" target="">flashx.textLayout.edit.EditManager</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/events/FlowOperationEvent.html" target="">flashx.textLayout.events.FlowOperationEvent</a>
	 * </div><br><hr>
	 */
	public class CompositeOperation extends FlowOperation {
		private var _operations:Array;

		/**
		 * <p> Creates a CompositeOperation object. </p>
		 * 
		 * @param operations  — The operations to group. 
		 */
		public function CompositeOperation(operations:Array = null) {
			super(textFlow);
			throw new Error("Not implemented");
		}

		/**
		 * <p> An array containing the operations grouped by this composite operation. </p>
		 * 
		 * @return 
		 */
		public function get operations():Array {
			return _operations;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set operations(value:Array):void {
			_operations = value;
		}

		/**
		 * <p> Adds an additional operation to the end of the list. </p>
		 * <p>The new operation must operate on the same TextFlow object as the other operations in the list.</p>
		 * 
		 * @param operation
		 */
		public function addOperation(operation:FlowOperation):void {
			throw new Error("Not implemented");
		}
	}
}
