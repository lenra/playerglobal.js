package flashx.textLayout.operations {
	/**
	 *  The UndoOperation class encapsulates an undo operation. <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flashx/textLayout/edit/EditManager.html" target="">flashx.textLayout.edit.EditManager</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/events/FlowOperationEvent.html" target="">flashx.textLayout.events.FlowOperationEvent</a>
	 * </div><br><hr>
	 */
	public class UndoOperation extends FlowOperation {
		private var _operation:FlowOperation;

		/**
		 * <p> Creates an UndoOperation object. </p>
		 * 
		 * @param op  — The operation to undo. 
		 */
		public function UndoOperation(op:FlowOperation) {
			super(textFlow);
			throw new Error("Not implemented");
		}

		/**
		 * <p> The operation to undo. </p>
		 * 
		 * @return 
		 */
		public function get operation():FlowOperation {
			return _operation;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set operation(value:FlowOperation):void {
			_operation = value;
		}
	}
}
