package flashx.textLayout.operations {
	import flashx.textLayout.edit.SelectionState;
	import flashx.textLayout.elements.FlowElement;

	/**
	 *  The ApplyElementUserStyleOperation class encapsulates a change in a style value of an element. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flashx/textLayout/elements/FlowElement.html#userStyles" target="">flashx.textLayout.elements.FlowElement.userStyles</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/edit/EditManager.html" target="">flashx.textLayout.edit.EditManager</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/events/FlowOperationEvent.html" target="">flashx.textLayout.events.FlowOperationEvent</a>
	 * </div><br><hr>
	 */
	public class ApplyElementUserStyleOperation extends FlowElementOperation {
		private var _newValue:*;
		private var _styleName:String;

		/**
		 * <p> Creates a ApplyElementUserStyleOperation object. </p>
		 * <p>If the <code>relativeStart</code> and <code>relativeEnd</code> parameters are set, then the existing element is split into multiple elements, the selected portion using the new style value and the rest using the existing style value.</p>
		 * 
		 * @param operationState  — Describes the range of text to style. 
		 * @param targetElement  — Specifies the element to change. 
		 * @param styleName  — The name of the style to change. 
		 * @param value  — The new style value. 
		 * @param relativeStart  — An offset from the beginning of the target element. 
		 * @param relativeEnd  — An offset from the end of the target element. 
		 */
		public function ApplyElementUserStyleOperation(operationState:SelectionState, targetElement:FlowElement, styleName:String, value:*, relativeStart:int = 0, relativeEnd:int = -1) {
			super(operationState, targetElement, relativeStart, relativeEnd);
			throw new Error("Not implemented");
		}

		/**
		 * <p> The new style value. </p>
		 * 
		 * @return 
		 */
		public function get newValue():* {
			return _newValue;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set newValue(value:*):void {
			_newValue = value;
		}

		/**
		 * <p> The name of the style changed. </p>
		 * 
		 * @return 
		 */
		public function get styleName():String {
			return _styleName;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set styleName(value:String):void {
			_styleName = value;
		}
	}
}
