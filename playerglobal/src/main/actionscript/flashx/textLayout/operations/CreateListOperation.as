package flashx.textLayout.operations {
	import flashx.textLayout.edit.SelectionState;
	import flashx.textLayout.elements.FlowGroupElement;
	import flashx.textLayout.elements.ListElement;
	import flashx.textLayout.formats.ITextLayoutFormat;

	/**
	 *  The CreateListOperation class encapsulates creating list <br><hr>
	 */
	public class CreateListOperation extends FlowTextOperation {
		private var _listFormat:ITextLayoutFormat;
		private var _parent:FlowGroupElement;

		private var _newListElement:ListElement;

		/**
		 * <p> Creates an CreateListOperation object. </p>
		 * 
		 * @param operationState
		 * @param parent
		 * @param listFormat
		 */
		public function CreateListOperation(operationState:SelectionState, parent:FlowGroupElement = null, listFormat:ITextLayoutFormat = null) {
			super(operationState);
			throw new Error("Not implemented");
		}

		/**
		 * <p> TextLayoutFormat to be applied to the new ListElement. </p>
		 * 
		 * @return 
		 */
		public function get listFormat():ITextLayoutFormat {
			return _listFormat;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set listFormat(value:ITextLayoutFormat):void {
			_listFormat = value;
		}

		/**
		 * <p> The new ListElement. </p>
		 * 
		 * @return 
		 */
		public function get newListElement():ListElement {
			return _newListElement;
		}

		/**
		 * <p> Specifies the element this operation adds a new ListElement to. </p>
		 * 
		 * @return 
		 */
		public function get parent():FlowGroupElement {
			return _parent;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set parent(value:FlowGroupElement):void {
			_parent = value;
		}
	}
}
