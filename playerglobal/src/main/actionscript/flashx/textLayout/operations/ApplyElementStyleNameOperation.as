package flashx.textLayout.operations {
	import flashx.textLayout.edit.SelectionState;
	import flashx.textLayout.elements.FlowElement;

	/**
	 *  The ApplyElementStyleNameOperation class encapsulates a style name change. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flashx/textLayout/elements/FlowElement.html#styleName" target="">flashx.textLayout.elements.FlowElement.styleName</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/edit/EditManager.html" target="">flashx.textLayout.edit.EditManager</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/events/FlowOperationEvent.html" target="">flashx.textLayout.events.FlowOperationEvent</a>
	 * </div><br><hr>
	 */
	public class ApplyElementStyleNameOperation extends FlowElementOperation {
		private var _newStyleName:String;

		/**
		 * <p> Creates a ApplyElementStyleNameOperation object. </p>
		 * <p>If the <code>relativeStart</code> and <code>relativeEnd</code> parameters are set, then the existing element is split into multiple elements, the selected portion using the new style name and the rest using the existing style name.</p>
		 * 
		 * @param operationState  — Describes the current selection. 
		 * @param targetElement  — Specifies the element to change. 
		 * @param newStyleName  — The style name to assign. 
		 * @param relativeStart  — An offset from the beginning of the target element. 
		 * @param relativeEnd  — An offset from the end of the target element. 
		 */
		public function ApplyElementStyleNameOperation(operationState:SelectionState, targetElement:FlowElement, newStyleName:String, relativeStart:int = 0, relativeEnd:int = -1) {
			super(operationState, targetElement, relativeStart, relativeEnd);
			throw new Error("Not implemented");
		}

		/**
		 * <p> The style name assigned by this operation. </p>
		 * 
		 * @return 
		 */
		public function get newStyleName():String {
			return _newStyleName;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set newStyleName(value:String):void {
			_newStyleName = value;
		}
	}
}
