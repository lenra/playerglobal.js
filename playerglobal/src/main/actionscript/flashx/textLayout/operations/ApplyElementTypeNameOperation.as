package flashx.textLayout.operations {
	import flashx.textLayout.edit.SelectionState;
	import flashx.textLayout.elements.FlowElement;

	/**
	 *  The ApplyElementTypeNameOperation class encapsulates a type name change. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flashx/textLayout/elements/FlowElement.html#typeName" target="">flashx.textLayout.elements.FlowElement.typeName</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/edit/EditManager.html" target="">flashx.textLayout.edit.EditManager</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/events/FlowOperationEvent.html" target="">flashx.textLayout.events.FlowOperationEvent</a>
	 * </div><br><hr>
	 */
	public class ApplyElementTypeNameOperation extends FlowElementOperation {
		private var _typeName:String;

		/**
		 * <p> Creates a ApplyElementTypeNameOperation object. </p>
		 * <p>If the <code>relativeStart</code> and <code>relativeEnd</code> parameters are set, then the existing element is split into multiple elements, the selected portion using the new type name and the rest using the existing type name.</p>
		 * 
		 * @param operationState  — Describes the current selection. 
		 * @param targetElement  — Specifies the element to change. 
		 * @param typeName  — The type name to assign. 
		 * @param relativeStart  — An offset from the beginning of the target element. 
		 * @param relativeEnd  — An offset from the end of the target element. 
		 */
		public function ApplyElementTypeNameOperation(operationState:SelectionState, targetElement:FlowElement, typeName:String, relativeStart:int = 0, relativeEnd:int = -1) {
			super(operationState, targetElement, relativeStart, relativeEnd);
			throw new Error("Not implemented");
		}

		/**
		 * <p> The type name assigned by this operation. </p>
		 * 
		 * @return 
		 */
		public function get typeName():String {
			return _typeName;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set typeName(value:String):void {
			_typeName = value;
		}
	}
}
