package flashx.textLayout.operations {
	import flashx.textLayout.edit.SelectionState;
	import flashx.textLayout.elements.TCYElement;

	/**
	 *  The ApplyTCYOperation class encapsulates a TCY transformation. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flashx/textLayout/elements/TCYElement.html" target="">flashx.textLayout.elements.TCYElement</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/edit/EditManager.html" target="">flashx.textLayout.edit.EditManager</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/events/FlowOperationEvent.html" target="">flashx.textLayout.events.FlowOperationEvent</a>
	 * </div><br><hr>
	 */
	public class ApplyTCYOperation extends FlowTextOperation {
		private var _tcyOn:Boolean;

		private var _newTCYElement:TCYElement;

		/**
		 * <p> Creates an ApplyTCYOperation object. </p>
		 * 
		 * @param operationState  — Describes the range of text to which the operation is applied. 
		 * @param tcyOn  — Specifies whether to apply TCY (<code>true</code>), or remove TCY (<code>false</code>). 
		 */
		public function ApplyTCYOperation(operationState:SelectionState, tcyOn:Boolean) {
			super(operationState);
			throw new Error("Not implemented");
		}

		/**
		 * <p> The TCYElement that was created by doOperation. </p>
		 * 
		 * @return 
		 */
		public function get newTCYElement():TCYElement {
			return _newTCYElement;
		}

		/**
		 * <p> Indicates whether the operation applies or removes TCY formatting. </p>
		 * <p>If <code>true</code>, then the operation transforms the range into a TCY element. If <code>false</code>, then the operation removes TCY formatting from the first TCY element in the range.</p>
		 * 
		 * @return 
		 */
		public function get tcyOn():Boolean {
			return _tcyOn;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set tcyOn(value:Boolean):void {
			_tcyOn = value;
		}
	}
}
