package flashx.textLayout.operations {
	import flashx.textLayout.edit.SelectionState;
	import flashx.textLayout.elements.FlowGroupElement;
	import flashx.textLayout.elements.SubParagraphGroupElement;
	import flashx.textLayout.formats.ITextLayoutFormat;

	/**
	 *  The CreateSPGEOperation class encapsulates creating a SubPargraphGroupElement <br><hr>
	 */
	public class CreateSubParagraphGroupOperation extends FlowTextOperation {
		private var _format:ITextLayoutFormat;
		private var _parent:FlowGroupElement;

		private var _newSubParagraphGroupElement:SubParagraphGroupElement;

		/**
		 * <p> Constructor. This operation creates a single SubParagraphGroupElement in the first paragraph of the selection range. That paragraph must have at least one character selected the paragraph terminator does not count towards that selection. Specifying the spgeParent creates an SubParagraphGroupElement int he part of the selection range included by that spgeParent. </p>
		 * 
		 * @param operationState  — selection over which to apply the operation. 
		 * @param parent  — optional parent for the spge element. If not specified one is chosen based on the selection 
		 * @param format  — optional format to set in the new spge element. 
		 */
		public function CreateSubParagraphGroupOperation(operationState:SelectionState, parent:FlowGroupElement = null, format:ITextLayoutFormat = null) {
			super(operationState);
			throw new Error("Not implemented");
		}

		/**
		 * <p> Format to be applied to the new SubParagraphGroupElement </p>
		 * 
		 * @return 
		 */
		public function get format():ITextLayoutFormat {
			return _format;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set format(value:ITextLayoutFormat):void {
			_format = value;
		}

		/**
		 * <p> The new SubParagraphGroupElement. </p>
		 * 
		 * @return 
		 */
		public function get newSubParagraphGroupElement():SubParagraphGroupElement {
			return _newSubParagraphGroupElement;
		}

		/**
		 * <p> Specifies the element this operation modifies. </p>
		 * 
		 * @return 
		 */
		public function get parent():FlowGroupElement {
			return _parent;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set parent(value:FlowGroupElement):void {
			_parent = value;
		}
	}
}
