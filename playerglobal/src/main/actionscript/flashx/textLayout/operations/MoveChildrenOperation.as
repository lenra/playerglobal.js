package flashx.textLayout.operations {
	import flashx.textLayout.edit.SelectionState;
	import flashx.textLayout.elements.FlowGroupElement;

	/**
	 *  The MoveChildrenOperation class allows moving a set of siblings out of its immediate parent chain, and the operation removes any empty ancestor chain left behind. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flashx/textLayout/elements/FlowElement.html" target="">flashx.textLayout.elements.FlowElement</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/edit/EditManager.html" target="">flashx.textLayout.edit.EditManager</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/events/FlowOperationEvent.html" target="">flashx.textLayout.events.FlowOperationEvent</a>
	 * </div><br><hr>
	 */
	public class MoveChildrenOperation extends FlowTextOperation {
		private var _destination:FlowGroupElement;
		private var _destinationIndex:int;
		private var _numChildren:int;
		private var _source:FlowGroupElement;
		private var _sourceIndex:int;

		/**
		 * <p> Creates a MoveChildrenOperation object. </p>
		 * <p>This operation moves a consecutive number of children of source into the destination context. Also, if moving the children leaves the source element with no children, then source will be removed. The removal is done recursively such that if source's parent becomes empty from the removal of source, it too will be deleted, and on up the parent chain.</p>
		 * 
		 * @param operationState  — Specifies the SelectionState of this operation 
		 * @param source  — Specifies the parent of the item(s) to move. 
		 * @param sourceIndex  — Specifies the index of the first item to move. 
		 * @param numChildren  — Specifies the number of children to move. 
		 * @param destination  — Specifies the new parent of the items. 
		 * @param destinationIndex  — Specifies the new child index of the first element. 
		 */
		public function MoveChildrenOperation(operationState:SelectionState, source:FlowGroupElement, sourceIndex:int, numChildren:int, destination:FlowGroupElement, destinationIndex:int) {
			super(operationState);
			throw new Error("Not implemented");
		}

		/**
		 * <p> Specifies the new parent of the items. </p>
		 * 
		 * @return 
		 */
		public function get destination():FlowGroupElement {
			return _destination;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set destination(value:FlowGroupElement):void {
			_destination = value;
		}

		/**
		 * <p> Specifies the new child index of the first element. </p>
		 * 
		 * @return 
		 */
		public function get destinationIndex():int {
			return _destinationIndex;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set destinationIndex(value:int):void {
			_destinationIndex = value;
		}

		/**
		 * <p> Specifies the index of the first item to move. </p>
		 * 
		 * @return 
		 */
		public function get numChildren():int {
			return _numChildren;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set numChildren(value:int):void {
			_numChildren = value;
		}

		/**
		 * <p> Specifies the parent of the item(s) to move. </p>
		 * 
		 * @return 
		 */
		public function get source():FlowGroupElement {
			return _source;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set source(value:FlowGroupElement):void {
			_source = value;
		}

		/**
		 * <p> Specifies the number of children to move. </p>
		 * 
		 * @return 
		 */
		public function get sourceIndex():int {
			return _sourceIndex;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set sourceIndex(value:int):void {
			_sourceIndex = value;
		}
	}
}
