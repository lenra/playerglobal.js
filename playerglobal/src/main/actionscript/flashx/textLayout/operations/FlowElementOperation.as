package flashx.textLayout.operations {
	import flashx.textLayout.edit.SelectionState;
	import flashx.textLayout.elements.FlowElement;

	/**
	 *  The FlowElementOperation class is the base class for operations that transform a FlowElement. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flashx/textLayout/formats/TextLayoutFormat.html" target="">flashx.textLayout.formats.TextLayoutFormat</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/edit/EditManager.html" target="">flashx.textLayout.edit.EditManager</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/events/FlowOperationEvent.html" target="">flashx.textLayout.events.FlowOperationEvent</a>
	 * </div><br><hr>
	 */
	public class FlowElementOperation extends FlowTextOperation {
		private var _relativeEnd:int;
		private var _relativeStart:int;
		private var _targetElement:FlowElement;

		/**
		 * <p> Creates a FlowElementOperation object. </p>
		 * 
		 * @param operationState  — Specifies the TextFlow object this operation acts upon. 
		 * @param targetElement  — Specifies the element this operation modifies. 
		 * @param relativeStart  — An offset from the beginning of the <code>targetElement</code>. 
		 * @param relativeEnd  — An offset from the end of the <code>targetElement</code>. 
		 */
		public function FlowElementOperation(operationState:SelectionState, targetElement:FlowElement, relativeStart:int = 0, relativeEnd:int = -1) {
			super(operationState);
			throw new Error("Not implemented");
		}

		/**
		 * <p> An offset from the start of the <code>targetElement</code>. </p>
		 * 
		 * @return 
		 */
		public function get relativeEnd():int {
			return _relativeEnd;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set relativeEnd(value:int):void {
			_relativeEnd = value;
		}

		/**
		 * <p> An offset from the beginning of the <code>targetElement</code>. </p>
		 * 
		 * @return 
		 */
		public function get relativeStart():int {
			return _relativeStart;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set relativeStart(value:int):void {
			_relativeStart = value;
		}

		/**
		 * <p> Specifies the element this operation modifies. </p>
		 * 
		 * @return 
		 */
		public function get targetElement():FlowElement {
			return _targetElement;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set targetElement(value:FlowElement):void {
			_targetElement = value;
		}
	}
}
