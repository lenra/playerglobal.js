package flashx.textLayout.operations {
	import flashx.textLayout.edit.SelectionState;

	/**
	 *  The CopyOperation class encapsulates a copy operation. <p> <b>Note:</b> The operation is responsible for copying the text scrap to the clipboard. Undonig a copy operation does not restore the original clipboard state.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flashx/textLayout/edit/EditManager.html" target="">flashx.textLayout.edit.EditManager</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/events/FlowOperationEvent.html" target="">flashx.textLayout.events.FlowOperationEvent</a>
	 * </div><br><hr>
	 */
	public class CopyOperation extends FlowTextOperation {

		/**
		 * <p> Creates a CopyOperation object. </p>
		 * 
		 * @param operationState  — The range of text to be copied. 
		 */
		public function CopyOperation(operationState:SelectionState) {
			super(operationState);
			throw new Error("Not implemented");
		}
	}
}
