package flashx.textLayout.operations {
	import flashx.textLayout.edit.SelectionState;

	/**
	 *  The DeleteTextOperation class encapsulates the deletion of a range of text. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flashx/textLayout/edit/EditManager.html" target="">flashx.textLayout.edit.EditManager</a>
	 *  <br>
	 *  <a href="../../../flashx/textLayout/events/FlowOperationEvent.html" target="">flashx.textLayout.events.FlowOperationEvent</a>
	 * </div><br><hr>
	 */
	public class DeleteTextOperation extends FlowTextOperation {
		private var _allowMerge:Boolean;
		private var _deleteSelectionState:SelectionState;

		/**
		 * <p> Creates a DeleteTextOperation operation. </p>
		 * 
		 * @param operationState  — The original range of text. 
		 * @param deleteSelectionState  — The range of text to delete, if different from the range described by <code>operationState</code>. (Set to <code>null</code> to delete the range described by <code>operationState</code>.) 
		 * @param allowMerge  — Set to <code>true</code> if this operation can be merged with the next or previous operation. 
		 */
		public function DeleteTextOperation(operationState:SelectionState, deleteSelectionState:SelectionState = null, allowMerge:Boolean = false) {
			super(operationState);
			throw new Error("Not implemented");
		}

		/**
		 * <p> Indicates whether this operation can be merged with operations executed before or after it. </p>
		 * <p>Some delete operations, for example, a sequence of backspace keystrokes, can be fruitfully merged into one operation so that undoing the operation reverses the entire sequence.</p>
		 * 
		 * @return 
		 */
		public function get allowMerge():Boolean {
			return _allowMerge;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set allowMerge(value:Boolean):void {
			_allowMerge = value;
		}

		/**
		 * <p> deleteSelectionState The range of text to delete </p>
		 * 
		 * @return 
		 */
		public function get deleteSelectionState():SelectionState {
			return _deleteSelectionState;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set deleteSelectionState(value:SelectionState):void {
			_deleteSelectionState = value;
		}
	}
}
