package flashx.textLayout.factory {
	import flashx.textLayout.elements.IConfiguration;
	import flashx.textLayout.formats.ITextLayoutFormat;

	/**
	 *  The StringTextLineFactory class provides a simple way to create TextLines from a string. <p>The text lines are static and are created using a single format and a single paragraph. The lines are created to fit in the specified bounding rectangle.</p> <p>The StringTextLineFactory provides an efficient way to create TextLines, since it reuses single TextFlow, ParagraphElement, SpanElement, and ContainerController objects across many repeated invocations. You can create a single factory, and use it again and again. You can also reuse all the parts that are the same each time you call it; for instance, you can reuse the various formats and the bounds.</p> <p> <b>Note:</b> To create static lines that use multiple formats or paragraphs, or that include inline graphics, use a TextFlowTextLineFactory and a TextFlow object. </p> <p> <b>Note:</b> The StringTextLineFactory ignores the truncationIndicatorFormat property set in the truncationOptions when truncating text.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="TextFlowTextLineFactory.html" target="">TextFlowTextLineFactory</a>
	 * </div><br><hr>
	 */
	public class StringTextLineFactory extends TextLineFactoryBase {
		private static var _defaultConfiguration:IConfiguration;

		private var _paragraphFormat:ITextLayoutFormat;
		private var _spanFormat:ITextLayoutFormat;
		private var _text:String;
		private var _textFlowFormat:ITextLayoutFormat;

		private var _configuration:IConfiguration;

		/**
		 * <p> Creates a StringTextLineFactory object. </p>
		 * 
		 * @param configuration  — The configuration object used to set the properties of the internal TextFlow object used to compose lines produced by this factory. 
		 */
		public function StringTextLineFactory(configuration:IConfiguration = null) {
			throw new Error("Not implemented");
		}

		/**
		 * <p> The configuration used by the internal TextFlow object. </p>
		 * 
		 * @return 
		 */
		public function get configuration():IConfiguration {
			return _configuration;
		}

		/**
		 * <p> The paragraph format. </p>
		 * 
		 * @return 
		 */
		public function get paragraphFormat():ITextLayoutFormat {
			return _paragraphFormat;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set paragraphFormat(value:ITextLayoutFormat):void {
			_paragraphFormat = value;
		}

		/**
		 * <p> The character format. </p>
		 * 
		 * @return 
		 */
		public function get spanFormat():ITextLayoutFormat {
			return _spanFormat;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set spanFormat(value:ITextLayoutFormat):void {
			_spanFormat = value;
		}

		/**
		 * <p> The text to convert into TextLine objects. </p>
		 * <p>To produce TextLines, call <code>createTextLines()</code> after setting this <code>text</code> property and the desired formats.</p>
		 * 
		 * @return 
		 */
		public function get text():String {
			return _text;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set text(value:String):void {
			_text = value;
		}

		/**
		 * <p> The text flow format. </p>
		 * 
		 * @return 
		 */
		public function get textFlowFormat():ITextLayoutFormat {
			return _textFlowFormat;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set textFlowFormat(value:ITextLayoutFormat):void {
			_textFlowFormat = value;
		}

		/**
		 * <p> Creates TextLine objects using the text currently assigned to this factory object. </p>
		 * <p>The text lines are created using the currently assigned text and formats and are composed to fit the bounds assigned to the <code>compositionBounds</code> property. As each line is created, the factory calls the function specified in the <code>callback</code> parameter. This function is passed the TextLine object and is responsible for displaying the line.</p>
		 * <p>To create a different set of lines, change any properties desired and call <code>createTextLines()</code> again.</p>
		 * <p>Note that the scroll policies of the factory will control how many lines are generated.</p>
		 * 
		 * @param callback  — The callback function called for each TextLine object created. 
		 */
		public function createTextLines(callback:Function):void {
			throw new Error("Not implemented");
		}


		/**
		 * <p> The default configuration used by this factory if none is specified. </p>
		 * 
		 * @return 
		 */
		public static function get defaultConfiguration():IConfiguration {
			return _defaultConfiguration;
		}
	}
}
