package flashx.textLayout.factory {
	import flash.geom.Rectangle;

	import flashx.textLayout.compose.ISWFContext;

	/**
	 *  The TextLineFactoryBase class serves as the base class for the Text Layout Framework text line factories. <p> <b>Note:</b> Application code does not typically need to create or use a TextLineFactoryBase object directly. Use one of the derived text factory classes instead.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flashx/textLayout/elements/TextFlow.html" target="">flashx.textLayout.elements.TextFlow</a>
	 * </div><br><hr>
	 */
	public class TextLineFactoryBase {
		private var _compositionBounds:Rectangle;
		private var _horizontalScrollPolicy:String;
		private var _swfContext:ISWFContext;
		private var _truncationOptions:TruncationOptions;
		private var _verticalScrollPolicy:String;

		private var _isTruncated:Boolean;

		/**
		 * <p> Base-class constructor for text line factories. </p>
		 * <p><b>Note:</b> Application code does not typically need to create or use a TextLineFactoryBase object directly. Use one of the derived text factory classes instead.</p>
		 */
		public function TextLineFactoryBase() {
			throw new Error("Not implemented");
		}

		/**
		 * <p> The rectangle within which text lines are created. </p>
		 * 
		 * @return 
		 */
		public function get compositionBounds():Rectangle {
			return _compositionBounds;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set compositionBounds(value:Rectangle):void {
			_compositionBounds = value;
		}

		/**
		 * <p> Specifies how lines are created when the composition bounds are not large enough. </p>
		 * <p>If set to <code>ScrollPolicy.ON</code> or <code>ScrollPolicy.AUTO</code>, all lines are created. It is the your responsibility to scroll lines in the viewable area (and to mask lines outside this area, if necessary). If set to <code>ScrollPolicy.OFF</code>, then only lines that fit within the composition bounds are created.</p>
		 * <p>If the <code>truncationOptions</code> property is set, the scroll policy is ignored (and treated as <code>ScrollPolicy.OFF</code>).</p>
		 * 
		 * @return 
		 */
		public function get horizontalScrollPolicy():String {
			return _horizontalScrollPolicy;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set horizontalScrollPolicy(value:String):void {
			_horizontalScrollPolicy = value;
		}

		/**
		 * <p> Indicates whether text was truncated when lines were last created. </p>
		 * 
		 * @return 
		 */
		public function get isTruncated():Boolean {
			return _isTruncated;
		}

		/**
		 * <p> The ISWFContext instance used to make FTE calls as needed. </p>
		 * <p>By default, the ISWFContext implementation is this FlowComposerBase object. Applications can provide a custom implementation to use fonts embedded in a different SWF file or to cache and reuse text lines.</p>
		 * 
		 * @return 
		 */
		public function get swfContext():ISWFContext {
			return _swfContext;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set swfContext(value:ISWFContext):void {
			_swfContext = value;
		}

		/**
		 * <p> Specifies the options for truncating the text if it doesn't fit in the composition bounds. </p>
		 * 
		 * @return 
		 */
		public function get truncationOptions():TruncationOptions {
			return _truncationOptions;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set truncationOptions(value:TruncationOptions):void {
			_truncationOptions = value;
		}

		/**
		 * <p> Specifies how lines are created when the composition bounds are not large enough. </p>
		 * <p>If set to <code>ScrollPolicy.ON</code> or <code>ScrollPolicy.AUTO</code>, all lines are created. It is the your responsibility to scroll lines in the viewable area (and to mask lines outside this area, if necessary). If set to <code>ScrollPolicy.OFF</code>, then only lines that fit within the composition bounds are created.</p>
		 * <p>If the <code>truncationOptions</code> property is set, the scroll policy is ignored (and treated as <code>ScrollPolicy.OFF</code>).</p>
		 * 
		 * @return 
		 */
		public function get verticalScrollPolicy():String {
			return _verticalScrollPolicy;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set verticalScrollPolicy(value:String):void {
			_verticalScrollPolicy = value;
		}

		/**
		 * <p> The smallest rectangle in which the layed-out content fits. </p>
		 * <p><b>Note:</b> Truncated lines are not included in the size calculation.</p>
		 * 
		 * @return 
		 */
		public function getContentBounds():Rectangle {
			throw new Error("Not implemented");
		}
	}
}
