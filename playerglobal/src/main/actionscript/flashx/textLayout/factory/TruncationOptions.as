package flashx.textLayout.factory {
	import flashx.textLayout.formats.ITextLayoutFormat;

	/**
	 *  The TruncationOptions class specifies options for limiting the number of lines of text created by a text line factory and for indicating when lines have been left out. <br><hr>
	 */
	public class TruncationOptions {
		/**
		 * <p> Defines the <code>truncationIndicator</code> property value, <code>\u2026</code>, that represents a horizontal ellipsis. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="TruncationOptions.html#truncationIndicator" target="">truncationIndicator</a>
		 * </div>
		 */
		public static const HORIZONTAL_ELLIPSIS:String = "…";
		/**
		 * <p> Defines the <code>lineCountLimit</code> property value, <code>-1</code>, that represents no limit. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="TruncationOptions.html#lineCountLimit" target="">lineCountLimit</a>
		 * </div>
		 */
		public static const NO_LINE_COUNT_LIMIT:int = -1;

		private var _lineCountLimit:int;
		private var _truncationIndicator:String;
		private var _truncationIndicatorFormat:ITextLayoutFormat;

		/**
		 * <p> Creates a TruncationOptions object. </p>
		 * 
		 * @param truncationIndicator  — the string used to indicate that text has been truncated. It appears at the end of the composed text. The default value is the horizontal ellipsis (U+2026). 
		 * @param lineCountLimit  — specifies a truncation criterion in the form of the maximum number of lines allowed. The default value of <code>NO_LINE_COUNT_LIMIT</code> indicates that there is no line count limit. 
		 * @param truncationIndicatorFormat  — specifies the format for the truncation indicator. A null format (the default value) specifies that the truncation indicator assume the format of content just before the truncation point. The <code>TextLineFactory</code> methods that take a simple string as input also ignore this parameter and implement the default behavior. 
		 */
		public function TruncationOptions(truncationIndicator:String = null, lineCountLimit:int = NaN, truncationIndicatorFormat:ITextLayoutFormat = null) {
			throw new Error("Not implemented");
		}

		/**
		 * <p> The maximum number of lines to create. </p>
		 * 
		 * @return 
		 */
		public function get lineCountLimit():int {
			return _lineCountLimit;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set lineCountLimit(value:int):void {
			_lineCountLimit = value;
		}

		/**
		 * <p> A string used to indicate that content could not be fully displayed because of limits on the number of lines. </p>
		 * 
		 * @return 
		 */
		public function get truncationIndicator():String {
			return _truncationIndicator;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set truncationIndicator(value:String):void {
			_truncationIndicator = value;
		}

		/**
		 * <p> The style applied to the truncation indicator string. </p>
		 * 
		 * @return 
		 */
		public function get truncationIndicatorFormat():ITextLayoutFormat {
			return _truncationIndicatorFormat;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set truncationIndicatorFormat(value:ITextLayoutFormat):void {
			_truncationIndicatorFormat = value;
		}
	}
}
