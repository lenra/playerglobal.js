package {
	/**
	 *  The XML class contains methods and properties for working with XML objects. The XML class (along with the XMLList, Namespace, and QName classes) implements the powerful XML-handling standards defined in ECMAScript for XML (E4X) specification (ECMA-357 edition 2). <p>Use the <code>toXMLString()</code> method to return a string representation of the XML object regardless of whether the XML object has simple content or complex content.</p> <p> <b>Note</b>: The XML class (along with related classes) from ActionScript 2.0 has been renamed XMLDocument and moved into the flash.xml package. It is included in ActionScript 3.0 for backward compatibility.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7f95.html" target="_blank">Initializing XML variables</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e68.html" target="_blank">Assembling and transforming XML objects</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e6b.html" target="_blank">Traversing XML structures</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e6c.html" target="_blank">Using XML namespaces</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e6d.html" target="_blank">XML type conversion</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e5e.html" target="_blank">Converting XML and XMLList objects to strings</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e6a.html" target="_blank">Reading external XML documents</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e69.html" target="_blank">XML in ActionScript example: Loading RSS data from the Internet</a>
	 * </div><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS2db454920e96a9e51e63e3d11c0bf69084-7b69.html" target="_blank">Hierarchical data objects</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7ff5.html" target="_blank">Working with XML</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e65.html" target="_blank">Basics of XML</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e72.html" target="_blank">The E4X approach to XML processing</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e71.html" target="_blank">XML objects</a>
	 *  <br>
	 *  <a href="http://www.ecma-international.org/publications/standards/Ecma-357.htm" target="_blank">ECMAScript for XML (E4X) specification (ECMA-357 edition 2)</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Namespace.html" target="">Namespace</a>
	 *  <br>
	 *  <a href="QName.html" target="">QName</a>
	 *  <br>
	 *  <a href="XMLList.html" target="">XMLList</a>
	 *  <br>
	 *  <a href="XML.html#toXMLString()" target="">XML.toXMLString()</a>
	 * </div><br><hr>
	 */
	public class XML {
		private static var _ignoreComments:Boolean;
		private static var _ignoreProcessingInstructions:Boolean;
		private static var _ignoreWhitespace:Boolean;
		private static var _prettyIndent:int;
		private static var _prettyPrinting:Boolean;

		/**
		 * <p> Creates a new XML object. You must use the constructor to create an XML object before you call any of the methods of the XML class. </p>
		 * <p>Use the <code>toXMLString()</code> method to return a string representation of the XML object regardless of whether the XML object has simple content or complex content.</p>
		 * 
		 * @param value  — Any object that can be converted to XML with the top-level <code>XML()</code> function. 
		 */
		public function XML(value:Object) {
			value.valueOf();
			throw new Error("Not implemented");
		}

		/**
		 * <p> Adds a namespace to the set of in-scope namespaces for the XML object. If the namespace already exists in the in-scope namespaces for the XML object (with a prefix matching that of the given parameter), then the prefix of the existing namespace is set to <code>undefined</code>. If the input parameter is a Namespace object, it's used directly. If it's a QName object, the input parameter's URI is used to create a new namespace; otherwise, it's converted to a String and a namespace is created from the String. </p>
		 * 
		 * @param ns  — The namespace to add to the XML object. 
		 * @return  — The new XML object, with the namespace added. 
		 */
		public function addNamespace(ns:Object):XML {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Appends the given child to the end of the XML object's properties. The <code>appendChild()</code> method takes an XML object, an XMLList object, or any other data type that is then converted to a String. </p>
		 * <p>Use the <code>delete</code> (XML) operator to remove XML nodes.</p>
		 * 
		 * @param child  — The XML object to append. 
		 * @return  — The resulting XML object. 
		 */
		public function appendChild(child:Object):XML {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the XML value of the attribute that has the name matching the <code>attributeName</code> parameter. Attributes are found within XML elements. In the following example, the element has an attribute named "<code>gender</code>" with the value "<code>boy</code>": <code>&lt;first gender="boy"&gt;John&lt;/first&gt;</code>. </p>
		 * <p>The <code>attributeName</code> parameter can be any data type; however, String is the most common data type to use. When passing any object other than a QName object, the <code>attributeName</code> parameter uses the <code>toString()</code> method to convert the parameter to a string. </p>
		 * <p>If you need a qualified name reference, you can pass in a QName object. A QName object defines a namespace and the local name, which you can use to define the qualified name of an attribute. Therefore calling <code>attribute(qname)</code> is not the same as calling <code>attribute(qname.toString())</code>.</p>
		 * 
		 * @param attributeName  — The name of the attribute. 
		 * @return  — An XMLList object or an empty XMLList object. Returns an empty XMLList object when an attribute value has not been defined. 
		 */
		public function attribute(attributeName:*):XMLList {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a list of attribute values for the given XML object. Use the <code>name()</code> method with the <code>attributes()</code> method to return the name of an attribute. Use of <code>xml.attributes()</code> is equivalent to <code>xml.@*</code>. </p>
		 * 
		 * @return  — The list of attribute values. 
		 */
		public function attributes():XMLList {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Lists the children of an XML object. An XML child is an XML element, text node, comment, or processing instruction. </p>
		 * <p>Use the <code>propertyName</code> parameter to list the contents of a specific XML child. For example, to return the contents of a child named <code>&lt;first&gt;</code>, call <code>child("first")</code> on the XML object. You can generate the same result by using the child's index number. The index number identifies the child's position in the list of other XML children. For example, <code>child(0)</code> returns the first child in a list. </p>
		 * <p>Use an asterisk (*) to output all the children in an XML document. For example, <code>doc.child("*")</code>.</p>
		 * <p>Use the <code>length()</code> method with the asterisk (*) parameter of the <code>child()</code> method to output the total number of children. For example, <code>numChildren = doc.child("*").length()</code>.</p>
		 * 
		 * @param propertyName  — The element name or integer of the XML child. 
		 * @return  — An XMLList object of child nodes that match the input parameter. 
		 */
		public function child(propertyName:Object):XMLList {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Identifies the zero-indexed position of this XML object within the context of its parent. </p>
		 * 
		 * @return  — The position of the object. Returns -1 as well as positive integers. 
		 */
		public function childIndex():int {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Lists the children of the XML object in the sequence in which they appear. An XML child is an XML element, text node, comment, or processing instruction. </p>
		 * 
		 * @return  — An XMLList object of the XML object's children. 
		 */
		public function children():XMLList {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Lists the properties of the XML object that contain XML comments. </p>
		 * 
		 * @return  — An XMLList object of the properties that contain comments. 
		 */
		public function comments():XMLList {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Compares the XML object against the given <code>value</code> parameter. </p>
		 * 
		 * @param value  — A value to compare against the current XML object. 
		 * @return  — If the XML object matches the  parameter, then ; otherwise . 
		 */
		public function contains(value:XML):Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a copy of the given XML object. The copy is a duplicate of the entire tree of nodes. The copied XML object has no parent and returns <code>null</code> if you attempt to call the <code>parent()</code> method. </p>
		 * 
		 * @return  — The copy of the object. 
		 */
		public function copy():XML {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns all descendants (children, grandchildren, great-grandchildren, and so on) of the XML object that have the given <code>name</code> parameter. The <code>name</code> parameter is optional. The <code>name</code> parameter can be a QName object, a String data type or any other data type that is then converted to a String data type. </p>
		 * <p>To return all descendants, use the "*" parameter. If no parameter is passed, the string "*" is passed and returns all descendants of the XML object.</p>
		 * 
		 * @param name  — The name of the element to match. 
		 * @return  — An XMLList object of matching descendants. If there are no descendants, returns an empty XMLList object. 
		 */
		public function descendants(name:Object = "*"):XMLList {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Lists the elements of an XML object. An element consists of a start and an end tag; for example <code>&lt;first&gt;&lt;/first&gt;</code>. The <code>name</code> parameter is optional. The <code>name</code> parameter can be a QName object, a String data type, or any other data type that is then converted to a String data type. Use the <code>name</code> parameter to list a specific element. For example, the element "<code>first</code>" returns "<code>John</code>" in this example: <code>&lt;first&gt;John&lt;/first&gt;</code>. </p>
		 * <p>To list all elements, use the asterisk (*) as the parameter. The asterisk is also the default parameter. </p>
		 * <p>Use the <code>length()</code> method with the asterisk parameter to output the total number of elements. For example, <code>numElement = addressbook.elements("*").length()</code>.</p>
		 * 
		 * @param name  — The name of the element. An element's name is surrounded by angle brackets. For example, "<code>first</code>" is the <code>name</code> in this example: <code>&lt;first&gt;&lt;/first&gt;</code>. 
		 * @return  — An XMLList object of the element's content. The element's content falls between the start and end tags. If you use the asterisk (*) to call all elements, both the element's tags and content are returned. 
		 */
		public function elements(name:Object = "*"):XMLList {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Checks to see whether the XML object contains complex content. An XML object contains complex content if it has child elements. XML objects that representing attributes, comments, processing instructions, and text nodes do not have complex content. However, an object that <i>contains</i> these can still be considered to contain complex content (if the object has child elements). </p>
		 * 
		 * @return  — If the XML object contains complex content, ; otherwise . 
		 */
		public function hasComplexContent():Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Checks to see whether the object has the property specified by the <code>p</code> parameter. </p>
		 * 
		 * @param p  — The property to match. 
		 * @return  — If the property exists, ; otherwise . 
		 */
		/*public function hasOwnProperty(p:*):Boolean {
			throw new Error("Not implemented");
		}*/

		/**
		 * <p> Checks to see whether the XML object contains simple content. An XML object contains simple content if it represents a text node, an attribute node, or an XML element that has no child elements. XML objects that represent comments and processing instructions do <i>not</i> contain simple content. </p>
		 * 
		 * @return  — If the XML object contains simple content, ; otherwise . 
		 */
		public function hasSimpleContent():Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Lists the namespaces for the XML object, based on the object's parent. </p>
		 * 
		 * @return  — An array of Namespace objects. 
		 */
		public function inScopeNamespaces():Array {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Inserts the given <code>child2</code> parameter after the <code>child1</code> parameter in this XML object and returns the resulting object. If the <code>child1</code> parameter is <code>null</code>, the method inserts the contents of <code>child2</code> <i>before</i> all children of the XML object (in other words, after <i>none</i>). If <code>child1</code> is provided, but it does not exist in the XML object, the XML object is not modified and <code>undefined</code> is returned. </p>
		 * <p>If you call this method on an XML child that is not an element (text, attributes, comments, pi, and so on) <code>undefined</code> is returned.</p>
		 * <p>Use the <code>delete</code> (XML) operator to remove XML nodes.</p>
		 * 
		 * @param child1  — The object in the source object that you insert before <code>child2</code>. 
		 * @param child2  — The object to insert. 
		 * @return  — The resulting XML object or . 
		 */
		public function insertChildAfter(child1:Object, child2:Object):* {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Inserts the given <code>child2</code> parameter before the <code>child1</code> parameter in this XML object and returns the resulting object. If the <code>child1</code> parameter is <code>null</code>, the method inserts the contents of <code>child2</code> <i>after</i> all children of the XML object (in other words, before <i>none</i>). If <code>child1</code> is provided, but it does not exist in the XML object, the XML object is not modified and <code>undefined</code> is returned. </p>
		 * <p>If you call this method on an XML child that is not an element (text, attributes, comments, pi, and so on) <code>undefined</code> is returned.</p>
		 * <p>Use the <code>delete</code> (XML) operator to remove XML nodes.</p>
		 * 
		 * @param child1  — The object in the source object that you insert after <code>child2</code>. 
		 * @param child2  — The object to insert. 
		 * @return  — The resulting XML object or . 
		 */
		public function insertChildBefore(child1:Object, child2:Object):* {
			throw new Error("Not implemented");
		}

		/**
		 * <p> For XML objects, this method always returns the integer <code>1</code>. The <code>length()</code> method of the XMLList class returns a value of <code>1</code> for an XMLList object that contains only one value. </p>
		 * 
		 * @return  — Always returns  for any XML object. 
		 */
		public function length():int {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Gives the local name portion of the qualified name of the XML object. </p>
		 * 
		 * @return  — The local name as either a String or . 
		 */
		public function localName():Object {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Gives the qualified name for the XML object. </p>
		 * 
		 * @return  — The qualified name is either a QName or . 
		 */
		public function name():Object {
			throw new Error("Not implemented");
		}

		/**
		 * <p> If no parameter is provided, gives the namespace associated with the qualified name of this XML object. If a <code>prefix</code> parameter is specified, the method returns the namespace that matches the <code>prefix</code> parameter and that is in scope for the XML object. If there is no such namespace, the method returns <code>undefined</code>. </p>
		 * 
		 * @param prefix  — The prefix you want to match. 
		 * @return  — Returns , , or a namespace. 
		 */
		public function namespace(prefix:String = null):* {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Lists namespace declarations associated with the XML object in the context of its parent. </p>
		 * 
		 * @return  — An array of Namespace objects. 
		 */
		public function namespaceDeclarations():Array {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Specifies the type of node: text, comment, processing-instruction, attribute, or element. </p>
		 * 
		 * @return  — The node type used. 
		 */
		public function nodeKind():String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> For the XML object and all descendant XML objects, merges adjacent text nodes and eliminates empty text nodes. </p>
		 * 
		 * @return  — The resulting normalized XML object. 
		 */
		public function normalize():XML {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the parent of the XML object. If the XML object has no parent, the method returns <code>undefined</code>. </p>
		 * 
		 * @return  — Either an XML reference of the parent node, or  if the XML object has no parent. 
		 */
		public function parent():* {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Inserts a copy of the provided <code>child</code> object into the XML element before any existing XML properties for that element. </p>
		 * <p>Use the <code>delete</code> (XML) operator to remove XML nodes.</p>
		 * 
		 * @param value  — The object to insert. 
		 * @return  — The resulting XML object. 
		 */
		public function prependChild(value:Object):XML {
			throw new Error("Not implemented");
		}

		/**
		 * <p> If a <code>name</code> parameter is provided, lists all the children of the XML object that contain processing instructions with that <code>name</code>. With no parameters, the method lists all the children of the XML object that contain any processing instructions. </p>
		 * 
		 * @param name  — The name of the processing instructions to match. 
		 * @return  — A list of matching child objects. 
		 */
		public function processingInstructions(name:String = "*"):XMLList {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Checks whether the property <code>p</code> is in the set of properties that can be iterated in a <code>for..in</code> statement applied to the XML object. Returns <code>true</code> only if <code>toString(p) == "0"</code>. </p>
		 * 
		 * @param p  — The property that you want to check. 
		 * @return  — If the property can be iterated in a  statement, ; otherwise, . 
		 */
		override public function propertyIsEnumerable(p:String):Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Removes the given namespace for this object and all descendants. The <code>removeNamespaces()</code> method does not remove a namespace if it is referenced by the object's qualified name or the qualified name of the object's attributes. </p>
		 * 
		 * @param ns  — The namespace to remove. 
		 * @return  — A copy of the resulting XML object. 
		 */
		public function removeNamespace(ns:Namespace):XML {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Replaces the properties specified by the <code>propertyName</code> parameter with the given <code>value</code> parameter. If no properties match <code>propertyName</code>, the XML object is left unmodified. </p>
		 * 
		 * @param propertyName  — Can be a numeric value, an unqualified name for a set of XML elements, a qualified name for a set of XML elements, or the asterisk wildcard ("*"). Use an unqualified name to identify XML elements in the default namespace. 
		 * @param value  — The replacement value. This can be an XML object, an XMLList object, or any value that can be converted with <code>toString()</code>. 
		 * @return  — The resulting XML object, with the matching properties replaced. 
		 */
		public function replace(propertyName:Object, value:XML):XML {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Replaces the child properties of the XML object with the specified set of XML properties, provided in the <code>value</code> parameter. </p>
		 * 
		 * @param value  — The replacement XML properties. Can be a single XML object or an XMLList object. 
		 * @return  — The resulting XML object. 
		 */
		public function setChildren(value:Object):XML {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Changes the local name of the XML object to the given <code>name</code> parameter. </p>
		 * 
		 * @param name  — The replacement name for the local name. 
		 */
		public function setLocalName(name:String):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets the name of the XML object to the given qualified name or attribute name. </p>
		 * 
		 * @param name  — The new name for the object. 
		 */
		public function setName(name:String):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets the namespace associated with the XML object. </p>
		 * 
		 * @param ns  — The new namespace. 
		 */
		public function setNamespace(ns:Namespace):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns an XMLList object of all XML properties of the XML object that represent XML text nodes. </p>
		 * 
		 * @return  — The list of properties. 
		 */
		public function text():XMLList {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Provides an overridable method for customizing the JSON encoding of values in an XML object. </p>
		 * <p>The <code>JSON.stringify()</code> method looks for a <code>toJSON()</code> method on each object that it traverses. If the <code>toJSON()</code> method is found, <code>JSON.stringify()</code> calls it for each value it encounters, passing in the key that is paired with the value.</p>
		 * <p>XML provides a default implementation of <code>toJSON()</code> that simply returns the name of the class. Clients that wish to export XML objects to JSON must provide their own implementation. You can do this by redefining the <code>toJSON()</code> method on the class prototype. </p>
		 * <p> The <code>toJSON()</code> method can return a value of any type. If it returns an object, <code>stringify()</code> recurses into that object. If <code>toJSON()</code> returns a string, <code>stringify()</code> does not recurse and continues its traversal.</p>
		 * 
		 * @param k  — The key of a key/value pair that <code>JSON.stringify()</code> has encountered in its traversal of this object 
		 * @return  — The class name string. 
		 */
		/*public function toJSON(k:String):* {
			throw new Error("Not implemented");
		}*/

		/**
		 * <p> Returns a string representation of the XML object. The rules for this conversion depend on whether the XML object has simple content or complex content: </p>
		 * <ul>
		 *  <li>If the XML object has simple content, <code>toString()</code> returns the String contents of the XML object with the following stripped out: the start tag, attributes, namespace declarations, and end tag.</li>
		 * </ul>
		 * <ul>
		 *  <li> If the XML object has complex content, <code>toString()</code> returns an XML encoded String representing the entire XML object, including the start tag, attributes, namespace declarations, and end tag.</li>
		 * </ul>
		 * <p>To return the entire XML object every time, use <code>toXMLString()</code>.</p>
		 * 
		 * @return  — The string representation of the XML object. 
		 */
		public function toString():String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a string representation of the XML object. Unlike the <code>toString()</code> method, the <code>toXMLString()</code> method always returns the start tag, attributes, and end tag of the XML object, regardless of whether the XML object has simple content or complex content. (The <code>toString()</code> method strips out these items for XML objects that contain simple content.) </p>
		 * 
		 * @return  — The string representation of the XML object. 
		 */
		public function toXMLString():String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the XML object. </p>
		 * 
		 * @return  — The primitive value of an XML instance. 
		 */
		/*public function valueOf():* {
			throw new Error("Not implemented");
		}*/


		/**
		 * <p> Determines whether XML comments are ignored when XML objects parse the source XML data. By default, the comments are ignored (<code>true</code>). To include XML comments, set this property to <code>false</code>. The <code>ignoreComments</code> property is used only during the XML parsing, not during the call to any method such as <code>myXMLObject.child(*).toXMLString()</code>. If the source XML includes comment nodes, they are kept or discarded during the XML parsing. </p>
		 * 
		 * @return 
		 */
		public static function get ignoreComments():Boolean {
			return _ignoreComments;
		}


		/**
		 * @param value
		 * @return 
		 */
		public static function set ignoreComments(value:Boolean):void {
			_ignoreComments = value;
		}


		/**
		 * <p> Determines whether XML processing instructions are ignored when XML objects parse the source XML data. By default, the processing instructions are ignored (<code>true</code>). To include XML processing instructions, set this property to <code>false</code>. The <code>ignoreProcessingInstructions</code> property is used only during the XML parsing, not during the call to any method such as <code>myXMLObject.child(*).toXMLString()</code>. If the source XML includes processing instructions nodes, they are kept or discarded during the XML parsing. </p>
		 * 
		 * @return 
		 */
		public static function get ignoreProcessingInstructions():Boolean {
			return _ignoreProcessingInstructions;
		}


		/**
		 * @param value
		 * @return 
		 */
		public static function set ignoreProcessingInstructions(value:Boolean):void {
			_ignoreProcessingInstructions = value;
		}


		/**
		 * <p> Determines whether white space characters at the beginning and end of text nodes are ignored during parsing. By default, white space is ignored (<code>true</code>). If a text node is 100% white space and the <code>ignoreWhitespace</code> property is set to <code>true</code>, then the node is not created. To show white space in a text node, set the <code>ignoreWhitespace</code> property to <code>false</code>. </p>
		 * <p>When you create an XML object, it caches the current value of the <code>ignoreWhitespace</code> property. Changing the <code>ignoreWhitespace</code> does not change the behavior of existing XML objects.</p>
		 * 
		 * @return 
		 */
		public static function get ignoreWhitespace():Boolean {
			return _ignoreWhitespace;
		}


		/**
		 * @param value
		 * @return 
		 */
		public static function set ignoreWhitespace(value:Boolean):void {
			_ignoreWhitespace = value;
		}


		/**
		 * <p> Determines the amount of indentation applied by the <code>toString()</code> and <code>toXMLString()</code> methods when the <code>XML.prettyPrinting</code> property is set to <code>true</code>. Indentation is applied with the space character, not the tab character. The default value is <code>2</code>. </p>
		 * 
		 * @return 
		 */
		public static function get prettyIndent():int {
			return _prettyIndent;
		}


		/**
		 * @param value
		 * @return 
		 */
		public static function set prettyIndent(value:int):void {
			_prettyIndent = value;
		}


		/**
		 * <p> Determines whether the <code>toString()</code> and <code>toXMLString()</code> methods normalize white space characters between some tags. The default value is <code>true</code>. </p>
		 * 
		 * @return 
		 */
		public static function get prettyPrinting():Boolean {
			return _prettyPrinting;
		}


		/**
		 * @param value
		 * @return 
		 */
		public static function set prettyPrinting(value:Boolean):void {
			_prettyPrinting = value;
		}


		/**
		 * <p> Returns an object with the following properties set to the default values: <code>ignoreComments</code>, <code>ignoreProcessingInstructions</code>, <code>ignoreWhitespace</code>, <code>prettyIndent</code>, and <code>prettyPrinting</code>. The default values are as follows: </p>
		 * <ul>
		 *  <li><code>ignoreComments = true</code></li>
		 *  <li><code>ignoreProcessingInstructions = true</code></li>
		 *  <li><code>ignoreWhitespace = true</code></li>
		 *  <li><code>prettyIndent = 2</code></li>
		 *  <li><code>prettyPrinting = true</code></li>
		 * </ul>
		 * <p><b>Note:</b> You do not apply this method to an instance of the XML class; you apply it to <code>XML</code>, as in the following code: <code>var df:Object = XML.defaultSettings()</code>. </p>
		 * 
		 * @return  — An object with properties set to the default settings. 
		 */
		public static function defaultSettings():Object {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Sets values for the following XML properties: <code>ignoreComments</code>, <code>ignoreProcessingInstructions</code>, <code>ignoreWhitespace</code>, <code>prettyIndent</code>, and <code>prettyPrinting</code>. The following are the default settings, which are applied if no <code>setObj</code> parameter is provided: </p>
		 * <ul>
		 *  <li><code>XML.ignoreComments = true</code></li>
		 *  <li><code>XML.ignoreProcessingInstructions = true</code></li>
		 *  <li><code>XML.ignoreWhitespace = true</code></li>
		 *  <li><code>XML.prettyIndent = 2</code></li>
		 *  <li><code>XML.prettyPrinting = true</code></li>
		 * </ul>
		 * <p><b>Note</b>: You do not apply this method to an instance of the XML class; you apply it to <code>XML</code>, as in the following code: <code>XML.setSettings()</code>.</p>
		 * 
		 * @param rest  — An object with each of the following properties: <ul>
		 *  <li><code>ignoreComments</code></li>
		 *  <li><code>ignoreProcessingInstructions</code></li>
		 *  <li><code>ignoreWhitespace</code></li>
		 *  <li><code>prettyIndent</code></li>
		 *  <li><code>prettyPrinting</code></li>
		 * </ul> 
		 */
		public static function setSettings(...rest):void {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Retrieves the following properties: <code>ignoreComments</code>, <code>ignoreProcessingInstructions</code>, <code>ignoreWhitespace</code>, <code>prettyIndent</code>, and <code>prettyPrinting</code>. </p>
		 * 
		 * @return  — An object with the following XML properties: <ul>
		 *  <li><code>ignoreComments</code></li>
		 *  <li><code>ignoreProcessingInstructions</code></li>
		 *  <li><code>ignoreWhitespace</code></li>
		 *  <li><code>prettyIndent</code></li>
		 *  <li><code>prettyPrinting</code></li>
		 * </ul> 
		 */
		public static function settings():Object {
			throw new Error("Not implemented");
		}
	}
}
