package {
	/**
	 *  The uint class provides methods for working with a data type representing a 32-bit unsigned integer. Because an unsigned integer can only be positive, its maximum value is twice that of the int class. <p>The range of values represented by the uint class is 0 to 4,294,967,295 (2^32-1).</p> <p>You can create a uint object by declaring a variable of type uint and assigning the variable a literal value. The default value of a variable of type uint is <code>0</code>.</p> <p>The uint class is primarily useful for pixel color values (ARGB and RGBA) and other situations where the int data type does not work well. For example, the number 0xFFFFFFFF, which represents the color value white with an alpha value of 255, can't be represented using the int data type because it is not within the valid range of the int values.</p> <p>The following example creates a uint object and calls the <code> toString()</code> method:</p> <pre>
	 *  var myuint:uint = 1234;
	 *  trace(myuint.toString()); // 1234
	 *  </pre> <p>The following example assigns the value of the <code>MIN_VALUE</code> property to a variable without the use of the constructor:</p> <pre>
	 *  var smallest:uint = uint.MIN_VALUE;
	 *  trace(smallest.toString()); // 0
	 *  </pre> <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="int.html" target="">int</a>
	 *  <br>
	 *  <a href="Number.html" target="">Number</a>
	 * </div><br><hr>
	 */
	public class uint {
		/**
		 * <p> The largest representable 32-bit unsigned integer, which is 4,294,967,295. </p>
		 */
		public static const MAX_VALUE:uint = 4294967295;
		/**
		 * <p> The smallest representable unsigned integer, which is <code>0</code>. </p>
		 */
		public static const MIN_VALUE:uint = 0;

		/**
		 * <p> Creates a new uint object. You can create a variable of uint type and assign it a literal value. The <code>new uint()</code> constructor is primarily used as a placeholder. A uint object is not the same as the <code> uint()</code> function, which converts a parameter to a primitive value. </p>
		 * 
		 * @param num  — The numeric value of the uint object being created, or a value to be converted to a number. If <code>num</code> is not provided, the default value is <code>0</code>. 
		 */
		public function uint(num:Object) {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a string representation of the number in exponential notation. The string contains one digit before the decimal point and up to 20 digits after the decimal point, as specified by the <code>fractionDigits</code> parameter. </p>
		 * 
		 * @param fractionDigits  — An integer between 0 and 20, inclusive, that represents the desired number of decimal places. 
		 * @return 
		 */
		public function toExponential(fractionDigits:uint):String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a string representation of the number in fixed-point notation. Fixed-point notation means that the string will contain a specific number of digits after the decimal point, as specified in the <code>fractionDigits</code> parameter. The valid range for the <code>fractionDigits</code> parameter is from 0 to 20. Specifying a value outside this range throws an exception. </p>
		 * 
		 * @param fractionDigits  — An integer between 0 and 20, inclusive, that represents the desired number of decimal places. 
		 * @return 
		 */
		public function toFixed(fractionDigits:uint):String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a string representation of the number either in exponential notation or in fixed-point notation. The string will contain the number of digits specified in the <code>precision</code> parameter. </p>
		 * 
		 * @param precision  — An integer between 1 and 21, inclusive, that represents the desired number of digits to represent in the resulting string. 
		 * @return 
		 */
		public function toPrecision(precision:uint):String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the string representation of a uint object. </p>
		 * 
		 * @param radix  — Specifies the numeric base (from 2 to 36) to use for the number-to-string conversion. If you do not specify the <code>radix</code> parameter, the default value is <code>10</code>. 
		 * @return  — The string representation of the uint object. 
		 */
		public function toString(radix:uint=10):String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the primitive uint type value of the specified uint object. </p>
		 * 
		 * @return  — The primitive uint type value of this uint object. 
		 */
		/*public function valueOf():uint {
			throw new Error("Not implemented");
		}*/
	}
}
