package fr.lenra.playerglobal.utils {
	public class ColorUtil {
		/**
		 * Format color to RGBA string
		 * @param	color The color
		 * @param	alpha The alpha
		 * @return The alpha and color formated to RGBA string
		 */
		public static function toColorString(color:int, alpha:Number = 1):String {
			var r:int = (color >> 16) & 0xFF;
			var g:int = (color >> 8) & 0xFF;
			var b:int = color & 0xFF;
			alpha = Math.min(1, Math.max(0, alpha));
			return "rgba(" + r + ", " + g + ", " + b + ", " + alpha + ")";
		}
	}
}