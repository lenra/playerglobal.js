package fr.lenra.playerglobal.utils {
	public class BlobUtil {
		
		{
			self["URL"] = self["URL"] || self["webkitURL"];
			self["BlobBuilder"] = self["BlobBuilder"] || self["WebkitBlobBuilder"] || self["MozBlobBuilder"];
		}
		
		public static function jsStringToBlob(str:String):Blob {
			try {
				return new Blob([str], { type: "text/javascript" } as BlobPropertyBag);
			}
			catch (e:Error) {
				var bb:* = new self["BlobBuilder"]();
				bb.append(str, "text/javascript");
				return bb.getBlob();
			}
		}
		
		public static function blobToURL(blob:Blob):* {
			return self["URL"].createObjectURL(blob);
		}
	}
}