package flash.text.engine {
	/**
	 *  The RenderingMode class provides values for rendering mode in the FontDescription class. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="FontDescription.html" target="">FontDescription</a>
	 * </div><br><hr>
	 */
	public class RenderingMode {
		/**
		 * <p> Sets rendering mode to CFF (Compact Font Format). CFF rendering improves readability of text on a display. This setting is recommended for applications that have a lot of small text. This constant is used for the <code>renderingMode</code> property in the FontDescription class. Use the syntax <code>RenderingMode.CFF</code>. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="FontDescription.html#renderingMode" target="">flash.text.engine.FontDescription.renderingMode</a>
		 * </div>
		 */
		public static const CFF:String = "cff";
		/**
		 * <p> Sets rendering mode to the rendering mode that is used in Flash Player 7 and earlier. This setting is recommended for animated text. This constant is used for the <code>renderingMode</code> property in the FontDescription class. Use the syntax <code>RenderingMode.NORMAL</code>. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="FontDescription.html#renderingMode" target="">flash.text.engine.FontDescription.renderingMode</a>
		 * </div>
		 */
		public static const NORMAL:String = "normal";
	}
}
