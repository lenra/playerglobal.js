package flash.text.engine {
	/**
	 *  The DigitCase class is an enumeration of constant values used in setting the <code>digitCase</code> property of the ElementFormat class. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="ElementFormat.html#digitCase" target="">flash.text.engine.ElementFormat.digitCase</a>
	 * </div><br><hr>
	 */
	public class DigitCase {
		/**
		 * <p> Used to specify default digit case. The results are font-dependent; characters use the settings specified by the font designer without any features applied. </p>
		 */
		public static const DEFAULT:String = "default";
		/**
		 * <p> Used to specify lining digit case. </p>
		 */
		public static const LINING:String = "lining";
		/**
		 * <p> Used to specify old style digit case. </p>
		 */
		public static const OLD_STYLE:String = "oldStyle";
	}
}
