package flash.text.engine {
	/**
	 *  The SpaceJustifier class represents properties that control the justification options for text lines in a text block. <p> Use the constructor <code>new SpaceJustifier()</code> to create a SpaceJustifier object before setting its properties. Setting the properties of a SpaceJustifier object after you apply it to a TextBlock does not invalidate the TextBlock.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS9dd7ed846a005b294b857bfa122bd808ea6-7fed.html" target="_blank">Justifying text</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="LineJustification.html" target="">LineJustification</a>
	 *  <br>
	 *  <a href="TextBlock.html#textJustifier" target="">TextBlock.textJustifier</a>
	 *  <br>
	 *  <a href="TextJustifier.html" target="">TextJustifier</a>
	 * </div><br><hr>
	 */
	public class SpaceJustifier extends TextJustifier {
		private var _letterSpacing:Boolean;
		private var _maximumSpacing:Number;
		private var _minimumSpacing:Number;
		private var _optimumSpacing:Number;

		/**
		 * <p> Creates a SpaceJustifier object. The LineJustification class contains constants for specifying the types of line justification that you can apply. </p>
		 * 
		 * @param locale  — The locale to determine the justification rules. The default value is <code>"en"</code>. 
		 * @param lineJustification  — The type of line justification for the paragraph. Use <code>LineJustification</code> constants for this property. The default value is <code>LineJustification.UNJUSTIFIED</code>. 
		 * @param letterSpacing  — Specifies whether to use letter spacing during justification. The default value is <code>false</code>. 
		 */
		public function SpaceJustifier(locale:String = "en", lineJustification:String = "unjustified", letterSpacing:Boolean = false) {
			super(locale, lineJustification);
			this.letterSpacing = letterSpacing;
		}

		/**
		 * <p> Specifies whether to use letter spacing during justification. </p>
		 * <p>The default value is <code>false</code></p>
		 * 
		 * @return 
		 */
		public function get letterSpacing():Boolean {
			return _letterSpacing;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set letterSpacing(value:Boolean):void {
			_letterSpacing = value;
		}

		/**
		 * <p> Specifies the maximum spacing (as a multiplier of the width of a normal space) between words to use during justification. If <code>letterSpacing</code> is <code>true</code>, letter spacing will be used after the spaces between words reach the maximum. If <code>letterSpacing</code> is <code>false</code>, the spaces between words will be expanded beyond the maximum. </p>
		 * <p>The default value is <code>1.5</code></p>
		 * 
		 * @return 
		 */
		public function get maximumSpacing():Number {
			return _maximumSpacing;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set maximumSpacing(value:Number):void {
			_maximumSpacing = value;
		}

		/**
		 * <p> Specifies the minimum spacing (as a multiplier of the width of a normal space) between words to use during justification. </p>
		 * <p>The default value is <code>0.5</code></p>
		 * 
		 * @return 
		 */
		public function get minimumSpacing():Number {
			return _minimumSpacing;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set minimumSpacing(value:Number):void {
			_minimumSpacing = value;
		}

		/**
		 * <p> Specifies the optimum spacing (as a multiplier of the width of a normal space) between words to use during justification. </p>
		 * <p>The default value is <code>1.0</code></p>
		 * 
		 * @return 
		 */
		public function get optimumSpacing():Number {
			return _optimumSpacing;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set optimumSpacing(value:Number):void {
			_optimumSpacing = value;
		}

		/**
		 * <p> Constructs a cloned copy of the SpaceJustifier. </p>
		 * 
		 * @return  — A copy of the  object. 
		 */
		override public function clone():TextJustifier {
			throw new Error("Not implemented");
		}
	}
}
