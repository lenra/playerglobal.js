package flash.text.engine {
	/**
	 *  The TabStop class represents the properties of a tab stop in a text block. You assign tab stops as a Vector of TabStop objects to the <code>TextBlock.tabStops</code> property. <p>Setting the properties of a TabStop object after you apply it to a TextBlock does not invalidate the TextBlock.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS9dd7ed846a005b294b857bfa122bd808ea6-7fe9.html" target="_blank">Tab stops</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="TextBlock.html#tabStops" target="">TextBlock.tabStops</a>
	 *  <br>
	 *  <a href="TabAlignment.html" target="">TabAlignment</a>
	 * </div><br><hr>
	 */
	public class TabStop {
		private var _alignment:String;
		private var _decimalAlignmentToken:String;
		private var _position:Number;

		/**
		 * <p> Creates a new TabStop. </p>
		 * 
		 * @param alignment  — The tab alignment type of this tab stop. Valid values for this property are found in the members of the <code>TabAlignment</code> class. The default value is <code>TabAlignment.START</code>. 
		 * @param position  — The position of the tab stop, in pixels. The default value is <code>0.0</code>. 
		 * @param decimalAlignmentToken  — The alignment token to be used if the <code>alignment</code> is <code>TabAlignment.DECIMAL</code>, The default value is <code>""</code>. 
		 */
		public function TabStop(alignment:String = "start", position:Number = 0.0, decimalAlignmentToken:String = "") {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Specifies the tab alignment for this tab stop. Use the constants in the TabAlignment class to set this property. </p>
		 * <p>The default value is <code>TabAlignment.START</code>.</p>
		 * <p>Use the <code>lineOffset</code> argument to <code>TextBlock.createTextLine()</code> to adjust the tabs if the origin of the line does not align with other lines that share the same tab stops.</p>
		 * <p>Use the following constants from the TabAlignment class to set the value for this property:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>String value</th>
		 *    <th>Description</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TabAlignment.START</code></td>
		 *    <td>The <code>position</code> property specifies the number of pixels that the <i>start</i> of the tabbed text is from the start of the text line.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TabAlignment.CENTER</code></td>
		 *    <td>The <code>position</code> property specifies the number of pixels that the <i>center</i> of the tabbed text is from the start of the text line.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TabAlignment.END</code></td>
		 *    <td>The <code>position</code> property specifies the number of pixels that the <i>end</i> of the tabbed text is from the start of the text line.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TabAlignment.DECIMAL</code></td>
		 *    <td>The <code>position</code> property specifies the number of pixels that the alignment token is from the start of the text line.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * 
		 * @return 
		 */
		public function get alignment():String {
			return _alignment;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set alignment(value:String):void {
			_alignment = value;
		}

		/**
		 * <p> Specifies the alignment token to use when you set the <code>alignment</code> property to <code>TabAlignment.DECIMAL</code>. The value is a String that occurs in the text line. </p>
		 * <p>The default value is <code>""</code>.</p>
		 * 
		 * @return 
		 */
		public function get decimalAlignmentToken():String {
			return _decimalAlignmentToken;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set decimalAlignmentToken(value:String):void {
			_decimalAlignmentToken = value;
		}

		/**
		 * <p> The position of the tab stop, in pixels, relative to the start of the text line. </p>
		 * <p>The default value is 0.0.</p>
		 * 
		 * @return 
		 */
		public function get position():Number {
			return _position;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set position(value:Number):void {
			_position = value;
		}
	}
}
