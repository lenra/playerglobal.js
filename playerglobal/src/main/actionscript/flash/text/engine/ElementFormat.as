package flash.text.engine {
	/**
	 *  The ElementFormat class represents formatting information which can be applied to a ContentElement. Use the ElementFormat class to create specific text formatting for the various subclasses of ContentElement. The properties of the ElementFormat class apply to device and embedded fonts. <p>An ElementFormat object that is applied to a ContentElement in a TextBlock does not invalidate the TextBlock. Once an ElementFormat has been applied to a ContentElement, its <code>locked</code> property is set to <code>true</code>. The properties of a locked ElementFormat object cannot be changed. Instead, use the <code>clone()</code> method to create an unlocked copy of the object, which can be modified and assigned to the ContentElement.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS9dd7ed846a005b294b857bfa122bd808ea6-7ffa.html" target="_blank">Formatting text</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="ContentElement.html#elementFormat" target="">ContentElement.elementFormat</a>
	 * </div><br><hr>
	 */
	public class ElementFormat {
		private var _alignmentBaseline:String;
		private var _alpha:Number;
		private var _baselineShift:Number;
		private var _breakOpportunity:String;
		private var _color:uint;
		private var _digitCase:String;
		private var _digitWidth:String;
		private var _dominantBaseline:String;
		private var _fontDescription:FontDescription;
		private var _fontSize:Number;
		private var _kerning:String;
		private var _ligatureLevel:String;
		private var _locale:String;
		private var _locked:Boolean;
		private var _textRotation:String;
		private var _trackingLeft:Number;
		private var _trackingRight:Number;
		private var _typographicCase:String;

		/**
		 * <p> Creates an ElementFormat object. </p>
		 * 
		 * @param fontDescription  — The FontDescription object which identifies the font used with this element format. The default value is <code>null</code>. If no font description is provided, a default font description is constructed. 
		 * @param fontSize  — The size of text in pixels. 
		 * @param color  — The color of text. A hexadecimal number containing three 8-bit RGB components; for example, 0xFF0000 is red and 0x00FF00 is green. 
		 * @param alpha  — The <code>alpha</code> property applied to all line atoms based on the element format. 
		 * @param textRotation  — the rotation applied to individual glyphs. Use TextRotation constants for this property. 
		 * @param dominantBaseline  — The baseline to which the glyphs in the text snap. Use TextBaseline constants for this property. 
		 * @param alignmentBaseline  — The baseline on the containing line to which the dominant baseline snaps. Use TextBaseline constants for this property. 
		 * @param baselineShift  — The baseline shift for the text in pixels em. 
		 * @param kerning  — The kerning used for this text. Use constants defined in the Kerning class. 
		 * @param trackingRight  — The tracking or manual kerning applied to the right of each glyph in pixels. 
		 * @param trackingLeft  — The tracking or manual kerning applied to the left of each glyph in pixels. 
		 * @param locale  — The locale of the text. 
		 * @param breakOpportunity  — The line break opportunity applied to this text. Use BreakOpportunity constants for this property. 
		 * @param digitCase  — The digit case used for this text. Use DigitCase constants for this property. 
		 * @param digitWidth  — The digit width used for this text. Use DigitWidth constants for this property. 
		 * @param ligatureLevel  — The ligature level used for this text. Use LigatureLevel constants for this property. 
		 * @param typographicCase  — The typographic case used for this text. Use TypographicCase constants for this property. 
		 */
		public function ElementFormat(fontDescription:FontDescription = null, fontSize:Number = 12.0, color:uint = 0x000000, alpha:Number = 1.0, textRotation:String = "auto", dominantBaseline:String = "roman", alignmentBaseline:String = "useDominantBaseline", baselineShift:Number = 0.0, kerning:String = "on", trackingRight:Number = 0.0, trackingLeft:Number = 0.0, locale:String = "en", breakOpportunity:String = "auto", digitCase:String = "default", digitWidth:String = "default", ligatureLevel:String = "common", typographicCase:String = "default") {
			this.fontDescription = fontDescription;
			this.fontSize = fontSize;
			this.color = color;
			this.alpha = alpha;
			this.textRotation = textRotation;
			this.dominantBaseline = dominantBaseline;
			this.alignmentBaseline = alignmentBaseline;
			this.baselineShift = baselineShift;
			this.kerning = kerning;
			this.trackingRight = trackingRight;
			this.trackingLeft = trackingLeft;
			this.locale = locale;
			this.breakOpportunity = breakOpportunity;
			this.digitCase = digitCase;
			this.digitWidth = digitWidth;
			this.ligatureLevel = ligatureLevel;
			this.typographicCase = typographicCase;
		}

		/**
		 * <p> Specifies the type of baseline in the containing element to which to align the dominant baselines of elements having this format. Use <code>TextBaseline</code> constants for this property. </p>
		 * <p>The largest vertical element in the line determines the alignment of baselines unless <code>TextBlock.baselineFontDescription</code> and <code>TextBlock.baselineFontSize</code> are set to override that logic.</p>
		 * <p>The default value is <code>TextBaseline.USE_DOMINANT_BASELINE</code>.</p>
		 * <p>To set values for this property, use the following string values:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>String value</th>
		 *    <th>Description</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextBaseline.ROMAN</code></td>
		 *    <td>The <code>dominantBaseline</code> aligns with the roman baseline of the line.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextBaseline.ASCENT</code></td>
		 *    <td>The <code>dominantBaseline</code> aligns with the ascent baseline of the line.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextBaseline.DESCENT</code></td>
		 *    <td>The <code>dominantBaseline</code> aligns with the descent baseline of the line.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextBaseline.IDEOGRAPHIC_TOP</code></td>
		 *    <td>The <code>dominantBaseline</code> aligns with the ideographic top baseline of the line.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextBaseline.IDEOGRAPHIC_CENTER</code></td>
		 *    <td>The <code>dominantBaseline</code> aligns with the ideographic center baseline of the line.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextBaseline.IDEOGRAPHIC_BOTTOM</code></td>
		 *    <td>The <code>dominantBaseline</code> aligns with the ideographic bottom baseline of the line.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextBaseline.USE_DOMINANT_BASELINE</code></td>
		 *    <td>The <code>dominantBaseline</code> aligns with the same baseline of the line.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Subclass</th>
		 *    <th>Effect of setting property</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>GraphicElement</code></td>
		 *    <td>Sets the alignment baseline of the line to which the <code>dominantBaseline</code> of the graphic element aligns.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>GroupElement</code></td>
		 *    <td>Has no effect.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextElement</code></td>
		 *    <td>Sets the alignment baseline of the line to which the <code>dominantBaseline</code> of the text element aligns.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * 
		 * @return 
		 */
		public function get alignmentBaseline():String {
			return _alignmentBaseline;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set alignmentBaseline(value:String):void {
			_alignmentBaseline = value;
		}

		/**
		 * <p> Specifies the transparency of the line elements affected by this obect. Valid values range from 0 (fully transparent) to 1 (fully opaque). Display objects with <code>alpha</code> set to 0 are active, even though they are invisible. </p>
		 * <p>The default value is <code>1</code>.</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Subclass</th>
		 *    <th>Effect of setting property</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>GraphicElement</code></td>
		 *    <td>Applies the specified alpha to the graphic element. Combines multiplicatively with any alpha set on the graphic <code>DisplayObject</code> itself or on the <code>TextLine</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>GroupElement</code></td>
		 *    <td>Has no effect.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextElement</code></td>
		 *    <td>Applies the specified alpha to the text element. Combines multiplicatively with any alpha set on the <code>TextLine</code>.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * 
		 * @return 
		 */
		public function get alpha():Number {
			return _alpha;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set alpha(value:Number):void {
			_alpha = value;
		}

		/**
		 * <p> Indicates the baseline shift for the element in pixels. </p>
		 * <p>The element is shifted away from the <code>dominantBaseline</code> by this amount. The offset is added to the y position of the members of the element, so in non-rotated text, a positive baseline shift moves the element down and a negative baseline shift moves the element up.</p>
		 * <p>The default value is <code>0.0</code>, indicating no shift.</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Subclass</th>
		 *    <th>Effect of setting property</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>GraphicElement</code></td>
		 *    <td>Shifts the graphic away from the baseline.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>GroupElement</code></td>
		 *    <td>Has no effect.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextElement</code></td>
		 *    <td>Shifts the text away from the baseline.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * 
		 * @return 
		 */
		public function get baselineShift():Number {
			return _baselineShift;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set baselineShift(value:Number):void {
			_baselineShift = value;
		}

		/**
		 * <p> The line break opportunity applied to this text. This property determines which characters can be used for breaking when wrapping text is broken into multiple lines. Use <code>BreakOpportunity</code> constants for this property. </p>
		 * <p>The default value is <code>BreakOpportunity.AUTO</code>.</p>
		 * <p>To set values for this property, use the following string values:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>String value</th>
		 *    <th>Description</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>BreakOpportunity.AUTO</code></td>
		 *    <td>Line breaking opportunities are based on standard Unicode character properties, such as breaking between words and on hyphens.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>BreakOpportunity.ANY</code></td>
		 *    <td>Any character in the <code>ContentElement</code> object is treated as a line break opportunity. This value is typically used when Roman text is embedded in Asian text and it is desirable for breaks to happen in the middle of words.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>BreakOpportunity.NONE</code></td>
		 *    <td>No characters in the range are treated as line break opportunities.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>BreakOpportunity.ALL</code></td>
		 *    <td>All characters in the range are treated as line break opportunities, meaning that a line break will occur after each character. Useful for creating effects like text on a path.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Subclass</th>
		 *    <th>Effect of setting property</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>GraphicElement</code></td>
		 *    <td>Has no effect.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>GroupElement</code></td>
		 *    <td>Determines the break opportunity between adjacent text elements in the group. If the elementFormat of the group is <code>null</code>, the format of the first of the adjacent elements is used.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextElement</code></td>
		 *    <td>Determines the break opportunity between the characters in the text element.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * 
		 * @return 
		 */
		public function get breakOpportunity():String {
			return _breakOpportunity;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set breakOpportunity(value:String):void {
			_breakOpportunity = value;
		}

		/**
		 * <p> Indicates the color of the text. An integer containing three 8-bit RGB components; for example, 0xFF0000 is red and 0x00FF00 is green. </p>
		 * <p>The default value is <code>0x000000</code>, which is black.</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Subclass</th>
		 *    <th>Effect of setting property</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>GraphicElement</code></td>
		 *    <td>Has no effect.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>GroupElement</code></td>
		 *    <td>Has no effect.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextElement</code></td>
		 *    <td>Sets the color of the text.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * 
		 * @return 
		 */
		public function get color():uint {
			return _color;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set color(value:uint):void {
			_color = value;
		}

		/**
		 * <p> The digit case used for this text. Digit case affects the style and positioning of groups of numeric characters. Use <code>DigitCase</code> constants for this property. </p>
		 * <p>The default value is <code>DigitCase.DEFAULT</code>.</p>
		 * <p>To set values for this property, use the following string values:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>String value</th>
		 *    <th>Description</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>DigitCase.DEFAULT</code></td>
		 *    <td>Applies default digit case to the text.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>DigitCase.LINING</code></td>
		 *    <td>Applies lining digit case to the text.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>DigitCase.OLD_STYLE</code></td>
		 *    <td>Applies old style digit case to the text.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Subclass</th>
		 *    <th>Effect of setting property</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>GraphicElement</code></td>
		 *    <td>Has no effect.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>GroupElement</code></td>
		 *    <td>Has no effect.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextElement</code></td>
		 *    <td>Determines the digit case used for the text in the element.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * 
		 * @return 
		 */
		public function get digitCase():String {
			return _digitCase;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set digitCase(value:String):void {
			_digitCase = value;
		}

		/**
		 * <p> The digit width used for this text. Use <code>DigitWidth</code> constants for this property. </p>
		 * <p>The default value is <code>DigitWidth.DEFAULT</code>.</p>
		 * <p>To set values for this property, use the following string values:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>String value</th>
		 *    <th>Description</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>DigitWidth.DEFAULT</code></td>
		 *    <td>Applies default digit width to the text.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>DigitWidth.PROPORTIONAL</code></td>
		 *    <td>Applies proportional digit width to the text.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>DigitWidth.TABULAR</code></td>
		 *    <td>Applies tabular digit width to the text.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Subclass</th>
		 *    <th>Effect of setting property</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>GraphicElement</code></td>
		 *    <td>Has no effect.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>GroupElement</code></td>
		 *    <td>Has no effect.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextElement</code></td>
		 *    <td>Determines the digit width used for the text in the element.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * 
		 * @return 
		 */
		public function get digitWidth():String {
			return _digitWidth;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set digitWidth(value:String):void {
			_digitWidth = value;
		}

		/**
		 * <p> Specifies the type of baseline to use as the dominant baseline. The dominant baseline is aligned with the alignment baseline to determine the vertical position of the element on the line. Use <code>TextBaseline</code> constants for this property. </p>
		 * <p>The content of the element determines the baselines. In the case of a <code>TextElement</code>, the font and the point size determine the baselines. In the case of a <code>GraphicElement</code>, the height of the element determines the baselines.</p>
		 * <p>The default value is <code>TextBaseline.ROMAN</code>.</p>
		 * <p>To set values for this property, use the following string values:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>String value</th>
		 *    <th>Description</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextBaseline.ROMAN</code></td>
		 *    <td>The roman baseline of the element aligns with the <code>alignmentBaseline</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextBaseline.ASCENT</code></td>
		 *    <td>The ascent baseline of the element aligns with the <code>alignmentBaseline</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextBaseline.DESCENT</code></td>
		 *    <td>The descent baseline of the element aligns with the <code>alignmentBaseline</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextBaseline.IDEOGRAPHIC_TOP</code></td>
		 *    <td>The ideographic top baseline of the element aligns with the <code>alignmentBaseline</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextBaseline.IDEOGRAPHIC_CENTER</code></td>
		 *    <td>The ideographic center baseline of the element aligns with the <code>alignmentBaseline</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextBaseline.IDEOGRAPHIC_BOTTOM</code></td>
		 *    <td>The ideographic bottom baseline of the element aligns with the <code>alignmentBaseline</code>.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Subclass</th>
		 *    <th>Effect of setting property</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>GraphicElement</code></td>
		 *    <td>Determines which of the baselines of the graphic element aligns with the <code>alignmentBaseline</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>GroupElement</code></td>
		 *    <td>Has no effect.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextElement</code></td>
		 *    <td>Determines which of the baselines of the text element aligns with the <code>alignmentBaseline</code>.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * 
		 * @return 
		 */
		public function get dominantBaseline():String {
			return _dominantBaseline;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set dominantBaseline(value:String):void {
			_dominantBaseline = value;
		}

		/**
		 * <p> An object whose properties describe a font. </p>
		 * <p>The default value is a default-constructed FontDescription object.</p>
		 * <p>When the fontDescription property is set, the FontDescription object provided is locked: its <code>locked</code> property is set to <code>true</code>. A locked FontDescription cannot be modified.</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Subclass</th>
		 *    <th>Effect of setting property</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>GraphicElement</code></td>
		 *    <td>Has no effect.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>GroupElement</code></td>
		 *    <td>Has no effect.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextElement</code></td>
		 *    <td>Determines the font used for the text in the element.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * 
		 * @return 
		 */
		public function get fontDescription():FontDescription {
			return _fontDescription;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set fontDescription(value:FontDescription):void {
			_fontDescription = value;
		}

		/**
		 * <p> The size of text in pixels. </p>
		 * <p>The default value is <code>12.0</code>.</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Subclass</th>
		 *    <th>Effect of setting property</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>GraphicElement</code></td>
		 *    <td>Has no effect.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>GroupElement</code></td>
		 *    <td>Has no effect.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextElement</code></td>
		 *    <td>Determines the size in pixels for the text in the element.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * 
		 * @return 
		 */
		public function get fontSize():Number {
			return _fontSize;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set fontSize(value:Number):void {
			_fontSize = value;
		}

		/**
		 * <p> Kerning adjusts the pixels between certain character pairs to improve readability. Kerning is supported for all fonts which have kerning tables. </p>
		 * <p>The default value is <code>Kerning.ON</code>.</p>
		 * <p>To set values for this property, use the following constants in the Kerning class:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>String value</th>
		 *    <th>Description</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>Kerning.ON</code></td>
		 *    <td>Kerning is enabled.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>Kerning.OFF</code></td>
		 *    <td>Kerning is disabled.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>Kerning.AUTO</code></td>
		 *    <td>Kerning is enabled except where inappropriate in Asian typography. Kerning is applied between two characters if neither is Kanji, Hiragana, or Katakana.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Subclass</th>
		 *    <th>Effect of setting property</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>GraphicElement</code></td>
		 *    <td>Has no effect.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>GroupElement</code></td>
		 *    <td>Determines whether kerning is applied between adjacent text elements in the group. If the elementFormat of the group is <code>null</code>, the format of the first of the adjacent elements is used.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextElement</code></td>
		 *    <td>Determines whether kerning is applied between the characters in the text element.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * 
		 * @return 
		 */
		public function get kerning():String {
			return _kerning;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set kerning(value:String):void {
			_kerning = value;
		}

		/**
		 * <p> The ligature level used for this text. A ligature occurs where two or more letter-forms are joined as a single glyph. Ligatures usually replace consecutive characters sharing common components, such as the letter pairs 'fi', 'fl', or 'ae'. They are used with both Latin and non-Latin character sets. Use <code>LigatureLevel</code> constants for this property. </p>
		 * <p>The default value is <code>LigatureLevel.COMMON</code>.</p>
		 * <p>To set values for this property, use the following string values:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>String value</th>
		 *    <th>Description</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>LigatureLevel.NONE</code></td>
		 *    <td>No ligatures are created.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>LigatureLevel.MINIMUM</code></td>
		 *    <td>Minimal ligatures are created.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>LigatureLevel.COMMON</code></td>
		 *    <td>Common ligatures are created.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>LigatureLevel.UNCOMMON</code></td>
		 *    <td>Uncommon ligatures are created.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>LigatureLevel.EXOTIC</code></td>
		 *    <td>Exotic ligatures are created.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Subclass</th>
		 *    <th>Effect of setting property</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>GraphicElement</code></td>
		 *    <td>Has no effect.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>GroupElement</code></td>
		 *    <td>Determines the ligature level between adjacent text elements in the group. If the elementFormat of the group is <code>null</code>, the format of the first of the adjacent elements is used.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextElement</code></td>
		 *    <td>Determines the ligature level between the characters in the text element.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * 
		 * @return 
		 */
		public function get ligatureLevel():String {
			return _ligatureLevel;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set ligatureLevel(value:String):void {
			_ligatureLevel = value;
		}

		/**
		 * <p> The locale of the text. Controls case transformations and shaping. Standard locale identifiers are used. For example "en", "en_US" and "en-US" are all English, "ja" is Japanese. See <a href="http://www.loc.gov/standards/iso639-2/php/code_list.php" target="external">iso639-2 code list</a> for a list of locale codes. </p>
		 * <p>The default value is <code>"en"</code>.</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Subclass</th>
		 *    <th>Effect of setting property</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>GraphicElement</code></td>
		 *    <td>Has no effect.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>GroupElement</code></td>
		 *    <td>Has no effect.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextElement</code></td>
		 *    <td>Determines transformations and shaping for the text in the element.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * 
		 * @return 
		 */
		public function get locale():String {
			return _locale;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set locale(value:String):void {
			_locale = value;
		}

		/**
		 * <p> Indicates whether the ElementFormat is locked. If <code>true</code> the ElementFormat cannot be modified. Call <code>ElementFormat.clone()</code> to get an unlocked copy of the ElementFormat object. </p>
		 * 
		 * @return 
		 */
		public function get locked():Boolean {
			return _locked;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set locked(value:Boolean):void {
			_locked = value;
		}

		/**
		 * <p> Sets the rotation applied to individual glyphs. Use constants defined in the TextRotation class for this property. </p>
		 * <p>The default value is <code>TextRotation.AUTO</code>.</p>
		 * <p>The final rotation of any glyph is the sum of <code>ElementFormat.textRotation</code>, <code>ContentElement.textRotation</code>, and <code>TextBlock.lineRotation</code>.</p>
		 * <p>You typically use this property for Asian text where characters must be rotated to display properly in vertical layout. Use <code>TextRotation.AUTO</code> in combination with <code>TextBlock.lineRotation = TextRotation.ROTATE_90</code> to accomplish this.</p>
		 * <p>Setting this property on fonts which do not contain vertical layout information can give undesirable results. Fonts that contain a vmtx or VORG table, such as the Japanese font, "MS Mincho", work correctly because the tables supply the data that the layout engine requires for correct layout. Fonts such as Verdana, which do not contain the necessary information, do not.</p>
		 * <p>To set values for this property, use the following string values:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>String value</th>
		 *    <th>Description</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextRotation.ROTATE_0</code></td>
		 *    <td>Glyphs are not rotated.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextRotation.ROTATE_90</code></td>
		 *    <td>Glyphs are rotated 90 degrees clockwise.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextRotation.ROTATE_180</code></td>
		 *    <td>Glyphs are rotated 180 degrees.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextRotation.ROTATE_270</code></td>
		 *    <td>Glyphs are rotated 270 degrees clockwise.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextRotation.AUTO</code></td>
		 *    <td>Specifies a 90 degree counter clockwise rotation for full width and wide glyphs only, as determined by the Unicode properties of the glyph. This value is typically used with Asian text to rotate only those glyphs that require rotation. This rotation is applied only in vertical text to return full width and wide characters to a vertical orientation without affecting other characters.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Subclass</th>
		 *    <th>Effect of setting property</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>GraphicElement</code></td>
		 *    <td>Has no effect.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>GroupElement</code></td>
		 *    <td>Has no effect.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextElement</code></td>
		 *    <td>Determines the rotation of the glyphs in the text element.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * 
		 * @return 
		 */
		public function get textRotation():String {
			return _textRotation;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set textRotation(value:String):void {
			_textRotation = value;
		}

		/**
		 * <p> The tracking or manual kerning applied to the left of each glyph in pixels. If <code>kerning</code> is enabled, the <code>trackingLeft</code> value is added to the values in the kerning table for the font. If <code>kerning</code> is disabled, the <code>trackingLeft</code> value is used as a manual kerning value. Supports both positive and negative values. </p>
		 * <p>Typically, the desired tracking value is split between <code>trackingRight</code> and <code>trackingLeft</code>. Otherwise, in mixed directionality text, there is twice the tracking at one bidi boundary and none at the other.</p>
		 * <p>The default value is <code>0.0</code>.</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Subclass</th>
		 *    <th>Effect of setting property</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>GraphicElement</code></td>
		 *    <td>Determines the tracking applied to the left side of the graphic.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>GroupElement</code></td>
		 *    <td>Has no effect.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextElement</code></td>
		 *    <td>Determines the tracking applied to the left side of characters in the text element.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p>Example:</p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *           //positive tracking added to kerning
		 *           var ef1:ElementFormat = new ElementFormat();
		 *           ef1.kerning = flash.text.engine.Kerning.ON;
		 *           ef1.trackingLeft = 0.5;
		 *      
		 *           //negative manual kerning
		 *           var ef2:ElementFormat = new ElementFormat();
		 *           ef2.kerning = flash.text.engine.Kerning.OFF;
		 *           ef2.trackingLeft = -1.0;
		 *      </pre>
		 * </div>
		 * 
		 * @return 
		 */
		public function get trackingLeft():Number {
			return _trackingLeft;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set trackingLeft(value:Number):void {
			_trackingLeft = value;
		}

		/**
		 * <p> The tracking or manual kerning applied to the right of each glyph in pixels. If <code>kerning</code> is enabled, the <code>trackingRight</code> value is added to the values in the kerning table for the font. If <code>kerning</code> is disabled, the <code>trackingRight</code> value is used as a manual kerning value. Supports both positive and negative values. </p>
		 * <p>Typically, the desired tracking value is split between <code>trackingRight</code> and <code>trackingLeft</code>. Otherwise, in mixed directionality text, there is twice the tracking at one bidi boundary and none at the other.</p>
		 * <p>The default value is <code>0.0</code>.</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Subclass</th>
		 *    <th>Effect of setting property</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>GraphicElement</code></td>
		 *    <td>Determines the tracking applied to the right side of the graphic.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>GroupElement</code></td>
		 *    <td>Has no effect.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextElement</code></td>
		 *    <td>Determines the tracking applied to the right side of characters in the text element.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p>Example:</p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *           //positive tracking added to kerning
		 *           var ef1:ElementFormat = new ElementFormat();
		 *           ef1.kerning = flash.text.engine.Kerning.ON;
		 *           ef1.trackingRight = 0.5;
		 *      
		 *           //negative manual kerning
		 *           var ef2:ElementFormat = new ElementFormat();
		 *           ef2.kerning = flash.text.engine.Kerning.OFF;
		 *           ef2.trackingRight = -1.0;
		 *      </pre>
		 * </div>
		 * 
		 * @return 
		 */
		public function get trackingRight():Number {
			return _trackingRight;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set trackingRight(value:Number):void {
			_trackingRight = value;
		}

		/**
		 * <p> The typographic case used for this text. Use constants defined in the TypographicCase class for this property. </p>
		 * <p>The default value is <code>TypographicCase.DEFAULT</code>.</p>
		 * <p>To set values for this property, use the following string values:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>String value</th>
		 *    <th>Description</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TypographicCase.DEFAULT</code></td>
		 *    <td>Specifies that normal case is used for all characters.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TypographicCase.TITLE</code></td>
		 *    <td>Specifies that uppercase characters use title glyphs on output.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TypographicCase.CAPS</code></td>
		 *    <td>Specifies that spacing is adjusted for uppercase characters on output.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TypographicCase.SMALL_CAPS</code></td>
		 *    <td>Specifies that uppercase characters use small caps glyphs on output.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TypographicCase.UPPERCASE</code></td>
		 *    <td>Specifies that all characters use uppercase glyphs on output.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TypographicCase.LOWERCASE</code></td>
		 *    <td>Specifies that all characters use lowercase glyphs on output.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TypographicCase.CAPS_AND_SMALL_CAPS</code></td>
		 *    <td>Specifies that all lowercase characters use small caps glyphs on output.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Subclass</th>
		 *    <th>Effect of setting property</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>GraphicElement</code></td>
		 *    <td>Has no effect.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>GroupElement</code></td>
		 *    <td>Has no effect.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextElement</code></td>
		 *    <td>Determines the typographic case used for the text in the element.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * 
		 * @return 
		 */
		public function get typographicCase():String {
			return _typographicCase;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set typographicCase(value:String):void {
			_typographicCase = value;
		}

		/**
		 * <p> Constructs an unlocked, cloned copy of the ElementFormat. </p>
		 * 
		 * @return  — An unlocked copy of the  object. 
		 */
		public function clone():ElementFormat {
			return new ElementFormat(this._fontDescription, this._fontSize, this._color, this._alpha, this._textRotation, this._dominantBaseline, this._alignmentBaseline, this._baselineShift, this._kerning, this._trackingRight, this._trackingLeft, this._locale, this._breakOpportunity, this._digitCase, this._digitWidth, this._ligatureLevel, this._typographicCase);
		}

		/**
		 * <p> </p>
		 * <p>Returns a <code>FontMetrics</code> object with properties which describe the emBox, strikethrough position, strikethrough thickness, underline position, and underline thickness for the font specified by <code>fontDescription</code> and <code>fontSize</code>.</p>
		 * 
		 * @return  — A  object describing properties of the font specified by . 
		 */
		public function getFontMetrics():FontMetrics {
			throw new Error("Not implemented");
		}
	}
}
