package flash.text.engine {
	/**
	 *  The LineJustification class is an enumeration of constant values used in setting the <code>lineJustfication</code> property of the TextJustifier subclasses. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="TextJustifier.html#lineJustification" target="">TextJustifier.lineJustification</a>
	 * </div><br><hr>
	 */
	public class LineJustification {
		/**
		 * <p> Justify all but the last line. </p>
		 */
		public static const ALL_BUT_LAST:String = "allButLast";
		/**
		 * <p> Justify all but the last line and lines ending in mandatory breaks. </p>
		 */
		public static const ALL_BUT_MANDATORY_BREAK:String = "allButMandatoryBreak";
		/**
		 * <p> Justify all lines. </p>
		 */
		public static const ALL_INCLUDING_LAST:String = "allIncludingLast";
		/**
		 * <p> Do not justify lines. </p>
		 */
		public static const UNJUSTIFIED:String = "unjustified";
	}
}
