package flash.text.engine {
	import flash.display.DisplayObject;
	import flash.events.EventDispatcher;

	/**
	 *  The GraphicElement class represents a graphic element in a TextBlock or GroupElement object. Assign a GraphicElement object to the <code>content</code> property of a TextBlock object to display a graphic or an image with <code>TextBlock.createTextLine()</code>. Assign it to a GroupElement object to combine it with other graphic and text elements. <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS9dd7ed846a005b294b857bfa122bd808ea6-7ffe.html" target="_blank">Adding GraphicElement and GroupElement objects</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="ContentElement.html" target="">ContentElement</a>
	 *  <br>
	 *  <a href="GroupElement.html" target="">GroupElement</a>
	 *  <br>
	 *  <a href="TextBlock.html" target="">TextBlock</a>
	 * </div><br><hr>
	 */
	public class GraphicElement extends ContentElement {
		private var _elementHeight:Number;
		private var _elementWidth:Number;
		private var _graphic:DisplayObject;

		/**
		 * <p> Creates a new GraphicElement instance. </p>
		 * <p>The registration point of the graphic aligns with the upper-left corner of the region defined by <code>elementHeight</code>, <code>elementWidth</code> and <code>elementFormat.baselineShift</code>. The graphic is not scaled to match the size of the region. If the GraphicElement has an <code>eventMirror</code>, the <code>elementWidth</code> and <code>elementHeight</code> properties, and not the graphic, determine the size and position of the resulting mirror region. If a loader is used, the graphic might not be loaded at the time the text line and the mirror regions are created.</p>
		 * 
		 * @param graphic  — The DisplayObject to populate the GraphicElement. The default value is <code>null</code>. 
		 * @param elementWidth  — The width of the area reserved for the element in pixels. The default value is 15. 
		 * @param elementHeight  — The height of the area reserved for the element in pixels. The default value is 15. 
		 * @param elementFormat  — The element format for the element. The default value is <code>null</code>. 
		 * @param eventMirror  — The <code>EventDispatcher</code> object that receives copies of every event dispatched to text lines created based on this content element. The default value is <code>null</code>. 
		 * @param textRotation  — The rotation applied to the element as a unit. Use <code>flash.text.engine.TextRotation</code> constants for this property. The default value is <code>flash.text.engine.TextRotation.ROTATE_0</code>. 
		 */
		public function GraphicElement(graphic:DisplayObject = null, elementWidth:Number = 15.0, elementHeight:Number = 15.0, elementFormat:ElementFormat = null, eventMirror:EventDispatcher = null, textRotation:String = "rotate0") {
			super(elementFormat, eventMirror, textRotation);
			throw new Error("Not implemented");
		}

		/**
		 * <p> The height in pixels to reserve for the graphic in the line. It is the responsibility of the caller to scale the graphic. </p>
		 * <p>The default value is 15.0.</p>
		 * 
		 * @return 
		 */
		public function get elementHeight():Number {
			return _elementHeight;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set elementHeight(value:Number):void {
			_elementHeight = value;
		}

		/**
		 * <p> The width in pixels to reserve for the graphic in the line. It is the responsibility of the caller to scale the graphic. </p>
		 * <p>The default value is 15.0.</p>
		 * 
		 * @return 
		 */
		public function get elementWidth():Number {
			return _elementWidth;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set elementWidth(value:Number):void {
			_elementWidth = value;
		}

		/**
		 * <p> The DisplayObject to be used as a graphic for the GraphicElement. </p>
		 * <p>The default value is <code>null</code>.</p>
		 * <p>When the GraphicElement becomes part of a text line, the graphic is added as a child of the line. Setting the graphic removes the old graphic from the line and adds the new one.</p>
		 * 
		 * @return 
		 */
		public function get graphic():DisplayObject {
			return _graphic;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set graphic(value:DisplayObject):void {
			_graphic = value;
		}
	}
}
