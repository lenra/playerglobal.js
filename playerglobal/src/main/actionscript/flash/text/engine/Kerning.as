package flash.text.engine {
	/**
	 *  The Kerning class is an enumeration of constant values used with <code>ElementFormat.kerning</code>. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="ElementFormat.html#kerning" target="">ElementFormat.kerning</a>
	 * </div><br><hr>
	 */
	public class Kerning {
		/**
		 * <p> Used to indicate that kerning is enabled except where inappropriate in Asian typography. Kerning is applied between two characters if neither is Kanji, Hiragana, or Katakana. </p>
		 */
		public static const AUTO:String = "auto";
		/**
		 * <p> Used to indicate kerning is disabled. </p>
		 */
		public static const OFF:String = "off";
		/**
		 * <p> Used to indicate kerning is enabled. </p>
		 */
		public static const ON:String = "on";
	}
}
