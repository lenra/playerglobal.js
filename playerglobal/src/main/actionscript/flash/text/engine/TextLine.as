package flash.text.engine {
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.events.EventDispatcher;
	import flash.geom.Rectangle;

	/**
	 *  The TextLine class is used to display text on the display list. <p>You cannot create a TextLine object directly from ActionScript code. If you call <code>new TextLine()</code>, an exception is thrown. To create a TextLine object, call the <code>createTextLine()</code> method of a TextBlock.</p> <p>The TextLine encapsulates the minimum information necessary to render its contents and to provide interactivity through some methods that describe the properties of the atoms of the line. The term atom refers to both graphic elements and characters (including groups of combining characters), the indivisible entities that make up a text line.</p> <p>After normal event-dispatching for a text line finishes, if the line is valid, events are mirrored to the event dispatchers that are specified in the <code>eventMirror</code> properties of the content element objects that contributed to the text line. These objects are recorded in the <code>TextLine.mirrorRegions</code> property. The events are not mirrored if event propagation failed or was stopped, or if the text line is not valid.</p> <p>Mirroring of mouse events is a special case. Because mirror regions aren't actually display objects, <code>mouseOver</code> and <code>mouseOut</code> events are simulated for them. <code>rollOver</code> and <code>rollOut</code> events are not simulated. All naturally occurring <code>mouseOver</code>, <code>mouseOut</code>, <code>rollOver</code> and <code>rollOut</code> events (whether targeted at the text line or at children of the text line) are ignored - they are not mirrored.</p> <p>The origin of a text line object is the beginning of the baseline. If you don't set the vertical position (<code>y</code> property) of a line that contains Latin text on a Roman baseline, only the descenders of the text appear below the top of the Sprite to which you add the text line. See the following diagram:</p> <p> <img src="../../../images/TextLine.gif" alt="Text baselines"> </p> <p>The TextLine class has several ancestor classes — DisplayObjectContainer, InteractiveObject, DisplayObject, and EventDispatcher — from which it inherits properties and methods. The following inherited properties are inapplicable to TextLine objects: </p> <ul> 
	 *  <li> <code>contextMenu</code> </li> 
	 *  <li> <code>focusRect</code> </li> 
	 *  <li> <code>tabChildren</code> </li> 
	 *  <li> <code>tabEnabled</code> </li> 
	 *  <li> <code>tabIndex</code> </li> 
	 * </ul> <p>If you try to set these properties, the text engine throws the error: IllegalOperationError. You can read these properties, but they always contain default values.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS9dd7ed846a005b294b857bfa122bd808ea6-7fff.html" target="_blank">Creating and displaying text</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS9dd7ed846a005b294b857bfa122bd808ea6-7ffc.html" target="_blank">Handling Events in FTE</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="ContentElement.html#eventMirror" target="">ContentElement.eventMirror</a>
	 *  <br>
	 *  <a href="TextBlock.html#createTextLine()" target="">TextBlock.createTextLine()</a>
	 *  <br>
	 *  <a href="TextLineValidity.html" target="">TextLineValidity</a>
	 * </div><br><hr>
	 */
	public class TextLine extends DisplayObjectContainer {
		/**
		 * <p> The maximum requested width of a text line, in pixels. The <code>TextBlock.createTextLine()</code> method uses this constant as the default value for the <code>width</code> parameter, if you do not specify a value. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="TextBlock.html#createTextLine()" target="">TextBlock.createTextLine()</a>
		 * </div>
		 */
		public static const MAX_LINE_WIDTH:int = 1000000;

		private var _userData:*;
		private var _validity:String;

		protected var _ascent:Number;
		protected var _atomCount:int;
		protected var _descent:Number;
		protected var _hasGraphicElement:Boolean;
		protected var _hasTabs:Boolean;
		protected var _mirrorRegions:Vector;
		protected var _nextLine:TextLine;
		protected var _previousLine:TextLine;
		protected var _rawTextLength:int;
		protected var _specifiedWidth:Number;
		protected var _textBlock:TextBlock;
		protected var _textBlockBeginIndex:int;
		protected var _textHeight:Number;
		protected var _textWidth:Number;
		protected var _totalAscent:Number;
		protected var _totalDescent:Number;
		protected var _totalHeight:Number;
		protected var _unjustifiedTextWidth:Number;

		/**
		 * <p> Specifies the number of pixels from the baseline to the top of the tallest characters in the line. For a TextLine that contains only a graphic element, <code>ascent</code> is set to 0. </p>
		 * 
		 * @return 
		 */
		public function get ascent():Number {
			return _ascent;
		}

		/**
		 * <p> The number of atoms in the line, which is the number of indivisible elements, including spaces and graphic elements. </p>
		 * 
		 * @return 
		 */
		public function get atomCount():int {
			return _atomCount;
		}

		/**
		 * <p> Specifies the number of pixels from the baseline to the bottom of the lowest-descending characters in the line. For a TextLine that contains only a graphic element, <code>descent</code> is set to 0. </p>
		 * 
		 * @return 
		 */
		public function get descent():Number {
			return _descent;
		}

		/**
		 * <p> Indicates whether the text line contains any graphic elements. </p>
		 * 
		 * @return 
		 */
		public function get hasGraphicElement():Boolean {
			return _hasGraphicElement;
		}

		/**
		 * <p> Indicates whether the text line contains any tabs. </p>
		 * 
		 * @return 
		 */
		public function get hasTabs():Boolean {
			return _hasTabs;
		}

		/**
		 * <p> A Vector containing the <code>TextLineMirrorRegion</code> objects associated with the line, or <code>null</code> if none exist. </p>
		 * 
		 * @return 
		 */
		public function get mirrorRegions():Vector {
			return _mirrorRegions;
		}

		/**
		 * <p> The next TextLine in the TextBlock, or <code>null</code> if the current line is the last line in the block or the validity of the line is <code>TextLineValidity.STATIC</code>. </p>
		 * 
		 * @return 
		 */
		public function get nextLine():TextLine {
			return _nextLine;
		}

		/**
		 * <p> The previous TextLine in the TextBlock, or <code>null</code> if the line is the first line in the block or the validity of the line is <code>TextLineValidity.STATIC</code>. </p>
		 * 
		 * @return 
		 */
		public function get previousLine():TextLine {
			return _previousLine;
		}

		/**
		 * <p> The length of the raw text in the text block that became the line, including the U+FDEF characters representing graphic elements and any trailing spaces, which are part of the line but not are displayed. </p>
		 * 
		 * @return 
		 */
		public function get rawTextLength():int {
			return _rawTextLength;
		}

		/**
		 * <p> The width that was specified to the <code>TextBlock.createTextLine()</code> method when it created the line. This value is useful when deciding if a change requires rebreaking the line. </p>
		 * 
		 * @return 
		 */
		public function get specifiedWidth():Number {
			return _specifiedWidth;
		}

		/**
		 * <p> The TextBlock containing this text line, or null if the validity of the line is <code>TextLineValidity.STATIC</code>, meaning that the connection between the line and the TextBlock has been severed. </p>
		 * 
		 * @return 
		 */
		public function get textBlock():TextBlock {
			return _textBlock;
		}

		/**
		 * <p> The index of the first character of the line in the raw text of the text block. </p>
		 * 
		 * @return 
		 */
		public function get textBlockBeginIndex():int {
			return _textBlockBeginIndex;
		}

		/**
		 * <p> The logical height of the text line, which is equal to <code>ascent</code> + <code>descent</code>. To get the inked height, access the inherited <code>height</code> property. </p>
		 * <p>The value is calculated based on the difference between the baselines that bound the line, either ideo top/bottom or ascent/descent depending on whether TextBlock.baselineZero is ideo or not. Graphic elements are not considered when computing these baselines.</p>
		 * 
		 * @return 
		 */
		public function get textHeight():Number {
			return _textHeight;
		}

		/**
		 * <p> The logical width of the text line, which is the width that the text engine uses to lay out the line. Access the inherited <code>width</code> property to get the actual width of the bounding box of all the drawn pixels. </p>
		 * 
		 * @return 
		 */
		public function get textWidth():Number {
			return _textWidth;
		}

		/**
		 * <p> Specifies the number of pixels from the baseline to the top of the tallest character or graphic in the line. </p>
		 * 
		 * @return 
		 */
		public function get totalAscent():Number {
			return _totalAscent;
		}

		/**
		 * <p> Specifies the number of pixels from the baseline to the bottom of the lowest-descending character or graphic in the line. </p>
		 * 
		 * @return 
		 */
		public function get totalDescent():Number {
			return _totalDescent;
		}

		/**
		 * <p> The total logical height of the text line, which is equal to <code>totalAscent</code> + <code>totalDescent</code>. </p>
		 * 
		 * @return 
		 */
		public function get totalHeight():Number {
			return _totalHeight;
		}

		/**
		 * <p> The width of the line if it was not justified. For unjustified text, this value is the same as <code>textWidth</code>. For justified text, this value is what the length would have been without justification, and <code>textWidth</code> represents the actual line width. For example, when the following String is justified and submitted to <code>TextBlock.createTextLine()</code> with a width of 500, it has an actual width of 500 but an unjustified width of 268.9921875. </p>
		 * 
		 * @return 
		 */
		public function get unjustifiedTextWidth():Number {
			return _unjustifiedTextWidth;
		}

		/**
		 * <p> Provides a way for the application to associate arbitrary data with the text line. </p>
		 * 
		 * @return 
		 */
		public function get userData():* {
			return _userData;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set userData(value:*):void {
			_userData = value;
		}

		/**
		 * <p> Specifies the current validity of the text line. Values for this property are found in the members of the <code>TextLineValidity</code> class. The rules for setting this property are as follows: </p>
		 * <p>A line is considered USER_INVALID if validity is set to any string which is not a member of <code>TextLineValidity</code>. USER_INVALID is an abstraction used here to represent any such value.</p>
		 * <p>When the contents of the TextBlock are modified, the Flash runtime marks affected text lines, the previous line, and all following lines as INVALID. The previous line must be marked invalid when a change allows the previous line to absorb part of the content that was originally on the first affected line.</p>
		 * <p>Newly broken lines are always VALID. The Flash runtime may change lines that follow from VALID to POSSIBLY_INVALID or INVALID. It may change POSSIBLY_INVALID lines to VALID if the line breaks match up, or to INVALID if they don't.</p>
		 * <p>Application code can mark VALID lines as INVALID or USER_INVALID, and can mark USER_INVALID lines as VALID. Application code cannot mark lines POSSIBLY_INVALID.</p>
		 * <p>Application code can mark any line STATIC. Doing so causes the <code>block</code> member to become <code>null</code>. Any graphic elements in a STATIC text line are removed and reparented if they are part of a new text line broken from the text block from which the STATIC text line originally derived.</p>
		 * 
		 * @return 
		 */
		public function get validity():String {
			return _validity;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set validity(value:String):void {
			_validity = value;
		}

		/**
		 * <p> Dumps the underlying contents of the TextLine as an XML string. This can be useful in automated testing, and includes text, formatting, and layout information. </p>
		 * <p>The following describes the output:</p>
		 * <pre>
		 * 	 [LINE]
		 * 	 &lt;line ascent=[Number] descent=[Number] rotation=[int]&gt;
		 * 	 	&lt;elements&gt;
		 * 	 		[0-N ELEMENT]
		 * 	 	&lt;/elements&gt;
		 * 	 	&lt;clusters&gt;
		 * 	 		[0-N CLUSTER]
		 * 	 	&lt;/clusters&gt;
		 * 	 &lt;/line&gt;
		 * 	 
		 * 	 [ELEMENT]
		 * 	 &lt;glyph isEmbedded=[Boolean] fontName=[String] isBold=[Boolean] isItalic=[Boolean] gid=[int] pointSize=[Number] x=[Number] y=[Number] rotation=[int]/&gt;
		 * 	 or
		 * 	 &lt;graphic child=[int] x=[Number] y=[Number] rotation=[int]/&gt;
		 * 	 or
		 * 	 &lt;embeddedRun x=[Number] y=[Number]&gt;
		 * 	 	[LINE]
		 * 	 &lt;/embeddedRun&gt;
		 * 	 
		 * 	 [CLUSTER]
		 * 	 &lt;cluster xLeft=[Number] xCenter=[Number] xRight=[Number] cursorOnLeft=[Boolean] cursorOnRight=[Boolean] wordBoundaryOnLeft=[Boolean] wordBoundaryOnRight=[Boolean]/&gt;
		 * 	 </pre>
		 * <p><b>Note:</b> The content and format of the output from this method could change in the future. Adobe does not guarantee backward compatibility for this method.</p>
		 * 
		 * @return 
		 */
		public function dump():String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> This method is deprecated and has no effect. Atom data is compressed and is not a factor in managing memory efficiency. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="TextBlock.html#dump()" target="">TextBlock.dump()</a>
		 * </div>
		 */
		public function flushAtomData():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Gets the bidirectional level of the atom at the specified index. Determined by a combination of <code>TextBlock.bidiLevel</code> and the Unicode bidirectional properties of the characters that form the line. </p>
		 * <p>For example, if you start a text block with some Hebrew text, you set <code>TextBlock.bidiLevel</code> to 1, establishing a default of right to left. If within the text you have a quote in English (left to right), that text has an <code>AtomBidiLevel</code> of 2. If within the English you have a bit of Arabic (right to left), <code>AtomBidiLevel</code> for that run goes to 3. If within the Arabic a number (left to right) occurs, the <code>AtomBidiLevel</code> setting for the number is 4. It does not matter in which line the atoms end up; the Hebrew atoms are <code>AtomBidiLevel</code> 1, the English atoms are <code>AtomBidiLevel</code> 2, Arabic atoms are <code>AtomBidiLevel</code> 3, and the number atoms are <code>AtomBidiLevel</code> 4.</p>
		 * 
		 * @param atomIndex  — The zero-based index value of the atom (for example, the first atom is 0, the second atom is 1, and so on). 
		 * @return  — The bidirectional level of the atom at . 
		 */
		public function getAtomBidiLevel(atomIndex:int):int {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Gets the bounds of the atom at the specified index relative to the text line. The bounds of the specified atom consist of its horizontal position (<code>x</code>) in the line, its vertical position in the line (<code>y</code>), its width (<code>w</code>), and its height (<code>h</code>). All values are in pixels. </p>
		 * 
		 * @param atomIndex  — The zero-based index value of the atom (for example, the first atom is 0, the second atom is 1, and so on). 
		 * @return  — The bounds of the atom at . 
		 */
		public function getAtomBounds(atomIndex:int):Rectangle {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Gets the center of the atom as measured along the baseline at the specified index. </p>
		 * 
		 * @param atomIndex  — The zero-based index value of the atom (for example, the first atom is 0, the second atom is 1, and so on). 
		 * @return  — The center of the atom at . 
		 */
		public function getAtomCenter(atomIndex:int):Number {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Gets the graphic of the atom at the specified index, or <code>null</code> if the atom is a character. </p>
		 * 
		 * @param atomIndex  — The zero-based index value of the atom (for example, the first atom is 0, the second atom is 1, and so on). 
		 * @return  — The graphic of the atom at . 
		 */
		public function getAtomGraphic(atomIndex:int):DisplayObject {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the index of the atom containing the character specified by the <code>charIndex</code> parameter, or -1 if the character does not contribute to any atom in the line. The <code>charIndex</code> is relative to the entire contents of the text block containing the line. </p>
		 * 
		 * @param charIndex  — The zero-based index value of the character (for example, the first character is 0, the second character is 1, and so on). 
		 * @return  — The index of the atom containing the character at . Returns -1 if the character does not contribute to any atom in the line. 
		 */
		public function getAtomIndexAtCharIndex(charIndex:int):int {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the index of the atom at the point specified by the <code>x</code> and <code>y</code> parameters, or -1 if no atom exists at that point. </p>
		 * <p>This method takes global coordinates so that you can easily use it with <code>MouseEvent.stageX</code> and <code>MouseEvent.stageY</code> properties.</p>
		 * 
		 * @param stageX  — The global <i>x</i> coordinate of the point to test. 
		 * @param stageY  — The global <i>y</i> coordinate of the point to test. 
		 * @return  — The index of the atom under the point. Returns -1 if the point is not over any atom. 
		 */
		public function getAtomIndexAtPoint(stageX:Number, stageY:Number):int {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Gets the text block begin index of the atom at the specified index. </p>
		 * 
		 * @param atomIndex  — The zero-based index value of the atom (for example, the first atom is 0, the second atom is 1, and so on). 
		 * @return  — The text block begin index of the atom at . 
		 */
		public function getAtomTextBlockBeginIndex(atomIndex:int):int {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Gets the text block end index of the atom at the specified index. </p>
		 * 
		 * @param atomIndex  — The zero-based index value of the atom (for example, the first atom is 0, the second atom is 1, and so on). 
		 * @return  — The text block end index of the atom at . 
		 */
		public function getAtomTextBlockEndIndex(atomIndex:int):int {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Gets the rotation of the atom at the specified index. TextRotation constants are used for this property. The rotation of the atom is the cumulative rotations of the element and the line. Its primary use is for setting the orientation of the caret (cursor) when interacting with a TextLine. </p>
		 * 
		 * @param atomIndex  — The zero-based index value of the atom (for example, the first atom is 0, the second atom is 1, and so on). 
		 * @return  — The rotation of the atom at . 
		 */
		public function getAtomTextRotation(atomIndex:int):String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Indicates whether a word boundary occurs to the left of the atom at the specified index. Word boundaries are determined based on the Unicode properties of the characters which contributed to the line. </p>
		 * 
		 * @param atomIndex  — The zero-based index value of the atom (for example, the first atom is 0, the second atom is 1, and so on). 
		 * @return  — A Boolean value that indicates whether a word boundary occurs to the left of the atom at . 
		 */
		public function getAtomWordBoundaryOnLeft(atomIndex:int):Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Gets the position of the specified baseline, relative to <code>TextBlock.baselineZero</code>. </p>
		 * 
		 * @param baseline  — The baseline for which to retrieve the position. Use <code>TextBaseline</code> values. 
		 * @return  — The position of the specified baseline relative to . 
		 */
		public function getBaselinePosition(baseline:String):Number {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the first <code>TextLineMirrorRegion</code> on the line whose <code>mirror</code> property matches that specified by the <code>mirror</code> parameter, or <code>null</code> if no match exists. </p>
		 * <p>Even a single <code>TextElement</code> can produce multiple <code>TextLineMirrorRegion</code> objects on one or more lines, depending on bidirectional level and line breaking. The <code>nextRegion</code> and <code>previousRegion</code> properties link all the mirror regions generated from one text element.</p>
		 * 
		 * @param mirror  — The <code>EventDispatcher</code> mirror object to search for. 
		 * @return  — The first  on the line whose  property matches the specified value, or  if no match exists. 
		 */
		public function getMirrorRegion(mirror:EventDispatcher):TextLineMirrorRegion {
			throw new Error("Not implemented");
		}
	}
}
