package flash.text.engine {
	/**
	 *  The LigatureLevel class is an enumeration of constant values used in setting the <code>ligatureLevel</code> property of the ElementFormat class. A ligature occurs where two or more letter-forms are joined as a single glyph. Ligatures usually replace consecutive characters sharing common components, such as the letter pairs 'fi', 'fl', or 'ae'. They are used with both Latin and Non-Latin character sets. <p> <b>Note:</b> When working with Arabic or Syriac fonts, <code>ligatureLevel</code> must be set to MINIMUM or above.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="ElementFormat.html#ligatureLevel" target="">ElementFormat.ligatureLevel</a>
	 * </div><br><hr>
	 */
	public class LigatureLevel {
		/**
		 * <p> Used to specify common ligatures. </p>
		 */
		public static const COMMON:String = "common";
		/**
		 * <p> Used to specify exotic ligatures. </p>
		 */
		public static const EXOTIC:String = "exotic";
		/**
		 * <p> Used to specify minimum ligatures. </p>
		 */
		public static const MINIMUM:String = "minimum";
		/**
		 * <p> Used to specify no ligatures. </p>
		 */
		public static const NONE:String = "none";
		/**
		 * <p> Used to specify uncommon ligatures. </p>
		 */
		public static const UNCOMMON:String = "uncommon";
	}
}
