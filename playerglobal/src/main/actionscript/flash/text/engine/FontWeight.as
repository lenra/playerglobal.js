package flash.text.engine {
	/**
	 *  The FontWeight class is an enumeration of constant values used with <code>FontDescription.fontWeight</code>. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="FontDescription.html#fontWeight" target="">flash.text.engine.FontDescription.fontWeight</a>
	 * </div><br><hr>
	 */
	public class FontWeight {
		/**
		 * <p> Used to indicate bold font weight. </p>
		 */
		public static const BOLD:String = "bold";
		/**
		 * <p> Used to indicate normal font weight. </p>
		 */
		public static const NORMAL:String = "normal";
	}
}
