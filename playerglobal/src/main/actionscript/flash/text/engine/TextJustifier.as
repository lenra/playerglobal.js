package flash.text.engine {
	/**
	 *  The TextJustifier class is an abstract base class for the justifier types that you can apply to a TextBlock, specifically the EastAsianJustifier and SpaceJustifier classes. <p>You cannot instantiate the TextJustifier class directly. Invoking <code>new TextJustifier()</code> throws an <code>ArgumentError</code> exception. Setting the properties of an EastAsianJustifier or SpaceJustifier object after you apply it to a TextBlock does not invalidate the TextBlock.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="EastAsianJustifier.html" target="">EastAsianJustifier</a>
	 *  <br>
	 *  <a href="SpaceJustifier.html" target="">SpaceJustifier</a>
	 *  <br>
	 *  <a href="TextBlock.html#textJustifier" target="">TextBlock.textJustifier</a>
	 * </div><br><hr>
	 */
	public class TextJustifier {
		private var _lineJustification:String;

		private var _locale:String;

		/**
		 * <p> Calling the <code>new TextJustifier()</code> constructor throws an <code>ArgumentError</code> exception. You <i>can</i>, however, call constructors for the following subclasses of TextJustifier: </p>
		 * <ul>
		 *  <li><code>new SpaceJustifier()</code></li>
		 *  <li><code>new EastAsianJustifier()</code></li>
		 * </ul>
		 * 
		 * @param locale  — The locale to determine the justification rules. 
		 * @param lineJustification  — The type of line justification for the paragraph. Use <code>LineJustification</code> constants for this property. 
		 */
		public function TextJustifier(locale:String, lineJustification:String) {
			this._locale = locale;
			this._lineJustification = lineJustification;
		}

		/**
		 * <p> Specifies the line justification for the text in a text block. </p>
		 * <p>Use the following constants defined by the <code>LineJustification</code> as valid values for this property:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>String value</th>
		 *    <th>Description</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>LineJustification.UNJUSTIFIED</code></td>
		 *    <td>Generates unjustified lines.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>LineJustification.ALL_BUT_LAST</code></td>
		 *    <td>Generates all lines justified except for the last one.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>LineJustification.ALL_INCLUDING_LAST</code></td>
		 *    <td>Generates all lines justified.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>LineJustification.ALL_BUT_MANDATORY_BREAK</code></td>
		 *    <td>Generates all lines justified except for the last line and lines ending in mandatory breaks.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * 
		 * @return 
		 */
		public function get lineJustification():String {
			return _lineJustification;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set lineJustification(value:String):void {
			_lineJustification = value;
		}

		/**
		 * <p> Specifies the locale to determine the justification rules for the text in a text block. Standard locale identifiers are used. For example "en", "en_US" and "en-US" are all English, "ja" is Japanese. </p>
		 * 
		 * @return 
		 */
		public function get locale():String {
			return _locale;
		}

		/**
		 * <p> Constructs a cloned copy of the TextJustifier. </p>
		 * <p>Subclasses of TextJustifier must override this method.</p>
		 * 
		 * @return  — A copy of the  object. 
		 */
		public function clone():TextJustifier {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Constructs a default TextJustifier subclass appropriate to the specified locale. </p>
		 * <p>If the locale is Chinese, Korean, or Japanese, the method constructs a default EastAsianJustifier object. Otherwise the text engine constructs a default SpaceJustifier object.</p>
		 * 
		 * @param locale  — The locale to determine the justifier constructed. 
		 * @return  — A reference to a  object. 
		 */
		public static function getJustifierForLocale(locale:String):TextJustifier {
			throw new Error("Not implemented");
		}
	}
}
