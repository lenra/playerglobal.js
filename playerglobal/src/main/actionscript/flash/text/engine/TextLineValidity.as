package flash.text.engine {
	/**
	 *  The TextLineValidity class is an enumeration of constant values for setting the <code>validity</code> property of the TextLine class. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="TextBlock.html#firstInvalidLine" target="">TextBlock.firstInvalidLine</a>
	 *  <br>
	 *  <a href="TextLine.html#validity" target="">TextLine.validity</a>
	 * </div><br><hr>
	 */
	public class TextLineValidity {
		/**
		 * <p> Specifies that the line is invalid. </p>
		 */
		public static const INVALID:String = "invalid";
		/**
		 * <p> Specifies that the text line is possibly invalid. The Flash runtime uses this validity during rebreaking of a previously broken text block whose content has not changed. You cannot set this value. </p>
		 */
		public static const POSSIBLY_INVALID:String = "possiblyInvalid";
		/**
		 * <p> Specifies that the line is static, and that the connection between the line and the text block has been severed. </p>
		 */
		public static const STATIC:String = "static";
		/**
		 * <p> Specifies that the text line is valid. </p>
		 */
		public static const VALID:String = "valid";
	}
}
