package flash.text.engine {
	/**
	 *  The TextLineCreationResult class is an enumeration of constant values used with <code>TextBlock.textLineCreationResult</code>. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="TextBlock.html#createTextLine()" target="">TextBlock.createTextLine()</a>
	 *  <br>
	 *  <a href="TextBlock.html#textLineCreationResult" target="">TextBlock.textLineCreationResult</a>
	 * </div><br><hr>
	 */
	public class TextLineCreationResult {
		/**
		 * <p> Indicates no line was created because all text in the block had already been broken. </p>
		 */
		public static const COMPLETE:String = "complete";
		/**
		 * <p> Indicates the line was created with an emergency break because no break opportunity was available in the specified width. </p>
		 */
		public static const EMERGENCY:String = "emergency";
		/**
		 * <p> Indicates no line was created because no text could fit in the specified width and <code>fitSomething</code> was not specified in the call to <code>createTextLine()</code>. </p>
		 */
		public static const INSUFFICIENT_WIDTH:String = "insufficientWidth";
		/**
		 * <p> Indicates the line was successfully broken. </p>
		 */
		public static const SUCCESS:String = "success";
	}
}
