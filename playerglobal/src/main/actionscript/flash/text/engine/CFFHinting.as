package flash.text.engine {
	/**
	 *  The CFFHinting class defines values for cff hinting in the FontDescription class. <p>Hinting adjusts the display of an outline font so it lines up with the pixel grid. At small screen sizes, hinting produces a clear, legible text for human readers. </p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="FontDescription.html" target="">flash.text.engine.FontDescription</a>
	 * </div><br><hr>
	 */
	public class CFFHinting {
		/**
		 * <p> Fits strong horizontal stems to the pixel grid for improved readability. This constant is used in setting the <code>cffHinting</code> property of the FontDescription class. Use the syntax <code>CFFHinting.HORIZONTAL_STEM</code>. </p>
		 * <p><b>Note:</b> Not recommended for animation.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="FontDescription.html#cffHinting" target="">flash.text.engine.FontDescription.cffHinting</a>
		 * </div>
		 */
		public static const HORIZONTAL_STEM:String = "horizontalStem";
		/**
		 * <p> No hinting is applied. Horizontal stems in the glyphs are not forced to the pixel grid. This constant is used in setting the <code>cffHinting</code> property of the FontDescription class. Recommended setting for animation or for large font sizes. Use the syntax <code>CFFHinting.NONE</code>. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="FontDescription.html#cffHinting" target="">flash.text.engine.FontDescription.cffHinting</a>
		 * </div>
		 */
		public static const NONE:String = "none";
	}
}
