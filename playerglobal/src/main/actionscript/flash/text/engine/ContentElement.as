package flash.text.engine {
	import flash.events.EventDispatcher;

	/**
	 *  The ContentElement class serves as a base class for the element types that can appear in a GroupElement, namely a GraphicElement, another GroupElement, or a TextElement. <p>ContentElement is an abstract base class; therefore, you cannot instantiate ContentElement directly. Invoking <code>new ContentElement()</code> throws an <code>ArgumentError</code> exception. </p> <p>You can assign a ContentElement element to exactly one <code>GroupElement</code> or to the <code>content</code> property of exactly one text block.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="ElementFormat.html" target="">ElementFormat</a>
	 *  <br>
	 *  <a href="GraphicElement.html" target="">GraphicElement</a>
	 *  <br>
	 *  <a href="GroupElement.html" target="">GroupElement</a>
	 *  <br>
	 *  <a href="TextBlock.html#content" target="">TextBlock.content</a>
	 *  <br>
	 *  <a href="TextElement.html" target="">TextElement</a>
	 *  <br>
	 *  <a href="TextLineMirrorRegion.html" target="">TextLineMirrorRegion</a>
	 *  <br>
	 *  <a href="TextRotation.html" target="">TextRotation</a>
	 * </div><br><hr>
	 */
	public class ContentElement {
		/**
		 * <p> Indicates the presence of a graphic element in the text. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="ContentElement.html#rawText" target="">rawText</a>
		 * </div>
		 */
		public static const GRAPHIC_ELEMENT:uint = 0xFDEF;

		private var _elementFormat:ElementFormat;
		private var _eventMirror:EventDispatcher;
		private var _textRotation:String;
		private var _userData:*;

		private var _groupElement:GroupElement;
		private var _rawText:String;
		protected var _text:String;
		private var _textBlock:TextBlock;
		private var _textBlockBeginIndex:int;

		/**
		 * <p> Calling the <code>new ContentElement()</code> constructor throws an <code>ArgumentError</code> exception. You <i>can</i>, however, call constructors for the following subclasses of ContentElement: </p>
		 * <ul>
		 *  <li><code>new GraphicElement()</code></li>
		 *  <li><code>new GroupElement()</code></li>
		 *  <li><code>new TextElement()</code></li>
		 * </ul>
		 * 
		 * @param elementFormat  — The element format for the text in the element. The default value is <code>null</code>. 
		 * @param eventMirror  — The <code>EventDispatcher</code> object that receives copies of every event dispatched to valid text lines created based on this content element. The default value is <code>null</code>. 
		 * @param textRotation  — The rotation applied the element as a unit. Use <code>TextRotation</code> constants for this property. The default value is <code>TextRotation.ROTATE_0</code>. 
		 */
		public function ContentElement(elementFormat:ElementFormat = null, eventMirror:EventDispatcher = null, textRotation:String = "rotate0") {
			this.elementFormat = elementFormat;
			this.eventMirror = eventMirror;
			this.textRotation = textRotation;
		}

		/**
		 * <p> The ElementFormat object used for the element. </p>
		 * <p>The default value is <code>null</code>.</p>
		 * <p>When the <code>elementFormat</code> property is set, the ElementFormat object provided is locked: its <code>locked</code> property is set to <code>true</code>. A locked ElementFormat cannot be modified.</p>
		 * 
		 * @return 
		 */
		public function get elementFormat():ElementFormat {
			return _elementFormat;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set elementFormat(value:ElementFormat):void {
			_elementFormat = value;
		}

		/**
		 * <p> The <code>EventDispatcher</code> object that receives copies of every event dispatched to valid text lines based on this content element. The specified object can be used to set up listeners for a text link or other interactive piece of text, as it can be difficult to determine at runtime which parts of lines have resulted from particular content elements. You can also use listeners to apply decorations such as underlines, the metrics of which you cannot determine until after the text is laid out. The default value is <code>null</code>, which means no mirrored events are dispatched. </p>
		 * <p>Event mirrors manifest themselves in text lines as instances of the <code>TextLineMirrorRegion</code> class. Depending on bidirectional processing and line breaking, one or more mirror regions can be produced.</p>
		 * <p>The default value is <code>null</code>.</p>
		 * 
		 * @return 
		 */
		public function get eventMirror():EventDispatcher {
			return _eventMirror;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set eventMirror(value:EventDispatcher):void {
			_eventMirror = value;
		}

		/**
		 * <p> The GroupElement object that contains this element, or <code>null</code> if it is not in a group. </p>
		 * <p>The default value is <code>null</code>.</p>
		 * 
		 * @return 
		 */
		public function get groupElement():GroupElement {
			return _groupElement;
		}

		/**
		 * <p> A copy of the text in the element, including any U+FDEF characters. The Unicode character, U+FDEF, marks the location of a graphic element in the String. </p>
		 * 
		 * @return 
		 */
		public function get rawText():String {
			return _rawText;
		}

		/**
		 * <p> A copy of the text in the element, not including any U+FDEF characters, which represent graphic elements in the String. </p>
		 * 
		 * @return 
		 */
		public function get text():String {
			return _text;
		}

		/**
		 * <p> The TextBlock to which this element belongs. </p>
		 * <p>The default value is <code>null</code>.</p>
		 * 
		 * @return 
		 */
		public function get textBlock():TextBlock {
			return _textBlock;
		}
		
		internal function setTextBlock(value:TextBlock):void {
			_textBlock = value;
		}

		/**
		 * <p> The index in the text block of the first character of this element. This value is not cached; it is calculated whenever this method is called. </p>
		 * <p>The default value is -1.</p>
		 * 
		 * @return 
		 */
		public function get textBlockBeginIndex():int {
			if (this.textBlock==null)
				return -1;
			//TODO: manage content elements in groups
			return 0;
		}

		/**
		 * <p> The rotation to apply to the element as a unit. Use <code>TextRotation</code> constants for this property. </p>
		 * <p>The default value is <code>TextRotation.ROTATE_0</code>.</p>
		 * <p>The final rotation of any glyph is the sum of <code>ElementFormat.textRotation</code>, <code>ContentElement.textRotation</code>, and <code>TextBlock.lineRotation</code>.</p>
		 * <p><code>ContentElement.textRotation</code> is used to create a short run of text whose rotation differs from the containing line. TCY runs in Japanese text are an example. TCY stands for Tate-Chu-Yoko and refers to a little horizontal run of text (usually a number) in some vertical Japanese text. To create a Paragraph of vertical Japanese text containing a TCY run, do the following:</p>
		 * <ol>
		 *  <li>Set <code>TextBlock.lineRotation=TextRotation.ROTATE_90</code></li>
		 *  <li>Set <code>TextBlock.content</code> to a GroupElement, consisting of three TextElement objects. The first of these elements is the Japanese text before the TCY run, the second is the Latin text of the TCY run, and the third is the Japanese text after the TCY run.</li>
		 *  <li>Set the <code>textRotation</code> property of the TCY TextElement to <code>TextRotation.ROTATE_270</code>. The TCY text element rotates as a unit. It starts with a 90 degree rotation inherited from the line. Adding another 270 degrees takes it around to horizontal.</li>
		 * </ol>
		 * <p>Rotated content elements cannot be nested. In any hierarchy of content elements, no matter how complex, only one content element can have its <code>textRotation</code> property set. The following methods and property setters throw an argument error if nested rotations are detected:</p>
		 * <ol>
		 *  <li><code>ContentElement.textRotation</code></li>
		 *  <li><code>GroupElement.setElements</code></li>
		 *  <li><code>GroupElement.replaceElements</code></li>
		 * </ol>
		 * <p>To set values for this property, use the following string values:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>String value</th>
		 *    <th>Description</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextRotation.ROTATE_0</code></td>
		 *    <td>Element is not rotated.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextRotation.ROTATE_90</code></td>
		 *    <td>Element is rotated 90 degrees clockwise.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextRotation.ROTATE_180</code></td>
		 *    <td>Element is rotated 180 degrees.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextRotation.ROTATE_270</code></td>
		 *    <td>Element is rotated 270 degrees clockwise.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextRotation.AUTO</code></td>
		 *    <td>Not supported.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * 
		 * @return 
		 */
		public function get textRotation():String {
			return _textRotation;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set textRotation(value:String):void {
			_textRotation = value;
		}

		/**
		 * <p> Provides a way for an application to associate arbitrary data with the element. </p>
		 * <p>The default value is <code>null</code>.</p>
		 * 
		 * @return 
		 */
		public function get userData():* {
			return _userData;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set userData(value:*):void {
			_userData = value;
		}
	}
}
