package flash.text.engine {
	/**
	 *  The JustificationStyle class is an enumeration of constant values for setting the <code>justificationStyle</code> property of the EastAsianJustifier class. These constants specify options for handling kinsoku characters, which are Japanese characters that cannot appear at either the beginning or end of a line. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="EastAsianJustifier.html#justificationStyle" target="">EastAsianJustifier.justificationStyle</a>
	 * </div><br><hr>
	 */
	public class JustificationStyle {
		/**
		 * <p> Bases justification on either expanding or compressing the line, whichever gives a result closest to the desired width. </p>
		 */
		public static const PRIORITIZE_LEAST_ADJUSTMENT:String = "prioritizeLeastAdjustment";
		/**
		 * <p> Bases justification on compressing kinsoku at the end of the line, or expanding it if no kinsoku occurs or if that space is insufficient. </p>
		 */
		public static const PUSH_IN_KINSOKU:String = "pushInKinsoku";
		/**
		 * <p> Bases justification on expanding the line. </p>
		 */
		public static const PUSH_OUT_ONLY:String = "pushOutOnly";
	}
}
