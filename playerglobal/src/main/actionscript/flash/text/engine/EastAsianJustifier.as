package flash.text.engine {
	/**
	 *  The EastAsianJustifier class has properties to control the justification options for text lines whose content is primarily East Asian text. <p> Use the constructor <code>new EastAsianJustifier()</code> to create an EastAsianJustifier object before setting its properties. Setting the properties of an EastAsianJustifier object after it has been applied to a TextBlock does not invalidate the TextBlock.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS9dd7ed846a005b294b857bfa122bd808ea6-7fec.html" target="_blank">Justifying East Asian text</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="JustificationStyle.html" target="">JustificationStyle</a>
	 *  <br>
	 *  <a href="LineJustification.html" target="">LineJustification</a>
	 *  <br>
	 *  <a href="TextBlock.html#textJustifier" target="">TextBlock.textJustifier</a>
	 * </div><br><hr>
	 */
	public class EastAsianJustifier extends TextJustifier {
		private var _composeTrailingIdeographicSpaces:Boolean;
		private var _justificationStyle:String;

		/**
		 * <p> Creates an EastAsianJustifier object. </p>
		 * 
		 * @param locale  — The locale to determine the justification rules. The default value is <code>"ja"</code>. 
		 * @param lineJustification  — The type of line justification for the paragraph. Use <code>LineJustification</code> constants for this property. The default value is <code>LineJustification.ALL_BUT_LAST</code>. 
		 * @param justificationStyle  — The justification style for the text in a text block using an East Asian justifier. Use <code>JustificationStyle</code> constants for this property. The default value is <code>JustificationStyle.PUSH_IN_KINSOKU</code>. 
		 */
		public function EastAsianJustifier(locale:String = "ja", lineJustification:String = "allButLast", justificationStyle:String = "pushInKinsoku") {
			super(locale, lineJustification);
			throw new Error("Not implemented");
		}

		/**
		 * <p> Specifies whether ideographic spaces at the ends of lines should be composed or dropped during justification. </p>
		 * <p>The default value is <code>false</code></p>
		 * 
		 * @return 
		 */
		public function get composeTrailingIdeographicSpaces():Boolean {
			return _composeTrailingIdeographicSpaces;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set composeTrailingIdeographicSpaces(value:Boolean):void {
			_composeTrailingIdeographicSpaces = value;
		}

		/**
		 * <p> Specifies the justification style for the text in a text block. </p>
		 * <p>The default value is <code>JustificationStyle.PUSH_IN_KINSOKU</code>.</p>
		 * <p>Use one of the constants in the JustificationStyle class to set the value for this property. The following table lists the possible values:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>String value</th>
		 *    <th>Description</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>JustificationStyle.PUSH_IN_KINSOKU</code></td>
		 *    <td>Specifies push in justification.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>JustificationStyle.PUSH_OUT_ONLY</code></td>
		 *    <td>Specifies push out justification.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>JustificationStyle.PRIORITIZE_LEAST_ADJUSTMENT</code></td>
		 *    <td>Specifies justification wherein the least adjustment is prioritized.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * 
		 * @return 
		 */
		public function get justificationStyle():String {
			return _justificationStyle;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set justificationStyle(value:String):void {
			_justificationStyle = value;
		}

		/**
		 * <p> Constructs a cloned copy of the EastAsianJustifier. </p>
		 * 
		 * @return  — A copy of the  object. 
		 */
		override public function clone():TextJustifier {
			throw new Error("Not implemented");
		}
	}
}
