package flash.text.engine {
	/**
	 *  The DigitWidth class is an enumeration of constant values used in setting the <code>digitWidth</code> property of the ElementFormat class. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="ElementFormat.html#digitWidth" target="">flash.text.engine.ElementFormat.digitWidth</a>
	 * </div><br><hr>
	 */
	public class DigitWidth {
		/**
		 * <p> Used to specify default digit width. The results are font-dependent; characters use the settings specified by the font designer without any features applied. </p>
		 */
		public static const DEFAULT:String = "default";
		/**
		 * <p> Used to specify proportional digit width. </p>
		 */
		public static const PROPORTIONAL:String = "proportional";
		/**
		 * <p> Used to specify tabular digit width. </p>
		 */
		public static const TABULAR:String = "tabular";
	}
}
