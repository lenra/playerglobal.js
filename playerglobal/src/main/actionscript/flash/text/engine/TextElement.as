package flash.text.engine {
	import flash.events.EventDispatcher;

	/**
	 *  The TextElement class represents a string of formatted text. Assign a TextElement object to the <code>content</code> property of a TextBlock object to create a block of text. Assign it to a GroupElement object to combine it with other text and graphic elements as a unit. Use the ElementFormat class to format the text. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="ContentElement.html" target="">ContentElement</a>
	 *  <br>
	 *  <a href="ElementFormat.html" target="">ElementFormat</a>
	 *  <br>
	 *  <a href="TextBlock.html" target="">TextBlock</a>
	 * </div><br><hr>
	 */
	public class TextElement extends ContentElement {

		/**
		 * <p> Creates a new TextElement instance. </p>
		 * 
		 * @param text  — The text for the element. The default value is <code>null</code>. 
		 * @param elementFormat  — The element format for the text in the element. The default value is <code>null</code>. 
		 * @param eventMirror  — The <code>EventDispatcher</code> object that receives copies of every event dispatched to text lines based on this content element. The default value is <code>null</code>. 
		 * @param textRotation  — The rotation applied the element as a unit. Use <code>TextRotation</code> constants for this property. The default value is <code>TextRotation.ROTATE_0</code>. 
		 */
		public function TextElement(text:String = null, elementFormat:ElementFormat = null, eventMirror:EventDispatcher = null, textRotation:String = "rotate0") {
			super(elementFormat, eventMirror, textRotation);
			this._text = text;
		}
		
		override public function get text():String {
			return super.text;
		}

		/**
		 * <p> Receives the text that is the content of the element. </p>
		 * <p>The default value is <code>null</code>.</p>
		 * 
		 * @param value
		 * @return 
		 */
		public function set text(value:String):void {
			this._text = value;
		}

		/**
		 * <p> Replaces the range of characters that the <code>beginIndex</code> and <code>endIndex</code> parameters specify with the contents of the <code>newText</code> parameter. The <code>beginIndex</code> and <code>endIndex</code> values refer to the current contents of <code>text</code>. </p>
		 * <p>To delete text, pass <code>null</code> for <code>newText</code>.</p>
		 * <p>To insert text, pass the same value for <code>beginIndex</code> and <code>endIndex</code>. The new text is inserted before the specified index.</p>
		 * <p>To append text, pass <code>text.length</code> for <code>beginIndex</code> and <code>endIndex</code>.</p>
		 * <p>To set all the text, pass 0 for <code>beginIndex</code> and <code>text.length</code> for <code>endIndex</code>.</p>
		 * 
		 * @param beginIndex  — The zero-based index value for the start position of the replacement range. 
		 * @param endIndex  — The zero-based index value following the end position of the replacement range. 
		 * @param newText  — The text to use to replace the specified range of characters. 
		 */
		public function replaceText(beginIndex:int, endIndex:int, newText:String):void {
			throw new Error("Not implemented");
		}
	}
}
