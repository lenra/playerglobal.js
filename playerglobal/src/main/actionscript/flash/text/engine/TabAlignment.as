package flash.text.engine {
	/**
	 *  The TabAlignment class is an enumeration of constant values that you can use to set the <code>tabAlignment</code> property of the <code>TabStop</code> class. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="TabStop.html#alignment" target="">TabStop.alignment</a>
	 *  <br>
	 *  <a href="TextBlock.html#tabStops" target="">TextBlock.tabStops</a>
	 * </div><br><hr>
	 */
	public class TabAlignment {
		/**
		 * <p> Positions the center of the tabbed text at the tab stop. </p>
		 */
		public static const CENTER:String = "center";
		/**
		 * <p> Positions the alignment token of the tabbed text at the tab stop. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="TabStop.html#decimalAlignmentToken" target="">TabStop.decimalAlignmentToken</a>
		 * </div>
		 */
		public static const DECIMAL:String = "decimal";
		/**
		 * <p> Positions the end of the tabbed text at the tab stop. </p>
		 */
		public static const END:String = "end";
		/**
		 * <p> Positions the start of the tabbed text at the tab stop. </p>
		 */
		public static const START:String = "start";
	}
}
