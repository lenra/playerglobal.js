package flash.text.engine {
	/**
	 *  The FontPosture class is an enumeration of constant values used with <code>FontDescription.fontPosture</code> to set text to italic or normal. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="FontDescription.html#fontPosture" target="">FontDescription.fontPosture</a>
	 * </div><br><hr>
	 */
	public class FontPosture {
		/**
		 * <p> Used to indicate italic font posture. </p>
		 */
		public static const ITALIC:String = "italic";
		/**
		 * <p> Used to indicate normal font posture. </p>
		 */
		public static const NORMAL:String = "normal";
	}
}
