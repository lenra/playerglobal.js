package flash.text.engine {
	/**
	 *  The TypographicCase class is an enumeration of constant values for setting the <code>typographicCase</code> property of the ElementFormat class. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="ElementFormat.html#typographicCase" target="">ElementFormat.typographicCase</a>
	 * </div><br><hr>
	 */
	public class TypographicCase {
		/**
		 * <p> Specifies that spacing is adjusted for uppercase characters on output. </p>
		 */
		public static const CAPS:String = "caps";
		/**
		 * <p> Specifies that all lowercase characters use small-caps glyphs on output. </p>
		 */
		public static const CAPS_AND_SMALL_CAPS:String = "capsAndSmallCaps";
		/**
		 * <p> Specifies default typographic case. The results are font-dependent; characters use the settings specified by the font designer without any features applied. </p>
		 */
		public static const DEFAULT:String = "default";
		/**
		 * <p> Specifies that all characters use lowercase glyphs on output. </p>
		 */
		public static const LOWERCASE:String = "lowercase";
		/**
		 * <p> Specifies that uppercase characters use small-caps glyphs on output. </p>
		 */
		public static const SMALL_CAPS:String = "smallCaps";
		/**
		 * <p> Specifies that uppercase characters use title glyphs on output. </p>
		 */
		public static const TITLE:String = "title";
		/**
		 * <p> Specifies that all characters use uppercase glyphs on output. </p>
		 */
		public static const UPPERCASE:String = "uppercase";
	}
}
