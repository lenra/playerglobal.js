package flash.text.engine {
	import flash.geom.Rectangle;

	/**
	 *  The FontMetrics class contains measurement and offset information about a font. The <code>ElementFormat.getFontMetrics()</code> method returns objects of this class. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="ElementFormat.html#getFontMetrics()" target="">ElementFormat.getFontMetrics()</a>
	 * </div><br><hr>
	 */
	public class FontMetrics {
		private var _emBox:Rectangle;
		private var _lineGap:Number;
		private var _strikethroughOffset:Number;
		private var _strikethroughThickness:Number;
		private var _subscriptOffset:Number;
		private var _subscriptScale:Number;
		private var _superscriptOffset:Number;
		private var _superscriptScale:Number;
		private var _underlineOffset:Number;
		private var _underlineThickness:Number;

		/**
		 * <p> Creates a FontMetrics object. The FontMetrics object contains information about the metrics of a font in an element format. The <code>flash.text.engine.ElementFormat.getFontMetrics()</code> method returns objects of this class. </p>
		 * 
		 * @param emBox  — The emBox of the font in pixels. 
		 * @param strikethroughOffset  — The offset for a strikethrough in pixels. 
		 * @param strikethroughThickness  — The thickness for a strikethrough in pixels. 
		 * @param underlineOffset  — The offset for an underline in pixels. 
		 * @param underlineThickness  — The thickness for an underline in pixels. 
		 * @param subscriptOffset  — The offset for a subscript in pixels. 
		 * @param subscriptScale  — The scale to apply to the point size of a subscript. 
		 * @param superscriptOffset  — The offset for a superscript in pixels. 
		 * @param superscriptScale  — The scale to apply to the point size of a superscript. 
		 * @param lineGap
		 */
		public function FontMetrics(emBox:Rectangle, strikethroughOffset:Number, strikethroughThickness:Number, underlineOffset:Number, underlineThickness:Number, subscriptOffset:Number, subscriptScale:Number, superscriptOffset:Number, superscriptScale:Number, lineGap:Number = 0.0) {
			throw new Error("Not implemented");
		}

		/**
		 * <p> The emBox value represents the design space of the font and is used to place Chinese, Korean, or Japanese glyphs relative to the Roman baseline. Typically a square, sized to the point size of the font. The origin (coordinate 0,0) of the emBox is set to the left edge and Roman baseline of the rect. For example, for a 10-point font, the emBox can be a rect [L,T,R,B] of [0, -8.8, 10, 1.2]. </p>
		 * 
		 * @return 
		 */
		public function get emBox():Rectangle {
			return _emBox;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set emBox(value:Rectangle):void {
			_emBox = value;
		}

		/**
		 * <p> The lineGap value is the suggested gap between lines. </p>
		 * <p>This value is set by the font designer, and can vary widely from font to font. In a multi-script font, different line gaps may be suitable for different scripts; no single value may suffice. Furthermore, different kinds of text usage for a particular script (for example, whether Japanese text is annotated with ruby or not) may require different line gaps.</p>
		 * 
		 * @return 
		 */
		public function get lineGap():Number {
			return _lineGap;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set lineGap(value:Number):void {
			_lineGap = value;
		}

		/**
		 * <p> The strikethroughOffset value is the suggested vertical offset from the Roman baseline for a strikethrough. </p>
		 * <p>Note that depending on the rotation of the line, this value is either added or subtracted from the position of the line to find the position for the strikethrough. In a line with <code>TextRotation.ROTATE_0</code>, <code>strikethrough.y = line.y + strikethroughOffset</code>. In a line with <code>TextRotation.ROTATE_90</code>, <code>strikethrough.x = line.x - strikethroughOffset</code>.</p>
		 * <p>When applying decorations such as strikethroughs to a <code>TextLine</code>, the recommended procedure is to specify an <code>eventMirror</code> on the <code>ContentElement</code> which is to receive the decoration. In response to the <code>Event.ADDED</code> event, the <code>bounds</code> of the <code>TextLineMirrorRegion</code> can be used in conjunction with the <code>strikethroughOffset</code> to place the strikethrough.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="ContentElement.html#eventMirror" target="">ContentElement.eventMirror</a>
		 *  <br>
		 *  <a href="TextLineMirrorRegion.html" target="">TextLineMirrorRegion</a>
		 * </div>
		 * 
		 * @return 
		 */
		public function get strikethroughOffset():Number {
			return _strikethroughOffset;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set strikethroughOffset(value:Number):void {
			_strikethroughOffset = value;
		}

		/**
		 * <p> The strikethroughThickness value is the suggested thickness for a strikethrough. </p>
		 * 
		 * @return 
		 */
		public function get strikethroughThickness():Number {
			return _strikethroughThickness;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set strikethroughThickness(value:Number):void {
			_strikethroughThickness = value;
		}

		/**
		 * <p> The subscriptOffset value is the suggested vertical offset from the Roman baseline for a subscript. </p>
		 * <p>The subscriptOffset value is used with <code>ElementFormat.baselineShift</code> to position the subscript.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="ElementFormat.html#baselineShift" target="">ElementFormat.baselineShift</a>
		 * </div>
		 * 
		 * @return 
		 */
		public function get subscriptOffset():Number {
			return _subscriptOffset;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set subscriptOffset(value:Number):void {
			_subscriptOffset = value;
		}

		/**
		 * <p> The subscriptScale value is the suggested scale factor to apply to the point size for a subscript. A scale factor of 1.0 means no scaling. </p>
		 * 
		 * @return 
		 */
		public function get subscriptScale():Number {
			return _subscriptScale;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set subscriptScale(value:Number):void {
			_subscriptScale = value;
		}

		/**
		 * <p> The superscriptOffset value is the suggested vertical offset from the Roman baseline for a superscript. </p>
		 * <p>The superscriptOffset value is used with <code>ElementFormat.baselineShift</code> to position the superscript.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="ElementFormat.html#baselineShift" target="">ElementFormat.baselineShift</a>
		 * </div>
		 * 
		 * @return 
		 */
		public function get superscriptOffset():Number {
			return _superscriptOffset;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set superscriptOffset(value:Number):void {
			_superscriptOffset = value;
		}

		/**
		 * <p> The superscriptScale value is the suggested scale factor to apply to the point size for a superscript. A scale factor of 1.0 means no scaling. </p>
		 * 
		 * @return 
		 */
		public function get superscriptScale():Number {
			return _superscriptScale;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set superscriptScale(value:Number):void {
			_superscriptScale = value;
		}

		/**
		 * <p> The underlineOffset value is the suggested vertical offset from the Roman baseline for an underline. </p>
		 * <p>Note that depending on the rotation of the line, this value is either added or subtracted from the position of the line to find the position for the underline. In a line with <code>TextRotation.ROTATE_0</code>, <code>underline.y = line.y + underlineOffset</code>. In a line with <code>TextRotation.ROTATE_90</code>, <code>underline.x = line.x - underlineOffset</code>.</p>
		 * <p>When applying decorations such as underlines to a <code>TextLine</code>, the recommended procedure is to specify an <code>eventMirror</code> on the <code>ContentElement</code> which is to receive the decoration. In response to the <code>Event.ADDED</code> event, the <code>bounds</code> of the <code>TextLineMirrorRegion</code> can be used in conjunction with the <code>underlineOffset</code> to place the underline.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="ContentElement.html#eventMirror" target="">ContentElement.eventMirror</a>
		 *  <br>
		 *  <a href="../../../flash/events/Event.html" target="">Event</a>
		 *  <br>
		 *  <a href="TextLineMirrorRegion.html" target="">TextLineMirrorRegion</a>
		 *  <br>
		 *  <a href="TextRotation.html" target="">TextRotation</a>
		 * </div>
		 * 
		 * @return 
		 */
		public function get underlineOffset():Number {
			return _underlineOffset;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set underlineOffset(value:Number):void {
			_underlineOffset = value;
		}

		/**
		 * <p> The underlineThickness value is the suggested thickness for an underline. </p>
		 * 
		 * @return 
		 */
		public function get underlineThickness():Number {
			return _underlineThickness;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set underlineThickness(value:Number):void {
			_underlineThickness = value;
		}
	}
}
