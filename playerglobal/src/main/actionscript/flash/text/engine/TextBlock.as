package flash.text.engine {
	import flash.display.DisplayObject;
	import flash.geom.Rectangle;
	import fr.lenra.playerglobal.utils.ColorUtil;
	
	
	/**
	 *  The TextBlock class is a factory for the creation of TextLine objects, which you can render by placing them on the display list. <p>The TextBlock class is intended to contain a single paragraph because the Unicode bidirectional and line-break algorithms operate on one paragraph at a time. For applications that compose multiple paragraphs of text, use a markup language, or text analysis to divide the text into paragraphs and create one TextBlock per paragraph.</p> <p>The TextBlock object stores its content in the <code>content</code> property, which is an instance of the ContentElement class. Because you can't create an instance of the ContentElement class, set <code>content</code> to an instance of one of its subclasses: TextElement, GraphicElement, or GroupElement. Use TextElement for purely text content, GraphicElement for an image or graphic content, and GroupElement for content that contains a combination of TextElement, GraphicElement, and other GroupElement objects. See the ContentElement class and its subclasses for details on managing formatted runs of text, embedded sub-runs, and graphic elements.</p> <p>After you create the TextBlock instance and set the <code>content</code> property, call the <code>createTextLine()</code> method to create lines of text, which are instances of the <code>TextLine</code> class. </p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS9dd7ed846a005b294b857bfa122bd808ea6-7fff.html" target="_blank">Creating and displaying text</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS9dd7ed846a005b294b857bfa122bd808ea6-7fe8.html" target="_blank">Flash Text Engine example: News layout</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="ContentElement.html" target="">ContentElement</a>
	 *  <br>
	 *  <a href="GraphicElement.html" target="">GraphicElement</a>
	 *  <br>
	 *  <a href="GroupElement.html" target="">GroupElement</a>
	 *  <br>
	 *  <a href="TextBaseline.html" target="">TextBaseline</a>
	 *  <br>
	 *  <a href="TextElement.html" target="">TextElement</a>
	 *  <br>
	 *  <a href="TextJustifier.html" target="">TextJustifier</a>
	 *  <br>
	 *  <a href="TextLine.html" target="">TextLine</a>
	 *  <br>
	 *  <a href="TextRotation.html" target="">TextRotation</a>
	 *  <br>
	 *  <a href="TabStop.html" target="">TabStop</a>
	 * </div><br><hr>
	 */
	public class TextBlock {
		private var _applyNonLinearFontScaling:Boolean;
		private var _baselineFontDescription:FontDescription;
		private var _baselineFontSize:Number;
		private var _baselineZero:String;
		private var _bidiLevel:int;
		private var _content:ContentElement;
		private var _lineRotation:String;
		private var _tabStops:Vector;
		private var _textJustifier:TextJustifier;
		private var _userData:*;

		private var _firstInvalidLine:TextLine;
		private var _firstLine:TextLine;
		private var _lastLine:TextLine;
		private var _textLineCreationResult:String;

		/**
		 * <p> Creates a TextBlock object </p>
		 * 
		 * @param content  — The contents of the text block. 
		 * @param tabStops  — The tab stops for the text in the text block. 
		 * @param textJustifier  — The TextJustifier object to use during line creation for this block. If no justifier is provided, a default justifier is constructed based on an English locale. 
		 * @param lineRotation  — The rotation applied to the text lines generated from the text block as units. 
		 * @param baselineZero  — Specifies which baseline is at y=0 for all lines in the block. 
		 * @param bidiLevel  — The default bidirectional embedding level of the text in the text block. 
		 * @param applyNonLinearFontScaling  — Specifies that you want to enhance screen appearance at the expense of WYSIWYG print fidelity. 
		 * @param baselineFontDescription  — Specifies a font description from which to derive line baselines for all lines in the block. 
		 * @param baselineFontSize  — Specifies the size to use with the baselineFontDescription. This parameter is ignored if <code>baselineFontDescription</code> is <code>null</code>. 
		 */
		public function TextBlock(content:ContentElement = null, tabStops:Vector = null, textJustifier:TextJustifier = null, lineRotation:String = "rotate0", baselineZero:String = "roman", bidiLevel:int = 0, applyNonLinearFontScaling:Boolean = true, baselineFontDescription:FontDescription = null, baselineFontSize:Number = 12.0) {
			this.content = content;
			this.tabStops = tabStops;
			this.textJustifier = textJustifier;
			this.lineRotation = lineRotation;
			this.baselineZero = baselineZero;
			this.bidiLevel = bidiLevel;
			this.applyNonLinearFontScaling = applyNonLinearFontScaling;
			this.baselineFontDescription = baselineFontDescription;
			this.baselineFontSize = baselineFontSize;
		}

		/**
		 * <p> Specifies that you want to enhance screen appearance at the expense of what-you-see-is-what-you-get (WYSIWYG) print fidelity. For platforms and fonts that do not support sub pixel glyph positioning during device font rendering, but do support non-linear scaling, setting this property to <code>true</code> enables the use of those metrics at some cost to WYSIWYG print fidelity, particularly for small point sizes. Non linear font scaling works by selectivly scaling the width of individual glyphs to conceal unsightly gaps caused by pixel snapping. </p>
		 * <p>On platforms which do support sub-pixel glyph positioning, this flag is ignored.</p>
		 * <p>This flag has no effect on embedded font rendering</p>
		 * <p>The default value is <code>true</code>.</p>
		 * 
		 * @return 
		 */
		public function get applyNonLinearFontScaling():Boolean {
			return _applyNonLinearFontScaling;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set applyNonLinearFontScaling(value:Boolean):void {
			_applyNonLinearFontScaling = value;
		}

		/**
		 * <p> The font used to determine the baselines for all the lines created from the block, independent of their content. Baselines depend on font and font size. </p>
		 * <p>The default value is <code>null</code>. When the baseline font is <code>null</code>, the baseline font size is ignored and the baseline for any given line is based on the font and size of the largest text in the line. When you specify both <code>baselineFontDescription</code> and <code>baselineFontSize</code>, they determine the baselines for all the lines in the text block, independent of their content. This combination is most often useful in Asian typography.</p>
		 * 
		 * @return 
		 */
		public function get baselineFontDescription():FontDescription {
			return _baselineFontDescription;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set baselineFontDescription(value:FontDescription):void {
			_baselineFontDescription = value;
		}

		/**
		 * <p> The font size used to calculate the baselines for the lines created from the block. Baselines depend on font and font size. </p>
		 * <p>The default value is <code>12</code>. When the baseline font is <code>null</code>, the baseline font size is ignored and the baseline for any given line is based on the font and size of the largest text in the line.</p>
		 * 
		 * @return 
		 */
		public function get baselineFontSize():Number {
			return _baselineFontSize;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set baselineFontSize(value:Number):void {
			_baselineFontSize = value;
		}

		/**
		 * <p> Specifies which baseline is at y=0 for lines created from this block. Valid values for this property are found in the members of the <code>TextBaseline</code> class. </p>
		 * <p>The default value is <code>TextBaseline.ROMAN</code>.</p>
		 * <p>To set values for this property, use the following string values:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>String value</th>
		 *    <th>Description</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextBaseline.ROMAN</code></td>
		 *    <td>The roman baseline of the lines is at y=0.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextBaseline.ASCENT</code></td>
		 *    <td>The ascent baseline of the lines is at y=0.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextBaseline.DESCENT</code></td>
		 *    <td>The descent baseline of the lines is at y=0.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextBaseline.IDEOGRAPHIC_TOP</code></td>
		 *    <td>The ideographic top baseline of the lines is at y=0.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextBaseline.IDEOGRAPHIC_CENTER</code></td>
		 *    <td>The ideographic center baseline of the lines is at y=0.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextBaseline.IDEOGRAPHIC_BOTTOM</code></td>
		 *    <td>The ideographic bottom baseline of the lines is at y=0.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * 
		 * @return 
		 */
		public function get baselineZero():String {
			return _baselineZero;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set baselineZero(value:String):void {
			_baselineZero = value;
		}

		/**
		 * <p> Specifies the bidirectional <i>paragraph embedding level</i> of the text block. In general, use 0 for LTR paragraphs (English, Japanese, etc) and 1 for RTL paragraphs (Arabic, Hebrew, etc). The use of values other than 0 or 1 is reserved for special cases - see UAX#9, "Unicode Bidirectional Algorithm" (http://www.unicode.org/reports/tr9/) for details. </p>
		 * <p>The default value is 0.</p>
		 * <p>Modifying <code>bidiLevel</code> changes the validity of all previously broken lines to TextLineValidity.INVALID. After changing <code>bidiLevel</code>, the <code>firstInvalidLine</code> property equals the <code>firstLine</code> property, and you must rebreak all the lines in the TextBlock.</p>
		 * 
		 * @return 
		 */
		public function get bidiLevel():int {
			return _bidiLevel;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set bidiLevel(value:int):void {
			_bidiLevel = value;
		}

		/**
		 * <p> Holds the contents of the text block. Because ContentElement is a base class, assign <code>content</code> an instance of a ContentElement subclass: TextElement, GraphicElement, or GroupElement. A TextElement object contains a String, a GraphicElement object contains a DisplayObject, and a GroupElement contains a Vector object that contains one or more TextElement, GraphicElement, or other GroupElement objects. Use a TextElement for a paragraph of homogenous text, a GraphicElement for a graphic, and a GroupElement for a combination of text and graphic elements or multiples instances of these elements, as well as other GroupElement objects. </p>
		 * <p>The default value is <code>null</code>.</p>
		 * <p>Modifying the <code>content</code> property changes the validity of all previously created lines to <code>TextLineValidity.INVALID</code>. After changing <code>content</code>, the <code>firstInvalidLine</code> property equals the <code>firstLine</code> property and you must rebreak all lines in the TextBlock.</p>
		 * 
		 * @return 
		 */
		public function get content():ContentElement {
			return _content;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set content(value:ContentElement):void {
			_content = value;
			if (value!=null)
				value.setTextBlock(this);
		}

		/**
		 * <p> Identifies the first line in the text block in which <code>TextLine.validity</code> is not equal to <code>TextLineValidity.VALID</code>. </p>
		 * <p>The default value is <code>null</code>.</p>
		 * 
		 * @return 
		 */
		public function get firstInvalidLine():TextLine {
			return _firstInvalidLine;
		}

		/**
		 * <p> The first TextLine in the TextBlock, if any. </p>
		 * <p>The default value is <code>null</code>.</p>
		 * 
		 * @return 
		 */
		public function get firstLine():TextLine {
			return _firstLine;
		}

		/**
		 * <p> The last TextLine in the TextBlock, if any. </p>
		 * <p>The default value is <code>null</code>.</p>
		 * 
		 * @return 
		 */
		public function get lastLine():TextLine {
			return _lastLine;
		}

		/**
		 * <p> Rotates the text lines in the text block as a unit. Call the <code>createTextLine()</code> method <i>after</i> setting <code>lineRotation</code> for it to take effect. The default value is <code>TextRotation.ROTATE_0</code>. </p>
		 * <p>The final rotation of any glyph depends on the values of <code>ElementFormat.textRotation</code>, <code>ContentElement.textRotation</code>, and <code>TextBlock.lineRotation</code>.</p>
		 * <p><code>TextBlock.lineRotation</code> is typically used for Asian text. To create a paragraph of vertical Japanese text, do the following:</p>
		 * <ol>
		 *  <li>Set the <code>TextBlock.lineRotation</code> property to <code>TextRotation.ROTATE_90</code>.</li>
		 *  <li>Leave the <code>ElementFormat.textRotation</code> property of the content as the default, <code>TextRotation.AUTO</code>.</li>
		 * </ol>
		 * <p>Use the following constants, which are defined in the <code>TextRotation</code> class, to set the value for this property:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>String value</th>
		 *    <th>Description</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextRotation.ROTATE_0</code></td>
		 *    <td>Lines are not rotated.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextRotation.ROTATE_90</code></td>
		 *    <td>Lines are rotated 90 degrees clockwise.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextRotation.ROTATE_180</code></td>
		 *    <td>Lines are rotated 180 degrees.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextRotation.ROTATE_270</code></td>
		 *    <td>Lines are rotated 270 degrees clockwise.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextRotation.AUTO</code></td>
		 *    <td>Not supported.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * 
		 * @return 
		 */
		public function get lineRotation():String {
			return _lineRotation;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set lineRotation(value:String):void {
			_lineRotation = value;
		}

		/**
		 * <p> Specifies the tab stops for the text in the text block, in the form of a Vector of <code>TabStop</code> objects. </p>
		 * <p>The default value is <code>null</code>, which means no tab stops are specified. When no tab stops are specified (or the insertion point is beyond the last specified tab stop) the runtime creates half-inch tabs by default.</p>
		 * <p>When the <code>tabStops</code> property is set, the TextBlock makes a copy of the Vector for internal use. Modifying the original Vector or its contents does not affect the TextBlock. When the <code>tabStops</code> property is queried, a copy of the internal Vector is returned. Again, modifying this returned vector or its contents does not affect the TextBlock.</p>
		 * 
		 * @return 
		 */
		public function get tabStops():Vector {
			return _tabStops;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set tabStops(value:Vector):void {
			_tabStops = value;
		}

		/**
		 * <p> Specifies the TextJustifier to use during line creation. </p>
		 * <p>The default value is a constructed default TextJustifier object.</p>
		 * <p>When the <code>textJustifier</code> property is set, the TextBlock makes a copy of the object for internal use. Modifying the original object does not affect the TextBlock. When the <code>textJustifier</code> property is queried, a copy of the internal object is returned. Again, modifying this returned object does not affect the TextBlock.</p>
		 * 
		 * @return 
		 */
		public function get textJustifier():TextJustifier {
			return _textJustifier;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set textJustifier(value:TextJustifier):void {
			_textJustifier = value;
		}

		/**
		 * <p> Indicates the result of a <code>createTextLine()</code> operation. Changing the content of the block invalidates previously broken lines and resets this property to <code>null</code>. </p>
		 * <p>The default value is <code>null</code>.</p>
		 * <p>Values for this property are found in <code>TextLineCreationResult</code></p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>String value</th>
		 *    <th>Description</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextLineCreationResult.SUCCESS</code></td>
		 *    <td>The line was successfully broken.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextLineCreationResult.COMPLETE</code></td>
		 *    <td>Either the new line created aligned perfectly with following lines which have transitioned from POSSIBLY_INVALID to VALID, or no line was created because all text in the block had already been broken.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>TextLineCreationResult.INSUFFICIENT_WIDTH</code></td>
		 *    <td>No line was created because no text could fit in the specified width.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * 
		 * @return 
		 */
		public function get textLineCreationResult():String {
			return _textLineCreationResult;
		}

		/**
		 * <p> Provides a way for the application to associate arbitrary data with the text block. The data could be information that refers to the content, such as a revision date or the name of the author, or it could be cached data that you use during processing. </p>
		 * 
		 * @return 
		 */
		public function get userData():* {
			return _userData;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set userData(value:*):void {
			_userData = value;
		}

		/**
		 * <p> Instructs the text block to create a line of text from its content, beginning at the point specified by the <code>previousLine</code> parameter and breaking at the point specified by the <code>width</code> parameter. The text line is a TextLine object, which you can add to the display list. </p>
		 * <p>Breaking lines over a range of a text block that has already been broken can change the validity of lines in and beyond the area where breaking takes place. The status of lines can change from VALID to INVALID or POSSIBLY_INVALID. If a newly broken line aligns perfectly with a previously broken line which has a status of POSSIBLY_INVALID, that previously broken line and all following POSSIBLY_INVALID lines change back to a status of VALID. The validity of lines that have been set to values that are not members of <code>TextLineValidity</code> do not change to VALID, but could change to INVALID. Check the <code>firstInvalidLine</code> property after any change to the text block to see where to begin or continue rebreaking text lines.</p>
		 * <p>You can create artificial word breaks by including the Unicode Zero Width Space (ZWSP) character in the text. This can be useful for languages such as Thai, which require a dictionary for correct line breaking. The Flash runtime does not include such a dictionary.</p>
		 * <p>In order to reduce memory overhead, when all the desired lines have been created, unless it is expected that the lines will need to be repeatedly rebroken due to, for example, the resizing of the container, the user should call the <code>releaseLineCreationData()</code> method allowing the text block to dispose of the temporary data associated with line breaking.</p>
		 * 
		 * @param previousLine  — Specifies the previously broken line after which breaking is to commence. Can be <code>null</code> when breaking the first line. 
		 * @param width  — Specifies the desired width of the line in pixels. The actual width may be less. 
		 * @param lineOffset  — An optional parameter which specifies the difference in pixels between the origin of the line and the origin of the tab stops. This can be used when lines are not aligned, but it is desirable for their tabs to be so. The default value for this parameter is <code>0.0</code>. 
		 * @param fitSomething  — An optional parameter which instructs Flash Player to fit at least one character into the text line, no matter what width has been specified (even if width is zero or negative, which would otherwise result in an exception being thrown). 
		 * @return  — A text line, or  if the text block is empty or the width specified is less than the width of the next element. To distinguish between these cases, check the  property of the text block. 
		 */
		public function createTextLine(previousLine:TextLine = null, width:Number = 1000000, lineOffset:Number = 0.0, fitSomething:Boolean = false):TextLine {
			if (width < 0) throw new ArgumentError("The width parameter can't be less than 0");
			// Define the result to success as default
			this._textLineCreationResult = TextLineCreationResult.SUCCESS;
			
			var contents:Vector.<ContentElement> = new Vector.<ContentElement>();
			contents.push(content);
			var i:int = 0;
			while (i < contents.length) {
				if (contents[i] is GroupElement) {
					var els:Array = [i, 1];
					var g:GroupElement = contents[i] as GroupElement;
					for (var j:int = 0; j < g.elementCount; j++) {
						els.push(g.getElementAt(j));
					}
					contents.splice.apply(contents, els);
				}
				else
					++i;
			}
			
			var elements:Vector.<SVGElement> = new Vector.<SVGElement>();
			var overWidth:Boolean = false;
			var blockIndex:int = previousLine != null ? previousLine.textBlockBeginIndex + previousLine.rawTextLength : 0;
			var blockPos:int = blockIndex;
			var linePos:Number = lineOffset;
			var textOffset:int = 0;
			var maxAscent:Number = 0;
			var maxDescent:Number = 0;
			for each (var c:ContentElement in contents) {
				var text:String = c.text;
				if (blockPos < textOffset + text.length) {
					text = text.substring(blockPos-textOffset);
					// Initiate the visual text element
					var t:SVGTextElement = document.createElementNS(DisplayObject.SVG_NAMESPACE, "text") as SVGTextElement;
					var tRef:Object = t;
					
					t.style.pointerEvents = "none";
					if (c.elementFormat!=null) {
						t.style.fontSize = c.elementFormat.fontSize + "px";
						if (c.elementFormat.fontDescription!=null) {
							t.style.fontFamily = c.elementFormat.fontDescription.fontName;
							t.style.fontWeight = c.elementFormat.fontDescription.fontWeight;
							t.style.fontStyle = c.elementFormat.fontDescription.fontPosture;
						}
						t.style.textTransform = c.elementFormat.typographicCase;
						t.setAttribute("fill", ColorUtil.toColorString(c.elementFormat.color as int));
					}
					
					// Try the full remaining text
					t.innerHTML = text;
					
					var finalBoundingRect:Rectangle = DisplayObject.render(t, function():Rectangle {
						// While the text doesn't fit in the width, cut off the last word
						//TODO: manage tabs and line breaks
						var tmp:Object = tRef.getBBox();
						var ret:Rectangle = new Rectangle(tmp.x, tmp.y, tmp.width, tmp.height);
						var remainingWidth:Number = width - linePos;
						while (ret.width > remainingWidth && text.indexOf(" ") !=-1) {
							var pos:int = Math.min(Math.ceil(text.length * remainingWidth / ret.width * 1.02), text.lastIndexOf(" "));
							text = text.substring(0, text.indexOf(" ", pos));
							t.innerHTML = text;
							tmp = tRef.getBBox();
							ret.x = tmp.x;
							ret.y = tmp.y;
							ret.width = tmp.width;
							ret.height = tmp.height;
						}
						return ret;
					});
					if (linePos + finalBoundingRect.width > width) {
						overWidth = true;
						if (elements.length == 0) {
							if (fitSomething) {
								finalBoundingRect = DisplayObject.render(t, function():Rectangle {
									// While the text doesn't fit in the width, cut off the last char
									var tmp:Object = tRef.getBBox();
									var ret:Rectangle = new Rectangle(tmp.x, tmp.y, tmp.width, tmp.height);
									while (linePos + ret.width > width && text.length>0) {
										text = text.substring(0, text.length-1);
										t.innerHTML = text;
										tmp = tRef.getBBox();
										ret.x = tmp.x;
										ret.y = tmp.y;
										ret.width = tmp.width;
										ret.height = tmp.height;
									}
									return ret;
								});
								this._textLineCreationResult = TextLineCreationResult.EMERGENCY;
							}
							else {
								this._textLineCreationResult = TextLineCreationResult.INSUFFICIENT_WIDTH;
								return null;
							}
						}
						else
							break;
					}
					if (maxAscent <-finalBoundingRect.top)
						maxAscent = -finalBoundingRect.top;
					if (maxDescent < finalBoundingRect.bottom)
						maxDescent = finalBoundingRect.bottom;
					t.setAttribute("x", linePos);
					blockPos += text.length;
					linePos += finalBoundingRect.width;
					elements.push(t);
					if (overWidth) {
						break;
					}
				}
				textOffset += text.length;
			}
			
			// If the text still doesn't fit, define the correct result
			if (!overWidth && elements.length==0) {
				this._textLineCreationResult = TextLineCreationResult.COMPLETE;
				return null;
			}
			
			// Create the returned TextLine
			var tl:TextLineImplementation = new TextLineImplementation(this, blockIndex, blockPos - blockIndex, previousLine, width, maxAscent, maxDescent);
			for each (var el:SVGElement in elements) {
				tl.svgElement.appendChild(el);
			}
			if (previousLine != null)
				(previousLine as TextLineImplementation).nextLine = tl;
			else
				this._firstLine = tl;
			this._lastLine = tl;
			return tl;
		}

		/**
		 * <p> Dumps the underlying contents of the TextBlock as an XML string. This can be useful in automated testing, and includes text, formatting, and layout information. </p>
		 * <p>The following describes the output:</p>
		 * <pre>
		 * 	 &gt;block&lt;
		 * 	 	[0-N LINE]
		 * 	 &gt;/block&lt;
		 * 	 </pre>
		 * <p>For a description of the output for each line, see the TextLine.dump() method.</p>
		 * <p><b>Note:</b> The content and format of the output may change in the future. Adobe does not guarantee backward compatibility of this method.</p>
		 * 
		 * @return 
		 */
		public function dump():String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Finds the index of the next atom boundary from the specified character index, not including the character at the specified index. The characters between atom boundaries combine to form one atom in a <code>TextLine</code>, such as an 'e' and a combining acute accent. </p>
		 * 
		 * @param afterCharIndex  — Specifies the index of the character from which to search for the next atom boundary. 
		 * @return  — The index of the next atom boundary from the specified character index. 
		 */
		public function findNextAtomBoundary(afterCharIndex:int):int {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Finds the index of the next word boundary from the specified character index, not including the character at the specified index. Word boundaries are determined based on the Unicode properties of the characters. </p>
		 * 
		 * @param afterCharIndex  — Specifies the index of the character from which to search for the next word boundary. 
		 * @return  — The index of the next word boundary from the specified character index. 
		 */
		public function findNextWordBoundary(afterCharIndex:int):int {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Finds the index of the previous atom boundary to the specified character index, not including the character at the specified index. The characters between atom boundaries combine to form one atom in a <code>TextLine</code>, such as an 'e' and a combining acute accent. </p>
		 * 
		 * @param beforeCharIndex  — Specifies the index of the character from which to search for the previous atom boundary. 
		 * @return  — The index of the previous atom boundary to the specified character index. 
		 */
		public function findPreviousAtomBoundary(beforeCharIndex:int):int {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Finds the index of the previous word boundary to the specified character index, not including the character at the specified index. Word boundaries are determined based on the Unicode properties of the characters. </p>
		 * 
		 * @param beforeCharIndex  — Specifies the index of the character from which to search for the previous word boundary. 
		 * @return  — The index of the previous word boundary to the specified character index. 
		 */
		public function findPreviousWordBoundary(beforeCharIndex:int):int {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the TextLine containing the character specified by the <code>charIndex</code> parameter. </p>
		 * 
		 * @param charIndex  — The zero-based index value of the character (for example, the first character is 0, the second character is 1, and so on). 
		 * @return  — The TextLine containing the character at . 
		 */
		public function getTextLineAtCharIndex(charIndex:int):TextLine {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Instructs the text block to re-use an existing text line to create a line of text from its content, beginning at the point specified by the <code>previousLine</code> parameter and breaking at the point specified by the <code>width</code> parameter. The text line is a TextLine object, which you can add to the display list. By re-using an existing text line, performance is enhanced due to reduced object creation. </p>
		 * <p>The <code>textLine</code> being recreated is released from whatever text block it is in, if any. In addition, all properties, including inherited properties from <code>DisplayObjectContainer</code>, <code>InteractiveObject</code>, and <code>DisplayObject</code> are reset to their default values. Finally, all children of the line are removed including graphic elements and other decorations, and all event listeners on the line are removed. To improve performance, the only exception to this complete reset is that the line itself is not removed from its parent.</p>
		 * <p>Breaking lines over a range of a text block that has already been broken can change the validity of lines in and beyond the area where breaking takes place. The status of lines can change from VALID to INVALID or POSSIBLY_INVALID. If a newly broken line aligns perfectly with a previously broken line which has a status of POSSIBLY_INVALID, that previously broken line and all following POSSIBLY_INVALID lines change back to a status of VALID. The validity of lines that have been set to values that are not members of <code>TextLineValidity</code> do not change to VALID, but could change to INVALID. Check the <code>firstInvalidLine</code> property after any change to the text block to see where to begin or continue rebreaking text lines.</p>
		 * <p>You can create artificial word breaks by including the Unicode Zero Width Space (ZWSP) character in the text. This can be useful for languages such as Thai, which require a dictionary for correct line breaking. The Flash runtime does not include such a dictionary.</p>
		 * <p>In order to reduce memory overhead, when all the desired lines have been created, unless it is expected that the lines will need to be repeatedly rebroken due to, for example, the resizing of the container, the user should call the <code>releaseLineCreationData()</code> method allowing the text block to dispose of the temporary data associated with line breaking.</p>
		 * 
		 * @param textLine  — Specifies a previously created TextLine to be re-used. 
		 * @param previousLine  — Specifies the previously broken line after which breaking is to commence. Can be <code>null</code> when breaking the first line. 
		 * @param width  — Specifies the desired width of the line in pixels. The actual width may be less. 
		 * @param lineOffset  — An optional parameter which specifies the difference in pixels between the origin of the line and the origin of the tab stops. This can be used when lines are not aligned, but it is desirable for their tabs to be so. The default value for this parameter is <code>0.0</code>. 
		 * @param fitSomething  — An optional parameter which instructs Flash Player to fit at least one character into the text line, no matter what width has been specified (even if width is zero or negative, which would otherwise result in an exception being thrown). 
		 * @return  — A text line, or  if the text block is empty or the width specified is less than the width of the next element. To distinguish between these cases, check the  property of the text block. 
		 */
		public function recreateTextLine(textLine:TextLine, previousLine:TextLine = null, width:Number = 1000000, lineOffset:Number = 0.0, fitSomething:Boolean = false):TextLine {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Instructs the text block to release all the temporary data associated with the creation of text lines. To minimize an application's memory foot print, you should call the <code>releaseLineCreationData()</code> method when you are done creating text lines from a text block. However, to maximize performance for rebreaking the lines (for example when the container is resized) the <code>releaseLineCreationData()</code> method should not be called. It is up to the application to balance memory vs. performance. </p>
		 * <p>The recommended process for text that is not expected to change is: initialize a text block, call the <code>createTextLine()</code> method as often as necessary to create the desired output, and then call the <code>releaseLineCreationData()</code> method.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="TextBlock.html#createTextLine()" target="">TextBlock.createTextLine()</a>
		 * </div>
		 */
		public function releaseLineCreationData():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Removes a range of text lines from the list of lines maintained by the TextBlock. This allows the lines to be garbage-collected if no other references exist. </p>
		 * <p>Sets the <code>textBlock</code>, <code>nextLine</code>, and <code>previousLine</code> members of the removed lines to <code>null</code>. Sets the <code>validity</code> of the removed lines and of all lines which follow the removed lines in the TextBlock to <code>TextLineValidity.INVALID</code>.</p>
		 * 
		 * @param firstLine  — Specifies the first line to release. 
		 * @param lastLine  — Specifies the last line to release. 
		 */
		public function releaseLines(firstLine:TextLine, lastLine:TextLine):void {
			throw new Error("Not implemented");
		}
	}
}

import flash.text.engine.TextBlock;
import flash.text.engine.TextLine;

class TextLineImplementation extends TextLine {
	public function TextLineImplementation(textBlock:TextBlock, textBlockBeginIndex:int, rawTextLength:int, previousLine:TextLine, specifiedWidth:Number, ascent:Number, descent:Number) {
		this._textBlock = textBlock;
		this._textBlockBeginIndex = textBlockBeginIndex;
		this._rawTextLength = rawTextLength;
		this._previousLine = previousLine;
		this._specifiedWidth = specifiedWidth;
		this._ascent = ascent;
		this._descent = descent;
	}
	
	/**
	 * <p> The next TextLine in the TextBlock, or <code>null</code> if the current line is the last line in the block or the validity of the line is <code>TextLineValidity.STATIC</code>. </p>
	 * 
	 * @return 
	 */
	override public function get nextLine():TextLine {
		return _nextLine;
	}
	
	public function set nextLine(value:TextLine):void {
		_nextLine = value;
	}
}