package flash.text {
	/**
	 *  The TextColorType class provides color values for the flash.text.TextRenderer class. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="TextRenderer.html" target="">flash.text.TextRenderer</a>
	 * </div><br><hr>
	 */
	public class TextColorType {
		/**
		 * <p> Used in the <code>colorType</code> parameter in the <code>setAdvancedAntiAliasingTable()</code> method. Use the syntax <code>TextColorType.DARK_COLOR</code>. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="TextRenderer.html#setAdvancedAntiAliasingTable()" target="">flash.text.TextRenderer.setAdvancedAntiAliasingTable()</a>
		 * </div>
		 */
		public static const DARK_COLOR:String = "dark";
		/**
		 * <p> Used in the <code>colorType</code> parameter in the <code>setAdvancedAntiAliasingTable()</code> method. Use the syntax <code>TextColorType.LIGHT_COLOR</code>. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="TextRenderer.html#setAdvancedAntiAliasingTable()" target="">flash.text.TextRenderer.setAdvancedAntiAliasingTable()</a>
		 * </div>
		 */
		public static const LIGHT_COLOR:String = "light";
	}
}
