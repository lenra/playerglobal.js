package flash.text {
	/**
	 *  The CSMSettings class contains properties for use with the <code>TextRenderer.setAdvancedAntiAliasingTable()</code> method to provide continuous stroke modulation (CSM). CSM is the continuous modulation of both stroke weight and edge sharpness. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="TextRenderer.html#setAdvancedAntiAliasingTable()" target="">TextRenderer.setAdvancedAntiAliasingTable()</a>
	 * </div><br><hr>
	 */
	public class CSMSettings {
		private var _fontSize:Number;
		private var _insideCutoff:Number;
		private var _outsideCutoff:Number;

		/**
		 * <p> Creates a new CSMSettings object which stores stroke values for custom anti-aliasing settings. </p>
		 * 
		 * @param fontSize  — The size, in pixels, for which the settings apply. 
		 * @param insideCutoff  — The inside cutoff value, above which densities are set to a maximum density value (such as 255). 
		 * @param outsideCutoff  — The outside cutoff value, below which densities are set to zero. 
		 */
		public function CSMSettings(fontSize:Number, insideCutoff:Number, outsideCutoff:Number) {
			throw new Error("Not implemented");
		}

		/**
		 * <p> The size, in pixels, for which the settings apply. </p>
		 * <p>The <code>advancedAntiAliasingTable</code> array passed to the <code>setAdvancedAntiAliasingTable()</code> method can contain multiple entries that specify CSM settings for different font sizes. Using this property, you can specify the font size to which the other settings apply. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="TextRenderer.html#setAdvancedAntiAliasingTable()" target="">TextRenderer.setAdvancedAntiAliasingTable()</a>
		 * </div>
		 * 
		 * @return 
		 */
		public function get fontSize():Number {
			return _fontSize;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set fontSize(value:Number):void {
			_fontSize = value;
		}

		/**
		 * <p> The inside cutoff value, above which densities are set to a maximum density value (such as 255). </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="TextRenderer.html#setAdvancedAntiAliasingTable()" target="">TextRenderer.setAdvancedAntiAliasingTable()</a>
		 * </div>
		 * 
		 * @return 
		 */
		public function get insideCutoff():Number {
			return _insideCutoff;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set insideCutoff(value:Number):void {
			_insideCutoff = value;
		}

		/**
		 * <p> The outside cutoff value, below which densities are set to zero. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="TextRenderer.html#setAdvancedAntiAliasingTable()" target="">TextRenderer.setAdvancedAntiAliasingTable()</a>
		 * </div>
		 * 
		 * @return 
		 */
		public function get outsideCutoff():Number {
			return _outsideCutoff;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set outsideCutoff(value:Number):void {
			_outsideCutoff = value;
		}
	}
}
