package flash.text {
	import flash.events.EventDispatcher;

	/**
	 *  The StyleSheet class lets you create a StyleSheet object that contains text formatting rules for font size, color, and other styles. You can then apply styles defined by a style sheet to a TextField object that contains HTML- or XML-formatted text. The text in the TextField object is automatically formatted according to the tag styles defined by the StyleSheet object. You can use text styles to define new formatting tags, redefine built-in HTML tags, or create style classes that you can apply to certain HTML tags. <p>To apply styles to a TextField object, assign the StyleSheet object to a TextField object's <code>styleSheet</code> property.</p> <p> <b>Note:</b> A text field with a style sheet is not editable. In other words, a text field with the <code>type</code> property set to <code>TextFieldType.INPUT</code> applies the StyleSheet to the default text for the text field, but the content will no longer be editable by the user. Consider using the TextFormat class to assign styles to input text fields.</p> <p>Flash Player supports a subset of properties in the original CSS1 specification (<a href="http://www.w3.org/TR/REC-CSS1" target="external">www.w3.org/TR/REC-CSS1</a>). The following table shows the supported Cascading Style Sheet (CSS) properties and values, as well as their corresponding ActionScript property names. (Each ActionScript property name is derived from the corresponding CSS property name; if the name contains a hyphen, the hyphen is omitted and the subsequent character is capitalized.)</p> <table class="innertable">
	 *  <tbody>
	 *   <tr>
	 *    <th>CSS property</th>
	 *    <th>ActionScript property</th>
	 *    <th>Usage and supported values</th>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>color</code> </td>
	 *    <td> <code>color</code> </td>
	 *    <td>Only hexadecimal color values are supported. Named colors (such as <code>blue</code>) are not supported. Colors are written in the following format: <code>#FF0000</code>.</td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>display</code> </td>
	 *    <td> <code>display</code> </td>
	 *    <td>Supported values are <code>inline</code>, <code>block</code>, and <code>none</code>.</td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>font-family</code> </td>
	 *    <td> <code>fontFamily</code> </td>
	 *    <td>A comma-separated list of fonts to use, in descending order of desirability. Any font family name can be used. If you specify a generic font name, it is converted to an appropriate device font. The following font conversions are available: <code>mono</code> is converted to <code>_typewriter</code>, <code>sans-serif</code> is converted to <code>_sans</code>, and <code>serif</code> is converted to <code>_serif</code>.</td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>font-size</code> </td>
	 *    <td> <code>fontSize</code> </td>
	 *    <td>Only the numeric part of the value is used. Units (px, pt) are not parsed; pixels and points are equivalent.</td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>font-style</code> </td>
	 *    <td> <code>fontStyle</code> </td>
	 *    <td>Recognized values are <code>normal</code> and <code>italic</code>.</td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>font-weight</code> </td>
	 *    <td> <code>fontWeight</code> </td>
	 *    <td>Recognized values are <code>normal</code> and <code>bold</code>.</td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>kerning</code> </td>
	 *    <td> <code>kerning</code> </td>
	 *    <td>Recognized values are <code>true</code> and <code>false</code>. Kerning is supported for embedded fonts only. Certain fonts, such as Courier New, do not support kerning. The kerning property is only supported in SWF files created in Windows, not in SWF files created on the Macintosh. However, these SWF files can be played in non-Windows versions of Flash Player and the kerning still applies.</td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>leading</code> </td>
	 *    <td> <code>leading</code> </td>
	 *    <td>The amount of space that is uniformly distributed between lines. The value specifies the number of pixels that are added after each line. A negative value condenses the space between lines. Only the numeric part of the value is used. Units (px, pt) are not parsed; pixels and points are equivalent.</td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>letter-spacing</code> </td>
	 *    <td> <code>letterSpacing</code> </td>
	 *    <td>The amount of space that is uniformly distributed between characters. The value specifies the number of pixels that are added after each character. A negative value condenses the space between characters. Only the numeric part of the value is used. Units (px, pt) are not parsed; pixels and points are equivalent.</td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>margin-left</code> </td>
	 *    <td> <code>marginLeft</code> </td>
	 *    <td>Only the numeric part of the value is used. Units (px, pt) are not parsed; pixels and points are equivalent. </td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>margin-right</code> </td>
	 *    <td> <code>marginRight</code> </td>
	 *    <td>Only the numeric part of the value is used. Units (px, pt) are not parsed; pixels and points are equivalent.</td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>text-align</code> </td>
	 *    <td> <code>textAlign</code> </td>
	 *    <td>Recognized values are <code>left</code>, <code>center</code>, <code>right</code>, and <code>justify</code>.</td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>text-decoration</code> </td>
	 *    <td> <code>textDecoration</code> </td>
	 *    <td>Recognized values are <code>none</code> and <code>underline</code>.</td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>text-indent</code> </td>
	 *    <td> <code>textIndent</code> </td>
	 *    <td>Only the numeric part of the value is used. Units (px, pt) are not parsed; pixels and points are equivalent. </td>
	 *   </tr>
	 *  </tbody>
	 * </table> <p> </p> <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="TextField.html" target="">flash.text.TextField</a>
	 * </div><br><hr>
	 */
	public class StyleSheet extends EventDispatcher {
		private static const CSS_RULE_REGEX:RegExp = /^([^{}]*)\s*{([^{}]*)}\s*/;
		private static const CSS_PROPERTY_REGEX:RegExp = /([^{}:;]+):([^{}:;]+);/;
		
		private var _styleNames:Array;
		private var _style:Object = {};

		/**
		 * <p> Creates a new StyleSheet object. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="StyleSheet.html#getStyle()" target="">flash.text.StyleSheet.getStyle()</a>
		 * </div>
		 */
		public function StyleSheet() {
			super(this);
		}

		/**
		 * <p> An array that contains the names (as strings) of all of the styles registered in this style sheet. </p>
		 * 
		 * @return 
		 */
		public function get styleNames():Array {
			return _styleNames;
		}

		/**
		 * <p> Removes all styles from the style sheet object. </p>
		 */
		public function clear():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a copy of the style object associated with the style named <code>styleName</code>. If there is no style object associated with <code>styleName</code>, <code>null</code> is returned. </p>
		 * 
		 * @param styleName  — A string that specifies the name of the style to retrieve. 
		 * @return  — An object. 
		 */
		public function getStyle(styleName:String):Object {
			return this._style[styleName];
		}

		/**
		 * <p> Parses the CSS in <code>CSSText</code> and loads the style sheet with it. If a style in <code>CSSText</code> is already in <code>styleSheet</code>, the properties in <code>styleSheet</code> are retained, and only the ones in <code>CSSText</code> are added or changed in <code>styleSheet</code>. </p>
		 * <p>To extend the native CSS parsing capability, you can override this method by creating a subclass of the StyleSheet class.</p>
		 * 
		 * @param CSSText  — The CSS text to parse (a string). 
		 */
		public function parseCSS(CSSText:String):void {
			// get all css rules
			var o:Object;
			while ((o = CSS_RULE_REGEX.exec(CSSText)) != null) {
				
				// get the selectors
				var selectors:Array = (o[1] as String).split(",");
				for (var i:int = 0; i < selectors.length; i++) {
					var selector:String = (selectors[i] as String).trim();
					var rule:Object = this._style[selector];
					if (rule == null) {
						rule = {};
						this._style[selector] = rule;
					}
					selectors[i] = rule;
				}
				
				// parse properties
				var propStr:String = o[2];
				
				while ((o = CSS_PROPERTY_REGEX.exec(propStr)) != null) {
					var propertyName:String = cssPropertyNameToJsPropertyName(o[1].trim());
					var propertyValue:String = o[2].trim();
					
					for each (rule in selectors) {
						rule[propertyName] = propertyValue;
					}
					
					propStr = propStr.replace(CSS_PROPERTY_REGEX, "");
				}
				
				CSSText = CSSText.replace(CSS_RULE_REGEX, "");
			}
		}

		/**
		 * <p> Adds a new style with the specified name to the style sheet object. If the named style does not already exist in the style sheet, it is added. If the named style already exists in the style sheet, it is replaced. If the <code>styleObject</code> parameter is <code>null</code>, the named style is removed. </p>
		 * <p>Flash Player creates a copy of the style object that you pass to this method.</p>
		 * <p>For a list of supported styles, see the table in the description for the StyleSheet class.</p>
		 * 
		 * @param styleName  — A string that specifies the name of the style to add to the style sheet. 
		 * @param styleObject  — An object that describes the style, or <code>null</code>. 
		 */
		public function setStyle(styleName:String, styleObject:Object):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Extends the CSS parsing capability. Advanced developers can override this method by extending the StyleSheet class. </p>
		 * 
		 * @param formatObject  — An object that describes the style, containing style rules as properties of the object, or <code>null</code>. 
		 * @return  — A TextFormat object containing the result of the mapping of CSS rules to text format properties. 
		 */
		public function transform(formatObject:Object):TextFormat {
			throw new Error("Not implemented");
		}
		
		private static function cssPropertyNameToJsPropertyName(propertyName:String):String {
			var ind:int;
			while ((ind = propertyName.indexOf("-")) !=-1) {
				propertyName = propertyName.substr(0, ind) + propertyName.substr(ind + 1, 1).toUpperCase() + propertyName.substr(ind + 2);
			}
			return propertyName;
		}
	}
}
