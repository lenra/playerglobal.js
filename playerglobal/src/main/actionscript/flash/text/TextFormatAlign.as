package flash.text {
	/**
	 *  The TextFormatAlign class provides values for text alignment in the TextFormat class. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="TextFormat.html" target="">flash.text.TextFormat</a>
	 * </div><br><hr>
	 */
	public class TextFormatAlign {
		/**
		 * <p> Constant; centers the text in the text field. Use the syntax <code>TextFormatAlign.CENTER</code>. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="TextFormat.html#align" target="">flash.text.TextFormat.align</a>
		 * </div>
		 */
		public static const CENTER:String = "center";
		/**
		 * <p> Constant; aligns text to the end edge of a line. Same as right for left-to-right languages and same as left for right-to-left languages. </p>
		 * <p>The <code>END</code> constant may only be used with the <code>StageText</code> class.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="StageText.html#textAlign" target="">flash.text.StageText.textAlign</a>
		 *  <br>
		 *  <a href="StageText.html#locale" target="">flash.text.StageText.locale</a>
		 * </div>
		 */
		public static const END:String = "end";
		/**
		 * <p> Constant; justifies text within the text field. Use the syntax <code>TextFormatAlign.JUSTIFY</code>. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="TextFormat.html#align" target="">flash.text.TextFormat.align</a>
		 * </div>
		 */
		public static const JUSTIFY:String = "justify";
		/**
		 * <p> Constant; aligns text to the left within the text field. Use the syntax <code>TextFormatAlign.LEFT</code>. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="TextFormat.html#align" target="">flash.text.TextFormat.align</a>
		 * </div>
		 */
		public static const LEFT:String = "left";
		/**
		 * <p> Constant; aligns text to the right within the text field. Use the syntax <code>TextFormatAlign.RIGHT</code>. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="TextFormat.html#align" target="">flash.text.TextFormat.align</a>
		 * </div>
		 */
		public static const RIGHT:String = "right";
		/**
		 * <p> Constant; aligns text to the start edge of a line. Same as left for left-to-right languages and same as right for right-to-left languages. </p>
		 * <p>The <code>START</code> constant may only be used with the <code>StageText</code> class.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="StageText.html#textAlign" target="">flash.text.StageText.textAlign</a>
		 *  <br>
		 *  <a href="StageText.html#locale" target="">flash.text.StageText.locale</a>
		 * </div>
		 */
		public static const START:String = "start";
	}
}
