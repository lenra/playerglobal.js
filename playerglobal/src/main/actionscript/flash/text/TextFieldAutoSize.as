package flash.text {
	/**
	 *  The TextFieldAutoSize class is an enumeration of constant values used in setting the <code>autoSize</code> property of the TextField class. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="TextField.html#autoSize" target="">flash.text.TextField.autoSize</a>
	 * </div><br><hr>
	 */
	public class TextFieldAutoSize {
		/**
		 * <p> Specifies that the text is to be treated as center-justified text. Any resizing of a single line of a text field is equally distributed to both the right and left sides. </p>
		 */
		public static const CENTER:String = "center";
		/**
		 * <p> Specifies that the text is to be treated as left-justified text, meaning that the left side of the text field remains fixed and any resizing of a single line is on the right side. </p>
		 */
		public static const LEFT:String = "left";
		/**
		 * <p> Specifies that no resizing is to occur. </p>
		 */
		public static const NONE:String = "none";
		/**
		 * <p> Specifies that the text is to be treated as right-justified text, meaning that the right side of the text field remains fixed and any resizing of a single line is on the left side. </p>
		 */
		public static const RIGHT:String = "right";
	}
}
