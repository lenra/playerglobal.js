package flash.text {
	/**
	 *  The FontStyle class provides values for the TextRenderer class. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="TextRenderer.html" target="">flash.text.TextRenderer</a>
	 * </div><br><hr>
	 */
	public class FontStyle {
		/**
		 * <p> Defines the bold style of a font for the <code>fontStyle</code> parameter in the <code>setAdvancedAntiAliasingTable()</code> method. Use the syntax <code>FontStyle.BOLD</code>. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="TextRenderer.html#setAdvancedAntiAliasingTable()" target="">flash.text.TextRenderer.setAdvancedAntiAliasingTable()</a>
		 * </div>
		 */
		public static const BOLD:String = "bold";
		/**
		 * <p> Defines the combined bold and italic style of a font for the <code>fontStyle</code> parameter in the <code>setAdvancedAntiAliasingTable()</code> method. Use the syntax <code>FontStyle.BOLD_ITALIC</code>. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="TextRenderer.html#setAdvancedAntiAliasingTable()" target="">flash.text.TextRenderer.setAdvancedAntiAliasingTable()</a>
		 * </div>
		 */
		public static const BOLD_ITALIC:String = "boldItalic";
		/**
		 * <p> Defines the italic style of a font for the <code>fontStyle</code> parameter in the <code>setAdvancedAntiAliasingTable()</code> method. Use the syntax <code>FontStyle.ITALIC</code>. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="TextRenderer.html#setAdvancedAntiAliasingTable()" target="">flash.text.TextRenderer.setAdvancedAntiAliasingTable()</a>
		 * </div>
		 */
		public static const ITALIC:String = "italic";
		/**
		 * <p> Defines the plain style of a font for the <code>fontStyle</code> parameter in the <code>setAdvancedAntiAliasingTable()</code> method. Use the syntax <code>FontStyle.REGULAR</code>. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="TextRenderer.html#setAdvancedAntiAliasingTable()" target="">flash.text.TextRenderer.setAdvancedAntiAliasingTable()</a>
		 * </div>
		 */
		public static const REGULAR:String = "regular";
	}
}
