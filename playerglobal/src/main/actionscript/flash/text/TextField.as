package flash.text {
	import flash.display.DisplayObject;
	import flash.display.InteractiveObject;
	import flash.events.Event;
	import flash.events.TextEvent;
	import flash.geom.Rectangle;
	
	import fr.lenra.playerglobal.utils.ColorUtil;

	[Event(name="change", type="flash.events.Event")]
	[Event(name="link", type="flash.events.TextEvent")]
	[Event(name="scroll", type="flash.events.Event")]
	[Event(name="textInput", type="flash.events.TextEvent")]
	[Event(name="textInteractionModeChange", type="flash.events.Event")]
	/**
	 *  The TextField class is used to create display objects for text display and input. <span src="flashonly">You can give a text field an instance name in the Property inspector and use the methods and properties of the TextField class to manipulate it with ActionScript. TextField instance names are displayed in the Movie Explorer and in the Insert Target Path dialog box in the Actions panel.</span> <p>To create a text field dynamically, use the <code>TextField()</code> constructor.</p> <p>The methods of the TextField class let you set, select, and manipulate text in a dynamic or input text field that you create during authoring or at runtime. </p> <p>ActionScript provides several ways to format your text at runtime. The TextFormat class lets you set character and paragraph formatting for TextField objects. You can apply Cascading Style Sheets (CSS) styles to text fields by using the <code>TextField.styleSheet</code> property and the StyleSheet class. You can use CSS to style built-in HTML tags, define new formatting tags, or apply styles. You can assign HTML formatted text, which optionally uses CSS styles, directly to a text field. HTML text that you assign to a text field can contain embedded media (movie clips, SWF files, GIF files, PNG files, and JPEG files). The text wraps around the embedded media in the same way that a web browser wraps text around media embedded in an HTML document. </p> <p>Flash Player supports a subset of HTML tags that you can use to format text. See the list of supported HTML tags in the description of the <code>htmlText</code> property.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS8d7bb3e8da6fb92f-20050207122bd5f80cb-7ffe.html" target="_blank">Modifying the text field contents</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS8d7bb3e8da6fb92f-20050207122bd5f80cb-7ffd.html" target="_blank">Displaying HTML text</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS8d7bb3e8da6fb92f-20050207122bd5f80cb-7ffc.html" target="_blank">Using images in text fields</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS8d7bb3e8da6fb92f-20050207122bd5f80cb-7ffb.html" target="_blank">Scrolling text in a text field</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS8d7bb3e8da6fb92f-20050207122bd5f80cb-7ffa.html" target="_blank">Selecting and manipulating text</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS8d7bb3e8da6fb92f-20050207122bd5f80cb-7ff7.html" target="_blank">Capturing text input</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS8d7bb3e8da6fb92f-20050207122bd5f80cb-7ff6.html" target="_blank">Restricting text input</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS8d7bb3e8da6fb92f-20050207122bd5f80cb-7ff5.html" target="_blank">Formatting text</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS8d7bb3e8da6fb92f-20050207122bd5f80cb-7fed.html" target="_blank">Working with static text</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS8d7bb3e8da6fb92f-20050207122bd5f80cb-7fe6.html" target="_blank">TextField Example: Newspaper-style text formatting</a>
	 * </div><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/mobileapps/WS19f279b149e7481c-51e256961331cc2eae5-8000.html" target="_blank">Use native features with a soft keyboard</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e58.html" target="_blank">Display programming</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e3e.html" target="_blank">Basics of display programming</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e3c.html" target="_blank">Core display classes</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7dfa.html" target="_blank">Choosing a DisplayObject subclass</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS8d7bb3e8da6fb92f-6724c60a122bd5c5b9e-8000.html" target="_blank">Basics of Working with text</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WSb2ba3b1aad8a27b07258e35912218ac0e60-8000.html" target="_blank">Using the TextField class</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS8d7bb3e8da6fb92f-20050207122bd5f80cb-8000.html" target="_blank">Displaying text</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS8d7bb3e8da6fb92f-20050207122bd5f80cb-7ff0.html" target="_blank">Advanced text rendering</a>
	 *  <br>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="TextFormat.html" target="">flash.text.TextFormat</a>
	 *  <br>
	 *  <a href="StyleSheet.html" target="">flash.text.StyleSheet</a>
	 *  <br>
	 *  <a href="TextField.html#htmlText" target="">htmlText</a>
	 * </div><br><hr>
	 */
	public class TextField extends InteractiveObject {
		private var _alwaysShowSelection:Boolean;
		private var _antiAliasType:String;
		private var _autoSize:String;
		private var _background:Boolean;
		private var _backgroundColor:uint;
		private var _border:Boolean;
		private var _borderColor:uint;
		private var _condenseWhite:Boolean;
		private var _defaultTextFormat:TextFormat;
		private var _displayAsPassword:Boolean;
		private var _embedFonts:Boolean;
		private var _gridFitType:String;
		private var _htmlText:String;
		private var _maxChars:int;
		private var _mouseWheelEnabled:Boolean;
		private var _multiline:Boolean;
		private var _restrict:String;
		private var _scrollH:int;
		private var _scrollV:int;
		private var _selectable:Boolean;
		private var _sharpness:Number;
		private var _styleSheet:StyleSheet;
		private var _text:String;
		private var _textColor:uint;
		private var _thickness:Number;
		private var _type:String;
		private var _useRichTextClipboard:Boolean;
		private var _wordWrap:Boolean;

		private var _bottomScrollV:int;
		private var _caretIndex:int;
		private var _length:int;
		private var _maxScrollH:int;
		private var _maxScrollV:int;
		private var _numLines:int;
		private var _selectionBeginIndex:int;
		private var _selectionEndIndex:int;
		private var _textHeight:Number;
		private var _textInteractionMode:String;
		private var _textWidth:Number;

		/**
		 * <p> Creates a new TextField instance. After you create the TextField instance, call the <code>addChild()</code> or <code>addChildAt()</code> method of the parent DisplayObjectContainer object to add the TextField instance to the display list. </p>
		 * <p>The default size for a text field is 100 x 100 pixels.</p>
		 * <span id="pageFilter"><br><span class="label"> Example &nbsp;( <span class="usage"><a href="http://www.adobe.com/go/learn_as3_usingexamples_en"> How to use this example </a></span>) </span><br><br>
		 *  <div class="detailBody">
		 *    The following example shows how you can dynamically create an input TextField object in ActionScript 3.0 by setting the text field object's type property to the TextFieldType.INPUT constant. Example provided by 
		 *   <a href="http://actionscriptexamples.com/2008/12/02/dynamically-creating-an-input-text-field-in-actionscript-30/" target="_mmexternal">ActionScriptExamples.com</a>. 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>
		 * var theTextField:TextField = new TextField();
		 * theTextField.type = TextFieldType.INPUT;
		 * theTextField.border = true;
		 * theTextField.x = 10;
		 * theTextField.y = 10;
		 * theTextField.multiline = true;
		 * theTextField.wordWrap = true;
		 * addChild(theTextField);
		 * </pre>
		 *   </div>
		 *  </div></span>
		 */
		public function TextField() {
			
		}

		override protected function createSVGElement():SVGElement {
			return document.createElementNS(DisplayObject.SVG_NAMESPACE, "text") as SVGTextElement;
		}

		/**
		 * <p> When set to <code>true</code> and the text field is not in focus, Flash Player highlights the selection in the text field in gray. When set to <code>false</code> and the text field is not in focus, Flash Player does not highlight the selection in the text field. </p>
		 * <p> The default value is <code>false.</code></p>
		 * 
		 * @return 
		 */
		public function get alwaysShowSelection():Boolean {
			return _alwaysShowSelection;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set alwaysShowSelection(value:Boolean):void {
			_alwaysShowSelection = value;
		}

		/**
		 * <p> The type of anti-aliasing used for this text field. Use <code>flash.text.AntiAliasType</code> constants for this property. You can control this setting only if the font is embedded (with the <code>embedFonts</code> property set to <code>true</code>). The default setting is <code>flash.text.AntiAliasType.NORMAL</code>. </p>
		 * <p>To set values for this property, use the following string values:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>String value</th>
		 *    <th>Description</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>flash.text.AntiAliasType.NORMAL</code></td>
		 *    <td>Applies the regular text anti-aliasing. This value matches the type of anti-aliasing that Flash Player 7 and earlier versions used.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>flash.text.AntiAliasType.ADVANCED</code></td>
		 *    <td>Applies advanced anti-aliasing, which makes text more legible. (This feature became available in Flash Player 8.) Advanced anti-aliasing allows for high-quality rendering of font faces at small sizes. It is best used with applications with a lot of small text. Advanced anti-aliasing is not recommended for fonts that are larger than 48 points.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * 
		 * @return 
		 */
		public function get antiAliasType():String {
			return _antiAliasType;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set antiAliasType(value:String):void {
			_antiAliasType = value;
		}

		/**
		 * <p> Controls automatic sizing and alignment of text fields. Acceptable values for the <code>TextFieldAutoSize</code> constants: <code>TextFieldAutoSize.NONE</code> (the default), <code>TextFieldAutoSize.LEFT</code>, <code>TextFieldAutoSize.RIGHT</code>, and <code>TextFieldAutoSize.CENTER</code>. </p>
		 * <p>If <code>autoSize</code> is set to <code>TextFieldAutoSize.NONE</code> (the default) no resizing occurs.</p>
		 * <p>If <code>autoSize</code> is set to <code>TextFieldAutoSize.LEFT</code>, the text is treated as left-justified text, meaning that the left margin of the text field remains fixed and any resizing of a single line of the text field is on the right margin. If the text includes a line break (for example, <code>"\n"</code> or <code>"\r"</code>), the bottom is also resized to fit the next line of text. If <code>wordWrap</code> is also set to <code>true</code>, only the bottom of the text field is resized and the right side remains fixed.</p>
		 * <p>If <code>autoSize</code> is set to <code>TextFieldAutoSize.RIGHT</code>, the text is treated as right-justified text, meaning that the right margin of the text field remains fixed and any resizing of a single line of the text field is on the left margin. If the text includes a line break (for example, <code>"\n" or "\r")</code>, the bottom is also resized to fit the next line of text. If <code>wordWrap</code> is also set to <code>true</code>, only the bottom of the text field is resized and the left side remains fixed.</p>
		 * <p>If <code>autoSize</code> is set to <code>TextFieldAutoSize.CENTER</code>, the text is treated as center-justified text, meaning that any resizing of a single line of the text field is equally distributed to both the right and left margins. If the text includes a line break (for example, <code>"\n"</code> or <code>"\r"</code>), the bottom is also resized to fit the next line of text. If <code>wordWrap</code> is also set to <code>true</code>, only the bottom of the text field is resized and the left and right sides remain fixed.</p>
		 * 
		 * @return 
		 */
		public function get autoSize():String {
			return _autoSize;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set autoSize(value:String):void {
			_autoSize = value;
		}

		/**
		 * <p> Specifies whether the text field has a background fill. If <code>true</code>, the text field has a background fill. If <code>false</code>, the text field has no background fill. Use the <code>backgroundColor</code> property to set the background color of a text field. </p>
		 * <p> The default value is <code>false.</code></p>
		 * 
		 * @return 
		 */
		public function get background():Boolean {
			return _background;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set background(value:Boolean):void {
			_background = value;
		}

		/**
		 * <p> The color of the text field background. The default value is <code>0xFFFFFF</code> (white). This property can be retrieved or set, even if there currently is no background, but the color is visible only if the text field has the <code>background</code> property set to <code>true</code>. </p>
		 * 
		 * @return 
		 */
		public function get backgroundColor():uint {
			return _backgroundColor;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set backgroundColor(value:uint):void {
			_backgroundColor = value;
		}

		/**
		 * <p> Specifies whether the text field has a border. If <code>true</code>, the text field has a border. If <code>false</code>, the text field has no border. Use the <code>borderColor</code> property to set the border color. </p>
		 * <p> The default value is <code>false.</code></p>
		 * 
		 * @return 
		 */
		public function get border():Boolean {
			return _border;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set border(value:Boolean):void {
			_border = value;
		}

		/**
		 * <p> The color of the text field border. The default value is <code>0x000000</code> (black). This property can be retrieved or set, even if there currently is no border, but the color is visible only if the text field has the <code>border</code> property set to <code>true</code>. </p>
		 * 
		 * @return 
		 */
		public function get borderColor():uint {
			return _borderColor;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set borderColor(value:uint):void {
			_borderColor = value;
		}

		/**
		 * <p> An integer (1-based index) that indicates the bottommost line that is currently visible in the specified text field. Think of the text field as a window onto a block of text. The <code>scrollV</code> property is the 1-based index of the topmost visible line in the window. </p>
		 * <p>All the text between the lines indicated by <code>scrollV</code> and <code>bottomScrollV</code> is currently visible in the text field.</p>
		 * 
		 * @return 
		 */
		public function get bottomScrollV():int {
			return _bottomScrollV;
		}

		/**
		 * <p> The index of the insertion point (caret) position. If no insertion point is displayed, the value is the position the insertion point would be if you restored focus to the field (typically where the insertion point last was, or 0 if the field has not had focus). </p>
		 * <p>Selection span indexes are zero-based (for example, the first position is 0, the second position is 1, and so on).</p>
		 * 
		 * @return 
		 */
		public function get caretIndex():int {
			return _caretIndex;
		}

		/**
		 * <p> A Boolean value that specifies whether extra white space (spaces, line breaks, and so on) in a text field with HTML text is removed. The default value is <code>false</code>. The <code>condenseWhite</code> property only affects text set with the <code>htmlText</code> property, not the <code>text</code> property. If you set text with the <code>text</code> property, <code>condenseWhite</code> is ignored. </p>
		 * <p>If <code>condenseWhite</code> is set to <code>true</code>, use standard HTML commands such as <code>&lt;BR&gt;</code> and <code>&lt;P&gt;</code> to place line breaks in the text field.</p>
		 * <p>Set the <code>condenseWhite</code> property before setting the <code>htmlText</code> property.</p>
		 * 
		 * @return 
		 */
		public function get condenseWhite():Boolean {
			return _condenseWhite;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set condenseWhite(value:Boolean):void {
			_condenseWhite = value;
		}

		/**
		 * <p> Specifies the format applied to newly inserted text, such as text entered by a user or text inserted with the <code>replaceSelectedText()</code> method. </p>
		 * <p><b>Note:</b> When selecting characters to be replaced with <code>setSelection()</code> and <code>replaceSelectedText()</code>, the <code>defaultTextFormat</code> will be applied only if the text has been selected up to and including the last character. Here is an example:</p>
		 * <pre>
		 *      var my_txt:TextField new TextField();
		 *      my_txt.text = "Flash Macintosh version";
		 *      var my_fmt:TextFormat = new TextFormat();
		 *      my_fmt.color = 0xFF0000;
		 *      my_txt.defaultTextFormat = my_fmt;
		 *      my_txt.setSelection(6,15); // partial text selected - defaultTextFormat not applied
		 *      my_txt.setSelection(6,23); // text selected to end - defaultTextFormat applied
		 *      my_txt.replaceSelectedText("Windows version");
		 *      </pre>
		 * <p>When you access the <code>defaultTextFormat</code> property, the returned TextFormat object has all of its properties defined. No property is <code>null</code>.</p>
		 * <p><b>Note:</b> You can't set this property if a style sheet is applied to the text field.</p>
		 * 
		 * @return 
		 */
		public function get defaultTextFormat():TextFormat {
			return _defaultTextFormat;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set defaultTextFormat(value:TextFormat):void {
			_defaultTextFormat = value;
			if (value.align)
				svgElement.style.textAlign = value.align;
			if (value.bold)
				svgElement.style.fontWeight = "bold";
			if (value.color!=null)
				svgElement.setAttribute("fill", ColorUtil.toColorString(value.color as int));
			svgElement.style.fontSize = value.size+"px";
		}

		/**
		 * <p> Specifies whether the text field is a password text field. If the value of this property is <code>true</code>, the text field is treated as a password text field and hides the input characters using asterisks instead of the actual characters. If <code>false</code>, the text field is not treated as a password text field. When password mode is enabled, the Cut and Copy commands and their corresponding keyboard shortcuts will not function. This security mechanism prevents an unscrupulous user from using the shortcuts to discover a password on an unattended computer. </p>
		 * <p> The default value is <code>false.</code></p>
		 * 
		 * @return 
		 */
		public function get displayAsPassword():Boolean {
			return _displayAsPassword;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set displayAsPassword(value:Boolean):void {
			_displayAsPassword = value;
		}

		/**
		 * <p> Specifies whether to render by using embedded font outlines. If <code>false</code>, Flash Player renders the text field by using device fonts. </p>
		 * <p>If you set the <code>embedFonts</code> property to <code>true</code> for a text field, you must specify a font for that text by using the <code>font</code> property of a TextFormat object applied to the text field. If the specified font is not embedded in the SWF file, the text is not displayed.</p>
		 * <p> The default value is <code>false.</code></p>
		 * 
		 * @return 
		 */
		public function get embedFonts():Boolean {
			return _embedFonts;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set embedFonts(value:Boolean):void {
			_embedFonts = value;
		}

		/**
		 * <p> The type of grid fitting used for this text field. This property applies only if the <code>flash.text.AntiAliasType</code> property of the text field is set to <code>flash.text.AntiAliasType.ADVANCED</code>. </p>
		 * <p>The type of grid fitting used determines whether Flash Player forces strong horizontal and vertical lines to fit to a pixel or subpixel grid, or not at all.</p>
		 * <p>For the <code>flash.text.GridFitType</code> property, you can use the following string values:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>String value</th>
		 *    <th>Description</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>flash.text.GridFitType.NONE</code></td>
		 *    <td>Specifies no grid fitting. Horizontal and vertical lines in the glyphs are not forced to the pixel grid. This setting is recommended for animation or for large font sizes.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>flash.text.GridFitType.PIXEL</code></td>
		 *    <td>Specifies that strong horizontal and vertical lines are fit to the pixel grid. This setting works only for left-aligned text fields. To use this setting, the <code>flash.dispaly.AntiAliasType</code> property of the text field must be set to <code>flash.text.AntiAliasType.ADVANCED</code>. This setting generally provides the best legibility for left-aligned text.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>flash.text.GridFitType.SUBPIXEL</code></td>
		 *    <td>Specifies that strong horizontal and vertical lines are fit to the subpixel grid on an LCD monitor. To use this setting, the <code>flash.text.AntiAliasType</code> property of the text field must be set to <code>flash.text.AntiAliasType.ADVANCED</code>. The <code>flash.text.GridFitType.SUBPIXEL</code> setting is often good for right-aligned or centered dynamic text, and it is sometimes a useful trade-off for animation versus text quality.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p> The default value is <code>pixel.</code></p>
		 * 
		 * @return 
		 */
		public function get gridFitType():String {
			return _gridFitType;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set gridFitType(value:String):void {
			_gridFitType = value;
		}

		/**
		 * <p> Contains the HTML representation of the text field contents. </p>
		 * <p>Flash Player supports the following HTML tags:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th> Tag </th>
		 *    <th> Description </th>
		 *   </tr>
		 *   <tr>
		 *    <td> Anchor tag </td>
		 *    <td> The <code>&lt;a&gt;</code> tag creates a hypertext link and supports the following attributes: 
		 *     <ul>
		 *      <li> <code>target</code>: Specifies the name of the target window where you load the page. Options include <code>_self</code>, <code>_blank</code>, <code>_parent</code>, and <code>_top</code>. The <code>_self</code> option specifies the current frame in the current window, <code>_blank</code> specifies a new window, <code>_parent</code> specifies the parent of the current frame, and <code>_top</code> specifies the top-level frame in the current window. </li>
		 *      <li> <code>href</code>: Specifies a URL or an ActionScript <code>link</code> event.The URL can be either absolute or relative to the location of the SWF file that is loading the page. An example of an absolute reference to a URL is <code>http://www.adobe.com</code>; an example of a relative reference is <code>/index.html</code>. Absolute URLs must be prefixed with http://; otherwise, Flash Player or AIR treats them as relative URLs. You can use the <code>link</code> event to cause the link to execute an ActionScript function in a SWF file instead of opening a URL. To specify a <code>link</code> event, use the event scheme instead of the http scheme in your <code>href</code> attribute. An example is <code>href="event:myText"</code> instead of <code>href="http://myURL"</code>; when the user clicks a hypertext link that contains the event scheme, the text field dispatches a <code>link</code> TextEvent with its <code>text</code> property set to "<code>myText</code>". You can then create an ActionScript function that executes whenever the link TextEvent is dispatched. You can also define <code>a:link</code>, <code>a:hover</code>, and <code>a:active</code> styles for anchor tags by using style sheets. </li>
		 *     </ul> </td>
		 *   </tr>
		 *   <tr>
		 *    <td> Bold tag </td>
		 *    <td> The <code>&lt;b&gt;</code> tag renders text as bold. A bold typeface must be available for the font used. </td>
		 *   </tr>
		 *   <tr>
		 *    <td> Break tag </td>
		 *    <td> The <code>&lt;br&gt;</code> tag creates a line break in the text field. Set the text field to be a multiline text field to use this tag. </td>
		 *   </tr>
		 *   <tr>
		 *    <td> Font tag </td>
		 *    <td> The <code>&lt;font&gt;</code> tag specifies a font or list of fonts to display the text.The font tag supports the following attributes: 
		 *     <ul>
		 *      <li> <code>color</code>: Only hexadecimal color (<code>#FFFFFF</code>) values are supported. </li>
		 *      <li> <code>face</code>: Specifies the name of the font to use. As shown in the following example, you can specify a list of comma-delimited font names, in which case Flash Player selects the first available font. If the specified font is not installed on the local computer system or isn't embedded in the SWF file, Flash Player selects a substitute font. </li>
		 *      <li> <code>size</code>: Specifies the size of the font. You can use absolute pixel sizes, such as 16 or 18, or relative point sizes, such as +2 or -4. </li>
		 *     </ul> </td>
		 *   </tr>
		 *   <tr>
		 *    <td> Image tag </td>
		 *    <td> The <code>&lt;img&gt;</code> tag lets you embed external image files (JPEG, GIF, PNG), SWF files, and movie clips inside text fields. Text automatically flows around images you embed in text fields. You must set the text field to be multiline to wrap text around an image. <p>The <code>&lt;img&gt;</code> tag supports the following attributes: </p> 
		 *     <ul>
		 *      <li> <code>src</code>: Specifies the URL to an image or SWF file, or the linkage identifier for a movie clip symbol in the library. This attribute is required; all other attributes are optional. External files (JPEG, GIF, PNG, and SWF files) do not show until they are downloaded completely. </li>
		 *      <li> <code>width</code>: The width of the image, SWF file, or movie clip being inserted, in pixels. </li>
		 *      <li> <code>height</code>: The height of the image, SWF file, or movie clip being inserted, in pixels. </li>
		 *      <li> <code>align</code>: Specifies the horizontal alignment of the embedded image within the text field. Valid values are <code>left</code> and <code>right</code>. The default value is <code>left</code>. </li>
		 *      <li> <code>hspace</code>: Specifies the amount of horizontal space that surrounds the image where no text appears. The default value is 8. </li>
		 *      <li> <code>vspace</code>: Specifies the amount of vertical space that surrounds the image where no text appears. The default value is 8. </li>
		 *      <li> <code>id</code>: Specifies the name for the movie clip instance (created by Flash Player) that contains the embedded image file, SWF file, or movie clip. This approach is used to control the embedded content with ActionScript. </li>
		 *      <li> <code>checkPolicyFile</code>: Specifies that Flash Player checks for a URL policy file on the server associated with the image domain. If a policy file exists, SWF files in the domains listed in the file can access the data of the loaded image, for example, by calling the <code>BitmapData.draw()</code> method with this image as the <code>source</code> parameter. For more information related to security, see the Flash Player Developer Center Topic: <a href="http://www.adobe.com/go/devnet_security_en" target="external">Security</a>. </li>
		 *     </ul> <p>Flash displays media embedded in a text field at full size. To specify the dimensions of the media you are embedding, use the <code>&lt;img&gt;</code> tag <code>height</code> and <code>width</code> attributes. </p> <p>In general, an image embedded in a text field appears on the line following the <code>&lt;img&gt;</code> tag. However, when the <code>&lt;img&gt;</code> tag is the first character in the text field, the image appears on the first line of the text field. </p> <p>For AIR content in the application security sandbox, AIR ignores <code>img</code> tags in HTML content in ActionScript TextField objects. This is to prevent possible phishing attacks,</p> </td>
		 *   </tr>
		 *   <tr>
		 *    <td> Italic tag </td>
		 *    <td> The <code>&lt;i&gt;</code> tag displays the tagged text in italics. An italic typeface must be available for the font used. </td>
		 *   </tr>
		 *   <tr>
		 *    <td> List item tag </td>
		 *    <td> The <code>&lt;li&gt;</code> tag places a bullet in front of the text that it encloses. <b>Note:</b> Because Flash Player and AIR do not recognize ordered and unordered list tags (<code>&lt;ol&gt;</code> and <code>&lt;ul&gt;</code>, they do not modify how your list is rendered. All lists are unordered and all list items use bullets. </td>
		 *   </tr>
		 *   <tr>
		 *    <td> Paragraph tag </td>
		 *    <td> The <code>&lt;p&gt;</code> tag creates a new paragraph. The text field must be set to be a multiline text field to use this tag. The <code>&lt;p&gt;</code> tag supports the following attributes: 
		 *     <ul>
		 *      <li> align: Specifies alignment of text within the paragraph; valid values are <code>left</code>, <code>right</code>, <code>justify</code>, and <code>center</code>. </li>
		 *      <li> class: Specifies a CSS style class defined by a flash.text.StyleSheet object. </li>
		 *     </ul> </td>
		 *   </tr>
		 *   <tr>
		 *    <td> Span tag </td>
		 *    <td> The <code>&lt;span&gt;</code> tag is available only for use with CSS text styles. It supports the following attribute: 
		 *     <ul>
		 *      <li> class: Specifies a CSS style class defined by a flash.text.StyleSheet object. </li>
		 *     </ul> </td>
		 *   </tr>
		 *   <tr>
		 *    <td> Text format tag </td>
		 *    <td> <p>The <code>&lt;textformat&gt;</code> tag lets you use a subset of paragraph formatting properties of the TextFormat class within text fields, including line leading, indentation, margins, and tab stops. You can combine <code>&lt;textformat&gt;</code> tags with the built-in HTML tags. </p> <p>The <code>&lt;textformat&gt;</code> tag has the following attributes: </p> 
		 *     <ul>
		 *      <li> <code>blockindent</code>: Specifies the block indentation in points; corresponds to <code>TextFormat.blockIndent</code>. </li>
		 *      <li> <code>indent</code>: Specifies the indentation from the left margin to the first character in the paragraph; corresponds to <code>TextFormat.indent</code>. Both positive and negative numbers are acceptable. </li>
		 *      <li> <code>leading</code>: Specifies the amount of leading (vertical space) between lines; corresponds to <code>TextFormat.leading</code>. Both positive and negative numbers are acceptable. </li>
		 *      <li> <code>leftmargin</code>: Specifies the left margin of the paragraph, in points; corresponds to <code>TextFormat.leftMargin</code>. </li>
		 *      <li> <code>rightmargin</code>: Specifies the right margin of the paragraph, in points; corresponds to <code>TextFormat.rightMargin</code>. </li>
		 *      <li> <code>tabstops</code>: Specifies custom tab stops as an array of non-negative integers; corresponds to <code>TextFormat.tabStops</code>. </li>
		 *     </ul> </td>
		 *   </tr>
		 *   <tr>
		 *    <td> Underline tag </td>
		 *    <td> The <code>&lt;u&gt;</code> tag underlines the tagged text. </td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p>Flash Player and AIR support the following HTML entities:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th> Entity </th>
		 *    <th> Description </th>
		 *   </tr>
		 *   <tr>
		 *    <td> &amp;lt; </td>
		 *    <td> &lt; (less than) </td>
		 *   </tr>
		 *   <tr>
		 *    <td> &amp;gt; </td>
		 *    <td> &gt; (greater than) </td>
		 *   </tr>
		 *   <tr>
		 *    <td> &amp;amp; </td>
		 *    <td> &amp; (ampersand) </td>
		 *   </tr>
		 *   <tr>
		 *    <td> &amp;quot; </td>
		 *    <td> " (double quotes) </td>
		 *   </tr>
		 *   <tr>
		 *    <td> &amp;apos; </td>
		 *    <td> ' (apostrophe, single quote) </td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p>Flash Player and AIR also support explicit character codes, such as &amp;#38; (ASCII ampersand) and &amp;#x20AC; (Unicode € symbol). </p>
		 * 
		 * @return 
		 */
		public function get htmlText():String {
			return _htmlText;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set htmlText(value:String):void {
			_htmlText = value;
		}

		/**
		 * <p> The number of characters in a text field. A character such as tab (<code>\t</code>) counts as one character. </p>
		 * 
		 * @return 
		 */
		public function get length():int {
			return _length;
		}

		/**
		 * <p> The maximum number of characters that the text field can contain, as entered by a user. A script can insert more text than <code>maxChars</code> allows; the <code>maxChars</code> property indicates only how much text a user can enter. If the value of this property is <code>0</code>, a user can enter an unlimited amount of text. </p>
		 * <p> The default value is <code>0.</code></p>
		 * 
		 * @return 
		 */
		public function get maxChars():int {
			return _maxChars;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set maxChars(value:int):void {
			_maxChars = value;
		}

		/**
		 * <p> The maximum value of <code>scrollH</code>. </p>
		 * 
		 * @return 
		 */
		public function get maxScrollH():int {
			return _maxScrollH;
		}

		/**
		 * <p> The maximum value of <code>scrollV</code>. </p>
		 * 
		 * @return 
		 */
		public function get maxScrollV():int {
			return _maxScrollV;
		}

		/**
		 * <p> A Boolean value that indicates whether Flash Player automatically scrolls multiline text fields when the user clicks a text field and rolls the mouse wheel. By default, this value is <code>true</code>. This property is useful if you want to prevent mouse wheel scrolling of text fields, or implement your own text field scrolling. </p>
		 * 
		 * @return 
		 */
		public function get mouseWheelEnabled():Boolean {
			return _mouseWheelEnabled;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set mouseWheelEnabled(value:Boolean):void {
			_mouseWheelEnabled = value;
		}

		/**
		 * <p> Indicates whether field is a multiline text field. If the value is <code>true</code>, the text field is multiline; if the value is <code>false</code>, the text field is a single-line text field. In a field of type <code>TextFieldType.INPUT</code>, the <code>multiline</code> value determines whether the <code>Enter</code> key creates a new line (a value of <code>false</code>, and the <code>Enter</code> key is ignored). If you paste text into a <code>TextField</code> with a <code>multiline</code> value of <code>false</code>, newlines are stripped out of the text. </p>
		 * <p> The default value is <code>false.</code></p>
		 * 
		 * @return 
		 */
		public function get multiline():Boolean {
			return _multiline;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set multiline(value:Boolean):void {
			_multiline = value;
		}

		/**
		 * <p> Defines the number of text lines in a multiline text field. If <code>wordWrap</code> property is set to <code>true</code>, the number of lines increases when text wraps. </p>
		 * 
		 * @return 
		 */
		public function get numLines():int {
			return _numLines;
		}

		/**
		 * <p> Indicates the set of characters that a user can enter into the text field. If the value of the <code>restrict</code> property is <code>null</code>, you can enter any character. If the value of the <code>restrict</code> property is an empty string, you cannot enter any character. If the value of the <code>restrict</code> property is a string of characters, you can enter only characters in the string into the text field. The string is scanned from left to right. You can specify a range by using the hyphen (-) character. Only user interaction is restricted; a script can put any text into the text field. <span src="flashonly">This property does not synchronize with the Embed font options in the Property inspector.</span> </p>
		 * <p>If the string begins with a caret (^) character, all characters are initially accepted and succeeding characters in the string are excluded from the set of accepted characters. If the string does not begin with a caret (^) character, no characters are initially accepted and succeeding characters in the string are included in the set of accepted characters.</p>
		 * <p>The following example allows only uppercase characters, spaces, and numbers to be entered into a text field:</p>
		 * <pre>
		 *      my_txt.restrict = "A-Z 0-9";
		 *      </pre>
		 * <p>The following example includes all characters, but excludes lowercase letters:</p>
		 * <pre>
		 *      my_txt.restrict = "^a-z";
		 *      </pre>
		 * <p>You can use a backslash to enter a ^ or - verbatim. The accepted backslash sequences are \-, \^ or \\. The backslash must be an actual character in the string, so when specified in ActionScript, a double backslash must be used. For example, the following code includes only the dash (-) and caret (^):</p>
		 * <pre>
		 *      my_txt.restrict = "\\-\\^";
		 *      </pre>
		 * <p>The ^ can be used anywhere in the string to toggle between including characters and excluding characters. The following code includes only uppercase letters, but excludes the uppercase letter Q:</p>
		 * <pre>
		 *      my_txt.restrict = "A-Z^Q";
		 *      </pre>
		 * <p>You can use the <code>\u</code> escape sequence to construct <code>restrict</code> strings. The following code includes only the characters from ASCII 32 (space) to ASCII 126 (tilde).</p>
		 * <pre>
		 *      my_txt.restrict = "\u0020-\u007E";
		 *      </pre>
		 * <p> The default value is <code>null.</code></p>
		 * 
		 * @return 
		 */
		public function get restrict():String {
			return _restrict;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set restrict(value:String):void {
			_restrict = value;
		}

		/**
		 * <p> The current horizontal scrolling position. If the <code>scrollH</code> property is 0, the text is not horizontally scrolled. This property value is an integer that represents the horizontal position in pixels. </p>
		 * <p>The units of horizontal scrolling are pixels, whereas the units of vertical scrolling are lines. Horizontal scrolling is measured in pixels because most fonts you typically use are proportionally spaced; that is, the characters can have different widths. Flash Player performs vertical scrolling by line because users usually want to see a complete line of text rather than a partial line. Even if a line uses multiple fonts, the height of the line adjusts to fit the largest font in use.</p>
		 * <p><b>Note: </b>The <code>scrollH</code> property is zero-based, not 1-based like the <code>scrollV</code> vertical scrolling property.</p>
		 * 
		 * @return 
		 */
		public function get scrollH():int {
			return _scrollH;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set scrollH(value:int):void {
			_scrollH = value;
		}

		/**
		 * <p> The vertical position of text in a text field. The <code>scrollV</code> property is useful for directing users to a specific paragraph in a long passage, or creating scrolling text fields. </p>
		 * <p>The units of vertical scrolling are lines, whereas the units of horizontal scrolling are pixels. If the first line displayed is the first line in the text field, scrollV is set to 1 (not 0). Horizontal scrolling is measured in pixels because most fonts are proportionally spaced; that is, the characters can have different widths. Flash performs vertical scrolling by line because users usually want to see a complete line of text rather than a partial line. Even if there are multiple fonts on a line, the height of the line adjusts to fit the largest font in use.</p>
		 * 
		 * @return 
		 */
		public function get scrollV():int {
			return _scrollV;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set scrollV(value:int):void {
			_scrollV = value;
		}

		/**
		 * <p> A Boolean value that indicates whether the text field is selectable. The value <code>true</code> indicates that the text is selectable. The <code>selectable</code> property controls whether a text field is selectable, not whether a text field is editable. A dynamic text field can be selectable even if it is not editable. If a dynamic text field is not selectable, the user cannot select its text. </p>
		 * <p>If <code>selectable</code> is set to <code>false</code>, the text in the text field does not respond to selection commands from the mouse or keyboard, and the text cannot be copied with the Copy command. If <code>selectable</code> is set to <code>true</code>, the text in the text field can be selected with the mouse or keyboard, and the text can be copied with the Copy command. You can select text this way even if the text field is a dynamic text field instead of an input text field. </p>
		 * <p> The default value is <code>true.</code></p>
		 * 
		 * @return 
		 */
		public function get selectable():Boolean {
			return _selectable;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set selectable(value:Boolean):void {
			_selectable = value;
		}

		/**
		 * <p> The zero-based character index value of the first character in the current selection. For example, the first character is 0, the second character is 1, and so on. If no text is selected, this property is the value of <code>caretIndex</code>. </p>
		 * 
		 * @return 
		 */
		public function get selectionBeginIndex():int {
			return _selectionBeginIndex;
		}

		/**
		 * <p> The zero-based character index value of the last character in the current selection. For example, the first character is 0, the second character is 1, and so on. If no text is selected, this property is the value of <code>caretIndex</code>. </p>
		 * 
		 * @return 
		 */
		public function get selectionEndIndex():int {
			return _selectionEndIndex;
		}

		/**
		 * <p> The sharpness of the glyph edges in this text field. This property applies only if the <code>flash.text.AntiAliasType</code> property of the text field is set to <code>flash.text.AntiAliasType.ADVANCED</code>. The range for <code>sharpness</code> is a number from -400 to 400. If you attempt to set <code>sharpness</code> to a value outside that range, Flash sets the property to the nearest value in the range (either -400 or 400). </p>
		 * <p> The default value is <code>0.</code></p>
		 * 
		 * @return 
		 */
		public function get sharpness():Number {
			return _sharpness;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set sharpness(value:Number):void {
			_sharpness = value;
		}

		/**
		 * <p> Attaches a style sheet to the text field. For information on creating style sheets, see the StyleSheet class and the <i>ActionScript 3.0 Developer's Guide</i>. </p>
		 * <p>You can change the style sheet associated with a text field at any time. If you change the style sheet in use, the text field is redrawn with the new style sheet. You can set the style sheet to <code>null</code> or <code>undefined</code> to remove the style sheet. If the style sheet in use is removed, the text field is redrawn without a style sheet. </p>
		 * <p><b>Note:</b> If the style sheet is removed, the contents of both <code>TextField.text</code> and <code> TextField.htmlText</code> change to incorporate the formatting previously applied by the style sheet. To preserve the original <code>TextField.htmlText</code> contents without the formatting, save the value in a variable before removing the style sheet.</p>
		 * 
		 * @return 
		 */
		public function get styleSheet():StyleSheet {
			return _styleSheet;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set styleSheet(value:StyleSheet):void {
			_styleSheet = value;
		}

		/**
		 * <p> A string that is the current text in the text field. Lines are separated by the carriage return character (<code>'\r'</code>, ASCII 13). This property contains unformatted text in the text field, without HTML tags. </p>
		 * <p>To get the text in HTML form, use the <code>htmlText</code> property.</p>
		 * <p><b>Note:</b> If a style sheet is applied to the text field the content of the <code>text</code> property will be interpreted as HTML.</p>
		 * 
		 * @return 
		 */
		public function get text():String {
			return _text;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set text(value:String):void {
			_text = value;
			svgElement.innerHTML = _text;
		}

		/**
		 * <p> The color of the text in a text field, in hexadecimal format. The hexadecimal color system uses six digits to represent color values. Each digit has 16 possible values or characters. The characters range from 0-9 and then A-F. For example, black is <code>0x000000</code>; white is <code>0xFFFFFF</code>. </p>
		 * <p> The default value is <code>0 (0x000000).</code></p>
		 * 
		 * @return 
		 */
		public function get textColor():uint {
			return _textColor;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set textColor(value:uint):void {
			_textColor = value;
		}

		/**
		 * <p> The height of the text in pixels. </p>
		 * 
		 * @return 
		 */
		public function get textHeight():Number {
			return _textHeight;
		}

		/**
		 * <p> The interaction mode property, Default value is TextInteractionMode.NORMAL. On mobile platforms, the normal mode implies that the text can be scrolled but not selected. One can switch to the selectable mode through the in-built context menu on the text field. On Desktop, the normal mode implies that the text is in scrollable as well as selection mode. </p>
		 * 
		 * @return 
		 */
		public function get textInteractionMode():String {
			return _textInteractionMode;
		}

		/**
		 * <p> The width of the text in pixels. </p>
		 * 
		 * @return 
		 */
		public function get textWidth():Number {
			return _textWidth;
		}

		/**
		 * <p> The thickness of the glyph edges in this text field. This property applies only when <code>flash.text.AntiAliasType</code> is set to <code>flash.text.AntiAliasType.ADVANCED</code>. </p>
		 * <p>The range for <code>thickness</code> is a number from -200 to 200. If you attempt to set <code>thickness</code> to a value outside that range, the property is set to the nearest value in the range (either -200 or 200).</p>
		 * <p> The default value is <code>0.</code></p>
		 * 
		 * @return 
		 */
		public function get thickness():Number {
			return _thickness;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set thickness(value:Number):void {
			_thickness = value;
		}

		/**
		 * <p> The type of the text field. Either one of the following TextFieldType constants: <code>TextFieldType.DYNAMIC</code>, which specifies a dynamic text field, which a user cannot edit, or <code>TextFieldType.INPUT</code>, which specifies an input text field, which a user can edit. </p>
		 * <p> The default value is <code>dynamic.</code></p>
		 * 
		 * @return 
		 */
		public function get type():String {
			return _type;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set type(value:String):void {
			_type = value;
		}

		/**
		 * <p> Specifies whether to copy and paste the text formatting along with the text. When set to <code>true</code>, Flash Player copies and pastes formatting (such as alignment, bold, and italics) when you copy and paste between text fields. Both the origin and destination text fields for the copy and paste procedure must have <code>useRichTextClipboard</code> set to <code>true</code>. The default value is <code>false</code>. </p>
		 * 
		 * @return 
		 */
		public function get useRichTextClipboard():Boolean {
			return _useRichTextClipboard;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set useRichTextClipboard(value:Boolean):void {
			_useRichTextClipboard = value;
		}

		/**
		 * <p> A Boolean value that indicates whether the text field has word wrap. If the value of <code>wordWrap</code> is <code>true</code>, the text field has word wrap; if the value is <code>false</code>, the text field does not have word wrap. The default value is <code>false</code>. </p>
		 * 
		 * @return 
		 */
		public function get wordWrap():Boolean {
			return _wordWrap;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set wordWrap(value:Boolean):void {
			_wordWrap = value;
		}

		/**
		 * <p> Appends the string specified by the <code>newText</code> parameter to the end of the text of the text field. This method is more efficient than an addition assignment (<code>+=</code>) on a <code>text</code> property (such as <code>someTextField.text += moreText</code>), particularly for a text field that contains a significant amount of content. </p>
		 * 
		 * @param newText  — The string to append to the existing text. 
		 */
		public function appendText(newText:String):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a rectangle that is the bounding box of the character. </p>
		 * 
		 * @param charIndex  — The zero-based index value for the character (for example, the first position is 0, the second position is 1, and so on). 
		 * @return  — A rectangle with  and  minimum and maximum values defining the bounding box of the character. 
		 */
		public function getCharBoundaries(charIndex:int):Rectangle {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the zero-based index value of the character at the point specified by the <code>x</code> and <code>y</code> parameters. </p>
		 * 
		 * @param x  — The <i>x</i> coordinate of the character. 
		 * @param y  — The <i>y</i> coordinate of the character. 
		 * @return  — The zero-based index value of the character (for example, the first position is 0, the second position is 1, and so on). Returns -1 if the point is not over any character. 
		 */
		public function getCharIndexAtPoint(x:Number, y:Number):int {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Given a character index, returns the index of the first character in the same paragraph. </p>
		 * 
		 * @param charIndex  — The zero-based index value of the character (for example, the first character is 0, the second character is 1, and so on). 
		 * @return  — The zero-based index value of the first character in the same paragraph. 
		 */
		public function getFirstCharInParagraph(charIndex:int):int {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a DisplayObject reference for the given <code>id</code>, for an image or SWF file that has been added to an HTML-formatted text field by using an <code>&lt;img&gt;</code> tag. The <code>&lt;img&gt;</code> tag is in the following format: </p>
		 * <pre><code>   &lt;img src = 'filename.jpg' id = 'instanceName' &gt;</code></pre>
		 * 
		 * @param id  — The <code>id</code> to match (in the <code>id</code> attribute of the <code>&lt;img&gt;</code> tag). 
		 * @return  — The display object corresponding to the image or SWF file with the matching  attribute in the  tag of the text field. For media loaded from an external source, this object is a Loader object, and, once loaded, the media object is a child of that Loader object. For media embedded in the SWF file, it is the loaded object. If no  tag with the matching  exists, the method returns . 
		 */
		public function getImageReference(id:String):DisplayObject {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the zero-based index value of the line at the point specified by the <code>x</code> and <code>y</code> parameters. </p>
		 * 
		 * @param x  — The <i>x</i> coordinate of the line. 
		 * @param y  — The <i>y</i> coordinate of the line. 
		 * @return  — The zero-based index value of the line (for example, the first line is 0, the second line is 1, and so on). Returns -1 if the point is not over any line. 
		 */
		public function getLineIndexAtPoint(x:Number, y:Number):int {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the zero-based index value of the line containing the character specified by the <code>charIndex</code> parameter. </p>
		 * 
		 * @param charIndex  — The zero-based index value of the character (for example, the first character is 0, the second character is 1, and so on). 
		 * @return  — The zero-based index value of the line. 
		 */
		public function getLineIndexOfChar(charIndex:int):int {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the number of characters in a specific text line. </p>
		 * 
		 * @param lineIndex  — The line number for which you want the length. 
		 * @return  — The number of characters in the line. 
		 */
		public function getLineLength(lineIndex:int):int {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns metrics information about a given text line. </p>
		 * 
		 * @param lineIndex  — The line number for which you want metrics information. 
		 * @return  — A TextLineMetrics object. 
		 */
		public function getLineMetrics(lineIndex:int):TextLineMetrics {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the character index of the first character in the line that the <code>lineIndex</code> parameter specifies. </p>
		 * 
		 * @param lineIndex  — The zero-based index value of the line (for example, the first line is 0, the second line is 1, and so on). 
		 * @return  — The zero-based index value of the first character in the line. 
		 */
		public function getLineOffset(lineIndex:int):int {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the text of the line specified by the <code>lineIndex</code> parameter. </p>
		 * 
		 * @param lineIndex  — The zero-based index value of the line (for example, the first line is 0, the second line is 1, and so on). 
		 * @return  — The text string contained in the specified line. 
		 */
		public function getLineText(lineIndex:int):String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Given a character index, returns the length of the paragraph containing the given character. The length is relative to the first character in the paragraph (as returned by <code>getFirstCharInParagraph()</code>), not to the character index passed in. </p>
		 * 
		 * @param charIndex  — The zero-based index value of the character (for example, the first character is 0, the second character is 1, and so on). 
		 * @return  — Returns the number of characters in the paragraph. 
		 */
		public function getParagraphLength(charIndex:int):int {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a TextFormat object that contains formatting information for the range of text that the <code>beginIndex</code> and <code>endIndex</code> parameters specify. Only properties that are common to the entire text specified are set in the resulting TextFormat object. Any property that is <i>mixed</i>, meaning that it has different values at different points in the text, has a value of <code>null</code>. </p>
		 * <p>If you do not specify values for these parameters, this method is applied to all the text in the text field. </p>
		 * <p>The following table describes three possible usages:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Usage</th>
		 *    <th>Description</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>my_textField.getTextFormat()</code></td>
		 *    <td>Returns a TextFormat object containing formatting information for all text in a text field. Only properties that are common to all text in the text field are set in the resulting TextFormat object. Any property that is <i>mixed</i>, meaning that it has different values at different points in the text, has a value of <code>null</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>my_textField.getTextFormat(beginIndex:Number)</code></td>
		 *    <td>Returns a TextFormat object containing a copy of the text format of the character at the <code>beginIndex</code> position.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>my_textField.getTextFormat(beginIndex:Number,endIndex:Number)</code></td>
		 *    <td>Returns a TextFormat object containing formatting information for the span of text from <code>beginIndex</code> to <code>endIndex-1</code>. Only properties that are common to all of the text in the specified range are set in the resulting TextFormat object. Any property that is mixed (that is, has different values at different points in the range) has its value set to <code>null</code>.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * 
		 * @param beginIndex  — Optional; an integer that specifies the starting location of a range of text within the text field. 
		 * @param endIndex  — Optional; an integer that specifies the position of the first character after the desired text span. As designed, if you specify <code>beginIndex</code> and <code>endIndex</code> values, the text from <code>beginIndex</code> to <code>endIndex-1</code> is read. 
		 * @return  — The TextFormat object that represents the formatting properties for the specified text. 
		 */
		public function getTextFormat(beginIndex:int = -1, endIndex:int = -1):TextFormat {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Replaces the current selection with the contents of the <code>value</code> parameter. The text is inserted at the position of the current selection, using the current default character format and default paragraph format. The text is not treated as HTML. </p>
		 * <p>You can use the <code>replaceSelectedText()</code> method to insert and delete text without disrupting the character and paragraph formatting of the rest of the text.</p>
		 * <p><b>Note:</b> This method does not work if a style sheet is applied to the text field.</p>
		 * 
		 * @param value  — The string to replace the currently selected text. 
		 */
		public function replaceSelectedText(value:String):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Replaces the range of characters that the <code>beginIndex</code> and <code>endIndex</code> parameters specify with the contents of the <code>newText</code> parameter. As designed, the text from <code>beginIndex</code> to <code>endIndex-1</code> is replaced. </p>
		 * <p><b>Note:</b> This method does not work if a style sheet is applied to the text field.</p>
		 * 
		 * @param beginIndex  — The zero-based index value for the start position of the replacement range. 
		 * @param endIndex  — The zero-based index position of the first character after the desired text span. 
		 * @param newText  — The text to use to replace the specified range of characters. 
		 */
		public function replaceText(beginIndex:int, endIndex:int, newText:String):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets as selected the text designated by the index values of the first and last characters, which are specified with the <code>beginIndex</code> and <code>endIndex</code> parameters. If the two parameter values are the same, this method sets the insertion point, as if you set the <code>caretIndex</code> property. </p>
		 * 
		 * @param beginIndex  — The zero-based index value of the first character in the selection (for example, the first character is 0, the second character is 1, and so on). 
		 * @param endIndex  — The zero-based index value of the last character in the selection. 
		 */
		public function setSelection(beginIndex:int, endIndex:int):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Applies the text formatting that the <code>format</code> parameter specifies to the specified text in a text field. The value of <code>format</code> must be a TextFormat object that specifies the desired text formatting changes. Only the non-null properties of <code>format</code> are applied to the text field. Any property of <code>format</code> that is set to <code>null</code> is not applied. By default, all of the properties of a newly created TextFormat object are set to <code>null</code>. </p>
		 * <p><b>Note:</b> This method does not work if a style sheet is applied to the text field.</p>
		 * <p>The <code>setTextFormat()</code> method changes the text formatting applied to a range of characters or to the entire body of text in a text field. To apply the properties of format to all text in the text field, do not specify values for <code>beginIndex</code> and <code>endIndex</code>. To apply the properties of the format to a range of text, specify values for the <code>beginIndex</code> and the <code>endIndex</code> parameters. You can use the <code>length</code> property to determine the index values.</p>
		 * <p>The two types of formatting information in a TextFormat object are character level formatting and paragraph level formatting. Each character in a text field can have its own character formatting settings, such as font name, font size, bold, and italic.</p>
		 * <p>For paragraphs, the first character of the paragraph is examined for the paragraph formatting settings for the entire paragraph. Examples of paragraph formatting settings are left margin, right margin, and indentation.</p>
		 * <p>Any text inserted manually by the user, or replaced by the <code>replaceSelectedText()</code> method, receives the default text field formatting for new text, and not the formatting specified for the text insertion point. To set the default formatting for new text, use <code>defaultTextFormat</code>.</p>
		 * 
		 * @param format  — A TextFormat object that contains character and paragraph formatting information. 
		 * @param beginIndex  — Optional; an integer that specifies the zero-based index position specifying the first character of the desired range of text. 
		 * @param endIndex  — Optional; an integer that specifies the first character after the desired text span. As designed, if you specify <code>beginIndex</code> and <code>endIndex</code> values, the text from <code>beginIndex</code> to <code>endIndex-1</code> is updated. <p> </p><table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Usage</th>
		 *    <th>Description</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>my_textField.setTextFormat(textFormat:TextFormat)</code></td>
		 *    <td>Applies the properties of <code>textFormat</code> to all text in the text field.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>my_textField.setTextFormat(textFormat:TextFormat, beginIndex:int)</code></td>
		 *    <td>Applies the properties of <code>textFormat</code> to the text starting with the <code>beginIndex</code> position.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>my_textField.setTextFormat(textFormat:TextFormat, beginIndex:int, endIndex:int)</code></td>
		 *    <td>Applies the properties of the <code>textFormat</code> parameter to the span of text from the <code>beginIndex</code> position to the <code>endIndex-1</code> position.</td>
		 *   </tr>
		 *  </tbody>
		 * </table> <p></p> <p>Notice that any text inserted manually by the user, or replaced by the <code>replaceSelectedText()</code> method, receives the default text field formatting for new text, and not the formatting specified for the text insertion point. To set a text field's default formatting for new text, use the <code>defaultTextFormat</code> property.</p> 
		 */
		public function setTextFormat(format:TextFormat, beginIndex:int = -1, endIndex:int = -1):void {
			//TODO: gérer autrement
			defaultTextFormat = format;
		}


		/**
		 * <p> Returns true if an embedded font is available with the specified <code>fontName</code> and <code>fontStyle</code> where <code>Font.fontType</code> is <code>flash.text.FontType.EMBEDDED</code>. Starting with Flash Player 10, two kinds of embedded fonts can appear in a SWF file. Normal embedded fonts are only used with TextField objects. CFF embedded fonts are only used with the flash.text.engine classes. The two types are distinguished by the <code>fontType</code> property of the <code>Font</code> class, as returned by the <code>enumerateFonts()</code> function. </p>
		 * <p>TextField cannot use a font of type <code>EMBEDDED_CFF</code>. If <code>embedFonts</code> is set to <code>true</code> and the only font available at run time with the specified name and style is of type <code>EMBEDDED_CFF</code>, Flash Player fails to render the text, as if no embedded font were available with the specified name and style.</p>
		 * <p>If both <code>EMBEDDED</code> and <code>EMBEDDED_CFF</code> fonts are available with the same name and style, the <code>EMBEDDED</code> font is selected and text renders with the <code>EMBEDDED</code> font.</p>
		 * 
		 * @param fontName  — The name of the embedded font to check. 
		 * @param fontStyle  — Specifies the font style to check. Use <code>flash.text.FontStyle</code> 
		 * @return  —  if a compatible embedded font is available, otherwise . 
		 */
		public static function isFontCompatible(fontName:String, fontStyle:String):Boolean {
			throw new Error("Not implemented");
		}
	}
}
