package flash.text {
	/**
	 *  The GridFitType class defines values for grid fitting in the TextField class. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="TextField.html" target="">flash.text.TextField</a>
	 * </div><br><hr>
	 */
	public class GridFitType {
		/**
		 * <p> Doesn't set grid fitting. Horizontal and vertical lines in the glyphs are not forced to the pixel grid. This constant is used in setting the <code>gridFitType</code> property of the TextField class. This is often a good setting for animation or for large font sizes. Use the syntax <code>GridFitType.NONE</code>. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="TextField.html#gridFitType" target="">flash.text.TextField.gridFitType</a>
		 * </div>
		 */
		public static const NONE:String = "none";
		/**
		 * <p> Fits strong horizontal and vertical lines to the pixel grid. This constant is used in setting the <code>gridFitType</code> property of the TextField class. This setting only works for left-justified text fields and acts like the <code>GridFitType.SUBPIXEL</code> constant in static text. This setting generally provides the best readability for left-aligned text. Use the syntax <code>GridFitType.PIXEL</code>. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="TextField.html#gridFitType" target="">flash.text.TextField.gridFitType</a>
		 * </div>
		 */
		public static const PIXEL:String = "pixel";
		/**
		 * <p> Fits strong horizontal and vertical lines to the sub-pixel grid on LCD monitors. (Red, green, and blue are actual pixels on an LCD screen.) This is often a good setting for right-aligned or center-aligned dynamic text, and it is sometimes a useful tradeoff for animation vs. text quality. This constant is used in setting the <code>gridFitType</code> property of the TextField class. Use the syntax <code>GridFitType.SUBPIXEL</code>. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="TextField.html#gridFitType" target="">flash.text.TextField.gridFitType</a>
		 * </div>
		 */
		public static const SUBPIXEL:String = "subpixel";
	}
}
