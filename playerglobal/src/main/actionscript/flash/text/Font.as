package flash.text {
	/**
	 *  The Font class is used to manage embedded fonts in SWF files. Embedded fonts are represented as a subclass of the Font class. The Font class is currently useful only to find out information about embedded fonts; you cannot alter a font by using this class. You cannot use the Font class to load external fonts, or to create an instance of a Font object by itself. Use the Font class as an abstract base class. <p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS2db454920e96a9e51e63e3d11c0bf6320a-7ffe.html" target="_blank">Using device fonts</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS2db454920e96a9e51e63e3d11c0bf69084-7e06.html" target="_blank">Using multiple typefaces</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS2db454920e96a9e51e63e3d11c0bf6320a-7ff1.html" target="_blank">Embedding multi-byte fonts</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS0FA8AEDB-C69F-4f19-ADA5-AA5757217624.html" target="_blank">Embedding fonts with MX components</a>
	 * </div><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS2db454920e96a9e51e63e3d11c0bf6320a-7fff.html" target="_blank">About fonts</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS2db454920e96a9e51e63e3d11c0bf6320a-7fec.html" target="_blank">Troubleshooting fonts in applications</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS2db454920e96a9e51e63e3d11c0bf69084-7e09.html" target="_blank">About the font managers</a>
	 *  <br>
	 * </div><br><hr>
	 */
	public class Font {
		private var _fontName:String;
		private var _fontStyle:String;
		private var _fontType:String;

		/**
		 * <p> The name of an embedded font. </p>
		 * 
		 * @return 
		 */
		public function get fontName():String {
			return _fontName;
		}

		/**
		 * <p> The style of the font. This value can be any of the values defined in the FontStyle class. </p>
		 * 
		 * @return 
		 */
		public function get fontStyle():String {
			return _fontStyle;
		}

		/**
		 * <p> The type of the font. This value can be any of the constants defined in the FontType class. </p>
		 * 
		 * @return 
		 */
		public function get fontType():String {
			return _fontType;
		}

		/**
		 * <p> Specifies whether a provided string can be displayed using the currently assigned font. </p>
		 * 
		 * @param str  — The string to test against the current font. 
		 * @return  — A value of  if the specified string can be fully displayed using this font. 
		 */
		public function hasGlyphs(str:String):Boolean {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Specifies whether to provide a list of the currently available embedded fonts. </p>
		 * 
		 * @param enumerateDeviceFonts  — Indicates whether you want to limit the list to only the currently available embedded fonts. If this is set to <code>true</code> then a list of all fonts, both device fonts and embedded fonts, is returned. If this is set to <code>false</code> then only a list of embedded fonts is returned. 
		 * @return  — A list of available fonts as an array of Font objects. 
		 */
		public static function enumerateFonts(enumerateDeviceFonts:Boolean = false):Array {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Registers a font class in the global font list. </p>
		 * 
		 * @param font  — The class you want to add to the global font list. 
		 */
		public static function registerFont(font:Class):void {
			throw new Error("Not implemented");
		}
	}
}
