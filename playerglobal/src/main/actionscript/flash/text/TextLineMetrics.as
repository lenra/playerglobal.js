package flash.text {
	/**
	 *  The TextLineMetrics class contains information about the text position and measurements of a <i>line of text</i> within a text field. All measurements are in pixels. Objects of this class are returned by the <code>flash.text.TextField.getLineMetrics()</code> method. <p>For measurements related to the text field containing the line of text (for example, the "Text Field height" measurement in the diagram), see flash.text.TextField. </p> <p>The following diagram indicates the points and measurements of a text field and the line of text the field contains:</p> <p> <img src="../../images/text-metrics.jpg" alt="An image illustrating text metrics"> </p> <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="TextField.html" target="">flash.text.TextField</a>
	 * </div><br><hr>
	 */
	public class TextLineMetrics {
		private var _ascent:Number;
		private var _descent:Number;
		private var _height:Number;
		private var _leading:Number;
		private var _width:Number;
		private var _x:Number;

		/**
		 * <p> Creates a TextLineMetrics object. The TextLineMetrics object contains information about the text metrics of a line of text in a text field. Objects of this class are returned by the <code>flash.text.TextField.getLineMetrics()</code> method. </p>
		 * <p>See the diagram in the overview for this class for the properties in context.</p>
		 * 
		 * @param x  — The left position of the first character in pixels. 
		 * @param width  — The width of the text of the selected lines (not necessarily the complete text) in pixels. 
		 * @param height  — The height of the text of the selected lines (not necessarily the complete text) in pixels. 
		 * @param ascent  — The length from the baseline to the top of the line height in pixels. 
		 * @param descent  — The length from the baseline to the bottom depth of the line in pixels. 
		 * @param leading  — The measurement of the vertical distance between the lines of text. 
		 */
		public function TextLineMetrics(x:Number, width:Number, height:Number, ascent:Number, descent:Number, leading:Number) {
			throw new Error("Not implemented");
		}

		/**
		 * <p> The ascent value of the text is the length from the baseline to the top of the line height in pixels. See the "Ascent" measurement in the overview diagram for this class. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="TextLineMetrics.html" target="">TextLineMetrics class overview</a>
		 * </div>
		 * 
		 * @return 
		 */
		public function get ascent():Number {
			return _ascent;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set ascent(value:Number):void {
			_ascent = value;
		}

		/**
		 * <p> The descent value of the text is the length from the baseline to the bottom depth of the line in pixels. See the "Descent" measurement in the overview diagram for this class. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="TextLineMetrics.html" target="">TextLineMetrics class overview</a>
		 * </div>
		 * 
		 * @return 
		 */
		public function get descent():Number {
			return _descent;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set descent(value:Number):void {
			_descent = value;
		}

		/**
		 * <p> The height value of the text of the selected lines (not necessarily the complete text) in pixels. The height of the text line does not include the gutter height. See the "Line height" measurement in the overview diagram for this class. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="TextLineMetrics.html" target="">TextLineMetrics class overview</a>
		 * </div>
		 * 
		 * @return 
		 */
		public function get height():Number {
			return _height;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set height(value:Number):void {
			_height = value;
		}

		/**
		 * <p> The leading value is the measurement of the vertical distance between the lines of text. See the "Leading" measurement in the overview diagram for this class. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="TextLineMetrics.html" target="">TextLineMetrics class overview</a>
		 * </div>
		 * 
		 * @return 
		 */
		public function get leading():Number {
			return _leading;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set leading(value:Number):void {
			_leading = value;
		}

		/**
		 * <p> The width value is the width of the text of the selected lines (not necessarily the complete text) in pixels. The width of the text line is not the same as the width of the text field. The width of the text line is relative to the text field width, minus the gutter width of 4 pixels (2 pixels on each side). See the "Text Line width" measurement in the overview diagram for this class. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="TextLineMetrics.html" target="">TextLineMetrics class overview</a>
		 * </div>
		 * 
		 * @return 
		 */
		public function get width():Number {
			return _width;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set width(value:Number):void {
			_width = value;
		}

		/**
		 * <p> The x value is the left position of the first character in pixels. This value includes the margin, indent (if any), and gutter widths. See the "Text Line x-position" in the overview diagram for this class. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="TextLineMetrics.html" target="">TextLineMetrics class overview</a>
		 * </div>
		 * 
		 * @return 
		 */
		public function get x():Number {
			return _x;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set x(value:Number):void {
			_x = value;
		}
	}
}
