package flash.text {
	/**
	 *  The TextFieldType class is an enumeration of constant values used in setting the <code>type</code> property of the TextField class. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="TextField.html#type" target="">flash.text.TextField.type</a>
	 * </div><br><hr>
	 */
	public class TextFieldType {
		/**
		 * <p> Used to specify a <code>dynamic</code> TextField. </p>
		 */
		public static const DYNAMIC:String = "dynamic";
		/**
		 * <p> Used to specify an <code>input</code> TextField. </p>
		 */
		public static const INPUT:String = "input";
	}
}
