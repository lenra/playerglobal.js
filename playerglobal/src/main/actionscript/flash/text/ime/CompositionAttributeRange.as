package flash.text.ime {
	/**
	 *  The CompositionAttributeRange class represents a range of composition attributes for use with IME (input method editor) events. For example, when editing text in the IME, the text is divided by the IME into composition ranges. These composition ranges are flagged as selected (such as currently being lengthened, shortened, or edited), and/or converted (meaning the range went through the IME dictionary lookup, already). <p>By convention, the client should adorn these composition ranges with underlining or highlighting according to the flags.</p> <p>For example:</p> <div class="listing">
	 *  <div class="clipcopy">
	 *   <a href="#" class="copyText">Copy</a>
	 *  </div>
	 *  <pre>
	 *      !converted              = thick gray underline (raw text)
	 *      !selected &amp;&amp; converted  = thin black underline
	 *       selected &amp;&amp; converted  = thick black underline
	 * </pre>
	 * </div> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="IIMEClient.html" target="">flash.text.ime.IIMEClient</a>
	 * </div><br><hr>
	 */
	public class CompositionAttributeRange {
		private var _converted:Boolean;
		private var _relativeEnd:int;
		private var _relativeStart:int;
		private var _selected:Boolean;

		/**
		 * <p> Creates a CompositionAttributeRange object. </p>
		 * 
		 * @param relativeStart  — The zero based index of the first character included in the character range. 
		 * @param relativeEnd  — The zero based index of the last character included in the character range. 
		 * @param selected  — Defines the current composition clause as active or not. 
		 * @param converted  — Defines the current clause as processed by the IME and waiting for user confirmation. 
		 */
		public function CompositionAttributeRange(relativeStart:int, relativeEnd:int, selected:Boolean, converted:Boolean) {
			throw new Error("Not implemented");
		}

		/**
		 * <p> A property defining the current clause has been processed by the IME and the clause is waiting to be accepted or confirmed by the user. </p>
		 * 
		 * @return 
		 */
		public function get converted():Boolean {
			return _converted;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set converted(value:Boolean):void {
			_converted = value;
		}

		/**
		 * <p> The position of the end of the composition clause, relative to the beginning of the inline edit session. For example, <code>0</code> equals the start of the text the IME reads (however, text might exist before that position in the edit field). </p>
		 * 
		 * @return 
		 */
		public function get relativeEnd():int {
			return _relativeEnd;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set relativeEnd(value:int):void {
			_relativeEnd = value;
		}

		/**
		 * <p> The relative start position from the beginning of the current inline editing session. For example, <code>0</code> equals the start of the text the IME reads (however, text might exist before that position in the edit field). </p>
		 * 
		 * @return 
		 */
		public function get relativeStart():int {
			return _relativeStart;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set relativeStart(value:int):void {
			_relativeStart = value;
		}

		/**
		 * <p> A property defining the current composition clause is active and lengthened or shortened or edited with the IME while the neighboring clauses are not changing. </p>
		 * 
		 * @return 
		 */
		public function get selected():Boolean {
			return _selected;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set selected(value:Boolean):void {
			_selected = value;
		}
	}
}
