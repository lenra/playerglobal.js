package flash.concurrent {
	/**
	 *  A Condition object is a tool for sharing a resource between workers with the additional capability of pausing execution until a particular condition is satisfied. A Condition object is used in conjunction with a Mutex object, and adds additional functionality to the mutex's behavior. By working in combination with a mutex, the runtime ensures that each transition of ownership between workers is atomic. <p>The following is one possible workflow for using a Condition object:</p> <ol> 
	 *  <li>Before using a Condition object, the first worker must take ownership of the condition's associated mutex by calling the Mutex object's <code>lock()</code> or <code>tryLock()</code> methods.</li> 
	 *  <li>The worker's code operates on the shared resource until some condition becomes false, preventing the worker from doing more work with the shared resource. For example, if the shared resource is a set of data to process, when there is no more data to process the worker can't do any more work.</li> 
	 *  <li>At that point, call the Condition object's <code>wait()</code> method to pause the worker's execution and release ownership of the mutex.</li> 
	 *  <li>At some point, a second worker takes ownership of the mutex. Because the mutex is available, it is safe for the second worker's code to operate on the shared resource. The second worker does whatever is necessary to satisfy the condition so that the first worker can do its work again. For example, if the first worker has no data to process, the second worker could pass more data to process into the shared resource.</li> 
	 *  <li>At that point, the condition related to the first worker's work is now true, so the first worker needs to be notified that the condition is fulfilled. To notify the first worker, the second worker's code calls the Condition object's <code>notify()</code> method or its <code>notifyAll()</code> method.</li> 
	 *  <li>In addition to calling <code>notify()</code>, the second worker needs to release ownership of the mutex. It does this either by calling the Mutex object's <code>unlock()</code> method or the Condition object's <code>wait()</code> method. Since the first worker called the <code>wait()</code> method, ownership of the mutex returns to the first worker. Code execution in the first worker then continues again with the next line of code following the <code>wait()</code> call.</li> 
	 * </ol> <p>The Condition class is one of the special object types that are shared between workers rather than copied between them. When you pass a condition from one worker to another worker either by calling the Worker object's <code>setSharedProperty()</code> method or by using a MessageChannel object, both workers have a reference to the same Condition object in the runtime's memory.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Mutex.html" target="">Mutex class</a>
	 *  <br>
	 *  <a href="../../flash/system/Worker.html" target="">Worker class</a>
	 * </div><br><hr>
	 */
	public class Condition {
		private static var _isSupported:Boolean;

		private var _mutex:Mutex;

		/**
		 * <p> Creates a new Condition instance. </p>
		 * 
		 * @param mutex  — the mutex that the condition uses to control transitions between workers 
		 */
		public function Condition(mutex:Mutex) {
			throw new Error("Not implemented");
		}

		/**
		 * <p> The mutex associated with this condition. </p>
		 * 
		 * @return 
		 */
		public function get mutex():Mutex {
			return _mutex;
		}

		/**
		 * <p> Specifes that the condition that this Condition object represents has been satisfied and that ownership of the mutex will be returned to the next worker (if any) that's waiting on this condition. </p>
		 * <p>Calling this method doesn't automatically release ownership of the mutex. After calling <code>notify()</code>, you should explicitly release ownership of the mutex in one of two ways: call the <code>Mutex.unlock()</code> method if the current worker doesn't need the mutex again, or call <code>wait()</code> if the worker should get ownership of the mutex again after other workers have completed their work.</p>
		 * <p>One the mutex's lock is released, the next worker in the queue of workers that have called the <code>wait()</code> method acquires the mutex and resumes code execution.</p>
		 */
		public function notify():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Specifies that the condition that this Condition object represents has been satisfied and that ownership of the mutex will be returned to all workers that are waiting on this condition. </p>
		 * <p>Calling this method doesn't automatically release ownership of the mutex. After calling <code>notify()</code>, you should explicitly release ownership of the mutex in one of two ways: call the <code>Mutex.unlock()</code> method if the current worker doesn't need the mutex again, or call <code>wait()</code> if the worker should get ownership of the mutex again after other workers have completed their work.</p>
		 * <p>Once the mutex's lock is released, the waiting workers receive ownership one at a time in the order they called the <code>wait()</code> method. Each worker that has called the <code>wait()</code> method acquires the mutex in turn and resumes code execution. When that worker calls the <code>Mutex.unlock()</code> method or the <code>wait()</code> method, mutex ownership then switches to the next waiting worker. Each time mutex ownership switches between workers, the transition is performed as a single atomic operation.</p>
		 */
		public function notifyAll():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Specifies that the condition that this Condition object represents isn't satisfied, and the current worker needs to wait until it is satisfied before executing more code. Calling this method pauses the current worker's execution thread and releases ownership of the condition's mutex. These steps are performed as a single atomic operation. The worker remains paused until another worker calls this Condition object's <code>notify()</code> or <code>notifyAll()</code> methods. </p>
		 * 
		 * @param timeout  — the maximum amount of time, in milliseconds, that the worker should pause execution before continuing. If this value is -1 (the default) there is no no timeout and execution pauses indefinitely. 
		 * @return  —  if the method returned because the timeout time elapsed. Otherwise the method returns . 
		 */
		public function wait(timeout:Number = -1):Boolean {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Indicates whether the Condition class is supported for the current platform. </p>
		 * <p><b>Note</b>: if the Mutex class is not supported, creation of a Condition instance is not possible and this property is <code>false</code>.</p>
		 * 
		 * @return 
		 */
		public static function get isSupported():Boolean {
			return _isSupported;
		}
	}
}
