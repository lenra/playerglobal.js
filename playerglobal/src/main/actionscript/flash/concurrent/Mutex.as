package flash.concurrent {
	/**
	 *  The Mutex (short for "mutual exclusion") class provides a way to make sure that only one set of code operates on a particular block of memory or other shared resource at a time. The primary use for a Mutex is to manage code in different workers accessing a shareable byte array (a ByteArray object whose <code>shareable</code> property is <code>true</code>). However, a Mutex can be used to manage workers' access to any shareable resource, such as an AIR native extension or a filesystem file. No matter what the resource, the purpose of the mutex is to ensure that only one set of code accesses the resource at a time. <p>A mutex manages access using the concept of resource ownership. At any time a single mutex is "owned" by at most one worker. When ownership of a mutex switches from one worker to another the transision is atomic. This guarantees that it will never be possible for more than one worker to take ownership of the mutex. As long as code in a worker only operates on a shared resource when that worker owns the mutex, you can be certain that there will never be a conflict from multiple workers.</p> <p>Use the <code>tryLock()</code> method to take ownership of the mutex if it is available. Use the <code>lock()</code> method to pause the current worker's execution until the mutex is available, then take ownership of the mutex. Once the current worker has ownership of the mutex, it can safely operate on the shared resource. When those operations are complete, call the <code>unlock()</code> method to release the mutex. At that point the current worker should no longer access the shared resource.</p> <p>The Mutex class is one of the special object types that are shared between workers rather than copied between them. When you pass a mutex from one worker to another worker either by calling the Worker object's <code>setSharedProperty()</code> method or by using a MessageChannel object, both workers have a reference to the same Mutex object in the runtime's memory.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../flash/system/Worker.html" target="">Worker class</a>
	 *  <br>
	 *  <a href="../../flash/utils/ByteArray.html#shareable" target="">ByteArray.shareable property</a>
	 *  <br>
	 *  <a href="Condition.html" target="">Condition class</a>
	 * </div><br><hr>
	 */
	public class Mutex {

		/**
		 * <p> Creates a new Mutex instance. </p>
		 */
		public function Mutex() {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Pauses execution of the current worker until this mutex is available and then takes ownership of the mutex. If another worker owns the mutex when <code>lock()</code> is called, the calling worker's execution thread pauses at the <code>lock()</code> call and the worker is added to the queue of ownership requests. Once the calling worker acquires the mutex, the worker's execution continues with the line of code following the <code>lock()</code> call. </p>
		 * <p>Once the current worker has ownership of the mutex, it can safely operate on the shared resource. When those operations are complete, call the <code>unlock()</code> method to release the mutex. At that point the current worker should no longer access the shared resource.</p>
		 * <p>Internally, a mutex keeps a count of the number of lock requests it has received. The mutex must receive the same number of unlock requests before it is completely released. If code in the worker that owns the mutex locks it again (by calling the <code>lock()</code> method) the internal lock count increases by one. You must call the <code>unlock()</code> method as many times as the number of lock requests to release ownership of the mutex.</p>
		 * <p>When multiple workers are waiting for a mutex, the mutex gives priority to assigning ownership to the longest-waiting worker. However, scheduling of worker threads is managed by the host operating system so there is no guarantee of a particular code execution order across workers.</p>
		 */
		public function lock():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Acquires ownership of the mutex if it is available. If another worker already owns the mutex or another worker has called the <code>lock()</code> method and is waiting to acquire the mutex, the mutex is not available. In that case, calling this method returns <code>false</code> and code execution continues immediately. </p>
		 * <p>Once the current worker has ownership of the mutex, it can safely operate on the shared resource. When those operations are complete, call the <code>unlock()</code> method to release the mutex. At that point the current worker should no longer access the shared resource.</p>
		 * <p>When multiple workers are waiting for a mutex, the mutex gives priority to assigning ownership to the longest-waiting worker. However, scheduling of worker threads is managed by the host operating system so there is no guarantee of a particular code execution order across workers.</p>
		 * 
		 * @return  —  if the mutex was available (and is now owned by the current worker), or  if the current worker did not acquire ownership of the mutex. 
		 */
		public function tryLock():Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Releases ownership of this mutex, allowing any worker to acquire it and perform work on the associated resource. </p>
		 * <p>Internally, a mutex keeps a count of the number of lock requests it has received. Code in a worker must call the <code>unlock()</code> method as many times as the number of lock requests in order to release ownership of the mutex.</p>
		 */
		public function unlock():void {
			throw new Error("Not implemented");
		}
	}
}
