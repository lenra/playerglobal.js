package flash.errors {
	/**
	 *  The Flash runtimes throw this exception when they encounter a corrupted SWF file. <br><hr>
	 */
	public class InvalidSWFError extends Error {

		/**
		 * <p> Creates a new InvalidSWFError object. </p>
		 * 
		 * @param message  — A string associated with the error object. 
		 * @param id
		 */
		public function InvalidSWFError(message:String = "", id:int = 0) {
			super(message, id);
			throw new Error("Not implemented");
		}
	}
}
