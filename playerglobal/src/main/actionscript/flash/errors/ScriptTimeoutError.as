package flash.errors {
	/**
	 *  The ScriptTimeoutError exception is thrown when the script timeout interval is reached. The script timeout interval is 15 seconds. <p>Two ScriptTimeoutError exceptions are thrown. The first exception you can catch and exit cleanly. If there is no exception handler, the uncaught exception terminates execution. The second exception is thrown but cannot be caught by user code; it goes to the uncaught exception handler. It is uncatchable to prevent the player from hanging indefinitely.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7ece.html" target="_blank">Comparing the Error classes</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7eb1.html" target="_blank">flash.error package Error classes</a>
	 * </div><br><hr>
	 */
	public class ScriptTimeoutError extends Error {

		/**
		 * <p> Creates a new ScriptTimeoutError object. </p>
		 * 
		 * @param message  — A string associated with the error object. 
		 */
		public function ScriptTimeoutError(message:String = "") {
			super(message);
			throw new Error("Not implemented");
		}
	}
}
