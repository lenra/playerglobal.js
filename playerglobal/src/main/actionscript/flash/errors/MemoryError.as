package flash.errors {
	/**
	 *  The MemoryError exception is thrown when a memory allocation request fails. <p>On a desktop machine, memory allocation failures are rare unless an allocation request is extremely large. For example, a 32-bit Windows program can access only 2GB of address space, so a request for 10 billion bytes is impossible.</p> <p>By default, Flash Player does not impose a limit on how much memory an ActionScript program can allocate.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7ece.html" target="_blank">Comparing the Error classes</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7eb1.html" target="_blank">flash.error package Error classes</a>
	 * </div><br><hr>
	 */
	public class MemoryError extends Error {

		/**
		 * <p> Creates a new MemoryError object. </p>
		 * 
		 * @param message  — A string associated with the error object. 
		 */
		public function MemoryError(message:String = "") {
			super(message);
			throw new Error("Not implemented");
		}
	}
}
