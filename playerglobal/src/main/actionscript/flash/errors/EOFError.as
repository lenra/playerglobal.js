package flash.errors {
	/**
	 *  An EOFError exception is thrown when you attempt to read past the end of the available data. For example, an EOFError is thrown when one of the read methods in the IDataInput interface is called and there is insufficient data to satisfy the read request. <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7ecc.html" target="_blank">Working with the debugger versions of Flash runtimes</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7ece.html" target="_blank">Comparing the Error classes</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7eb1.html" target="_blank">flash.error package Error classes</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../flash/utils/ByteArray.html" target="">flash.utils.ByteArray</a>
	 *  <br>
	 *  <a href="../../flash/utils/IDataInput.html" target="">flash.utils.IDataInput</a>
	 *  <br>
	 *  <a href="../../flash/net/Socket.html" target="">flash.net.Socket</a>
	 *  <br>
	 *  <a href="../../flash/net/URLStream.html" target="">flash.net.URLStream</a>
	 * </div><br><hr>
	 */
	public class EOFError extends IOError {

		/**
		 * <p> Creates a new EOFError object. </p>
		 * 
		 * @param message  — A string associated with the error object. 
		 */
		public function EOFError(message:String = "") {
			super(message);
		}
	}
}
