package flash.errors {
	/**
	 *  The IOError exception is thrown when some type of input or output failure occurs. For example, an IOError exception is thrown if a read/write operation is attempted on a socket that has not connected or that has become disconnected. <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7ecc.html" target="_blank">Working with the debugger versions of Flash runtimes</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7ece.html" target="_blank">Comparing the Error classes</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7eb1.html" target="_blank">flash.error package Error classes</a>
	 * </div><br><hr>
	 */
	public class IOError extends Error {

		/**
		 * <p> Creates a new IOError object. </p>
		 * 
		 * @param message  — A string associated with the error object. 
		 */
		public function IOError(message:String = "") {
			super(message, 2030);
		}
	}
}
