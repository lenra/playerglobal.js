package flash.ui {
	/**
	 *  The KeyboardType class is an enumeration class that provides values for different categories of physical computer or device keyboards. <p>Use the values defined by the KeyboardType class with the <code>Keybooard.physicalKeyboardType</code> property.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Keyboard.html#physicalKeyboardType" target="">Keyboard.physicalKeyboardType</a>
	 * </div><br><hr>
	 */
	public class KeyboardType {
		/**
		 * <p> A standard keyboard with a full set of numbers and letters. </p>
		 * <p>Most desktop computers and some mobile devices provide an alphanumeric keyboard.</p>
		 */
		public static const ALPHANUMERIC:String = "alphanumeric";
		/**
		 * <p> A phone-style 12-button keypad. </p>
		 * <p>Many mobile devices provide a keypad, although some provide an alphanumeric keyboard.</p>
		 */
		public static const KEYPAD:String = "keypad";
		/**
		 * <p> No physical keyboard is supported. </p>
		 * <p>Typically, a virtual keyboard is provided in the absence of a physical keyboard.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Keyboard.html#hasVirtualKeyboard" target="">flash.ui.Keyboard.hasVirtualKeyboard</a>
		 * </div>
		 */
		public static const NONE:String = "none";
	}
}
