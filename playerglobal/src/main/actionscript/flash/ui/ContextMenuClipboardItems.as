package flash.ui {
	/**
	 *  The ContextMenuClipboardItems class lets you enable or disable the commands in the clipboard context menu. <p>Enable or disable the context menu clipboard commands using the <code>clipboardItems</code> property of the ContextMenu object. The <code>clipboardItems</code> property is an instance of this ContextMenuClipboardItems class. The clipboard context menu is shown in a context menu when the <code>clipboardMenu</code> property of the context menu is <code>true</code>, unless the context menu is for a TextField object. TextField objects control the display of the context menu and the state of its clipboard items automatically.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="ContextMenu.html#clipboardMenu" target="">ContextMenu.clipboardMenu</a>
	 * </div><br><hr>
	 */
	public class ContextMenuClipboardItems {
		private var _clear:Boolean;
		private var _copy:Boolean;
		private var _cut:Boolean;
		private var _paste:Boolean;
		private var _selectAll:Boolean;

		/**
		 * <p> Creates a new ContextMenuClipboardItems object. </p>
		 */
		public function ContextMenuClipboardItems() {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Enables or disables the 'Delete' or 'Clear' item on the clipboard menu. This should be enabled only if an object that can be cleared is selected. </p>
		 * 
		 * @return 
		 */
		public function get clear():Boolean {
			return _clear;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set clear(value:Boolean):void {
			_clear = value;
		}

		/**
		 * <p> Enables or disables the 'Copy' item on the clipboard menu. This should be enabled only if an object that can be copied is selected. </p>
		 * 
		 * @return 
		 */
		public function get copy():Boolean {
			return _copy;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set copy(value:Boolean):void {
			_copy = value;
		}

		/**
		 * <p> Enables or disables the 'Cut' item on the clipboard menu. This should be enabled only if an object that can be cut is selected. </p>
		 * 
		 * @return 
		 */
		public function get cut():Boolean {
			return _cut;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set cut(value:Boolean):void {
			_cut = value;
		}

		/**
		 * <p> Enables or disables the 'Paste' item on the clipboard menu. This should be enabled only if pastable data is on the clipboard. </p>
		 * 
		 * @return 
		 */
		public function get paste():Boolean {
			return _paste;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set paste(value:Boolean):void {
			_paste = value;
		}

		/**
		 * <p> Enables or disables the 'Select All' item on the clipboard menu. This should only be enabled in a context where a selection can be expanded to include all similar items, such as in a list or a text editing control. </p>
		 * 
		 * @return 
		 */
		public function get selectAll():Boolean {
			return _selectAll;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set selectAll(value:Boolean):void {
			_selectAll = value;
		}
	}
}
