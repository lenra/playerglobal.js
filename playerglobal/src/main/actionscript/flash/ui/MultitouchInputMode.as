package flash.ui {
	/**
	 *  The MultitouchInputMode class provides values for the <code>inputMode</code> property in the flash.ui.Multitouch class. These values set the type of touch events the Flash runtime dispatches when the user interacts with a touch-enabled device. <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS6db89f63a9bf49c14717f65126af9d2579-8000.html" target="_blank">Set the input type</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Multitouch.html#inputMode" target="">flash.ui.Multitouch.inputMode</a>
	 * </div><br><hr>
	 */
	public class MultitouchInputMode {
		/**
		 * <p> Specifies that TransformGestureEvent, PressAndTapGestureEvent, and GestureEvent events are dispatched for the related user interaction supported by the current environment, and other touch events (such as a simple tap) are interpreted as mouse events. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Multitouch.html#inputMode" target="">flash.ui.Multitouch.inputMode</a>
		 *  <br>
		 *  <a href="../../flash/events/TransformGestureEvent.html" target="">flash.events.TransformGestureEvent</a>
		 *  <br>
		 *  <a href="../../flash/events/GestureEvent.html" target="">flash.events.GestureEvent</a>
		 *  <br>
		 *  <a href="../../flash/events/PressAndTapGestureEvent.html" target="">flash.events.PressAndTapGestureEvent</a>
		 *  <br>
		 *  <a href="../../flash/events/TouchEvent.html" target="">flash.events.TouchEvent</a>
		 * </div>
		 */
		public static const GESTURE:String = "gesture";
		/**
		 * <p> Specifies that all user contact with a touch-enabled device is interpreted as a type of mouse event. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Multitouch.html#inputMode" target="">flash.ui.Multitouch.inputMode</a>
		 *  <br>
		 *  <a href="../../flash/events/MouseEvent.html" target="">flash.events.MouseEvent</a>
		 * </div>
		 */
		public static const NONE:String = "none";
		/**
		 * <p> Specifies that events are dispatched only for basic touch events, such as a single finger tap. When you use this setting, events listed in the TouchEvent class are dispatched; events listed in the TransformGestureEvent, PressAndTapGestureEvent, and GestureEvent classes are not dispatched. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Multitouch.html#inputMode" target="">flash.ui.Multitouch.inputMode</a>
		 *  <br>
		 *  <a href="../../flash/events/TransformGestureEvent.html" target="">flash.events.TransformGestureEvent</a>
		 *  <br>
		 *  <a href="../../flash/events/GestureEvent.html" target="">flash.events.GestureEvent</a>
		 *  <br>
		 *  <a href="../../flash/events/PressAndTapGestureEvent.html" target="">flash.events.PressAndTapGestureEvent</a>
		 *  <br>
		 *  <a href="../../flash/events/TouchEvent.html" target="">flash.events.TouchEvent</a>
		 * </div>
		 */
		public static const TOUCH_POINT:String = "touchPoint";
	}
}
