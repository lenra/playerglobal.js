package flash.ui {
	/**
	 *  The ContextMenuBuiltInItems class describes the items that are built in to a context menu. You can hide these items by using the <code>ContextMenu.hideBuiltInItems()</code> method. <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="ContextMenu.html#hideBuiltInItems()" target="">ContextMenu.hideBuiltInItems()</a>
	 * </div><br><hr>
	 */
	public class ContextMenuBuiltInItems {
		private var _forwardAndBack:Boolean;
		private var _loop:Boolean;
		private var _play:Boolean;
		private var _print:Boolean;
		private var _quality:Boolean;
		private var _rewind:Boolean;
		private var _save:Boolean;
		private var _zoom:Boolean;

		/**
		 * <p> Creates a new ContextMenuBuiltInItems object so that you can set the properties for Flash Player to display or hide each menu item. </p>
		 */
		public function ContextMenuBuiltInItems() {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Lets the user move forward or backward one frame in a SWF file at run time (does not appear for a single-frame SWF file). </p>
		 * 
		 * @return 
		 */
		public function get forwardAndBack():Boolean {
			return _forwardAndBack;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set forwardAndBack(value:Boolean):void {
			_forwardAndBack = value;
		}

		/**
		 * <p> Lets the user set a SWF file to start over automatically when it reaches the final frame (does not appear for a single-frame SWF file). </p>
		 * 
		 * @return 
		 */
		public function get loop():Boolean {
			return _loop;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set loop(value:Boolean):void {
			_loop = value;
		}

		/**
		 * <p> Lets the user start a paused SWF file (does not appear for a single-frame SWF file). </p>
		 * 
		 * @return 
		 */
		public function get play():Boolean {
			return _play;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set play(value:Boolean):void {
			_play = value;
		}

		/**
		 * <p> Lets the user send the displayed frame image to a printer. </p>
		 * 
		 * @return 
		 */
		public function get print():Boolean {
			return _print;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set print(value:Boolean):void {
			_print = value;
		}

		/**
		 * <p> Lets the user set the resolution of the SWF file at run time. </p>
		 * 
		 * @return 
		 */
		public function get quality():Boolean {
			return _quality;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set quality(value:Boolean):void {
			_quality = value;
		}

		/**
		 * <p> Lets the user set a SWF file to play from the first frame when selected, at any time (does not appear for a single-frame SWF file). </p>
		 * 
		 * @return 
		 */
		public function get rewind():Boolean {
			return _rewind;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set rewind(value:Boolean):void {
			_rewind = value;
		}

		/**
		 * <p> Lets the user with Shockmachine installed save a SWF file. </p>
		 * 
		 * @return 
		 */
		public function get save():Boolean {
			return _save;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set save(value:Boolean):void {
			_save = value;
		}

		/**
		 * <p> Lets the user zoom in and out on a SWF file at run time. </p>
		 * 
		 * @return 
		 */
		public function get zoom():Boolean {
			return _zoom;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set zoom(value:Boolean):void {
			_zoom = value;
		}
	}
}
