package flash.ui {
	/**
	 *  The Keyboard class is used to build an interface that can be controlled by a user with a standard keyboard. You can use the methods and properties of the Keyboard class without using a constructor. The properties of the Keyboard class are constants representing the keys that are most commonly used to control games. <p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d01.html" target="_blank">Capturing keyboard input</a>
	 * </div><br><hr>
	 */
	public class Keyboard {
		/**
		 * <p> Constant associated with the key code value for the A key (65). </p>
		 */
		public static const A:uint = 65;
		/**
		 * <p> Constant associated with the key code value for the Alternate (Option) key (18). </p>
		 */
		public static const ALTERNATE:uint = 18;
		/**
		 * <p> Constant associated with the key code value for the button for selecting the audio mode. </p>
		 * <p>This constant is supported on AIR for TV. On other platforms, its value is <code>undefined</code>.</p>
		 */
		public static const AUDIO:uint = 0x01000017;
		/**
		 * <p> Constant associated with the key code value for the B key (66). </p>
		 */
		public static const B:uint = 66;
		/**
		 * <p> Constant associated with the key code value for the button for returning to the previous page in the application. </p>
		 * <p>This constant is supported on AIR for TV. On other platforms, its value is <code>undefined</code>.</p>
		 */
		public static const BACK:uint = 0x01000016;
		/**
		 * <p> Constant associated with the key code value for the ` key (192). </p>
		 */
		public static const BACKQUOTE:uint = 192;
		/**
		 * <p> Constant associated with the key code value for the \ key (220). </p>
		 */
		public static const BACKSLASH:uint = 220;
		/**
		 * <p> Constant associated with the key code value for the Backspace key (8). </p>
		 */
		public static const BACKSPACE:uint = 8;
		/**
		 * <p> Constant associated with the key code value for the blue function key button. </p>
		 * <p>This constant is supported on AIR for TV. On other platforms, its value is <code>undefined</code>.</p>
		 */
		public static const BLUE:uint = 0x01000003;
		/**
		 * <p> Constant associated with the key code value for the C key (67). </p>
		 */
		public static const C:uint = 67;
		/**
		 * <p> Constant associated with the key code value for the Caps Lock key (20). </p>
		 */
		public static const CAPS_LOCK:uint = 20;
		/**
		 * <p> Constant associated with the key code value for the channel down button. </p>
		 * <p>This constant is supported on AIR for TV. On other platforms, its value is <code>undefined</code>.</p>
		 */
		public static const CHANNEL_DOWN:uint = 0x01000005;
		/**
		 * <p> Constant associated with the key code value for the channel up button. </p>
		 * <p>This constant is supported on AIR for TV. On other platforms, its value is <code>undefined</code>.</p>
		 */
		public static const CHANNEL_UP:uint = 0x01000004;
		/**
		 * <p> Constant associated with the key code value for the , key (188). </p>
		 */
		public static const COMMA:uint = 188;
		/**
		 * <p> Constant associated with the Mac command key (15). This constant is currently only used for setting menu key equivalents. </p>
		 */
		public static const COMMAND:uint = 15;
		/**
		 * <p> Constant associated with the key code value for the Control key (17). </p>
		 */
		public static const CONTROL:uint = 17;
		/**
		 * <p> An array containing all the defined key name constants. </p>
		 */
		public static const CharCodeStrings:Array = [];
		/**
		 * <p> Constant associated with the key code value for the D key (68). </p>
		 */
		public static const D:uint = 68;
		/**
		 * <p> Constant associated with the key code value for the Delete key (46). </p>
		 */
		public static const DELETE:uint = 46;
		/**
		 * <p> Constant associated with the key code value for the Down Arrow key (40). </p>
		 */
		public static const DOWN:uint = 40;
		/**
		 * <p> Constant associated with the key code value for the button for engaging DVR application mode. </p>
		 * <p>This constant is supported on AIR for TV. On other platforms, its value is <code>undefined</code>.</p>
		 */
		public static const DVR:uint = 0x01000019;
		/**
		 * <p> Constant associated with the key code value for the E key (69). </p>
		 */
		public static const E:uint = 69;
		/**
		 * <p> Constant associated with the key code value for the End key (35). </p>
		 */
		public static const END:uint = 35;
		/**
		 * <p> Constant associated with the key code value for the Enter key (13). </p>
		 */
		public static const ENTER:uint = 13;
		/**
		 * <p> Constant associated with the key code value for the = key (187). </p>
		 */
		public static const EQUAL:uint = 187;
		/**
		 * <p> Constant associated with the key code value for the Escape key (27). </p>
		 */
		public static const ESCAPE:uint = 27;
		/**
		 * <p> Constant associated with the key code value for the button for exiting the current application mode. </p>
		 * <p>This constant is supported on AIR for TV. On other platforms, its value is <code>undefined</code>.</p>
		 */
		public static const EXIT:uint = 0x01000015;
		/**
		 * <p> Constant associated with the key code value for the F key (70). </p>
		 */
		public static const F:uint = 70;
		/**
		 * <p> Constant associated with the key code value for the F1 key (112). </p>
		 */
		public static const F1:uint = 112;
		/**
		 * <p> Constant associated with the key code value for the F10 key (121). </p>
		 */
		public static const F10:uint = 121;
		/**
		 * <p> Constant associated with the key code value for the F11 key (122). </p>
		 */
		public static const F11:uint = 122;
		/**
		 * <p> Constant associated with the key code value for the F12 key (123). </p>
		 */
		public static const F12:uint = 123;
		/**
		 * <p> Constant associated with the key code value for the F13 key (124). </p>
		 */
		public static const F13:uint = 124;
		/**
		 * <p> Constant associated with the key code value for the F14 key (125). </p>
		 */
		public static const F14:uint = 125;
		/**
		 * <p> Constant associated with the key code value for the F15 key (126). </p>
		 */
		public static const F15:uint = 126;
		/**
		 * <p> Constant associated with the key code value for the F2 key (113). </p>
		 */
		public static const F2:uint = 113;
		/**
		 * <p> Constant associated with the key code value for the F3 key (114). </p>
		 */
		public static const F3:uint = 114;
		/**
		 * <p> Constant associated with the key code value for the F4 key (115). </p>
		 */
		public static const F4:uint = 115;
		/**
		 * <p> Constant associated with the key code value for the F5 key (116). </p>
		 */
		public static const F5:uint = 116;
		/**
		 * <p> Constant associated with the key code value for the F6 key (117). </p>
		 */
		public static const F6:uint = 117;
		/**
		 * <p> Constant associated with the key code value for the F7 key (118). </p>
		 */
		public static const F7:uint = 118;
		/**
		 * <p> Constant associated with the key code value for the F8 key (119). </p>
		 */
		public static const F8:uint = 119;
		/**
		 * <p> Constant associated with the key code value for the F9 key (120). </p>
		 */
		public static const F9:uint = 120;
		/**
		 * <p> Constant associated with the key code value for the button for engaging fast-forward transport mode. </p>
		 * <p>This constant is supported on AIR for TV. On other platforms, its value is <code>undefined</code>.</p>
		 */
		public static const FAST_FORWARD:uint = 0x0100000A;
		/**
		 * <p> Constant associated with the key code value for the G key (71). </p>
		 */
		public static const G:uint = 71;
		/**
		 * <p> Constant associated with the key code value for the green function key button. </p>
		 * <p>This constant is supported on AIR for TV. On other platforms, its value is <code>undefined</code>.</p>
		 */
		public static const GREEN:uint = 0x01000001;
		/**
		 * <p> Constant associated with the key code value for the button for engaging the program guide. </p>
		 * <p>This constant is supported on AIR for TV. On other platforms, its value is <code>undefined</code>.</p>
		 */
		public static const GUIDE:uint = 0x01000014;
		/**
		 * <p> Constant associated with the key code value for the H key (72). </p>
		 */
		public static const H:uint = 72;
		/**
		 * <p> Constant associated with the key code value for the button for engaging the help application or context-sensitive help. </p>
		 * <p>This constant is supported on AIR for TV. On other platforms, its value is <code>undefined</code>.</p>
		 */
		public static const HELP:uint = 0x0100001D;
		/**
		 * <p> Constant associated with the key code value for the Home key (36). </p>
		 */
		public static const HOME:uint = 36;
		/**
		 * <p> Constant associated with the key code value for the I key (73). </p>
		 */
		public static const I:uint = 73;
		/**
		 * <p> Constant associated with the key code value for the info button. </p>
		 * <p>This constant is supported on AIR for TV. On other platforms, its value is <code>undefined</code>.</p>
		 */
		public static const INFO:uint = 0x01000013;
		/**
		 * <p> Constant associated with the key code value for the button for cycling inputs. </p>
		 * <p>This constant is supported on AIR for TV. On other platforms, its value is <code>undefined</code>.</p>
		 */
		public static const INPUT:uint = 0x0100001B;
		/**
		 * <p> Constant associated with the key code value for the Insert key (45). </p>
		 */
		public static const INSERT:uint = 45;
		/**
		 * <p> Constant associated with the key code value for the J key (74). </p>
		 */
		public static const J:uint = 74;
		/**
		 * <p> Constant associated with the key code value for the K key (75). </p>
		 */
		public static const K:uint = 75;
		/**
		 * <p> The Begin key </p>
		 */
		public static const KEYNAME_BEGIN:String = "Begin";
		/**
		 * <p> The Break key </p>
		 */
		public static const KEYNAME_BREAK:String = "Break";
		/**
		 * <p> The Clear Display key </p>
		 */
		public static const KEYNAME_CLEARDISPLAY:String = "ClrDsp";
		/**
		 * <p> The Clear Line key </p>
		 */
		public static const KEYNAME_CLEARLINE:String = "ClrLn";
		/**
		 * <p> The Delete key </p>
		 */
		public static const KEYNAME_DELETE:String = "Delete";
		/**
		 * <p> The Delete Character key </p>
		 */
		public static const KEYNAME_DELETECHAR:String = "DelChr";
		/**
		 * <p> The Delete Line key </p>
		 */
		public static const KEYNAME_DELETELINE:String = "DelLn";
		/**
		 * <p> The down arrow </p>
		 */
		public static const KEYNAME_DOWNARROW:String = "Down";
		/**
		 * <p> The End key </p>
		 */
		public static const KEYNAME_END:String = "End";
		/**
		 * <p> The Execute key </p>
		 */
		public static const KEYNAME_EXECUTE:String = "Exec";
		/**
		 * <p> The F1 key </p>
		 */
		public static const KEYNAME_F1:String = "F1";
		/**
		 * <p> The F10 key </p>
		 */
		public static const KEYNAME_F10:String = "F10";
		/**
		 * <p> The F11 key </p>
		 */
		public static const KEYNAME_F11:String = "F11";
		/**
		 * <p> The F12 key </p>
		 */
		public static const KEYNAME_F12:String = "F12";
		/**
		 * <p> The F13 key </p>
		 */
		public static const KEYNAME_F13:String = "F13";
		/**
		 * <p> The F14 key </p>
		 */
		public static const KEYNAME_F14:String = "F14";
		/**
		 * <p> The F15 key </p>
		 */
		public static const KEYNAME_F15:String = "F15";
		/**
		 * <p> The F16 key </p>
		 */
		public static const KEYNAME_F16:String = "F16";
		/**
		 * <p> The F17 key </p>
		 */
		public static const KEYNAME_F17:String = "F17";
		/**
		 * <p> The F18 key </p>
		 */
		public static const KEYNAME_F18:String = "F18";
		/**
		 * <p> The F19 key </p>
		 */
		public static const KEYNAME_F19:String = "F19";
		/**
		 * <p> The F2 key </p>
		 */
		public static const KEYNAME_F2:String = "F2";
		/**
		 * <p> The F20 key </p>
		 */
		public static const KEYNAME_F20:String = "F20";
		/**
		 * <p> The F21 key </p>
		 */
		public static const KEYNAME_F21:String = "F21";
		/**
		 * <p> The F22 key </p>
		 */
		public static const KEYNAME_F22:String = "F22";
		/**
		 * <p> The F23 key </p>
		 */
		public static const KEYNAME_F23:String = "F23";
		/**
		 * <p> The F24 key </p>
		 */
		public static const KEYNAME_F24:String = "F24";
		/**
		 * <p> The F25 key </p>
		 */
		public static const KEYNAME_F25:String = "F25";
		/**
		 * <p> The F26 key </p>
		 */
		public static const KEYNAME_F26:String = "F26";
		/**
		 * <p> The F27 key </p>
		 */
		public static const KEYNAME_F27:String = "F27";
		/**
		 * <p> The F28 key </p>
		 */
		public static const KEYNAME_F28:String = "F28";
		/**
		 * <p> The F29 key </p>
		 */
		public static const KEYNAME_F29:String = "F29";
		/**
		 * <p> The F3 key </p>
		 */
		public static const KEYNAME_F3:String = "F3";
		/**
		 * <p> </p>
		 */
		public static const KEYNAME_F30:String = "F30";
		/**
		 * <p> The F31 key </p>
		 */
		public static const KEYNAME_F31:String = "F31";
		/**
		 * <p> The F32 key </p>
		 */
		public static const KEYNAME_F32:String = "F32";
		/**
		 * <p> The F33 key </p>
		 */
		public static const KEYNAME_F33:String = "F33";
		/**
		 * <p> The F34 key </p>
		 */
		public static const KEYNAME_F34:String = "F34";
		/**
		 * <p> The F35 key </p>
		 */
		public static const KEYNAME_F35:String = "F35";
		/**
		 * <p> The F4 key </p>
		 */
		public static const KEYNAME_F4:String = "F4";
		/**
		 * <p> The F5 key </p>
		 */
		public static const KEYNAME_F5:String = "F5";
		/**
		 * <p> The F6 key </p>
		 */
		public static const KEYNAME_F6:String = "F6";
		/**
		 * <p> The F7 key </p>
		 */
		public static const KEYNAME_F7:String = "F7";
		/**
		 * <p> The F8 key </p>
		 */
		public static const KEYNAME_F8:String = "F8";
		/**
		 * <p> The F9 key </p>
		 */
		public static const KEYNAME_F9:String = "F9";
		/**
		 * <p> The Find key </p>
		 */
		public static const KEYNAME_FIND:String = "Find";
		/**
		 * <p> The Help key </p>
		 */
		public static const KEYNAME_HELP:String = "Help";
		/**
		 * <p> The Home key </p>
		 */
		public static const KEYNAME_HOME:String = "Home";
		/**
		 * <p> The Insert key </p>
		 */
		public static const KEYNAME_INSERT:String = "Insert";
		/**
		 * <p> The Insert Character key </p>
		 */
		public static const KEYNAME_INSERTCHAR:String = "InsChr";
		/**
		 * <p> The Insert Line key </p>
		 */
		public static const KEYNAME_INSERTLINE:String = "InsLn";
		/**
		 * <p> The left arrow </p>
		 */
		public static const KEYNAME_LEFTARROW:String = "Left";
		/**
		 * <p> The Menu key </p>
		 */
		public static const KEYNAME_MENU:String = "Menu";
		/**
		 * <p> The Mode Switch key </p>
		 */
		public static const KEYNAME_MODESWITCH:String = "ModeSw";
		/**
		 * <p> The Next key </p>
		 */
		public static const KEYNAME_NEXT:String = "Next";
		/**
		 * <p> The Page Down key </p>
		 */
		public static const KEYNAME_PAGEDOWN:String = "PgDn";
		/**
		 * <p> The Page Up key </p>
		 */
		public static const KEYNAME_PAGEUP:String = "PgUp";
		/**
		 * <p> The Pause key </p>
		 */
		public static const KEYNAME_PAUSE:String = "Pause";
		/**
		 * <p> The Play_Pause key </p>
		 */
		public static const KEYNAME_PLAYPAUSE:String = "PlayPause";
		/**
		 * <p> The Previous key </p>
		 */
		public static const KEYNAME_PREV:String = "Prev";
		/**
		 * <p> The Print key </p>
		 */
		public static const KEYNAME_PRINT:String = "Print";
		/**
		 * <p> The Print Screen </p>
		 */
		public static const KEYNAME_PRINTSCREEN:String = "PrntScrn";
		/**
		 * <p> The Redo key </p>
		 */
		public static const KEYNAME_REDO:String = "Redo";
		/**
		 * <p> The Reset key </p>
		 */
		public static const KEYNAME_RESET:String = "Reset";
		/**
		 * <p> The right arrow </p>
		 */
		public static const KEYNAME_RIGHTARROW:String = "Right";
		/**
		 * <p> The Scroll Lock key </p>
		 */
		public static const KEYNAME_SCROLLLOCK:String = "ScrlLck";
		/**
		 * <p> The Select key </p>
		 */
		public static const KEYNAME_SELECT:String = "Select";
		/**
		 * <p> The Stop key </p>
		 */
		public static const KEYNAME_STOP:String = "Stop";
		/**
		 * <p> The System Request key </p>
		 */
		public static const KEYNAME_SYSREQ:String = "SysReq";
		/**
		 * <p> The System key </p>
		 */
		public static const KEYNAME_SYSTEM:String = "Sys";
		/**
		 * <p> The Undo key </p>
		 */
		public static const KEYNAME_UNDO:String = "Undo";
		/**
		 * <p> The up arrow </p>
		 */
		public static const KEYNAME_UPARROW:String = "Up";
		/**
		 * <p> The User key </p>
		 */
		public static const KEYNAME_USER:String = "User";
		/**
		 * <p> Constant associated with the key code value for the L key (76). </p>
		 */
		public static const L:uint = 76;
		/**
		 * <p> Constant associated with the key code value for the button for watching the last channel or show watched. </p>
		 * <p>This constant is supported on AIR for TV. On other platforms, its value is <code>undefined</code>.</p>
		 */
		public static const LAST:uint = 0x01000011;
		/**
		 * <p> Constant associated with the key code value for the Left Arrow key (37). </p>
		 */
		public static const LEFT:uint = 37;
		/**
		 * <p> Constant associated with the key code value for the [ key (219). </p>
		 */
		public static const LEFTBRACKET:uint = 219;
		/**
		 * <p> Constant associated with the key code value for the button for returning to live [position in broadcast]. </p>
		 * <p>This constant is supported on AIR for TV. On other platforms, its value is <code>undefined</code>.</p>
		 */
		public static const LIVE:uint = 0x01000010;
		/**
		 * <p> Constant associated with the key code value for the M key (77). </p>
		 */
		public static const M:uint = 77;
		/**
		 * <p> Constant associated with the key code value for the button for engaging the "Master Shell" (e.g. TiVo or other vendor button). </p>
		 * <p>This constant is supported on AIR for TV. On other platforms, its value is <code>undefined</code>.</p>
		 */
		public static const MASTER_SHELL:uint = 0x0100001E;
		/**
		 * <p> Constant associated with the key code value for the button for engaging the menu. </p>
		 * <p>This constant is supported on AIR for TV. On other platforms, its value is <code>undefined</code>.</p>
		 */
		public static const MENU:uint = 0x01000012;
		/**
		 * <p> Constant associated with the key code value for the - key (189). </p>
		 */
		public static const MINUS:uint = 189;
		/**
		 * <p> Constant associated with the key code value for the N key (78). </p>
		 */
		public static const N:uint = 78;
		/**
		 * <p> Constant associated with the key code value for the button for skipping to next track or chapter. </p>
		 * <p>This constant is supported on AIR for TV. On other platforms, its value is <code>undefined</code>.</p>
		 */
		public static const NEXT:uint = 0x0100000E;
		/**
		 * <p> Constant associated with the key code value for the 0 key (48). </p>
		 */
		public static const NUMBER_0:uint = 48;
		/**
		 * <p> Constant associated with the key code value for the 1 key (49). </p>
		 */
		public static const NUMBER_1:uint = 49;
		/**
		 * <p> Constant associated with the key code value for the 2 key (50). </p>
		 */
		public static const NUMBER_2:uint = 50;
		/**
		 * <p> Constant associated with the key code value for the 3 key (51). </p>
		 */
		public static const NUMBER_3:uint = 51;
		/**
		 * <p> Constant associated with the key code value for the 4 key (52). </p>
		 */
		public static const NUMBER_4:uint = 52;
		/**
		 * <p> Constant associated with the key code value for the 5 key (53). </p>
		 */
		public static const NUMBER_5:uint = 53;
		/**
		 * <p> Constant associated with the key code value for the 6 key (54). </p>
		 */
		public static const NUMBER_6:uint = 54;
		/**
		 * <p> Constant associated with the key code value for the 7 key (55). </p>
		 */
		public static const NUMBER_7:uint = 55;
		/**
		 * <p> Constant associated with the key code value for the 8 key (56). </p>
		 */
		public static const NUMBER_8:uint = 56;
		/**
		 * <p> Constant associated with the key code value for the 9 key (57). </p>
		 */
		public static const NUMBER_9:uint = 57;
		/**
		 * <p> Constant associated with the pseudo-key code for the the number pad (21). Use to set numpad modifier on key equivalents </p>
		 */
		public static const NUMPAD:uint = 21;
		/**
		 * <p> Constant associated with the key code value for the number 0 key on the number pad (96). </p>
		 */
		public static const NUMPAD_0:uint = 96;
		/**
		 * <p> Constant associated with the key code value for the number 1 key on the number pad (97). </p>
		 */
		public static const NUMPAD_1:uint = 97;
		/**
		 * <p> Constant associated with the key code value for the number 2 key on the number pad (98). </p>
		 */
		public static const NUMPAD_2:uint = 98;
		/**
		 * <p> Constant associated with the key code value for the number 3 key on the number pad (99). </p>
		 */
		public static const NUMPAD_3:uint = 99;
		/**
		 * <p> Constant associated with the key code value for the number 4 key on the number pad (100). </p>
		 */
		public static const NUMPAD_4:uint = 100;
		/**
		 * <p> Constant associated with the key code value for the number 5 key on the number pad (101). </p>
		 */
		public static const NUMPAD_5:uint = 101;
		/**
		 * <p> Constant associated with the key code value for the number 6 key on the number pad (102). </p>
		 */
		public static const NUMPAD_6:uint = 102;
		/**
		 * <p> Constant associated with the key code value for the number 7 key on the number pad (103). </p>
		 */
		public static const NUMPAD_7:uint = 103;
		/**
		 * <p> Constant associated with the key code value for the number 8 key on the number pad (104). </p>
		 */
		public static const NUMPAD_8:uint = 104;
		/**
		 * <p> Constant associated with the key code value for the number 9 key on the number pad (105). </p>
		 */
		public static const NUMPAD_9:uint = 105;
		/**
		 * <p> Constant associated with the key code value for the addition key on the number pad (107). </p>
		 */
		public static const NUMPAD_ADD:uint = 107;
		/**
		 * <p> Constant associated with the key code value for the decimal key on the number pad (110). </p>
		 */
		public static const NUMPAD_DECIMAL:uint = 110;
		/**
		 * <p> Constant associated with the key code value for the division key on the number pad (111). </p>
		 */
		public static const NUMPAD_DIVIDE:uint = 111;
		/**
		 * <p> Constant associated with the key code value for the Enter key on the number pad (108). </p>
		 */
		public static const NUMPAD_ENTER:uint = 108;
		/**
		 * <p> Constant associated with the key code value for the multiplication key on the number pad (106). </p>
		 */
		public static const NUMPAD_MULTIPLY:uint = 106;
		/**
		 * <p> Constant associated with the key code value for the subtraction key on the number pad (109). </p>
		 */
		public static const NUMPAD_SUBTRACT:uint = 109;
		/**
		 * <p> Constant associated with the key code value for the O key (79). </p>
		 */
		public static const O:uint = 79;
		/**
		 * <p> Constant associated with the key code value for the P key (80). </p>
		 */
		public static const P:uint = 80;
		/**
		 * <p> Constant associated with the key code value for the Page Down key (34). </p>
		 */
		public static const PAGE_DOWN:uint = 34;
		/**
		 * <p> Constant associated with the key code value for the Page Up key (33). </p>
		 */
		public static const PAGE_UP:uint = 33;
		/**
		 * <p> Constant associated with the key code value for the button for pausing transport mode. </p>
		 * <p>This constant is supported on AIR for TV. On other platforms, its value is <code>undefined</code>.</p>
		 */
		public static const PAUSE:uint = 0x01000008;
		/**
		 * <p> Constant associated with the key code value for the . key (190). </p>
		 */
		public static const PERIOD:uint = 190;
		/**
		 * <p> Constant associated with the key code value for the button for engaging play transport mode. </p>
		 * <p>This constant is supported on AIR for TV. On other platforms, its value is <code>undefined</code>.</p>
		 */
		public static const PLAY:uint = 0x01000007;
		/**
		 * <p> Constant associated with the key code value for the button for engaging play/pause transport mode. </p>
		 * <p>This constant is supported on AIR for TV. On other platforms, its value is <code>undefined</code>.</p>
		 */
		public static const PLAY_PAUSE:uint = 0x01000020;
		/**
		 * <p> Constant associated with the key code value for the button for skipping to previous track or chapter. </p>
		 * <p>This constant is supported on AIR for TV. On other platforms, its value is <code>undefined</code>.</p>
		 */
		public static const PREVIOUS:uint = 0x0100000F;
		/**
		 * <p> Constant associated with the key code value for the Q key (81). </p>
		 */
		public static const Q:uint = 81;
		/**
		 * <p> Constant associated with the key code value for the ' key (222). </p>
		 */
		public static const QUOTE:uint = 222;
		/**
		 * <p> Constant associated with the key code value for the R key (82). </p>
		 */
		public static const R:uint = 82;
		/**
		 * <p> Constant associated with the key code value for the button for recording or engaging record transport mode. </p>
		 * <p>This constant is supported on AIR for TV. On other platforms, its value is <code>undefined</code>.</p>
		 */
		public static const RECORD:uint = 0x01000006;
		/**
		 * <p> Red function key button. </p>
		 * <p>This constant is supported on AIR for TV. On other platforms, its value is <code>undefined</code>.</p>
		 */
		public static const RED:uint = 0x01000000;
		/**
		 * <p> Constant associated with the key code value for the button for engaging rewind transport mode. </p>
		 * <p>This constant is supported on AIR for TV. On other platforms, its value is <code>undefined</code>.</p>
		 */
		public static const REWIND:uint = 0x0100000B;
		/**
		 * <p> Constant associated with the key code value for the Right Arrow key (39). </p>
		 */
		public static const RIGHT:uint = 39;
		/**
		 * <p> Constant associated with the key code value for the ] key (221). </p>
		 */
		public static const RIGHTBRACKET:uint = 221;
		/**
		 * <p> Constant associated with the key code value for the S key (83). </p>
		 */
		public static const S:uint = 83;
		/**
		 * <p> Constant associated with the key code value for the button for the search button. </p>
		 * <p>This constant is supported on AIR for TV. On other platforms, its value is <code>undefined</code>.</p>
		 */
		public static const SEARCH:uint = 0x0100001F;
		/**
		 * <p> Constant associated with the key code value for the ; key (186). </p>
		 */
		public static const SEMICOLON:uint = 186;
		/**
		 * <p> Constant associated with the key code value for the button for engaging the setup application or menu. </p>
		 * <p>This constant is supported on AIR for TV. On other platforms, its value is <code>undefined</code>.</p>
		 */
		public static const SETUP:uint = 0x0100001C;
		/**
		 * <p> Constant associated with the key code value for the Shift key (16). </p>
		 */
		public static const SHIFT:uint = 16;
		/**
		 * <p> Constant associated with the key code value for the button for engaging quick skip backward (usually 7-10 seconds). </p>
		 * <p>This constant is supported on AIR for TV. On other platforms, its value is <code>undefined</code>.</p>
		 */
		public static const SKIP_BACKWARD:uint = 0x0100000D;
		/**
		 * <p> Constant associated with the key code value for the button for engaging quick skip ahead (usually 30 seconds). </p>
		 * <p>This constant is supported on AIR for TV. On other platforms, its value is <code>undefined</code>.</p>
		 */
		public static const SKIP_FORWARD:uint = 0x0100000C;
		/**
		 * <p> Constant associated with the key code value for the / key (191). </p>
		 */
		public static const SLASH:uint = 191;
		/**
		 * <p> Constant associated with the key code value for the Spacebar (32). </p>
		 */
		public static const SPACE:uint = 32;
		/**
		 * <p> Constant associated with the key code value for the button for stopping transport mode. </p>
		 * <p>This constant is supported on AIR for TV. On other platforms, its value is <code>undefined</code>.</p>
		 */
		public static const STOP:uint = 0x01000009;
		/**
		 * <p> The OS X Unicode Begin constant </p>
		 */
		public static const STRING_BEGIN:String = "";
		/**
		 * <p> The OS X Unicode Break constant </p>
		 */
		public static const STRING_BREAK:String = "";
		/**
		 * <p> The OS X Unicode Clear Display constant </p>
		 */
		public static const STRING_CLEARDISPLAY:String = "";
		/**
		 * <p> The OS X Unicode Clear Line constant </p>
		 */
		public static const STRING_CLEARLINE:String = "";
		/**
		 * <p> The OS X Unicode Delete constant </p>
		 */
		public static const STRING_DELETE:String = "";
		/**
		 * <p> The OS X Unicode Delete Character constant </p>
		 */
		public static const STRING_DELETECHAR:String = "";
		/**
		 * <p> The OS X Unicode Delete Line constant </p>
		 */
		public static const STRING_DELETELINE:String = "";
		/**
		 * <p> The OS X Unicode down arrow constant </p>
		 */
		public static const STRING_DOWNARROW:String = "";
		/**
		 * <p> The OS X Unicode End constant </p>
		 */
		public static const STRING_END:String = "";
		/**
		 * <p> The OS X Unicode Execute constant </p>
		 */
		public static const STRING_EXECUTE:String = "";
		/**
		 * <p> The OS X Unicode F1 constant </p>
		 */
		public static const STRING_F1:String = "";
		/**
		 * <p> The OS X Unicode F10 constant </p>
		 */
		public static const STRING_F10:String = "";
		/**
		 * <p> The OS X Unicode F11 constant </p>
		 */
		public static const STRING_F11:String = "";
		/**
		 * <p> The OS X Unicode F12 constant </p>
		 */
		public static const STRING_F12:String = "";
		/**
		 * <p> The OS X Unicode F13 constant </p>
		 */
		public static const STRING_F13:String = "";
		/**
		 * <p> The OS X Unicode F14 constant </p>
		 */
		public static const STRING_F14:String = "";
		/**
		 * <p> The OS X Unicode F15 constant </p>
		 */
		public static const STRING_F15:String = "";
		/**
		 * <p> The OS X Unicode F16 constant </p>
		 */
		public static const STRING_F16:String = "";
		/**
		 * <p> The OS X Unicode F17 constant </p>
		 */
		public static const STRING_F17:String = "";
		/**
		 * <p> The OS X Unicode F18 constant </p>
		 */
		public static const STRING_F18:String = "";
		/**
		 * <p> The OS X Unicode F19 constant </p>
		 */
		public static const STRING_F19:String = "";
		/**
		 * <p> The OS X Unicode F2 constant </p>
		 */
		public static const STRING_F2:String = "";
		/**
		 * <p> The OS X Unicode F20 constant </p>
		 */
		public static const STRING_F20:String = "";
		/**
		 * <p> The OS X Unicode F21 constant </p>
		 */
		public static const STRING_F21:String = "";
		/**
		 * <p> The OS X Unicode F22 constant </p>
		 */
		public static const STRING_F22:String = "";
		/**
		 * <p> The OS X Unicode F23 constant </p>
		 */
		public static const STRING_F23:String = "";
		/**
		 * <p> The OS X Unicode F24 constant </p>
		 */
		public static const STRING_F24:String = "";
		/**
		 * <p> The OS X Unicode F25 constant </p>
		 */
		public static const STRING_F25:String = "";
		/**
		 * <p> The OS X Unicode F26 constant </p>
		 */
		public static const STRING_F26:String = "";
		/**
		 * <p> The OS X Unicode F27 constant </p>
		 */
		public static const STRING_F27:String = "";
		/**
		 * <p> The OS X Unicode F28 constant </p>
		 */
		public static const STRING_F28:String = "";
		/**
		 * <p> The OS X Unicode F29 constant </p>
		 */
		public static const STRING_F29:String = "";
		/**
		 * <p> The OS X Unicode F3 constant </p>
		 */
		public static const STRING_F3:String = "";
		/**
		 * <p> The OS X Unicode F30 constant </p>
		 */
		public static const STRING_F30:String = "";
		/**
		 * <p> The OS X Unicode F31 constant </p>
		 */
		public static const STRING_F31:String = "";
		/**
		 * <p> The OS X Unicode F32 constant </p>
		 */
		public static const STRING_F32:String = "";
		/**
		 * <p> The OS X Unicode F33 constant </p>
		 */
		public static const STRING_F33:String = "";
		/**
		 * <p> The OS X Unicode F34 constant </p>
		 */
		public static const STRING_F34:String = "";
		/**
		 * <p> The OS X Unicode F35 constant </p>
		 */
		public static const STRING_F35:String = "";
		/**
		 * <p> The OS X Unicode F4 constant </p>
		 */
		public static const STRING_F4:String = "";
		/**
		 * <p> The OS X Unicode F5 constant </p>
		 */
		public static const STRING_F5:String = "";
		/**
		 * <p> The OS X Unicode F6 constant </p>
		 */
		public static const STRING_F6:String = "";
		/**
		 * <p> The OS X Unicode F7 constant </p>
		 */
		public static const STRING_F7:String = "";
		/**
		 * <p> The OS X Unicode F8 constant </p>
		 */
		public static const STRING_F8:String = "";
		/**
		 * <p> The OS X Unicode F9 constant </p>
		 */
		public static const STRING_F9:String = "";
		/**
		 * <p> The OS X Unicode Find constant </p>
		 */
		public static const STRING_FIND:String = "";
		/**
		 * <p> The OS X Unicode Help constant </p>
		 */
		public static const STRING_HELP:String = "";
		/**
		 * <p> The OS X Unicode Home constant </p>
		 */
		public static const STRING_HOME:String = "";
		/**
		 * <p> The OS X Unicode Insert constant </p>
		 */
		public static const STRING_INSERT:String = "";
		/**
		 * <p> The OS X Unicode Insert Character constant </p>
		 */
		public static const STRING_INSERTCHAR:String = "";
		/**
		 * <p> The OS X Unicode Insert Line constant </p>
		 */
		public static const STRING_INSERTLINE:String = "";
		/**
		 * <p> The OS X Unicode left arrow constant </p>
		 */
		public static const STRING_LEFTARROW:String = "";
		/**
		 * <p> The OS X Unicode Menu constant </p>
		 */
		public static const STRING_MENU:String = "";
		/**
		 * <p> The OS X Unicode Mode Switch constant </p>
		 */
		public static const STRING_MODESWITCH:String = "";
		/**
		 * <p> The OS X Unicode Next constant </p>
		 */
		public static const STRING_NEXT:String = "";
		/**
		 * <p> The OS X Unicode Page Down constant </p>
		 */
		public static const STRING_PAGEDOWN:String = "";
		/**
		 * <p> The OS X Unicode Page Up constant </p>
		 */
		public static const STRING_PAGEUP:String = "";
		/**
		 * <p> The OS X Unicode Pause constant </p>
		 */
		public static const STRING_PAUSE:String = "";
		/**
		 * <p> The OS X Unicode Previous constant </p>
		 */
		public static const STRING_PREV:String = "";
		/**
		 * <p> The OS X Unicode Print constant </p>
		 */
		public static const STRING_PRINT:String = "";
		/**
		 * <p> The OS X Unicode Print Screen constant </p>
		 */
		public static const STRING_PRINTSCREEN:String = "";
		/**
		 * <p> The OS X Unicode Redo constant </p>
		 */
		public static const STRING_REDO:String = "";
		/**
		 * <p> The OS X Unicode Reset constant </p>
		 */
		public static const STRING_RESET:String = "";
		/**
		 * <p> The OS X Unicode right arrow constant </p>
		 */
		public static const STRING_RIGHTARROW:String = "";
		/**
		 * <p> The OS X Unicode Scroll Lock constant </p>
		 */
		public static const STRING_SCROLLLOCK:String = "";
		/**
		 * <p> The OS X Unicode Select constant </p>
		 */
		public static const STRING_SELECT:String = "";
		/**
		 * <p> The OS X Unicode Stop constant </p>
		 */
		public static const STRING_STOP:String = "";
		/**
		 * <p> The OS X Unicode System Request constant </p>
		 */
		public static const STRING_SYSREQ:String = "";
		/**
		 * <p> The OS X Unicode System constant </p>
		 */
		public static const STRING_SYSTEM:String = "";
		/**
		 * <p> The OS X Unicode Undo constant </p>
		 */
		public static const STRING_UNDO:String = "";
		/**
		 * <p> The OS X Unicode up arrow constant </p>
		 */
		public static const STRING_UPARROW:String = "";
		/**
		 * <p> The OS X Unicode User constant </p>
		 */
		public static const STRING_USER:String = "";
		/**
		 * <p> Constant associated with the key code value for the button for toggling subtitles. </p>
		 * <p>This constant is supported on AIR for TV. On other platforms, its value is <code>undefined</code>.</p>
		 */
		public static const SUBTITLE:uint = 0x01000018;
		/**
		 * <p> Constant associated with the key code value for the T key (84). </p>
		 */
		public static const T:uint = 84;
		/**
		 * <p> Constant associated with the key code value for the Tab key (9). </p>
		 */
		public static const TAB:uint = 9;
		/**
		 * <p> Constant associated with the key code value for the U key (85). </p>
		 */
		public static const U:uint = 85;
		/**
		 * <p> Constant associated with the key code value for the Up Arrow key (38). </p>
		 */
		public static const UP:uint = 38;
		/**
		 * <p> Constant associated with the key code value for the V key (86). </p>
		 */
		public static const V:uint = 86;
		/**
		 * <p> Constant associated with the key code value for the button for engaging video-on-demand. </p>
		 * <p>This constant is supported on AIR for TV. On other platforms, its value is <code>undefined</code>.</p>
		 */
		public static const VOD:uint = 0x0100001A;
		/**
		 * <p> Constant associated with the key code value for the W key (87). </p>
		 */
		public static const W:uint = 87;
		/**
		 * <p> Constant associated with the key code value for the X key (88). </p>
		 */
		public static const X:uint = 88;
		/**
		 * <p> Constant associated with the key code value for the Y key (89). </p>
		 */
		public static const Y:uint = 89;
		/**
		 * <p> Constant associated with the key code value for the yellow function key button. </p>
		 * <p>This constant is supported on AIR for TV. On other platforms, its value is <code>undefined</code>.</p>
		 */
		public static const YELLOW:uint = 0x01000002;
		/**
		 * <p> Constant associated with the key code value for the Z key (90). </p>
		 */
		public static const Z:uint = 90;

		private static var _capsLock:Boolean;
		private static var _hasVirtualKeyboard:Boolean;
		private static var _numLock:Boolean;
		private static var _physicalKeyboardType:String;


		/**
		 * <p> Specifies whether the Caps Lock key is activated (<code>true</code>) or not (<code>false</code>). </p>
		 * 
		 * @return 
		 */
		public static function get capsLock():Boolean {
			return _capsLock;
		}


		/**
		 * <p> Indicates whether the computer or device provides a virtual keyboard. If the current environment provides a virtual keyboard, this value is <code>true</code>. </p>
		 * 
		 * @return 
		 */
		public static function get hasVirtualKeyboard():Boolean {
			return _hasVirtualKeyboard;
		}


		/**
		 * <p> Specifies whether the Num Lock key is activated (<code>true</code>) or not (<code>false</code>). </p>
		 * 
		 * @return 
		 */
		public static function get numLock():Boolean {
			return _numLock;
		}


		/**
		 * <p> Indicates the type of physical keyboard provided by the computer or device, if any. </p>
		 * <p>Use the constants defined in the KeyboardType class to test the values reported by this property.</p>
		 * <p><b>Note:</b> If a computer or device has both an alphanumeric keyboard and a 12-button keypad, this property only reports the presence of the alphanumeric keyboard.</p>
		 * 
		 * @return 
		 */
		public static function get physicalKeyboardType():String {
			return _physicalKeyboardType;
		}


		/**
		 * <p> Specifies whether the last key pressed is accessible by other SWF files. By default, security restrictions prevent code from a SWF file in one domain from accessing a keystroke generated from a SWF file in another domain. </p>
		 * 
		 * @return  — The value  if the last key pressed can be accessed. If access is not permitted, this method returns . 
		 */
		public static function isAccessible():Boolean {
			throw new Error("Not implemented");
		}
	}
}
