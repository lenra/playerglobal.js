package flash.ui {
	/**
	 *  The MouseCursor class is an enumeration of constant values used in setting the <code>cursor</code> property of the Mouse class. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Mouse.html#cursor" target="">flash.ui.Mouse.cursor</a>
	 * </div><br><hr>
	 */
	public class MouseCursor {
		/**
		 * <p> Used to specify that the arrow cursor should be used. </p>
		 */
		public static const ARROW:String = "arrow";
		/**
		 * <p> Used to specify that the cursor should be selected automatically based on the object under the mouse. </p>
		 */
		public static const AUTO:String = "auto";
		/**
		 * <p> Used to specify that the button pressing hand cursor should be used. </p>
		 */
		public static const BUTTON:String = "button";
		/**
		 * <p> Used to specify that the dragging hand cursor should be used. </p>
		 */
		public static const HAND:String = "hand";
		/**
		 * <p> Used to specify that the I-beam cursor should be used. </p>
		 */
		public static const IBEAM:String = "ibeam";
	}
}
