package flash.ui {
	/*import flash.display.NativeMenu;
	import flash.display.NativeMenuItem;*/
	import flash.display.Stage;
	import flash.events.ContextMenuEvent;
	import flash.net.URLRequest;

	[Event(name="menuSelect", type="flash.events.ContextMenuEvent")]
	/**
	 *  The ContextMenu class provides control over the items displayed in context menus. <p> <b>Mobile Browser Support:</b> This class is not supported in mobile browsers.</p> <p> <i>AIR profile support:</i> This feature is not supported on mobile devices or AIR for TV devices. See <a href="http://help.adobe.com/en_US/air/build/WS144092a96ffef7cc16ddeea2126bb46b82f-8000.html"> AIR Profile Support</a> for more information regarding API support across multiple profiles.</p> <p>In Flash Player, users open the context menu by right-clicking (Windows or Linux) or Control-clicking (Macintosh) Flash Player. You can use the methods and properties of the ContextMenu class to add custom menu items, control the display of the built-in context menu items (for example, Zoom In, and Print), or create copies of menus. In AIR, there are no built-in items and no standard context menu.</p> <p>In Flash Professional, you can attach a ContextMenu object to a specific button, movie clip, or text field object, or to an entire movie level. You use the <code>contextMenu</code> property of the InteractiveObject class to do this.</p> <p>In Flex or Flash Builder, only top-level components in the application can have context menus. For example, if a DataGrid control is a child of a TabNavigator or VBox container, the DataGrid control cannot have its own context menu.</p> <p>To add new items to a ContextMenu object, you create a ContextMenuItem object, and then add that object to the <code>ContextMenu.customItems</code> array. For more information about creating context menu items, see the ContextMenuItem class entry.</p> <p>Flash Player has three types of context menus: the standard menu (which appears when you right-click in Flash Player), the edit menu (which appears when you right-click a selectable or editable text field), and an error menu (which appears when a SWF file has failed to load into Flash Player). Only the standard and edit menus can be modified with the ContextMenu class. Only the edit menu appears in AIR.</p> <p>Custom menu items always appear at the top of the Flash Player context menu, above any visible built-in menu items; a separator bar distinguishes built-in and custom menu items. You cannot remove the Settings menu item from the context menu. The Settings menu item is required in Flash so that users can access the settings that affect privacy and storage on their computers. You also cannot remove the About menu item, which is required so that users can find out what version of Flash Player they are using. (In AIR, the built-in Settings and About menu items are not used.)</p> <p>You can add no more than 15 custom items to a context menu in Flash Player. In AIR, there is no explicit limit imposed on the number of items in a context menu.</p> <p>You must use the <code>ContextMenu()</code> constructor to create a ContextMenu object before calling its methods.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118676a48d0-8000.html" target="_blank">Working with menus</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="ContextMenuItem.html" target="">ContextMenuItem class</a>
	 *  <br>
	 *  <a href="../../flash/display/InteractiveObject.html#contextMenu" target="">flash.display.InteractiveObject.contextMenu</a>
	 * </div><br><hr>
	 */
	public class ContextMenu/* extends NativeMenu*/ {
		private static var _isSupported:Boolean;

		private var _builtInItems:ContextMenuBuiltInItems;
		private var _clipboardItems:ContextMenuClipboardItems;
		private var _clipboardMenu:Boolean;
		private var _customItems:Array;
		private var _items:Array;
		private var _link:URLRequest;

		private var _numItems:int;

		/**
		 * <p> Creates a ContextMenu object. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="ContextMenu.html#customItems" target="">ContextMenu.customItems</a>
		 *  <br>
		 *  <a href="ContextMenu.html#hideBuiltInItems()" target="">ContextMenu.hideBuiltInItems()</a>
		 * </div>
		 */
		public function ContextMenu() {
			throw new Error("Not implemented");
		}

		/**
		 * <p> An instance of the ContextMenuBuiltInItems class with the following properties: <code>forwardAndBack</code>, <code>loop</code>, <code>play</code>, <code>print</code>, <code>quality</code>, <code>rewind</code>, <code>save</code>, and <code>zoom</code>. Setting these properties to <code>false</code> removes the corresponding menu items from the specified ContextMenu object. These properties are enumerable and are set to <code>true</code> by default. </p>
		 * <p><b>Note:</b> In AIR, context menus do not have built-in items.</p>
		 * 
		 * @return 
		 */
		public function get builtInItems():ContextMenuBuiltInItems {
			return _builtInItems;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set builtInItems(value:ContextMenuBuiltInItems):void {
			_builtInItems = value;
		}

		/**
		 * <p> An instance of the ContextMenuClipboardItems class with the following properties: <code>cut</code>, <code>copy</code>, <code>paste</code>, <code>delete</code>, <code>selectAll</code>. Setting one of these properties to <code>false</code> disables the corresponding item in the clipboard menu. </p>
		 * 
		 * @return 
		 */
		public function get clipboardItems():ContextMenuClipboardItems {
			return _clipboardItems;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set clipboardItems(value:ContextMenuClipboardItems):void {
			_clipboardItems = value;
		}

		/**
		 * <p> Specifies whether or not the clipboard menu should be used. If this value is <code>true</code>, the <code>clipboardItems</code> property determines which items are enabled or disabled on the clipboard menu. </p>
		 * <p>If the <code>link</code> property is non-null, this <code>clipBoardMenu</code> property is ignored.</p>
		 * 
		 * @return 
		 */
		public function get clipboardMenu():Boolean {
			return _clipboardMenu;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set clipboardMenu(value:Boolean):void {
			_clipboardMenu = value;
		}

		/**
		 * <p> An array of ContextMenuItem objects. Each object in the array represents a context menu item that you have defined. Use this property to add, remove, or modify these custom menu items. </p>
		 * <p>To add new menu items, you create a ContextMenuItem object and then add it to the <code>customItems</code> array (for example, by using <code>Array.push()</code>). For more information about creating menu items, see the ContextMenuItem class entry.</p>
		 * 
		 * @return 
		 */
		public function get customItems():Array {
			return _customItems;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set customItems(value:Array):void {
			_customItems = value;
		}

		/**
		 * <p> The array of custom items in this menu. </p>
		 * <p>Using this property is equivalent to using the <code>customItems</code> property. The array is sorted in display order.</p>
		 * 
		 * @return 
		 */
		public function get items():Array {
			return _items;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set items(value:Array):void {
			_items = value;
		}

		/**
		 * <p> The <code>URLRequest</code> of the link. If this property is <code>null</code>, a normal context menu is displayed. If this property is not <code>null</code>, the link context menu is displayed, and operates on the url specified. </p>
		 * <p>If a <code>link</code> is specified, the <code>clipboardMenu</code> property is ignored.</p>
		 * <p>The default value is <code>null</code>.</p>
		 * 
		 * @return 
		 */
		public function get link():URLRequest {
			return _link;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set link(value:URLRequest):void {
			_link = value;
		}

		/**
		 * <p> The number of items in this menu. </p>
		 * 
		 * @return 
		 */
		public function get numItems():int {
			return _numItems;
		}

		/**
		 * <p> Adds a menu item at the bottom of the menu. </p>
		 * <p>When creating a context menu, you can add either NativeMenuItem or ContextMenuItem objects. However, it is advisable to use only one type of object in a context menu so that all items in the menu have the same properties.</p>
		 * 
		 * @param item  — The item to add at the bottom of the menu. 
		 * @param index
		 * @return 
		 */
		/*public function addItemAt(item:NativeMenuItem, index:int):NativeMenuItem {
			throw new Error("Not implemented");
		}*/

		/**
		 * <p> Creates a copy of the specified ContextMenu object. The copy inherits all the properties of the original menu object. </p>
		 * 
		 * @return  — A ContextMenu object with all the properties of the original menu object. 
		 */
		/*public function clone():NativeMenu {
			throw new Error("Not implemented");
		}*/

		/**
		 * <p> Reports whether this menu contains the specified menu item. </p>
		 * 
		 * @param item  — The item to look up. 
		 * @return  —  if  is in this menu. 
		 */
		/*public function containsItem(item:NativeMenuItem):Boolean {
			throw new Error("Not implemented");
		}*/

		/**
		 * <p> Pops up this menu at the specified location. </p>
		 * <p><b>Note:</b> In Flash Player, this method is not supported.</p>
		 * 
		 * @param stage  — The Stage object on which to display this menu. 
		 * @param stageX  — The number of horizontal pixels, relative to the origin of stage, at which to display this menu. 
		 * @param stageY  — The number of vertical pixels, relative to the origin of stage, at which to display this menu. 
		 */
		public function display(stage:Stage, stageX:Number, stageY:Number):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Gets the menu item at the specified index. </p>
		 * 
		 * @param index  — The (zero-based) position of the item to return. 
		 * @return  — The item at the specified position in the menu. 
		 */
		/*public function getItemAt(index:int):NativeMenuItem {
			throw new Error("Not implemented");
		}*/

		/**
		 * <p> Gets the position of the specified item. </p>
		 * 
		 * @param item  — The NativeMenuItem object to look up. 
		 * @return  — The (zero-based) position of the specified item in this menu or , if the item is not in this menu. 
		 */
		/*public function getItemIndex(item:NativeMenuItem):int {
			throw new Error("Not implemented");
		}*/

		/**
		 * <p> Hides all built-in menu items (except Settings) in the specified ContextMenu object. If the debugger version of Flash Player is running, the Debugging menu item appears, although it is dimmed for SWF files that do not have remote debugging enabled. </p>
		 * <p>This method hides only menu items that appear in the standard context menu; it does not affect items that appear in the edit and error menus. </p>
		 * <p>This method works by setting all the Boolean members of <code><i>my_cm</i></code><code>.builtInItems</code> to <code>false</code>. You can selectively make a built-in item visible by setting its corresponding member in <code><i>my_cm</i></code><code>.builtInItems</code> to <code>true</code>. </p>
		 * <p><b>Note:</b> In AIR, context menus do not have built-in items. Calling this method will have no effect.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="ContextMenuBuiltInItems.html" target="">ContextMenuBuiltInItems class</a>
		 *  <br>
		 *  <a href="ContextMenu.html#builtInItems" target="">ContextMenu.builtInItems</a>
		 * </div>
		 */
		public function hideBuiltInItems():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Removes all items from the menu. </p>
		 */
		public function removeAllItems():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Removes and returns the menu item at the specified index. </p>
		 * 
		 * @param index  — The (zero-based) position of the item to remove. 
		 * @return  — The NativeMenuItem object removed. 
		 */
		/*public function removeItemAt(index:int):NativeMenuItem {
			throw new Error("Not implemented");
		}*/


		/**
		 * <p> The <code>isSupported</code> property is set to <code>true</code> if the ContextMenu class is supported on the current platform, otherwise it is set to <code>false</code>. </p>
		 * 
		 * @return 
		 */
		public static function get isSupported():Boolean {
			return _isSupported;
		}
	}
}
