package flash.ui {
	/**
	 *  The methods of the Mouse class are used to hide and show the mouse pointer, or to set the pointer to a specific style. The Mouse class is a top-level class whose properties and methods you can access without using a constructor. <span src="flashonly">The pointer is visible by default, but you can hide it and implement a custom pointer. </span> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b8f499-7ff5.html" target="_blank">Customizing the mouse cursor</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7cff.html" target="_blank">Mouse input example: WordSearch </a>
	 * </div><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d00.html" target="_blank">Capturing mouse input</a>
	 *  <br>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../flash/events/MouseEvent.html" target="">flash.events.MouseEvent</a>
	 * </div><br><hr>
	 */
	public class Mouse {
		private static var _cursor:String;

		private static var _supportsCursor:Boolean;
		private static var _supportsNativeCursor:Boolean;


		/**
		 * <p> The name of the native cursor. </p>
		 * 
		 * @return 
		 */
		public static function get cursor():String {
			return _cursor;
		}


		/**
		 * @param value
		 * @return 
		 */
		public static function set cursor(value:String):void {
			_cursor = value;
		}


		/**
		 * <p> Indicates whether the computer or device displays a persistent cursor. </p>
		 * <p>The <code>supportsCursor</code> property is <code>true</code> on most desktop computers and <code>false</code> on most mobile devices.</p>
		 * <p><b>Note:</b> Mouse events can be dispatched whether or not this property is <code>true</code>. However, mouse events may behave differently depending on the physical characteristics of the pointing device.</p>
		 * 
		 * @return 
		 */
		public static function get supportsCursor():Boolean {
			return _supportsCursor;
		}


		/**
		 * <p> Indicates whether the current configuration supports native cursors. </p>
		 * 
		 * @return 
		 */
		public static function get supportsNativeCursor():Boolean {
			return _supportsNativeCursor;
		}


		/**
		 * <p> Hides the pointer. The pointer is visible by default. </p>
		 * <p><b>Note:</b> You need to call <code>Mouse.hide()</code> only once, regardless of the number of previous calls to <code>Mouse.show()</code>.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/DisplayObject.html#mouseX" target="">flash.display.DisplayObject.mouseX</a>
		 *  <br>
		 *  <a href="../../flash/display/DisplayObject.html#mouseY" target="">flash.display.DisplayObject.mouseY</a>
		 * </div>
		 */
		public static function hide():void {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Registers a native cursor under the given name, with the given data. </p>
		 * 
		 * @param name
		 * @param cursor
		 */
		public static function registerCursor(name:String, cursor:MouseCursorData):void {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Displays the pointer. The pointer is visible by default. </p>
		 * <p><b>Note:</b> You need to call <code>Mouse.show()</code> only once, regardless of the number of previous calls to <code>Mouse.hide()</code>.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/DisplayObject.html#mouseX" target="">flash.display.DisplayObject.mouseX</a>
		 *  <br>
		 *  <a href="../../flash/display/DisplayObject.html#mouseY" target="">flash.display.DisplayObject.mouseY</a>
		 * </div>
		 */
		public static function show():void {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Unregisters the native cursor with the given name. </p>
		 * 
		 * @param name
		 */
		public static function unregisterCursor(name:String):void {
			throw new Error("Not implemented");
		}
	}
}
