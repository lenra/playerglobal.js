package flash.ui {
	/**
	 *  The Multitouch class manages and provides information about the current environment's support for handling contact from user input devices, including contact that has two or more touch points (such as a user's fingers on a touch screen). When a user interacts with a device such as a mobile phone or tablet with a touch screen, the user typically touches the screen with his or her fingers or a pointing device. While there is a broad range of pointing devices, such as a mouse or a stylus, many of these devices only have a single point of contact with an application. For pointing devices with a single point of contact, user interaction events can be handled as a mouse event, or using a basic set of touch events (called "touch point" events). However, for pointing devices that have several points of contact and perform complex movement, such as the human hand, Flash runtimes support an additional set of event handling API called gesture events. The API for handling user interaction with these gesture events includes the following classes: <p> </p><ul> 
	 *  <li>flash.events.TouchEvent</li> 
	 *  <li>flash.events.GestureEvent</li> 
	 *  <li>flash.events.GesturePhase</li> 
	 *  <li>flash.events.TransformGestureEvent</li> 
	 *  <li>flash.events.PressAndTapGestureEvent</li> 
	 * </ul>  <p>Use the listed classes to write code that handles touch events. Use the Multitouch class to determine the current environment's support for touch interaction, and to manage the support of touch interaction if the current environment supports touch input.</p> <p>You cannot create a Multitouch object directly from ActionScript code. If you call <code>new Multitouch()</code>, an exception is thrown.</p> <p> <b>Note:</b> The Multitouch feature is not supported for SWF files embedded in HTML running on Mac OS.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WSb2ba3b1aad8a27b0-6ffb37601221e58cc29-8000.html" target="_blank">Touch, multitouch and gesture input</a>
	 * </div><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://www.riagora.com/2010/05/tetris-touch-api-and-android/" target="_blank">Michael Chaize: Tetris, Touch API and Android</a>
	 *  <br>
	 *  <a href="http://www.adobe.com/devnet/flash/articles/multitouch_gestures.html" target="_blank">Christian Cantrell: Multitouch and gesture support on the Flash Platform</a>
	 *  <br>
	 *  <a href="http://blog.theflashblog.com/?p=1678" target="_blank">Lee Brimelow: Flash Player 10.1 multi-touch FAQ</a>
	 *  <br>
	 *  <a href="http://my.adobe.acrobat.com/p84912063/?launcher=false&amp;fcsContent=true&amp;pbMode=normal" target="_blank">Piotr Walczyszyn: Multitouch development in Flex</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../flash/events/TouchEvent.html" target="">flash.events.TouchEvent</a>
	 *  <br>
	 *  <a href="../../flash/events/GestureEvent.html" target="">flash.events.GestureEvent</a>
	 *  <br>
	 *  <a href="../../flash/events/TransformGestureEvent.html" target="">flash.events.TransformGestureEvent</a>
	 *  <br>
	 *  <a href="../../flash/events/GesturePhase.html" target="">flash.events.GesturePhase</a>
	 *  <br>
	 *  <a href="../../flash/events/PressAndTapGestureEvent.html" target="">flash.events.PressAndTapGestureEvent</a>
	 *  <br>
	 *  <a href="../../flash/events/MouseEvent.html" target="">flash.events.MouseEvent</a>
	 *  <br>
	 *  <a href="../../flash/events/EventDispatcher.html#addEventListener()" target="">flash.events.EventDispatcher.addEventListener()</a>
	 * </div><br><hr>
	 */
	public class Multitouch {
		private static var _inputMode:String;
		private static var _mapTouchToMouse:Boolean;

		private static var _maxTouchPoints:int;
		private static var _supportedGestures:Vector.<String>;
		private static var _supportsGestureEvents:Boolean;
		private static var _supportsTouchEvents:Boolean;


		/**
		 * <p> Identifies the multi-touch mode for touch and gesture event handling. Use this property to manage whether or not events are dispatched as touch events with multiple points of contact and specific events for different gestures (such as rotation and pan), or only a single point of contact (such as tap), or none at all (contact is handled as a mouse event). To set this property, use values from the flash.ui.MultitouchInputMode class. </p>
		 * <p> The default value is <code>gesture.</code></p>
		 * 
		 * @return 
		 */
		public static function get inputMode():String {
			return _inputMode;
		}


		/**
		 * @param value
		 * @return 
		 */
		public static function set inputMode(value:String):void {
			_inputMode = value;
		}


		/**
		 * <p> Specifies whether the AIR runtime maps touch events to mouse events. </p>
		 * <p>When <code>true</code>, the default, the AIR runtime dispatches a mouse event in addition to a touch event for touch inputs. When <code>false</code>, the runtime does not dispatch an additional mouse event. Setting this property to <code>false</code> can cause existing code, libraries, and frameworks that rely on mouse events to function incorrectly on devices that support touch input.</p>
		 * 
		 * @return 
		 */
		public static function get mapTouchToMouse():Boolean {
			return _mapTouchToMouse;
		}


		/**
		 * @param value
		 * @return 
		 */
		public static function set mapTouchToMouse(value:Boolean):void {
			_mapTouchToMouse = value;
		}


		/**
		 * <p> The maximum number of concurrent touch points supported by the current environment. </p>
		 * <p> <b>Note:</b> On Android, for the devices supporting more than two touch points, the return value is 2.</p>
		 * 
		 * @return 
		 */
		public static function get maxTouchPoints():int {
			return _maxTouchPoints;
		}


		/**
		 * <p> A Vector array (a typed array of string values) of multi-touch contact types supported in the current environment. The array of strings can be used as event types to register event listeners. Possible values are constants from the GestureEvent, PressAndTapGestureEvent, and TransformGestureEvent classes (such as <code>GESTURE_PAN</code>). </p>
		 * <p>If the Flash runtime is in an environment that does not support any multi-touch gestures, the value is <code>null</code>.</p>
		 * <p><b>Note:</b> For Mac OS 10.5.3 and later, <code>Multitouch.supportedGestures</code> returns non-null values (possibly indicating incorrectly that gesture events are supported) even if the current hardware does not support gesture input.</p>
		 * <p>Use this property to test for multi-touch gesture support. Then, use event handlers for the available multi-touch gestures. For those gestures that are not supported in the current evironment, you'll need to create alternative event handling.</p>
		 * 
		 * @return 
		 */
		public static function get supportedGestures():Vector.<String> {
			return _supportedGestures;
		}


		/**
		 * <p> Indicates whether the current environment supports gesture input, such as rotating two fingers around a touch screen. Gesture events are listed in the TransformGestureEvent, PressAndTapGestureEvent, and GestureEvent classes. </p>
		 * <p><b>Note:</b> For Mac OS 10.5.3 and later, this value is always <code>true</code>. <code>Multitouch.supportsGestureEvent</code> returns <code>true</code> even if the hardware does not support gesture events.</p>
		 * 
		 * @return 
		 */
		public static function get supportsGestureEvents():Boolean {
			return _supportsGestureEvents;
		}


		/**
		 * <p> Indicates whether the current environment supports basic touch input, such as a single finger tap. Touch events are listed in the TouchEvent class. </p>
		 * 
		 * @return 
		 */
		public static function get supportsTouchEvents():Boolean {
			return _supportsTouchEvents;
		}
	}
}
