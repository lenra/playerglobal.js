package flash.ui {
	import flash.display.BitmapData;
	import flash.geom.Point;

	/**
	 *  The MouseCursorData class lets you define the appearance of a "native" mouse cursor. <p>To display the cursor, use the <code>Mouse.registerCursor()</code> function. To return control of the cursor image to the operating system, call <code>Mouse.unregisterCursor()</code>. Call <code>Mouse.supportsNativeCursor</code> to test whether native cursors are supported on the current computer.</p> <p>The maximum cursor size is 32x32 pixels.Transparency is supported on most operating systems.</p> <p>A native mouse cursor is implemented directly through the operating system cursor mechanism and is a more efficient means for displaying a custom cursor image than using a display object. You can animate the cursor by supplying more than one image in the <code>data</code> property and setting the frame rate. </p> <p>The cursor is only displayed within the bounds of the stage. Outside the stage, control of the cursor image returns to the operating system</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://cookbooks.adobe.com/post_Native_Mouse_cursor_for_Flash_Player_10_2_-18576.html" target="_blank">AIR Cookbook: Native Mouse cursor for Flash Player 10.2+</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Mouse.html#cursor" target="">flash.ui.Mouse.cursor</a>
	 * </div><br><hr>
	 */
	public class MouseCursorData {
		private var _data:Vector.<BitmapData>;
		private var _frameRate:Number;
		private var _hotSpot:Point;

		/**
		 * <p> Creates a MouseCursorData object. </p>
		 * <p>To display the cursor, call the <code>Mouse.registerCursor()</code> function.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Mouse.html#registerCursor()" target="">flash.ui.Mouse.registerCursor()</a>
		 * </div>
		 */
		public function MouseCursorData() {
			throw new Error("Not implemented");
		}

		/**
		 * <p> A Vector of BitmapData objects containing the cursor image or images. </p>
		 * <p>Supply more than one image and set the <code>framerate</code> property to animate the cursor.</p>
		 * <p>The maximum cursor size is 32x32 pixels.</p>
		 * 
		 * @return 
		 */
		public function get data():Vector.<BitmapData> {
			return _data;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set data(value:Vector.<BitmapData>):void {
			_data = value;
		}

		/**
		 * <p> The frame rate for animating the cursor. </p>
		 * <p>Suppy more than one image in the <code>data</code> property and set the frame rate to a value greater than 0 to animate the cursor. The cursor frame rate may differ from the current SWF frame rate.</p>
		 * 
		 * @return 
		 */
		public function get frameRate():Number {
			return _frameRate;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set frameRate(value:Number):void {
			_frameRate = value;
		}

		/**
		 * <p> The hot spot of the cursor in pixels. </p>
		 * <p>The hotspot is the point on the cursor under which mouse clicks are registered. By default, the hot spot is the upper-left corner (0,0).</p>
		 * 
		 * @return 
		 */
		public function get hotSpot():Point {
			return _hotSpot;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set hotSpot(value:Point):void {
			_hotSpot = value;
		}
	}
}
