package flash.ui {
	//import flash.display.NativeMenuItem;
	import flash.events.ContextMenuEvent;

	[Event(name="menuItemSelect", type="flash.events.ContextMenuEvent")]
	/**
	 *  The ContextMenuItem class represents an item in the context menu. Each ContextMenuItem object has a caption (text) that is displayed in the context menu. To add a new item to a context menu, you add it to the <code>customItems</code> array of a ContextMenu object. <p>With the properties of the ContextMenuItem class you can enable or disable specific menu items, and you can make items visible or invisible.</p> You write an event handler for the <code>menuItemSelect</code> event to add functionality to the menu item when the user selects it. <p>Custom menu items appear at the top of the context menu, above any built-in items. A separator bar divides custom menu items from built-in items. In AIR, there are no built-in items and the following restrictions do not apply to content in the AIR application sandbox.</p> <p>Restrictions:</p> <ul> 
	 *  <li>You can add no more than 15 custom items to a context menu.</li> 
	 *  <li>Each caption must contain at least one visible character.</li> 
	 *  <li>Control characters, newlines, and other white space characters are ignored.</li> 
	 *  <li>No caption can be more than 100 characters long.</li> 
	 *  <li>Captions that are identical to any built-in menu item, or to another custom item, are ignored, whether the matching item is visible or not. Menu captions are compared to built-in captions or existing custom captions without regard to case, punctuation, or white space.</li> 
	 *  <li>The following captions are not allowed, but the words may be used in conjunction with other words to form a custom caption (for example, although "Paste" is not allowed, "Paste tastes great" is allowed): <pre>
	 *  Save
	 *  Zoom In
	 *  Zoom Out
	 *  100%
	 *  Show All
	 *  Quality
	 *  Play
	 *  Loop
	 *  Rewind
	 *  Forward
	 *  Back
	 *  Movie not loaded
	 *  About
	 *  Print
	 *  Show Redraw Regions
	 *  Debugger
	 *  Undo
	 *  Cut
	 *  Copy
	 *  Paste
	 *  Delete
	 *  Select All
	 *  Open
	 *  Open in new window
	 *  Copy link
	 *  </pre> </li> 
	 *  <li>None of the following words can appear in a custom caption on their own or in conjunction with other words: <pre>
	 *  Adobe
	 *  Macromedia
	 *  Flash Player
	 *  Settings
	 *  </pre> </li> 
	 * </ul> <p> <b>Note:</b> When the player is running on a non-English system, the caption strings are compared to both the English list and the localized equivalents.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118676a48d0-8000.html" target="_blank">Working with menus</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="ContextMenu.html" target="">ContextMenu class</a>
	 *  <br>
	 *  <a href="ContextMenuBuiltInItems.html" target="">ContextMenuBuiltInItems class</a>
	 * </div><br><hr>
	 */
	public class ContextMenuItem /*extends NativeMenuItem*/ {
		private var _caption:String;
		private var _separatorBefore:Boolean;
		private var _visible:Boolean;

		/**
		 * <p> Creates a new ContextMenuItem object that can be added to the <code>ContextMenu.customItems</code> array. </p>
		 * 
		 * @param caption  — Specifies the text associated with the menu item. See the ContextMenuItem class overview for <code>caption</code> value restrictions. 
		 * @param separatorBefore  — Specifies whether a separator bar appears above the menu item in the context menu. The default value is <code>false</code>. 
		 * @param enabled  — Specifies whether the menu item is enabled or disabled in the context menu. The default value is <code>true</code> (enabled). This parameter is optional. 
		 * @param visible  — Specifies whether the menu item is visible or invisible. The default value is <code>true</code> (visible). 
		 */
		public function ContextMenuItem(caption:String, separatorBefore:Boolean = false, enabled:Boolean = true, visible:Boolean = true) {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Specifies the menu item caption (text) displayed in the context menu. See the ContextMenuItem class overview for <code>caption</code> value restrictions. </p>
		 * 
		 * @return 
		 */
		public function get caption():String {
			return _caption;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set caption(value:String):void {
			_caption = value;
		}

		/**
		 * <p> Indicates whether a separator bar should appear above the specified menu item. </p>
		 * <p><b>Note: </b>A separator bar always appears between any custom menu items and the built-in menu items.</p>
		 * <p> The default value is <code>false.</code></p>
		 * 
		 * @return 
		 */
		public function get separatorBefore():Boolean {
			return _separatorBefore;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set separatorBefore(value:Boolean):void {
			_separatorBefore = value;
		}

		/**
		 * <p> Indicates whether the specified menu item is visible when the Flash Player context menu is displayed. </p>
		 * <p> The default value is <code>true.</code></p>
		 * 
		 * @return 
		 */
		public function get visible():Boolean {
			return _visible;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set visible(value:Boolean):void {
			_visible = value;
		}

		/**
		 * @return 
		 */
		/*public function clone():NativeMenuItem {
			throw new Error("Not implemented");
		}*/


		/**
		 * @return 
		 */
		public static function systemClearMenuItem():ContextMenuItem {
			throw new Error("Not implemented");
		}


		/**
		 * @return 
		 */
		public static function systemCopyLinkMenuItem():ContextMenuItem {
			throw new Error("Not implemented");
		}


		/**
		 * @return 
		 */
		public static function systemCopyMenuItem():ContextMenuItem {
			throw new Error("Not implemented");
		}


		/**
		 * @return 
		 */
		public static function systemCutMenuItem():ContextMenuItem {
			throw new Error("Not implemented");
		}


		/**
		 * @return 
		 */
		public static function systemOpenLinkMenuItem():ContextMenuItem {
			throw new Error("Not implemented");
		}


		/**
		 * @return 
		 */
		public static function systemPasteMenuItem():ContextMenuItem {
			throw new Error("Not implemented");
		}


		/**
		 * @return 
		 */
		public static function systemSelectAllMenuItem():ContextMenuItem {
			throw new Error("Not implemented");
		}
	}
}
