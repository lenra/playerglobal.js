package flash.filters {
	/**
	 *  The BevelFilter class lets you add a bevel effect to display objects. A bevel effect gives objects such as buttons a three-dimensional look. You can customize the look of the bevel with different highlight and shadow colors, the amount of blur on the bevel, the angle of the bevel, the placement of the bevel, and a knockout effect. You can apply the filter to any display object (that is, objects that inherit from the DisplayObject class), such as MovieClip, SimpleButton, TextField, and Video objects, as well as to BitmapData objects. <p>To create a new filter, use the constructor <code>new BevelFilter()</code>. The use of filters depends on the object to which you apply the filter:</p> <ul> 
	 *  <li>To apply filters to movie clips, text fields, buttons, and video, use the <code>filters</code> property (inherited from DisplayObject). Setting the <code>filters</code> property of an object does not modify the object, and you can remove the filter by clearing the <code>filters</code> property. </li> 
	 *  <li>To apply filters to BitmapData objects, use the <code>BitmapData.applyFilter()</code> method. Calling <code>applyFilter()</code> on a BitmapData object takes the source BitmapData object and the filter object and generates a filtered image as a result.</li> 
	 * </ul> <p>If you apply a filter to a display object, the value of the <code>cacheAsBitmap</code> property of the object is set to <code>true</code>. If you remove all filters, the original value of <code>cacheAsBitmap</code> is restored.</p> <p>This filter supports Stage scaling. However, it does not support general scaling, rotation, and skewing. If the object itself is scaled (if the <code>scaleX</code> and <code>scaleY</code> properties are not set to 100%), the filter is not scaled. It is scaled only when the user zooms in on the Stage.</p> <p>A filter is not applied if the resulting image exceeds the maximum dimensions. In AIR 1.5 and Flash Player 10, the maximum is 8,191 pixels in width or height, and the total number of pixels cannot exceed 16,777,215 pixels. (So, if an image is 8,191 pixels wide, it can only be 2,048 pixels high.) In Flash Player 9 and earlier and AIR 1.1 and earlier, the limitation is 2,880 pixels in height and 2,880 pixels in width. If, for example, you zoom in on a large movie clip with a filter applied, the filter is turned off if the resulting image exceeds the maximum dimensions.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../flash/display/DisplayObject.html#filters" target="">flash.display.DisplayObject.filters</a>
	 *  <br>
	 *  <a href="../../flash/display/DisplayObject.html#cacheAsBitmap" target="">flash.display.DisplayObject.cacheAsBitmap</a>
	 *  <br>
	 *  <a href="../../flash/display/BitmapData.html#applyFilter()" target="">flash.display.BitmapData.applyFilter()</a>
	 * </div><br><hr>
	 */
	public class BevelFilter extends BitmapFilter {
		private var _angle:Number;
		private var _blurX:Number;
		private var _blurY:Number;
		private var _distance:Number;
		private var _highlightAlpha:Number;
		private var _highlightColor:uint;
		private var _knockout:Boolean;
		private var _quality:int;
		private var _shadowAlpha:Number;
		private var _shadowColor:uint;
		private var _strength:Number;
		private var _type:String;

		/**
		 * <p> Initializes a new BevelFilter instance with the specified parameters. </p>
		 * 
		 * @param distance  — The offset distance of the bevel, in pixels (floating point). 
		 * @param angle  — The angle of the bevel, from 0 to 360 degrees. 
		 * @param highlightColor  — The highlight color of the bevel, <i>0xRRGGBB</i>. 
		 * @param highlightAlpha  — The alpha transparency value of the highlight color. Valid values are 0.0 to 1.0. For example, .25 sets a transparency value of 25%. 
		 * @param shadowColor  — The shadow color of the bevel, <i>0xRRGGBB</i>. 
		 * @param shadowAlpha  — The alpha transparency value of the shadow color. Valid values are 0.0 to 1.0. For example, .25 sets a transparency value of 25%. 
		 * @param blurX  — The amount of horizontal blur in pixels. Valid values are 0 to 255.0 (floating point). 
		 * @param blurY  — The amount of vertical blur in pixels. Valid values are 0 to 255.0 (floating point). 
		 * @param strength  — The strength of the imprint or spread. The higher the value, the more color is imprinted and the stronger the contrast between the bevel and the background. Valid values are 0 to 255.0. 
		 * @param quality  — The quality of the bevel. Valid values are 0 to 15, but for most applications, you can use <code>BitmapFilterQuality</code> constants: <ul>
		 *  <li><code>BitmapFilterQuality.LOW</code></li>
		 *  <li><code>BitmapFilterQuality.MEDIUM</code></li>
		 *  <li><code>BitmapFilterQuality.HIGH</code></li>
		 * </ul> <p>Filters with lower values render faster. You can use the other available numeric values to achieve different effects.</p> 
		 * @param type  — The type of bevel. Valid values are <code>BitmapFilterType</code> constants: <code>BitmapFilterType.INNER</code>, <code>BitmapFilterType.OUTER</code>, or <code>BitmapFilterType.FULL</code>. 
		 * @param knockout  — Applies a knockout effect (<code>true</code>), which effectively makes the object's fill transparent and reveals the background color of the document. 
		 */
		public function BevelFilter(distance:Number = 4.0, angle:Number = 45, highlightColor:uint = 0xFFFFFF, highlightAlpha:Number = 1.0, shadowColor:uint = 0x000000, shadowAlpha:Number = 1.0, blurX:Number = 4.0, blurY:Number = 4.0, strength:Number = 1, quality:int = 1, type:String = "inner", knockout:Boolean = false) {
			throw new Error("Not implemented");
		}

		/**
		 * <p> The angle of the bevel. Valid values are from 0 to 360°. The default value is 45°. </p>
		 * <p>The angle value represents the angle of the theoretical light source falling on the object and determines the placement of the effect relative to the object. If the <code>distance</code> property is set to 0, the effect is not offset from the object and, therefore, the <code>angle</code> property has no effect.</p>
		 * 
		 * @return 
		 */
		public function get angle():Number {
			return _angle;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set angle(value:Number):void {
			_angle = value;
		}

		/**
		 * <p> The amount of horizontal blur, in pixels. Valid values are from 0 to 255 (floating point). The default value is 4. Values that are a power of 2 (such as 2, 4, 8, 16, and 32) are optimized to render more quickly than other values. </p>
		 * 
		 * @return 
		 */
		public function get blurX():Number {
			return _blurX;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set blurX(value:Number):void {
			_blurX = value;
		}

		/**
		 * <p> The amount of vertical blur, in pixels. Valid values are from 0 to 255 (floating point). The default value is 4. Values that are a power of 2 (such as 2, 4, 8, 16, and 32) are optimized to render more quickly than other values. </p>
		 * 
		 * @return 
		 */
		public function get blurY():Number {
			return _blurY;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set blurY(value:Number):void {
			_blurY = value;
		}

		/**
		 * <p> The offset distance of the bevel. Valid values are in pixels (floating point). The default is 4. </p>
		 * 
		 * @return 
		 */
		public function get distance():Number {
			return _distance;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set distance(value:Number):void {
			_distance = value;
		}

		/**
		 * <p> The alpha transparency value of the highlight color. The value is specified as a normalized value from 0 to 1. For example, .25 sets a transparency value of 25%. The default value is 1. </p>
		 * 
		 * @return 
		 */
		public function get highlightAlpha():Number {
			return _highlightAlpha;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set highlightAlpha(value:Number):void {
			_highlightAlpha = value;
		}

		/**
		 * <p> The highlight color of the bevel. Valid values are in hexadecimal format, <i>0xRRGGBB</i>. The default is 0xFFFFFF. </p>
		 * 
		 * @return 
		 */
		public function get highlightColor():uint {
			return _highlightColor;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set highlightColor(value:uint):void {
			_highlightColor = value;
		}

		/**
		 * <p> Applies a knockout effect (<code>true</code>), which effectively makes the object's fill transparent and reveals the background color of the document. The default value is <code>false</code> (no knockout). </p>
		 * 
		 * @return 
		 */
		public function get knockout():Boolean {
			return _knockout;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set knockout(value:Boolean):void {
			_knockout = value;
		}

		/**
		 * <p> The number of times to apply the filter. The default value is <code>BitmapFilterQuality.LOW</code>, which is equivalent to applying the filter once. The value <code>BitmapFilterQuality.MEDIUM</code> applies the filter twice; the value <code>BitmapFilterQuality.HIGH</code> applies it three times. Filters with lower values are rendered more quickly. </p>
		 * <p>For most applications, a <code>quality</code> value of low, medium, or high is sufficient. Although you can use additional numeric values up to 15 to achieve different effects, higher values are rendered more slowly. Instead of increasing the value of <code>quality</code>, you can often get a similar effect, and with faster rendering, by simply increasing the values of the <code>blurX</code> and <code>blurY</code> properties.</p>
		 * <p>You can use the following <code>BitmapFilterQuality</code> constants to specify values of the <code>quality</code> property: </p>
		 * <ul>
		 *  <li><code>BitmapFilterQuality.LOW</code></li>
		 *  <li><code>BitmapFilterQuality.MEDIUM</code></li>
		 *  <li><code>BitmapFilterQuality.HIGH</code></li>
		 * </ul>
		 * 
		 * @return 
		 */
		public function get quality():int {
			return _quality;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set quality(value:int):void {
			_quality = value;
		}

		/**
		 * <p> The alpha transparency value of the shadow color. This value is specified as a normalized value from 0 to 1. For example, .25 sets a transparency value of 25%. The default is 1. </p>
		 * 
		 * @return 
		 */
		public function get shadowAlpha():Number {
			return _shadowAlpha;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set shadowAlpha(value:Number):void {
			_shadowAlpha = value;
		}

		/**
		 * <p> The shadow color of the bevel. Valid values are in hexadecimal format, <i>0xRRGGBB</i>. The default is 0x000000. </p>
		 * 
		 * @return 
		 */
		public function get shadowColor():uint {
			return _shadowColor;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set shadowColor(value:uint):void {
			_shadowColor = value;
		}

		/**
		 * <p> The strength of the imprint or spread. Valid values are from 0 to 255. The larger the value, the more color is imprinted and the stronger the contrast between the bevel and the background. The default value is 1. </p>
		 * 
		 * @return 
		 */
		public function get strength():Number {
			return _strength;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set strength(value:Number):void {
			_strength = value;
		}

		/**
		 * <p> The placement of the bevel on the object. Inner and outer bevels are placed on the inner or outer edge; a full bevel is placed on the entire object. Valid values are the <code>BitmapFilterType</code> constants: </p>
		 * <ul>
		 *  <li><code>BitmapFilterType.INNER</code></li>
		 *  <li><code>BitmapFilterType.OUTER</code></li>
		 *  <li><code>BitmapFilterType.FULL</code></li>
		 * </ul>
		 * 
		 * @return 
		 */
		public function get type():String {
			return _type;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set type(value:String):void {
			_type = value;
		}

		/**
		 * <p> Returns a copy of this filter object. </p>
		 * 
		 * @return  — A new BevelFilter instance with all the same properties as the original BevelFilter instance. 
		 */
		override public function clone():BitmapFilter {
			throw new Error("Not implemented");
		}
	}
}
