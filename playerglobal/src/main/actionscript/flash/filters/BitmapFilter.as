package flash.filters {
	/**
	 *  The BitmapFilter class is the base class for all image filter effects. <p>The BevelFilter, BlurFilter, ColorMatrixFilter, ConvolutionFilter, DisplacementMapFilter, DropShadowFilter, GlowFilter, GradientBevelFilter, and GradientGlowFilter classes all extend the BitmapFilter class. You can apply these filter effects to any display object.</p> <p>You can neither directly instantiate nor extend BitmapFilter.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><br><hr>
	 */
	public class BitmapFilter {

		/**
		 * <p> Returns a BitmapFilter object that is an exact copy of the original BitmapFilter object. </p>
		 * 
		 * @return  — A BitmapFilter object. 
		 */
		public function clone():BitmapFilter {
			throw new Error("Not implemented");
		}
	}
}
