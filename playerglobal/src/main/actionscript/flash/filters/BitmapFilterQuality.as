package flash.filters {
	/**
	 *  The BitmapFilterQuality class contains values to set the rendering quality of a BitmapFilter object. <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="BevelFilter.html" target="">BevelFilter</a>
	 *  <br>
	 *  <a href="BlurFilter.html" target="">BlurFilter</a>
	 *  <br>
	 *  <a href="GlowFilter.html" target="">GlowFilter</a>
	 *  <br>
	 *  <a href="DropShadowFilter.html" target="">DropShadowFilter</a>
	 *  <br>
	 *  <a href="GradientBevelFilter.html" target="">GradientBevelFilter</a>
	 *  <br>
	 *  <a href="GradientGlowFilter.html" target="">GradientGlowFilter</a>
	 * </div><br><hr>
	 */
	public class BitmapFilterQuality {
		/**
		 * <p> Defines the high quality filter setting. </p>
		 */
		public static const HIGH:int = 3;
		/**
		 * <p> Defines the low quality filter setting. </p>
		 */
		public static const LOW:int = 1;
		/**
		 * <p> Defines the medium quality filter setting. </p>
		 */
		public static const MEDIUM:int = 2;
	}
}
