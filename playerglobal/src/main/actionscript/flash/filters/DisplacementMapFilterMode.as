package flash.filters {
	/**
	 *  The DisplacementMapFilterMode class provides values for the <code>mode</code> property of the DisplacementMapFilter class. <br><hr>
	 */
	public class DisplacementMapFilterMode {
		/**
		 * <p> Clamps the displacement value to the edge of the source image. Use with the <code>DisplacementMapFilter.mode</code> property. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="DisplacementMapFilter.html#mode" target="">flash.filters.DisplacementMapFilter.mode</a>
		 * </div>
		 */
		public static const CLAMP:String = "clamp";
		/**
		 * <p> If the displacement value is outside the image, substitutes the values in the <code>color</code> and <code>alpha</code> properties. Use with the <code>DisplacementMapFilter.mode</code> property. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="DisplacementMapFilter.html#mode" target="">flash.filters.DisplacementMapFilter.mode</a>
		 * </div>
		 */
		public static const COLOR:String = "color";
		/**
		 * <p> If the displacement value is out of range, ignores the displacement and uses the source pixel. Use with the <code>DisplacementMapFilter.mode</code> property. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="DisplacementMapFilter.html#mode" target="">flash.filters.DisplacementMapFilter.mode</a>
		 * </div>
		 */
		public static const IGNORE:String = "ignore";
		/**
		 * <p> Wraps the displacement value to the other side of the source image. Use with the <code>DisplacementMapFilter.mode</code> property. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="DisplacementMapFilter.html#mode" target="">flash.filters.DisplacementMapFilter.mode</a>
		 * </div>
		 */
		public static const WRAP:String = "wrap";
	}
}
