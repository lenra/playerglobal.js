package flash.filters {
	import flash.display.BitmapData;
	import flash.geom.Point;

	/**
	 *  The DisplacementMapFilter class uses the pixel values from the specified BitmapData object (called the <i>displacement map image</i>) to perform a displacement of an object. You can use this filter to apply a warped or mottled effect to any object that inherits from the DisplayObject class, such as MovieClip, SimpleButton, TextField, and Video objects, as well as to BitmapData objects. <p>The use of filters depends on the object to which you apply the filter:</p> <ul> 
	 *  <li>To apply filters to a display object, use the <code>filters</code> property of the display object. Setting the <code>filters</code> property of an object does not modify the object, and you can remove the filter by clearing the <code>filters</code> property. </li> 
	 *  <li>To apply filters to BitmapData objects, use the <code>BitmapData.applyFilter()</code> method. Calling <code>applyFilter()</code> on a BitmapData object takes the source BitmapData object and the filter object and generates a filtered image.</li> 
	 * </ul> <p>If you apply a filter to a display object, the value of the <code>cacheAsBitmap</code> property of the display object is set to <code>true</code>. If you clear all filters, the original value of <code>cacheAsBitmap</code> is restored.</p> <p>The filter uses the following formula:</p> <div class="listing">
	 *  <pre>
	 * dstPixel[x, y] = srcPixel[x + ((componentX(x, y) - 128) * scaleX) / 256, y + ((componentY(x, y) - 128) *scaleY) / 256)
	 * </pre>
	 *  <div class="clipcopy">
	 *   <a href="#" class="copyText">Copy</a>
	 *  </div>
	 * </div> <p>where <code>componentX(x, y)</code> gets the <code>componentX</code> property color value from the <code>mapBitmap</code> property at <code>(x - mapPoint.x ,y - mapPoint.y)</code>.</p> <p>The map image used by the filter is scaled to match the Stage scaling. It is not scaled when the object itself is scaled.</p> <p>This filter supports Stage scaling. However, general scaling, rotation, and skewing are not supported. If the object itself is scaled (if the <code>scaleX</code> and <code>scaleY</code> properties are set to a value other than 1.0), the filter effect is not scaled. It is scaled only when the user zooms in on the Stage.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../flash/display/BitmapData.html#applyFilter()" target="">flash.display.BitmapData.applyFilter()</a>
	 *  <br>
	 *  <a href="../../flash/display/DisplayObject.html#filters" target="">flash.display.DisplayObject.filters</a>
	 *  <br>
	 *  <a href="../../flash/display/DisplayObject.html#cacheAsBitmap" target="">flash.display.DisplayObject.cacheAsBitmap</a>
	 * </div><br><hr>
	 */
	public class DisplacementMapFilter extends BitmapFilter {
		private var _alpha:Number;
		private var _color:uint;
		private var _componentX:uint;
		private var _componentY:uint;
		private var _mapBitmap:BitmapData;
		private var _mapPoint:Point;
		private var _mode:String;
		private var _scaleX:Number;
		private var _scaleY:Number;

		/**
		 * <p> Initializes a DisplacementMapFilter instance with the specified parameters. </p>
		 * 
		 * @param mapBitmap  — A BitmapData object containing the displacement map data. 
		 * @param mapPoint  — A value that contains the offset of the upper-left corner of the target display object from the upper-left corner of the map image. 
		 * @param componentX  — Describes which color channel to use in the map image to displace the <i>x</i> result. Possible values are the BitmapDataChannel constants. 
		 * @param componentY  — Describes which color channel to use in the map image to displace the <i>y</i> result. Possible values are the BitmapDataChannel constants. 
		 * @param scaleX  — The multiplier to use to scale the <i>x</i> displacement result from the map calculation. 
		 * @param scaleY  — The multiplier to use to scale the <i>y</i> displacement result from the map calculation. 
		 * @param mode  — The mode of the filter. Possible values are the DisplacementMapFilterMode constants. 
		 * @param color  — Specifies the color to use for out-of-bounds displacements. The valid range of displacements is 0.0 to 1.0. Use this parameter if <code>mode</code> is set to <code>DisplacementMapFilterMode.COLOR</code>. 
		 * @param alpha  — Specifies what alpha value to use for out-of-bounds displacements. It is specified as a normalized value from 0.0 to 1.0. For example, .25 sets a transparency value of 25%. Use this parameter if <code>mode</code> is set to <code>DisplacementMapFilterMode.COLOR</code>. 
		 */
		public function DisplacementMapFilter(mapBitmap:BitmapData = null, mapPoint:Point = null, componentX:uint = 0, componentY:uint = 0, scaleX:Number = 0.0, scaleY:Number = 0.0, mode:String = "wrap", color:uint = 0, alpha:Number = 0.0) {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Specifies the alpha transparency value to use for out-of-bounds displacements. It is specified as a normalized value from 0.0 to 1.0. For example, .25 sets a transparency value of 25%. The default value is 0. Use this property if the <code>mode</code> property is set to <code>DisplacementMapFilterMode.COLOR</code>. </p>
		 * 
		 * @return 
		 */
		public function get alpha():Number {
			return _alpha;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set alpha(value:Number):void {
			_alpha = value;
		}

		/**
		 * <p> Specifies what color to use for out-of-bounds displacements. The valid range of displacements is 0.0 to 1.0. Values are in hexadecimal format. The default value for <code>color</code> is 0. Use this property if the <code>mode</code> property is set to <code>DisplacementMapFilterMode.COLOR</code>. </p>
		 * 
		 * @return 
		 */
		public function get color():uint {
			return _color;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set color(value:uint):void {
			_color = value;
		}

		/**
		 * <p> Describes which color channel to use in the map image to displace the <i>x</i> result. Possible values are BitmapDataChannel constants: </p>
		 * <ul>
		 *  <li><code>BitmapDataChannel.ALPHA</code></li>
		 *  <li><code>BitmapDataChannel.BLUE</code></li>
		 *  <li><code>BitmapDataChannel.GREEN</code></li>
		 *  <li><code>BitmapDataChannel.RED</code></li>
		 * </ul>
		 * 
		 * @return 
		 */
		public function get componentX():uint {
			return _componentX;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set componentX(value:uint):void {
			_componentX = value;
		}

		/**
		 * <p> Describes which color channel to use in the map image to displace the <i>y</i> result. Possible values are BitmapDataChannel constants: </p>
		 * <ul>
		 *  <li><code>BitmapDataChannel.ALPHA</code></li>
		 *  <li><code>BitmapDataChannel.BLUE</code></li>
		 *  <li><code>BitmapDataChannel.GREEN</code></li>
		 *  <li><code>BitmapDataChannel.RED</code></li>
		 * </ul>
		 * 
		 * @return 
		 */
		public function get componentY():uint {
			return _componentY;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set componentY(value:uint):void {
			_componentY = value;
		}

		/**
		 * <p> A BitmapData object containing the displacement map data. </p>
		 * 
		 * @return 
		 */
		public function get mapBitmap():BitmapData {
			return _mapBitmap;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set mapBitmap(value:BitmapData):void {
			_mapBitmap = value;
		}

		/**
		 * <p> A value that contains the offset of the upper-left corner of the target display object from the upper-left corner of the map image. </p>
		 * 
		 * @return 
		 */
		public function get mapPoint():Point {
			return _mapPoint;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set mapPoint(value:Point):void {
			_mapPoint = value;
		}

		/**
		 * <p> The mode for the filter. Possible values are DisplacementMapFilterMode constants: </p>
		 * <ul>
		 *  <li><code>DisplacementMapFilterMode.WRAP</code> — Wraps the displacement value to the other side of the source image.</li>
		 *  <li><code>DisplacementMapFilterMode.CLAMP</code> — Clamps the displacement value to the edge of the source image.</li>
		 *  <li><code>DisplacementMapFilterMode.IGNORE</code> — If the displacement value is out of range, ignores the displacement and uses the source pixel.</li>
		 *  <li><code>DisplacementMapFilterMode.COLOR</code> — If the displacement value is outside the image, substitutes the values in the <code>color</code> and <code>alpha</code> properties.</li>
		 * </ul>
		 * 
		 * @return 
		 */
		public function get mode():String {
			return _mode;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set mode(value:String):void {
			_mode = value;
		}

		/**
		 * <p> The multiplier to use to scale the <i>x</i> displacement result from the map calculation. </p>
		 * 
		 * @return 
		 */
		public function get scaleX():Number {
			return _scaleX;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set scaleX(value:Number):void {
			_scaleX = value;
		}

		/**
		 * <p> The multiplier to use to scale the <i>y</i> displacement result from the map calculation. </p>
		 * 
		 * @return 
		 */
		public function get scaleY():Number {
			return _scaleY;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set scaleY(value:Number):void {
			_scaleY = value;
		}

		/**
		 * <p> Returns a copy of this filter object. </p>
		 * 
		 * @return  — A new DisplacementMapFilter instance with all the same properties as the original one. 
		 */
		override public function clone():BitmapFilter {
			throw new Error("Not implemented");
		}
	}
}
