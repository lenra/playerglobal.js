package flash.filters {
	/**
	 *  The BitmapFilterType class contains values to set the type of a BitmapFilter. <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="BevelFilter.html" target="">BevelFilter</a>
	 *  <br>
	 *  <a href="GradientBevelFilter.html" target="">GradientBevelFilter</a>
	 *  <br>
	 *  <a href="GradientGlowFilter.html" target="">GradientGlowFilter</a>
	 * </div><br><hr>
	 */
	public class BitmapFilterType {
		/**
		 * <p> Defines the setting that applies a filter to the entire area of an object. </p>
		 */
		public static const FULL:String = "full";
		/**
		 * <p> Defines the setting that applies a filter to the inner area of an object. </p>
		 */
		public static const INNER:String = "inner";
		/**
		 * <p> Defines the setting that applies a filter to the outer area of an object. </p>
		 */
		public static const OUTER:String = "outer";
	}
}
