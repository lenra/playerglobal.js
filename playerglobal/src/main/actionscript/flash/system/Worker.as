package flash.system {
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.net.URLLoader;
	import flash.utils.ByteArray;
	import flash.utils.Dictionary;
	
	import fr.lenra.playerglobal.utils.BlobUtil;

	[Event(name="workerState", type="flash.events.Event")]
	/**
	 *  A Worker object represents a <i>worker</i>, which is a virtual instance of the Flash runtime. Each Worker instance controls and provides access to the lifecycle and shared data of a single worker. <p>A worker allows you to execute code "in the background" at the same time that other operations are running in another worker (including the main swf's worker). In a non-worker context some operations, for example processing a large set of data in a loop, take so long to execute that they prevent the main application thread from updating the screen quickly enough. This can cause stuttering or freezing the screen.</p> <p>Using a worker allows you to perform a long-running or slow operation in the background. Each worker runs its code in a separate thread of execution from other workers. Long-running code in one worker does not block code in another worker from executing. Instead, the two sets of code run in parallel. Consequently, a worker can be used to execute code in the background while the main application thread stays free to continue updating the screen.</p> <p>This capability of simultaneously executing multiple sets of code instructions is known as <i>concurrency</i>.</p> <p> <b>Note:</b> The use of workers for concurrency is supported in both Flash Player and AIR on desktop platforms. For mobile platforms, concurrency is supported in AIR on both Android and iOS. You can use the static isSupported property to check whether concurrency is supported before attempting to use it. </p> <p>You do not create Worker instances directly by calling the <code>Worker()</code> constructor. In contexts where the use of workers for concurrency is supported, at startup the runtime automatically creates the Worker associated with the main SWF, known as the <i>primordial worker</i>.</p> <p>Each additional worker is created from a separate swf. To create a new instance of the Worker class, pass a ByteArray with the bytes of the background worker's swf as an argument to the WorkerDomain class's <code>createWorker()</code>method. There are three common ways to access the bytes of a swf for this purpose:</p> <ul> 
	 *  <li> <p>Use the [Embed] metatag to embed the .swf file in the application as a ByteArray:</p> 
	 *   <div class="listing">
	 *    <div class="clipcopy">
	 *     <a href="#" class="copyText">Copy</a>
	 *    </div>
	 *    <pre>
	 *  // Embed the SWF file
	 *  [Embed(source="../swfs/BgWorker.swf", mimeType="application/octet-stream")]
	 *  private static var BgWorker_ByteClass:Class;
	 *  
	 *  private function createWorker():void
	 *  {
	 *    // create the background worker
	 *    var workerBytes:ByteArray = new BgWorker_ByteClass();
	 *    var bgWorker:Worker = WorkerDomain.current.createWorker(workerBytes);
	 *    
	 *    // listen for worker state changes to know when the worker is running
	 *    bgWorker.addEventListener(Event.WORKER_STATE, workerStateHandler);
	 *    
	 *    // set up communication between workers using 
	 *    // setSharedProperty(), createMessageChannel(), etc.
	 *    // ... (not shown)
	 *    
	 *    bgWorker.start();
	 *  }</pre>
	 *   </div> </li> 
	 *  <li> <p>Load an external SWF file using a URLLoader:</p> 
	 *   <div class="listing">
	 *    <div class="clipcopy">
	 *     <a href="#" class="copyText">Copy</a>
	 *    </div>
	 *    <pre>
	 *  // load the SWF file
	 *  var workerLoader:URLLoader = new URLLoader();
	 *  workerLoader.dataFormat = URLLoaderDataFormat.BINARY;
	 *  workerLoader.addEventListener(Event.COMPLETE, loadComplete);
	 *  workerLoader.load(new URLRequest("BgWorker.swf"));
	 *  
	 *  private function loadComplete(event:Event):void
	 *  {
	 *    // create the background worker
	 *    var workerBytes:ByteArray = event.target.data as ByteArray;
	 *    var bgWorker:Worker = WorkerDomain.current.createWorker(workerBytes);
	 *    
	 *    // listen for worker state changes to know when the worker is running
	 *    bgWorker.addEventListener(Event.WORKER_STATE, workerStateHandler);
	 *    
	 *    // set up communication between workers using 
	 *    // setSharedProperty(), createMessageChannel(), etc.
	 *    // ... (not shown)
	 *    
	 *    bgWorker.start();
	 *  }</pre>
	 *   </div> </li> 
	 *  <li> <p>Use a single swf as both the primordial worker and the background worker:</p> 
	 *   <div class="listing">
	 *    <div class="clipcopy">
	 *     <a href="#" class="copyText">Copy</a>
	 *    </div>
	 *    <pre>
	 *  // The primordial worker's main class constructor
	 *  public function PrimordialWorkerClass()
	 *  {
	 *    init();
	 *  }
	 *  
	 *  private function init():void
	 *  {
	 *    var swfBytes:ByteArray = this.loaderInfo.bytes;
	 *    
	 *    // Check to see if this is the primordial worker
	 *    if (Worker.current.isPrimordial)    
	 *    {
	 *      // create a background worker
	 *      var bgWorker:Worker = WorkerDomain.current.createWorker(swfBytes);
	 *      
	 *      // listen for worker state changes to know when the worker is running
	 *      bgWorker.addEventListener(Event.WORKER_STATE, workerStateHandler);
	 *      
	 *      // set up communication between workers using 
	 *      // setSharedProperty(), createMessageChannel(), etc.
	 *      // ... (not shown)
	 *      
	 *      bgWorker.start();
	 *    }
	 *    else // entry point for the background worker
	 *    {
	 *      // set up communication between workers using getSharedProperty()
	 *      // ... (not shown)
	 *      
	 *      // start the background work
	 *    }
	 *  }</pre>
	 *   </div> </li> 
	 * </ul> <p>Workers execute in isolation from each other and do not have access to the same memory, variables, and code. However, there are three mechanisms available for passing messages and data between Worker instances:</p> <ul> 
	 *  <li>Shared properties: Each worker has an internal set of named values that can be set and read both from within the worker itself as well as from other workers. You can set a value using the <code>setSharedProperty()</code> method and read a value using the <code>getSharedProperty()</code> method.</li> 
	 *  <li>MessageChannel: A MessageChannel object allows you to send one-way messages and data from one worker to another. Code in the receiving worker can listen for an event to be notified when a message arrives. To create a MessageChannel object, use the <code>createMessageChannel()</code> method.</li> 
	 *  <li>Shareable ByteArray: If a ByteArray object's <code>shareable</code> property is <code>true</code>, the same underlying memory is used for instances of that ByteArray in all workers. Because code in multiple workers can access the shared memory at the same time, your code should use the mechanisms described in the <code>ByteArray.shareable</code> property description to avoid problems from unexpected data changes.</li> 
	 * </ul> <p>Several runtime APIs are not available in code running in a background worker. These primarily consist of APIs related to user input and output mechanisms, or operating system elements like windows and dragging. As a rule, for any API that isn't supported in all contexts, use the <code>isSupported</code>, <code>available</code>, and similar properties to check whether the API is available in the background worker context before attempting to use the API.</p> <p> <b>Note:</b> Native Extensions are not supported for background and secondary workers.</p> <p>Workers are useful because they decrease the chances of the frame rate dropping due to the main rendering thread being blocked by other code. However, workers require additional system memory and CPU use, which can be costly to overall application performance. Because each worker uses its own instance of the runtime virtual machine, even the overhead of a trivial worker can be large. When using workers, test your code across all your target platforms to ensure that the demands on the system are not too large. Adobe recommends that you do not use more than one or two background workers in a typical scenario.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS2f73111e7a180bd0-5856a8af1390d64d08c-8000.html" target="_blank">Understanding workers and concurrency</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS2f73111e7a180bd0-5856a8af1390d64d08c-7fff.html" target="_blank">Creating and managing workers</a>
	 * </div><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://gotoandlearn.com/play.php?id=162" target="_blank">Using ActionScript Workers by Lee Brimelow</a>
	 *  <br>
	 *  <a href="http://esdot.ca/site/2012/intro-to-as3-workers-part-2-image-processing" target="_blank">Intro to AS3 Workers: Image Processing by Shawn Blais</a>
	 *  <br>
	 *  <a href="http://esdot.ca/site/2012/intro-to-as3-workers-part-3-nape-physics-starling" target="_blank">Multithreaded Physics: Using AS3 Workers for a physics engine by Shawn Blais</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="WorkerDomain.html" target="">WorkerDomain class</a>
	 *  <br>
	 *  <a href="WorkerDomain.html#createWorker()" target="">WorkerDomain.createWorker() method</a>
	 *  <br>
	 *  <a href="MessageChannel.html" target="">MessageChannel class</a>
	 * </div><br><hr>
	 */
	public class Worker extends EventDispatcher {
		private static const SET_SHARED_PROPERTY:int = 0;
		private static const SET_SHARED_OBJECT:int = 1;//MessageChannel or other shared Object
		private static const CREATE_MESSAGE_CHANNEL:int = 2;//create a MessageChannel
		private static const SEND_MESSAGE:int = 3;//send a MessageChannel message
		
		private static var _current:Worker = new Worker();
		private static const _isSupported:Boolean = self["Worker"];
		private static var creating:Boolean = false;
		private static const createdWorkers:Object = {};
		private static var workerCreateCounter:uint = 0;

		private var _id:String = self["workerId"] || "main";
		private var sharedPropertiesData:*;
		private var sharedProperties:Object = {};//self["workerSharedProperties"]
		//private const sendingMessageChannels:Dictionary = new Dictionary();
		//private const receivingMessageChannels:Dictionary = new Dictionary();
		private var _isPrimordial:Boolean = false;
		private var _state:String;
		private var _worker:*;
		private var url:URL;
		
		public function Worker(url:URL=null) {
			if (_current) {
				if (url!=null) {
					if (!creating)
						throw new Error("The Worker class is not instanciable");
					creating = false;
					_id = _current._id + "." + (workerCreateCounter++);
					createdWorkers[_id] = this;
					sharedProperties = {};
					_state = WorkerState.NEW;
					this.url = url;
				}
			}
			else {//Init of the current worker
				_current = this;
				this._state = WorkerState.RUNNING;
				this._isPrimordial = !!self.document;
				worker = self;
				if (!this._isPrimordial) {
					var spData:ArrayBuffer = worker["workerSharedProperties"];
					if (spData) {
						var d:ByteArray = new ByteArray();
						d.fromArrayBuffer(spData);
						while (d.bytesAvailable) {
							var key:String = d.readObject();
							var value:* = d.readObject();
							this.sharedProperties[key] = value;
						}
					}
				}
			}
		}

		internal function get id():String {
			return _id;
		}

		internal function set id(value:String):void {
			_id = id;
		}
		
		internal function get worker():* {
			return _worker;
		}
		
		internal function set worker(value:*):void {
			if (_worker != null) // remove listeners (only at the worker stop)
				_worker.removeEventListener("message", onMessage);
			_worker = value;
			if (_worker != null) // add listeners
				_worker.addEventListener("message", onMessage, false);
		}

		/**
		 * <p> Indicates whether this worker is the primordial worker. </p>
		 * <p>The <i>primordial worker</i> is the worker in which the initial swf is running. This worker controls the rendering to the screen.</p>
		 * <p>This property can be used to architect an application where the primordial worker and the background worker are two instances of the same swf file. The alternative is to structure your code so that the background worker uses different code compiled to a different swf from the primorial worker.</p>
		 * 
		 * @return 
		 */
		public function get isPrimordial():Boolean {
			return _isPrimordial;
		}

		/**
		 * <p> The current state of the worker in its lifecycle. The possible values for this property are defined in the WorkerState class. </p>
		 * 
		 * @return 
		 */
		public function get state():String {
			return _state;
		}

		/**
		 * <p> Creates a new MessageChannel instance to send messages from the worker on which the method is called to another receiver worker. Code in the worker that creates the MessageChannel object can use it to send one-way messages to the Worker object specified as the <code>receiver</code> argument. </p>
		 * <p>Although a MessageChannel instance can be used to send messages and data from one Worker instance to another, at least one MessageChannel instance needs to be passed to a child Worker as a shared property by calling the Worker object's <code>setSharedProperty()</code> method.</p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *     outgoingChannel = Worker.current.createMessageChannel(bgWorker);
		 *     incomingChannel = bgWorker.createMessageChannel(Worker.current);
		 *     
		 *     bgWorker.setSharedProperty("incoming", outgoingChannel);
		 *     bgWorker.setSharedProperty("outgoing", incomingChannel);
		 *     
		 *     // listen for messages from the receiving MessageChannel
		 *     // This event is triggered when the background sends a message to this worker
		 *     incomingChannel.addEventListener(Event.CHANNEL_MESSAGE, incomingMessageHandler);</pre>
		 * </div>
		 * 
		 * @param receiver  — The worker that will receive messages transmitted via the created message channel 
		 * @return  — The MessageChannel object created by the operation 
		 */
		public function createMessageChannel(receiver:Worker):MessageChannel {
			return new MessageChannel(this, receiver);
		}

		/**
		 * <p> Retrieves a value stored in this worker with a named key. </p>
		 * <p>Code in a child worker can call this method to retrieve a value as early as in the constructor of the worker swf's main class.</p>
		 * 
		 * @param key  — The name of the shared property to retrieve 
		 * @return  — The shared property value stored with the specified key, or  if no value is stored for the specified key 
		 */
		public function getSharedProperty(key:String):* {
			return this.sharedProperties[key];
		}

		/**
		 * <p> Provides a named value that is available to code running in the worker's swf. </p>
		 * <p>You can call this method before calling the worker's <code>start()</code> method. In that case the shared property is available to code in the worker's swf at construction time.</p>
		 * <p>The value passed to the <code>value</code> parameter can be almost any object. Other than the exceptions noted below, any object that is passed to the <code>value</code> parameter is not passed by reference. Any changes made to the object in one worker after <code>setSharedProperty()</code> is called are not carried over to the other worker. The object is copied by serializing it to AMF3 format and deserializing it into a new object in the receiving worker. For this reason, any object that can't be serialized in AMF3 format, including display objects, can't be passed to the <code>value</code> parameter. In order for a custom class to be passed properly, the class definition must be registered using the <code>flash.net.registerClassAlias()</code> function or <code>[RemoteClass]</code> metadata. With either technique the same alias must be used for both worker's versions of the class.</p>
		 * <p>There are five types of objects that are an exception to the rule that objects aren't shared between workers:</p>
		 * <ul>
		 *  <li>Worker</li>
		 *  <li>MessageChannel</li>
		 *  <li>shareable ByteArray (a ByteArray object with its <code>shareable</code> property set to <code>true</code></li>
		 *  <li>Mutex</li>
		 *  <li>Condition</li>
		 * </ul>
		 * <p>If you pass an instance of these objects to the <code>value</code> parameter, each worker has a reference to the same underlying object. Changes made to an instance in one worker are immediately available in other workers. In addition, if you pass the same instance of these objects more than once using <code>setSharedProperty()</code>, the runtime doesn't create a new copy of the object in the receiving worker. Instead, the same reference is re-used, reducing system memory use.</p>
		 * <p>Calling this method with <code>null</code> or <code>undefined</code> for the <code>value</code> argument clears any previously-set value for the specified <code>key</code> argument. Cleaning up a value in this way removes the reference to it, allowing it to be garbage collected.</p>
		 * <p>You can use any String value in the key argument. These shared properties are available to any code that has access to a worker. To avoid unintentionally overwriting a value, consider using a prefix, suffix, or similar mechanism to attempt to make your key names unique.</p>
		 * 
		 * @param key  — The name under which the shared property is stored. 
		 * @param value  — The value of the shared property. 
		 */
		public function setSharedProperty(key:String, value:*):void {
			if (value == null || value == undefined)
				delete this.sharedProperties[key];
			else 
				this.sharedProperties[key] = value;
			if (state==WorkerState.RUNNING) {
				var ba:ByteArray = new ByteArray();
				ba.writeObject(value);

				this.worker.postMessage({
					cmd:SET_SHARED_PROPERTY, 
					key: key,
					value: ba.toArrayBuffer()
				});
			}
			else {
				if (!this.sharedPropertiesData)
					this.sharedPropertiesData = new ByteArray();
				this.sharedPropertiesData.writeObject(key);
				this.sharedPropertiesData.writeObject(value);
			}
		}

		/**
		 * <p> Starts the execution of the worker. The runtime creates the worker thread and calls the constructor of the worker swf's main class. </p>
		 * <p>This operation is asynchronous. Once the worker startup is complete, it changes its <code>state</code> property to <code>WorkerState.RUNNING</code> and dispatches a <code>workerState</code> event.</p>
		 */
		public function start():void {
			if (state != WorkerState.NEW)
				return;
			this.worker = new self["Worker"](url);
			var spData:ArrayBuffer = null;
			if (this.sharedPropertiesData!=null) {
				spData = this.sharedPropertiesData.toArrayBuffer();
				this.sharedPropertiesData = null;
			}
			this.worker.postMessage({
				id: this.id,
				location: "" + location,
				sharedProperties: spData
			});
		}
		
		private function onMessage(e:*):void {
			var msg:* = e.data;
			var cmd:int = msg.cmd;
			switch (cmd) {
				case SET_SHARED_PROPERTY:
					var ba:ByteArray = new ByteArray();
					ba.fromArrayBuffer(msg.value);
					var value:* = ba.readObject();
					if (value == null || value == undefined)
						delete this.sharedProperties[msg.key];
					else {
						this.sharedProperties[msg.key] = value;
						
					}
				break;
				case SEND_MESSAGE:
					//TODO: manage receiving message (maybe not for the current worker)
					if (msg.receiver!=_current.id)
						sendMessage(msg.channel, msg.sender, msg.receiver, msg.message);
					else {
						MessageChannel.receiveMessage(msg.channel, msg.message);
					}
				break;
			}
		}

		/**
		 * <p> Stops this worker's code execution. Calling this method aborts any current ActionScript in the worker's swf. </p>
		 * 
		 * @return  —  if code in the worker was running and interrupted, or  if the worker was never started 
		 */
		public function terminate():Boolean {
			if (state==WorkerState.RUNNING) {
				this.worker.close();
				return true;
			}
			return false;
		}


		/**
		 * <p> Provides access to the worker that contains the current code </p>
		 * 
		 * @return 
		 */
		public static function get current():Worker {
			return _current;
		}


		/**
		 * <p> Indicates whether the current runtime context supports the use of Worker objects for concurrent code execution. </p>
		 * <p>If concurrency is available, this property's value is <code>true</code>.</p>
		 * 
		 * @return 
		 */
		public static function get isSupported():Boolean {
			return _isSupported;
		}
		
		internal static function createWorker(ba:ByteArray):Worker {
			creating = true;
			ba.position = 0;
			return new Worker(BlobUtil.blobToURL(BlobUtil.jsStringToBlob(ba.readUTFBytes(ba.length))));
		}

		internal static function getWorker(id:String):Worker {
			if (id in createdWorkers)
				return createdWorkers[id];
			if (id==_current.id)
				return _current;
			return null;
		}

		internal static function sendMessage(channelId:String, senderId:String, receiverId:String, messageData:ArrayBuffer):void {
			// get the worker that will post the message
			var senderWorker:Worker = getWorker(getSender(_current.id, receiverId));
			if (senderWorker==null)
				throw new Error("The sender worker was not found in " + _current.id + " to send a message from " + senderId + " to " + receiverId);

			//TODO: send the message
			senderWorker.worker.postMessage({
				cmd:SEND_MESSAGE,
				channel: channelId,
				sender: senderId,
				receiver: receiverId,
				message: messageData
			});
		}

		private static function getSender(cId:String, receiverId:String):String {
			if (receiverId.indexOf(cId+".")!=0)
				return cId;
			var len:int = cId.length+1;
			var ind:int =  receiverId.indexOf(".", len);
			if (ind==-1)
				return receiverId;
			return receiverId.substring(0, ind);
		}
	}
}
