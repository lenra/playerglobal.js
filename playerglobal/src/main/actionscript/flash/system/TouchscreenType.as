package flash.system {
	/**
	 *  The TouchscreenType class is an enumeration class that provides values for the different types of touch screens. <p>Use the values defined by the TouchscreenType class with the <code>Capabilities.touchscreenType</code> property.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Capabilities.html#touchscreenType" target="">Capabilities.touchscreenType</a>
	 *  <br>
	 *  <a href="../../flash/ui/Mouse.html#supportsCursor" target="">flash.ui.Mouse.supportsCursor</a>
	 * </div><br><hr>
	 */
	public class TouchscreenType {
		/**
		 * <p> A touchscreen designed to respond to finger touches. </p>
		 */
		public static const FINGER:String = "finger";
		/**
		 * <p> The computer or device does not have a supported touchscreen. </p>
		 */
		public static const NONE:String = "none";
		/**
		 * <p> A touchscreen designed for use with a stylus. </p>
		 */
		public static const STYLUS:String = "stylus";
	}
}
