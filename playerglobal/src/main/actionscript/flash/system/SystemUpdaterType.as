package flash.system {
	/**
	 *  The SystemUpdaterType class provides constants for a system update. These constants are used in the <code>SystemUpdater.update()</code> function. <p> <b>Note</b>: The SystemUpdater API is supported on desktop platforms.</p> <br><hr>
	 */
	public class SystemUpdaterType {
		/**
		 * <p> Updates the DRM module. </p>
		 */
		public static const DRM:String = "drm";
		/**
		 * <p> Updates the player runtime itself. </p>
		 */
		public static const SYSTEM:String = "system";
	}
}
