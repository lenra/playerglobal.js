package flash.system {
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.utils.ByteArray;

	[Event(name="channelMessage", type="flash.events.Event")]
	[Event(name="channelState", type="flash.events.Event")]
	/**
	 *  The MessageChannel class provides a mechanism for a worker to communicate with another worker. A message channel is a one-way communication channel. The message channel's sending worker uses the message channel to send objects to the receiving worker. A MessageChannel object is the only way to send a message between workers that dispatches an event indicating to the receiver that the message is available. Other mechanisms for sharing data allow a value to be set but do not provide an event to notify you of the changed data. <p>Each MessageChannel object contains a queue of message objects sent from the sending worker to the receiving worker. Each call to <code>send()</code> adds an object to the queue. Each call to <code>receive()</code> retrieves the oldest message object from the queue.</p> <p>You do not create MessageChannel instances directly by calling the <code>MessageChannel()</code> constructor. To create a MessageChannel instance, call the <code>createMessageChannel()</code> method of the Worker object that will send messages on the channel, passing the receiving Worker object as an argument.</p> <p>The typical workflow for sending messages with a MessageChannel object is as follows:</p> <ol> 
	 *  <li> <p>Call the sending worker's <code>createMessageChannel()</code> method to create the message channel</p> 
	 *   <div class="listing">
	 *    <div class="clipcopy">
	 *     <a href="#" class="copyText">Copy</a>
	 *    </div>
	 *    <pre>
	 *     // In the sending worker swf
	 *     var sendChannel:MessageChannel;
	 *     sendChannel = Worker.current.createMessageChannel(receivingWorker);</pre>
	 *   </div> </li> 
	 *  <li> <p>Pass the message channel to the other worker, either by calling <code>Worker.setSharedProperty()</code> or by sending it through an existing message channel</p> 
	 *   <div class="listing">
	 *    <div class="clipcopy">
	 *     <a href="#" class="copyText">Copy</a>
	 *    </div>
	 *    <pre>
	 *     receivingWorker.setSharedProperty("incomingChannel", sendChannel);</pre>
	 *   </div> </li> 
	 *  <li> <p>Code in the receiving worker registers a listener with the MessageChannel object for the <code>channelMessage</code> event</p> 
	 *   <div class="listing">
	 *    <div class="clipcopy">
	 *     <a href="#" class="copyText">Copy</a>
	 *    </div>
	 *    <pre>
	 *     // In the receiving worker swf
	 *     var incomingChannel:MessageChannel;
	 *     incomingChannel = Worker.current.getSharedProperty("incomingChannel");
	 *     incomingChannel.addEventListener(Event.CHANNEL_MESSAGE, handleIncomingMessage);</pre>
	 *   </div> </li> 
	 *  <li> <p>Code in the sending worker sends a message by calling the <code>send()</code> method</p> 
	 *   <div class="listing">
	 *    <div class="clipcopy">
	 *     <a href="#" class="copyText">Copy</a>
	 *    </div>
	 *    <pre>
	 *     // In the sending worker swf
	 *     sendChannel.send("This is a message");</pre>
	 *   </div> </li> 
	 *  <li> <p>The runtime calls the event handler in the receiving worker code, indicating that a message has been sent</p> 
	 *   <div class="listing">
	 *    <div class="clipcopy">
	 *     <a href="#" class="copyText">Copy</a>
	 *    </div>
	 *    <pre>
	 *     // In the receiving worker swf
	 *     // This method is called when the message channel gets a message
	 *     private function handleIncomingMessage(event:Event):void
	 *     {
	 *         // Do something with the message, as shown in the next code listing
	 *     }</pre>
	 *   </div> </li> 
	 *  <li> <p>Code in the receiving worker calls the <code>receive()</code> method to get the message. The object returned by the <code>receive()</code> method has the same data type as the object passed to the <code>send()</code> method.</p> 
	 *   <div class="listing">
	 *    <div class="clipcopy">
	 *     <a href="#" class="copyText">Copy</a>
	 *    </div>
	 *    <pre>
	 *     var message:String = incomingChannel.receive() as String;</pre>
	 *   </div> </li> 
	 * </ol> <p>In addition to the asynchronous workflow outlined above, you can use an alternative workflow with the <code>receive()</code> method to pause the code in the receiving worker and wait until a message is sent. See the <code>receive()</code> method description for more information.</p> <p>The MessageChannel class is one of the special object types that are shared between workers rather than copied between them. When you pass a message channel from one worker to another worker either by calling the Worker object's <code>setSharedProperty()</code> method or by using a MessageChannel object, both workers have a reference to the same MessageChannel object in the runtime's memory.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Worker.html" target="">Worker class</a>
	 *  <br>
	 *  <a href="Worker.html#createMessageChannel()" target="">Worker.createMessageChannel() method</a>
	 * </div><br><hr>
	 */
	public class MessageChannel extends EventDispatcher {
		private static const channels:Object = {};
		private var _id:String;
		private const _messages:Array = [];
		private var _state:String;
		private var senderWorkerId:String;
		private var receiverWorkerId:String;

		public function MessageChannel(from:Worker = null, to:Worker = null) {
			if (from!=null) {
				this.senderWorkerId = from.id;
				this.receiverWorkerId = to.id;
				this._id = this.senderWorkerId + "->" + this.receiverWorkerId + ":" + (new Date().getTime());
				var tmp:String = _id;
				var cnt:int = 1;
				while (tmp in channels)
					tmp = this._id + "_" + (cnt++);
				this._id = tmp;
				channels[this._id] = this;
			}
		}

		public function get id():String {
			return _id;
		}

		public function set id(value:String):void {
			if (!_id) {
				_id = value;
				// parse the id to get the worker ids
				var workers:Array = value.split(":")[0].split("->");
				this.senderWorkerId = workers[0];
				this.receiverWorkerId = workers[1];
				channels[this._id] = this;
			}
		}

		/**
		 * <p> Indicates whether the MessageChannel has one or more messages from the sending worker in its internal message queue. </p>
		 * 
		 * @return 
		 */
		public function get messageAvailable():Boolean {
			return _messages.length;
		}

		/**
		 * <p> Indicates the current state of the MessageChannel object (open, closing, or closed). The possible values for this property are defined as constants in the MessageChannelState class. </p>
		 * 
		 * @return 
		 */
		public function get state():String {
			return _state;
		}

		/**
		 * <p> Instructs the current MessageChannel to close once all messages have been received. </p>
		 * <p>Once you call this method, you can no longer call the <code>send()</code> method to add messages to the queue. The <code>send()</code> call will fail and return <code>false</code>.</p>
		 * <p>You can also only call the <code>receive()</code> method to receive messages that are already waiting in the queue. If the queue is empty, the <code>receive()</code> call will return <code>null</code>.</p>
		 */
		public function close():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Retrieves a single message object from the queue of messages sent through this message channel. </p>
		 * <p>Each time the sending worker's code calls the MessageChannel object's <code>send()</code> method, a single object is added to the message channel's internal message queue. These objects stack up in the queue until they are removed one at a time by the receiving worker calling the <code>receive()</code> method. The message objects are received in the order they are sent.</p>
		 * <p>To check if the queue contains a message object to receive, use the <code>messageAvailable</code> property.</p>
		 * <p>In the standard case, the object passed into <code>send()</code> is serialized in AMF3 format. When it is removed from the queue by the <code>receive()</code> call, it is deserialized into an ActionScript object (a copy of the original object) in the receiving worker and the worker receives a reference to that copy. Certain types of objects are shared between workers rather than copied. In that case the object that the receiving worker gets is a reference to the shared object itself rather than a new copy of the object. For more information about this case see the <code>send()</code> method description.</p>
		 * <p>The method's behavior changes if the message queue is empty and you pass <code>true</code> for the <code>blockUntilReceived</code> parameter. In that case the worker pauses its execution thread at the <code>receive()</code> call and does not execute more code. Once the sending worker calls <code>send()</code>, the <code>receive()</code> call completes by receiving the message. The worker then resumes code execution at the next line of code following the receive call.</p>
		 * 
		 * @param blockUntilReceived  — indicates whether the worker's execution thread should receive a message object and then continue execution (<code>false</code>), or if it should pause at the <code>receive()</code> call and wait for a message to be sent if the queue is empty (<code>true</code>) 
		 * @return  — a copy of the object passed into the  method by the sending worker. If the object is one of the special types that are shared between workers, the return value is a reference to the shared object rather than to a copy of it. If no message is available on the queue, the method returns . 
		 */
		public function receive(blockUntilReceived:Boolean = false):* {
			if (!messageAvailable) {
				if (blockUntilReceived)
					trace("MessageChannel.receive does not manage the blockUntilReceived parameter");
				return null;
			}
			return _messages.shift();
		}

		/**
		 * <p> Sends an object from the sending worker, adding it to the message queue for the receiving worker. </p>
		 * <p>The object passed to the <code>arg</code> parameter can be almost any object. Other than the exceptions noted below, any object that is passed to the <code>arg</code> parameter is not passed by reference. Any changes made to the object in one worker after <code>send()</code> is called are not carried over to the other worker. The object is copied by serializing it to AMF3 format and deserializing it into a new object in the receiving worker when <code>receive()</code> is called. For this reason, any object that can't be serialized in AMF3 format, including display objects, can't be passed to the <code>arg</code> parameter. In order for a custom class to be passed properly, the class definition must be registered using the <code>flash.net.registerClassAlias()</code> function or <code>[RemoteClass]</code> metadata. With either technique the same alias must be used for both worker's versions of the class.</p>
		 * <p>There are five types of objects that are an exception to the rule that objects aren't shared between workers:</p>
		 * <ul>
		 *  <li>Worker</li>
		 *  <li>MessageChannel</li>
		 *  <li>shareable ByteArray (a ByteArray object with its <code>shareable</code> property set to <code>true</code></li>
		 *  <li>Mutex</li>
		 *  <li>Condition</li>
		 * </ul>
		 * <p>If you pass an instance of these objects to the <code>arg</code> parameter, each worker has a reference to the same underlying object. Changes made to an instance in one worker are immediately available in other workers. In addition, if you pass the same instance of these objects more than once using <code>send()</code>, the runtime doesn't create a new copy of the object in the receiving worker. Instead, the same reference is re-used, reducing system memory use.</p>
		 * <p>By default, this method adds the object to the queue and immediately returns, continuing execution with the next line of code. If you want to prevent the queue from growing beyond a certain size, you can use the <code>queueLimit</code> parameter to specify the maximum number of items to allow in the queue. If at the time you call <code>send()</code> the number of items in the queue is greater than the limit you specify, the worker pauses the execution thread at the <code>send()</code> call. Once the receiving worker calls <code>receive()</code> enough times that the queue size is less than the specified queue limit, the send() call completes. The worker then continues execution at the next line of code.</p>
		 * 
		 * @param arg  — the object to add to the message queue 
		 * @param queueLimit  — the maximum number of message objects that the message queue can contain. If the queue contains more objects than the limit, the sending worker pauses execution until messages are received and the queue size drops below the limit. 
		 */
		public function send(arg:*, queueLimit:int = -1):void {
			if (this.senderWorkerId!=Worker.current.id)
				throw new Error("The current worker is not the sender");
			if (queueLimit!=-1)
				console.warn("The queueLimit parameter of MessageChannel.send method is not managed");
			var ba:ByteArray = new ByteArray();
			ba.writeObject(arg);
			Worker.sendMessage(this._id, this.senderWorkerId, this.receiverWorkerId, ba.toArrayBuffer());
		}

		internal static function receiveMessage(channelId:String, messageData:ArrayBuffer):void {
			// get the message channel
			var channel:MessageChannel = channels[channelId];
			if (!channel)
				throw new Error("The message channel was not found in " + Worker.current.id + " for this id " + channelId);
			

			// add the message
			var ba:ByteArray = new ByteArray();
			ba.fromArrayBuffer(messageData);

			channel._messages.push(ba.readObject());
			channel.dispatchEvent(new flash.events.Event(flash.events.Event.CHANNEL_MESSAGE));
		}
	}
}
