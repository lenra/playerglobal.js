package flash.system {
	/**
	 *  The SecurityPanel class provides values for specifying which Security Settings panel you want to display. <p>This class contains static constants that are used with the <code>Security.showSettings()</code> method. You cannot create new instances of the SecurityPanel class.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><br><hr>
	 */
	public class SecurityPanel {
		/**
		 * <p> When passed to <code>Security.showSettings()</code>, displays the Camera panel in Flash Player Settings. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Security.html#showSettings()" target="">Security.showSettings()</a>
		 * </div>
		 */
		public static const CAMERA:String = "camera";
		/**
		 * <p> When passed to <code>Security.showSettings()</code>, displays the panel that was open the last time the user closed the Flash Player Settings. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Security.html#showSettings()" target="">Security.showSettings()</a>
		 * </div>
		 */
		public static const DEFAULT:String = "default";
		/**
		 * <p> When passed to <code>Security.showSettings()</code>, displays the Display panel in Flash Player Settings. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Security.html#showSettings()" target="">Security.showSettings()</a>
		 * </div>
		 */
		public static const DISPLAY:String = "display";
		/**
		 * <p> When passed to <code>Security.showSettings()</code>, displays the Local Storage Settings panel in Flash Player Settings. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Security.html#showSettings()" target="">Security.showSettings()</a>
		 * </div>
		 */
		public static const LOCAL_STORAGE:String = "localStorage";
		/**
		 * <p> When passed to <code>Security.showSettings()</code>, displays the Microphone panel in Flash Player Settings. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Security.html#showSettings()" target="">Security.showSettings()</a>
		 * </div>
		 */
		public static const MICROPHONE:String = "microphone";
		/**
		 * <p> When passed to <code>Security.showSettings()</code>, displays the Privacy Settings panel in Flash Player Settings. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Security.html#showSettings()" target="">Security.showSettings()</a>
		 * </div>
		 */
		public static const PRIVACY:String = "privacy";
		/**
		 * <p> When passed to <code>Security.showSettings()</code>, displays the Settings Manager (in a separate browser window). </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Security.html#showSettings()" target="">Security.showSettings()</a>
		 * </div>
		 */
		public static const SETTINGS_MANAGER:String = "settingsManager";
	}
}
