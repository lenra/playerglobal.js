package flash.system {
	/**
	 *  This class defines constants that represent the possible values of the Worker class's <code>state</code> property. These values are the states in a Worker object's lifecycle. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Worker.html" target="">Worker class</a>
	 *  <br>
	 *  <a href="Worker.html#state" target="">Worker.state property</a>
	 * </div><br><hr>
	 */
	public class WorkerState {
		/**
		 * <p> This state indicates that an object that represents the new worker has been created, but the worker is not executing code. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Worker.html#state" target="">Worker.state property</a>
		 * </div>
		 */
		public static const NEW:String = "new";
		/**
		 * <p> This state indicates that the worker has begun executing application code and it has not been instructed to stop execution. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Worker.html#state" target="">Worker.state property</a>
		 * </div>
		 */
		public static const RUNNING:String = "running";
		/**
		 * <p> This state indicates that the worker has been stopped by code in another worker calling this Worker object's <code>terminate()</code> method. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Worker.html#state" target="">Worker.state property</a>
		 * </div>
		 */
		public static const TERMINATED:String = "terminated";
	}
}
