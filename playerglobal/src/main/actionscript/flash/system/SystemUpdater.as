package flash.system {
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.events.StatusEvent;

	[Event(name="cancel", type="flash.events.Event")]
	[Event(name="complete", type="flash.events.Event")]
	[Event(name="ioError", type="flash.events.IOErrorEvent")]
	[Event(name="open", type="flash.events.Event")]
	[Event(name="progress", type="flash.events.ProgressEvent")]
	[Event(name="securityError", type="flash.events.SecurityErrorEvent")]
	[Event(name="status", type="flash.events.StatusEvent")]
	/**
	 *  The SystemUpdater class allows you to update modules of the Flash Player, such as the DRM module for Adobe Access, as well as the Flash Player itself. Available modules are listed in the SystemUpdaterType class. <p>Flash Player identifies the need for a Adobe-Access-module update by dispatching a NetStatusEvent event. The event has a <code>code</code> property with a value of <code>"DRM.UpdateNeeded"</code>. For updates to the Adobe Access module, user consent is not required. Listen for the event and initiate the update by calling <code>update("DRM")</code>.</p> <p>Flash Player identifies the need for a player update by dispatching a StatusEvent event, with several possible <code>code</code> property values (see the <code>status</code> event). For updates to the player, user consent is required. Listen for the event and present the user with the option to update. The user must agree to the actual update and initiate the update by, for example, clicking a button in the user interface. You can then initiate the player update directly in ActionScript by calling <code>update("SYSTEM")</code>.</p> <p> <b>Note</b>: The SystemUpdater API is supported on all desktop platforms.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="SystemUpdaterType.html" target="">flash.system.SystemUpdaterType</a>
	 * </div><br><hr>
	 */
	public class SystemUpdater extends EventDispatcher {

		/**
		 * <p> Constructor. </p>
		 */
		public function SystemUpdater() {
			super();
			throw new Error("Not implemented");
		}

		/**
		 * <p> Cancels an active update. </p>
		 */
		public function cancel():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Begins an update of a given type. Update types are one of the string constants defined in the SystemUpdaterType class. Only one update is allowed at a time across all browsers. </p>
		 * <p>After the update begins, listen for the events defined in this class. The following events events indicate the end of an update and allow a new update or update attempt to proceed, as does calling the <code>update()</code> function:</p>
		 * <ul>
		 *  <li><code>complete</code></li>
		 *  <li><code>cancel</code></li>
		 *  <li><code>securityError</code></li>
		 *  <li><code>ioError</code></li>
		 *  <li><code>status</code></li>
		 * </ul>
		 * 
		 * @param type
		 */
		public function update(type:String):void {
			throw new Error("Not implemented");
		}
	}
}
