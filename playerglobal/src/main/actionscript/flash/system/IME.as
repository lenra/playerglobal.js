package flash.system {
	import flash.events.EventDispatcher;
	import flash.events.IMEEvent;

	[Event(name="imeComposition", type="flash.events.IMEEvent")]
	/**
	 *  The IME class lets you directly manipulate the operating system's input method editor (IME) in the Flash runtime application that is running on a client computer. You can determine whether an IME is installed, whether or not the IME is currently enabled, and which IME is enabled. You can disable or enable the IME in the application, and you can perform other limited functions, depending on the operating system. <p> <i>AIR profile support:</i> This feature is supported on desktop operating systems, but it is not supported on all mobile devices. It is also not supported on AIR for TV devices. You can test for support at run time using the <code>IME.isSupported</code> property. See <a href="http://help.adobe.com/en_US/air/build/WS144092a96ffef7cc16ddeea2126bb46b82f-8000.html"> AIR Profile Support</a> for more information regarding API support across multiple profiles.</p> <p>IMEs let users type non-ASCII text characters in multibyte languages such as Chinese, Japanese, and Korean. For more information on working with IMEs, see the documentation for the operating system for which you are developing applications. For additional resources, see the following websites: </p><ul> 
	 *  <li> <a href="http://www.microsoft.com/globaldev/default.mspx" target="external">http://www.microsoft.com/globaldev/default.mspx</a> </li> 
	 *  <li> <a href="http://developer.apple.com/documentation/" target="external">http://developer.apple.com/documentation/</a> </li> 
	 *  <li> <a href="http://java.sun.com" target="external">http://java.sun.com</a> </li> 
	 * </ul>  <p>If an IME is not active on the user's computer, calls to IME methods or properties, other than <code>Capabilities.hasIME</code>, will fail. Once you manually activate an IME, subsequent ActionScript calls to IME methods and properties will work as expected. For example, if you are using a Japanese IME, it must be activated before any IME method or property is called.</p> <p>The following table shows the platform coverage of this class:</p> <table class="innertable">
	 *  <tbody>
	 *   <tr>
	 *    <th>Capability</th>
	 *    <th>Windows</th>
	 *    <th>Mac OSX</th>
	 *    <th>Linux</th>
	 *   </tr>
	 *   <tr>
	 *    <td>Determine whether the IME is installed: <code>Capabilities.hasIME</code> </td>
	 *    <td>Yes</td>
	 *    <td>Yes</td>
	 *    <td>Yes</td>
	 *   </tr>
	 *   <tr>
	 *    <td>Set IME on or off: <code>IME.enabled</code> </td>
	 *    <td>Yes</td>
	 *    <td>Yes</td>
	 *    <td>Yes</td>
	 *   </tr>
	 *   <tr>
	 *    <td>Find out whether IME is on or off: <code>IME.enabled</code> </td>
	 *    <td>Yes</td>
	 *    <td>Yes</td>
	 *    <td>Yes</td>
	 *   </tr>
	 *   <tr>
	 *    <td>Get or set IME conversion mode: <code>IME.conversionMode</code> </td>
	 *    <td>Yes</td>
	 *    <td>Yes **</td>
	 *    <td>No</td>
	 *   </tr>
	 *   <tr>
	 *    <td>Send IME the string to be converted: <code>IME.setCompositionString()</code> </td>
	 *    <td>Yes *</td>
	 *    <td>No</td>
	 *    <td>No</td>
	 *   </tr>
	 *   <tr>
	 *    <td>Get from IME the original string before conversion: <code>System.ime.addEventListener()</code> </td>
	 *    <td>Yes *</td>
	 *    <td>No</td>
	 *    <td>No</td>
	 *   </tr>
	 *   <tr>
	 *    <td>Send request to convert to IME: <code>IME.doConversion()</code> </td>
	 *    <td>Yes *</td>
	 *    <td>No</td>
	 *    <td>No</td>
	 *   </tr>
	 *  </tbody>
	 * </table> <p>* Not all Windows IMEs support all of these operations. The only IME that supports them all is the Japanese IME.</p> <p>** On the Macintosh, only the Japanese IME supports these methods, and third-party IMEs do not support them.</p> <p>The ActionScript 3.0 version of this class does not support Macintosh Classic.</p> <p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7cd5.html" target="_blank">Using the IME class</a>
	 * </div><br><hr>
	 */
	public class IME extends EventDispatcher {
		private static var _conversionMode:String;
		private static var _enabled:Boolean;

		private static var _isSupported:Boolean;


		/**
		 * <p> The conversion mode of the current IME. Possible values are IME mode string constants that indicate the conversion mode: </p>
		 * <ul>
		 *  <li><code>ALPHANUMERIC_FULL</code></li>
		 *  <li><code>ALPHANUMERIC_HALF</code></li>
		 *  <li><code>CHINESE</code></li>
		 *  <li><code>JAPANESE_HIRAGANA</code></li>
		 *  <li><code>JAPANESE_KATAKANA_FULL</code></li>
		 *  <li><code>JAPANESE_KATAKANA_HALF</code></li>
		 *  <li><code>KOREAN</code></li>
		 *  <li><code>UNKNOWN</code> (read-only value; this value cannot be set)</li>
		 * </ul>
		 * 
		 * @return 
		 */
		public static function get conversionMode():String {
			return _conversionMode;
		}


		/**
		 * @param value
		 * @return 
		 */
		public static function set conversionMode(value:String):void {
			_conversionMode = value;
		}


		/**
		 * <p> Indicates whether the system IME is enabled (<code>true</code>) or disabled (<code>false</code>). An enabled IME performs multibyte input; a disabled IME performs alphanumeric input. </p>
		 * 
		 * @return 
		 */
		public static function get enabled():Boolean {
			return _enabled;
		}


		/**
		 * @param value
		 * @return 
		 */
		public static function set enabled(value:Boolean):void {
			_enabled = value;
		}


		/**
		 * <p> The <code>isSupported</code> property is set to <code>true</code> if the IME class is available on the current platform, otherwise it is set to <code>false</code>. </p>
		 * 
		 * @return 
		 */
		public static function get isSupported():Boolean {
			return _isSupported;
		}


		/**
		 * <p> Causes the runtime to abandon any composition that is in progress. Call this method when the user clicks outside of the composition area or when the interactive object that has focus is being destroyed or reset. The runtime confirms the composition by calling <code>confirmComposition()</code> in the client. The runtime also resets the IME to inform the operating system that the composition has been abandoned. </p>
		 */
		public static function compositionAbandoned():void {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Call this method when the selection within the composition has been updated, either interactively or programmatically. </p>
		 * 
		 * @param start  — Specifies the offset in bytes of the start of the selection. 
		 * @param end  — Specifies the offset in bytes of the end of the selection. 
		 */
		public static function compositionSelectionChanged(start:int, end:int):void {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Instructs the IME to select the first candidate for the current composition string. </p>
		 */
		public static function doConversion():void {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Sets the IME composition string. When this string is set, the user can select IME candidates before committing the result to the text field that currently has focus. </p>
		 * <p>If no text field has focus, this method fails and throws an error.</p>
		 * 
		 * @param composition  — The string to send to the IME. 
		 */
		public static function setCompositionString(composition:String):void {
			throw new Error("Not implemented");
		}
	}
}
