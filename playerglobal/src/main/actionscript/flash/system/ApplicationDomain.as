package flash.system {
	import flash.utils.ByteArray;

	/**
	 *  The ApplicationDomain class is a container for discrete groups of class definitions. Application domains are used to partition classes that are in the same security domain. They allow multiple definitions of the same class to exist and allow children to reuse parent definitions. <p>Application domains are used when an external SWF file is loaded through the Loader class. All ActionScript 3.0 definitions in the loaded SWF file are stored in the application domain, which is specified by the <code>applicationDomain</code> property of the LoaderContext object that you pass as a <code>context</code> parameter of the Loader object's <code>load()</code> or <code>loadBytes()</code> method. The LoaderInfo object also contains an <code>applicationDomain</code> property, which is read-only.</p> <p>All code in a SWF file is defined to exist in an application domain. The current application domain is where your main application runs. The system domain contains all application domains, including the current domain, which means that it contains all Flash Player classes.</p> <p>Every application domain, except the system domain, has an associated parent domain. The parent domain of your main application's application domain is the system domain. Loaded classes are defined only when their parent doesn't already define them. You cannot override a loaded class definition with a newer definition.</p> <p>For usage examples of application domains, see the <i>ActionScript 3.0 Developer's Guide</i>.</p> <p>The <code>ApplicationDomain()</code> constructor function allows you to create an ApplicationDomain object.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WSd75bf4610ec9e22f43855da312214da1d8f-8000.html" target="_blank">Working with application domains</a>
	 * </div><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS2db454920e96a9e51e63e3d11c0bf619ab-7ffc.html" target="_blank">About application domains</a>
	 *  <br>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../flash/display/Loader.html#load()" target="">flash.display.Loader.load()</a>
	 *  <br>
	 *  <a href="../../flash/display/Loader.html#loadBytes()" target="">flash.display.Loader.loadBytes()</a>
	 *  <br>
	 *  <a href="../../flash/display/LoaderInfo.html" target="">flash.display.LoaderInfo</a>
	 *  <br>
	 *  <a href="../../flash/net/URLRequest.html" target="">flash.net.URLRequest</a>
	 *  <br>
	 *  <a href="LoaderContext.html" target="">flash.system.LoaderContext</a>
	 * </div><br><hr>
	 */
	public class ApplicationDomain {
		private static var _MIN_DOMAIN_MEMORY_LENGTH:uint;
		private static var _currentDomain:ApplicationDomain;

		private var _domainMemory:ByteArray;

		private var _parentDomain:ApplicationDomain;

		/**
		 * <p> Creates a new application domain. </p>
		 * 
		 * @param parentDomain  — If no parent domain is passed in, this application domain takes the system domain as its parent. 
		 */
		public function ApplicationDomain(parentDomain:ApplicationDomain = null) {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Gets and sets the object on which domain-global memory operations will operate within this ApplicationDomain. </p>
		 * 
		 * @return 
		 */
		public function get domainMemory():ByteArray {
			return _domainMemory;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set domainMemory(value:ByteArray):void {
			_domainMemory = value;
		}

		/**
		 * <p> Gets the parent domain of this application domain. </p>
		 * 
		 * @return 
		 */
		public function get parentDomain():ApplicationDomain {
			return _parentDomain;
		}

		/**
		 * <p> Gets a public definition from the specified application domain. The definition can be that of a class, a namespace, or a function. </p>
		 * 
		 * @param name  — The name of the definition. 
		 * @return  — The object associated with the definition. 
		 */
		public function getDefinition(name:String):Object {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Gets all fully-qualified names of public definitions from the specified application domain. The definition can be that of a class, a namespace, or a function. The names returned from this method can be passed to the <code>getDefinition()</code> method to get the object of the actual definition. </p>
		 * <p>The returned Vector is of type String, where each String is in the form: <code><i>package</i>.<i>path</i>::<i>definitionName</i></code></p>
		 * <p>If <code><i>definitionName</i></code> is in the top-level package, then <code><i>package</i>.<i>path</i>::</code> is omitted.</p>
		 * <p>For example, for the following class definition:</p>
		 * <pre>
		 *      package my.Example
		 *      {
		 *        public class SampleClass extends Sprite
		 *        { }
		 *      }</pre>
		 * <p>This method returns "my.Example::SampleClass".</p>
		 * 
		 * @return  — An unsorted Vector of Strings which are the names of the definitions. If there is no definition, an empty Vector.&lt;String&gt; is returned. 
		 */
		public function getQualifiedDefinitionNames():Vector.<String> {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Checks to see if a public definition exists within the specified application domain. The definition can be that of a class, a namespace, or a function. </p>
		 * 
		 * @param name  — The name of the definition. 
		 * @return  — A value of  if the specified definition exists; otherwise, . 
		 */
		public function hasDefinition(name:String):Boolean {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Gets the minimum memory object length required to be used as ApplicationDomain.domainMemory. </p>
		 * 
		 * @return 
		 */
		public static function get MIN_DOMAIN_MEMORY_LENGTH():uint {
			return _MIN_DOMAIN_MEMORY_LENGTH;
		}


		/**
		 * <p> Gets the current application domain in which your code is executing. </p>
		 * 
		 * @return 
		 */
		public static function get currentDomain():ApplicationDomain {
			return _currentDomain;
		}
	}
}
