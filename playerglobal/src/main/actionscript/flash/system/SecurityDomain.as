package flash.system {
	/**
	 *  The SecurityDomain class represents the current security "sandbox," also known as a security domain. By passing an instance of this class to <code>Loader.load()</code>, you can request that loaded media be placed in a particular sandbox. <p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS2db454920e96a9e51e63e3d11c0bf69084-7f03.html" target="_blank">Specifying a LoaderContext</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS2db454920e96a9e51e63e3d11c0bf69084-7f0d.html" target="_blank">Developing sandboxed applications</a>
	 * </div><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS2db454920e96a9e51e63e3d11c0bf619ab-7ff2.html" target="_blank">About security domains</a>
	 *  <br>
	 * </div><br><hr>
	 */
	public class SecurityDomain {
		private static var _currentDomain:SecurityDomain;


		/**
		 * <p> Gets the current security domain. </p>
		 * 
		 * @return 
		 */
		public static function get currentDomain():SecurityDomain {
			return _currentDomain;
		}
	}
}
