package flash.system {
	/**
	 *  The JPEGLoaderContext class includes a property for enabling a deblocking filter when loading a JPEG image. The deblocking filter improves an image's quality at higher compression settings by smoothing neighboring pixels. To apply deblocking when loading a JPEG image, create a JPEGLoaderContext object, and set its <code>deblockingFilter</code> property. Then use the JPEGLoaderContext object name as the value of the <code>context</code> parameter of the <code>load()</code> method of the Loader object used to load the image. <p>The JPEGLoaderContext class extends the LoaderContext class. Set the <code>checkPolicyFile</code> property to <code>true</code> if you need programmatic access to the pixels of the loaded image (for example, if you're using the <code>BitmapData.draw()</code> method). Setting the <code>checkPolicyFile</code> property is not necessary for AIR content running in the application sandbox.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../flash/display/Loader.html#load()" target="">flash.display.Loader.load()</a>
	 *  <br>
	 *  <a href="../../flash/display/BitmapData.html#draw()" target="">flash.display.BitmapData.draw()</a>
	 * </div><br><hr>
	 */
	public class JPEGLoaderContext extends LoaderContext {
		private var _deblockingFilter:Number;

		/**
		 * <p> Creates a new JPEGLoaderContext object with the specified settings. </p>
		 * 
		 * @param deblockingFilter  — Specifies the strength of the deblocking filter. A value of 1.0 applies a full strength deblocking filter, a value of 0.0 disables the deblocking filter. 
		 * @param checkPolicyFile  — Specifies whether Flash Player should check for the existence of a URL policy file before loading the object. Does not apply for AIR content running in the application sandbox. 
		 * @param applicationDomain  — Specifies the ApplicationDomain object to use for a Loader object. 
		 * @param securityDomain  — Specifies the SecurityDomain object to use for a Loader object. 
		 */
		public function JPEGLoaderContext(deblockingFilter:Number = 0.0, checkPolicyFile:Boolean = false, applicationDomain:ApplicationDomain = null, securityDomain:SecurityDomain = null) {
			super(checkPolicyFile, applicationDomain, securityDomain);
			throw new Error("Not implemented");
		}

		/**
		 * <p> Specifies the strength of the deblocking filter. A value of 1.0 applies a full strength deblocking filter, a value of 0.0 disables the deblocking filter. </p>
		 * 
		 * @return 
		 */
		public function get deblockingFilter():Number {
			return _deblockingFilter;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set deblockingFilter(value:Number):void {
			_deblockingFilter = value;
		}
	}
}
