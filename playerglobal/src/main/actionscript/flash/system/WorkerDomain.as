package flash.system {
	import flash.utils.ByteArray;

	/**
	 *  The WorkerDomain class provides a way to create Worker objects and access them. A WorkerDomain represents the runtime's mechanism for managing the set of workers within a security domain. <p> <b>Note:</b> The use of workers for concurrency is supported in both Flash Player and AIR on desktop platforms. For mobile platforms, concurrency is supported in AIR on Android but not in AIR on iOS. You can use the static isSupported property to check whether concurrency is supported before attempting to use it.</p> <p>You do not create WorkerDomain instances directly by calling the <code>WorkerDomain()</code> constructor. There is one single WorkerDomain instance for an application. In contexts where the use of workers for concurrency is supported, the runtime automatically creates the WorkerDomain at startup. You access that instance using the static <code>current</code> property.</p> <p>To create a new instance of the Worker class, use the <code>createWorker()</code> method. To access the set of Worker objects that are currently running, use the <code>listWorkers()</code> method.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Worker.html" target="">flash.system.Worker</a>
	 * </div><br><hr>
	 */
	public class WorkerDomain {
		private static const _current:WorkerDomain = new WorkerDomain();
		
		private const workers:Vector.<Worker> = new Vector.<Worker>();

		public function WorkerDomain() {
			if (_current)
				throw new Error("The worker domain class is not instanciable");
		}
		
		/**
		 * <p> Creates a new <code>Worker</code> instance from the bytes of a swf. </p>
		 * <p>Each worker is created from and executes as an isolated swf application. To create a Worker instance, you obtain the bytes of the SWF file as a ByteArray instance and pass it to this method. There are three common ways to access the bytes of a swf for this purpose:</p>
		 * <ul>
		 *  <li><p>Use the [Embed] metatag to embed the .swf file in the application as a ByteArray:</p> 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>
		 *     // Embed the SWF file
		 *     [Embed(source="../swfs/BgWorker.swf", mimeType="application/octet-stream")]
		 *     private static var BgWorker_ByteClass:Class;
		 *     
		 *     private function createWorker():void
		 *     {
		 *       // create the background worker
		 *       var workerBytes:ByteArray = new BgWorker_ByteClass();
		 *       var bgWorker:Worker = WorkerDomain.current.createWorker(workerBytes);
		 *       
		 *       // listen for worker state changes to know when the worker is running
		 *       bgWorker.addEventListener(Event.WORKER_STATE, workerStateHandler);
		 *       
		 *       // set up communication between workers using 
		 *       // setSharedProperty(), createMessageChannel(), etc.
		 *       // ... (not shown)
		 *       
		 *       bgWorker.start();
		 *     }</pre>
		 *   </div> </li>
		 *  <li><p>Load an external SWF file using a URLLoader:</p> 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>
		 *     // load the SWF file
		 *     var workerLoader:URLLoader = new URLLoader();
		 *     workerLoader.dataFormat = URLLoaderDataFormat.BINARY;
		 *     workerLoader.addEventListener(Event.COMPLETE, loadComplete);
		 *     workerLoader.load(new URLRequest("BgWorker.swf"));
		 *     
		 *     private function loadComplete(event:Event):void
		 *     {
		 *       // create the background worker
		 *       var workerBytes:ByteArray = event.target.data as ByteArray;
		 *       var bgWorker:Worker = WorkerDomain.current.createWorker(workerBytes);
		 *       
		 *       // listen for worker state changes to know when the worker is running
		 *       bgWorker.addEventListener(Event.WORKER_STATE, workerStateHandler);
		 *       
		 *       // set up communication between workers using 
		 *       // setSharedProperty(), createMessageChannel(), etc.
		 *       // ... (not shown)
		 *       
		 *       bgWorker.start();
		 *     }</pre>
		 *   </div> </li>
		 *  <li><p>Use a single swf as both the primordial worker and the background worker:</p> 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>
		 *     // The primordial worker's main class constructor
		 *     public function PrimordialWorkerClass()
		 *     {
		 *       init();
		 *     }
		 *     
		 *     private function init():void
		 *     {
		 *       var swfBytes:ByteArray = this.loaderInfo.bytes;
		 *       
		 *       // Check to see if this is the primordial worker
		 *       if (Worker.current.isPrimordial)    
		 *       {
		 *         // create a background worker
		 *         var bgWorker:Worker = WorkerDomain.current.createWorker(swfBytes);
		 *         
		 *         // listen for worker state changes to know when the worker is running
		 *         bgWorker.addEventListener(Event.WORKER_STATE, workerStateHandler);
		 *         
		 *         // set up communication between workers using 
		 *         // setSharedProperty(), createMessageChannel(), etc.
		 *         // ... (not shown)
		 *         
		 *         bgWorker.start();
		 *       }
		 *       else // entry point for the background worker
		 *       {
		 *         // set up communication between workers using getSharedProperty()
		 *         // ... (not shown)
		 *         
		 *         // start the background work
		 *       }</pre>
		 *   </div> </li>
		 * </ul>
		 * <p>Creating a Worker object using <code>createWorker()</code> does not start execution of the worker. To start a worker's code execution, call the Worker object's <code>start()</code> method.</p>
		 * <p>Workers are useful because they decrease the chances of the frame rate dropping due to the main rendering thread being blocked by other code. However, workers require additional system memory and CPU use, which can be costly to overall application performance. Because each worker uses its own instance of the runtime virtual machine, even the overhead of a trivial worker can be large. When using workers, test your code across all your target platforms to ensure that the demands on the system are not too large. Adobe recommends that you do not use more than one or two background workers in a typical scenario.</p>
		 * 
		 * @param swf  — A ByteArray containing the bytes of a valid swf 
		 * @param giveAppPrivileges  — indicates whether the worker should be given application sandbox privileges in AIR. This parameter is ignored in Flash Player 
		 * @return  — the newly created Worker if creation succeeds. A return value of  indicates that a worker could not be created either because the current context doesn't support concurrency or because creating a new worker would exceed implementation limits. 
		 */
		public function createWorker(swf:ByteArray, giveAppPrivileges:Boolean = false):Worker {
			if (!isSupported)
				throw new Error("Workers are not supported");
			return Worker.createWorker(swf);
		}

		/**
		 * <p> Provides access to the set of workers in the WorkerDomain that are currently running (the Worker instance's <code>state</code> property is <code>WorkerState.RUNNING</code>). </p>
		 * 
		 * @return  — A Vector of Worker instances containing the workers that are currently running. 
		 */
		public function listWorkers():Vector.<Worker> {
			throw new Error("Not implemented");
		}


		/**
		 * <p> The WorkerDomain instance in which the code is currently running. This is the only WorkerDomain in the application. </p>
		 * 
		 * @return 
		 */
		public static function get current():WorkerDomain {
			return _current;
		}


		/**
		 * <p> Indicates whether the current runtime context supports the WorkerDomain and Worker objects for concurrent code execution. </p>
		 * <p>If concurrency is available, this property's value is <code>true</code>.</p>
		 * 
		 * @return 
		 */
		public static function get isSupported():Boolean {
			return Worker.isSupported;
		}
	}
}
