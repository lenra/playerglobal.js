package flash.system {
	import flash.display.DisplayObjectContainer;

	/**
	 *  The LoaderContext class provides options for loading SWF files and other media by using the Loader class. The LoaderContext class is used as the <code>context</code> parameter in the <code>load()</code> and <code>loadBytes()</code> methods of the Loader class. <p>When loading SWF files with the <code>Loader.load()</code> method, you have two decisions to make: into which security domain the loaded SWF file should be placed, and into which application domain within that security domain? For more details on these choices, see the <code>applicationDomain</code> and <code>securityDomain</code> properties.</p> <p>When loading a SWF file with the <code>Loader.loadBytes()</code> method, you have the same application domain choice to make as for <code>Loader.load()</code>, but it's not necessary to specify a security domain, because <code>Loader.loadBytes()</code> always places its loaded SWF file into the security domain of the loading SWF file.</p> <p>When loading images (JPEG, GIF, or PNG) instead of SWF files, there is no need to specify a SecurityDomain or an application domain, because those concepts are meaningful only for SWF files. Instead, you have only one decision to make: do you need programmatic access to the pixels of the loaded image? If so, see the <code>checkPolicyFile</code> property. If you want to apply deblocking when loading an image, use the JPEGLoaderContext class instead of the LoaderContext class.</p> <p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS2db454920e96a9e51e63e3d11c0bf69084-7f03.html" target="_blank">Specifying a LoaderContext</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS2db454920e96a9e51e63e3d11c0bf619ab-7fe3.html" target="_blank">Creating class instances from loaded applications</a>
	 * </div><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e13.html" target="_blank">Loading display content dynamically</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7de0.html" target="_blank">Specifying loading context</a>
	 *  <br>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../flash/display/Loader.html#load()" target="">flash.display.Loader.load()</a>
	 *  <br>
	 *  <a href="../../flash/display/Loader.html#loadBytes()" target="">flash.display.Loader.loadBytes()</a>
	 *  <br>
	 *  <a href="ApplicationDomain.html" target="">flash.system.ApplicationDomain</a>
	 *  <br>
	 *  <a href="JPEGLoaderContext.html" target="">flash.system.JPEGLoaderContext</a>
	 *  <br>
	 *  <a href="LoaderContext.html#applicationDomain" target="">flash.system.LoaderContext.applicationDomain</a>
	 *  <br>
	 *  <a href="LoaderContext.html#checkPolicyFile" target="">flash.system.LoaderContext.checkPolicyFile</a>
	 *  <br>
	 *  <a href="LoaderContext.html#securityDomain" target="">flash.system.LoaderContext.securityDomain</a>
	 *  <br>
	 *  <a href="SecurityDomain.html" target="">flash.system.SecurityDomain</a>
	 *  <br>
	 *  <a href="ImageDecodingPolicy.html" target="">flash.system.ImageDecodingPolicy</a>
	 * </div><br><hr>
	 */
	public class LoaderContext {
		private var _allowCodeImport:Boolean;
		private var _allowLoadBytesCodeExecution:Boolean;
		private var _applicationDomain:ApplicationDomain;
		private var _checkPolicyFile:Boolean;
		private var _imageDecodingPolicy:String;
		private var _parameters:Object;
		private var _requestedContentParent:DisplayObjectContainer;
		private var _securityDomain:SecurityDomain;

		/**
		 * <p> Creates a new LoaderContext object, with the specified settings. For complete details on these settings, see the descriptions of the properties of this class. </p>
		 * 
		 * @param checkPolicyFile  — Specifies whether a check should be made for the existence of a URL policy file before loading the object. 
		 * @param applicationDomain  — Specifies the ApplicationDomain object to use for a Loader object. 
		 * @param securityDomain  — Specifies the SecurityDomain object to use for a Loader object. <p><i>Note:</i> Content in the air application security sandbox cannot load content from other sandboxes into its SecurityDomain.</p> 
		 */
		public function LoaderContext(checkPolicyFile:Boolean = false, applicationDomain:ApplicationDomain = null, securityDomain:SecurityDomain = null) {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Specifies whether you can use a <code>Loader</code> object to import content with executable code, such as a SWF file, into the caller's security sandbox. There are two affected importing operations: the <code>Loader.loadBytes()</code> method, and the <code>Loader.load()</code> method with <code>LoaderContext.securityDomain = SecurityDomain.currentDomain</code>. (The latter operation is not supported in the AIR application sandbox.) With the <code>allowCodeImport</code> property set to <code>false</code>, these importing operations are restricted to safe operations, such as loading images. Normal, non-importing SWF file loading with the <code>Loader.load()</code> method is not affected by the value of this property. </p>
		 * <p>This property is useful when you want to import image content into your sandbox - for example, when you want to replicate or process an image from a different domain - but you don't want to take the security risk of receiving a SWF file when you expected only an image file. Since SWF files may contain ActionScript code, importing a SWF file is a much riskier operation than importing an image file.</p>
		 * <p>In AIR content in the application sandbox, the default value is <code>false</code>. In non-application content (which includes all content in Flash Player), the default value is <code>true</code>.</p>
		 * <p>The <code>allowCodeImport</code> property was added in Flash Player 10.1 and AIR 2.0. However, this property is made available to SWF files and AIR applications of all versions when the Flash Runtime supports it.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/Loader.html#loadBytes()" target="">flash.display.Loader.loadBytes()</a>
		 *  <br>
		 *  <a href="../../flash/display/Loader.html#load()" target="">flash.display.Loader.load()</a>
		 *  <br>
		 *  <a href="LoaderContext.html#securityDomain" target="">securityDomain</a>
		 * </div>
		 * 
		 * @return 
		 */
		public function get allowCodeImport():Boolean {
			return _allowCodeImport;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set allowCodeImport(value:Boolean):void {
			_allowCodeImport = value;
		}

		/**
		 * <p> Legacy property, replaced by <code>allowCodeImport</code>, but still supported for compatibility. Previously, the only operation affected by <code>allowLoadBytesCodeExecution</code> was the <code>Loader.loadBytes()</code> method, but as of Flash Player 10.1 and AIR 2.0, the import-loading operation of <code>Loader.load()</code> with <code>LoaderContext.securityDomain = SecurityDomain.currentDomain</code> is affected as well. (The latter operation is not supported in the AIR application sandbox.) This dual effect made the property name <code>allowLoadBytesCodeExecution</code> overly specific, so now <code>allowCodeImport</code> is the preferred property name. Setting either of <code>allowCodeImport</code> or <code>allowLoadBytesCodeExecution</code> will affect the value of both. </p>
		 * <p>Specifies whether you can use a <code>Loader</code> object to import content with executable code, such as a SWF file, into the caller's security sandbox. With this property set to <code>false</code>, these importing operations are restricted to safe operations, such as loading images.</p>
		 * <p>In AIR content in the application sandbox, the default value is <code>false</code>. In non-application content, the default value is <code>true</code>.</p>
		 * 
		 * @return 
		 */
		public function get allowLoadBytesCodeExecution():Boolean {
			return _allowLoadBytesCodeExecution;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set allowLoadBytesCodeExecution(value:Boolean):void {
			_allowLoadBytesCodeExecution = value;
		}

		/**
		 * <p> Specifies the application domain to use for the <code>Loader.load()</code> or <code>Loader.loadBytes()</code> method. Use this property only when loading a SWF file written in ActionScript 3.0 (not an image or a SWF file written in ActionScript 1.0 or ActionScript 2.0). </p>
		 * <p>Every security domain is divided into one or more application domains, represented by ApplicationDomain objects. Application domains are not for security purposes; they are for managing cooperating units of ActionScript code. If you are loading a SWF file from another domain, and allowing it to be placed in a separate security domain, then you cannot control the choice of application domain into which the loaded SWF file is placed; and if you have specified a choice of application domain, it will be ignored. However, if you are loading a SWF file into your own security domain — either because the SWF file comes from your own domain, or because you are importing it into your security domain — then you can control the choice of application domain for the loaded SWF file.</p>
		 * <p>You can pass an application domain only from your own security domain in <code>LoaderContext.applicationDomain</code>. Attempting to pass an application domain from any other security domain results in a <code>SecurityError</code> exception.</p>
		 * <p>You have four choices for what kind of <code>ApplicationDomain</code> property to use:</p>
		 * <ul>
		 *  <li><b>Child of loader's ApplicationDomain.</b> The default. You can explicitly represent this choice with the syntax <code>new ApplicationDomain(ApplicationDomain.currentDomain)</code>. This allows the loaded SWF file to use the parent's classes directly, for example by writing <code>new MyClassDefinedInParent()</code>. The parent, however, cannot use this syntax; if the parent wishes to use the child's classes, it must call <code>ApplicationDomain.getDefinition()</code> to retrieve them. The advantage of this choice is that, if the child defines a class with the same name as a class already defined by the parent, no error results; the child simply inherits the parent's definition of that class, and the child's conflicting definition goes unused unless either child or parent calls the <code>ApplicationDomain.getDefinition()</code> method to retrieve it.</li>
		 *  <li><b>Loader's own ApplicationDomain.</b> You use this application domain when using <code>ApplicationDomain.currentDomain</code>. When the load is complete, parent and child can use each other's classes directly. If the child attempts to define a class with the same name as a class already defined by the parent, the parent class is used and the child class is ignored.</li>
		 *  <li><b>Child of the system ApplicationDomain.</b> You use this application domain when using <code>new ApplicationDomain(null)</code>. This separates loader and loadee entirely, allowing them to define separate versions of classes with the same name without conflict or overshadowing. The only way either side sees the other's classes is by calling the <code>ApplicationDomain.getDefinition()</code> method.</li>
		 *  <li><b>Child of some other ApplicationDomain.</b> Occasionally you may have a more complex ApplicationDomain hierarchy. You can load a SWF file into any ApplicationDomain from your own SecurityDomain. For example, <code>new ApplicationDomain(ApplicationDomain.currentDomain.parentDomain.parentDomain)</code> loads a SWF file into a new child of the current domain's parent's parent.</li>
		 * </ul>
		 * <p>When a load is complete, either side (loading or loaded) may need to find its own ApplicationDomain, or the other side's ApplicationDomain, for the purpose of calling <code>ApplicationDomain.getDefinition()</code>. Either side can retrieve a reference to its own application domain by using <code>ApplicationDomain.currentDomain</code>. The loading SWF file can retrieve a reference to the loaded SWF file's ApplicationDomain via <code>Loader.contentLoaderInfo.applicationDomain</code>. If the loaded SWF file knows how it was loaded, it can find its way to the loading SWF file's ApplicationDomain object. For example, if the child was loaded in the default way, it can find the loading SWF file's application domain by using <code>ApplicationDomain.currentDomain.parentDomain</code>.</p>
		 * <p>For more information, see the "ApplicationDomain class" section of the "Client System Environment" chapter of the <i>ActionScript 3.0 Developer's Guide</i>.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/Loader.html#load()" target="">flash.display.Loader.load()</a>
		 *  <br>
		 *  <a href="../../flash/display/Loader.html#loadBytes()" target="">flash.display.Loader.loadBytes()</a>
		 *  <br>
		 *  <a href="ApplicationDomain.html" target="">flash.system.ApplicationDomain</a>
		 * </div>
		 * 
		 * @return 
		 */
		public function get applicationDomain():ApplicationDomain {
			return _applicationDomain;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set applicationDomain(value:ApplicationDomain):void {
			_applicationDomain = value;
		}

		/**
		 * <p> Specifies whether the application should attempt to download a URL policy file from the loaded object's server before beginning to load the object itself. This flag is applicable to the <code>Loader.load()</code> method, but not to the <code>Loader.loadBytes()</code> method. </p>
		 * <p>Set this flag to <code>true</code> when you are loading an image (JPEG, GIF, or PNG) from outside the calling SWF file's own domain, and you expect to need access to the content of that image from ActionScript. Examples of accessing image content include referencing the <code>Loader.content</code> property to obtain a Bitmap object, and calling the <code>BitmapData.draw()</code> method to obtain a copy of the loaded image's pixels. If you attempt one of these operations without having specified <code>checkPolicyFile</code> at loading time, you may get a <code>SecurityError</code> exception because the needed policy file has not been downloaded yet.</p>
		 * <p>When you call the <code>Loader.load()</code> method with <code>LoaderContext.checkPolicyFile</code> set to <code>true</code>, the application does not begin downloading the specified object in <code>URLRequest.url</code> until it has either successfully downloaded a relevant URL policy file or discovered that no such policy file exists. Flash Player or AIR first considers policy files that have already been downloaded, then attempts to download any pending policy files specified in calls to the <code>Security.loadPolicyFile()</code> method, then attempts to download a policy file from the default location that corresponds to <code>URLRequest.url</code>, which is <code>/crossdomain.xml</code> on the same server as <code>URLRequest.url</code>. In all cases, the given policy file is required to exist at <code>URLRequest.url</code> by virtue of the policy file's location, and the file must permit access by virtue of one or more <code>&lt;allow-access-from&gt;</code> tags.</p>
		 * <p>If you set <code>checkPolicyFile</code> to <code>true</code>, the main download that specified in the <code>Loader.load()</code> method does not load until the policy file has been completely processed. Therefore, as long as the policy file that you need exists, as soon as you have received any <code>ProgressEvent.PROGRESS</code> or <code>Event.COMPLETE</code> events from the <code>contentLoaderInfo</code> property of your Loader object, the policy file download is complete, and you can safely begin performing operations that require the policy file.</p>
		 * <p>If you set <code>checkPolicyFile</code> to <code>true</code>, and no relevant policy file is found, you will not receive any error indication until you attempt an operation that throws a <code>SecurityError</code> exception. However, once the LoaderInfo object dispatches a <code>ProgressEvent.PROGRESS</code> or <code>Event.COMPLETE</code> event, you can test whether a relevant policy file was found by checking the value of the <code>LoaderInfo.childAllowsParent</code> property.</p>
		 * <p>If you will not need pixel-level access to the image that you are loading, you should not set the <code>checkPolicyFile</code> property to <code>true</code>. Checking for a policy file in this case is wasteful, because it may delay the start of your download, and it may consume network bandwidth unnecessarily.</p>
		 * <p>Also try to avoid setting <code>checkPolicyFile</code> to <code>true</code> if you are using the <code>Loader.load()</code> method to download a SWF file. This is because SWF-to-SWF permissions are not controlled by policy files, but rather by the <code>Security.allowDomain()</code> method, and thus <code>checkPolicyFile</code> has no effect when you load a SWF file. Checking for a policy file in this case is wasteful, because it may delay the download of the SWF file, and it may consume network bandwidth unnecessarily. (Flash Player or AIR cannot tell whether your main download will be a SWF file or an image, because the policy file download occurs before the main download.)</p>
		 * <p>Be careful with <code>checkPolicyFile</code> if you are downloading an object from a URL that may use server-side HTTP redirects. Policy files are always retrieved from the corresponding initial URL that you specify in <code>URLRequest.url</code>. If the final object comes from a different URL because of HTTP redirects, then the initially downloaded policy files might not be applicable to the object's final URL, which is the URL that matters in security decisions. If you find yourself in this situation, you can examine the value of <code>LoaderInfo.url</code> after you have received a <code>ProgressEvent.PROGRESS</code> or <code>Event.COMPLETE</code> event, which tells you the object's final URL. Then call the <code>Security.loadPolicyFile()</code> method with a policy file URL based on the object's final URL. Then poll the value of <code>LoaderInfo.childAllowsParent</code> until it becomes <code>true</code>.</p>
		 * <p>You do not need to set this property for AIR content running in the application sandbox. Content in the AIR application sandbox can call the <code>BitmapData.draw()</code> method using any loaded image content as the source. </p>
		 * <p id="moreExamples"><span class="label">More examples</span></p>
		 * <div class="seeAlso">
		 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7de0.html" target="_blank">Specifying loading context</a>
		 * </div>
		 * <p id="learnMore"><span class="label">Learn more</span></p>
		 * <div class="seeAlso">
		 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e13.html" target="_blank">Loading display content dynamically</a>
		 *  <br>
		 * </div>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/BitmapData.html#draw()" target="">flash.display.BitmapData.draw()</a>
		 *  <br>
		 *  <a href="../../flash/display/Loader.html#content" target="">flash.display.Loader.content</a>
		 *  <br>
		 *  <a href="../../flash/display/Loader.html#load()" target="">flash.display.Loader.load()</a>
		 *  <br>
		 *  <a href="../../flash/display/LoaderInfo.html#childAllowsParent" target="">flash.display.LoaderInfo.childAllowsParent</a>
		 *  <br>
		 *  <a href="../../flash/display/LoaderInfo.html#url" target="">flash.display.LoaderInfo.url</a>
		 *  <br>
		 *  <a href="Security.html#allowDomain()" target="">flash.system.Security.allowDomain()</a>
		 *  <br>
		 *  <a href="Security.html#loadPolicyFile()" target="">flash.system.Security.loadPolicyFile()</a>
		 * </div>
		 * 
		 * @return 
		 */
		public function get checkPolicyFile():Boolean {
			return _checkPolicyFile;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set checkPolicyFile(value:Boolean):void {
			_checkPolicyFile = value;
		}

		/**
		 * <p> Specifies whether to decode bitmap image data when it is used or when it is loaded. </p>
		 * <p>Under the default policy, <code>ImageDecodingPolicy.ON_DEMAND</code>, the runtime decodes the image data when the data is needed (for display or some other purpose). This policy maintains the decoding behavior used by previous versions of the runtime.</p>
		 * <p>Under the <code>ImageDecodingPolicy.ON_LOAD</code> policy, the runtime decodes the image immediately after it is loaded and before dispatching the <code>complete</code> event. Decoding images on load rather than on demand can improve animation and user interface performance. You can see improvements when several loaded images are displayed in quick succession. Some examples of a rapid display of images are scrolling lists, or cover flow control. On the other hand, using the <code>onLoad</code> policy indiscriminately can increase the peak memory usage of your application. More decoded image data could be in memory at one time than would be the case under the <code>onDemand</code> policy.</p>
		 * <p>Under both policies, the runtime uses the same cache and flush behavior after the image is decoded. The runtime can flush the decoded data at any time and decode the image again the next time it is required.</p>
		 * <p>To set the image decoding policy (for example, to <code>ON_LOAD</code>): </p>
		 * <pre>
		 *      var loaderContext:LoaderContext = new LoaderContext(); 
		 *      loaderContext.imageDecodingPolicy = ImageDecodingPolicy.ON_LOAD 
		 *      var loader:Loader = new Loader(); 
		 *      loader.load(new URLRequest("http://www.adobe.com/myimage.png"), loaderContext);
		 *      </pre>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="ImageDecodingPolicy.html" target="">flash.system.ImageDecodingPolicy</a>
		 * </div>
		 * 
		 * @return 
		 */
		public function get imageDecodingPolicy():String {
			return _imageDecodingPolicy;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set imageDecodingPolicy(value:String):void {
			_imageDecodingPolicy = value;
		}

		/**
		 * <p> An Object containing the parameters to pass to the LoaderInfo object of the content. </p>
		 * <p>Normally, the value of the <code>contentLoaderInfo.parameters</code> property is obtained by parsing the requesting URL. If the <code>parameters</code> var is set, the <code>contentLoaderInfo.parameters</code> gets its value from the LoaderContext object, instead of from the requesting URL. The <code>parameters</code> var accepts only objects containing name-value string pairs, similar to URL parameters. If the object does not contain name-value string pairs, an <code>IllegalOperationError</code> is thrown.</p>
		 * <p>The intent of this API is to enable the loading SWF file to forward its parameters to a loaded SWF file. This functionality is especially helpful when you use the <code>loadBytes()</code> method, since <code>LoadBytes</code> does not provide a means of passing parameters through the URL. Parameters can be forwarded successfully only to another AS3 SWF file; an AS1 or AS2 SWF file cannot receive the parameters in an accessible form, although the AVM1Movie's AS3 loaderInfo.parameters object will be the forwarded object. </p>
		 * <p>For example, consider the following URL: </p>
		 * <p><code>http://yourdomain/users/jdoe/test01/child.swf?foo=bar;</code></p>
		 * <p>The following code uses the LoaderContext.parameters property to replicate a parameter passed to this URL: </p>
		 * <pre>
		 *       import flash.system.LoaderContext; 
		 *       import flash.display.Loader; 
		 *       var l:Loader = new Loader(); 
		 *       var lc:LoaderContext = new LoaderContext; 
		 *       lc.parameters = { "foo": "bar" }; 
		 *       l.load(new URLRequest("child.swf"), lc);
		 *      </pre>
		 * <p>To verify that the parameter passed properly, use the following trace statement after you run this code:</p>
		 * <p><code>trace(loaderInfo.parameters.foo);</code></p>
		 * <p>If the content loaded successfully, this trace prints "bar".</p>
		 * 
		 * @return 
		 */
		public function get parameters():Object {
			return _parameters;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set parameters(value:Object):void {
			_parameters = value;
		}

		/**
		 * <p> The parent to which the Loader will attempt to add the loaded content. </p>
		 * <p>When content is completely loaded, the Loader object normally becomes the parent of the content. If <code>requestedContentParent</code> is set, the object that it specifies becomes the parent, unless a runtime error prevents the assignment. This reparenting can also be done after the <code>complete</code> event without use of this property. However, specifying the parent with <code>LoaderContext.requestedContentParent</code> eliminates extra events. </p>
		 * <p><code>LoaderContext.requestedContentParent</code> sets the desired parent before frame one scripts in the loaded content execute, but after the constructor has run. If <code>requestedContentParent</code> is null (the default), the Loader object becomes the content's parent.</p>
		 * <p>If the loaded content is an AVM1Movie object, or if an error is thrown when <code>addChild()</code> is called on the <code>requestedContentParent</code> object, then the following actions occur: </p>
		 * <ul>
		 *  <li>The Loader object becomes the parent of the loaded content.</li>
		 *  <li>The runtime dispatches an <code>AsyncErrorEvent</code>.</li>
		 * </ul>
		 * <p>If the requested parent and the loaded content are in different security sandboxes, and if the requested parent does not have access to the loaded content, then the following actions occur: </p>
		 * <ul>
		 *  <li>The Loader becomes the parent of the loaded content.</li>
		 *  <li>The runtime dispatches a <code>SecurityErrorEvent</code>.</li>
		 * </ul>
		 * <p>The following code uses <code>requestedContentParent</code> to place the loaded content into a Sprite object:</p>
		 * <pre>
		 *       import flash.system.LoaderContext; 
		 *       import flash.display.Loader; 
		 *       import flash.display.Sprite; 
		 *      
		 *       var lc:LoaderContext = new LoaderContext(); 
		 *       var l:Loader = new Loader(); 
		 *       var s:Sprite = new Sprite(); 
		 *       lc.requestedContentParent = s; 
		 *       addChild(s); 
		 *       l.load(new URLRequest("child.swf"), lc);
		 *      </pre>
		 * <p>When this code runs, the child SWF file appears on stage. This fact confirms that the Sprite object you added to the stage is the parent of the loaded child.swf file.</p>
		 * 
		 * @return 
		 */
		public function get requestedContentParent():DisplayObjectContainer {
			return _requestedContentParent;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set requestedContentParent(value:DisplayObjectContainer):void {
			_requestedContentParent = value;
		}

		/**
		 * <p> Specifies the security domain to use for a <code>Loader.load()</code> operation. Use this property only when loading a SWF file (not an image). </p>
		 * <p>The choice of security domain is meaningful only if you are loading a SWF file that might come from a different domain (a different server) than the loading SWF file. When you load a SWF file from your own domain, it is always placed into your security domain. But when you load a SWF file from a different domain, you have two options. You can allow the loaded SWF file to be placed in its "natural" security domain, which is different from that of the loading SWF file; this is the default. The other option is to specify that you want to place the loaded SWF file placed into the same security domain as the loading SWF file, by setting <code>myLoaderContext.securityDomain</code> to be equal to <code>SecurityDomain.currentDomain</code>. This is called <i>import loading</i>, and it is equivalent, for security purposes, to copying the loaded SWF file to your own server and loading it from there. In order for import loading to succeed, the loaded SWF file's server must have a policy file trusting the domain of the loading SWF file.</p>
		 * <p>You can pass your own security domain only in <code>LoaderContext.securityDomain</code>. Attempting to pass any other security domain results in a <code>SecurityError</code> exception.</p>
		 * <p>Content in the AIR application security sandbox cannot load content from other sandboxes into its SecurityDomain.</p>
		 * <p>For more information, see the "Security" chapter in the <i>ActionScript 3.0 Developer's Guide</i>.</p>
		 * <p id="moreExamples"><span class="label">More examples</span></p>
		 * <div class="seeAlso">
		 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7de0.html" target="_blank">Specifying loading context</a>
		 * </div>
		 * <p id="learnMore"><span class="label">Learn more</span></p>
		 * <div class="seeAlso">
		 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e13.html" target="_blank">Loading display content dynamically</a>
		 *  <br>
		 * </div>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/Loader.html#load()" target="">flash.display.Loader.load()</a>
		 *  <br>
		 *  <a href="SecurityDomain.html" target="">flash.system.SecurityDomain</a>
		 * </div>
		 * 
		 * @return 
		 */
		public function get securityDomain():SecurityDomain {
			return _securityDomain;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set securityDomain(value:SecurityDomain):void {
			_securityDomain = value;
		}
	}
}
