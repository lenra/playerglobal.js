package flash.system {
	/**
	 *  <span>The Capabilities class provides properties that describe the system and runtime that are hosting the application. For example, a mobile phone's screen might be 100 square pixels, black and white, whereas a PC screen might be 1000 square pixels, color. By using the Capabilities class to determine what capabilities the client has, you can provide appropriate content to as many users as possible. When you know the device's capabilities, you can tell the server to send the appropriate SWF files or tell the SWF file to alter its presentation.</span> <p>However, some capabilities of Adobe AIR are not listed as properties in the Capabilities class. They are properties of other classes:</p> <table class="innertable">
	 *  <tbody>
	 *   <tr>
	 *    <th>Property</th>
	 *    <th>Description</th>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>NativeApplication.supportsDockIcon</code> </td>
	 *    <td>Whether the operating system supports application doc icons.</td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>NativeApplication.supportsMenu</code> </td>
	 *    <td>Whether the operating system supports a global application menu bar.</td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>NativeApplication.supportsSystemTrayIcon</code> </td>
	 *    <td>Whether the operating system supports system tray icons.</td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>NativeWindow.supportsMenu</code> </td>
	 *    <td>Whether the operating system supports window menus.</td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>NativeWindow.supportsTransparency</code> </td>
	 *    <td>Whether the operating system supports transparent windows.</td>
	 *   </tr>
	 *  </tbody>
	 * </table> <p>Do <i>not</i> use <code>Capabilities.os</code> or <code>Capabilities.manufacturer</code> to determine a capability based on the operating system. Basing a capability on the operating system is a bad idea, since it can lead to problems if an application does not consider all potential target operating systems. Instead, use the property corresponding to the capability for which you are testing.</p> <p>You can send capabilities information, which is stored in the <code>Capabilities.serverString</code> property as a URL-encoded string, using the <code>GET</code> or <code>POST</code> HTTP method. The following example shows a server string for a computer that has MP3 support and 1600 x 1200 pixel resolution, that is running Windows XP with an input method editor (IME) installed, and does not have support for multichannel audio:</p> <pre>A=t&amp;SA=t&amp;SV=t&amp;EV=t&amp;MP3=t&amp;AE=t&amp;VE=t&amp;ACC=f&amp;PR=t&amp;SP=t&amp;
	 *      SB=f&amp;DEB=t&amp;V=WIN%209%2C0%2C0%2C0&amp;M=Adobe%20Windows&amp;
	 *      R=1600x1200&amp;DP=72&amp;COL=color&amp;AR=1.0&amp;OS=Windows%20XP&amp;
	 *      L=en&amp;PT=External&amp;AVD=f&amp;LFD=f&amp;WD=f&amp;IME=t&amp;DD=f&amp;
	 *      DDP=f&amp;DTS=f&amp;DTE=f&amp;DTH=f&amp;DTM=f</pre> <p>The following table lists the properties of the Capabilities class and corresponding server strings. It also lists the server strings for the multichannel audio types. </p><table class="innertable">
	 *  <tbody>
	 *   <tr>
	 *    <th align="left">Capabilities class property</th>
	 *    <th align="left">Server string</th>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>avHardwareDisable</code> </td>
	 *    <td> <code>AVD</code> </td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>hasAccessibility</code> </td>
	 *    <td> <code>ACC</code> </td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>hasAudio</code> </td>
	 *    <td> <code>A</code> </td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>hasAudioEncoder</code> </td>
	 *    <td> <code>AE</code> </td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>hasEmbeddedVideo</code> </td>
	 *    <td> <code>EV</code> </td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>hasIME</code> </td>
	 *    <td> <code>IME</code> </td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>hasMP3</code> </td>
	 *    <td> <code>MP3</code> </td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>hasPrinting</code> </td>
	 *    <td> <code>PR</code> </td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>hasScreenBroadcast</code> </td>
	 *    <td> <code>SB</code> </td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>hasScreenPlayback</code> </td>
	 *    <td> <code>SP</code> </td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>hasStreamingAudio</code> </td>
	 *    <td> <code>SA</code> </td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>hasStreamingVideo</code> </td>
	 *    <td> <code>SV</code> </td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>hasTLS</code> </td>
	 *    <td> <code>TLS</code> </td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>hasVideoEncoder</code> </td>
	 *    <td> <code>VE</code> </td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>isDebugger</code> </td>
	 *    <td> <code>DEB</code> </td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>language</code> </td>
	 *    <td> <code>L</code> </td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>localFileReadDisable</code> </td>
	 *    <td> <code>LFD</code> </td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>manufacturer</code> </td>
	 *    <td> <code>M</code> </td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>maxLevelIDC</code> </td>
	 *    <td> <code>ML</code> </td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>os</code> </td>
	 *    <td> <code>OS</code> </td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>pixelAspectRatio</code> </td>
	 *    <td> <code>AR</code> </td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>playerType</code> </td>
	 *    <td> <code>PT</code> </td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>screenColor</code> </td>
	 *    <td> <code>COL</code> </td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>screenDPI</code> </td>
	 *    <td> <code>DP</code> </td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>screenResolutionX</code> </td>
	 *    <td> <code>R</code> </td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>screenResolutionY</code> </td>
	 *    <td> <code>R</code> </td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>version</code> </td>
	 *    <td> <code>V</code> </td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>supports Dolby Digital audio</code> </td>
	 *    <td> <code>DD</code> </td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>supports Dolby Digital Plus audio</code> </td>
	 *    <td> <code>DDP</code> </td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>supports DTS audio</code> </td>
	 *    <td> <code>DTS</code> </td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>supports DTS Express audio</code> </td>
	 *    <td> <code>DTE</code> </td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>supports DTS-HD High Resolution Audio</code> </td>
	 *    <td> <code>DTH</code> </td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>supports DTS-HD Master Audio</code> </td>
	 *    <td> <code>DTM</code> </td>
	 *   </tr>
	 *  </tbody>
	 * </table>  <p>There is also a <code>WD</code> server string that specifies whether windowless mode is disabled. Windowless mode can be disabled in Flash Player due to incompatibility with the web browser or to a user setting in the mms.cfg file. There is no corresponding Capabilities property.</p> <p>All properties of the Capabilities class are read-only.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS2db454920e96a9e51e63e3d11c0bf69084-7ebb.html" target="_blank">Determining Flash Player version in Flex</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7cd8.html" target="_blank">Using the Capabilities class</a>
	 * </div><br><hr>
	 */
	public class Capabilities {
		private static var _avHardwareDisable:Boolean;
		private static var _cpuArchitecture:String;
		private static var _hasAccessibility:Boolean;
		private static var _hasAudio:Boolean;
		private static var _hasAudioEncoder:Boolean;
		private static var _hasEmbeddedVideo:Boolean;
		private static var _hasIME:Boolean;
		private static var _hasMP3:Boolean;
		private static var _hasPrinting:Boolean;
		private static var _hasScreenBroadcast:Boolean;
		private static var _hasScreenPlayback:Boolean;
		private static var _hasStreamingAudio:Boolean;
		private static var _hasStreamingVideo:Boolean;
		private static var _hasTLS:Boolean;
		private static var _hasVideoEncoder:Boolean;
		private static var _isDebugger:Boolean;
		private static var _isEmbeddedInAcrobat:Boolean;
		private static var _language:String;
		private static var _languages:Array;
		private static var _localFileReadDisable:Boolean;
		private static var _manufacturer:String;
		private static var _maxLevelIDC:String;
		private static var _os:String = "";
		private static var _pixelAspectRatio:Number;
		private static var _playerType:String;
		private static var _screenColor:String;
		private static var _screenDPI:Number;
		private static var _screenResolutionX:Number;
		private static var _screenResolutionY:Number;
		private static var _serverString:String;
		private static var _supports32BitProcesses:Boolean;
		private static var _supports64BitProcesses:Boolean;
		private static var _touchscreenType:String;
		private static var _version:String;


		/**
		 * <p> Specifies whether access to the user's camera and microphone has been administratively prohibited (<code>true</code>) or allowed (<code>false</code>). The server string is <code>AVD</code>. </p>
		 * <p>For content in Adobe AIR™, this property applies only to content in security sandboxes other than the application security sandbox. Content in the application security sandbox can always access the user's camera and microphone.</p>
		 * 
		 * @return 
		 */
		public static function get avHardwareDisable():Boolean {
			return _avHardwareDisable;
		}


		/**
		 * <p> Specifies the current CPU architecture. The <code>cpuArchitecture</code> property can return the following strings: "<code>PowerPC</code>", "<code>x86</code>", "<code>SPARC</code>", and "<code>ARM</code>". The server string is <code>ARCH</code>. </p>
		 * 
		 * @return 
		 */
		public static function get cpuArchitecture():String {
			return _cpuArchitecture;
		}


		/**
		 * <p> Specifies whether the system supports (<code>true</code>) or does not support (<code>false</code>) communication with accessibility aids. The server string is <code>ACC</code>. </p>
		 * 
		 * @return 
		 */
		public static function get hasAccessibility():Boolean {
			return _hasAccessibility;
		}


		/**
		 * <p> Specifies whether the system has audio capabilities. This property is always <code>true</code>. The server string is <code>A</code>. </p>
		 * 
		 * @return 
		 */
		public static function get hasAudio():Boolean {
			return _hasAudio;
		}


		/**
		 * <p> Specifies whether the system can (<code>true</code>) or cannot (<code>false</code>) encode an audio stream, such as that coming from a microphone. The server string is <code>AE</code>. </p>
		 * 
		 * @return 
		 */
		public static function get hasAudioEncoder():Boolean {
			return _hasAudioEncoder;
		}


		/**
		 * <p> Specifies whether the system supports (<code>true</code>) or does not support (<code>false</code>) embedded video. The server string is <code>EV</code>. </p>
		 * 
		 * @return 
		 */
		public static function get hasEmbeddedVideo():Boolean {
			return _hasEmbeddedVideo;
		}


		/**
		 * <p> Specifies whether the system does (<code>true</code>) or does not (<code>false</code>) have an input method editor (IME) installed. The server string is <code>IME</code>. </p>
		 * 
		 * @return 
		 */
		public static function get hasIME():Boolean {
			return _hasIME;
		}


		/**
		 * <p> Specifies whether the system does (<code>true</code>) or does not (<code>false</code>) have an MP3 decoder. The server string is <code>MP3</code>. </p>
		 * 
		 * @return 
		 */
		public static function get hasMP3():Boolean {
			return _hasMP3;
		}


		/**
		 * <p> Specifies whether the system does (<code>true</code>) or does not (<code>false</code>) support printing. The server string is <code>PR</code>. </p>
		 * 
		 * @return 
		 */
		public static function get hasPrinting():Boolean {
			return _hasPrinting;
		}


		/**
		 * <p> Specifies whether the system does (<code>true</code>) or does not (<code>false</code>) support the development of screen broadcast applications to be run through Flash Media Server. The server string is <code>SB</code>. </p>
		 * 
		 * @return 
		 */
		public static function get hasScreenBroadcast():Boolean {
			return _hasScreenBroadcast;
		}


		/**
		 * <p> Specifies whether the system does (<code>true</code>) or does not (<code>false</code>) support the playback of screen broadcast applications that are being run through Flash Media Server. The server string is <code>SP</code>. </p>
		 * 
		 * @return 
		 */
		public static function get hasScreenPlayback():Boolean {
			return _hasScreenPlayback;
		}


		/**
		 * <p> Specifies whether the system can (<code>true</code>) or cannot (<code>false</code>) play streaming audio. The server string is <code>SA</code>. </p>
		 * 
		 * @return 
		 */
		public static function get hasStreamingAudio():Boolean {
			return _hasStreamingAudio;
		}


		/**
		 * <p> Specifies whether the system can (<code>true</code>) or cannot (<code>false</code>) play streaming video. The server string is <code>SV</code>. </p>
		 * 
		 * @return 
		 */
		public static function get hasStreamingVideo():Boolean {
			return _hasStreamingVideo;
		}


		/**
		 * <p> Specifies whether the system supports native SSL sockets through NetConnection (<code>true</code>) or does not (<code>false</code>). The server string is <code>TLS</code>. </p>
		 * 
		 * @return 
		 */
		public static function get hasTLS():Boolean {
			return _hasTLS;
		}


		/**
		 * <p> Specifies whether the system can (<code>true</code>) or cannot (<code>false</code>) encode a video stream, such as that coming from a web camera. The server string is <code>VE</code>. </p>
		 * 
		 * @return 
		 */
		public static function get hasVideoEncoder():Boolean {
			return _hasVideoEncoder;
		}


		/**
		 * <p> Specifies whether the system is a special debugging version (<code>true</code>) or an officially released version (<code>false</code>). The server string is <code>DEB</code>. This property is set to <code>true</code> when running in <span>the debug version of Flash Player or</span> the AIR Debug Launcher (ADL). </p>
		 * 
		 * @return 
		 */
		public static function get isDebugger():Boolean {
			return _isDebugger;
		}


		/**
		 * <p> Specifies whether the Flash runtime is embedded in a PDF file that is open in Acrobat 9.0 or higher (<code>true</code>) or not (<code>false</code>). </p>
		 * 
		 * @return 
		 */
		public static function get isEmbeddedInAcrobat():Boolean {
			return _isEmbeddedInAcrobat;
		}


		/**
		 * <p> Specifies the language code of the system on which the content is running. The language is specified as a lowercase two-letter language code from ISO 639-1. For Chinese, an additional uppercase two-letter country code from ISO 3166 distinguishes between Simplified and Traditional Chinese. The languages codes are based on the English names of the language: for example, <code>hu</code> specifies Hungarian. </p>
		 * <p>On English systems, this property returns only the language code (<code>en</code>), not the country code. On Microsoft Windows systems, this property returns the user interface (UI) language, which refers to the language used for all menus, dialog boxes, error messages, and help files. The following table lists the possible values: </p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th align="left">Language</th>
		 *    <th align="left">Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td>Czech</td>
		 *    <td><code>cs</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Danish</td>
		 *    <td><code>da</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Dutch</td>
		 *    <td><code>nl</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>English</td>
		 *    <td><code>en</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Finnish</td>
		 *    <td><code>fi</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>French</td>
		 *    <td><code>fr</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>German</td>
		 *    <td><code>de</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Hungarian</td>
		 *    <td><code>hu</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Italian</td>
		 *    <td><code>it</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Japanese</td>
		 *    <td><code>ja</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Korean</td>
		 *    <td><code>ko</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Norwegian</td>
		 *    <td><code>nb</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Other/unknown</td>
		 *    <td><code>xu</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Polish</td>
		 *    <td><code>pl</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Portuguese</td>
		 *    <td><code>pt</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Russian</td>
		 *    <td><code>ru</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Simplified Chinese</td>
		 *    <td><code>zh-CN</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Spanish</td>
		 *    <td><code>es</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Swedish</td>
		 *    <td><code>sv</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Traditional Chinese</td>
		 *    <td><code>zh-TW</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Turkish</td>
		 *    <td><code>tr</code></td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><i>Note:</i> The value of <code>Capabilities.language</code> property is limited to the possible values on this list. Because of this limitation, Adobe AIR applications should use the first element in the <code>Capabilities.languages</code> array to determine the primary user interface language for the system. </p>
		 * <p>The server string is <code>L</code>.</p>
		 * 
		 * @return 
		 */
		public static function get language():String {
			return _language;
		}


		/**
		 * <p> An array of strings that contain information about the user's preferred user interface languages, as set through the operating system. The strings will contain language tags (and script and region information, where applicable) defined by RFC4646 (<a href="http://www.ietf.org/rfc/rfc4646.txt" target="external">http://www.ietf.org/rfc/rfc4646.txt</a>) and will use dashes as a delimiter (for example, <code>"en-US"</code> or <code>"ja-JP"</code>). Languages are listed in the array in the order of preference, as determined by the operating system settings. </p>
		 * <p>Operating systems differ in region information returned in locale strings. One operating system may return <code>"en-us"</code>, whereas another may return <code>"en"</code>.</p>
		 * <p>The first entry in the returned array generally has the same primary language ID as the <code>Capabilities.language</code> property. For example, if <code>Capabilities.languages[0]</code> is set to <code>"en-US"</code>, then the <code>language</code> property is set to <code>"en"</code>. However, if the <code>Capabilities.language</code> property is set to <code>"xu"</code> (specifying an unknown language), the first element in this array will be different. For this reason, <code>Capabilities.languages[0]</code> can be more accurate than <code>Capabilities.language</code>.</p>
		 * <p>The server string is <code>LS</code>.</p>
		 * 
		 * @return 
		 */
		public static function get languages():Array {
			return _languages;
		}


		/**
		 * <p> Specifies whether read access to the user's hard disk has been administratively prohibited (<code>true</code>) or allowed (<code>false</code>). For content in Adobe AIR, this property applies only to content in security sandboxes other than the application security sandbox. (Content in the application security sandbox can always read from the file system.) <span>If this property is <code>true</code>, Flash Player cannot read files (including the first file that Flash Player launches with) from the user's hard disk.</span> If this property is <code>true</code>, AIR content outside of the application security sandbox cannot read files from the user's hard disk. For example, attempts to read a file on the user's hard disk using load methods will fail if this property is set to <code>true</code>. </p>
		 * <p>Reading runtime shared libraries is also blocked if this property is set to <code>true</code>, but reading local shared objects is allowed without regard to the value of this property.</p>
		 * <p>The server string is <code>LFD</code>.</p>
		 * 
		 * @return 
		 */
		public static function get localFileReadDisable():Boolean {
			return _localFileReadDisable;
		}


		/**
		 * <p> Specifies the manufacturer of <span>the running version of Flash Player or </span> the AIR runtime, in the format <code>"Adobe</code> <code><i>OSName</i>"</code>. The value for <code><i>OSName</i></code> could be <code>"Windows"</code>, <code>"Macintosh"</code>, <code>"Linux"</code>, or another operating system name. The server string is <code>M</code>. </p>
		 * <p>Do <i>not</i> use <code>Capabilities.manufacturer</code> to determine a capability based on the operating system if a more specific capability property exists. Basing a capability on the operating system is a bad idea, since it can lead to problems if an application does not consider all potential target operating systems. Instead, use the property corresponding to the capability for which you are testing. For more information, see the Capabilities class description.</p>
		 * 
		 * @return 
		 */
		public static function get manufacturer():String {
			return _manufacturer;
		}


		/**
		 * <p> Retrieves the highest H.264 Level IDC that the client hardware supports. Media run at this level are guaranteed to run; however, media run at the highest level might not run with the highest quality. This property is useful for servers trying to target a client's capabilities. Using this property, a server can determine the level of video to send to the client. </p>
		 * <p>The server string is <code>ML</code>.</p>
		 * 
		 * @return 
		 */
		public static function get maxLevelIDC():String {
			return _maxLevelIDC;
		}


		/**
		 * <p> Specifies the current operating system. The <code>os</code> property can return the following strings: </p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Operating system</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td>Windows 10</td>
		 *    <td><code>"Windows 10"</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Windows 8</td>
		 *    <td><code>"Windows 8"</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Windows 7</td>
		 *    <td><code>"Windows 7"</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Windows Vista</td>
		 *    <td><code>"Windows Vista"</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Windows Server 2012</td>
		 *    <td><code>"Windows Server 2012"</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Windows Server 2008 R2</td>
		 *    <td><code>"Windows Server 2008 R2"</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Windows Server 2008</td>
		 *    <td><code>"Windows Server 2008"</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Windows Home Server</td>
		 *    <td><code>"Windows Home Server"</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Windows Server 2003 R2</td>
		 *    <td><code>"Windows Server 2003 R2"</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Windows Server 2003</td>
		 *    <td><code>"Windows Server 2003"</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Windows XP 64</td>
		 *    <td><code>"Windows Server XP 64"</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Windows XP</td>
		 *    <td><code>"Windows XP"</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Windows 98</td>
		 *    <td><code>"Windows 98"</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Windows 95</td>
		 *    <td><code>"Windows 95"</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Windows NT</td>
		 *    <td><code>"Windows NT"</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Windows 2000</td>
		 *    <td><code>"Windows 2000"</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Windows ME</td>
		 *    <td><code>"Windows ME"</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Windows CE</td>
		 *    <td><code>"Windows CE"</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Windows SmartPhone</td>
		 *    <td><code>"Windows SmartPhone"</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Windows PocketPC</td>
		 *    <td><code>"Windows PocketPC"</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Windows CEPC</td>
		 *    <td><code>"Windows CEPC"</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Windows Mobile</td>
		 *    <td><code>"Windows Mobile"</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Mac OS</td>
		 *    <td><code>"Mac OS X.Y.Z"</code> (where X.Y.Z is the version number, for example: <code>"Mac OS 10.5.2"</code>)</td>
		 *   </tr>
		 *   <tr>
		 *    <td>Linux</td>
		 *    <td><code>"Linux"</code> (Flash Player attaches the Linux version, such as <code>"Linux 2.6.15-1.2054_FC5smp"</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td>iPhone OS 4.1</td>
		 *    <td><code>"iPhone3,1"</code></td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p>The server string is <code>OS</code>.</p>
		 * <p>Do <i>not</i> use <code>Capabilities.os</code> to determine a capability based on the operating system if a more specific capability property exists. Basing a capability on the operating system is a bad idea, since it can lead to problems if an application does not consider all potential target operating systems. Instead, use the property corresponding to the capability for which you are testing. For more information, see the Capabilities class description.</p>
		 * 
		 * @return 
		 */
		public static function get os():String {
			return _os;
		}


		/**
		 * <p> Specifies the pixel aspect ratio of the screen. The server string is <code>AR</code>. </p>
		 * 
		 * @return 
		 */
		public static function get pixelAspectRatio():Number {
			return _pixelAspectRatio;
		}


		/**
		 * <p> Specifies the type of runtime environment. This property can have one of the following values: </p>
		 * <ul>
		 *  <li><code>"ActiveX"</code> for the Flash Player ActiveX control used by Microsoft Internet Explorer</li>
		 *  <li><code>"Desktop"</code> for the Adobe AIR runtime (except for SWF content loaded by an HTML page, which has <code>Capabilities.playerType</code> set to <code>"PlugIn"</code>)</li>
		 *  <li><code>"External"</code> for the external Flash Player<span src="flashonly"> or in test mode</span></li>
		 *  <li><code>"PlugIn"</code> for the Flash Player browser plug-in (and for SWF content loaded by an HTML page in an AIR application)</li>
		 *  <li><code>"StandAlone"</code> for the stand-alone Flash Player</li>
		 * </ul>
		 * <p>The server string is <code>PT</code>.</p>
		 * 
		 * @return 
		 */
		public static function get playerType():String {
			return _playerType;
		}


		/**
		 * <p> Specifies the screen color. This property can have the value <code>"color"</code>, <code>"gray"</code> (for grayscale), or <code>"bw"</code> (for black and white). The server string is <code>COL</code>. </p>
		 * 
		 * @return 
		 */
		public static function get screenColor():String {
			return _screenColor;
		}


		/**
		 * <p> Specifies the dots-per-inch (dpi) resolution of the screen, in pixels. The server string is <code>DP</code>. </p>
		 * 
		 * @return 
		 */
		public static function get screenDPI():Number {
			return _screenDPI;
		}


		/**
		 * <p> Specifies the maximum horizontal resolution of the screen. The server string is <code>R</code> (which returns both the width and height of the screen). This property does not update with a user's screen resolution and instead only indicates the resolution at the time <span>Flash Player or </span> an Adobe AIR application started. Also, the value only specifies the primary screen. </p>
		 * 
		 * @return 
		 */
		public static function get screenResolutionX():Number {
			return _screenResolutionX;
		}


		/**
		 * <p> Specifies the maximum vertical resolution of the screen. The server string is <code>R</code> (which returns both the width and height of the screen). This property does not update with a user's screen resolution and instead only indicates the resolution at the time <span>Flash Player or </span> an Adobe AIR application started. Also, the value only specifies the primary screen. </p>
		 * 
		 * @return 
		 */
		public static function get screenResolutionY():Number {
			return _screenResolutionY;
		}


		/**
		 * <p> A URL-encoded string that specifies values for each Capabilities property. </p>
		 * <p>The following example shows a URL-encoded string: </p>
		 * <pre>A=t&amp;SA=t&amp;SV=t&amp;EV=t&amp;MP3=t&amp;AE=t&amp;VE=t&amp;ACC=f&amp;PR=t&amp;SP=t&amp;
		 * 	 SB=f&amp;DEB=t&amp;V=WIN%208%2C5%2C0%2C208&amp;M=Adobe%20Windows&amp;
		 * 	 R=1600x1200&amp;DP=72&amp;COL=color&amp;AR=1.0&amp;OS=Windows%20XP&amp;
		 * 	 L=en&amp;PT=External&amp;AVD=f&amp;LFD=f&amp;WD=f</pre>
		 * 
		 * @return 
		 */
		public static function get serverString():String {
			return _serverString;
		}


		/**
		 * <p> Specifies whether the system supports running 32-bit processes. The server string is <code>PR32</code>. </p>
		 * 
		 * @return 
		 */
		public static function get supports32BitProcesses():Boolean {
			return _supports32BitProcesses;
		}


		/**
		 * <p> Specifies whether the system supports running 64-bit processes. The server string is <code>PR64</code>. </p>
		 * 
		 * @return 
		 */
		public static function get supports64BitProcesses():Boolean {
			return _supports64BitProcesses;
		}


		/**
		 * <p> Specifies the type of touchscreen supported, if any. Values are defined in the flash.system.TouchscreenType class. </p>
		 * 
		 * @return 
		 */
		public static function get touchscreenType():String {
			return _touchscreenType;
		}


		/**
		 * <p> Specifies the Flash Player or Adobe<sup>®</sup> AIR<sup>®</sup> platform and version information. The format of the version number is: <i>platform majorVersion,minorVersion,buildNumber,internalBuildNumber</i>. Possible values for <i>platform</i> are <code>"WIN"</code>, ` <code>"MAC"</code>, <code>"LNX"</code>, and <code>"AND"</code>. Here are some examples of version information: </p>
		 * <pre>
		 * 	 WIN 9,0,0,0  // Flash Player 9 for Windows
		 * 	 MAC 7,0,25,0   // Flash Player 7 for Macintosh
		 * 	 LNX 9,0,115,0  // Flash Player 9 for Linux
		 * 	 AND 10,2,150,0 // Flash Player 10 for Android
		 * 	 </pre>
		 * <p>Do <i>not</i> use <code>Capabilities.version</code> to determine a capability based on the operating system if a more specific capability property exists. Basing a capability on the operating system is a bad idea, since it can lead to problems if an application does not consider all potential target operating systems. Instead, use the property corresponding to the capability for which you are testing. For more information, see the Capabilities class description.</p>
		 * <p>The server string is <code>V</code>.</p>
		 * 
		 * @return 
		 */
		public static function get version():String {
			return _version;
		}


		/**
		 * <p> Specifies whether the system supports multichannel audio of a specific type. The class flash.media.AudioDecoder enumerates the possible types. </p>
		 * <p><i>AIR profile support:</i> Multichannel audio is supported only on AIR for TV devices. On all other devices, this method always returns <code>false</code>. See <a href="http://help.adobe.com/en_US/air/build/WS144092a96ffef7cc16ddeea2126bb46b82f-8000.html"> AIR Profile Support</a> for more information regarding API support across multiple profiles.</p>
		 * <p><b>Note: </b>When using one of the DTS audio codecs, scenarios exist in which <code>hasMultiChannelAudio()</code> returns <code>true</code> but the DTS audio is not played. For example, consider a Blu-ray player with an S/PDIF output, connected to an old amplifier. The old amplifier does not support DTS, but S/PDIF has no protocol to notify the Blu-ray player. If the Blu-ray player sends the DTS stream to the old amplifier, the user hears nothing. Therefore, as a best practice when using DTS, provide a user interface so that the user can indicate if no sound is playing. Then, your application can revert to a different codec. </p>
		 * <p>The following table shows the server string for each multichannel audio type: </p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th align="left">Multichannel audio type</th>
		 *    <th align="left">Server string</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>AudioDecoder.DOLBY_DIGITAL</code></td>
		 *    <td><code>DD</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>AudioDecoder.DOLBY_DIGITAL_PLUS</code></td>
		 *    <td><code>DDP</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>AudioDecoder.DTS</code></td>
		 *    <td><code>DTS</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>AudioDecoder.DTS_EXPRESS</code></td>
		 *    <td><code>DTE</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>AudioDecoder.DTS_HD_HIGH_RESOLUTION_AUDIO</code></td>
		 *    <td><code>DTH</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>AudioDecoder.DTS_HD_MASTER_AUDIO</code></td>
		 *    <td><code>DTM</code></td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * 
		 * @param type  — A String value representing a multichannel audio type. The valid values are the constants defined in flash.media.AudioDecoder. 
		 * @return  — The Boolean value  if the system supports the multichannel audio type passed in the  parameter. Otherwise, the return value is . 
		 */
		public static function hasMultiChannelAudio(type:String):Boolean {
			throw new Error("Not implemented");
		}
	}
}
