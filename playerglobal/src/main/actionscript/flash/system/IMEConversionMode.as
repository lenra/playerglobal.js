package flash.system {
	/**
	 *  This class contains constants for use with the <code>IME.conversionMode</code> property. Setting <code>conversionMode</code> to either <code>ALPHANUMERIC_FULL</code> or <code>JAPANESE_KATAKANA_FULL</code> causes the player to use a full width font, whereas using <code>ALPHANUMERIC_HALF</code> or <code>JAPANESE_KATAKANA_HALF</code> uses a half width font. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="IME.html#conversionMode" target="">flash.system.IME.conversionMode</a>
	 * </div><br><hr>
	 */
	public class IMEConversionMode {
		/**
		 * <p> The string <code>"ALPHANUMERIC_FULL"</code>, for use with the <code>IME.conversionMode</code> property. This constant is used with all IMEs. Use the syntax <code>IMEConversionMode.ALPHANUMERIC_FULL</code>. </p>
		 * <p id="moreExamples"><span class="label">More examples</span></p>
		 * <div class="seeAlso">
		 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7cd5.html" target="_blank">Using the IME class</a>
		 * </div>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="IME.html#conversionMode" target="">flash.system.IME.conversionMode</a>
		 * </div>
		 */
		public static const ALPHANUMERIC_FULL:String = "ALPHANUMERIC_FULL";
		/**
		 * <p> The string <code>"ALPHANUMERIC_HALF"</code>, for use with the <code>IME.conversionMode</code> property. This constant is used with all IMEs. Use the syntax <code>IMEConversionMode.ALPHANUMERIC_HALF</code>. </p>
		 * <p id="moreExamples"><span class="label">More examples</span></p>
		 * <div class="seeAlso">
		 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7cd5.html" target="_blank">Using the IME class</a>
		 * </div>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="IME.html#conversionMode" target="">flash.system.IME.conversionMode</a>
		 * </div>
		 */
		public static const ALPHANUMERIC_HALF:String = "ALPHANUMERIC_HALF";
		/**
		 * <p> The string <code>"CHINESE"</code>, for use with the <code>IME.conversionMode</code> property. This constant is used with simplified and traditional Chinese IMEs. Use the syntax <code>IMEConversionMode.CHINESE</code>. </p>
		 * <p id="moreExamples"><span class="label">More examples</span></p>
		 * <div class="seeAlso">
		 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7cd5.html" target="_blank">Using the IME class</a>
		 * </div>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="IME.html#conversionMode" target="">flash.system.IME.conversionMode</a>
		 * </div>
		 */
		public static const CHINESE:String = "CHINESE";
		/**
		 * <p> The string <code>"JAPANESE_HIRAGANA"</code>, for use with the <code>IME.conversionMode</code> property. This constant is used with Japanese IMEs. Use the syntax <code>IMEConversionMode.JAPANESE_HIRAGANA</code>. </p>
		 * <p id="moreExamples"><span class="label">More examples</span></p>
		 * <div class="seeAlso">
		 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7cd5.html" target="_blank">Using the IME class</a>
		 * </div>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="IME.html#conversionMode" target="">flash.system.IME.conversionMode</a>
		 * </div>
		 */
		public static const JAPANESE_HIRAGANA:String = "JAPANESE_HIRAGANA";
		/**
		 * <p> The string <code>"JAPANESE_KATAKANA_FULL"</code>, for use with the <code>IME.conversionMode</code> property. This constant is used with Japanese IMEs. Use the syntax <code>IMEConversionMode.JAPANESE_KATAKANA_FULL</code>. </p>
		 * <p id="moreExamples"><span class="label">More examples</span></p>
		 * <div class="seeAlso">
		 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7cd5.html" target="_blank">Using the IME class</a>
		 * </div>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="IME.html#conversionMode" target="">flash.system.IME.conversionMode</a>
		 * </div>
		 */
		public static const JAPANESE_KATAKANA_FULL:String = "JAPANESE_KATAKANA_FULL";
		/**
		 * <p> The string <code>"JAPANESE_KATAKANA_HALF"</code>, for use with the <code>IME.conversionMode</code> property. This constant is used with Japanese IMEs. Use the syntax <code>IMEConversionMode.JAPANESE_KATAKANA_HALF</code>. </p>
		 * <p id="moreExamples"><span class="label">More examples</span></p>
		 * <div class="seeAlso">
		 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7cd5.html" target="_blank">Using the IME class</a>
		 * </div>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="IME.html#conversionMode" target="">flash.system.IME.conversionMode</a>
		 * </div>
		 */
		public static const JAPANESE_KATAKANA_HALF:String = "JAPANESE_KATAKANA_HALF";
		/**
		 * <p> The string <code>"KOREAN"</code>, for use with the <code>IME.conversionMode</code> property. This constant is used with Korean IMEs. Use the syntax <code>IMEConversionMode.KOREAN</code>. </p>
		 * <p id="moreExamples"><span class="label">More examples</span></p>
		 * <div class="seeAlso">
		 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7cd5.html" target="_blank">Using the IME class</a>
		 * </div>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="IME.html#conversionMode" target="">flash.system.IME.conversionMode</a>
		 * </div>
		 */
		public static const KOREAN:String = "KOREAN";
		/**
		 * <p> The string <code>"UNKNOWN"</code>, which can be returned by a call to the <code>IME.conversionMode</code> property. This value cannot be set, and is returned only if the player is unable to identify the currently active IME. Use the syntax <code>IMEConversionMode.UNKNOWN</code>. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="IME.html#conversionMode" target="">flash.system.IME.conversionMode</a>
		 * </div>
		 */
		public static const UNKNOWN:String = "UNKNOWN";
	}
}
