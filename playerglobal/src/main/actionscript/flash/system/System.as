package flash.system {
	/**
	 *  The System class contains properties related to local settings and operations. Among these are <span> settings for camers and microphones, operations with shared objects and</span> the use of the Clipboard. <p>Additional properties and methods are in other classes within the flash.system package: the Capabilities class, <span>the IME class,</span> and the Security class.</p> <p>This class contains only static methods and properties. You cannot create new instances of the System class.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7cd6.html" target="_blank">Using the System class</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Security.html" target="">flash.system.Security</a>
	 *  <br>
	 *  <a href="../../flash/events/IMEEvent.html" target="">flash.events.IMEEvent</a>
	 * </div><br><hr>
	 */
	public class System {
		private static var _useCodePage:Boolean;

		private static var _freeMemory:Number;
		private static var _ime:IME;
		private static var _privateMemory:Number;
		private static var _totalMemory:uint;
		private static var _totalMemoryNumber:Number;


		/**
		 * <p> The amount of memory (in bytes) that is allocated to <span>Adobe<sup>®</sup> Flash<sup>®</sup> Player or</span> Adobe<sup>®</sup> AIR<sup>®</sup> and that is not in use. This unused portion of allocated memory (<code>System.totalMemory</code>) fluctuates as garbage collection takes place. Use this property to monitor garbage collection. </p>
		 * 
		 * @return 
		 */
		public static function get freeMemory():Number {
			return _freeMemory;
		}


		/**
		 * <p> The currently installed system IME. To register for imeComposition events, call <code>addEventListener()</code> on this instance. </p>
		 * 
		 * @return 
		 */
		public static function get ime():IME {
			return _ime;
		}


		/**
		 * <p> The entire amount of memory (in bytes) used by an application. This is the amount of resident private memory for the entire process. </p>
		 * <p>AIR developers should use this property to determine the entire memory consumption of an application.</p>
		 * <p>For Flash Player, this includes the memory used by the container application, such as the web browser.</p>
		 * 
		 * @return 
		 */
		public static function get privateMemory():Number {
			return _privateMemory;
		}


		/**
		 * <p> The amount of memory (in bytes) currently in use that has been directly allocated by <span>Flash Player or</span> AIR. </p>
		 * <p>This property does not return <i>all</i> memory used by an Adobe AIR application <span>or by the application (such as a browser) containing Flash Player content</span>. The <span>browser or</span> operating system may consume other memory. The <code>System.privateMemory</code> property reflects <i>all</i> memory used by an application.</p>
		 * <p>If the amount of memory allocated is greater than the maximum value for a uint object (<code>uint.MAX_VALUE</code>, or 4,294,967,295), then this property is set to 0. The <code>System.totalMemoryNumber</code> property allows larger values.</p>
		 * 
		 * @return 
		 */
		public static function get totalMemory():uint {
			return _totalMemory;
		}


		/**
		 * <p> The amount of memory (in bytes) currently in use that has been directly allocated by <span>Flash Player or</span> AIR. </p>
		 * <p>This property is expressed as a Number, which allows higher values than the <code>System.totalMemory</code> property, which is of type int.</p>
		 * <p>This property does not return <i>all</i> memory used by an Adobe AIR application <span>or by the application (such as a browser) containing Flash Player content</span>. The <span>browser or</span> operating system may consume other memory. The <code>System.privateMemory</code> property reflects <i>all</i> memory used by an application.</p>
		 * 
		 * @return 
		 */
		public static function get totalMemoryNumber():Number {
			return _totalMemoryNumber;
		}


		/**
		 * <p> A Boolean value that determines which code page to use to interpret external text files. When the property is set to <code>false</code>, external text files are interpretted as Unicode. (These files must be encoded as Unicode when you save them.) When the property is set to <code>true</code>, external text files are interpretted using the traditional code page of the operating system running the application. The default value of <code>useCodePage</code> is <code>false</code>. </p>
		 * <p>Text that you load as an external file (using <code>Loader.load()</code>, the URLLoader class or URLStream) must have been saved as Unicode in order for the application to recognize it as Unicode. To encode external files as Unicode, save the files in an application that supports Unicode, such as Notepad on Windows.</p>
		 * <p>If you load external text files that are not Unicode-encoded, set <code>useCodePage</code> to <code>true</code>. Add the following as the first line of code of the file that is loading the data (for Flash Professional, add it to the first frame):</p>
		 * <pre><code>System.useCodePage = true;</code></pre>
		 * <p>When this code is present, the application interprets external text using the traditional code page of the operating system. For example, this is generally CP1252 for an English Windows operating system and Shift-JIS for a Japanese operating system.</p>
		 * <p><span>If you set <code>useCodePage</code> to <code>true</code>, Flash Player 6 and later treat text as Flash Player 5 does. (Flash Player 5 treated all text as if it were in the traditional code page of the operating system running the player.)</span></p>
		 * <p>If you set <code>useCodePage</code> to <code>true</code>, remember that the traditional code page of the operating system running the application must include the characters used in your external text file in order to display your text. For example, if you load an external text file that contains Chinese characters, those characters cannot display on a system that uses the CP1252 code page because that code page does not include Chinese characters.</p>
		 * <p>To ensure that users on all platforms can view external text files used in your application, you should encode all external text files as Unicode and leave <code>useCodePage</code> set to <code>false</code>. This way, the application <span>(Flash Player 6 and later, or AIR)</span> interprets the text as Unicode.</p>
		 * 
		 * @return 
		 */
		public static function get useCodePage():Boolean {
			return _useCodePage;
		}


		/**
		 * @param value
		 * @return 
		 */
		public static function set useCodePage(value:Boolean):void {
			_useCodePage = value;
		}


		/**
		 * <p> Makes the specified XML object immediately available for garbage collection. This method will remove parent and child connections between all the nodes for the specified XML node. </p>
		 * 
		 * @param node  — XML reference that should be made available for garbage collection. 
		 */
		public static function disposeXML(node:XML):void {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Closes Flash Player. </p>
		 * <p><i>For the standalone Flash Player debugger version only.</i></p>
		 * <p>AIR applications should call the <code>NativeApplication.exit()</code> method to exit the application.</p>
		 * 
		 * @param code  — A value to pass to the operating system. Typically, if the process exits normally, the value is 0. 
		 */
		public static function exit(code:uint):void {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Forces the garbage collection process. </p>
		 * <p><span><i>For the Flash Player debugger version and AIR applications only.</i></span> In an AIR application, the <code>System.gc()</code> method is only enabled in content running in the AIR Debug Launcher (ADL) or, in an installed applcation, in content in the application security sandbox.</p>
		 */
		public static function gc():void {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Pauses <span>Flash Player or</span> the AIR Debug Launcher (ADL). After calling this method, nothing in the application continues except the delivery of Socket events. </p>
		 * <p><i>For the Flash Player debugger version or the AIR Debug Launcher (ADL) only.</i></p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="System.html#resume()" target="">resume()</a>
		 * </div>
		 */
		public static function pause():void {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Advise the garbage collector that if the collector's imminence exceeds the function's imminence parameter then the collector should finish the incremental collection cycle. </p>
		 * <p>The Flash Runtime garbage collector algorithm runs incrementally while marking memory in use. It pauses application execution when collecting unused portions of memory. The pause that occurs as the incremental collection cycle finishes can be longer than desired and can be observable or audible in some programs. This function allows the application to advise the runtime that it is a good time to both complete the marking and perform collection. Scheduling potential pauses for times when the user won't notice them makes for a better user experience. For example, a game might call this function upon the completion of a level in a game, thus reducing the chances of a pause occurring during gameplay.</p>
		 * <p>Imminence is defined as how far through marking the collector believes it is, and therefore how close it is to triggering a collection pause. The imminence argument to this function is a threshold: the garbage collector will be invoked only if the actual imminence exceeds the threshold value. Otherwise, this call returns immediately without taking action.</p>
		 * <p>By calling this function with a low imminence value, the application indicates that it is willing to accept that a relatively large amount of marking&nbsp;must be completed. A high imminence value, on the other hand, indicates that the application&nbsp;should be paused only if marking is nearly complete. &nbsp;Typically, pauses are longer in the former case than in the latter.</p>
		 * <p>The amount of memory being freed does not depend on the imminence parameter. It only depends on the number of freeable objects. If the application has recently released references to large data structures or to a large number of objects, a low imminence parameter will tend to trigger a collection that will free those objects immediately.</p>
		 * 
		 * @param imminence  — A number between 0 and 1, where 0 means less imminent and 1 means most imminent. Values less than 0 default to 0.25. Values greater than 1.0 default to 1.0. NaN defaults to 0.75 
		 */
		public static function pauseForGCIfCollectionImminent(imminence:Number = 0.75):void {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Resumes the application after calling <code>System.pause()</code>. </p>
		 * <p><i>For the Flash Player debugger version or the AIR Debug Launcher (ADL) only.</i></p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="System.html#pause()" target="">pause()</a>
		 * </div>
		 */
		public static function resume():void {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Replaces the contents of the Clipboard with a specified text string. This method works from any security context when called as a result of a user event (such as a keyboard or input device event handler). </p>
		 * <p>This method is provided for SWF content running in Flash Player 9. It allows only adding String content to the Clipboard.</p>
		 * <p>Flash Player 10 content and content in the application security sandbox in an AIR application can call the <code>Clipboard.setData()</code> method.</p>
		 * 
		 * @param string  — A plain-text string of characters to put on the system Clipboard, replacing its current contents (if any). 
		 */
		public static function setClipboard(string:String):void {
			throw new Error("Not implemented");
		}
	}
}
