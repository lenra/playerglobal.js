package flash.system {
	/**
	 *  This class defines constants that represent the possible values for the MessageChannel class's <code>state</code> property. These values are the states in a MessageChannel object's lifecycle. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="MessageChannel.html" target="">MessageChannel class</a>
	 *  <br>
	 *  <a href="MessageChannel.html#state" target="">MessageChannel.state property</a>
	 * </div><br><hr>
	 */
	public class MessageChannelState {
		/**
		 * <p> This state indicates that the message channel has been closed and doesn't have any more messages to deliver. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="MessageChannel.html#state" target="">MessageChannel.state property</a>
		 * </div>
		 */
		public static const CLOSED:String = "closed";
		/**
		 * <p> This state indicates that the message channel has been instructed to close and is in the process of delivering the remaining messages on the channel. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="MessageChannel.html#state" target="">MessageChannel.state property</a>
		 * </div>
		 */
		public static const CLOSING:String = "closing";
		/**
		 * <p> This state indicates that the message channel is open and available for use. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="MessageChannel.html#state" target="">MessageChannel.state property</a>
		 * </div>
		 */
		public static const OPEN:String = "open";
	}
}
