package flash.net {
	/**
	 *  The URLVariables class allows you to transfer variables between an application and a server. Use URLVariables objects with methods of the URLLoader class, with the <code>data</code> property of the URLRequest class, and with flash.net package functions. <p><a href="#includeExamplesSummary">View the examples</a></p><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7cfd.html" target="_blank">Loading external data</a>
	 *  <br>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="URLLoader.html" target="">URLLoader</a>
	 * </div><br><hr>
	 */
	public class URLVariables {

		/**
		 * <p> Creates a new URLVariables object. You pass URLVariables objects to the <code>data</code> property of URLRequest objects. </p>
		 * <p>If you call the URLVariables constructor with a string, the <code>decode()</code> method is automatically called to convert the string to properties of the URLVariables object.</p>
		 * 
		 * @param source  — A URL-encoded string containing name/value pairs. 
		 */
		public function URLVariables(source:String = null) {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Converts the variable string to properties of the specified URLVariables object. </p>
		 * <p>This method is used internally by the URLVariables events. Most users do not need to call this method directly.</p>
		 * 
		 * @param source  — A URL-encoded query string containing name/value pairs. 
		 */
		public function decode(source:String):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a string containing all enumerable variables, in the MIME content encoding <i>application/x-www-form-urlencoded</i>. </p>
		 * 
		 * @return  — A URL-encoded string containing name/value pairs. 
		 */
		public function toString():String {
			throw new Error("Not implemented");
		}
	}
}
