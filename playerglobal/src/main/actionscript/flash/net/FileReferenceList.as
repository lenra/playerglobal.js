package flash.net {
	import flash.events.Event;
	import flash.events.EventDispatcher;

	[Event(name="cancel", type="flash.events.Event")]
	[Event(name="select", type="flash.events.Event")]
	/**
	 *  The FileReferenceList class provides a means to let users select one or more files for uploading. A FileReferenceList object represents a group of one or more local files on the user's disk as an array of FileReference objects. For detailed information and important considerations about FileReference objects and the FileReference class, which you use with FileReferenceList, see the FileReference class. <p>To work with the FileReferenceList class:</p> <ul> 
	 *  <li>Instantiate the class: <code>var myFileRef = new FileReferenceList();</code> </li> 
	 *  <li>Call the <code>FileReferenceList.browse()</code> method, which opens a dialog box that lets the user select one or more files for upload: <code>myFileRef.browse();</code> </li> 
	 *  <li>After the <code>browse()</code> method is called successfully, the <code>fileList</code> property of the FileReferenceList object is populated with an array of FileReference objects.</li> 
	 *  <li>Call <code>FileReference.upload()</code> on each element in the <code>fileList</code> array.</li> 
	 * </ul> <p>The FileReferenceList class includes a <code>browse()</code> method and a <code>fileList</code> property for working with multiple files. While a call to <code>FileReferenceList.browse()</code> is executing, SWF file playback pauses in stand-alone and external versions of Flash Player and in AIR for Linux and Mac OS X 10.1 and earlier.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="FileReference.html" target="">FileReference</a>
	 * </div><br><hr>
	 */
	public class FileReferenceList extends EventDispatcher {
		private var _fileList:Array;

		/**
		 * <p> Creates a new FileReferenceList object. A FileReferenceList object contains nothing until you call the <code>browse()</code> method on it and the user selects one or more files. When you call <code>browse()</code> on the FileReference object, the <code>fileList</code> property of the object is populated with an array of <code>FileReference</code> objects. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="FileReference.html" target="">FileReference</a>
		 *  <br>
		 *  <a href="FileReferenceList.html#browse()" target="">FileReferenceList.browse()</a>
		 * </div>
		 */
		public function FileReferenceList() {
			super();
			throw new Error("Not implemented");
		}

		/**
		 * <p> An array of <code>FileReference</code> objects. </p>
		 * <p>When the <code>FileReferenceList.browse()</code> method is called and the user has selected one or more files from the dialog box that the <code>browse()</code> method opens, this property is populated with an array of FileReference objects, each of which represents the files the user selected. You can then use this array to upload each file with the <code>FileReference.upload()</code>method. You must upload one file at a time.</p>
		 * <p>The <code>fileList</code> property is populated anew each time browse() is called on that FileReferenceList object.</p>
		 * <p>The properties of <code>FileReference</code> objects are described in the FileReference class documentation.</p>
		 * 
		 * @return 
		 */
		public function get fileList():Array {
			return _fileList;
		}

		/**
		 * <p> Displays a file-browsing dialog box that lets the user select one or more local files to upload. The dialog box is native to the user's operating system. </p>
		 * <p>In Flash Player 10 and later, you can call this method successfully only in response to a user event (for example, in an event handler for a mouse click or keypress event). Otherwise, calling this method results in Flash Player throwing an Error.</p>
		 * <p>When you call this method and the user successfully selects files, the <code>fileList</code> property of this FileReferenceList object is populated with an array of FileReference objects, one for each file that the user selects. Each subsequent time that the FileReferenceList.browse() method is called, the <code>FileReferenceList.fileList</code> property is reset to the file(s) that the user selects in the dialog box.</p>
		 * <p>Using the <code>typeFilter</code> parameter, you can determine which files the dialog box displays.</p>
		 * <p>Only one <code>FileReference.browse()</code>, <code>FileReference.download()</code>, or <code>FileReferenceList.browse()</code> session can be performed at a time on a FileReferenceList object (because only one dialog box can be opened at a time).</p>
		 * 
		 * @param typeFilter  — An array of FileFilter instances used to filter the files that are displayed in the dialog box. If you omit this parameter, all files are displayed. For more information, see the <a href="FileFilter.html">FileFilter</a> class. 
		 * @return  — Returns  if the parameters are valid and the file-browsing dialog box opens. 
		 */
		public function browse(typeFilter:Array = null):Boolean {
			throw new Error("Not implemented");
		}
	}
}
