package flash.net.drm {
	/**
	 *  The DRMPlaybackTimeWindow class represents the period of time during which a DRM voucher is valid. <p>The <code>startDate</code> and <code>endDate</code> properties are <code>null</code> until the first time that the user views the content.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="DRMVoucher.html" target="">flash.net.drm.DRMVoucher</a>
	 * </div><br><hr>
	 */
	public class DRMPlaybackTimeWindow {
		private var _endDate:Date;
		private var _period:uint;
		private var _startDate:Date;

		/**
		 * <p> The end date for the period of time during which a DRM voucher is valid (the playback window). </p>
		 * <p>The <code>endDate</code> is <code>null</code> if the playback window has not started.</p>
		 * 
		 * @return 
		 */
		public function get endDate():Date {
			return _endDate;
		}

		/**
		 * <p> The period of time during which a DRM voucher is valid (the playback window), in seconds. </p>
		 * 
		 * @return 
		 */
		public function get period():uint {
			return _period;
		}

		/**
		 * <p> The start date for the period of time during which a DRM voucher is valid (the playback window). </p>
		 * <p>The <code>startDate</code> is <code>null</code> if the playback window has not started.</p>
		 * 
		 * @return 
		 */
		public function get startDate():Date {
			return _startDate;
		}
	}
}
