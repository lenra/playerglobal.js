package flash.net.drm {
	import flash.events.DRMAuthenticationCompleteEvent;
	import flash.events.DRMAuthenticationErrorEvent;
	import flash.events.DRMErrorEvent;
	import flash.events.DRMStatusEvent;
	import flash.events.EventDispatcher;
	import flash.utils.ByteArray;

	[Event(name="authenticationComplete", type="flash.events.DRMAuthenticationCompleteEvent")]
	[Event(name="authenticationError", type="flash.events.DRMAuthenticationErrorEvent")]
	[Event(name="drmError", type="flash.events.DRMErrorEvent")]
	[Event(name="drmStatus", type="flash.events.DRMStatusEvent")]
	/**
	 *  The DRMManager manages the retrieval and storage of the vouchers needed to view DRM-protected content. With the static <code>DRMManager.getDRMManager()</code> method, you can access the existing DRMManager object to perform the following DRM-management tasks: <ul> 
	 *  <li>Preload vouchers from a media rights server, using a DRMContentData object.</li> 
	 *  <li>Query the local cache for an individual voucher, using a DRMContentData object.</li> 
	 *  <li>Reset all vouchers (AIR only)</li> 
	 * </ul> <p>No method is provided for enumerating all the vouchers in the local cache.</p> <p>Do not create an instance of the DRMManager class. Use the static <code>DRMManager.getDRMManager()</code> to access the existing DRMManager object.</p> <p> <i>AIR profile support:</i> This feature is supported on all desktop operating systems and AIR for TV devices, but it is not supported on mobile devices. You can test for support at run time using the <code>DRMManager.isSupported</code> property. See <a href="http://help.adobe.com/en_US/air/build/WS144092a96ffef7cc16ddeea2126bb46b82f-8000.html"> AIR Profile Support</a> for more information regarding API support across multiple profiles.</p> <p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118676a5be7-8000.html" target="_blank">Using digital rights management</a>
	 *  <br>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flash/net/NetStream.html" target="">flash.net.NetStream</a>
	 *  <br>
	 *  <a href="DRMVoucher.html" target="">flash.net.drm.DRMVoucher</a>
	 *  <br>
	 *  <a href="DRMContentData.html" target="">flash.net.drm.DRMContentData</a>
	 * </div><br><hr>
	 */
	public class DRMManager extends EventDispatcher {
		private static var _networkIdleTimeout:Number;

		private static var _isSupported:Boolean;

		/**
		 * <p> adds the currently running device to a device group. </p>
		 * 
		 * @param deviceGroup
		 * @param forceRefresh
		 */
		public function addToDeviceGroup(deviceGroup:DRMDeviceGroup, forceRefresh:Boolean = false):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Authenticates a user. </p>
		 * <p>Listen for the <code>authenticationComplete</code> and <code>authenticationError</code> events to determine the outcome of the authentication attempt. Multiple <code>authenticate()</code> calls are queued. The AuthenticationCompleteEvent object dispatched for the <code>authenticationComplete</code> event contains contains an authentication token that your application can save.</p>
		 * <p>You can use a saved authentication token, or a token downloaded by another means, to establish an authenticated session with the media rights server in the future. To establish a session using a token, call the DRMManager <code>setAuthenticationToken()</code> method. The properties of the token, such as expiration date, are determined by the settings of the server that generates the token.</p>
		 * <p><b>Important (AIR only):</b> The <code>authenticate()</code> method will not succeed when a user's Internet connection passes through a proxy server requiring authentication. Although such users are not able to preload a DRM voucher that requires authentication, your application can obtain the voucher by beginning playback and using the NetStream <code>setAuthenticationCredentials()</code> method to log the user into both the proxy and the media rights servers. Once the voucher has been obtained, the user can view the content offline (as long as the license represented by the voucher allows offline playback).</p>
		 * 
		 * @param serverURL  — The URL of a media rights server that can provide a voucher for viewing protected content 
		 * @param domain  — A domain on the server (not a network or Internet domain name) 
		 * @param username  — The user name 
		 * @param password  — The user password 
		 */
		public function authenticate(serverURL:String, domain:String, username:String, password:String):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Gets a preview voucher from the license server, which you can use to let a user verify that they can play content on particular computer. This capability lets users verify that they can play content on their computer before buying and downloading the content. It is useful when the user's computer doesn't have the necessary output protection capabilities or necessary software to play content. </p>
		 * <p>Like <code>loadVoucher()</code>, this method is an asynchronous call and issues a DRMStatusEvent on completion or a DRMErrorEvent in case of errors.</p>
		 * 
		 * @param contentData
		 */
		public function loadPreviewVoucher(contentData:DRMContentData):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Loads a voucher from a media rights server or the local voucher cache. </p>
		 * <p>The voucher is loaded according to the <code>setting</code> parameter:</p>
		 * <ul>
		 *  <li>LoadVoucherSetting.FORCE_REFRESH: The voucher is always downloaded from the media rights server.</li>
		 *  <li>LoadVoucherSetting.LOCAL_ONLY: The voucher is only loaded from the local cache.</li>
		 *  <li>LoadVoucherSetting.ALLOW_SERVER: The voucher is loaded from the local cache, if possible, but otherwise is downloaded from the server.</li>
		 * </ul>
		 * <p>The LoadVoucherSetting class defines string constants to use as values for the <code>setting</code> parameter.</p>
		 * <p>When the voucher is successfully loaded, the DRMManager dispatches a DRM status event. Your application can begin playback as soon as the voucher is loaded. The loaded voucher is available in the <code>voucher</code> property of the dispatched DRMStatusEvent object. You can use this voucher object to display the associated media rights information to the user. </p>
		 * <p>If a voucher cannot be loaded from the media rights server, a DRM error event is dispatched. The <code>errorID</code> property of the dispatched DRMErrorEvent object indicates the reason for the failure. Common causes of failure include attempting to connect to the media rights server when the user is offline and attempting to load a voucher when the user is not logged in. Your application can respond to these errors and take corrective action. For example, if authentication credentials are required to download the voucher, you can prompt the user for their account user name and password, call the DRMManager <code>authenticate()</code> method, and then attempt to load the voucher again.</p>
		 * <p>If a voucher cannot be obtained from the local cache and the <code>localOnly</code> setting is used, a DRMErrorEvent is not dispatched. Instead, a DRM status event is dispatched. The <code>detail</code> property of this DRMStatusEvent object is still <code>DRM.voucherObtained</code>, but the <code>voucher</code> property is <code>null</code>.</p>
		 * 
		 * @param contentData  — The DRMContentData object from a DRM-protected media file 
		 * @param setting  — Determines whether the voucher is retrieved from the local cache or the media rights server 
		 */
		public function loadVoucher(contentData:DRMContentData, setting:String):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Removes the currently running device from a device group. </p>
		 * 
		 * @param deviceGroup
		 */
		public function removeFromDeviceGroup(deviceGroup:DRMDeviceGroup):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> In AIR applications this deletes all locally cached digital rights management (DRM) voucher data. For browser based applications this is only available during <a href="http://help.adobe.com/en_US/FlashPlatform/reference/actionscript/3/runtimeErrors.html">3322, 3346, 3323, 3326</a> errors, and behaves like the <a href="http://www.adobe.com/support/documentation/en/flashplayer/help/settings_manager08.html">Protected Content Playback Settings panel</a>. </p>
		 * <p> The application must download the required vouchers again for the user to be able to access encrypted content. Calling this function is equivalent to calling <code>Netstream.resetDRMVouchers()</code>.</p>
		 */
		public function resetDRMVouchers():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns to the license server all vouchers that match all specified criteria. When a voucher is returned, it is removed from on disk storage and from memory. This will not interrupt Netstreams and AVStreams that are currently using the returned vouchers for video playback, but will prohibit future playback. DRMManager will issue a DRMReturnVoucherCompleteEvent on success or a DRMReturnVoucherErrorEvent on error. Will throw an ArgumentError if inServerURL is null or if licenseID and policyID are both null. </p>
		 * 
		 * @param inServerURL  — The license server URLs from which the returned licenses were downloaded. 
		 * @param immediateCommit  — Reserved. The only supported value is true. 
		 * @param licenseID  — (optional) Vouchers matching this licenseID will be returned. If null, policyID will be used instead. 
		 * @param policyID  — (optional) Vouchers matching this policyID will be returned. If null, licenseID will be used instead. If both licenseID and policyID are non-null, only a voucher that matches both licenseID and policyID will be returned. 
		 */
		public function returnVoucher(inServerURL:String, immediateCommit:Boolean, licenseID:String, policyID:String):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets the authentication token to use for communication with the specified server and domain. </p>
		 * <p>Authentication tokens are available from the <code>token</code> property of the DRMAuthenticationCompleteEvent object dispatched after a successful call to the <code>authenticate()</code> method. The token is cached automatically for the session, but you can use the <code>setAuthenticationToken()</code> method to manage tokens directly.</p>
		 * <p>Setting a token overwrites any existing cached token for the server and domain. Set the <code>token</code> parameter to <code>null</code> to clear the cached token.</p>
		 * 
		 * @param serverUrl  — The URL of the media rights server 
		 * @param domain  — The DRMContentData 
		 * @param token  — The authentication token 
		 */
		public function setAuthenticationToken(serverUrl:String, domain:String, token:ByteArray):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> </p>
		 * 
		 * @param voucher
		 */
		public function storeVoucher(voucher:ByteArray):void {
			throw new Error("Not implemented");
		}


		/**
		 * <p> The <code>isSupported</code> property is set to <code>true</code> if the DRMManager class is supported on the current platform, otherwise it is set to <code>false</code>. </p>
		 * 
		 * @return 
		 */
		public static function get isSupported():Boolean {
			return _isSupported;
		}


		/**
		 * @return 
		 */
		public static function get networkIdleTimeout():Number {
			return _networkIdleTimeout;
		}


		/**
		 * @param value
		 * @return 
		 */
		public static function set networkIdleTimeout(value:Number):void {
			_networkIdleTimeout = value;
		}


		/**
		 * <p> Returns an instance of the singleton DRMManager object. </p>
		 * <p>One DRMManager instance exists for each security domain.</p>
		 * 
		 * @return 
		 */
		public static function getDRMManager():DRMManager {
			throw new Error("Not implemented");
		}
	}
}
