package flash.net.drm {
	/**
	 *  A device group signifies a group of playback devices that shares protected-content playback rights. <br><hr>
	 */
	public class DRMDeviceGroup {
		private var _name:String;

		private var _authenticationMethod:String;
		private var _domain:String;
		private var _serverURL:String;

		/**
		 * <p> The type of authentication required to register to this device group. </p>
		 * <p>The supported types of authentication are:</p>
		 * <ul>
		 *  <li>AuthenticationMethod.ANONYMOUS — anyone can register.</li>
		 *  <li>AuthenticationMethod.USERNAME_AND_PASSWORD — the user must supply a valid username and password of an account that is authorized to register to this device group.</li>
		 * </ul>
		 * <p>The AuthenticationMethod class provides string constants to use with the <code>authenticationMethod</code> property.</p>
		 * 
		 * @return 
		 */
		public function get authenticationMethod():String {
			return _authenticationMethod;
		}

		/**
		 * <p> The content domain of the device group registration server to which the user must be authenticated before registering to this device group. </p>
		 * <p><b>Note:</b> The domain returned by this property has nothing to do with network or Internet domain names. In this case, a domain is a group of content or user accounts. For example, a single server could support several domains, each with its own set of content channels and subscribers.</p>
		 * 
		 * @return 
		 */
		public function get domain():String {
			return _domain;
		}

		/**
		 * <p> The domain name of this device group. This value is only set on the object returned in the DRMDeviceGroupEvent dispatched from DRMManager.addToDeviceGroup on success. </p>
		 * <p><b>Note:</b> The domain returned by this property has nothing to do with network or Internet domain names. In this case, a domain is the name of this device group. </p>
		 * 
		 * @return 
		 */
		public function get name():String {
			return _name;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set name(value:String):void {
			_name = value;
		}

		/**
		 * <p> The URL of the registration server for this device group. </p>
		 * 
		 * @return 
		 */
		public function get serverURL():String {
			return _serverURL;
		}
	}
}
