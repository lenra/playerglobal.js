package flash.net.drm {
	import flash.utils.ByteArray;

	/**
	 *  The DRMVoucher class is a handle to the license token that allows a user to view DRM-protected content. <p>The DRMVoucher properties describe the viewing rights conferred by the voucher. You can get a voucher using the <code>loadVoucher()</code> method of the DRMManager object. This method requires a DRMContentData object, obtained with the <code>preloadEmbeddedMetadata()</code> method of the NetStream class (AIR only) or by using the <code>DRMContentData()</code> constructor. When using a media rights server such as Flash Access, you can get a DRMContentData object from the metadata generated by the media packager tool.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="DRMContentData.html" target="">flash.net.drm.DRMContentData</a>
	 *  <br>
	 *  <a href="DRMManager.html#loadVoucher()" target="">flash.net.drm.DRMManager.loadVoucher()</a>
	 *  <br>
	 *  <a href="../../../flash/net/NetStream.html#preloadEmbeddedData()" target="">flash.net.NetStream.preloadEmbeddedData()</a>
	 * </div><br><hr>
	 */
	public class DRMVoucher {
		private var _licenseID:String;
		private var _offlineLeaseEndDate:Date;
		private var _offlineLeaseStartDate:Date;
		private var _playbackTimeWindow:DRMPlaybackTimeWindow;
		private var _policies:Object;
		private var _policyID:String;
		private var _serverURL:String;
		private var _voucherEndDate:Date;
		private var _voucherStartDate:Date;

		/**
		 * <p> The unique license ID for this voucher. </p>
		 * 
		 * @return 
		 */
		public function get licenseID():String {
			return _licenseID;
		}

		/**
		 * <p> The date and time at which this voucher expires for offline playback. </p>
		 * <p>If a voucher is only valid for the current online session, <code>offlineLeaseStartDate</code> is <code>null</code>.</p>
		 * 
		 * @return 
		 */
		public function get offlineLeaseEndDate():Date {
			return _offlineLeaseEndDate;
		}

		/**
		 * <p> The date and time at which this voucher becomes valid for offline playback. </p>
		 * <p>If a voucher is only valid for the current online session, <code>offlineLeaseStartDate</code> is <code>null</code>.</p>
		 * 
		 * @return 
		 */
		public function get offlineLeaseStartDate():Date {
			return _offlineLeaseStartDate;
		}

		/**
		 * <p> The time period, after the first viewing, during which the associated content can be viewed or reviewed. </p>
		 * <p>The time period allotted for viewing begins when the user first views the content and ends after the allotted amount of time has elapsed. If no time is allotted, the value of the <code>playbackTimeWindow</code> property is <code>null</code>. </p>
		 * 
		 * @return 
		 */
		public function get playbackTimeWindow():DRMPlaybackTimeWindow {
			return _playbackTimeWindow;
		}

		/**
		 * <p> The custom application-defined rights, if any, defined by the customer when packaging the content. </p>
		 * <p>If no custom rights have been defined, <code>null</code> will be returned.</p>
		 * 
		 * @return 
		 */
		public function get policies():Object {
			return _policies;
		}

		/**
		 * <p> The unique policy ID for this voucher. </p>
		 * 
		 * @return 
		 */
		public function get policyID():String {
			return _policyID;
		}

		/**
		 * <p> The url to the license server for this DRMVoucher. </p>
		 * 
		 * @return 
		 */
		public function get serverURL():String {
			return _serverURL;
		}

		/**
		 * <p> The date on which this voucher expires. </p>
		 * 
		 * @return 
		 */
		public function get voucherEndDate():Date {
			return _voucherEndDate;
		}

		/**
		 * <p> The beginning of this voucher's validity period. </p>
		 * 
		 * @return 
		 */
		public function get voucherStartDate():Date {
			return _voucherStartDate;
		}

		/**
		 * @return 
		 */
		public function toByteArray():ByteArray {
			throw new Error("Not implemented");
		}
	}
}
