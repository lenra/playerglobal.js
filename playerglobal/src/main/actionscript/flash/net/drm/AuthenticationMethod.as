package flash.net.drm {
	/**
	 *  The AuthenticationMethod class provides string constants enumerating the different types of authentication used by the <code>authenticationMethod</code> property of the DRMContentData class. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="DRMContentData.html" target="">flash.net.drm.DRMContentData</a>
	 * </div><br><hr>
	 */
	public class AuthenticationMethod {
		/**
		 * <p> Indicates that no authentication is required. </p>
		 */
		public static const ANONYMOUS:String = "anonymous";
		/**
		 * <p> Indicates that a valid user name and password are required. </p>
		 */
		public static const USERNAME_AND_PASSWORD:String = "usernameAndPassword";
	}
}
