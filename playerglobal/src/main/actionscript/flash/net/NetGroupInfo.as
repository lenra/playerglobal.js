package flash.net {
	/**
	 *  The NetGroupInfo class specifies various Quality of Service (QoS) statistics related to a NetGroup object's underlying RTMFP Peer-to-Peer data transport. The <code>NetGroup.info</code> property returns a NetGroupInfo object which is a snapshot of the current QoS state. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="NetGroup.html#info" target="">flash.net.NetGroup.info</a>
	 *  <br>
	 *  <a href="NetGroup.html#post()" target="">flash.net.NetGroup.post()</a>
	 *  <br>
	 *  <a href="NetGroup.html#sendToNearest()" target="">flash.net.NetGroup.sendToNearest()</a>
	 *  <br>
	 *  <a href="NetGroup.html#sendToNeighbor()" target="">flash.net.NetGroup.sendToNeighbor()</a>
	 *  <br>
	 *  <a href="NetGroup.html#sendToAllNeighbors()" target="">flash.net.NetGroup.sendToAllNeighbors()</a>
	 *  <br>
	 *  <a href="NetGroup.html#addWantObjects()" target="">flash.net.NetGroup.addWantObjects()</a>
	 *  <br>
	 *  <a href="NetGroup.html#writeRequestedObject()" target="">flash.net.NetGroup.writeRequestedObject()</a>
	 * </div><br><hr>
	 */
	public class NetGroupInfo {
		private var _objectReplicationReceiveBytesPerSecond:Number;
		private var _objectReplicationSendBytesPerSecond:Number;
		private var _postingReceiveControlBytesPerSecond:Number;
		private var _postingReceiveDataBytesPerSecond:Number;
		private var _postingSendControlBytesPerSecond:Number;
		private var _postingSendDataBytesPerSecond:Number;
		private var _routingReceiveBytesPerSecond:Number;
		private var _routingSendBytesPerSecond:Number;

		/**
		 * <p> Specifies the rate at which the local node is receiving objects from peers via the Object Replication system, in bytes per second. </p>
		 * 
		 * @return 
		 */
		public function get objectReplicationReceiveBytesPerSecond():Number {
			return _objectReplicationReceiveBytesPerSecond;
		}

		/**
		 * <p> Specifies the rate at which objects are being copied from the local node to peers by the Object Replication system, in bytes per second. </p>
		 * 
		 * @return 
		 */
		public function get objectReplicationSendBytesPerSecond():Number {
			return _objectReplicationSendBytesPerSecond;
		}

		/**
		 * <p> Specifies the rate at which the local node is receiving posting control overhead messages from peers, in bytes per second. </p>
		 * 
		 * @return 
		 */
		public function get postingReceiveControlBytesPerSecond():Number {
			return _postingReceiveControlBytesPerSecond;
		}

		/**
		 * <p> Specifies the rate at which the local node is receiving posting data from peers, in bytes per second. </p>
		 * 
		 * @return 
		 */
		public function get postingReceiveDataBytesPerSecond():Number {
			return _postingReceiveDataBytesPerSecond;
		}

		/**
		 * <p> Specifies the rate at which the local node is sending posting control overhead messages to peers, in bytes per second. </p>
		 * 
		 * @return 
		 */
		public function get postingSendControlBytesPerSecond():Number {
			return _postingSendControlBytesPerSecond;
		}

		/**
		 * <p> Specifies the rate at which the local node is sending posting data to peers, in bytes per second. </p>
		 * 
		 * @return 
		 */
		public function get postingSendDataBytesPerSecond():Number {
			return _postingSendDataBytesPerSecond;
		}

		/**
		 * <p> Specifies the rate at which the local node is receiving directed routing messages from peers, in bytes per second. </p>
		 * 
		 * @return 
		 */
		public function get routingReceiveBytesPerSecond():Number {
			return _routingReceiveBytesPerSecond;
		}

		/**
		 * <p> Specifies the rate at which the local node is sending directed routing messages to peers, in bytes per second. </p>
		 * 
		 * @return 
		 */
		public function get routingSendBytesPerSecond():Number {
			return _routingSendBytesPerSecond;
		}

		/**
		 * <p> Returns a string containing the values of the properties of the NetGroupInfo object. </p>
		 * 
		 * @return  — A string containing the values of the properties of the NetGroupInfo object 
		 */
		public function toString():String {
			throw new Error("Not implemented");
		}
	}
}
