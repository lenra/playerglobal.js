package flash.net {
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;

	[Event(name="close", type="flash.events.Event")]
	[Event(name="connect", type="flash.events.Event")]
	[Event(name="data", type="flash.events.DataEvent")]
	[Event(name="ioError", type="flash.events.IOErrorEvent")]
	[Event(name="securityError", type="flash.events.SecurityErrorEvent")]
	/**
	 *  The XMLSocket class implements client sockets that let the <span>Flash Player or</span> AIR application communicate with a server computer identified by an IP address or domain name. The XMLSocket class is useful for client-server applications that require low latency, such as real-time chat systems. A traditional HTTP-based chat solution frequently polls the server and downloads new messages using an HTTP request. In contrast, an XMLSocket chat solution maintains an open connection to the server, which lets the server immediately send incoming messages without a request from the client. To use the XMLSocket class, the server computer must run a daemon that understands the protocol used by the XMLSocket class. The protocol is described in the following list: <ul> 
	 *  <li>XML messages are sent over a full-duplex TCP/IP stream socket connection.</li> 
	 *  <li>Each XML message is a complete XML document, terminated by a zero (0) byte.</li> 
	 *  <li>An unlimited number of XML messages can be sent and received over a single XMLSocket connection.</li> 
	 * </ul> <p>Setting up a server to communicate with the XMLSocket object can be challenging. If your application does not require real-time interactivity, use the URLLoader class instead of the XMLSocket class. </p> <p>To use the methods of the XMLSocket class, first use the constructor, <code>new XMLSocket</code>, to create an XMLSocket object.</p> <p>SWF files in the local-with-filesystem sandbox may not use sockets.</p> <p> <i>Socket policy files</i> on the target host specify the hosts from which SWF files can make socket connections, and the ports to which those connections can be made. The security requirements with regard to socket policy files have become more stringent in the last several releases of Flash Player. In all versions of Flash Player, Adobe recommends the use of a socket policy file; in some circumstances, a socket policy file is required. Therefore, if you are using XMLSocket objects, make sure that the target host provides a socket policy file if necessary. </p> <p>The following list summarizes the requirements for socket policy files in different versions of Flash Player:</p> <ul> 
	 *  <li> In Flash Player 9.0.124.0 and later, a socket policy file is required for any XMLSocket connection. That is, a socket policy file on the target host is required no matter what port you are connecting to, and is required even if you are connecting to a port on the same host that is serving the SWF file. </li> 
	 *  <li> In Flash Player versions 9.0.115.0 and earlier, if you want to connect to a port number below 1024, or if you want to connect to a host other than the one serving the SWF file, a socket policy file on the target host is required. </li> 
	 *  <li> In Flash Player 9.0.115.0, even if a socket policy file isn't required, a warning is displayed when using the Flash Debug Player if the target host doesn't serve a socket policy file. </li> 
	 * </ul> <p>However, in Adobe AIR, content in the <code>application</code> security sandbox (content installed with the AIR application) are not restricted by these security limitations.</p> <p>For more information related to security, see the Flash Player Developer Center Topic: <a href="http://www.adobe.com/go/devnet_security_en" target="external">Security</a>.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7cfb.html" target="_blank">Binary client sockets</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7c63.html" target="_blank">Connecting to sockets</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="URLLoader.html#load()" target="">flash.net.URLLoader.load()</a>
	 *  <br>
	 *  <a href="URLLoader.html" target="">flash.net.URLLoader</a>
	 * </div><br><hr>
	 */
	public class XMLSocket extends EventDispatcher {
		private var _timeout:int;

		private var _connected:Boolean;

		/**
		 * <p> Creates a new XMLSocket object. If no parameters are specified, an initially disconnected socket is created. If parameters are specified, a connection is attempted to the specified host and port. </p>
		 * <p><b>Note:</b> It is strongly advised to use the constructor form <b>without parameters</b>, then add any event listeners, then call the <code>connect</code> method with <code>host</code> and <code>port</code> parameters. This sequence guarantees that all event listeners will work properly.</p>
		 * 
		 * @param host  — A fully qualified DNS domain name or an IP address in the form <i>.222.333.444</i>. In Flash Player 9.0.115.0 and AIR 1.0 and later, you can specify IPv6 addresses, such as rtmp://[2001:db8:ccc3:ffff:0:444d:555e:666f]. You can also specify <code>null</code> to connect to the host server on which the SWF file resides. If the SWF file issuing this call is running in a web browser, <code>host</code> must be in the same domain as the SWF file. 
		 * @param port  — The TCP port number on the target host used to establish a connection. In Flash Player 9.0.124.0 and later, the target host must serve a socket policy file specifying that socket connections are permitted from the host serving the SWF file to the specified port. In earlier versions of Flash Player, a socket policy file is required only if you want to connect to a port number below 1024, or if you want to connect to a host other than the one serving the SWF file. 
		 */
		public function XMLSocket(host:String = null, port:int = 0) {
			super();
			throw new Error("Not implemented");
		}

		/**
		 * <p> Indicates whether this XMLSocket object is currently connected. You can also check whether the connection succeeded by registering for the <code>connect</code> event and <code>ioError</code> event. </p>
		 * 
		 * @return 
		 */
		public function get connected():Boolean {
			return _connected;
		}

		/**
		 * <p> Indicates the number of milliseconds to wait for a connection. </p>
		 * <p>If the connection doesn't succeed within the specified time, the connection fails. The default value is 20,000 (twenty seconds).</p>
		 * 
		 * @return 
		 */
		public function get timeout():int {
			return _timeout;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set timeout(value:int):void {
			_timeout = value;
		}

		/**
		 * <p> Closes the connection specified by the XMLSocket object. The <code>close</code> event is dispatched only when the server closes the connection; it is not dispatched when you call the <code>close()</code> method. </p>
		 * <p id="moreExamples"><span class="label">More examples</span></p>
		 * <div class="seeAlso">
		 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7cfb.html" target="_blank">Binary client sockets</a>
		 * </div>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="XMLSocket.html#connect()" target="">connect()</a>
		 * </div>
		 */
		public function close():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Establishes a connection to the specified Internet host using the specified TCP port. </p>
		 * <p>If you specify <code>null</code> for the <code>host</code> parameter, the host contacted is the one where the file calling <code>XMLSocket.connect()</code> resides. For example, if the calling file was downloaded from www.adobe.com, specifying <code>null</code> for the host parameter means you are connecting to www.adobe.com.</p>
		 * <p>You can prevent a file from using this method by setting the <code>allowNetworking</code> parameter of the the <code>object</code> and <code>embed</code> tags in the HTML page that contains the SWF content.</p>
		 * <p>For more information, see the Flash Player Developer Center Topic: <a href="http://www.adobe.com/go/devnet_security_en" target="external">Security</a>.</p>
		 * 
		 * @param host  — A fully qualified DNS domain name or an IP address in the form <i>111.222.333.444</i>. You can also specify <code>null</code> to connect to the host server on which the SWF file resides. If the calling file is a SWF file running in a web browser, <code>host</code> must be in the same domain as the file. 
		 * @param port  — The TCP port number on the target host used to establish a connection. In Flash Player 9.0.124.0 and later, the target host must serve a socket policy file specifying that socket connections are permitted from the host serving the SWF file to the specified port. In earlier versions of Flash Player, a socket policy file is required only if you want to connect to a port number below 1024, or if you want to connect to a host other than the one serving the SWF file. 
		 */
		public function connect(host:String, port:int):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Converts the XML object or data specified in the <code>object</code> parameter to a string and transmits it to the server, followed by a zero (0) byte. If <code>object</code> is an XML object, the string is the XML textual representation of the XML object. The send operation is asynchronous; it returns immediately, but the data may be transmitted at a later time. The <code>XMLSocket.send()</code> method does not return a value indicating whether the data was successfully transmitted. </p>
		 * <p>If you do not connect the XMLSocket object to the server using <code>XMLSocket.connect()</code>), the <code>XMLSocket.send()</code> operation fails.</p>
		 * 
		 * @param object  — An XML object or other data to transmit to the server. 
		 */
		public function send(object:*):void {
			throw new Error("Not implemented");
		}
	}
}
