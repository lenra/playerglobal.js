package flash.net {
	/**
	 *  The URLRequest class captures all of the information in a single HTTP request. URLRequest objects are passed to the <code>load()</code> methods of the Loader, URLStream, and URLLoader classes, and to other loading operations, to initiate URL downloads. They are also passed to the <code>upload()</code> and <code>download()</code> methods of the FileReference class. <p>A SWF file in the local-with-filesystem sandbox may not load data from, or provide data to, a resource that is in the network sandbox. </p> <p>By default, the calling <span>SWF </span>file and the URL you load must be in the same domain. For example, a <span>SWF </span>file at www.adobe.com can load data only from sources that are also at www.adobe.com. <span>To load data from a different domain, place a URL policy file on the server hosting the data.</span> </p> <p> However, in Adobe AIR, content in the application security sandbox (content installed with the AIR application) is not restricted by these security limitations. For content running in Adobe AIR, files in the application security sandbox can access URLs using any of the following URL schemes:</p> <ul> 
	 *  <li> <code>http</code> and <code>https</code> </li> 
	 *  <li> <code>file</code> </li> 
	 *  <li> <code>app-storage</code> </li> 
	 *  <li> <code>app</code> </li> 
	 * </ul> <p>Content <span>running in Adobe AIR </span>that is not in the application security sandbox observes the same restrictions as content running in the browser <span>(in Flash Player)</span>, and loading is governed by the content's domain<span> and any permissions granted in URL policy files</span>.</p> <p>Note: App Transport Security was introduced by Apple in iOS9, which doesn’t allow unsecure connections between App and Web services. Due to this change all the connections which are made to Unsecure web sites via Loader, URLLoader will discontinue and not work due to App Transport Security. Please specify exceptions to the default behaviour by adding keys to the application descriptor of your app.</p> <p>Please specify exceptions to the default behavior by adding keys to InfoAdditions tag of application descriptor of your app.</p> <pre>
	 *   &lt;iPhone&gt;
	 *   &lt;InfoAdditions&gt;
	 *                    &lt;![CDATA[
	 *                           &lt;key&gt;NSAppTransportSecurity&lt;/key&gt;
	 *                               &lt;dict&gt;
	 *                                         &lt;key&gt;NSExceptionDomains&lt;/key&gt;
	 *                               &lt;dict&gt;
	 *                                        &lt;key&gt;www.example.com&lt;/key&gt;
	 *                               &lt;dict&gt;
	 *                                      &lt;!--Include to allow subdomains--&gt;
	 *                                      &lt;key&gt;NSIncludesSubdomains&lt;/key&gt;
	 *                                      &lt;true/&gt;
	 *                                      &lt;!--Include to allow HTTP requests--&gt;
	 *                                      &lt;key&gt;NSTemporaryExceptionAllowsInsecureHTTPLoads&lt;/key&gt;
	 *                                      &lt;true/&gt;
	 *                                       &lt;!--Include to specify minimum TLS version--&gt;
	 *                                       &lt;key&gt;NSTemporaryExceptionMinimumTLSVersion&lt;/key&gt;
	 *                                       &lt;string&gt;TLSv1.1&lt;/string&gt;
	 *                               &lt;/dict&gt;
	 *                               &lt;/dict&gt;
	 *                               &lt;/dict&gt;
	 *                   ]]&gt;
	 *          &lt;/InfoAdditions&gt;
	 *   &lt;/iPhone&gt;
	 *   </pre> <p>For more information related to security, see the Flash Player Developer Center Topic: <a href="http://www.adobe.com/go/devnet_security_en" target="external">Security</a>.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d9e.html" target="_blank">Loading an external SWF file</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b8f6c0-7ffe.html" target="_blank">Basics of networking and communication</a>
	 * </div><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7cfd.html" target="_blank">Loading external data</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118666ade46-7cb2.html" target="_blank">Using the URLRequest class </a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WSb2ba3b1aad8a27b02a2e08d61220f3e44be-7fff.html" target="_blank">Web service requests</a>
	 *  <br>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="FileReference.html" target="">FileReference</a>
	 *  <br>
	 *  <a href="URLRequestHeader.html" target="">URLRequestHeader</a>
	 *  <br>
	 *  <a href="URLRequestDefaults.html" target="">URLRequestDefaults</a>
	 *  <br>
	 *  <a href="URLLoader.html" target="">URLLoader</a>
	 *  <br>
	 *  <a href="URLStream.html" target="">URLStream</a>
	 *  <br>
	 *  <a href="../../flash/html/HTMLLoader.html" target="">HTMLLoader class</a>
	 * </div><br><hr>
	 */
	public class URLRequest {
		private var _authenticate:Boolean = true;
		private var _cacheResponse:Boolean = true;
		private var _contentType:String = "application/x-www-form-urlencoded";
		private var _data:Object;
		private var _digest:String;
		private var _followRedirects:Boolean = true;
		private var _idleTimeout:Number = -1;//TODO: URLRequestDefaults.idleTimeout;
		private var _manageCookies:Boolean = true;
		private var _method:String = URLRequestMethod.GET;
		private var _requestHeaders:Array = [];
		private var _url:String;
		private var _useCache:Boolean = true;
		private var _userAgent:String = null;//TODO: URLRequestDefaults.userAgent;

		/**
		 * <p> Creates a URLRequest object. If <code>System.useCodePage</code> is <code>true</code>, the request is encoded using the system code page, rather than Unicode. If <code>System.useCodePage</code> is <code>false</code>, the request is encoded using Unicode, rather than the system code page. </p>
		 * 
		 * @param url  — The URL to be requested. You can set the URL later by using the <code>url</code> property. 
		 */
		public function URLRequest(url:String = null) {
			this.url = url;
		}

		/**
		 * <p> Specifies whether authentication requests should be handled (<code>true</code> or not (<code>false</code>) for this request. If <code>false</code>, authentication challenges return an HTTP error. </p>
		 * <p>The supported authentication methods are:</p>
		 * <ul>
		 *  <li>Windows—HTTP Basic/Digest, Windows Integrated Authentication (including NTLM and Kerberos), SSL Certificate Authentication.</li>
		 *  <li>Mac—HTTP Basic/Digest, NTLM, SSL Certificate Authentication.</li>
		 * </ul>
		 * <p><b>Note</b>:The <code>FileReference.upload()</code>, <code>FileReference.download()</code>, and <code>HTMLLoader.load()</code> methods do not support the <code>URLRequest.authenticate</code> property.</p>
		 * <p> The default value is <code>true.</code></p>
		 * 
		 * @return 
		 */
		public function get authenticate():Boolean {
			return _authenticate;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set authenticate(value:Boolean):void {
			_authenticate = value;
		}

		/**
		 * <p> Specifies whether successful response data should be cached for this request. When set to <code>true</code>, the AIR application uses the operating system's HTTP cache. </p>
		 * <p><b>Note</b>:The <code>HTMLLoader.load()</code> method does not support the <code>URLRequest.cacheResponse</code> property.</p>
		 * <p> The default value is <code>true.</code></p>
		 * 
		 * @return 
		 */
		public function get cacheResponse():Boolean {
			return _cacheResponse;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set cacheResponse(value:Boolean):void {
			_cacheResponse = value;
		}

		/**
		 * <p> The MIME content type of the content in the the <code>data</code> property. </p>
		 * <p>The default value is <code>application/x-www-form-urlencoded</code>.</p>
		 * <p><b>Note</b>:The <code>FileReference.upload()</code>, <code>FileReference.download()</code>, and <code>HTMLLoader.load()</code> methods do not support the <code>URLRequest.contentType</code> property.</p>
		 * <p>When sending a POST request, the values of the <code>contentType</code> and <code>data</code> properties must correspond properly. The value of the <code>contentType</code> property instructs servers on how to interpret the value of the <code>data</code> property. </p>
		 * <ul>
		 *  <li>If the value of the <code>data</code> property is a URLVariables object, the value of <code>contentType</code> must be <code>application/x-www-form-urlencoded</code>. </li>
		 *  <li> If the value of the <code>data</code> property is any other type, the value of <code>contentType</code> should indicate the type of the POST data that will be sent (which is the binary or string data contained in the value of the <code>data</code> property). </li>
		 *  <li>For <code>FileReference.upload()</code>, the Content-Type of the request is set automatically to <code>multipart/form-data</code>, and the value of the <code>contentType</code> property is ignored.</li>
		 * </ul>
		 * <p> In Flash Player 10 and later, if you use a multipart Content-Type (for example "multipart/form-data") that contains an upload (indicated by a "filename" parameter in a "content-disposition" header within the POST body), the POST operation is subject to the security rules applied to uploads:</p>
		 * <ul>
		 *  <li>The POST operation must be performed in response to a user-initiated action, such as a mouse click or key press.</li>
		 *  <li>If the POST operation is cross-domain (the POST target is not on the same server as the SWF file that is sending the POST request), the target server must provide a URL policy file that permits cross-domain access.</li>
		 * </ul>
		 * <p>Also, for any multipart Content-Type, the syntax must be valid (according to the RFC2046 standards). If the syntax appears to be invalid, the POST operation is subject to the security rules applied to uploads.</p>
		 * 
		 * @return 
		 */
		public function get contentType():String {
			return _contentType;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set contentType(value:String):void {
			_contentType = value;
		}

		/**
		 * <p> An object containing data to be transmitted with the URL request. </p>
		 * <p>This property is used in conjunction with the <code>method</code> property. When the value of <code>method</code> is <code>GET</code>, the value of <code>data</code> is appended to the value of <code>URLRequest.url</code>, using HTTP query-string syntax. When the <code>method</code> value is <code>POST</code> (or any value other than <code>GET</code>), the value of <code>data</code> is transmitted in the body of the HTTP request.</p>
		 * <p>The URLRequest API offers binary <code>POST</code> support and support for URL-encoded variables, as well as support for strings. The data object can be a ByteArray, URLVariables, or String object.</p>
		 * <p>The way in which the data is used depends on the type of object used:</p>
		 * <ul>
		 *  <li>If the object is a ByteArray object, the binary data of the ByteArray object is used as <code>POST</code> data. For <code>GET</code>, data of ByteArray type is not supported. Also, data of ByteArray type is not supported for <code>FileReference.upload()</code> and <code>FileReference.download()</code>.</li>
		 *  <li>If the object is a URLVariables object and the method is <code>POST</code>, the variables are encoded using <i>x-www-form-urlencoded</i> format and the resulting string is used as <code>POST</code> data. An exception is a call to <code>FileReference.upload()</code>, in which the variables are sent as separate fields in a <code>multipart/form-data</code> post.</li>
		 *  <li>If the object is a URLVariables object and the method is <code>GET</code>, the URLVariables object defines variables to be sent with the URLRequest object.</li>
		 *  <li>Otherwise, the object is converted to a string, and the string is used as the <code>POST</code> or <code>GET</code> data.</li>
		 * </ul>
		 * <p>This data is not sent until a method, such as <code>navigateToURL()</code> or <code>FileReference.upload()</code>, uses the URLRequest object.</p>
		 * <p><b>Note</b>: The value of <code>contentType</code> must correspond to the type of data in the <code>data</code> property. See the note in the description of the <code>contentType</code> property.</p>
		 * 
		 * @return 
		 */
		public function get data():Object {
			return _data;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set data(value:Object):void {
			_data = value;
		}

		/**
		 * <p> A string that uniquely identifies the signed Adobe platform component to be stored to (or retrieved from) the Flash Player cache. <span> An Adobe platform component is a signed file (a SWZ file) that contains SWF content that is cached persistently on a user's machine. All SWZ files are signed by Adobe. A digest corresponds to a single cached file; if you change the file in any way, its digest will change in an unpredictable way. By using a digest, you can verify the cached file across multiple domains. Two files with the same digest are the same file, and two files with different digests are not the same file. A file cannot (practically) be created to "spoof" a digest and pretend to be another digest. </span> </p>
		 * <p>The digest is based on an SHA-256 message digest algorithm (64 characters long in hexadecimal format).</p>
		 * <p>For example, the Flex SDK includes a SWZ for the Flex framework (and it provides the digest string for that SWZ file). You can post this SWZ on your web server and load it in your SWF file (using the <code>load()</code> method of a URLLoader object). If the end user's machine already has the matching SWZ file cached, the application uses the cached SWZ file. (A SWZ file matches if its <code>digest</code> matches the one you provide.) Otherwise, the application downloads the SWZ file from the URL you specify. </p>
		 * <p>Only set the <code>digest</code> parameter in a URLRequest object used in a call to the <code>URLLoader.load()</code> method to load a SWZ file. If the <code>digest</code> property of a a URLRequest object is set when it is used in other methods, the application throws an IOError exception.</p>
		 * 
		 * @return 
		 */
		public function get digest():String {
			return _digest;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set digest(value:String):void {
			_digest = value;
		}

		/**
		 * <p> Specifies whether redirects are to be followed (<code>true</code>) or not (<code>false</code>). </p>
		 * <p><b>Note</b>:The <code>FileReference.upload()</code>, <code>FileReference.download()</code>, and <code>HTMLLoader.load()</code> methods do not support the <code>URLRequest.followRedirects</code> property.</p>
		 * <p> The default value is <code>true.</code></p>
		 * 
		 * @return 
		 */
		public function get followRedirects():Boolean {
			return _followRedirects;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set followRedirects(value:Boolean):void {
			_followRedirects = value;
		}

		/**
		 * <p> Specifies the idle timeout value (in milliseconds) for this request. </p>
		 * <p>The idle timeout is the amount of time the client waits for a response from the server, after the connection is established, before abandoning the request.</p>
		 * <p><b>Note</b>: The <code>HTMLLoader.load()</code> method does not support the <code>URLRequest.idleTimeout</code> property. The HTMLLoader class defines its own <code>idleTimeout</code> property.</p>
		 * <p> The default value is <code>initialized from the URLRequestDefaults.idleTimeout property.</code></p>
		 * 
		 * @return 
		 */
		public function get idleTimeout():Number {
			return _idleTimeout;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set idleTimeout(value:Number):void {
			_idleTimeout = value;
		}

		/**
		 * <p> Specifies whether the HTTP protocol stack should manage cookies for this request. When <code>true</code>, cookies are added to the request and response cookies are remembered. If <code>false</code>, cookies are <i>not</i> added to the request and response cookies are <i>not</i> remembered, but users can manage cookies themselves by direct header manipulation. <b>Note:</b> On Windows, you cannot add cookies to a URL request manually when <code>manageCookies</code> is set to <code>true</code>. On other operating systems, adding cookies to a request is permitted irrespective of whether <code>manageCookies</code> is set to <code>true</code> or <code>false</code>. When permitted, you can add cookies to a request manually by adding a <code>URLRequestHeader</code> object containing the cookie data to the <code>requestHeaders</code> array. </p>
		 * <p>On Mac OS, cookies are shared with Safari. To clear cookies on Mac OS:</p>
		 * <ol>
		 *  <li>Open Safari.</li>
		 *  <li>Select Safari &gt; Preferences, and click the Security panel.</li>
		 *  <li>Click the Show Cookies button.</li>
		 *  <li>Click the Reomove All button.</li>
		 * </ol>
		 * <p>To clear cookies on Windows:</p>
		 * <ol>
		 *  <li>Open the Internet Properties control panel, and click the General tab.</li>
		 *  <li>Click the Delete Cookies button. </li>
		 * </ol>
		 * <p> The default value is <code>true.</code></p>
		 * 
		 * @return 
		 */
		public function get manageCookies():Boolean {
			return _manageCookies;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set manageCookies(value:Boolean):void {
			_manageCookies = value;
		}

		/**
		 * <p> Controls the HTTP form submission method. </p>
		 * <p><span>For SWF content running in Flash Player (in the browser), this property is limited to GET or POST operations, and valid values are <code>URLRequestMethod.GET</code> or <code>URLRequestMethod.POST</code>.</span></p>
		 * <p><span>For content running in Adobe AIR, you </span> can use any string value if the content is in the application security sandbox. Otherwise, <span>as with content running in Flash Player,</span> you are restricted to using GET or POST operations.</p>
		 * <p><span>For content running in Adobe AIR, when </span> using the <code>navigateToURL()</code> function, the runtime treats a URLRequest that uses the POST method (one that has its <code>method</code> property set to <code>URLRequestMethod.POST</code>) as using the GET method.</p>
		 * <p><span><b>Note:</b> If running in Flash Player and the referenced form has no body, Flash Player automatically uses a GET operation, even if the method is set to <code>URLRequestMethod.POST</code>. For this reason, it is recommended to always include a "dummy" body to ensure that the correct method is used.</span></p>
		 * <p> The default value is <code>URLRequestMethod.GET.</code></p>
		 * 
		 * @return 
		 */
		public function get method():String {
			return _method;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set method(value:String):void {
			_method = value;
		}

		/**
		 * <p> The array of HTTP request headers to be appended to the HTTP request. The array is composed of URLRequestHeader objects. Each object in the array must be a URLRequestHeader object that contains a name string and a value string, as follows: </p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *      var rhArray:Array = new Array(new URLRequestHeader("Content-Type", "text/html"));
		 *      </pre>
		 * </div>
		 * <p><span>Flash Player and the AIR runtime impose</span> certain restrictions on request headers; for more information, see the URLRequestHeader class description.</p>
		 * <p>Not all methods that accept URLRequest parameters support the <code>requestHeaders</code> property, consult the documentation for the method you are calling. For example, the <code>FileReference.upload()</code> and <code>FileReference.download()</code> methods do not support the <code>URLRequest.requestHeaders</code> property.</p>
		 * <p>Due to browser limitations, custom HTTP request headers are only supported for <code>POST</code> requests, not for <code>GET</code> requests.</p>
		 * 
		 * @return 
		 */
		public function get requestHeaders():Array {
			return _requestHeaders;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set requestHeaders(value:Array):void {
			_requestHeaders = value;
		}

		/**
		 * <p> The URL to be requested. </p>
		 * <p>Be sure to encode any characters that are either described as unsafe in the Uniform Resource Locator specification (see http://www.faqs.org/rfcs/rfc1738.html) or that are reserved in the URL scheme of the URLRequest object (when not used for their reserved purpose). For example, use <code>"%25"</code> for the percent (%) symbol and <code>"%23"</code> for the number sign (#), as in <code>"http://www.example.com/orderForm.cfm?item=%23B-3&amp;discount=50%25"</code>.</p>
		 * <p>By default, the URL must be in the same domain as the calling file, unless the content is running in the <span>Adobe AIR </span>application security sandbox. <span>If you need to load data from a different domain, put a URL policy file on the server that is hosting the data</span>. For more information, see the description of the URLRequest class.</p>
		 * <p><span>For content running in Adobe AIR, files</span> in the application security sandobx — files installed with the AIR application — can access URLs using any of the following URL schemes:</p>
		 * <ul>
		 *  <li><code>http</code> and <code>https</code> </li>
		 *  <li><code>file</code> </li>
		 *  <li><code>app-storage</code> </li>
		 *  <li><code>app</code> </li>
		 * </ul>
		 * <p><b>Note:</b> IPv6 (Internet Protocol version 6) is supported<span> in AIR and in Flash Player 9.0.115.0 and later</span>. IPv6 is a version of Internet Protocol that supports 128-bit addresses (an improvement on the earlier IPv4 protocol that supports 32-bit addresses). You might need to activate IPv6 on your networking interfaces. For more information, see the Help for the operating system hosting the data. If IPv6 is supported on the hosting system, you can specify numeric IPv6 literal addresses in URLs enclosed in brackets ([])<span>, as in the following</span>. </p>
		 * <span> <pre>
		 *      rtmp://[2001:db8:ccc3:ffff:0:444d:555e:666f]:1935/test
		 *      </pre> </span>
		 * 
		 * @return 
		 */
		public function get url():String {
			return _url;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set url(value:String):void {
			_url = value;
		}

		/**
		 * <p> Specifies whether the local cache should be consulted before this URLRequest fetches data. </p>
		 * <p><b>Note</b>:The <code>HTMLLoader.load()</code> method does not support the <code>URLRequest.useCache</code> property.</p>
		 * <p> The default value is <code>true.</code></p>
		 * 
		 * @return 
		 */
		public function get useCache():Boolean {
			return _useCache;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set useCache(value:Boolean):void {
			_useCache = value;
		}

		/**
		 * <p> Specifies the user-agent string to be used in the HTTP request. </p>
		 * <p>The default value is the same user agent string that is used by Flash Player, which is different on Mac, Linux, and Windows.</p>
		 * <p><i>Note:</i> This property does not affect the user agent string when the URLRequest object is used with the <code>load()</code> method of an HTMLLoader object. To set the user agent string for an HTMLLoader object, set the <code>userAgent</code> property of the HTMLLoader object or set the static <code>URLRequestDefaults.userAgent</code> property.</p>
		 * 
		 * @return 
		 */
		public function get userAgent():String {
			return _userAgent;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set userAgent(value:String):void {
			_userAgent = value;
		}

		/**
		 * <p> Allows substitution of a redirected URL from a source URLRequest for some portion of the URL in a new URLRequest. After an initial request has been redirected, this allows subsequent reqeusts to be sent directly to the redirected server location. </p>
		 * <p>Once the initial URLRequest has completed, if <code>HTTPStatusEvent.redirected</code> is true, this method can be called using the inital request as the <code>sourceRequest</code>. By default, the domain from the redirected URL will be used in place of the domain in this URLRequest's URL. <code>URLRequest.url</code> must be set before calling this method. The method must be called before this URLRequest is loaded. Optionally, if the <code>wholeURL</code> parameter is true, the entire url, minus the filename, from the source URLRequest will be substituted for the entire URL minus the final filename in this URLRequest. Also, the caller can optionally supply a string or regular expression in the <code>pattern</code> parameter. The pattern is searched for and replaced with the string from the <code>replace</code> parameter. This search and replace happens after the URL substitution.</p>
		 * 
		 * @param sourceRequest  — An URLRequest object that has already been requested and was redirected. The redirected URL will be used to substitute for part of the URL of this URLRequest object. 
		 * @param wholeURL  — Whether to use just the domain of the sourceRequest final URL to substitute for the domain of this request's URL, or whether to use the entire URL minus the filename. The default is <code>false</code>. 
		 * @param pattern  — A pattern to search for in the URL. Can be either a String or a RegExp object. The default is null. 
		 * @param replace  — A string to use to replace for the pattern if found in the URL. The default is null. 
		 */
		public function useRedirectedURL(sourceRequest:URLRequest, wholeURL:Boolean = false, pattern:* = null, replace:String = null):void {
			throw new Error("Not implemented");
		}
	}
}
