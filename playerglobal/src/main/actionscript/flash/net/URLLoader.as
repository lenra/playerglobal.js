package flash.net {
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.utils.ByteArray;

	[Event(name="complete", type="flash.events.Event")]
	[Event(name="httpResponseStatus", type="flash.events.HTTPStatusEvent")]
	[Event(name="httpStatus", type="flash.events.HTTPStatusEvent")]
	[Event(name="ioError", type="flash.events.IOErrorEvent")]
	[Event(name="open", type="flash.events.Event")]
	[Event(name="progress", type="flash.events.ProgressEvent")]
	[Event(name="securityError", type="flash.events.SecurityErrorEvent")]
	/**
	 *  The URLLoader class downloads data from a URL as text, binary data, or URL-encoded variables. It is useful for downloading text files, XML, or other information to be used in a dynamic, data-driven application. <p>A URLLoader object downloads all of the data from a URL before making it available to code in the applications. It sends out notifications about the progress of the download, which you can monitor through the <code>bytesLoaded</code> and <code>bytesTotal</code> properties, as well as through dispatched events.</p> <p>When loading very large video files, such as FLV's, out-of-memory errors may occur. </p> <p>When you use this class <span>in Flash Player and</span> in AIR application content in security sandboxes other than then application security sandbox, consider the following security model:</p> <ul> 
	 *  <li>A SWF file in the local-with-filesystem sandbox may not load data from, or provide data to, a resource that is in the network sandbox. </li> 
	 *  <li> By default, the calling SWF file and the URL you load must be in exactly the same domain. For example, a SWF file at www.adobe.com can load data only from sources that are also at www.adobe.com. To load data from a different domain, place a URL policy file on the server hosting the data.</li> 
	 * </ul> <p>For more information related to security, see the Flash Player Developer Center Topic: <a href="http://www.adobe.com/go/devnet_security_en" target="external">Security</a>.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e6a.html" target="_blank">Reading external XML documents</a>
	 * </div><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7cfd.html" target="_blank">Loading external data</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7cf5.html" target="_blank">Using the URLLoader class</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WSb2ba3b1aad8a27b02a2e08d61220f3e44be-7fff.html" target="_blank">Web service requests</a>
	 *  <br>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="URLRequest.html" target="">URLRequest</a>
	 *  <br>
	 *  <a href="URLVariables.html" target="">URLVariables</a>
	 *  <br>
	 *  <a href="URLStream.html" target="">URLStream</a>
	 * </div><br><hr>
	 */
	public class URLLoader extends EventDispatcher {
		private var _bytesLoaded:uint;
		private var _bytesTotal:uint;
		private var _data:*;
		private var _dataFormat:String;
		private var xhr:XMLHttpRequest;

		/**
		 * <p> Creates a URLLoader object. </p>
		 * 
		 * @param request  — A URLRequest object specifying the URL to download. If this parameter is omitted, no load operation begins. If specified, the load operation begins immediately (see the <code>load</code> entry for more information). 
		 */
		public function URLLoader(request:URLRequest = null) {
			super(this);
			if (request != null)
				this.load(request);
		}

		/**
		 * <p> Indicates the number of bytes that have been loaded thus far during the load operation. </p>
		 * 
		 * @return 
		 */
		public function get bytesLoaded():uint {
			return _bytesLoaded;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set bytesLoaded(value:uint):void {
			_bytesLoaded = value;
		}

		/**
		 * <p> Indicates the total number of bytes in the downloaded data. This property contains 0 while the load operation is in progress and is populated when the operation is complete. Also, a missing Content-Length header will result in bytesTotal being indeterminate. </p>
		 * 
		 * @return 
		 */
		public function get bytesTotal():uint {
			return _bytesTotal;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set bytesTotal(value:uint):void {
			_bytesTotal = value;
		}

		/**
		 * <p> The data received from the load operation. This property is populated only when the load operation is complete. The format of the data depends on the setting of the <code>dataFormat</code> property: </p>
		 * <p>If the <code>dataFormat</code> property is <code>URLLoaderDataFormat.TEXT</code>, the received data is a string containing the text of the loaded file.</p>
		 * <p>If the <code>dataFormat</code> property is <code>URLLoaderDataFormat.BINARY</code>, the received data is a ByteArray object containing the raw binary data.</p>
		 * <p>If the <code>dataFormat</code> property is <code>URLLoaderDataFormat.VARIABLES</code>, the received data is a URLVariables object containing the URL-encoded variables.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="URLLoaderDataFormat.html" target="">URLLoaderDataFormat</a>
		 *  <br>
		 *  <a href="URLLoader.html#dataFormat" target="">URLLoader.dataFormat</a>
		 * </div>
		 * <span id="pageFilter"><br><span class="label"> Example &nbsp;( <span class="usage"><a href="http://www.adobe.com/go/learn_as3_usingexamples_en"> How to use this example </a></span>) </span><br>
		 *  <div class="detailBody">
		 *    The following example shows how you can load an external text file with URL encoded variables into an ActionScript 3.0 document using the URLLoader class and setting the dataFormat property to the URLLoaderDataFormat.VARIABLES constant ("variables"). Example provided by 
		 *   <a href="http://actionscriptexamples.com/2008/02/27/loading-url-encoded-variables-into-a-flash-application-using-the-urlloader-class-in-actionscript-30/" target="_mmexternal">ActionScriptExamples.com</a>. 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>
		 * //params.txt is a local file that includes: firstName=Tom&amp;lastName=Jones
		 * var lbl:TextField = new TextField();
		 * var urlRequest:URLRequest = new URLRequest("params.txt");
		 * var urlLoader:URLLoader = new URLLoader();
		 * urlLoader.dataFormat = URLLoaderDataFormat.VARIABLES;
		 * urlLoader.addEventListener(Event.COMPLETE, urlLoader_complete);
		 * urlLoader.load(urlRequest);
		 *  
		 * function urlLoader_complete(evt:Event):void {
		 *     lbl.text = urlLoader.data.lastName + "," + urlLoader.data.firstName;
		 *     addChild(lbl);
		 * }
		 * </pre>
		 *   </div>
		 *  </div></span>
		 * 
		 * @return 
		 */
		public function get data():* {
			return _data;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set data(value:*):void {
			_data = value;
		}

		/**
		 * <p> Controls whether the downloaded data is received as text (<code>URLLoaderDataFormat.TEXT</code>), raw binary data (<code>URLLoaderDataFormat.BINARY</code>), or URL-encoded variables (<code>URLLoaderDataFormat.VARIABLES</code>). </p>
		 * <p>If the value of the <code>dataFormat</code> property is <code>URLLoaderDataFormat.TEXT</code>, the received data is a string containing the text of the loaded file.</p>
		 * <p>If the value of the <code>dataFormat</code> property is <code>URLLoaderDataFormat.BINARY</code>, the received data is a ByteArray object containing the raw binary data.</p>
		 * <p>If the value of the <code>dataFormat</code> property is <code>URLLoaderDataFormat.VARIABLES</code>, the received data is a URLVariables object containing the URL-encoded variables.</p>
		 * <p> The default value is <code>URLLoaderDataFormat.TEXT.</code></p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="URLLoaderDataFormat.html" target="">URLLoaderDataFormat</a>
		 * </div>
		 * <span id="pageFilter"><br><span class="label"> Example &nbsp;( <span class="usage"><a href="http://www.adobe.com/go/learn_as3_usingexamples_en"> How to use this example </a></span>) </span><br>
		 *  <div class="detailBody">
		 *    The following example shows how you can load external text files. Use the URLRequest and URLLoader classes, and then listen for the complete event. Example provided by 
		 *   <a href="http://actionscriptexamples.com/2008/02/26/loading-text-files-using-the-urlloader-class-in-actionscript-30/" target="_mmexternal">ActionScriptExamples.com</a>. 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>
		 * var PATH:String = "lorem.txt";
		 * var urlRequest:URLRequest = new URLRequest(PATH);
		 * var urlLoader:URLLoader = new URLLoader();
		 * urlLoader.dataFormat = URLLoaderDataFormat.TEXT; // default
		 * urlLoader.addEventListener(Event.COMPLETE, urlLoader_complete);
		 * urlLoader.load(urlRequest);
		 *  
		 * function urlLoader_complete(evt:Event):void {
		 *     textArea.text = urlLoader.data;
		 * }
		 * </pre>
		 *   </div>
		 *  </div></span>
		 * 
		 * @return 
		 */
		public function get dataFormat():String {
			return _dataFormat;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set dataFormat(value:String):void {
			_dataFormat = value;
		}
		
		/**
		 * <p> Closes the load operation in progress. Any load operation in progress is immediately terminated. If no URL is currently being streamed, an invalid stream error is thrown. </p>
		 */
		public function close():void {
			this.xhr.abort();
		}

		/**
		 * <p> Sends and loads data from the specified URL. The data can be received as text, raw binary data, or URL-encoded variables, depending on the value you set for the <code>dataFormat</code> property. Note that the default value of the <code>dataFormat</code> property is text. If you want to send data to the specified URL, you can set the <code>data</code> property in the URLRequest object. </p>
		 * <p><b>Note:</b> If a file being loaded contains non-ASCII characters (as found in many non-English languages), it is recommended that you save the file with UTF-8 or UTF-16 encoding as opposed to a non-Unicode format like ASCII.</p>
		 * <p> A SWF file in the local-with-filesystem sandbox may not load data from, or provide data to, a resource that is in the network sandbox.</p>
		 * <p> By default, the calling SWF file and the URL you load must be in exactly the same domain. For example, a SWF file at www.adobe.com can load data only from sources that are also at www.adobe.com. To load data from a different domain, place a URL policy file on the server hosting the data.</p>
		 * <p>You cannot connect to commonly reserved ports. For a complete list of blocked ports, see "Restricting Networking APIs" in the <i>ActionScript 3.0 Developer's Guide</i>.</p>
		 * <p> In Flash Player 10 and later, if you use a multipart Content-Type (for example "multipart/form-data") that contains an upload (indicated by a "filename" parameter in a "content-disposition" header within the POST body), the POST operation is subject to the security rules applied to uploads:</p>
		 * <ul>
		 *  <li>The POST operation must be performed in response to a user-initiated action, such as a mouse click or key press.</li>
		 *  <li>If the POST operation is cross-domain (the POST target is not on the same server as the SWF file that is sending the POST request), the target server must provide a URL policy file that permits cross-domain access.</li>
		 * </ul>
		 * <p>Also, for any multipart Content-Type, the syntax must be valid (according to the RFC2046 standards). If the syntax appears to be invalid, the POST operation is subject to the security rules applied to uploads.</p>
		 * <p>For more information related to security, see the Flash Player Developer Center Topic: <a href="http://www.adobe.com/go/devnet_security_en" target="external">Security</a>.</p>
		 * 
		 * @param request  — A URLRequest object specifying the URL to download. 
		 */
		public function load(request:URLRequest):void {
			this.xhr = new XMLHttpRequest();
			this.xhr.onreadystatechange = this.onStateChange;
			// charger la requête
			this.xhr.open(request.method, request.url, true);
			if (request.requestHeaders!=null) {
				for each (var header:URLRequestHeader in request.requestHeaders) {
					this.xhr.setRequestHeader(header.name, header.value);
				}
			}
			if (dataFormat) {
				switch (dataFormat) {
					case URLLoaderDataFormat.BINARY: 
						this.xhr.responseType = "arraybuffer";
					break;
				}
			}
			this.xhr.send();
		}
		
		private function onStateChange(e:Event):void {
			//TODO: gérer la redirection
			var statusEvent:HTTPStatusEvent = new HTTPStatusEvent(HTTPStatusEvent.HTTP_STATUS, false, false, this.xhr.status, false);
			this.dispatchEvent(statusEvent);
			if (xhr.readyState == 4) {
				this.data = this.xhr.response;
				if (dataFormat == URLLoaderDataFormat.BINARY) {
					var ba:ByteArray = new ByteArray();
					var b:ArrayBuffer = this.data;
					var v:Int8Array = new Int8Array(b, 0, b.byteLength);
					ba.length = b.byteLength;
					for (var i:int = 0; i < b.byteLength; i++) {
						ba.writeByte(v[i]);
					}
					this.data = ba;
				}
				this.dispatchEvent(new Event(Event.COMPLETE));
			}
		}
	}
}
