package flash.net {
	/**
	 *  The SharedObjectFlushStatus class provides values for the code returned from a call to the <code>SharedObject.flush()</code> method. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="SharedObject.html#flush()" target="">SharedObject.flush()</a>
	 * </div><br><hr>
	 */
	public class SharedObjectFlushStatus {
		/**
		 * <p> Indicates that the flush completed successfully. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="SharedObject.html#flush()" target="">SharedObject.flush()</a>
		 * </div>
		 */
		public static const FLUSHED:String = "flushed";
		/**
		 * <p> Indicates that the user is being prompted to increase disk space for the shared object before the flush can occur. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="SharedObject.html#flush()" target="">SharedObject.flush()</a>
		 * </div>
		 */
		public static const PENDING:String = "pending";
	}
}
