package flash.net {
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.events.PermissionEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.utils.ByteArray;

	[Event(name="cancel", type="flash.events.Event")]
	[Event(name="complete", type="flash.events.Event")]
	[Event(name="httpResponseStatus", type="flash.events.HTTPStatusEvent")]
	[Event(name="httpStatus", type="flash.events.HTTPStatusEvent")]
	[Event(name="ioError", type="flash.events.IOErrorEvent")]
	[Event(name="open", type="flash.events.Event")]
	[Event(name="permissionStatus", type="flash.events.PermissionEvent")]
	[Event(name="progress", type="flash.events.ProgressEvent")]
	[Event(name="securityError", type="flash.events.SecurityErrorEvent")]
	[Event(name="select", type="flash.events.Event")]
	[Event(name="uploadCompleteData", type="flash.events.DataEvent")]
	/**
	 *  The FileReference class provides a means to upload and download files between a user's computer and a server. An operating-system dialog box prompts the user to select a file to upload or a location for download. Each FileReference object refers to a single file on the user's disk and has properties that contain information about the file's size, type, name, creation date, modification date, and creator type (Macintosh only). <p> <b>Note:</b> In Adobe AIR, the File class, which extends the FileReference class, provides more capabilities and has less security restrictions than the FileReference class.</p> <p>FileReference instances are created in the following ways:</p> <ul> 
	 *  <li>When you use the <code>new</code> operator with the FileReference constructor: <code>var myFileReference = new FileReference();</code> </li> 
	 *  <li>When you call the <code>FileReferenceList.browse()</code> method, which creates an array of FileReference objects.</li> 
	 * </ul> <p>During an upload operation, all the properties of a FileReference object are populated by calls to the <code>FileReference.browse()</code> or <code>FileReferenceList.browse()</code> methods. During a download operation, the <code>name</code> property is populated when the <code>select</code> event is dispatched; all other properties are populated when the <code>complete</code> event is dispatched.</p> <p>The <code>browse()</code> method opens an operating-system dialog box that prompts the user to select a file for upload. The <code>FileReference.browse()</code> method lets the user select a single file; the <code>FileReferenceList.browse()</code> method lets the user select multiple files. After a successful call to the <code>browse()</code> method, call the <code>FileReference.upload()</code> method to upload one file at a time. The <code>FileReference.download()</code> method prompts the user for a location to save the file and initiates downloading from a remote URL.</p> <p>The FileReference and FileReferenceList classes do not let you set the default file location for the dialog box that the <code>browse()</code> or <code>download()</code> methods generate. The default location shown in the dialog box is the most recently browsed folder, if that location can be determined, or the desktop. The classes do not allow you to read from or write to the transferred file. They do not allow the SWF file that initiated the upload or download to access the uploaded or downloaded file or the file's location on the user's disk.</p> <p>The FileReference and FileReferenceList classes also do not provide methods for authentication. With servers that require authentication, you can download files with the Flash<sup>®</sup> Player browser plug-in, but uploading (on all players) and downloading (on the stand-alone or external player) fails. Listen for FileReference events to determine whether operations complete successfully and to handle errors.</p> <p>For <span>content running in Flash Player or for</span> content running in Adobe AIR outside of the application security sandbox, uploading and downloading operations can access files only within its own domain and within any domains that a URL policy file specifies. Put a policy file on the file server if the content initiating the upload or download doesn't come from the same domain as the file server.</p> <p>Note that because of new functionality added to the Flash Player, when publishing to Flash Player 10, you can have only one of the following operations active at one time: <code>FileReference.browse()</code>, <code>FileReference.upload()</code>, <code>FileReference.download()</code>, <code>FileReference.load()</code>, <code>FileReference.save()</code>. Otherwise, Flash Player throws a runtime error (code 2174). Use <code>FileReference.cancel()</code> to stop an operation in progress. This restriction applies only to Flash Player 10. Previous versions of Flash Player are unaffected by this restriction on simultaneous multiple operations.</p> <p>While calls to the <code>FileReference.browse()</code>, <code>FileReferenceList.browse()</code>, or <code>FileReference.download()</code> methods are executing, SWF file playback pauses in stand-alone and external versions of Flash Player and in AIR for Linux and Mac OS X 10.1 and earlier</p> <p>The following sample HTTP <code>POST</code> request is sent from Flash Player to a server-side script if no parameters are specified: </p> <pre>
	 *   POST /handler.cfm HTTP/1.1 
	 *   Accept: text/*
	 *   Content-Type: multipart/form-data; 
	 *   boundary=----------Ij5ae0ae0KM7GI3KM7 
	 *   User-Agent: Shockwave Flash 
	 *   Host: www.example.com 
	 *   Content-Length: 421 
	 *   Connection: Keep-Alive 
	 *   Cache-Control: no-cache
	 *   
	 *   ------------Ij5GI3GI3ei4GI3ei4KM7GI3KM7KM7
	 *   Content-Disposition: form-data; name="Filename"
	 *   
	 *   MyFile.jpg
	 *   ------------Ij5GI3GI3ei4GI3ei4KM7GI3KM7KM7
	 *   Content-Disposition: form-data; name="Filedata"; filename="MyFile.jpg"
	 *   Content-Type: application/octet-stream
	 *   
	 *   FileDataHere
	 *   ------------Ij5GI3GI3ei4GI3ei4KM7GI3KM7KM7
	 *   Content-Disposition: form-data; name="Upload"
	 *   
	 *   Submit Query
	 *   ------------Ij5GI3GI3ei4GI3ei4KM7GI3KM7KM7--
	 *   </pre> <p>Flash Player sends the following HTTP <code>POST</code> request if the user specifies the parameters <code>"api_sig"</code>, <code>"api_key"</code>, and <code>"auth_token"</code>: </p> <pre>
	 *   POST /handler.cfm HTTP/1.1 
	 *   Accept: text/*
	 *   Content-Type: multipart/form-data; 
	 *   boundary=----------Ij5ae0ae0KM7GI3KM7 
	 *   User-Agent: Shockwave Flash 
	 *   Host: www.example.com 
	 *   Content-Length: 421 
	 *   Connection: Keep-Alive 
	 *   Cache-Control: no-cache
	 *   
	 *   ------------Ij5GI3GI3ei4GI3ei4KM7GI3KM7KM7
	 *   Content-Disposition: form-data; name="Filename"
	 *   
	 *   MyFile.jpg
	 *   ------------Ij5GI3GI3ei4GI3ei4KM7GI3KM7KM7
	 *   Content-Disposition: form-data; name="api_sig"
	 *   
	 *   XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
	 *   ------------Ij5GI3GI3ei4GI3ei4KM7GI3KM7KM7
	 *   Content-Disposition: form-data; name="api_key"
	 *   
	 *   XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
	 *   ------------Ij5GI3GI3ei4GI3ei4KM7GI3KM7KM7
	 *   Content-Disposition: form-data; name="auth_token"
	 *   
	 *   XXXXXXXXXXXXXXXXXXXXXX
	 *   ------------Ij5GI3GI3ei4GI3ei4KM7GI3KM7KM7
	 *   Content-Disposition: form-data; name="Filedata"; filename="MyFile.jpg"
	 *   Content-Type: application/octet-stream
	 *   
	 *   FileDataHere
	 *   ------------Ij5GI3GI3ei4GI3ei4KM7GI3KM7KM7
	 *   Content-Disposition: form-data; name="Upload"
	 *   
	 *   Submit Query
	 *   ------------Ij5GI3GI3ei4GI3ei4KM7GI3KM7KM7--
	 *   </pre> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7cf8.html" target="_blank">Using the FileReference class</a>
	 *  <br>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="FileReferenceList.html" target="">flash.net.FileReferenceList</a>
	 *  <br>
	 *  <a href="../../flash/filesystem/File.html" target="">flash.filesystem.File</a>
	 * </div><br><hr>
	 */
	public class FileReference extends EventDispatcher {
		private static var _permissionStatus:String;

		private var _creationDate:Date;
		private var _creator:String;
		private var _data:ByteArray;
		private var _extension:String;
		private var _modificationDate:Date;
		private var _name:String;
		private var _size:Number;
		private var _type:String;

		/**
		 * <p> Creates a new FileReference object. When populated, a FileReference object represents a file on the user's local disk. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="FileReference.html#browse()" target="">FileReference.browse()</a>
		 * </div>
		 */
		public function FileReference() {
			super();
			throw new Error("Not implemented");
		}

		/**
		 * <p> The creation date of the file on the local disk. If the object is was not populated, a call to get the value of this property returns <code>null</code>. </p>
		 * <p>Note: If an operating system does not have <code>creationDate</code> as its property, in such cases, <code>CreationDate</code> is equal to <code>lastModifiedDate</code>.</p>
		 * 
		 * @return 
		 */
		public function get creationDate():Date {
			return _creationDate;
		}

		/**
		 * <p> The Macintosh creator type of the file, which is only used in Mac OS versions prior to Mac OS X. In Windows or Linux, this property is <code>null</code>. If the FileReference object was not populated, a call to get the value of this property returns <code>null</code>. </p>
		 * 
		 * @return 
		 */
		public function get creator():String {
			return _creator;
		}

		/**
		 * <p> The ByteArray object representing the data from the loaded file after a successful call to the <code>load()</code> method. </p>
		 * 
		 * @return 
		 */
		public function get data():ByteArray {
			return _data;
		}

		/**
		 * <p> The filename extension. </p>
		 * <p>A file's extension is the part of the name following (and not including) the final dot ("."). If there is no dot in the filename, the extension is <code>null</code>.</p>
		 * <p><i>Note:</i> You should use the <code>extension</code> property to determine a file's type; do not use the <code>creator</code> or <code>type</code> properties. You should consider the <code>creator</code> and <code>type</code> properties to be considered deprecated. They apply to older versions of Mac OS.</p>
		 * 
		 * @return 
		 */
		public function get extension():String {
			return _extension;
		}

		/**
		 * <p> The date that the file on the local disk was last modified. If the FileReference object was not populated, a call to get the value of this property returns <code>null</code>. </p>
		 * 
		 * @return 
		 */
		public function get modificationDate():Date {
			return _modificationDate;
		}

		/**
		 * <p> The name of the file on the local disk. If the FileReference object was not populated (by a valid call to <code>FileReference.download()</code> or <code> FileReference.browse()</code>), Flash Player throws an error when you try to get the value of this property. </p>
		 * <p>All the properties of a FileReference object are populated by calling the <code>browse()</code> method. Unlike other FileReference properties, if you call the <code>download()</code> method, the <code>name</code> property is populated when the <code>select</code> event is dispatched.</p>
		 * 
		 * @return 
		 */
		public function get name():String {
			return _name;
		}

		/**
		 * <p> The size of the file on the local disk in bytes. </p>
		 * <p><i>Note:</i> In the initial version of ActionScript 3.0, the <code>size</code> property was defined as a uint object, which supported files with sizes up to about 4 GB. It is now implemented as a Number object to support larger files. </p>
		 * 
		 * @return 
		 */
		public function get size():Number {
			return _size;
		}

		/**
		 * <p> The file type. </p>
		 * <p>In Windows or Linux, this property is the file extension. On the Macintosh, this property is the four-character file type, which is only used in Mac OS versions prior to Mac OS X. If the FileReference object was not populated, a call to get the value of this property returns <code>null</code>.</p>
		 * <p>For Windows, Linux, and Mac OS X, the file extension — the portion of the <code>name</code> property that follows the last occurrence of the dot (.) character — identifies the file type.</p>
		 * 
		 * @return 
		 */
		public function get type():String {
			return _type;
		}

		/**
		 * <p> Displays a file-browsing dialog box that lets the user select a file to upload. The dialog box is native to the user's operating system. The user can select a file on the local computer or from other systems, for example, through a UNC path on Windows. </p>
		 * <p><b>Note:</b> The File class, available in Adobe AIR, includes methods for accessing more specific system file selection dialog boxes. These methods are <code>File.browseForDirectory()</code>, <code>File.browseForOpen()</code>, <code>File.browseForOpenMultiple()</code>, and <code>File.browseForSave()</code>.</p>
		 * <p>When you call this method and the user successfully selects a file, the properties of this FileReference object are populated with the properties of that file. Each subsequent time that the <code>FileReference.browse()</code> method is called, the FileReference object's properties are reset to the file that the user selects in the dialog box. Only one <code>browse()</code> or <code>download()</code> session can be performed at a time (because only one dialog box can be invoked at a time).</p>
		 * <p>Using the <code>typeFilter</code> parameter, you can determine which files the dialog box displays.</p>
		 * <p>In Flash Player 10 and Flash Player 9 Update 5, you can only call this method successfully in response to a user event (for example, in an event handler for a mouse click or keypress event). Otherwise, calling this method results in Flash Player throwing an Error exception.</p>
		 * <p>Note that because of new functionality added to the Flash Player, when publishing to Flash Player 10, you can have only one of the following operations active at one time: <code>FileReference.browse()</code>, <code>FileReference.upload()</code>, <code>FileReference.download()</code>, <code>FileReference.load()</code>, <code>FileReference.save()</code>. Otherwise, Flash Player throws a runtime error (code 2174). Use <code>FileReference.cancel()</code> to stop an operation in progress. This restriction applies only to Flash Player 10. Previous versions of Flash Player are unaffected by this restriction on simultaneous multiple operations.</p>
		 * <p>In Adobe AIR, the file-browsing dialog is not always displayed in front of windows that are "owned" by another window (windows that have a non-null <code>owner</code> property). To avoid window ordering issues, hide owned windows before calling this method.</p>
		 * 
		 * @param typeFilter  — An array of FileFilter instances used to filter the files that are displayed in the dialog box. If you omit this parameter, all files are displayed. For more information, see the <a href="FileFilter.html">FileFilter</a> class. 
		 * @return  — Returns  if the parameters are valid and the file-browsing dialog box opens. 
		 */
		public function browse(typeFilter:Array = null):Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Cancels any ongoing upload or download operation on this FileReference object. Calling this method does not dispatch the <code>cancel</code> event; that event is dispatched only when the user cancels the operation by dismissing the file upload or download dialog box. </p>
		 */
		public function cancel():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Opens a dialog box that lets the user download a file from a remote server. <span>Although Flash Player has no restriction on the size of files you can upload or download, the player officially supports uploads or downloads of up to 100 MB.</span> </p>
		 * <p>The <code>download()</code> method first opens an operating-system dialog box that asks the user to enter a filename and select a location on the local computer to save the file. When the user selects a location and confirms the download operation (for example, by clicking Save), the download from the remote server begins. Listeners receive events to indicate the progress, success, or failure of the download. To ascertain the status of the dialog box and the download operation after calling <code>download()</code>, your code must listen for events such as <code>cancel</code>, <code>open</code>, <code>progress</code>, and <code>complete</code>. </p>
		 * <p>The <code>FileReference.upload()</code> and <code>FileReference.download()</code> functions are nonblocking. These functions return after they are called, before the file transmission is complete. In addition, if the FileReference object goes out of scope, any upload or download that is not yet completed on that object is canceled upon leaving the scope. Be sure that your FileReference object remains in scope for as long as the upload or download is expected to continue.</p>
		 * <p>When the file is downloaded successfully, the properties of the FileReference object are populated with the properties of the local file. The <code>complete</code> event is dispatched if the download is successful.</p>
		 * <p>Only one <code>browse()</code> or <code>download()</code> session can be performed at a time (because only one dialog box can be invoked at a time).</p>
		 * <p>This method supports downloading of any file type, with either HTTP or HTTPS.</p>
		 * <p>You cannot connect to commonly reserved ports. For a complete list of blocked ports, see "Restricting Networking APIs" in the <i>ActionScript 3.0 Developer's Guide</i>.</p>
		 * <p><b>Note</b>: If your server requires user authentication, only SWF files running in a browser — that is, using the browser plug-in or ActiveX control — can provide a dialog box to prompt the user for a user name and password for authentication, and only for downloads. For uploads using the plug-in or ActiveX control, or for uploads and downloads using the stand-alone or external player, the file transfer fails.</p>
		 * <p>When you use this method , consider the <span>Flash Player</span> security model: </p>
		 * <ul>
		 *  <li>Loading operations are not allowed if the calling SWF file is in an untrusted local sandbox.</li>
		 *  <li>The default behavior is to deny access between sandboxes. A website can enable access to a resource by adding a URL policy file.</li>
		 *  <li>You can prevent a SWF file from using this method by setting the <code>allowNetworking</code> parameter of the the <code>object</code> and <code>embed</code> tags in the HTML page that contains the SWF content.</li>
		 *  <li>In Flash Player 10 and Flash Player 9 Update 5, you can only call this method successfully in response to a user event (for example, in an event handler for a mouse click or keypress event). Otherwise, calling this method results in Flash Player throwing an Error exception.</li>
		 * </ul>
		 * <p>However, <span>in Adobe AIR,</span> content in the <code>application</code> security sandbox (content installed with the AIR application) is not restricted by these security limitations.</p>
		 * <p>For more information related to security, see the Flash Player Developer Center Topic: <a href="http://www.adobe.com/go/devnet_security_en" target="external">Security</a>.</p>
		 * <p>When you download a file using this method, it is flagged as downloaded on operating systems that flag downloaded files:</p>
		 * <ul>
		 *  <li>Windows XP service pack 2 and later, and on Windows Vista</li>
		 *  <li>Mac OS 10.5 and later</li>
		 * </ul>
		 * <p>Some operating systems, such as Linux, do not flag downloaded files.</p>
		 * <p>Note that because of new functionality added to the Flash Player, when publishing to Flash Player 10, you can have only one of the following operations active at one time: <code>FileReference.browse()</code>, <code>FileReference.upload()</code>, <code>FileReference.download()</code>, <code>FileReference.load()</code>, <code>FileReference.save()</code>. Otherwise, Flash Player throws a runtime error (code 2174). Use <code>FileReference.cancel()</code> to stop an operation in progress. This restriction applies only to Flash Player 10. Previous versions of Flash Player are unaffected by this restriction on simultaneous multiple operations.</p>
		 * <p>In Adobe AIR, the download dialog is not always displayed in front of windows that are "owned" by another window (windows that have a non-null <code>owner</code> property). To avoid window ordering issues, hide owned windows before calling this method.</p>
		 * 
		 * @param request  — The URLRequest object. The <code>url</code> property of the URLRequest object should contain the URL of the file to download to the local computer. If this parameter is <code>null</code>, an exception is thrown. The <code>requestHeaders</code> property of the URLRequest object is ignored; custom HTTP request headers are not supported in uploads or downloads. To send <code>POST</code> or GET parameters to the server, set the value of <code>URLRequest.data</code> to your parameters, and set <code>URLRequest.method</code> to either <code>URLRequestMethod.POST</code> or <code>URLRequestMethod.GET</code>. <p>On some browsers, URL strings are limited in length. Lengths greater than 256 characters may fail on some browsers or servers.</p> 
		 * @param defaultFileName  — The default filename displayed in the dialog box for the file to be downloaded. This string must not contain the following characters: / \ : * ? " &lt; &gt; | % <p>If you omit this parameter, the filename of the remote URL is parsed and used as the default. </p> 
		 */
		public function download(request:URLRequest, defaultFileName:String = null):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Starts the load of a local file selected by a user. <span>Although Flash Player has no restriction on the size of files you can upload, download, load or save, it officially supports sizes of up to 100 MB. For content running in Flash Player, you must call the <code>FileReference.browse()</code> or <code>FileReferenceList.browse()</code> method before you call the <code>load()</code> method. However, content running in AIR in the application sandbox can call the <code>load()</code> method of a File object without first calling the <code>browse()</code> method. (The AIR File class extends the FileReference class.)</span> </p>
		 * <p>Listeners receive events to indicate the progress, success, or failure of the load. Although you can use the FileReferenceList object to let users select multiple files to load, you must load the files one by one. To load the files one by one, iterate through the <code>FileReferenceList.fileList</code> array of FileReference objects.</p>
		 * <p>Adobe AIR also includes the FileStream class which provides more options for reading files.</p>
		 * <p>The <code>FileReference.upload()</code>, <code>FileReference.download()</code>, <code>FileReference.load()</code> and <code>FileReference.save()</code> functions are nonblocking. These functions return after they are called, before the file transmission is complete. In addition, if the FileReference object goes out of scope, any transaction that is not yet completed on that object is canceled upon leaving the scope. Be sure that your FileReference object remains in scope for as long as the upload, download, load or save is expected to continue.</p>
		 * <p>If the file finishes loading successfully, its contents are stored as a byte array in the <code>data</code> property of the FileReference object.</p>
		 * <p>The following security considerations apply:</p>
		 * <ul>
		 *  <li>Loading operations are not allowed if the calling SWF file is in an untrusted local sandbox.</li>
		 *  <li>The default behavior is to deny access between sandboxes. A website can enable access to a resource by adding a cross-domain policy file.</li>
		 *  <li>You can prevent a file from using this method by setting the <code>allowNetworking</code> parameter of the the <code>object</code> and <code>embed</code> tags in the HTML page that contains the SWF content.</li>
		 * </ul>
		 * <p>However, these considerations do not apply to AIR content in the application sandbox.</p>
		 * <p>Note that when publishing to Flash Player 10 or AIR 1.5, you can have only one of the following operations active at one time: <code>FileReference.browse()</code>, <code>FileReference.upload()</code>, <code>FileReference.download()</code>, <code>FileReference.load()</code>, <code>FileReference.save()</code>. Otherwise, the application throws a runtime error (code 2174). Use <code>FileReference.cancel()</code> to stop an operation in progress. This restriction applies only to Flash Player 10 and AIR 1.5. Previous versions of Flash Player or AIR are unaffected by this restriction on simultaneous multiple operations.</p>
		 * <p>In Adobe AIR, the file-browsing dialog is not always displayed in front of windows that are "owned" by another window (windows that have a non-null <code>owner</code> property). To avoid window ordering issues, hide owned windows before calling this method.</p>
		 */
		public function load():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Requests permission to access filesystem. </p>
		 */
		public function requestPermission():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Opens a dialog box that lets the user save a file to the local filesystem. <span>Although Flash Player has no restriction on the size of files you can upload, download, load or save, the player officially supports sizes of up to 100 MB.</span> </p>
		 * <p>The <code>save()</code> method first opens an operating-system dialog box that asks the user to enter a filename and select a location on the local computer to save the file. When the user selects a location and confirms the save operation (for example, by clicking Save), the save process begins. Listeners receive events to indicate the progress, success, or failure of the save operation. To ascertain the status of the dialog box and the save operation after calling <code>save()</code>, your code must listen for events such as <code>cancel</code>, <code>open</code>, <code>progress</code>, and <code>complete</code>. </p>
		 * <p>Adobe AIR also includes the FileStream class which provides more options for saving files locally.</p>
		 * <p>The <code>FileReference.upload()</code>, <code>FileReference.download()</code>, <code>FileReference.load()</code> and <code>FileReference.save()</code> functions are nonblocking. These functions return after they are called, before the file transmission is complete. In addition, if the FileReference object goes out of scope, any transaction that is not yet completed on that object is canceled upon leaving the scope. Be sure that your FileReference object remains in scope for as long as the upload, download, load or save is expected to continue.</p>
		 * <p>When the file is saved successfully, the properties of the FileReference object are populated with the properties of the local file. The <code>complete</code> event is dispatched if the save is successful.</p>
		 * <p>Only one <code>browse()</code> or <code>save()</code> session can be performed at a time (because only one dialog box can be invoked at a time).</p>
		 * <p>In Flash Player, you can only call this method successfully in response to a user event (for example, in an event handler for a mouse click or keypress event). Otherwise, calling this method results in Flash Player throwing an Error exception. This limitation does not apply to AIR content in the application sandbox.</p>
		 * <p>In Adobe AIR, the save dialog is not always displayed in front of windows that are "owned" by another window (windows that have a non-null <code>owner</code> property). To avoid window ordering issues, hide owned windows before calling this method.</p>
		 * 
		 * @param data  — The data to be saved. The data can be in one of several formats, and will be treated appropriately: <ul>
		 *  <li>If the value is <code>null</code>, the application throws an ArgumentError exception.</li>
		 *  <li>If the value is a String, it is saved as a UTF-8 text file.</li>
		 *  <li>If the value is XML, it is written to a text file in XML format, with all formatting preserved.</li>
		 *  <li>If the value is a ByteArray object, it is written to a data file verbatim.</li>
		 *  <li>If the value is none of the above, the <code>save()</code> method calls the <code>toString()</code> method of the object to convert the data to a string, and it then saves the data as a text file. If that fails, the application throws an ArgumentError exception.</li>
		 * </ul> 
		 * @param defaultFileName  — The default filename displayed in the dialog box for the file to be saved. This string must not contain the following characters: / \ : * ? " &lt; &gt; | % <p> If a File object calls this method, the filename will be that of the file the File object references. (The AIR File class extends the FileReference class.)</p> 
		 */
		public function save(data:*, defaultFileName:String = null):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Starts the upload of the file to a remote server. <span>Although Flash Player has no restriction on the size of files you can upload or download, the player officially supports uploads or downloads of up to 100 MB.</span> You must call the <code>FileReference.browse()</code> or <code>FileReferenceList.browse()</code> method before you call this method. </p>
		 * <p>For the Adobe AIR File class, which extends the FileReference class, you can use the <code>upload()</code> method to upload any file. For the FileReference class (used in Flash Player), the user must first select a file.</p>
		 * <p>Listeners receive events to indicate the progress, success, or failure of the upload. Although you can use the FileReferenceList object to let users select multiple files for upload, you must upload the files one by one; to do so, iterate through the <code>FileReferenceList.fileList</code> array of FileReference objects.</p>
		 * <p>The <code>FileReference.upload()</code> and <code>FileReference.download()</code> functions are nonblocking. These functions return after they are called, before the file transmission is complete. In addition, if the FileReference object goes out of scope, any upload or download that is not yet completed on that object is canceled upon leaving the scope. Be sure that your FileReference object remains in scope for as long as the upload or download is expected to continue.</p>
		 * <p>The file is uploaded to the URL passed in the <code>url</code> parameter. The URL must be a server script configured to accept uploads. Flash Player uploads files by using the HTTP <code>POST</code> method. The server script that handles the upload should expect a <code>POST</code> request with the following elements:</p>
		 * <ul>
		 *  <li><code>Content-Type</code> of <code>multipart/form-data</code></li>
		 *  <li><code>Content-Disposition</code> with a <code>name</code> attribute set to <code>"Filedata"</code> by default and a <code>filename</code> attribute set to the name of the original file</li>
		 *  <li>The binary contents of the file</li>
		 * </ul>
		 * <p>You cannot connect to commonly reserved ports. For a complete list of blocked ports, see "Restricting Networking APIs" in the <i>ActionScript 3.0 Developer's Guide</i>.</p>
		 * <p>For a sample <code>POST</code> request, see the description of the <code>uploadDataFieldName</code> parameter. You can send <code>POST</code> or <code>GET</code> parameters to the server with the <code>upload()</code> method; see the description of the <code>request</code> parameter.</p>
		 * <p>If the <code>testUpload</code> parameter is <code>true</code>, and the file to be uploaded is bigger than approximately 10 KB, Flash Player on Windows first sends a test upload <code>POST</code> operation with zero content before uploading the actual file, to verify that the transmission is likely to succeed. Flash Player then sends a second <code>POST</code> operation that contains the actual file content. For files smaller than 10 KB, Flash Player performs a single upload <code>POST</code> with the actual file content to be uploaded. Flash Player on Macintosh does not perform test upload <code>POST</code> operations.</p>
		 * <p><b>Note</b>: If your server requires user authentication, only SWF files running in a browser — that is, using the browser plug-in or ActiveX control — can provide a dialog box to prompt the user for a username and password for authentication, and only for downloads. For uploads using the plug-in or ActiveX control, or for uploads and downloads using the stand-alone or external player, the file transfer fails.</p>
		 * <p>When you use this method , consider the <span>Flash Player</span> security model: </p>
		 * <ul>
		 *  <li>Loading operations are not allowed if the calling SWF file is in an untrusted local sandbox.</li>
		 *  <li>The default behavior is to deny access between sandboxes. A website can enable access to a resource by adding a URL policy file.</li>
		 *  <li>You can prevent a SWF file from using this method by setting the <code>allowNetworking</code> parameter of the the <code>object</code> and <code>embed</code> tags in the HTML page that contains the SWF content.</li>
		 * </ul>
		 * <p>However, in Adobe AIR, content in the <code>application</code> security sandbox (content installed with the AIR application) are not restricted by these security limitations.</p>
		 * <p>For more information related to security, see the Flash Player Developer Center Topic: <a href="http://www.adobe.com/go/devnet_security_en" target="external">Security</a>.</p>
		 * <p>Note that because of new functionality added to the Flash Player, when publishing to Flash Player 10, you can have only one of the following operations active at one time: <code>FileReference.browse()</code>, <code>FileReference.upload()</code>, <code>FileReference.download()</code>, <code>FileReference.load()</code>, <code>FileReference.save()</code>. Otherwise, Flash Player throws a runtime error (code 2174). Use <code>FileReference.cancel()</code> to stop an operation in progress. This restriction applies only to Flash Player 10. Previous versions of Flash Player are unaffected by this restriction on simultaneous multiple operations.</p>
		 * 
		 * @param request  — The URLRequest object; the <code>url</code> property of the URLRequest object should contain the URL of the server script configured to handle upload through HTTP <code>POST</code> calls. On some browsers, URL strings are limited in length. Lengths greater than 256 characters may fail on some browsers or servers. If this parameter is <code>null</code>, an exception is thrown. The <code>requestHeaders</code> property of the URLRequest object is ignored; custom HTTP request headers are not supported in uploads or downloads. <p>The URL can be HTTP or, for secure uploads, HTTPS. To use HTTPS, use an HTTPS url in the <code>url</code> parameter. If you do not specify a port number in the <code>url</code> parameter, port 80 is used for HTTP and port 443 us used for HTTPS, by default.</p> <p>To send <code>POST</code> or <code>GET</code> parameters to the server, set the <code>data</code> property of the URLRequest object to your parameters, and set the <code>method</code> property to either <code>URLRequestMethod.POST</code> or <code>URLRequestMethod.GET</code>.</p> 
		 * @param uploadDataFieldName  — The field name that precedes the file data in the upload <code>POST</code> operation. The <code>uploadDataFieldName</code> value must be non-null and a non-empty String. By default, the value of <code>uploadDataFieldName</code> is <code>"Filedata"</code>, as shown in the following sample <code>POST</code> request: <pre>
		 *     Content-Type: multipart/form-data; boundary=AaB03x
		 *     --AaB03x 
		 *     Content-Disposition: form-data; name="Filedata"; filename="example.jpg" 
		 *     Content-Type: application/octet-stream
		 *     ... contents of example.jpg ... 
		 *     --AaB03x-- 
		 *     </pre> 
		 * @param testUpload  — A setting to request a test file upload. If <code>testUpload</code> is <code>true</code>, for files larger than 10 KB, Flash Player attempts a test file upload <code>POST</code> with a Content-Length of 0. The test upload checks whether the actual file upload will be successful and that server authentication, if required, will succeed. A test upload is only available for Windows players. 
		 */
		public function upload(request:URLRequest, uploadDataFieldName:String = "Filedata", testUpload:Boolean = false):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Initiate uploading a file to a URL without any encoding. Whereas the <code>upload()</code> method encodes the file in a form-data envelope, the <code>uploadUnencoded()</code> method passes the file contents as-is in the HTTP request body. Use the uploadUnencoded() method if the data you wish to send is already encoded in a format that the receiving server can understand.You typically use the <code>uploadeUnencoded()</code> method with the <code>HTTP/WebDAV PUT</code> method. </p>
		 * 
		 * @param request  — The URLRequest object; the <code>url</code> property of the URLRequest object should contain the URL of the server script configured to handle upload through HTTP <code>POST</code> calls. On some browsers, URL strings are limited in length. Lengths greater than 256 characters may fail on some browsers or servers. If this parameter is <code>null</code>, an exception is thrown. <p>The URL can be HTTP or, for secure uploads, HTTPS. To use HTTPS, use an HTTPS url in the <code>url</code> parameter. If you do not specify a port number in the <code>url</code> parameter, port 80 is used for HTTP and port 443 us used for HTTPS, by default.</p> <p>To send <code>POST</code> or <code>GET</code> parameters to the server, set the <code>data</code> property of the URLRequest object to your parameters, and set the <code>method</code> property to either <code>URLRequestMethod.POST</code> or <code>URLRequestMethod.GET</code>.</p> 
		 */
		public function uploadUnencoded(request:URLRequest):void {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Determine whether the application has been granted the permission to access filesystem. </p>
		 * 
		 * @return 
		 */
		public static function get permissionStatus():String {
			return _permissionStatus;
		}
	}
}
