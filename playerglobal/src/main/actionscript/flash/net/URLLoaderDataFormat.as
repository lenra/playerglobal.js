package flash.net {
	/**
	 *  The URLLoaderDataFormat class provides values that specify how downloaded data is received. <p><a href="#includeExamplesSummary">View the examples</a></p><br><hr>
	 */
	public class URLLoaderDataFormat {
		/**
		 * <p> Specifies that downloaded data is received as raw binary data. </p>
		 */
		public static const BINARY:String = "binary";
		/**
		 * <p> Specifies that downloaded data is received as text. </p>
		 */
		public static const TEXT:String = "text";
		/**
		 * <p> Specifies that downloaded data is received as URL-encoded variables. </p>
		 */
		public static const VARIABLES:String = "variables";
	}
}
