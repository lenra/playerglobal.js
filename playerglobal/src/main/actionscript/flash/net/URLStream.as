package flash.net {
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.utils.ByteArray;
	import flash.utils.IDataInput;

	[Event(name="complete", type="flash.events.Event")]
	[Event(name="httpResponseStatus", type="flash.events.HTTPStatusEvent")]
	[Event(name="httpStatus", type="flash.events.HTTPStatusEvent")]
	[Event(name="ioError", type="flash.events.IOErrorEvent")]
	[Event(name="open", type="flash.events.Event")]
	[Event(name="progress", type="flash.events.ProgressEvent")]
	[Event(name="securityError", type="flash.events.SecurityErrorEvent")]
	/**
	 *  The URLStream class provides low-level access to downloading URLs. Data is made available to application code immediately as it is downloaded, instead of waiting until the entire file is complete as with URLLoader. The URLStream class also lets you close a stream before it finishes downloading. The contents of the downloaded file are made available as raw binary data. <p>The read operations in URLStream are nonblocking. This means that you must use the <code>bytesAvailable</code> property to determine whether sufficient data is available before reading it. An <code>EOFError</code> exception is thrown if insufficient data is available.</p> <p>All binary data is encoded by default in big-endian format, with the most significant byte first.</p> <p>The security rules that apply to URL downloading with the URLStream class are identical to the rules applied to URLLoader objects. Policy files may be downloaded as needed. Local file security rules are enforced, and security warnings are raised as needed.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7cfd.html" target="_blank">Loading external data</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118666ade46-7cb0.html" target="_blank">Using the URLStream class </a>
	 *  <br>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="URLLoader.html" target="">URLLoader</a>
	 *  <br>
	 *  <a href="URLRequest.html" target="">URLRequest</a>
	 * </div><br><hr>
	 */
	public class URLStream extends EventDispatcher implements IDataInput {
		private var _endian:String;
		private var _objectEncoding:uint;

		private var _bytesAvailable:uint;
		private var _connected:Boolean;

		/**
		 * <p> Returns the number of bytes of data available for reading in the input buffer. Your code must call the <code>bytesAvailable</code> property to ensure that sufficient data is available before you try to read it with one of the <code>read</code> methods. </p>
		 * 
		 * @return 
		 */
		public function get bytesAvailable():uint {
			return _bytesAvailable;
		}

		/**
		 * <p> Indicates whether this URLStream object is currently connected. A call to this property returns a value of <code>true</code> if the URLStream object is connected, or <code>false</code> otherwise. </p>
		 * 
		 * @return 
		 */
		public function get connected():Boolean {
			return _connected;
		}

		/**
		 * <p> Indicates the byte order for the data. Possible values are <code>Endian.BIG_ENDIAN</code> or <code>Endian.LITTLE_ENDIAN</code>. </p>
		 * <p> The default value is <code>Endian.BIG_ENDIAN.</code></p>
		 * 
		 * @return 
		 */
		public function get endian():String {
			return _endian;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set endian(value:String):void {
			_endian = value;
		}

		/**
		 * <p> Controls the version of Action Message Format (AMF) used when writing or reading an object. </p>
		 * 
		 * @return 
		 */
		public function get objectEncoding():uint {
			return _objectEncoding;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set objectEncoding(value:uint):void {
			_objectEncoding = value;
		}

		/**
		 * <p> Immediately closes the stream and cancels the download operation. No data can be read from the stream after the <code>close()</code> method is called. </p>
		 */
		public function close():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Begins downloading the URL specified in the <code>request</code> parameter. </p>
		 * <p><b>Note</b>: If a file being loaded contains non-ASCII characters (as found in many non-English languages), it is recommended that you save the file with UTF-8 or UTF-16 encoding, as opposed to a non-Unicode format like ASCII.</p>
		 * <p>If the loading operation fails immediately, an IOError or SecurityError (including the local file security error) exception is thrown describing the failure. Otherwise, an <code>open</code> event is dispatched if the URL download starts downloading successfully, or an error event is dispatched if an error occurs.</p>
		 * <p>By default, the calling SWF file and the URL you load must be in exactly the same domain. For example, a SWF file at www.adobe.com can load data only from sources that are also at www.adobe.com. To load data from a different domain, place a URL policy file on the server hosting the data.</p>
		 * <p>In Flash Player, you cannot connect to commonly reserved ports. For a complete list of blocked ports, see "Restricting Networking APIs" in the <i>ActionScript 3.0 Developer's Guide</i>.</p>
		 * <p>In Flash Player, you can prevent a SWF file from using this method by setting the <code>allowNetworking</code> parameter of the the <code>object</code> and <code>embed</code> tags in the HTML page that contains the SWF content.</p>
		 * <p> In Flash Player 10 and later, and in AIR 1.5 and later, if you use a multipart Content-Type (for example "multipart/form-data") that contains an upload (indicated by a "filename" parameter in a "content-disposition" header within the POST body), the POST operation is subject to the security rules applied to uploads:</p>
		 * <ul>
		 *  <li>The POST operation must be performed in response to a user-initiated action, such as a mouse click or key press.</li>
		 *  <li>If the POST operation is cross-domain (the POST target is not on the same server as the SWF file that is sending the POST request), the target server must provide a URL policy file that permits cross-domain access.</li>
		 * </ul>
		 * <p>Also, for any multipart Content-Type, the syntax must be valid (according to the RFC2046 standards). If the syntax appears to be invalid, the POST operation is subject to the security rules applied to uploads.</p>
		 * <p>These rules also apply to AIR content in non-application sandboxes. However, in Adobe AIR, content in the application sandbox (content installed with the AIR application) are not restricted by these security limitations.</p>
		 * <p>For more information related to security, see The Flash Player Developer Center Topic: <a href="http://www.adobe.com/go/devnet_security_en" target="external">Security</a>.</p>
		 * <p>In AIR, a URLRequest object can register for the <code>httpResponse</code> status event. Unlike the <code>httpStatus</code> event, the <code>httpResponseStatus</code> event is delivered before any response data. Also, the <code>httpResponseStatus</code> event includes values for the <code>responseHeaders</code> and <code>responseURL</code> properties (which are undefined for an <code>httpStatus</code> event. Note that the <code>httpResponseStatus</code> event (if any) will be sent before (and in addition to) any <code>complete</code> or <code>error</code> event. </p>
		 * <p>If there <i>is</i> an <code>httpResponseStatus</code> event listener, the body of the response message is <i>always</i> sent; and HTTP status code responses always results in a <code>complete</code> event. This is true in spite of whether the HTTP response status code indicates a success or an error.</p>
		 * <p><span>In AIR, if</span> there is <i>no</i> <code>httpResponseStatus</code> event listener, the behavior differs based on the <span>SWF</span> version:</p>
		 * <ul>
		 *  <li><span>For SWF 9 content</span>, the body of the HTTP response message is sent <i>only if</i> the HTTP response status code indicates success. Otherwise (if there is an error), no body is sent and the URLRequest object dispatches an IOError event.</li>
		 *  <li><span>For SWF 10 content</span>, the body of the HTTP response message is <i>always</i> sent. If there is an error, the URLRequest object dispatches an IOError event.</li>
		 * </ul>
		 * 
		 * @param request  — A URLRequest object specifying the URL to download. If the value of this parameter or the <code>URLRequest.url</code> property of the URLRequest object passed are <code>null</code>, the application throws a null pointer error. 
		 */
		public function load(request:URLRequest):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Reads a Boolean value from the stream. A single byte is read, and <code>true</code> is returned if the byte is nonzero, <code>false</code> otherwise. </p>
		 * 
		 * @return  —  is returned if the byte is nonzero,  otherwise. 
		 */
		public function readBoolean():Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Reads a signed byte from the stream. </p>
		 * <p>The returned value is in the range -128...127.</p>
		 * 
		 * @return  — Value in the range -128...127. 
		 */
		public function readByte():int {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Reads <code>length</code> bytes of data from the stream. The bytes are read into the ByteArray object specified by <code>bytes</code>, starting <code>offset</code> bytes into the ByteArray object. </p>
		 * 
		 * @param bytes  — The ByteArray object to read data into. 
		 * @param offset  — The offset into <code>bytes</code> at which data read should begin. Defaults to 0. 
		 * @param length  — The number of bytes to read. The default value of 0 will cause all available data to be read. 
		 */
		public function readBytes(bytes:ByteArray, offset:uint = 0, length:uint = 0):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Reads an IEEE 754 double-precision floating-point number from the stream. </p>
		 * 
		 * @return  — An IEEE 754 double-precision floating-point number from the stream. 
		 */
		public function readDouble():Number {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Reads an IEEE 754 single-precision floating-point number from the stream. </p>
		 * 
		 * @return  — An IEEE 754 single-precision floating-point number from the stream. 
		 */
		public function readFloat():Number {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Reads a signed 32-bit integer from the stream. </p>
		 * <p>The returned value is in the range -2147483648...2147483647.</p>
		 * 
		 * @return  — Value in the range -2147483648...2147483647. 
		 */
		public function readInt():int {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Reads a multibyte string of specified length from the byte stream using the specified character set. </p>
		 * 
		 * @param length  — The number of bytes from the byte stream to read. 
		 * @param charSet  — The string denoting the character set to use to interpret the bytes. Possible character set strings include <code>"shift_jis"</code>, <code>"CN-GB"</code>, <code>"iso-8859-1"</code>, and others. For a complete list, see <a href="../../charset-codes.html">Supported Character Sets</a>. <p><b>Note:</b> If the value for the <code>charSet</code> parameter is not recognized by the current system, the application uses the system's default code page as the character set. For example, a value for the <code>charSet</code> parameter, as in <code>myTest.readMultiByte(22, "iso-8859-01")</code> that uses <code>01</code> instead of <code>1</code> might work on your development machine, but not on another machine. On the other machine, the application will use the system's default code page.</p> 
		 * @return  — UTF-8 encoded string. 
		 */
		public function readMultiByte(length:uint, charSet:String):String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Reads an object from the socket, encoded in Action Message Format (AMF). </p>
		 * 
		 * @return  — The deserialized object. 
		 */
		public function readObject():* {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Reads a signed 16-bit integer from the stream. </p>
		 * <p>The returned value is in the range -32768...32767.</p>
		 * 
		 * @return  — Value in the range -32768...32767. 
		 */
		public function readShort():int {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Reads a UTF-8 string from the stream. The string is assumed to be prefixed with an unsigned short indicating the length in bytes. </p>
		 * 
		 * @return  — A UTF-8 string. 
		 */
		public function readUTF():String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Reads a sequence of <code>length</code> UTF-8 bytes from the stream, and returns a string. </p>
		 * 
		 * @param length  — A sequence of UTF-8 bytes. 
		 * @return  — A UTF-8 string produced by the byte representation of characters of specified length. 
		 */
		public function readUTFBytes(length:uint):String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Reads an unsigned byte from the stream. </p>
		 * <p>The returned value is in the range 0...255. </p>
		 * 
		 * @return  — Value in the range 0...255. 
		 */
		public function readUnsignedByte():uint {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Reads an unsigned 32-bit integer from the stream. </p>
		 * <p>The returned value is in the range 0...4294967295. </p>
		 * 
		 * @return  — Value in the range 0...4294967295. 
		 */
		public function readUnsignedInt():uint {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Reads an unsigned 16-bit integer from the stream. </p>
		 * <p>The returned value is in the range 0...65535. </p>
		 * 
		 * @return  — Value in the range 0...65535. 
		 */
		public function readUnsignedShort():uint {
			throw new Error("Not implemented");
		}
	}
}
