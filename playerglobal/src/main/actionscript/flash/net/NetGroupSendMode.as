package flash.net {
	/**
	 *  The NetGroupSendMode class is an enumeration of constant values used for the <code>sendMode</code> parameter of the <code>NetGroup.sendToNeighbor()</code> method. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="NetGroup.html#sendToNeighbor()" target="">flash.net.NetGroup.sendToNeighbor()</a>
	 * </div><br><hr>
	 */
	public class NetGroupSendMode {
		/**
		 * <p> Specifies the neighbor with the nearest group address in the decreasing direction. </p>
		 */
		public static const NEXT_DECREASING:String = "nextDecreasing";
		/**
		 * <p> Specifies the neighbor with the nearest group address in the increasing direction. </p>
		 */
		public static const NEXT_INCREASING:String = "nextIncreasing";
	}
}
