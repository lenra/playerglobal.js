package flash.net {
	/**
	 *  The NetGroupReplicationStrategy class is an enumeration of constant values used in setting the <code>replicationStrategy</code> property of the <code>NetGroup</code> class. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="NetGroup.html#addWantObjects()" target="">flash.net.NetGroup.addWantObjects()</a>
	 *  <br>
	 *  <a href="NetGroup.html#replicationStrategy" target="">flash.net.NetGroup.replicationStrategy</a>
	 * </div><br><hr>
	 */
	public class NetGroupReplicationStrategy {
		/**
		 * <p> Specifies that when fetching objects from a neighbor to satisfy a want, the objects with the lowest index numbers are requested first. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="NetGroup.html#addWantObjects()" target="">flash.net.NetGroup.addWantObjects()</a>
		 * </div>
		 */
		public static const LOWEST_FIRST:String = "lowestFirst";
		/**
		 * <p> Specifies that when fetching objects from a neighbor to satisfy a want, the objects with the fewest replicas among all the neighbors are requested first. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="NetGroup.html#addWantObjects()" target="">NetGroup.addWantObjects()</a>
		 * </div>
		 */
		public static const RAREST_FIRST:String = "rarestFirst";
	}
}
