package flash.net {
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.security.X509Certificate;
	import flash.utils.ByteArray;

	[Event(name="close", type="flash.events.Event")]
	[Event(name="connect", type="flash.events.Event")]
	[Event(name="ioError", type="flash.events.IOErrorEvent")]
	[Event(name="securityError", type="flash.events.SecurityErrorEvent")]
	[Event(name="socketData", type="flash.events.ProgressEvent")]
	/**
	 *  The SecureSocket class enables code to make socket connections using the Secure Sockets Layer (SSL) and Transport Layer Security (TLS) protocols. <p> <i>AIR profile support:</i> This feature is supported on all desktop operating systems, but is not supported on all AIR for TV devices. On mobile devices, it is supported on Android and also supported on iOS starting from AIR 20. You can test for support at run time using the <code>SecureSocket.isSupported</code> property. See <a href="http://help.adobe.com/en_US/air/build/WS144092a96ffef7cc16ddeea2126bb46b82f-8000.html"> AIR Profile Support</a> for more information regarding API support across multiple profiles.</p> <p>The SSL/TLS protocols provide a mechanism to handle both aspects of a secure socket connection: </p> <ol> 
	 *  <li>Encryption of data communication over the socket</li> 
	 *  <li>Authentication of the host's identity via its certificate</li> 
	 * </ol> <p>The supported encryption protocols are SSL 3.1 and higher, and TLS 1.0 and higher. (TLS is the successor protocol for SSL. TLS 1.0 equals SSL 3.1, TLS 1.1 equals SSL 3.2, and so on.) SSL versions 3.0 or lower are not supported.</p> <p>Validation of the server certificate is performed using the trust store and certificate validation support of the client platform. In addition you can add your own certificates programmatically with the <code>addBinaryChainBuildingCertificate()</code> method.This API isn't supported on iOS currently. Using this API on iOS would throw an exception - "ArgumentError: Error #2004"</p> <p>The SecureSocket class only connects to servers with valid, trusted certificates. You cannot choose to connect to a server in spite of a problem with its certificate. For example, there is no way to connect to a server with an expired certificate. The same is true for a certificate that doesn't chain to a trusted anchor certificate. The connection will not be made, even though the certificate would be valid otherwise.</p> <p>The SecureSocket class is useful for performing encrypted communication to a trusted server. In other respects, a SecureSocket object behaves like a regular Socket object.</p> <p>To use the SecureSocket class, create a SecureSocket object (<code>new SecureSocket()</code>). Next, set up your listeners, and then run <code>SecureSocket.connect(host, port)</code>. When you successfully connect to the server, the socket dispatches a <code>connect</code> event. A successful connection is one in which the server's security protocols are supported and its certificate is valid and trusted. If the certificate cannot be validated, the Socket dispatches an <code>IOError</code> event.</p> <p> <b>Important:</b> The Online Certificate Status Protocol (OCSP) is not supported by all operating systems. Users can also disable OCSP checking on individual computers. If OCSP is not supported or is disabled and a certificate does not contain the information necessary to check revocation using a Certificate Revocation List (CRL), then certificate revocation is not checked. The certificate is accepted if otherwise valid. This scenario could allow a server to use a revoked certificate.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7cfb.html" target="_blank">Binary client sockets</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Socket.html" target="">Socket class</a>
	 * </div><br><hr>
	 */
	public class SecureSocket extends Socket {
		private static var _isSupported:Boolean;

		private var _serverCertificate:X509Certificate;
		private var _serverCertificateStatus:String;

		/**
		 * <p> Creates a new SecureSocket object. </p>
		 * <p>Check <code>SecureSocket.isSupported</code> before attempting to create a SecureSocket instance. If SSL 3.0 or TLS 1.0 sockets are not supported, the runtime will throw an IllegalOperationError.</p>
		 */
		public function SecureSocket(host:String = null, port:int = 0) {
			super(host, port);
			throw new Error("Not implemented");
		}

		/**
		 * <p> Holds the X.509 certificate obtained from the server after a secure SSL/TLS connection is established. If a secure connection is not established, this property is set to <code>null</code>. Currently it is not supported on iOS, and hence it is set to <code>null</code> in case of iOS. </p>
		 * <p>For more information on X.509 certificates, see <a href="http://tools.ietf.org/rfc/rfc2459" target="external">RFC2459</a>.</p>
		 * 
		 * @return 
		 */
		public function get serverCertificate():X509Certificate {
			return _serverCertificate;
		}

		/**
		 * <p> Returns the status of the server's certificate. </p>
		 * <p>The status is <code>CertificateStatus.UNKNOWN</code> until the socket attempts to connect to a server. After validation, the status is one of the strings enumerated by the CertificateStatus class. The connection only succeeds when the certificate is valid and trusted. Thus, after a <code>connect</code> event, the value of <code>serverCertificateStatus</code> is always <code>trusted</code>.</p>
		 * <p><b>Note:</b> Once the certificate has been validated or rejected, the status value is not updated until the next call to the <code>connect()</code> method. Calling <code>close()</code> does not reset the status value to "unknown".</p>
		 * 
		 * @return 
		 */
		public function get serverCertificateStatus():String {
			return _serverCertificateStatus;
		}

		/**
		 * <p> Adds an X.509 certificate to the local certificate chain that your system uses for validating the server certificate. The certificate is temporary, and lasts for the duration of the session. </p>
		 * <p>Server certificate validation relies on your system's trust store for certificate chain building and validation. Use this method to programmatically add additional certification chains and trusted anchors.</p>
		 * <p> On Mac OS, the System keychain is the default keychain used during the SSL/TLS handshake process. Any intermediate certificates in that keychain are included when building the certification chain. </p>
		 * <p> The certificate you add with this API must be a DER-encoded X.509 certificate. If the <code>trusted</code> parameter is true, the certificate you add with this API is considered a trusted anchor. </p>
		 * <p>For more information on X.509 certificates, see <a href="http://tools.ietf.org/rfc/rfc2459" target="external">RFC2459</a>.</p>
		 * 
		 * @param certificate  — A ByteArray object containing a DER-encoded X.509 digital certificate. 
		 * @param trusted  — Set to true to designate this certificate as a trust anchor. 
		 */
		public function addBinaryChainBuildingCertificate(certificate:ByteArray, trusted:Boolean):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Connects the socket to the specified host and port using SSL or TLS. </p>
		 * <p>When you call the <code>SecureSocket.connect()</code> method, the socket attempts SSL/TLS handshaking with the server. If the handshake succeeds, the socket attempts to validate the server certificate. If the certificate is valid and trusted, then the secure socket connection is established, and the socket dispatches a <code>connect</code> event. If the handshake fails or the certificate cannot be validated, the socket dispatches an <code>IOError</code> event. You can check the certificate validation result by reading the <code>serverCertificateStatus</code> property after the <code>IOError</code> event is dispatched. (When a <code>connect</code> event is dispatched, the certificate status is always <code>trusted</code>.)</p>
		 * <p>If the socket was already connected, the existing connection is closed first.</p>
		 * 
		 * @param host  — The name or IP address of the host to connect to. 
		 * @param port  — The port number to connect to. 
		 */
		override public function connect(host:String, port:int):void {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Indicates whether secure sockets are supported on the current system. </p>
		 * <p>Secure sockets are not supported on all platforms. Check this property before attempting to create a SecureSocket instance.</p>
		 * 
		 * @return 
		 */
		public static function get isSupported():Boolean {
			return _isSupported;
		}
	}
}
