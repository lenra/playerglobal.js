package flash.net {
	/**
	 *  The NetGroupSendResult class is an enumeration of constant values used for the return value of the Directed Routing methods associated with a NetGroup instance. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="NetGroup.html#sendToNearest()" target="">flash.net.NetGroup.sendToNearest()</a>
	 *  <br>
	 *  <a href="NetGroup.html#sendToNeighbor()" target="">flash.net.NetGroup.sendToNeighbor()</a>
	 *  <br>
	 *  <a href="NetGroup.html#sendToAllNeighbors()" target="">flash.net.NetGroup.sendToAllNeighbors()</a>
	 * </div><br><hr>
	 */
	public class NetGroupSendResult {
		/**
		 * <p> Indicates an error occurred (such as no permission) when using a Directed Routing method. </p>
		 */
		public static const ERROR:String = "error";
		/**
		 * <p> Indicates no neighbor could be found to route the message toward its requested destination. </p>
		 */
		public static const NO_ROUTE:String = "no route";
		/**
		 * <p> Indicates that a route was found for the message and it was forwarded toward its destination. </p>
		 */
		public static const SENT:String = "sent";
	}
}
