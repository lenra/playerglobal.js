package flash.net {
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	//import flash.events.OutputProgressEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.utils.ByteArray;
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;

	[Event(name="close", type="flash.events.Event")]
	[Event(name="connect", type="flash.events.Event")]
	[Event(name="ioError", type="flash.events.IOErrorEvent")]
	//[Event(name="outputProgress", type="flash.events.OutputProgressEvent")]
	[Event(name="securityError", type="flash.events.SecurityErrorEvent")]
	[Event(name="socketData", type="flash.events.ProgressEvent")]
	/**
	 *  The Socket class enables code to establish Transport Control Protocol (TCP) socket connections for sending and receiving binary data. <p>The Socket class is useful for working with servers that use binary protocols.</p> <p>To use the methods of the Socket class, first use the constructor, <code>new Socket</code>, to create a Socket object.</p> <p>A socket transmits and receives data asynchronously. </p> <p>On some operating systems, flush() is called automatically between execution frames, but on other operating systems, such as Windows, the data is never sent unless you call <code>flush()</code> explicitly. To ensure your application behaves reliably across all operating systems, it is a good practice to call the <code>flush()</code> method after writing each message (or related group of data) to the socket.</p> <p>In Adobe AIR, Socket objects are also created when a listening ServerSocket receives a connection from an external process. The Socket representing the connection is dispatched in a ServerSocketConnectEvent. Your application is responsible for maintaining a reference to this Socket object. If you don't, the Socket object is eligible for garbage collection and may be destroyed by the runtime without warning.</p> <p>SWF content running in the local-with-filesystem security sandbox cannot use sockets.</p> <p> <i>Socket policy files</i> on the target host specify the hosts from which SWF files can make socket connections, and the ports to which those connections can be made. The security requirements with regard to socket policy files have become more stringent in the last several releases of Flash Player. In all versions of Flash Player, Adobe recommends the use of a socket policy file; in some circumstances, a socket policy file is required. Therefore, if you are using Socket objects, make sure that the target host provides a socket policy file if necessary. </p> <p>The following list summarizes the requirements for socket policy files in different versions of Flash Player:</p> <ul> 
	 *  <li> In Flash Player 9.0.124.0 and later, a socket policy file is required for any socket connection. That is, a socket policy file on the target host is required no matter what port you are connecting to, and is required even if you are connecting to a port on the same host that is serving the SWF file. </li> 
	 *  <li> In Flash Player versions 9.0.115.0 and earlier, if you want to connect to a port number below 1024, or if you want to connect to a host other than the one serving the SWF file, a socket policy file on the target host is required. </li> 
	 *  <li> In Flash Player 9.0.115.0, even if a socket policy file isn't required, a warning is displayed when using the Flash Debug Player if the target host doesn't serve a socket policy file. </li> 
	 *  <li>In AIR, a socket policy file is not required for content running in the application security sandbox. Socket policy files are required for any socket connection established by content running outside the AIR application security sandbox.</li> 
	 * </ul> <p>For more information related to security, see the Flash Player Developer Center Topic: <a href="http://www.adobe.com/go/devnet_security_en" target="external">Security</a> </p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7cfb.html" target="_blank">Binary client sockets</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7c63.html" target="_blank">Connecting to sockets</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="ServerSocket.html" target="">ServerSocket</a>
	 *  <br>
	 *  <a href="DatagramSocket.html" target="">DatagramSocket</a>
	 * </div><br><hr>
	 */
	public class Socket extends EventDispatcher implements IDataInput, IDataOutput {
		private var _timeout:uint;

		private var _localAddress:String;
		private var _localPort:int;
		private var _remoteAddress:String;
		private var _remotePort:int;

		private var wSocket:WebSocket;

		private const inBuffer:ByteArray = new ByteArray();
		private const outBuffer:ByteArray = new ByteArray();

		/**
		 * <p> Creates a new Socket object. If no parameters are specified, an initially disconnected socket is created. If parameters are specified, a connection is attempted to the specified host and port. </p>
		 * <p><b>Note:</b> It is strongly advised to use the constructor form <b>without parameters</b>, then add any event listeners, then call the <code>connect</code> method with <code>host</code> and <code>port</code> parameters. This sequence guarantees that all event listeners will work properly.</p>
		 * 
		 * @param host  — A fully qualified DNS domain name or an IP address. IPv4 addresses are specified in dot-decimal notation, such as <i>192.0.2.0</i>. In Flash Player 9.0.115.0 and AIR 1.0 and later, you can specify IPv6 addresses using hexadecimal-colon notation, such as <i>2001:db8:ccc3:ffff:0:444d:555e:666f</i>. You can also specify <code>null</code> to connect to the host server on which the SWF file resides. If the SWF file issuing this call is running in a web browser, <code>host</code> must be in the domain from which the SWF file originated. 
		 * @param port  — The TCP port number on the target host used to establish a connection. In Flash Player 9.0.124.0 and later, the target host must serve a socket policy file specifying that socket connections are permitted from the host serving the SWF file to the specified port. In earlier versions of Flash Player, a socket policy file is required only if you want to connect to a port number below 1024, or if you want to connect to a host other than the one serving the SWF file. 
		 */
		public function Socket(host:String = null, port:int = 0) {
			super();
			if (host!=null && port!=0)
				connect(host, port);
		}

		/**
		 * <p> The number of bytes of data available for reading in the input buffer. </p>
		 * <p>Your code must access <code>bytesAvailable</code> to ensure that sufficient data is available before trying to read it with one of the <code>read</code> methods.</p>
		 * 
		 * @return 
		 */
		public function get bytesAvailable():uint {
			return inBuffer.bytesAvailable;
		}

		/**
		 * <p> Indicates the number of bytes remaining in the write buffer. </p>
		 * <p> Use this property in combination with with the OutputProgressEvent. An OutputProgressEvent is thrown whenever data is written from the write buffer to the network. In the event handler, you can check <code>bytesPending</code> to see how much data is still left in the buffer waiting to be written. When <code>bytesPending</code> returns 0, it means that all the data has been transferred from the write buffer to the network, and it is safe to do things like remove event handlers, null out socket references,start the next upload in a queue, etc. </p>
		 * 
		 * @return 
		 */
		public function get bytesPending():uint {
			return outBuffer.length;
		}

		/**
		 * <p> Indicates whether this Socket object is currently connected. A call to this property returns a value of <code>true</code> if the socket is currently connected, or <code>false</code> otherwise. </p>
		 * 
		 * @return 
		 */
		public function get connected():Boolean {
			return wSocket;
		}

		/**
		 * <p> Indicates the byte order for the data. Possible values are constants from the flash.utils.Endian class, <code>Endian.BIG_ENDIAN</code> or <code>Endian.LITTLE_ENDIAN</code>. </p>
		 * <p> The default value is <code>Endian.BIG_ENDIAN.</code></p>
		 * 
		 * @return 
		 */
		public function get endian():String {
			return inBuffer.endian;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set endian(value:String):void {
			inBuffer.endian = outBuffer.endian = value;
		}

		/**
		 * <p> The IP address this socket is bound to on the local machine. </p>
		 * 
		 * @return 
		 */
		public function get localAddress():String {
			return _localAddress;
		}

		/**
		 * <p> The port this socket is bound to on the local machine. </p>
		 * 
		 * @return 
		 */
		public function get localPort():int {
			return _localPort;
		}

		/**
		 * <p> Controls the version of AMF used when writing or reading an object. </p>
		 * 
		 * @return 
		 */
		public function get objectEncoding():uint {
			return inBuffer.objectEncoding;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set objectEncoding(value:uint):void {
			inBuffer.objectEncoding = outBuffer.objectEncoding = value;
		}

		/**
		 * <p> The IP address of the remote machine to which this socket is connected. </p>
		 * <p>You can use this property to determine the IP address of a client socket dispatched in a ServerSocketConnectEvent by a ServerSocket object. Use the DNSResolver class to convert an IP address to a domain name, if desired.</p>
		 * 
		 * @return 
		 */
		public function get remoteAddress():String {
			return _remoteAddress;
		}

		/**
		 * <p> The port on the remote machine to which this socket is connected. </p>
		 * <p>You can use this property to determine the port number of a client socket dispatched in a ServerSocketConnectEvent by a ServerSocket object.</p>
		 * 
		 * @return 
		 */
		public function get remotePort():int {
			return _remotePort;
		}

		/**
		 * <p> Indicates the number of milliseconds to wait for a connection. </p>
		 * <p>If the connection doesn't succeed within the specified time, the connection fails. The default value is 20,000 (twenty seconds).</p>
		 * 
		 * @return 
		 */
		public function get timeout():uint {
			return _timeout;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set timeout(value:uint):void {
			_timeout = value;
		}

		/**
		 * <p> Closes the socket. You cannot read or write any data after the <code>close()</code> method has been called. </p>
		 * <p>The <code>close</code> event is dispatched only when the server closes the connection; it is not dispatched when you call the <code>close()</code> method.</p>
		 * <p>You can reuse the Socket object by calling the <code>connect()</code> method on it again.</p>
		 */
		public function close():void {
			if (!wSocket)
				throw new Error("The socket is not connected");
			// close the WebSocket
			wSocket.close();
			removeListeners();
			wSocket = null;
		}

		/**
		 * <p> Connects the socket to the specified host and port. </p>
		 * <p>If the connection fails immediately, either an event is dispatched or an exception is thrown: an error event is dispatched if a host was specified, and an exception is thrown if no host was specified. Otherwise, the status of the connection is reported by an event. If the socket is already connected, the existing connection is closed first.</p>
		 * 
		 * @param host  — The name or IP address of the host to connect to. If no host is specified, the host that is contacted is the host where the calling file resides. If you do not specify a host, use an event listener to determine whether the connection was successful. 
		 * @param port  — The port number to connect to. 
		 */
		public function connect(host:String, port:int):void {
			if (wSocket)
				throw new Error("The socket is already connected");
			// connect the WebSocket
			wSocket = new WebSocket(buildSocketURL(host, port));
			wSocket.binaryType = 'arraybuffer';
			wSocket.addEventListener("open", onOpen);
			wSocket.addEventListener("message", onMessage);
			wSocket.addEventListener("close", onClose);
		}

		/**
		 * <p> Flushes any accumulated data in the socket's output buffer. </p>
		 * <p>On some operating systems, flush() is called automatically between execution frames, but on other operating systems, such as Windows, the data is never sent unless you call <code>flush()</code> explicitly. To ensure your application behaves reliably across all operating systems, it is a good practice to call the <code>flush()</code> method after writing each message (or related group of data) to the socket.</p>
		 */
		public function flush():void {
			if (!connected)
				throw new Error("The socket is not connected");
			var d:ArrayBuffer = outBuffer.toArrayBuffer();
			outBuffer.clear();
			// send throught the WebSocket
			wSocket.send(d);
		}

		/**
		 * <p> Reads a Boolean value from the socket. After reading a single byte, the method returns <code>true</code> if the byte is nonzero, and <code>false</code> otherwise. </p>
		 * 
		 * @return  — A value of  if the byte read is nonzero, otherwise . 
		 */
		public function readBoolean():Boolean {
			return inBuffer.readBoolean();
		}

		/**
		 * <p> Reads a signed byte from the socket. </p>
		 * 
		 * @return  — A value from -128 to 127. 
		 */
		public function readByte():int {
			return inBuffer.readByte();
		}

		/**
		 * <p> Reads the number of data bytes specified by the length parameter from the socket. The bytes are read into the specified byte array, starting at the position indicated by <code>offset</code>. </p>
		 * 
		 * @param bytes  — The ByteArray object to read data into. 
		 * @param offset  — The offset at which data reading should begin in the byte array. 
		 * @param length  — The number of bytes to read. The default value of 0 causes all available data to be read. 
		 */
		public function readBytes(bytes:ByteArray, offset:uint = 0, length:uint = 0):void {
			inBuffer.readBytes(bytes, offset, length);
		}

		/**
		 * <p> Reads an IEEE 754 double-precision floating-point number from the socket. </p>
		 * 
		 * @return  — An IEEE 754 double-precision floating-point number. 
		 */
		public function readDouble():Number {
			return inBuffer.readDouble();
		}

		/**
		 * <p> Reads an IEEE 754 single-precision floating-point number from the socket. </p>
		 * 
		 * @return  — An IEEE 754 single-precision floating-point number. 
		 */
		public function readFloat():Number {
			return inBuffer.readFloat();
		}

		/**
		 * <p> Reads a signed 32-bit integer from the socket. </p>
		 * 
		 * @return  — A value from -2147483648 to 2147483647. 
		 */
		public function readInt():int {
			return inBuffer.readInt();
		}

		/**
		 * <p> Reads a multibyte string from the byte stream, using the specified character set. </p>
		 * 
		 * @param length  — The number of bytes from the byte stream to read. 
		 * @param charSet  — The string denoting the character set to use to interpret the bytes. Possible character set strings include <code>"shift_jis"</code>, <code>"CN-GB"</code>, and <code>"iso-8859-1"</code>. For a complete list, see <a href="../../charset-codes.html">Supported Character Sets</a>. <p><b>Note:</b> If the value for the <code>charSet</code> parameter is not recognized by the current system, then the application uses the system's default code page as the character set. For example, a value for the <code>charSet</code> parameter, as in <code>myTest.readMultiByte(22, "iso-8859-01")</code> that uses <code>01</code> instead of <code>1</code> might work on your development machine, but not on another machine. On the other machine, the application will use the system's default code page.</p> 
		 * @return  — A UTF-8 encoded string. 
		 */
		public function readMultiByte(length:uint, charSet:String):String {
			return inBuffer.readMultiByte(length, charSet);
		}

		/**
		 * <p> Reads an object from the socket, encoded in AMF serialized format. </p>
		 * 
		 * @return  — The deserialized object 
		 */
		public function readObject():* {
			return inBuffer.readObject();
		}

		/**
		 * <p> Reads a signed 16-bit integer from the socket. </p>
		 * 
		 * @return  — A value from -32768 to 32767. 
		 */
		public function readShort():int {
			return inBuffer.readShort();
		}

		/**
		 * <p> Reads a UTF-8 string from the socket. The string is assumed to be prefixed with an unsigned short integer that indicates the length in bytes. </p>
		 * 
		 * @return  — A UTF-8 string. 
		 */
		public function readUTF():String {
			return inBuffer.readUTF();
		}

		/**
		 * <p> Reads the number of UTF-8 data bytes specified by the <code>length</code> parameter from the socket, and returns a string. </p>
		 * 
		 * @param length  — The number of bytes to read. 
		 * @return  — A UTF-8 string. 
		 */
		public function readUTFBytes(length:uint):String {
			return inBuffer.readUTFBytes(length);
		}

		/**
		 * <p> Reads an unsigned byte from the socket. </p>
		 * 
		 * @return  — A value from 0 to 255. 
		 */
		public function readUnsignedByte():uint {
			return inBuffer.readUnsignedByte();
		}

		/**
		 * <p> Reads an unsigned 32-bit integer from the socket. </p>
		 * 
		 * @return  — A value from 0 to 4294967295. 
		 */
		public function readUnsignedInt():uint {
			return inBuffer.readUnsignedInt();
		}

		/**
		 * <p> Reads an unsigned 16-bit integer from the socket. </p>
		 * 
		 * @return  — A value from 0 to 65535. 
		 */
		public function readUnsignedShort():uint {
			return inBuffer.readUnsignedShort();
		}

		/**
		 * <p> Writes a Boolean value to the socket. This method writes a single byte, with either a value of 1 (<code>true</code>) or 0 (<code>false</code>). </p>
		 * 
		 * @param value  — The value to write to the socket: 1 (<code>true</code>) or 0 (<code>false</code>). 
		 */
		public function writeBoolean(value:Boolean):void {
			outBuffer.writeBoolean(value);
		}

		/**
		 * <p> Writes a byte to the socket. </p>
		 * 
		 * @param value  — The value to write to the socket. The low 8 bits of the value are used; the high 24 bits are ignored. 
		 */
		public function writeByte(value:int):void {
			outBuffer.writeByte(value);
		}

		/**
		 * <p> Writes a sequence of bytes from the specified byte array. The write operation starts at the position specified by <code>offset</code>. </p>
		 * <p>If you omit the <code>length</code> parameter the default length of 0 causes the method to write the entire buffer starting at <code>offset</code>.</p>
		 * <p>If you also omit the <code>offset</code> parameter, the entire buffer is written.</p>
		 * 
		 * @param bytes  — The ByteArray object to write data from. 
		 * @param offset  — The zero-based offset into the <code>bytes</code> ByteArray object at which data writing should begin. 
		 * @param length  — The number of bytes to write. The default value of 0 causes the entire buffer to be written, starting at the value specified by the <code>offset</code> parameter. 
		 */
		public function writeBytes(bytes:ByteArray, offset:uint = 0, length:uint = 0):void {
			outBuffer.writeBytes(bytes, offset, length);
		}

		/**
		 * <p> Writes an IEEE 754 double-precision floating-point number to the socket. </p>
		 * 
		 * @param value  — The value to write to the socket. 
		 */
		public function writeDouble(value:Number):void {
			outBuffer.writeDouble(value);
		}

		/**
		 * <p> Writes an IEEE 754 single-precision floating-point number to the socket. </p>
		 * 
		 * @param value  — The value to write to the socket. 
		 */
		public function writeFloat(value:Number):void {
			outBuffer.writeFloat(value);
		}

		/**
		 * <p> Writes a 32-bit signed integer to the socket. </p>
		 * 
		 * @param value  — The value to write to the socket. 
		 */
		public function writeInt(value:int):void {
			outBuffer.writeInt(value);
		}

		/**
		 * <p> Writes a multibyte string from the byte stream, using the specified character set. </p>
		 * 
		 * @param value  — The string value to be written. 
		 * @param charSet  — The string denoting the character set to use to interpret the bytes. Possible character set strings include <code>"shift_jis"</code>, <code>"CN-GB"</code>, and <code>"iso-8859-1"</code>. For a complete list, see <a href="../../charset-codes.html">Supported Character Sets</a>. 
		 */
		public function writeMultiByte(value:String, charSet:String):void {
			outBuffer.writeMultiByte(value, charSet);
		}

		/**
		 * <p> Write an object to the socket in AMF serialized format. </p>
		 * 
		 * @param object  — The object to be serialized. 
		 */
		public function writeObject(object:*):void {
			outBuffer.writeObject(object);
		}

		/**
		 * <p> Writes a 16-bit integer to the socket. The bytes written are as follows: </p>
		 * <pre><code>(v &gt;&gt; 8) &amp; 0xff v &amp; 0xff</code></pre>
		 * <p>The low 16 bits of the parameter are used; the high 16 bits are ignored.</p>
		 * 
		 * @param value  — The value to write to the socket. 
		 */
		public function writeShort(value:int):void {
			outBuffer.writeShort(value);
		}

		/**
		 * <p> Writes the following data to the socket: a 16-bit unsigned integer, which indicates the length of the specified UTF-8 string in bytes, followed by the string itself. </p>
		 * <p>Before writing the string, the method calculates the number of bytes that are needed to represent all characters of the string.</p>
		 * 
		 * @param value  — The string to write to the socket. 
		 */
		public function writeUTF(value:String):void {
			outBuffer.writeUTF(value);
		}

		/**
		 * <p> Writes a UTF-8 string to the socket. </p>
		 * 
		 * @param value  — The string to write to the socket. 
		 */
		public function writeUTFBytes(value:String):void {
			outBuffer.writeUTFBytes(value);
		}

		/**
		 * <p> Writes a 32-bit unsigned integer to the socket. </p>
		 * 
		 * @param value  — The value to write to the socket. 
		 */
		public function writeUnsignedInt(value:uint):void {
			outBuffer.writeUnsignedInt(value);
		}

		protected function buildSocketURL(host:String, port:int):String {
			return "ws://"+host+":"+port+"/socket";
		}

		private function onOpen(e:*):void {
			this.dispatchEvent(new flash.events.Event(flash.events.Event.CONNECT));
		}

		private function onMessage(e:*):void {
			console.log(e);
			var b:ArrayBuffer = e.data;
			inBuffer.bufferData(b);
			this.dispatchEvent(new ProgressEvent(ProgressEvent.SOCKET_DATA, false, false, inBuffer.bytesAvailable));
		}

		private function onClose(e:*):void {
			removeListeners();
			wSocket = null;
			this.dispatchEvent(new flash.events.Event(flash.events.Event.CLOSE));
		}

		private function removeListeners():void {
			wSocket.removeEventListener("open", onOpen);
			wSocket.removeEventListener("message", onMessage);
			wSocket.removeEventListener("close", onClose);
		}
	}
}
