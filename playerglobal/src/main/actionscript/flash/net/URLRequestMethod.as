package flash.net {
	/**
	 *  The URLRequestMethod class provides values that specify whether the URLRequest object should use the <code>POST</code> method or the <code>GET</code> method when sending data to a server. <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="URLRequest.html" target="">URLRequest</a>
	 *  <br>
	 *  <a href="URLVariables.html" target="">URLVariables</a>
	 * </div><br><hr>
	 */
	public class URLRequestMethod {
		/**
		 * <p> Specifies that the URLRequest object is a <code>DELETE</code>. </p>
		 */
		public static const DELETE:String = "DELETE";
		/**
		 * <p> Specifies that the URLRequest object is a <code>GET</code>. </p>
		 */
		public static const GET:String = "GET";
		/**
		 * <p> Specifies that the URLRequest object is a <code>HEAD</code>. </p>
		 */
		public static const HEAD:String = "HEAD";
		/**
		 * <p> Specifies that the URLRequest object is <code>OPTIONS</code>. </p>
		 */
		public static const OPTIONS:String = "OPTIONS";
		/**
		 * <p> Specifies that the URLRequest object is a <code>POST</code>. </p>
		 * <p><i>Note:</i> <span>For content running in Adobe AIR, when </span> using the <code>navigateToURL()</code> function, the runtime treats a URLRequest that uses the POST method (one that has its <code>method</code> property set to <code>URLRequestMethod.POST</code>) as using the GET method.</p>
		 */
		public static const POST:String = "POST";
		/**
		 * <p> Specifies that the URLRequest object is a <code>PUT</code>. </p>
		 */
		public static const PUT:String = "PUT";
	}
}
