package flash.net {
	/**
	 *  The NetStreamInfo class specifies the various Quality of Service (QOS) statistics and other information related to a NetStream object and the underlying streaming buffer for audio, video, and data. A NetStreamInfo object is returned in response to the <code>NetStream.info</code> call, which takes a snapshot of the current QOS state and provides the QOS statistics through the NetStreamInfo properties. <p> <b>Note:</b> AIR 3.0 for iOS does not support any NetStreamInfo properties.</p> <br><hr>
	 */
	public class NetStreamInfo {
		private var _SRTT:Number;
		private var _audioBufferByteLength:Number;
		private var _audioBufferLength:Number;
		private var _audioByteCount:Number;
		private var _audioBytesPerSecond:Number;
		private var _audioLossRate:Number;
		private var _byteCount:Number;
		private var _currentBytesPerSecond:Number;
		private var _dataBufferByteLength:Number;
		private var _dataBufferLength:Number;
		private var _dataByteCount:Number;
		private var _dataBytesPerSecond:Number;
		private var _droppedFrames:Number;
		private var _isLive:Boolean;
		private var _maxBytesPerSecond:Number;
		private var _metaData:Object;
		private var _playbackBytesPerSecond:Number;
		private var _resourceName:String;
		private var _uri:String;
		private var _videoBufferByteLength:Number;
		private var _videoBufferLength:Number;
		private var _videoByteCount:Number;
		private var _videoBytesPerSecond:Number;
		private var _videoLossRate:Number;
		private var _xmpData:Object;

		/**
		 * <p> The smoothed round trip time (SRTT) for the NetStream session, in milliseconds. This property contains a valid value only for RTMFP streams. For RTMP streams, the value is 0. </p>
		 * <p><b>Note:</b> Not supported in AIR 3.0 for iOS.</p>
		 * 
		 * @return 
		 */
		public function get SRTT():Number {
			return _SRTT;
		}

		/**
		 * <p> Provides the NetStream audio buffer size in bytes. It specifies the buffer size for audio data in streaming media, both live and recorded. This property is like <code>Netstream.bytesLoaded</code>, which is used in progressive downloads. </p>
		 * <p><b>Note:</b> Not supported in AIR 3.0 for iOS.</p>
		 * 
		 * @return 
		 */
		public function get audioBufferByteLength():Number {
			return _audioBufferByteLength;
		}

		/**
		 * <p> Provides NetStream audio buffer size in seconds. This property extends the <code>NetStream.bufferLength</code> property and provides the buffer length in time specifically for audio data. </p>
		 * <p><b>Note:</b> Not supported in AIR 3.0 for iOS.</p>
		 * 
		 * @return 
		 */
		public function get audioBufferLength():Number {
			return _audioBufferLength;
		}

		/**
		 * <p> Specifies the total number of audio bytes that have arrived in the queue, regardless of how many have been played or flushed. You can use this value to calculate the incoming audio data rate, using the metric of your choice, by creating a timer and calculating the difference in values in successive timer calls. Alternatively, use <code>audioBytesPerSecond</code>. </p>
		 * <p><b>Note:</b> Not supported in AIR 3.0 for iOS.</p>
		 * 
		 * @return 
		 */
		public function get audioByteCount():Number {
			return _audioByteCount;
		}

		/**
		 * <p> Specifies the rate at which the NetStream audio buffer is filled in bytes per second. The value is calculated as a smooth average for the audio data received in the last second. </p>
		 * <p><b>Note:</b> Not supported in AIR 3.0 for iOS.</p>
		 * 
		 * @return 
		 */
		public function get audioBytesPerSecond():Number {
			return _audioBytesPerSecond;
		}

		/**
		 * <p> Specifies the audio loss for the NetStream session. This value returns a valid value only for RTMFP streams and would return 0 for RTMP streams. Loss rate is defined as the ratio of lost messages to total messages. </p>
		 * <p><b>Note:</b> Not supported in AIR 3.0 for iOS.</p>
		 * 
		 * @return 
		 */
		public function get audioLossRate():Number {
			return _audioLossRate;
		}

		/**
		 * <p> Specifies the total number of bytes that have arrived into the queue, regardless of how many have been played or flushed. You can use this value to calculate the incoming data rate, using the metric of your choice, by creating a timer and calculating the difference in values in successive timer calls. Alternatively, use <code>currentBytesPerSecond</code>. </p>
		 * <p><b>Note:</b> Not supported in AIR 3.0 for iOS.</p>
		 * 
		 * @return 
		 */
		public function get byteCount():Number {
			return _byteCount;
		}

		/**
		 * <p> Specifies the rate at which the NetStream buffer is filled in bytes per second. The value is calculated as a smooth average for the total data received in the last second. </p>
		 * <p><b>Note:</b> Not supported in AIR 3.0 for iOS.</p>
		 * 
		 * @return 
		 */
		public function get currentBytesPerSecond():Number {
			return _currentBytesPerSecond;
		}

		/**
		 * <p> Provides the NetStream data buffer size in bytes. It specifies the buffer size for data messages in streaming media, both live and recorded. This property is like <code>Netstream.bytesLoaded</code>, which is used in progressive downloads. </p>
		 * <p><b>Note:</b> Not supported in AIR 3.0 for iOS.</p>
		 * 
		 * @return 
		 */
		public function get dataBufferByteLength():Number {
			return _dataBufferByteLength;
		}

		/**
		 * <p> Provides NetStream data buffer size in seconds. This property extends the <code>NetStream.bufferLength</code> property and provides the buffer length in time specifically for data messages. </p>
		 * <p><b>Note:</b> Not supported in AIR 3.0 for iOS.</p>
		 * 
		 * @return 
		 */
		public function get dataBufferLength():Number {
			return _dataBufferLength;
		}

		/**
		 * <p> Specifies the total number of bytes of data messages that have arrived in the queue, regardless of how many have been played or flushed. You can use this value to calculate the incoming data-messages rate, using the metric of your choice, by creating a timer and calculating the difference in values in successive timer calls. Alternatively, use <code>dataBytesPerSecond</code>. </p>
		 * <p><b>Note:</b> Not supported in AIR 3.0 for iOS.</p>
		 * 
		 * @return 
		 */
		public function get dataByteCount():Number {
			return _dataByteCount;
		}

		/**
		 * <p> Specifies the rate at which the NetStream data buffer is filled in bytes per second. The value is calculated as a smooth average for the data messages received in the last second. </p>
		 * <p><b>Note:</b> Not supported in AIR 3.0 for iOS.</p>
		 * 
		 * @return 
		 */
		public function get dataBytesPerSecond():Number {
			return _dataBytesPerSecond;
		}

		/**
		 * <p> Returns the number of video frames dropped in the current NetStream playback session. </p>
		 * <p>In recorded streaming or progressive download, if the video is a high-quality or high-resolution, high-bitrate video, the decoder can lag behind in decoding the required number of frames per second if it does not have adequate system CPU resources. In live streaming, the buffer drops video frames if the latency is too high. This property specifies the number of frames that were dropped and not presented normally.</p>
		 * <p><b>Note:</b> Not supported in AIR 3.0 for iOS.</p>
		 * 
		 * @return 
		 */
		public function get droppedFrames():Number {
			return _droppedFrames;
		}

		/**
		 * <p> Whether the media being played is recorded or live. This property is relevant for RTMP streaming only. For progressive download and HTTP Dynamic Streaming the property is always <code>false</code>. </p>
		 * <p><b>Note:</b> Not supported in AIR 3.0 for iOS.</p>
		 * <p><b>Note:</b> This property is always <code>false</code> in Flash Player in the browser on Android and Blackberry Tablet OS or in AIR on iOS.</p>
		 * 
		 * @return 
		 */
		public function get isLive():Boolean {
			return _isLive;
		}

		/**
		 * <p> Specifies the maximum rate at which the NetStream buffer is filled in bytes per second. This value provides information about the capacity of the client network based on the last messages received by the <code>NetStream</code> object. Depending on the size of the buffer specified in <code>NetStream.bufferTime</code> and the bandwidth available on the client, Flash Media Server fills the buffer in bursts. This property provides the maximum rate at which the client buffer is filled. </p>
		 * <p><b>Note:</b> Not supported in AIR 3.0 for iOS.</p>
		 * 
		 * @return 
		 */
		public function get maxBytesPerSecond():Number {
			return _maxBytesPerSecond;
		}

		/**
		 * <p> The most recent metadata object associated with the media being played. </p>
		 * <p><b>Note:</b> Not supported in AIR 3.0 for iOS.</p>
		 * <p><b>Note:</b> This property is always <code>null</code> in Flash Player in the browser on Android and Blackberry Tablet OS or in AIR on iOS.</p>
		 * 
		 * @return 
		 */
		public function get metaData():Object {
			return _metaData;
		}

		/**
		 * <p> Returns the stream playback rate in bytes per second. The playback buffer can contain content of various playlists. This property provides the playback rate that closely matches the bit rate of the currently playing stream. </p>
		 * <p><b>Note:</b> Not supported in AIR 3.0 for iOS.</p>
		 * 
		 * @return 
		 */
		public function get playbackBytesPerSecond():Number {
			return _playbackBytesPerSecond;
		}

		/**
		 * <p> The resource name used when <code>NetStream.play()</code> was called. This property contains the full URL for progressive download, the resource name for RTMP streaming and <code>null</code> for HTTP streaming. </p>
		 * <p><b>Note:</b> Not supported in AIR 3.0 for iOS.</p>
		 * <p><b>Note:</b> This property is always <code>null</code> in Flash Player in the browser on Android and Blackberry Tablet OS or in AIR on iOS.</p>
		 * 
		 * @return 
		 */
		public function get resourceName():String {
			return _resourceName;
		}

		/**
		 * <p> The URI used when <code>NetConnection.connect()</code> was called. This is <code>null</code> for progressive download or HTTP streaming. </p>
		 * <p><b>Note:</b> Not supported in AIR 3.0 for iOS.</p>
		 * <p><b>Note:</b> This property is always <code>null</code> in Flash Player in the browser on Android and Blackberry Tablet OS or in AIR on iOS.</p>
		 * 
		 * @return 
		 */
		public function get uri():String {
			return _uri;
		}

		/**
		 * <p> Provides the NetStream video buffer size in bytes. It specifies the buffer size for video data in streaming media, both live and recorded. This property is like <code>Netstream.bytesLoaded</code>, which is used in progressive downloads. </p>
		 * <p><b>Note:</b> Not supported in AIR 3.0 for iOS.</p>
		 * 
		 * @return 
		 */
		public function get videoBufferByteLength():Number {
			return _videoBufferByteLength;
		}

		/**
		 * <p> Provides NetStream video buffer size in seconds. This property extends the <code>NetStream.bufferLength</code> property and provides the buffer length in time specifically for video data. </p>
		 * <p><b>Note:</b> Not supported in AIR 3.0 for iOS.</p>
		 * 
		 * @return 
		 */
		public function get videoBufferLength():Number {
			return _videoBufferLength;
		}

		/**
		 * <p> Specifies the total number of video bytes that have arrived in the queue, regardless of how many have been played or flushed. You can use this value to calculate the incoming video data rate, using the metric of your choice, by creating a timer and calculating the difference in values in successive timer calls. Alternatively, use <code>videoBytesPerSecond</code>, </p>
		 * <p><b>Note:</b> Not supported in AIR 3.0 for iOS.</p>
		 * 
		 * @return 
		 */
		public function get videoByteCount():Number {
			return _videoByteCount;
		}

		/**
		 * <p> Specifies the rate at which the NetStream video buffer is filled in bytes per second. The value is calculated as a smooth average for the video data received in the last second. </p>
		 * <p><b>Note:</b> Not supported in AIR 3.0 for iOS.</p>
		 * 
		 * @return 
		 */
		public function get videoBytesPerSecond():Number {
			return _videoBytesPerSecond;
		}

		/**
		 * <p> Provides the NetStream video loss rate (ratio of lost messages to total messages). </p>
		 * <p>When the message size is smaller than the maximum transmission unit (MTU), this value corresponds to the network packet loss rate.</p>
		 * <p>This property returns a valid value only for RTMFP streams. For RTMP streams, it returns a value of zero. For more information, see the <a href="http://www.adobe.com/go/learn_fms_docs_en" target="external">Flash Media Server documentation</a>.</p>
		 * <p><b>Note:</b> Not supported in AIR 3.0 for iOS.</p>
		 * 
		 * @return 
		 */
		public function get videoLossRate():Number {
			return _videoLossRate;
		}

		/**
		 * <p> The most recent XMP data object associated with the media being played. </p>
		 * <p><b>Note:</b> Not supported in AIR 3.0 for iOS.</p>
		 * <p><b>Note:</b> This property is always <code>null</code> in Flash Player in the browser on Android and Blackberry Tablet OS or in AIR on iOS.</p>
		 * 
		 * @return 
		 */
		public function get xmpData():Object {
			return _xmpData;
		}

		/**
		 * <p> Returns a text value listing the properties of the NetStreamInfo object. </p>
		 * 
		 * @return  — A string containing the values of the properties of the NetStreamInfo object 
		 */
		public function toString():String {
			throw new Error("Not implemented");
		}
	}
}
