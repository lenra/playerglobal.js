package flash.net {
	/**
	 *  The NetStreamMulticastInfo class specifies various Quality of Service (QoS) statistics related to a NetStream object's underlying RTMFP Peer-to-Peer and IP Multicast stream transport. A NetStreamMulticastInfo object is returned by the <code>NetStream.multicastInfo</code> property. <p>Properties that return numbers represent totals computed from the beginning of the multicast stream. These types of properties include the number of media bytes sent or the number of media fragment messages received. Properties that are rates represent a snapshot of the current rate averaged over a few seconds. These types of properties include the rate at which a local node is receiving data. </p> <p>To see a list of values contained in the NetStreamMulticastInfo object, use the <code>NetStreamMulticastInfo.toString()</code> method.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="NetStreamMulticastInfo.html#toString()" target="">toString()</a>
	 *  <br>
	 *  <a href="NetStream.html#multicastInfo" target="">flash.net.NetStream.multicastInfo</a>
	 * </div><br><hr>
	 */
	public class NetStreamMulticastInfo {
		private var _bytesPushedFromPeers:Number;
		private var _bytesPushedToPeers:Number;
		private var _bytesReceivedFromIPMulticast:Number;
		private var _bytesReceivedFromServer:Number;
		private var _bytesRequestedByPeers:Number;
		private var _bytesRequestedFromPeers:Number;
		private var _fragmentsPushedFromPeers:Number;
		private var _fragmentsPushedToPeers:Number;
		private var _fragmentsReceivedFromIPMulticast:Number;
		private var _fragmentsReceivedFromServer:Number;
		private var _fragmentsRequestedByPeers:Number;
		private var _fragmentsRequestedFromPeers:Number;
		private var _receiveControlBytesPerSecond:Number;
		private var _receiveDataBytesPerSecond:Number;
		private var _receiveDataBytesPerSecondFromIPMulticast:Number;
		private var _receiveDataBytesPerSecondFromServer:Number;
		private var _sendControlBytesPerSecond:Number;
		private var _sendControlBytesPerSecondToServer:Number;
		private var _sendDataBytesPerSecond:Number;

		/**
		 * <p> Specifies the number of media bytes that were proactively pushed from peers and received by the local node. </p>
		 * 
		 * @return 
		 */
		public function get bytesPushedFromPeers():Number {
			return _bytesPushedFromPeers;
		}

		/**
		 * <p> Specifies the number of media bytes that the local node has proactively pushed to peers. </p>
		 * 
		 * @return 
		 */
		public function get bytesPushedToPeers():Number {
			return _bytesPushedToPeers;
		}

		/**
		 * <p> Specifies the number of media bytes that the local node has received from IP Multicast. </p>
		 * 
		 * @return 
		 */
		public function get bytesReceivedFromIPMulticast():Number {
			return _bytesReceivedFromIPMulticast;
		}

		/**
		 * <p> Specifies the number of media bytes that the local node has received from the server. </p>
		 * 
		 * @return 
		 */
		public function get bytesReceivedFromServer():Number {
			return _bytesReceivedFromServer;
		}

		/**
		 * <p> Specifies the number of media bytes that the local node has sent to peers in response to requests from those peers for specific fragments. </p>
		 * 
		 * @return 
		 */
		public function get bytesRequestedByPeers():Number {
			return _bytesRequestedByPeers;
		}

		/**
		 * <p> Specifies the number of media bytes that the local node requested and received from peers. </p>
		 * 
		 * @return 
		 */
		public function get bytesRequestedFromPeers():Number {
			return _bytesRequestedFromPeers;
		}

		/**
		 * <p> Specifies the number of media fragment messages that were proactively pushed from peers and received by the local node. </p>
		 * 
		 * @return 
		 */
		public function get fragmentsPushedFromPeers():Number {
			return _fragmentsPushedFromPeers;
		}

		/**
		 * <p> Specifies the number of media fragment messages that the local node has proactively pushed to peers. </p>
		 * 
		 * @return 
		 */
		public function get fragmentsPushedToPeers():Number {
			return _fragmentsPushedToPeers;
		}

		/**
		 * <p> Specifies the number of media fragment messages that the local node has received from IP Multicast. </p>
		 * 
		 * @return 
		 */
		public function get fragmentsReceivedFromIPMulticast():Number {
			return _fragmentsReceivedFromIPMulticast;
		}

		/**
		 * <p> Specifies the number of media fragment messages that the local node has received from the server. </p>
		 * 
		 * @return 
		 */
		public function get fragmentsReceivedFromServer():Number {
			return _fragmentsReceivedFromServer;
		}

		/**
		 * <p> Specifies the number of media fragment messages that the local node has sent to peers in response to requests from those peers for specific fragments. </p>
		 * 
		 * @return 
		 */
		public function get fragmentsRequestedByPeers():Number {
			return _fragmentsRequestedByPeers;
		}

		/**
		 * <p> Specifies the number of media fragment messages that the local node requested and received from peers. </p>
		 * 
		 * @return 
		 */
		public function get fragmentsRequestedFromPeers():Number {
			return _fragmentsRequestedFromPeers;
		}

		/**
		 * <p> Specifies the rate at which the local node is receiving control overhead messages from peers, in bytes per second. </p>
		 * 
		 * @return 
		 */
		public function get receiveControlBytesPerSecond():Number {
			return _receiveControlBytesPerSecond;
		}

		/**
		 * <p> Specifies the rate at which the local node is receiving media data from peers, from the server, and over IP multicast, in bytes per second. </p>
		 * 
		 * @return 
		 */
		public function get receiveDataBytesPerSecond():Number {
			return _receiveDataBytesPerSecond;
		}

		/**
		 * <p> Specifies the rate at which the local node is receiving data from IP Multicast, in bytes per second. </p>
		 * 
		 * @return 
		 */
		public function get receiveDataBytesPerSecondFromIPMulticast():Number {
			return _receiveDataBytesPerSecondFromIPMulticast;
		}

		/**
		 * <p> Specifies the rate at which the local node is receiving media data from the server, in bytes per second. </p>
		 * 
		 * @return 
		 */
		public function get receiveDataBytesPerSecondFromServer():Number {
			return _receiveDataBytesPerSecondFromServer;
		}

		/**
		 * <p> Specifies the rate at which the local node is sending control overhead messages to peers and the server, in bytes per second. </p>
		 * 
		 * @return 
		 */
		public function get sendControlBytesPerSecond():Number {
			return _sendControlBytesPerSecond;
		}

		/**
		 * <p> Specifies the rate at which the local node is sending control overhead messages to the server, in bytes per second. </p>
		 * 
		 * @return 
		 */
		public function get sendControlBytesPerSecondToServer():Number {
			return _sendControlBytesPerSecondToServer;
		}

		/**
		 * <p> Specifies the rate at which media data is being sent by the local node to peers, in bytes per second. </p>
		 * 
		 * @return 
		 */
		public function get sendDataBytesPerSecond():Number {
			return _sendDataBytesPerSecond;
		}

		/**
		 * <p> Returns a string listing the properties of the NetStreamMulticastInfo object. </p>
		 * 
		 * @return  — A string containing the values of the properties of the NetStreamMulticastInfo object 
		 */
		public function toString():String {
			throw new Error("Not implemented");
		}
	}
}
