package flash.net {
	import flash.events.EventDispatcher;
	import flash.events.NetStatusEvent;

	[Event(name="netStatus", type="flash.events.NetStatusEvent")]
	/**
	 *  Instances of the NetGroup class represent membership in an RTMFP group. Use this class to do the following: <ul> 
	 *  <li> <b>Monitor Quality of Service</b>. The <code>info</code> property contains a NetGroupInfo object whose properties provide QoS statistics for this group.</li> 
	 *  <li> <b>Posting</b>. Call <code>post()</code> to broadcast ActionScript messages to all members of a group.</li> 
	 *  <li> <b>Direct routing</b>. Call <code>sendToNearest()</code>, <code>sendToNeighbor()</code>, and <code>sendToAllNeighbors()</code> to send a short data message to a specific member of a peer-to-peer group. The source and the destination do not need to have a direct connection.</li> 
	 *  <li> <b>Object replication</b>. Call <code>addHaveObjects()</code>, <code>removeHaveObjects()</code>, <code>addWantObjects()</code>, <code>removeWantObjects()</code>, <code>writeRequestedObject()</code>, and <code>denyRequestedObject()</code> to break up large data into pieces and replicate it to all nodes in a peer-to-peer group.</li> 
	 * </ul> <p>In the client-side NetGroup class, the NetConnection dispatches the following events:</p> <ul> 
	 *  <li>NetGroup.Connect.Success</li> 
	 *  <li>NetGroup.Connect.Failed</li> 
	 *  <li>NetGroup.Connect.Rejected</li> 
	 * </ul> <p>The <code>info.group</code> property of the event object contains a reference to the event source (the NetGroup). The NetGroup dispatches all other events. In the server-side NetGroup class, the NetGroup dispatches all events.</p> <p>For information about using groups with peer-assisted networking, see <a href="http://tv.adobe.com/watch/adobe-at-nab-2010/social-media-experiences-with-adobe-flash-media-server-and-rtmfp/" target="external">Social Media Experiences with Flash Media and RTMFP</a>, also by Tom Krcha.</p> <p>For information about the technical details behind peer-assisted networking, see <a href="http://tv.adobe.com/watch/fitc/playertoplayer-communications-with-rtmfp/" target="external">P2P on the Flash Platform with RTMFP</a> by Adobe Computer Scientist Matthew Kaufman.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="GroupSpecifier.html" target="">flash.net.GroupSpecifier</a>
	 *  <br>
	 *  <a href="NetStream.html" target="">flash.net.NetStream</a>
	 *  <br>
	 *  <a href="../../flash/events/NetStatusEvent.html#info" target="">flash.events.NetStatusEvent info.code values starting with "NetGroup."</a>
	 * </div><br><hr>
	 */
	public class NetGroup extends EventDispatcher {
		private var _receiveMode:String;
		private var _replicationStrategy:String;

		private var _estimatedMemberCount:Number;
		private var _info:NetGroupInfo;
		private var _localCoverageFrom:String;
		private var _localCoverageTo:String;
		private var _neighborCount:Number;

		/**
		 * <p> Constructs a NetGroup on the specified NetConnection object and joins it to the group specified by <code>groupspec</code>. </p>
		 * <p>In most cases, a <code>groupspec</code> has the potential for using the network uplink on the local system. When a NetStream or NetGroup object is constructed with a groupspec, Flash Player displays a Privacy Dialog. The dialog asks whether Flash Player can use the connection to share data with a user's peers. If the user clicks "Allow for this domain", the dialog is not displayed the next time the user connects to this application. If a user does not allow peer-assisted networking, all peer features within the group (posting, directed routing, and object replication, and multicast) are disabled. If permission is allowed, a NetStatusEvent is sent to the NetConnection's event listener with <code>NetGroup.Connect.Success</code> in the <code>code</code> property of the <code>info</code> object. If permission is denied, the <code>code</code> property is <code>NetGroup.Connect.Rejected</code>. Until a <code>NetGroup.Connect.Success</code> event is received, an exception is thrown if you try to call any method of the NetGroup object.</p>
		 * <p><b>Note:</b> When a client subscribes to a native-IP multicast stream, the security dialog is not displayed.</p>
		 * 
		 * @param connection  — A NetConnection object. 
		 * @param groupspec  — A string specifying the RTMFP peer-to-peer group to join, including its name, capabilities, restrictions, and the authorizations of this member. <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *      new NetGroup(myConnection, myGroupSpecifier.groupspecWithAuthorizations());
		 *      </pre>
		 * </div> 
		 */
		public function NetGroup(connection:NetConnection, groupspec:String) {
			super();
			throw new Error("Not implemented");
		}

		/**
		 * <p> Specifies the estimated number of members of the group, based on local neighbor density and assuming an even distribution of group addresses. </p>
		 * 
		 * @return 
		 */
		public function get estimatedMemberCount():Number {
			return _estimatedMemberCount;
		}

		/**
		 * <p> Returns a NetGroupInfo object whose properties provide Quality of Service statistics about this NetGroup's RTMFP peer-to-peer data transport. </p>
		 * 
		 * @return 
		 */
		public function get info():NetGroupInfo {
			return _info;
		}

		/**
		 * <p> Specifies the start of the range of group addresses for which this node is the "nearest" and responsible. The range is specified in the increasing direction along the group address ring mod 2<sup>256</sup>. </p>
		 * 
		 * @return 
		 */
		public function get localCoverageFrom():String {
			return _localCoverageFrom;
		}

		/**
		 * <p> Specifies the end of the range of group addresses for which this node is the "nearest" and responsible. The range is specified in the increasing direction along the group address ring mod 2<sup>256</sup>. </p>
		 * 
		 * @return 
		 */
		public function get localCoverageTo():String {
			return _localCoverageTo;
		}

		/**
		 * <p> Specifies the number of group members to which this node is directly connected. </p>
		 * 
		 * @return 
		 */
		public function get neighborCount():Number {
			return _neighborCount;
		}

		/**
		 * <p> Specifies this node's routing receive mode as one of values in the NetGroupReceiveMode enum class. </p>
		 * 
		 * @return 
		 */
		public function get receiveMode():String {
			return _receiveMode;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set receiveMode(value:String):void {
			_receiveMode = value;
		}

		/**
		 * <p> Specifies the object replication fetch strategy. The value is one of the enumerated values in the NetGroupReplicationStrategy class. </p>
		 * 
		 * @return 
		 */
		public function get replicationStrategy():String {
			return _replicationStrategy;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set replicationStrategy(value:String):void {
			_replicationStrategy = value;
		}

		/**
		 * <p> Adds objects from <code>startIndex</code> through <code>endIndex</code>, to the set of objects this node advertises to neighbors as objects for which it fulfills requests. By default, the Have set is empty. Indices must be whole numbers from 0 through 9007199254740992. </p>
		 * <p>For more information about object replication, see <a href="http://www.adobe.com/go/learn_fms_replicate_en">"Replicate an object within a group"</a> in <i>Flash Media Server Developer’s Guide</i>.</p>
		 * <p>This method sends a NetStatusEvent to the NetGroup's event listener with <code>"NetGroup.Replication.Request"</code> in the <code>code</code> property of the <code>info</code> object. </p>
		 * <p><b>NOTE:</b> Test for the <code>NetGroup.Neighbor.Connect</code> event before calling this method.</p>
		 * 
		 * @param startIndex  — The beginning of the range of object indices to add to the Have set. 
		 * @param endIndex  — The end of the range of object indices to add to the Have set. 
		 */
		public function addHaveObjects(startIndex:Number, endIndex:Number):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Manually adds a record specifying that <code>peerID</code> is a member of the group. An immediate connection to it is attempted only if it is needed for the topology. </p>
		 * 
		 * @param peerID  — The peerID to add to the set of potential neighbors. 
		 * @return  — TRUE for success, FALSE for failure. 
		 */
		public function addMemberHint(peerID:String):Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Manually adds a neighbor by immediately connecting directly to the specified <code>peerID</code>, which must already be in this group. This direct connection may later be dropped if it is not needed for the topology. </p>
		 * 
		 * @param peerID  — The peerID to which to immediately connect. 
		 * @return  — TRUE for success, FALSE for failure. 
		 */
		public function addNeighbor(peerID:String):Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Adds objects from <code>startIndex</code> through <code>endIndex</code>, to the set of objects to retrieve. Indices must be whole numbers from 0 through 9007199254740992. By default, the Want set is empty. </p>
		 * <p>For more information about object replication, see <a href="http://www.adobe.com/go/learn_fms_replicate_en">"Replicate an object within a group"</a> in <i>Flash Media Server Developer’s Guide</i>.</p>
		 * <p>This method sends a NetStatusEvent to the NetGroup's event listener with <code>NetGroup.Replication.Fetch.SendNotify</code> in the <code>info.code</code> property. This event is followed by an <code>NetGroup.Replication.Fetch.Failed</code> or <code>NetGroup.Replication.Fetch.Result</code> event.</p>
		 * <p><b>NOTE:</b> Test for the <code>NetGroup.Neighbor.Connect</code> event before calling this method.</p>
		 * 
		 * @param startIndex  — The beginning of the range of object indices to add to the Want set. 
		 * @param endIndex  — The end of the range of object indices to add to the Want set. 
		 */
		public function addWantObjects(startIndex:Number, endIndex:Number):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Disconnect from the group and close this NetGroup. This NetGroup is not usable after calling this method. </p>
		 */
		public function close():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Converts a peerID to a group address suitable for use with the <code>sendToNearest()</code> method. </p>
		 * 
		 * @param peerID  — The peerID to convert. 
		 * @return  — The group address for the peerID. 
		 */
		public function convertPeerIDToGroupAddress(peerID:String):String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Denies a request received in a NetStatusEvent <code>NetGroup.Replication.Request</code> for an object previously advertised with <code>addHaveObjects()</code>. The requestor can request this object again unless or until it is withdrawn from the Have set. </p>
		 * <p>For more information about object replication, see <a href="http://www.adobe.com/go/learn_fms_replicate_en">"Replicate an object within a group"</a> in <i>Flash Media Server Developer’s Guide</i>.</p>
		 * <p><b>NOTE:</b> Test for the <code>NetGroup.Neighbor.Connect</code> event before calling this method.</p>
		 * 
		 * @param requestID  — The request identifier as given in the <code>NetGroup.Replication.Request</code> event. 
		 */
		public function denyRequestedObject(requestID:int):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sends a message to all members of a group. To call this method, the <code>GroupSpecifier.postingEnabled</code> property must be <code>true</code> in the groupspec passed to the NetGroup constructor. For more information, see <a href="http://www.adobe.com/go/learn_fms_post_en">"Post messages to a group"</a> in <i>Flash Media Server Developer’s Guide</i>. </p>
		 * <p>All messages must be unique. A message that is identical to one posted earlier might not be propagated. Use a sequence number to make messages unique.</p>
		 * <p>Message delivery is not ordered. Message delivery is not guaranteed.</p>
		 * <p>Messages are serialized in AMF. The message can be one of the following types: an Object, an int, a Number, or a String. The message cannot be a MovieClip.</p>
		 * <p>This method sends a NetStatusEvent to the NetGroup's event listener with <code>"NetGroup.Posting.Notify"</code> in the <code>info.code</code> property. The <code>"NetGroup.Posting.Notify"</code> event is dispatched to the NetGroup on both the client and the server.</p>
		 * <p><b>NOTE:</b> Test for the <code>NetGroup.Neighbor.Connect</code> event before calling this method.</p>
		 * 
		 * @param message  — The message to send to all other members of the group. 
		 * @return  — The messageID of the message if posted, or  on error. The messageID is the hexadecmial of the SHA256 of the raw bytes of the serialization of the message. 
		 */
		public function post(message:Object):String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Removes objects from <code>startIndex</code> through <code>endIndex</code>, from the set of objects this node advertises to neighbors as objects for which it fulfills requests. Indices must be whole numbers from 0 through 9007199254740992. </p>
		 * <p>For more information about object replication, see <a href="http://www.adobe.com/go/learn_fms_replicate_en">"Replicate an object within a group"</a> in <i>Flash Media Server Developer’s Guide</i>.</p>
		 * <p><b>NOTE:</b> Test for the <code>NetGroup.Neighbor.Connect</code> event before calling this method.</p>
		 * 
		 * @param startIndex  — The beginning of the range of object indices to remove from the Have set. 
		 * @param endIndex  — The end of the range of object indices to remove from the Have set. 
		 */
		public function removeHaveObjects(startIndex:Number, endIndex:Number):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Removes objects from <code>startIndex</code> through <code>endIndex</code>, from the set of objects to retrieve. Indices must be whole numbers from 0 through 9007199254740992. </p>
		 * <p>For more information about object replication, see <a href="http://www.adobe.com/go/learn_fms_replicate_en">"Replicate an object within a group"</a> in <i>Flash Media Server Developer’s Guide</i>.</p>
		 * <p><b>NOTE:</b> Test for the <code>NetGroup.Neighbor.Connect</code> event before calling this method.</p>
		 * 
		 * @param startIndex  — The beginning of the range of object indices to remove from the Want set. 
		 * @param endIndex  — The end of the range of object indices to remove from the Want set. 
		 */
		public function removeWantObjects(startIndex:Number, endIndex:Number):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sends a message to all neighbors. Returns <code>NetGroupSendResult.SENT</code> if at least one neighbor was selected. </p>
		 * <p>For more information about routing messages, see <a href="http://www.adobe.com/go/learn_fms_direct_routing_en">"Route messages directly to a peer"</a> in <i>Flash Media Server Developer’s Guide</i>.</p>
		 * <p>When a node receives a message, a NetStatusEvent is sent to the NetGroup's event listener with <code>NetGroup.SendTo.Notify</code> in the <code>code</code> property of the <code>info</code> object. </p>
		 * <p><b>NOTE:</b> Test for the <code>NetGroup.Neighbor.Connect</code> event before calling this method.</p>
		 * 
		 * @param message  — The message to send. 
		 * @return  — A property of enumeration class NetGroupSendResult indicating the success or failure of the send. 
		 */
		public function sendToAllNeighbors(message:Object):String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sends a message to the neighbor (or local node) nearest to the specified group address. Considers neighbors from the entire ring. Returns <code>NetGroupSendResult.SENT</code> if the message was successfully sent toward its destination. </p>
		 * <p>For more information about routing messages, see <a href="http://www.adobe.com/go/learn_fms_direct_routing_en">"Route messages directly to a peer"</a> in <i>Flash Media Server Developer’s Guide</i>.</p>
		 * <p>When a node receives a message, a NetStatusEvent is sent to the NetGroup's event listener with <code>NetGroup.SendTo.Notify</code> in the <code>code</code> property of the <code>info</code> object. </p>
		 * <p><b>NOTE:</b> Test for the <code>NetGroup.Neighbor.Connect</code> event before calling this method.</p>
		 * 
		 * @param message  — The message to send. 
		 * @param groupAddress  — The group address toward which to route the message. 
		 * @return  — A property of enumeration class NetGroupSendResult indicating the success or failure of the send. 
		 */
		public function sendToNearest(message:Object, groupAddress:String):String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sends a message to the neighbor specified by the <code>sendMode</code> parameter. Returns <code>NetGroupSendResult.SENT</code> if the message was successfully sent to the requested destination. </p>
		 * <p>For more information about routing messages, see <a href="http://www.adobe.com/go/learn_fms_direct_routing_en">"Route messages directly to a peer"</a> in <i>Flash Media Server Developer’s Guide</i>.</p>
		 * <p>When a node receives a message, a NetStatusEvent is sent to the NetGroup's event listener with <code>NetGroup.SendTo.Notify</code> in the <code>code</code> property of the <code>info</code> object. </p>
		 * <p><b>NOTE:</b> Test for the <code>NetGroup.Neighbor.Connect</code> event before calling this method.</p>
		 * 
		 * @param message  — The message to send. 
		 * @param sendMode  — A property of enumeration class NetGroupSendMode specifying the neighbor to which to send the message. 
		 * @return  — A property of enumeration class NetGroupSendResult indicating the success or failure of the send. 
		 */
		public function sendToNeighbor(message:Object, sendMode:String):String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Satisfies the request as received by NetStatusEvent <code>NetGroup.Replication.Request</code> for an object previously advertised with the <code>addHaveObjects()</code> method. The <code>object</code> can be any of the following: An Object, an int, a Number, and a String. The <code>object</code> cannot be a MovieClip. </p>
		 * <p>For more information about object replication, see <a href="http://www.adobe.com/go/learn_fms_replicate_en">"Replicate an object within a group"</a> in <i>Flash Media Server Developer’s Guide</i>.</p>
		 * <p><b>NOTE:</b> Test for the <code>NetGroup.Neighbor.Connect</code> event before calling this method.</p>
		 * 
		 * @param requestID  — The request identifier as given in the <code>NetGroup.Replication.Request</code> event. 
		 * @param object  — The object corresponding to the index given in the <code>NetGroup.Replication.Request</code> event. 
		 */
		public function writeRequestedObject(requestID:int, object:Object):void {
			throw new Error("Not implemented");
		}
	}
}
