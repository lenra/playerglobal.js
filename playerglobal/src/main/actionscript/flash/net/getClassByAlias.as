package flash.net {
	/**
	 * <p> Looks up a class that previously had an alias registered through a call to the <code>registerClassAlias()</code> method. </p>
	 * <p>This method does not interact with the <code>flash.utils.getDefinitionByName()</code> method.</p>
	 * 
	 * @param aliasName  — The alias to find. 
	 * @return  — The class associated with the given alias. If not found, an exception will be thrown. 
	 */
	public function getClassByAlias(aliasName:String):Class {
		if (aliasName.startsWith("flash.")) {
			var parts:Array = aliasName.split(".");
			var ret:* = self;
			var len:int = parts.length
			for(var i:int = 0; i < len; i++) {
				ret = ret[parts[i]];
			}
			return ret;
		}
		return aliasses[aliasName];
	}
}
const aliasses:Object = {};