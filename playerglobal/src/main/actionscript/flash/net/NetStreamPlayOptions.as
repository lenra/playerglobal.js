package flash.net {
	import flash.events.EventDispatcher;

	/**
	 *  The NetStreamPlayOptions class specifies the various options that can be passed to the <code>NetStream.play2()</code> method. You pass a NetStreamPlayOptions object to <code>play2()</code>, and the properties of the class specify the various options. The primary use case for this class is to implement transitions between streams dynamically, either to switch to streams of different bit rates and sizes or to swap to different content in a playlist. <br><hr>
	 */
	public class NetStreamPlayOptions extends EventDispatcher {
		private var _len:Number;
		private var _offset:Number;
		private var _oldStreamName:String;
		private var _start:Number;
		private var _streamName:String;
		private var _transition:String;

		/**
		 * <p> Creates a NetStreamPlayOptions object to specify the options that are passed to the <code>NetStream.play2()</code> method. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="NetStream.html#play2()" target="">NetStream.play2()</a>
		 * </div>
		 */
		public function NetStreamPlayOptions() {
			super(this);
			throw new Error("Not implemented");
		}

		/**
		 * <p> The duration of playback, in seconds, for the stream specified in <code>streamName</code>. The default value is -1, which means that Flash Player plays a live stream until it is no longer available or plays a recorded stream until it ends. If you pass 0 for <code>len</code>, Flash Player plays the single frame that is <code>start</code> seconds from the beginning of a recorded stream (assuming that <code>start</code> is equal to or greater than 0). </p>
		 * <p>If you pass a positive number for <code>len</code>, Flash Player plays a live stream for <code>len</code> seconds after it becomes available, or plays a recorded stream for <code>len</code> seconds. (If a stream ends before <code>len</code> seconds, playback ends when the stream ends.)</p>
		 * <p>If you pass a negative number other than -1 for <code>len</code>, Flash Player interprets the value as if it were -1.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="NetStream.html#play()" target="">NetStream.play()</a>
		 *  <br>
		 *  <a href="NetStream.html#play2()" target="">NetStream.play2()</a>
		 *  <br>
		 *  <a href="NetStreamPlayOptions.html#start" target="">start</a>
		 * </div>
		 * 
		 * @return 
		 */
		public function get len():Number {
			return _len;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set len(value:Number):void {
			_len = value;
		}

		/**
		 * <p> The absoulte stream time at which the server switches between streams of different bitrates for Flash Media Server dynamic streaming. The <code>offset</code> property is used when a <code>NetStream.play2()</code> call is made with the <code>NetStreamPlayTransitions.SWITCH</code> transition mode. There are two switching modes: fast switch and standard switch. The default value of <code>offset</code> is <code>-1</code> which is fast switch mode. Write ActionScript logic to decide when to use fast switch and when to use standard switch. </p>
		 * <p><b>Fast switch</b></p>
		 * <p>In fast switch mode, Flash Media Server switches to the new stream without waiting to play the data in the buffer. Any data buffered from the old stream past the <code>offset</code> is flushed. Fast switching is faster than standard switching because the buffered data from the old stream doesn't have to play out.</p>
		 * <p>The default value of <code>offset</code> is -1, which is fast switch mode. When <code>offset</code> is -1, the switch occurs at the first available keyframe after <code>netstream.time + 3</code>, which is about 3 seconds later than the playback point. You can also set the <code>offset</code> property to any value greater than <code>netstream.time</code>. For example, to fast switch 5 seconds after the playback point, set the <code>offset</code> property to <code>netstream.time + 5</code>. If the value of <code>offset</code> is less than <code>netstream.time</code>, a <code>NetStream.Play.Failed</code> status event is sent. </p>
		 * <p><b>Note:</b> <i>The <code>offset</code> property is absolute stream time, it is not an offset from the playback point. For example, to switch 5 seconds from the playback point, set the <code>offset</code> property to <code>netstream.time + 5</code>, not to <code>5</code></i>. </p>
		 * <p><b>Standard switch</b></p>
		 * <p> To use standard switch instead of fast switch, set <code>offset</code> to a value greater than <code>netstream.time + netstream.bufferLength</code>. </p>
		 * <p> Use standard switch when the client has a high bitrate in the buffer and wants to switch to a lower bandwidth due to bandwidth dropping (and not due to CPU issues or dropped frames). The client may want to play out the higher bitrate for as long as possible and switch to the lower bitrate at the end of the buffer (as in standard switching). </p>
		 * <p>For more information, see <a href="http://www.adobe.com/go/learn_fms_fastswitch_en" target="external">"Fast switching between streams"</a> in the <i>Adobe Flash Media Server Developer's Guide</i>.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="NetStreamPlayOptions.html#start" target="">start</a>
		 *  <br>
		 *  <a href="NetStream.html#play()" target="">NetStream.play()</a>
		 *  <br>
		 *  <a href="NetStream.html#play2()" target="">NetStream.play2()</a>
		 *  <br>
		 *  <a href="NetStream.html#time" target="">NetStream.time</a>
		 *  <br>
		 *  <a href="NetStreamPlayTransitions.html#SWITCH" target="">NetStreamPlayTransitions.SWITCH</a>
		 * </div>
		 * 
		 * @return 
		 */
		public function get offset():Number {
			return _offset;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set offset(value:Number):void {
			_offset = value;
		}

		/**
		 * <p> The name of the old stream or the stream to transition from. When <code>NetStream.play2()</code> is used to simply play a stream (not perform a transition), the value of this property should be either null or undefined. Otherwise, specify the stream to transition from. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="NetStreamPlayOptions.html#streamName" target="">streamName</a>
		 *  <br>
		 *  <a href="NetStream.html#play()" target="">NetStream.play()</a>
		 *  <br>
		 *  <a href="NetStream.html#play2()" target="">NetStream.play2()</a>
		 * </div>
		 * 
		 * @return 
		 */
		public function get oldStreamName():String {
			return _oldStreamName;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set oldStreamName(value:String):void {
			_oldStreamName = value;
		}

		/**
		 * <p> The start time, in seconds, for <code>streamName</code>. Valid values are -2, -1, and 0. </p>
		 * <p>The default value for <code>start</code> is -2, which means that Flash Player first tries to play the live stream specified in <code>streamName</code>. If a live stream of that name is not found, Flash Player plays the recorded stream specified in <code>streamName</code>. If neither a live nor a recorded stream is found, Flash Player opens a live stream named <code>streamName</code>, even though no one is publishing on it. When someone does begin publishing on that stream, Flash Player begins playing it.</p>
		 * <p>If you pass -1 for <code>start</code>, Flash Player plays only the live stream specified in <code>streamName</code>. If no live stream is found, Flash Player waits for it indefinitely if <code>len</code> is set to -1; if <code>len</code> is set to a different value, Flash Player waits for <code>len</code> seconds before it begins playing the next item in the playlist.</p>
		 * <p> If you pass 0 or a positive number for <code>start</code>, Flash Player plays only a recorded stream named <code>streamName</code>, beginning <code>start</code> seconds from the beginning of the stream. If no recorded stream is found, Flash Player begins playing the next item in the playlist immediately.</p>
		 * <p>If you pass a negative number other than -1 or -2 for <code>start</code>, Flash Player interprets the value as if it were -2.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="NetStream.html#play()" target="">NetStream.play()</a>
		 *  <br>
		 *  <a href="NetStream.html#play2()" target="">NetStream.play2()</a>
		 *  <br>
		 *  <a href="NetStreamPlayOptions.html#len" target="">len</a>
		 * </div>
		 * 
		 * @return 
		 */
		public function get start():Number {
			return _start;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set start(value:Number):void {
			_start = value;
		}

		/**
		 * <p> The name of the new stream to transition to or to play. When <code>oldStreamName</code> is null or undefined, calling <code>NetStream.play2()</code> simply starts playback of <code>streamName</code>. If <code>oldStreamName</code> is specified, calling <code>NetStream.play2()</code> transitions <code>oldStreamName</code> to <code>streamName</code> using the transition mode specified in the <code>transition</code> property. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="NetStreamPlayOptions.html#oldStreamName" target="">oldStreamName</a>
		 *  <br>
		 *  <a href="NetStream.html#play()" target="">NetStream.play()</a>
		 *  <br>
		 *  <a href="NetStream.html#play2()" target="">NetStream.play2()</a>
		 * </div>
		 * 
		 * @return 
		 */
		public function get streamName():String {
			return _streamName;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set streamName(value:String):void {
			_streamName = value;
		}

		/**
		 * <p> The mode in which <code>streamName</code> is played or transitioned to. Possible values are constants from the NetStreamPlayTransitions class. Depending on whether <code>Netstream.play2()</code> is called to play or transition a stream, the transition mode results in different behaviors. For more information on the transition modes, see the NetStreamPlayTransitions class. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="NetStreamPlayTransitions.html" target="">NetStreamPlayTransitions</a>
		 *  <br>
		 *  <a href="NetStream.html#play2()" target="">NetStream.play2()</a>
		 * </div>
		 * 
		 * @return 
		 */
		public function get transition():String {
			return _transition;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set transition(value:String):void {
			_transition = value;
		}
	}
}
