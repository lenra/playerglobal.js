package flash.net {

	/**
	 * <p> Preserves the class (type) of an object when the object is encoded in Action Message Format (AMF). When you encode an object into AMF, this function saves the alias for its class, so that you can recover the class when decoding the object. If the encoding context did not register an alias for an object's class, the object is encoded as an anonymous object. Similarly, if the decoding context does not have the same alias registered, an anonymous object is created for the decoded data. </p>
	 * <p>LocalConnection, ByteArray, SharedObject, NetConnection and NetStream are all examples of classes that encode objects in AMF.</p>
	 * <p>The encoding and decoding contexts do not need to use the same class for an alias; they can intentionally change classes, provided that the destination class contains all of the members that the source class serializes.</p>
	 * 
	 * @param aliasName  — The alias to use. 
	 * @param classObject  — The class associated with the given alias. 
	 */
	public function registerClassAlias(aliasName:String, classObject:Class):void {
		throw new Error("Not implemented");
	}
}
