package flash.net {
	import flash.events.EventDispatcher;
	import flash.events.NetMonitorEvent;

	[Event(name="netStreamCreate", type="flash.events.NetMonitorEvent")]
	/**
	 *  The NetMonitor class maintains a list of NetStream objects. <p>Use the NetMonitor class to keep track of NetStream objects in use in an application. An instance of this class dispatches a <code>netStreamCreate</code> event whenever a new NetStream object is created.</p> <p>You can use the NetMonitor class to help track video playback and related events without regard to the specific video player being used. This facility can be helpful when implementing media measurement, analytics, and usage tracking libraries.</p> <p> <b>Note:</b> NetStream monitoring is not supported by Flash Player in the browser on Android and Blackberry Tablet OS, or by AIR on iOS.</p> <p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS901d38e593cd1bac-3d11a09612fffaf8447-8000.html" target="_blank">Monitoring NetStream activity</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="NetStream.html" target="">NetStream</a>
	 * </div><br><hr>
	 */
	public class NetMonitor extends EventDispatcher {

		/**
		 * <p> Retrieves all NetStream objects belonging to this NetMonitor object's security context. </p>
		 * <p>Avoid caching the list of NetStream objects. Maintaining a reference to these NetStream objects can introduce memory leaks into an application by preventing the garbage collector from reclaiming an object's resources when it is no longer being used.</p>
		 * <p><b>Note:</b> if the NetStream monitoring is not supported on the current platform, the list returned by this function is always empty.</p>
		 * 
		 * @return  — Vector of NetStream objects 
		 */
		public function listStreams():Vector.<NetStream> {
			throw new Error("Not implemented");
		}
	}
}
