package flash.utils {
	/**
	 *  The CompressionAlgorithm class defines string constants for the names of compress and uncompress options. These constants are used as values of the <code>algorithm</code> parameter of the <code>ByteArray.compress()</code> and <code>ByteArray.uncompress()</code> methods. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="ByteArray.html#compress()" target="">flash.utils.ByteArray.compress()</a>
	 *  <br>
	 *  <a href="ByteArray.html#uncompress()" target="">flash.utils.ByteArray.uncompress()</a>
	 * </div><br><hr>
	 */
	public class CompressionAlgorithm {
		/**
		 * <p> Defines the string to use for the deflate compression algorithm. </p>
		 */
		public static const DEFLATE:String = "deflate";
		/**
		 * <p> Defines the string to use for the lzma compression algorithm. </p>
		 */
		public static const LZMA:String = "lzma";
		/**
		 * <p> Defines the string to use for the zlib compression algorithm. </p>
		 */
		public static const ZLIB:String = "zlib";
	}
}
