package flash.utils {

	/**
	 * <p> Returns the fully qualified class name of an object. </p>
	 * 
	 * @param value  — The object for which a fully qualified class name is desired. Any ActionScript value may be passed to this method including all available ActionScript types, object instances, primitive types such as uint, and class objects. 
	 * @return  — A string containing the fully qualified class name. 
	 */
	public function getQualifiedClassName(value:*):String {
		return value.prototype.ROYALE_CLASS_INFO.names[0].qName;
	}
}
