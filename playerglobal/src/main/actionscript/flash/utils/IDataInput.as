package flash.utils {
	/**
	 *  The IDataInput interface provides a set of methods for reading binary data. This interface is the I/O counterpart to the IDataOutput interface, which writes binary data. <p>All IDataInput and IDataOutput operations are "bigEndian" by default (the most significant byte in the sequence is stored at the lowest or first storage address), and are nonblocking. If insufficient data is available, an <code>EOFError</code> exception is thrown. Use the <code>IDataInput.bytesAvailable</code> property to determine how much data is available to read.</p> <p>Sign extension matters only when you read data, not when you write it. Therefore you do not need separate write methods to work with <code>IDataInput.readUnsignedByte()</code> and <code>IDataInput.readUnsignedShort()</code>. In other words:</p> <ul> 
	 *  <li>Use <code>IDataOutput.writeByte()</code> with <code>IDataInput.readUnsignedByte()</code> and <code>IDataInput.readByte()</code>.</li> 
	 *  <li>Use <code>IDataOutput.writeShort()</code> with <code>IDataInput.readUnsignedShort()</code> and <code>IDataInput.readShort()</code>.</li> 
	 * </ul> <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="IDataOutput.html" target="">IDataOutput interface</a>
	 *  <br>
	 *  <a href="IDataInput.html#endian" target="">endian</a>
	 *  <br>
	 *  <a href="../../flash/filesystem/FileStream.html" target="">FileStream class</a>
	 *  <br>
	 *  <a href="../../flash/net/Socket.html" target="">Socket class</a>
	 *  <br>
	 *  <a href="../../flash/net/URLStream.html" target="">URLStream class</a>
	 *  <br>
	 *  <a href="ByteArray.html" target="">ByteArray class</a>
	 *  <br>
	 *  <a href="../../flash/errors/EOFError.html" target="">EOFError class</a>
	 * </div><br><hr>
	 */
	public interface IDataInput {

		/**
		 * <p> Returns the number of bytes of data available for reading in the input buffer. User code must call <code>bytesAvailable</code> to ensure that sufficient data is available before trying to read it with one of the read methods. </p>
		 * 
		 * @return 
		 */
		function get bytesAvailable():uint;

		/**
		 * <p> The byte order for the data, either the <code>BIG_ENDIAN</code> or <code>LITTLE_ENDIAN</code> constant from the Endian class. </p>
		 * 
		 * @return 
		 */
		function get endian():String;

		/**
		 * @param value
		 * @return 
		 */
		function set endian(value:String):void;

		/**
		 * <p> Used to determine whether the AMF3 or AMF0 format is used when writing or reading binary data using the <code>readObject()</code> method. The value is a constant from the ObjectEncoding class. </p>
		 * 
		 * @return 
		 */
		function get objectEncoding():uint;

		/**
		 * @param value
		 * @return 
		 */
		function set objectEncoding(value:uint):void;

		/**
		 * <p> Reads a Boolean value from the file stream, byte stream, or byte array. A single byte is read and <code>true</code> is returned if the byte is nonzero, <code>false</code> otherwise. </p>
		 * 
		 * @return  — A Boolean value,  if the byte is nonzero,  otherwise. 
		 */
		function readBoolean():Boolean;

		/**
		 * <p> Reads a signed byte from the file stream, byte stream, or byte array. </p>
		 * 
		 * @return  — The returned value is in the range -128 to 127. 
		 */
		function readByte():int;

		/**
		 * <p> Reads the number of data bytes, specified by the <code>length</code> parameter, from the file stream, byte stream, or byte array. The bytes are read into the ByteArray objected specified by the <code>bytes</code> parameter, starting at the position specified by <code>offset</code>. </p>
		 * 
		 * @param bytes  — The <code>ByteArray</code> object to read data into. 
		 * @param offset  — The offset into the <code>bytes</code> parameter at which data read should begin. 
		 * @param length  — The number of bytes to read. The default value of 0 causes all available data to be read. 
		 */
		function readBytes(bytes:ByteArray, offset:uint = 0, length:uint = 0):void;

		/**
		 * <p> Reads an IEEE 754 double-precision floating point number from the file stream, byte stream, or byte array. </p>
		 * 
		 * @return  — An IEEE 754 double-precision floating point number. 
		 */
		function readDouble():Number;

		/**
		 * <p> Reads an IEEE 754 single-precision floating point number from the file stream, byte stream, or byte array. </p>
		 * 
		 * @return  — An IEEE 754 single-precision floating point number. 
		 */
		function readFloat():Number;

		/**
		 * <p> Reads a signed 32-bit integer from the file stream, byte stream, or byte array. </p>
		 * 
		 * @return  — The returned value is in the range -2147483648 to 2147483647. 
		 */
		function readInt():int;

		/**
		 * <p> Reads a multibyte string of specified length from the file stream, byte stream, or byte array using the specified character set. </p>
		 * 
		 * @param length  — The number of bytes from the byte stream to read. 
		 * @param charSet  — The string denoting the character set to use to interpret the bytes. Possible character set strings include <code>"shift-jis"</code>, <code>"cn-gb"</code>, <code>"iso-8859-1"</code>, and others. For a complete list, see <a href="../../charset-codes.html">Supported Character Sets</a>. <p><b>Note:</b> If the value for the <code>charSet</code> parameter is not recognized by the current system, then <span>Adobe<sup>®</sup> Flash<sup>®</sup> Player or</span> Adobe<sup>®</sup> AIR<sup>®</sup> uses the system's default code page as the character set. For example, a value for the <code>charSet</code> parameter, as in <code>myTest.readMultiByte(22, "iso-8859-01")</code>, that uses <code>01</code> instead of <code>1</code> might work on your development system, but not on another system. On the other system, <span>Flash Player or</span> the AIR runtime will use the system's default code page.</p> 
		 * @return  — UTF-8 encoded string. 
		 */
		function readMultiByte(length:uint, charSet:String):String;

		/**
		 * <p> Reads an object from the file stream, byte stream, or byte array, encoded in AMF serialized format. </p>
		 * 
		 * @return  — The deserialized object 
		 */
		function readObject():*;

		/**
		 * <p> Reads a signed 16-bit integer from the file stream, byte stream, or byte array. </p>
		 * 
		 * @return  — The returned value is in the range -32768 to 32767. 
		 */
		function readShort():int;

		/**
		 * <p> Reads a UTF-8 string from the file stream, byte stream, or byte array. The string is assumed to be prefixed with an unsigned short indicating the length in bytes. </p>
		 * <p>This method is similar to the <code>readUTF()</code> method in the Java<sup>®</sup> IDataInput interface.</p>
		 * 
		 * @return  — A UTF-8 string produced by the byte representation of characters. 
		 */
		function readUTF():String;

		/**
		 * <p> Reads a sequence of UTF-8 bytes from the byte stream or byte array and returns a string. </p>
		 * 
		 * @param length  — The number of bytes to read. 
		 * @return  — A UTF-8 string produced by the byte representation of characters of the specified length. 
		 */
		function readUTFBytes(length:uint):String;

		/**
		 * <p> Reads an unsigned byte from the file stream, byte stream, or byte array. </p>
		 * 
		 * @return  — The returned value is in the range 0 to 255. 
		 */
		function readUnsignedByte():uint;

		/**
		 * <p> Reads an unsigned 32-bit integer from the file stream, byte stream, or byte array. </p>
		 * 
		 * @return  — The returned value is in the range 0 to 4294967295. 
		 */
		function readUnsignedInt():uint;

		/**
		 * <p> Reads an unsigned 16-bit integer from the file stream, byte stream, or byte array. </p>
		 * 
		 * @return  — The returned value is in the range 0 to 65535. 
		 */
		function readUnsignedShort():uint;
	}
}
