package flash.utils {
	import flash.errors.EOFError;
	import flash.net.ObjectEncoding;
	import fr.lenra.playerglobal.net.AMFInterpreter;
	
	/**
	 *  The ByteArray class provides methods and properties to optimize reading, writing, and working with binary data. <p> <i>Note:</i> The ByteArray class is for advanced developers who need to access data on the byte level.</p> <p>In-memory data is a packed array (the most compact representation for the data type) of bytes, but an instance of the ByteArray class can be manipulated with the standard <code>[]</code> (array access) operators. It also can be read and written to as an in-memory file, using methods similar to those in the URLStream and Socket classes.</p> <p>In addition, zlib, deflate, and lzma compression and decompression are supported, as well as Action Message Format (AMF) object serialization.</p> <p>A ByteArray object can share its backing memory among multiple worker instances by setting its <code>shareable</code> property to <code>true</code>.</p> <p>Possible uses of the ByteArray class include the following: </p><ul>
	 *  <li>Creating a custom protocol to connect to a server.</li>
	 *  <li>Writing your own URLEncoder/URLDecoder.</li>
	 *  <li>Writing your own AMF/Remoting packet.</li>
	 *  <li>Optimizing the size of your data by using data types.</li>
	 *  <li>Working with binary data loaded from a file.</li>
	 * </ul>  <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118676a5388-8000.html" target="_blank">Working with byte arrays</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118666ade46-7d54.html" target="_blank">Reading and writing a ByteArray</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118666ade46-7d53.html" target="_blank">ByteArray example: Reading a .zip file</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../operators.html#array_access" target="">[] (array access)</a>
	 *  <br>
	 *  <a href="../../flash/net/Socket.html" target="">Socket class</a>
	 *  <br>
	 *  <a href="../../flash/net/URLStream.html" target="">URLStream class</a>
	 * </div><br><hr>
	 */
	public dynamic class ByteArray extends flash.utils.Proxy implements IDataInput, IDataOutput {
		private static const BUFFER_SIZE:int = 64;
		private static var _defaultObjectEncoding:uint = ObjectEncoding.AMF3;
		
		private var _endian:String = Endian.BIG_ENDIAN;
		private var _length:uint = 0;
		private var _objectEncoding:uint = _defaultObjectEncoding;
		private var _position:uint = 0;
		private var _shareable:Boolean = false;
		private const buffers:Vector.<ArrayBuffer> = new Vector.<ArrayBuffer>();
		private const views:Vector.<Int8Array> = new Vector.<Int8Array>();
		private var amf:AMFInterpreter = null;
		
		/**
		 * <p> Creates a ByteArray instance representing a packed array of bytes, so that you can use the methods and properties in this class to optimize your data storage and stream. </p>
		 */
		public function ByteArray() {
		
		}
		
		/**
		 * <p> The number of bytes of data available for reading from the current position in the byte array to the end of the array. </p>
		 * <p>Use the <code>bytesAvailable</code> property in conjunction with the read methods each time you access a ByteArray object to ensure that you are reading valid data.</p>
		 *
		 * @return
		 */
		public function get bytesAvailable():uint {
			return length - position;
		}
		
		/**
		 * <p> Changes or reads the byte order for the data; either <code>Endian.BIG_ENDIAN</code> or <code>Endian.LITTLE_ENDIAN</code>. The default value is <code>BIG_ENDIAN</code>. </p>
		 *
		 * @return
		 */
		public function get endian():String {
			return _endian;
		}
		
		/**
		 * @param value
		 * @return
		 */
		public function set endian(value:String):void {
			_endian = value;
		}
		
		/**
		 * <p> The length of the ByteArray object, in bytes. </p>
		 * <p>If the length is set to a value that is larger than the current length, the right side of the byte array is filled with zeros.</p>
		 * <p>If the length is set to a value that is smaller than the current length, the byte array is truncated.</p>
		 *
		 * @return
		 */
		public function get length():uint {
			return _length;
		}
		
		/**
		 * @param value
		 * @return
		 */
		public function set length(value:uint):void {
			_length = value;
			var aPos:int = Math.floor(value / BUFFER_SIZE);
			while (aPos >= buffers.length) {
				var b:ArrayBuffer = new ArrayBuffer(BUFFER_SIZE);
				buffers.push(b);
				views.push(new Int8Array(b));
			}
			if (aPos + 1 < buffers.length)
				views.length = buffers.length = aPos + 1;
		}
		
		/**
		 * <p> Used to determine whether the ActionScript 3.0, ActionScript 2.0, or ActionScript 1.0 format should be used when writing to, or reading from, a ByteArray instance. The value is a constant from the ObjectEncoding class. </p>
		 *
		 * @return
		 */
		public function get objectEncoding():uint {
			return _objectEncoding;
		}
		
		/**
		 * @param value
		 * @return
		 */
		public function set objectEncoding(value:uint):void {
			_objectEncoding = value;
		}
		
		/**
		 * <p> Moves, or returns the current position, in bytes, of the file pointer into the ByteArray object. This is the point at which the next call to a read method starts reading or a write method starts writing. </p>
		 *
		 * @return
		 */
		public function get position():uint {
			return _position;
		}
		
		/**
		 * @param value
		 * @return
		 */
		public function set position(value:uint):void {
			_position = value;
		}
		
		/**
		 * <p> Specifies whether the underlying memory of the byte array is <i>shareable</i>. For a shareable byte array, all ByteArray instances in all workers that reference the byte array use the same underlying system memory. The default value is <code>false</code>, indicating that the underlying memory is not shared among workers. </p>
		 * <p>This property also affects what the runtime does with this byte array if it is passed to a worker using the <code>Worker.setSharedProperty()</code> method or the <code>MessageChannel.send()</code> method:</p>
		 * <ul>
		 *  <li><b>Not shareable:</b> If this property is <code>false</code>, when the byte array is passed to a worker the runtime creates a complete copy of the byte array, including allocating a new segment of memory in which to store the duplicate byte array's contents</li>
		 *  <li><b>Shareable:</b> If this property is <code>true</code>, when the byte array is passed to a worker the runtime uses the same underlying memory as the storage buffer for the contents of both the original ByteArray instance and the new ByteArray instance created for the second worker. In essence, both ByteArray instances contain a reference to the same underlying byte array.</li>
		 * </ul>
		 * <p>The ability to access a shared byte array from multiple workers simultaneously can result in an undesirable situation where both workers are manipulating the byte array's underlying memory at the same time. You can use several mechanisms to control access to shared memory:</p>
		 * <ul>
		 *  <li>the compare-and-swap mechanisms provided by the ByteArray class's <code>atomicCompareAndSwapIntAt()</code> and <code>atomicCompareAndSwapLength()</code> methods</li>
		 *  <li>the specialized mechanisms provided by the Mutex and Condition classes (in the flash.concurrent package)</li>
		 * </ul>
		 * <p>Setting this property to <code>true</code> only affects subsequent code that passes this byte array to a worker. Any copy of this byte array that has already been passed to a worker continues to exist as a separate copy.</p>
		 * <p>If you set this property to <code>false</code> when it is previously <code>true</code>, the underlying memory for the byte array is immediately copied into a new segment of system memory. This ByteArray instance then uses the new underlying memory from that point on. Consequently, this byte array's underlying memory is no longer shared with other workers even if it was previously shared. If you subsequently pass this byte array to a worker, its underlying memory is copied as with any ByteArray object whose <code>shareable</code> property is <code>false</code>.</p>
		 * <p> The default value is <code>false.</code></p>
		 *
		 * @return
		 */
		public function get shareable():Boolean {
			return _shareable;
		}
		
		/**
		 * @param value
		 * @return
		 */
		public function set shareable(value:Boolean):void {
			throw new Error("Removed from the version 30 of the Flash Player due to security reason");
			_shareable = value;
		}
		
		private function getByte(pos:uint):int {
			if (pos < 0 || pos >= length)
				return undefined;
			return views[Math.floor(pos / BUFFER_SIZE)][pos % BUFFER_SIZE];
		}
		
		private function setByte(pos:uint, value:int):void {
			if (pos >= length)
				length = pos + 1;
			views[Math.floor(pos / BUFFER_SIZE)][pos % BUFFER_SIZE] = value;
		}
		
		override protected function getProperty(name:*):* {
			if (/^[0-9]+$/.test(name)) {
				return getByte(parseInt(name));
			}
			return super.getProperty(name);
		}
		
		override protected function setProperty(name:*, value:*):void {
			if (/^[0-9]+$/.test(name)) {
				setByte(parseInt(name), parseInt(value));
			}
			super.setProperty(name, value);
		}
		
		/**
		 * <p> In a single atomic operation, compares an integer value in this byte array with another integer value and, if they match, swaps those bytes with another value. </p>
		 * <p>This method is intended to be used with a byte array whose underlying memory is shared between multiple workers (the ByteArray instance's <code>shareable</code> property is <code>true</code>). It performs the following sequence of steps:</p>
		 * <ol>
		 *  <li>Reads an integer value from this byte array starting at the index (measured in bytes) specified in the <code>byteIndex</code> argument</li>
		 *  <li>Compares the actual value from this byte array to the value passed in the <code>expectedValue</code> argument</li>
		 *  <li>If the two values are equal, it writes the value in the <code>newValue</code> argument into the byte array at the location specified by the <code>byteIndex</code> parameter and returns the value that was previously contained in those bytes (the value read in step 1)</li>
		 *  <li>Otherwise, the contents of the byte array are not changed and the method returns the actual value read from the byte array</li>
		 * </ol>
		 * <p>All these steps are performed in one atomic hardware transaction. This guarantees that no operations from other workers make changes to the contents of the byte array during the compare-and-swap operation.</p>
		 *
		 * @param byteIndex  — the index position (in bytes) from which the integer to compare is read, and to which the <code>newValue</code> value is written if the comparison results in a match. This value must be a multiple of 4.
		 * @param expectedValue  — the value that is expected to match the contents of the byte array at the specified index
		 * @param newValue  — the new value that replaces the contents of the byte array at the specified index if the comparison results in a match
		 * @return  — the previous value at the specified location if the comparison results in a match, or the actual value from the byte array if the actual value and expected value don't match
		 */
		public function atomicCompareAndSwapIntAt(byteIndex:int, expectedValue:int, newValue:int):int {
			throw new Error("Not implemented");
		}
		
		/**
		 * <p> In a single atomic operation, compares this byte array's length with a provided value and, if they match, changes the length of this byte array. </p>
		 * <p>This method is intended to be used with a byte array whose underlying memory is shared between multiple workers (the ByteArray instance's <code>shareable</code> property is <code>true</code>). It does the following:</p>
		 * <ol>
		 *  <li>Reads the integer <code>length</code> property of the ByteArray instance</li>
		 *  <li>Compares the length to the value passed in the <code>expectedLength</code> argument</li>
		 *  <li>If the two values are equal, it changes the byte array's length to the value passed as the <code>newLength</code> parameter, either growing or shrinking the size of the byte array</li>
		 *  <li>Otherwise, the byte array is not changed</li>
		 * </ol>
		 * <p>All these steps are performed in one atomic hardware transaction. This guarantees that no operations from other workers make changes to the contents of the byte array during the compare-and-resize operation.</p>
		 *
		 * @param expectedLength  — the expected value of the ByteArray's <code>length</code> property. If the specified value and the actual value match, the byte array's length is changed.
		 * @param newLength  — the new length value for the byte array if the comparison succeeds
		 * @return  — the previous  value of the ByteArray, regardless of whether or not it changed
		 */
		public function atomicCompareAndSwapLength(expectedLength:int, newLength:int):int {
			throw new Error("Not implemented");
		}
		
		/**
		 * <p> Clears the contents of the byte array and resets the <code>length</code> and <code>position</code> properties to 0. Calling this method explicitly frees up the memory used by the ByteArray instance. </p>
		 */
		public function clear():void {
			length = 0;
			position = 0;
		}
		
		/**
		 * <p> Compresses the byte array. The entire byte array is compressed. After the call, the <code>length</code> property of the ByteArray is set to the new length. The <code>position</code> property is set to the end of the byte array. </p>
		 * <p>You specify a compression algorithm by passing a value (defined in the CompressionAlgorithm class) as the <code>algorithm</code> parameter. The supported algorithms include the following: </p>
		 * <p>The zlib compressed data format is described at <a href="http://www.ietf.org/rfc/rfc1950.txt" target="external">http://www.ietf.org/rfc/rfc1950.txt</a>.</p>
		 * <p>The deflate compression algorithm is described at <a href="http://www.ietf.org/rfc/rfc1951.txt" target="external">http://www.ietf.org/rfc/rfc1951.txt</a>.</p>
		 * <p>The lzma compression algorithm is described at <a href="http://www.7-zip.org/7z.html" target="external">http://www.7-zip.org/7z.html</a>.</p>
		 * <p>The deflate compression algorithm is used in several compression formats, such as zlib, gzip, some zip implementations, and others. When data is compressed using one of those compression formats, in addition to storing the compressed version of the original data, the compression format data (for example, the .zip file) includes metadata information. Some examples of the types of metadata included in various file formats are file name, file modification date/time, original file size, optional comments, checksum data, and more.</p>
		 * <p>For example, when a ByteArray is compressed using the zlib algorithm, the resulting ByteArray is structured in a specific format. Certain bytes contain metadata about the compressed data, while other bytes contain the actual compressed version of the original ByteArray data. As defined by the zlib compressed data format specification, those bytes (that is, the portion containing the compressed version of the original data) are compressed using the deflate algorithm. Consequently those bytes are identical to the result of calling <code>compress(<span src="javascript">air.</span>CompressionAlgorithm.DEFLATE)</code> on the original ByteArray. However, the result from <code>compress(<span src="javascript">air.</span>CompressionAlgorithm.ZLIB)</code> includes the extra metadata, while the <code>compress(CompressionAlgorithm.DEFLATE)</code> result includes only the compressed version of the original ByteArray data and nothing else.</p>
		 * <p>In order to use the deflate format to compress a ByteArray instance's data in a specific format such as gzip or zip, you cannot simply call <code>compress(CompressionAlgorithm.DEFLATE)</code>. You must create a ByteArray structured according to the compression format's specification, including the appropriate metadata as well as the compressed data obtained using the deflate format. Likewise, in order to decode data compressed in a format such as gzip or zip, you can't simply call <code>uncompress(CompressionAlgorithm.DEFLATE)</code> on that data. First, you must separate the metadata from the compressed data, and you can then use the deflate format to decompress the compressed data.</p>
		 *
		 * @param algorithm  — The compression algorithm to use when compressing. Valid values are defined as constants in the CompressionAlgorithm class. The default is to use zlib format. Calling <code>compress(CompressionAlgorithm.DEFLATE)</code> has the same effect as calling the <code>deflate()</code> method. <span>Support for the lzma algorithm was added for Flash Player 11.3 and AIR 3.3. You must have those player versions, or later, to use lzma compression.</span>
		 */
		public function compress(algorithm:String):void {
			throw new Error("Not implemented");
		}
		
		/**
		 * <p> Compresses the byte array using the deflate compression algorithm. The entire byte array is compressed. </p>
		 * <p>After the call, the <code>length</code> property of the ByteArray is set to the new length. The <code>position</code> property is set to the end of the byte array.</p>
		 * <p>The deflate compression algorithm is described at <a href="http://www.ietf.org/rfc/rfc1951.txt" target="external">http://www.ietf.org/rfc/rfc1951.txt</a>.</p>
		 * <p>In order to use the deflate format to compress a ByteArray instance's data in a specific format such as gzip or zip, you cannot simply call <code>deflate()</code>. You must create a ByteArray structured according to the compression format's specification, including the appropriate metadata as well as the compressed data obtained using the deflate format. Likewise, in order to decode data compressed in a format such as gzip or zip, you can't simply call <code>inflate()</code> on that data. First, you must separate the metadata from the compressed data, and you can then use the deflate format to decompress the compressed data.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="ByteArray.html#inflate()" target="">inflate()</a>
		 * </div>
		 */
		public function deflate():void {
			throw new Error("Not implemented");
		}
		
		/**
		 * <p> Decompresses the byte array using the deflate compression algorithm. The byte array must have been compressed using the same algorithm. </p>
		 * <p>After the call, the <code>length</code> property of the ByteArray is set to the new length. The <code>position</code> property is set to 0.</p>
		 * <p>The deflate compression algorithm is described at <a href="http://www.ietf.org/rfc/rfc1951.txt" target="external">http://www.ietf.org/rfc/rfc1951.txt</a>.</p>
		 * <p>In order to decode data compressed in a format that uses the deflate compression algorithm, such as data in gzip or zip format, it will not work to simply call <code>inflate()</code> on a ByteArray containing the compression formation data. First, you must separate the metadata that is included as part of the compressed data format from the actual compressed data. For more information, see the <code>compress()</code> method description.</p>
		 */
		public function inflate():void {
			throw new Error("Not implemented");
		}
		
		/**
		 * <p> Reads a Boolean value from the byte stream. A single byte is read, and <code>true</code> is returned if the byte is nonzero, <code>false</code> otherwise. </p>
		 *
		 * @return  — Returns  if the byte is nonzero,  otherwise.
		 */
		public function readBoolean():Boolean {
			return readByte() != 0;
		}
		
		/**
		 * <p> Reads a signed byte from the byte stream. </p>
		 * <p>The returned value is in the range -128 to 127.</p>
		 *
		 * @return  — An integer between -128 and 127.
		 */
		public function readByte():int {
			return getByte(position++);
		}
		
		/**
		 * <p> Reads the number of data bytes, specified by the <code>length</code> parameter, from the byte stream. The bytes are read into the ByteArray object specified by the <code>bytes</code> parameter, and the bytes are written into the destination ByteArray starting at the position specified by <code>offset</code>. </p>
		 *
		 * @param bytes  — The ByteArray object to read data into.
		 * @param offset  — The offset (position) in <code>bytes</code> at which the read data should be written.
		 * @param length  — The number of bytes to read. The default value of 0 causes all available data to be read.
		 */
		public function readBytes(bytes:ByteArray, offset:uint = 0, length:uint = 0):void {
			throw new Error("Not implemented");
		}
		
		private function readData(size:int, readFunctionName:String):* {
			if (position + size >= length)
				throw new EOFError("Error #2030: End of file detected");
			var b:ArrayBuffer = new ArrayBuffer(size);
			var v:DataView = new DataView(b, 0, size);
			for (var i:int = 0; i < size; i++) {
				v.setInt8(i, getByte(position++));
			}
			return v[readFunctionName](0, this.endian == Endian.LITTLE_ENDIAN);
		}
		
		/**
		 * <p> Reads an IEEE 754 double-precision (64-bit) floating-point number from the byte stream. </p>
		 *
		 * @return  — A double-precision (64-bit) floating-point number.
		 */
		public function readDouble():Number {
			return readData(8, "getFloat64");
		}
		
		/**
		 * <p> Reads an IEEE 754 single-precision (32-bit) floating-point number from the byte stream. </p>
		 *
		 * @return  — A single-precision (32-bit) floating-point number.
		 */
		public function readFloat():Number {
			return readData(4, "getFloat32");
		}
		
		/**
		 * <p> Reads a signed 32-bit integer from the byte stream. </p>
		 * <p>The returned value is in the range -2147483648 to 2147483647.</p>
		 *
		 * @return  — A 32-bit signed integer between -2147483648 and 2147483647.
		 */
		public function readInt():int {
			return readData(4, "getInt32");
		}
		
		/**
		 * <p> Reads a multibyte string of specified length from the byte stream using the specified character set. </p>
		 *
		 * @param length  — The number of bytes from the byte stream to read.
		 * @param charSet  — The string denoting the character set to use to interpret the bytes. Possible character set strings include <code>"shift-jis"</code>, <code>"cn-gb"</code>, <code>"iso-8859-1"</code>, and others. For a complete list, see <a href="../../charset-codes.html">Supported Character Sets</a>. <p><b>Note:</b> If the value for the <code>charSet</code> parameter is not recognized by the current system, the application uses the system's default code page as the character set. For example, a value for the <code>charSet</code> parameter, as in <code>myTest.readMultiByte(22, "iso-8859-01")</code> that uses <code>01</code> instead of <code>1</code> might work on your development system, but not on another system. On the other system, the application will use the system's default code page.</p>
		 * @return  — UTF-8 encoded string.
		 */
		public function readMultiByte(length:uint, charSet:String):String {
			throw new Error("Not implemented");
		}
		
		/**
		 * <p> Reads an object from the byte array, encoded in AMF serialized format. </p>
		 *
		 * @return  — The deserialized object.
		 */
		public function readObject():* {
			if (this.amf == null)
				this.amf = new AMFInterpreter(this);
			return this.amf.readObject();
		}
		
		/**
		 * <p> Reads a signed 16-bit integer from the byte stream. </p>
		 * <p>The returned value is in the range -32768 to 32767.</p>
		 *
		 * @return  — A 16-bit signed integer between -32768 and 32767.
		 */
		public function readShort():int {
			return readData(2, "getInt16");
		}
		
		/**
		 * <p> Reads a UTF-8 string from the byte stream. The string is assumed to be prefixed with an unsigned short indicating the length in bytes. </p>
		 *
		 * @return  — UTF-8 encoded string.
		 */
		public function readUTF():String {
			return readUTFBytes(readShort());
		}
		
		/**
		 * <p> Reads a sequence of UTF-8 bytes specified by the <code>length</code> parameter from the byte stream and returns a string. </p>
		 *
		 * @param length  — An unsigned short indicating the length of the UTF-8 bytes.
		 * @return  — A string composed of the UTF-8 bytes of the specified length.
		 */
		public function readUTFBytes(length:uint):String {
			var ret:String = "";
			var char2:int;
			var char3:int;
			while (ret.length < length) {
				var c:int = readByte();
				switch (c >> 4) {
					case 0: 
					case 1: 
					case 2: 
					case 3: 
					case 4: 
					case 5: 
					case 6: 
					case 7: 
						// 0xxxxxxx
						ret += String.fromCharCode(c);
						break;
					case 12: 
					case 13: 
						// 110x xxxx   10xx xxxx
						char2 = readByte();
						ret += String.fromCharCode(((c & 0x1F) << 6) | (char2 & 0x3F));
						break;
					case 14: 
						// 1110 xxxx  10xx xxxx  10xx xxxx
						char2 = readByte();
						char3 = readByte();
						ret += String.fromCharCode(((c & 0x0F) << 12) | ((char2 & 0x3F) << 6) | ((char3 & 0x3F) << 0));
						break;
				}
			}
			return ret;
		}
		
		/**
		 * <p> Reads an unsigned byte from the byte stream. </p>
		 * <p>The returned value is in the range 0 to 255. </p>
		 *
		 * @return  — A 32-bit unsigned integer between 0 and 255.
		 */
		public function readUnsignedByte():uint {
			return readByte();
		}
		
		/**
		 * <p> Reads an unsigned 32-bit integer from the byte stream. </p>
		 * <p>The returned value is in the range 0 to 4294967295. </p>
		 *
		 * @return  — A 32-bit unsigned integer between 0 and 4294967295.
		 */
		public function readUnsignedInt():uint {
			return readInt();
		}
		
		/**
		 * <p> Reads an unsigned 16-bit integer from the byte stream. </p>
		 * <p>The returned value is in the range 0 to 65535. </p>
		 *
		 * @return  — A 16-bit unsigned integer between 0 and 65535.
		 */
		public function readUnsignedShort():uint {
			return readShort();
		}
		
		/**
		 * <p> Provides an overridable method for customizing the JSON encoding of values in an ByteArray object. </p>
		 * <p>The <code>JSON.stringify()</code> method looks for a <code>toJSON()</code> method on each object that it traverses. If the <code>toJSON()</code> method is found, <code>JSON.stringify()</code> calls it for each value it encounters, passing in the key that is paired with the value.</p>
		 * <p>ByteArray provides a default implementation of <code>toJSON()</code> that simply returns the name of the class. Because the content of any ByteArray requires interpretation, clients that wish to export ByteArray objects to JSON must provide their own implementation. You can do so by redefining the <code>toJSON()</code> method on the class prototype. </p>
		 * <p> The <code>toJSON()</code> method can return a value of any type. If it returns an object, <code>stringify()</code> recurses into that object. If <code>toJSON()</code> returns a string, <code>stringify()</code> does not recurse and continues its traversal.</p>
		 *
		 * @param k  — The key of a key/value pair that <code>JSON.stringify()</code> has encountered in its traversal of this object
		 * @return  — The class name string.
		 */
		/*public function toJSON(k:String):* {
		   throw new Error("Not implemented");
		   }*/
		
		/**
		 * <p> Converts the byte array to a string. If the data in the array begins with a Unicode byte order mark, the application will honor that mark when converting to a string. If <code>System.useCodePage</code> is set to <code>true</code>, the application will treat the data in the array as being in the current system code page when converting. </p>
		 *
		 * @return  — The string representation of the byte array.
		 */
		public function toString():String {
			throw new Error("Not implemented");
		}
		
		/**
		 * <p> Decompresses the byte array. After the call, the <code>length</code> property of the ByteArray is set to the new length. The <code>position</code> property is set to 0. </p>
		 * <p>The byte array must have been compressed using the same algorithm as the uncompress. You specify an uncompression algorithm by passing a value (defined in the CompressionAlgorithm class) as the <code>algorithm</code> parameter. The supported algorithms include the following: </p>
		 * <p>The zlib compressed data format is described at <a href="http://www.ietf.org/rfc/rfc1950.txt" target="external">http://www.ietf.org/rfc/rfc1950.txt</a>.</p>
		 * <p>The deflate compression algorithm is described at <a href="http://www.ietf.org/rfc/rfc1951.txt" target="external">http://www.ietf.org/rfc/rfc1951.txt</a>.</p>
		 * <p>The lzma compression algorithm is described at <a href="http://www.7-zip.org/7z.html" target="external">http://www.7-zip.org/7z.html</a>.</p>
		 * <p>In order to decode data compressed in a format that uses the deflate compression algorithm, such as data in gzip or zip format, it will not work to call <code>uncompress(CompressionAlgorithm.DEFLATE)</code> on a ByteArray containing the compression formation data. First, you must separate the metadata that is included as part of the compressed data format from the actual compressed data. For more information, see the <code>compress()</code> method description.</p>
		 *
		 * @param algorithm  — The compression algorithm to use when decompressing. This must be the same compression algorithm used to compress the data. Valid values are defined as constants in the CompressionAlgorithm class. The default is to use zlib format. <span>Support for the lzma algorithm was added for Flash Player 11.3 and AIR 3.3. You must have those player versions, or later, to use lzma.</span>
		 */
		public function uncompress(algorithm:String):void {
			throw new Error("Not implemented");
		}
		
		/**
		 * <p> Writes a Boolean value. A single byte is written according to the <code>value</code> parameter, either 1 if <code>true</code> or 0 if <code>false</code>. </p>
		 *
		 * @param value  — A Boolean value determining which byte is written. If the parameter is <code>true</code>, the method writes a 1; if <code>false</code>, the method writes a 0.
		 */
		public function writeBoolean(value:Boolean):void {
			writeByte(value ? 1 : 0);
		}
		
		/**
		 * <p> Writes a byte to the byte stream. </p>
		 * <p>The low 8 bits of the parameter are used. The high 24 bits are ignored. </p>
		 *
		 * @param value  — A 32-bit integer. The low 8 bits are written to the byte stream.
		 */
		public function writeByte(value:int):void {
			setByte(position++, value);
		}
		
		/**
		 * <p> Writes a sequence of <code>length</code> bytes from the specified byte array, <code>bytes</code>, starting <code>offset</code>(zero-based index) bytes into the byte stream. </p>
		 * <p>If the <code>length</code> parameter is omitted, the default length of 0 is used; the method writes the entire buffer starting at <code>offset</code>. If the <code>offset</code> parameter is also omitted, the entire buffer is written. </p>
		 * <p>If <code>offset</code> or <code>length</code> is out of range, they are clamped to the beginning and end of the <code>bytes</code> array.</p>
		 *
		 * @param bytes  — The ByteArray object.
		 * @param offset  — A zero-based index indicating the position into the array to begin writing.
		 * @param length  — An unsigned integer indicating how far into the buffer to write.
		 */
		public function writeBytes(bytes:ByteArray, offset:uint = 0, length:uint = 0):void {
			if (length==0)
				length = bytes.length-offset;
			for(var i:int = 0; i < length; i++) {
				this.writeByte(bytes[i]);
			}
		}
		
		private function writeData(size:int, writeFunctionName:String, value:*):void {
			var b:ArrayBuffer = new ArrayBuffer(size);
			var v:DataView = new DataView(b, 0, size);
			v[writeFunctionName](0, value, this.endian == Endian.LITTLE_ENDIAN);
			for (var i:int = 0; i < size; i++) {
				writeByte(v.getInt8(i));
			}
		}
		
		/**
		 * <p> Writes an IEEE 754 double-precision (64-bit) floating-point number to the byte stream. </p>
		 *
		 * @param value  — A double-precision (64-bit) floating-point number.
		 */
		public function writeDouble(value:Number):void {
			writeData(8, "setFloat64", value);
		}
		
		/**
		 * <p> Writes an IEEE 754 single-precision (32-bit) floating-point number to the byte stream. </p>
		 *
		 * @param value  — A single-precision (32-bit) floating-point number.
		 */
		public function writeFloat(value:Number):void {
			writeData(4, "setFloat32", value);
		}
		
		/**
		 * <p> Writes a 32-bit signed integer to the byte stream. </p>
		 *
		 * @param value  — An integer to write to the byte stream.
		 */
		public function writeInt(value:int):void {
			writeData(4, "setInt32", value);
		}
		
		/**
		 * <p> Writes a multibyte string to the byte stream using the specified character set. </p>
		 *
		 * @param value  — The string value to be written.
		 * @param charSet  — The string denoting the character set to use. Possible character set strings include <code>"shift-jis"</code>, <code>"cn-gb"</code>, <code>"iso-8859-1"</code>, and others. For a complete list, see <a href="../../charset-codes.html">Supported Character Sets</a>.
		 */
		public function writeMultiByte(value:String, charSet:String):void {
			throw new Error("Not implemented");
		}
		
		/**
		 * <p> Writes an object into the byte array in AMF serialized format. </p>
		 *
		 * @param object  — The object to serialize.
		 */
		public function writeObject(object:*):void {
			if (this.amf == null)
				this.amf = new AMFInterpreter(this);
			this.amf.writeObject(object);
		}
		
		/**
		 * <p> Writes a 16-bit integer to the byte stream. The low 16 bits of the parameter are used. The high 16 bits are ignored. </p>
		 *
		 * @param value  — 32-bit integer, whose low 16 bits are written to the byte stream.
		 */
		public function writeShort(value:int):void {
			writeData(2, "setInt16", value);
		}
		
		/**
		 * <p> Writes a UTF-8 string to the byte stream. The length of the UTF-8 string in bytes is written first, as a 16-bit integer, followed by the bytes representing the characters of the string. </p>
		 *
		 * @param value  — The string value to be written.
		 */
		public function writeUTF(value:String):void {
			var initPos:uint = position;
			writeShort(0);
			writeUTFBytes(value);
			var len:uint = position - initPos - 2;
			var pos:uint = position;
			position = initPos;
			writeShort(len);
			position = pos;
		}
		
		/**
		 * <p> Writes a UTF-8 string to the byte stream. Similar to the <code>writeUTF()</code> method, but <code>writeUTFBytes()</code> does not prefix the string with a 16-bit length word. </p>
		 *
		 * @param value  — The string value to be written.
		 */
		public function writeUTFBytes(value:String):void {
			var len:int = value.length;
			for (var i:int = 0; i < len; i++) {
				var c:int = value.charCodeAt(i);
				if (c <= 0x007F) {
					writeByte(c);
				}
				else if (c <= 0x07FF) {
					writeByte(0xC0 | ((c >> 6) & 0x1F));
					writeByte(0x80 | ((c >> 0) & 0x3F));
				}
				else {
					writeByte(0xE0 | ((c >> 12) & 0x0F));
					writeByte(0x80 | ((c >> 6) & 0x3F));
					writeByte(0x80 | ((c >> 0) & 0x3F));
				}
			}
		}
		
		/**
		 * <p> Writes a 32-bit unsigned integer to the byte stream. </p>
		 *
		 * @param value  — An unsigned integer to write to the byte stream.
		 */
		public function writeUnsignedInt(value:uint):void {
			writeInt(value);
		}

		/**
		 * Converts the current ByteArray to ArrayBuffer(JS)
		 * @return An ArrayBuffer(JS) with the current ByteArray data
		 */
		public function toArrayBuffer():ArrayBuffer {
			var len:uint = this.length;
			var ret:ArrayBuffer = new ArrayBuffer(len);
			var v:Int8Array = new Int8Array(ret);
			for(var i:int = 0; i < len; i++) {
				v[i] = this.getByte(i);
			}
			return ret;
		}

		/**
		 * Sets the ByteArray data from an ArrayBuffer(JS)
		 * @param buf The array buffer with the data of the ByteArray
		 */
		public function fromArrayBuffer(buf:ArrayBuffer):void {
			if (this.length>0)
				throw new Error("This ByteArray is not empty");
			bufferData(buf);
		}
		
		/**
		 * Adds the ArrayBuffer(JS) data at the end of the current ByteArray
		 * @param buf The array buffer with the data
		 */
		public function bufferData(buf:ArrayBuffer):void {
			if (this.length>0)
				throw new Error("This ByteArray is not empty");
			var len:uint = buf.byteLength;
			var start:uint = this.length;
			this.length = start + len;
			var v:Int8Array = new Int8Array(buf);
			for(var i:int = 0; i < len; i++) {
				this.setByte(start + i, v[i]);
			}
		}
		
		/**
		 * <p> Denotes the default object encoding for the ByteArray class to use for a new ByteArray instance. When you create a new ByteArray instance, the encoding on that instance starts with the value of <code>defaultObjectEncoding</code>. The <code>defaultObjectEncoding</code> property is initialized to <code>ObjectEncoding.AMF3</code>. </p>
		 * <p>When an object is written to or read from binary data, the <code>objectEncoding</code> value is used to determine whether the ActionScript 3.0, ActionScript2.0, or ActionScript 1.0 format should be used. The value is a constant from the ObjectEncoding class.</p>
		 *
		 * @return
		 */
		public static function get defaultObjectEncoding():uint {
			return _defaultObjectEncoding;
		}
		
		/**
		 * @param value
		 * @return
		 */
		public static function set defaultObjectEncoding(value:uint):void {
			_defaultObjectEncoding = value;
		}
	}
}