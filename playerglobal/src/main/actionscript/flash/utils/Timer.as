package flash.utils {
	import flash.events.EventDispatcher;
	import flash.events.TimerEvent;

	[Event(name="timer", type="flash.events.TimerEvent")]
	[Event(name="timerComplete", type="flash.events.TimerEvent")]
	/**
	 *  The Timer class is the interface to timers, which let you run code on a specified time sequence. Use the <code>start()</code> method to start a timer. Add an event listener for the <code>timer</code> event to set up code to be run on the timer interval. <p>You can create Timer objects to run once or repeat at specified intervals to execute code on a schedule. <span>Depending on the SWF file's framerate or the runtime environment (available memory and other factors), the runtime may dispatch events at slightly offset intervals. For example, if a SWF file is set to play at 10 frames per second (fps), which is 100 millisecond intervals, but your timer is set to fire an event at 80 milliseconds, the event will be dispatched close to the 100 millisecond interval.</span> Memory-intensive scripts may also offset the events.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7f0a.html" target="_blank">Performing date and time arithmetic</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7f09.html" target="_blank">Converting between time zones</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7f0f.html" target="_blank">Controlling time intervals</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7f0e.html" target="_blank">Date and time example: Simple analog clock</a>
	 * </div><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e52.html" target="_blank">Working with dates and times</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7f07.html" target="_blank">The Timer class</a>
	 *  <br>
	 * </div><br><hr>
	 */
	public class Timer extends EventDispatcher {
		private var _delay:Number;
		private var _repeatCount:int;

		private var _currentCount:int;
		
		private var intervalId:* = null;

		/**
		 * <p> Constructs a new Timer object with the specified <code>delay</code> and <code>repeatCount</code> states. </p>
		 * <p>The timer does not start automatically; you must call the <code>start()</code> method to start it.</p>
		 * 
		 * @param delay  — The delay between timer events, in milliseconds. A <code>delay</code> lower than 20 milliseconds is not recommended. Timer frequency is limited to 60 frames per second, meaning a delay lower than 16.6 milliseconds causes runtime problems. 
		 * @param repeatCount  — Specifies the number of repetitions. If zero, the timer repeats indefinitely, up to a maximum of 24.86 days (int.MAX_VALUE + 1). If nonzero, the timer runs the specified number of times and then stops. 
		 */
		public function Timer(delay:Number, repeatCount:int = 0) {
			super(this);
			this.delay = delay;
			this.repeatCount = repeatCount;
		}

		/**
		 * <p> The total number of times the timer has fired since it started at zero. If the timer has been reset, only the fires since the reset are counted. </p>
		 * 
		 * @return 
		 */
		public function get currentCount():int {
			return _currentCount;
		}

		/**
		 * <p> The delay, in milliseconds, between timer events. If you set the delay interval while the timer is running, the timer will restart at the same <code>repeatCount</code> iteration. </p>
		 * <p><b>Note:</b> A <code>delay</code> lower than 20 milliseconds is not recommended. Timer frequency is limited to 60 frames per second, meaning a delay lower than 16.6 milliseconds causes runtime problems.</p>
		 * 
		 * @return 
		 */
		public function get delay():Number {
			return _delay;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set delay(value:Number):void {
			_delay = value;
			if (running) {
				stop();
				start();
			}
		}

		/**
		 * <p> The total number of times the timer is set to run. If the repeat count is set to 0, the timer continues indefinitely, up to a maximum of 24.86 days, or until the <code>stop()</code> method is invoked or the program stops. If the repeat count is nonzero, the timer runs the specified number of times. If <code>repeatCount</code> is set to a total that is the same or less then <code>currentCount</code> the timer stops and will not fire again. </p>
		 * 
		 * @return 
		 */
		public function get repeatCount():int {
			return _repeatCount;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set repeatCount(value:int):void {
			_repeatCount = value;
			if (running) {
				stop();
				start();
			}
		}

		/**
		 * <p> The timer's current state; <code>true</code> if the timer is running, otherwise <code>false</code>. </p>
		 * 
		 * @return 
		 */
		public function get running():Boolean {
			return intervalId!=null;
		}

		/**
		 * <p> Stops the timer, if it is running, and sets the <code>currentCount</code> property back to 0, like the reset button of a stopwatch. Then, when <code>start()</code> is called, the timer instance runs for the specified number of repetitions, as set by the <code>repeatCount</code> value. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Timer.html#stop()" target="">Timer.stop()</a>
		 * </div>
		 */
		public function reset():void {
			stop();
			_currentCount = 0;
		}

		/**
		 * <p> Starts the timer, if it is not already running. </p>
		 */
		public function start():void {
			if (!running && (repeatCount==0 || currentCount < repeatCount))
				intervalId = setInterval(tick, delay);
		}

		/**
		 * <p> Stops the timer. When <code>start()</code> is called after <code>stop()</code>, the timer instance runs for the <i>remaining</i> number of repetitions, as set by the <code>repeatCount</code> property. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Timer.html#reset()" target="">Timer.reset()</a>
		 * </div>
		 */
		public function stop():void {
			if (running) {
				clearInterval(intervalId);
				intervalId = null;
			}
		}
		
		private function tick():void {
			++_currentCount;
			this.dispatchEvent(new TimerEvent(TimerEvent.TIMER));
			if (_currentCount == repeatCount) {
				stop();
				this.dispatchEvent(new TimerEvent(TimerEvent.TIMER_COMPLETE));
			}
		}
	}
}
