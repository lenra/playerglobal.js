
package flash.utils {
	
	public class Proxy {
		
		/*flash_proxy*/protected function callProperty (name:*, ...rest):* {
			return this[name].apply(this, rest);
		}

		/*flash_proxy*/protected function deleteProperty (name:*):Boolean {
			throw new Error("Not implemented");
		}

		/*flash_proxy*/protected function getDescendants (name:*):* {
			throw new Error("Not implemented");
		}

		/*flash_proxy*/protected function getProperty (name:*):* {
			return this[name];
		}

		/*flash_proxy*/protected function hasProperty (name:*):Boolean {
			return name in this;
		}

		/*flash_proxy*/protected function isAttribute (name:*):Boolean {
			throw new Error("Not implemented");
		}

		/*flash_proxy*/protected function nextName (index:int):String {
			throw new Error("Not implemented");
		}

		/*flash_proxy*/protected function nextNameIndex (index:int):int {
			throw new Error("Not implemented");
		}

		/*flash_proxy*/protected function nextValue (index:int):* {
			throw new Error("Not implemented");
		}

		/*flash_proxy*/protected function setProperty (name:*, value:*):void {
			this[name] = value;
		}
	}
}
