package flash.utils {
	/**
	 *  The Dictionary class lets you create a dynamic collection of properties, which uses strict equality (<code>===</code>) for key comparison. When an object is used as a key, the object's identity is used to look up the object, and not the value returned from calling <code>toString()</code> on it. <p> <b>Note: </b>You cannot use a QName object as a Dictionary key. </p> <p>The following statements show the relationship between a Dictionary object and a key object:</p> <pre>
	 *  var dict = new Dictionary();
	 *  var obj = new Object();
	 *  var key:Object = new Object();
	 *  key.toString = function() { return "key" }
	 *  
	 *  dict[key] = "Letters";
	 *  obj["key"] = "Letters";
	 *  
	 *  dict[key] == "Letters"; // true
	 *  obj["key"] == "Letters"; // true
	 *  obj[key] == "Letters"; // true because key == "key" is true b/c key.toString == "key"
	 *  dict["key"] == "Letters"; // false because "key" === key is false
	 *  delete dict[key]; //removes the key
	 *  </pre> <p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7eea.html" target="_blank">Associative arrays</a>
	 *  <br>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../operators.html#strict_equality" target="">=== (strict equality)</a>
	 * </div><br><hr>
	 */
	public class Dictionary extends flash.utils.Proxy {
		private const keys:Array = [];
		private const values:Array = [];
		//private var weakKeys:Boolean;
		/**
		 * <p> Creates a new Dictionary object. To remove a key from a Dictionary object, use the <code>delete</code> operator. </p>
		 * 
		 * @param weakKeys  — Instructs the Dictionary object to use "weak" references on object keys. If the only reference to an object is in the specified Dictionary object, the key is eligible for garbage collection and is removed from the table when the object is collected. Note that the Dictionary never removes weak <code>String</code> keys from the table. Specifically in the case of <code>String</code> keys, the weak reference will never get removed from the key table, and the Dictionary will keep holding a strong reference to the respective values. 
		 */
		public function Dictionary(weakKeys:Boolean = false) {
			//this.weakKeys = weakKeys;
		}

		override protected function setProperty (name:*, value:*):void {
			console.log("setProperty : ", name, value);
			var ind:int = keys.indexOf(name);
			if (ind==-1) {
				ind = keys.length;
				keys.push(name);
			}
			values[ind] = value;
		}

		/**
		 * <p> Provides an overridable method for customizing the JSON encoding of values in an Dictionary object. </p>
		 * <p>The <code>JSON.stringify()</code> method looks for a <code>toJSON()</code> method on each object that it traverses. If the <code>toJSON()</code> method is found, <code>JSON.stringify()</code> calls it for each value it encounters, passing in the key that is paired with the value.</p>
		 * <p>Dictionary provides a default implementation of <code>toJSON()</code> that simply returns the name of the class. Clients that wish to export Dictionary objects to JSON must provide their own <code>toJSON()</code> implementations. You can do so by redefining the <code>toJSON()</code> method on the class prototype. </p>
		 * <p> The <code>toJSON()</code> method can return a value of any type. If it returns an object, <code>stringify()</code> recurses into that object. If <code>toJSON()</code> returns a string, <code>stringify()</code> does not recurse and continues its traversal.</p>
		 * 
		 * @param k  — The key of a key/value pair that <code>JSON.stringify()</code> has encountered in its traversal of this object 
		 * @return  — The class name string. 
		 */
		/*public function toJSON(k:String):* {
			throw new Error("Not implemented");
		}*/
	}
}
