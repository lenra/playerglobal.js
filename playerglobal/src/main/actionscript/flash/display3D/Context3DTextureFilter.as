package flash.display3D {
	/**
	 *  Defines the values to use for sampler filter mode. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Context3D.html#setSamplerStateAt()" target="">flash.display3D.Context3D.setSamplerStateAt()</a>
	 * </div><br><hr>
	 */
	public class Context3DTextureFilter {
		/**
		 * <p> Use anisotropic filter with radio 16 when upsampling textures </p>
		 */
		public static const ANISOTROPIC16X:String = "anisotropic16x";
		/**
		 * <p> Use anisotropic filter with radio 2 when upsampling textures </p>
		 */
		public static const ANISOTROPIC2X:String = "anisotropic2x";
		/**
		 * <p> Use anisotropic filter with radio 4 when upsampling textures </p>
		 */
		public static const ANISOTROPIC4X:String = "anisotropic4x";
		/**
		 * <p> Use anisotropic filter with radio 8 when upsampling textures </p>
		 */
		public static const ANISOTROPIC8X:String = "anisotropic8x";
		/**
		 * <p> Use linear interpolation when upsampling textures (gives a smooth, blurry look). </p>
		 */
		public static const LINEAR:String = "linear";
		/**
		 * <p> Use nearest neighbor sampling when upsampling textures (gives a pixelated, sharp mosaic look). </p>
		 */
		public static const NEAREST:String = "nearest";
	}
}
