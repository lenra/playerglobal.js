package flash.display3D {
	/**
	 *  Defines the values to use for specifying vertex buffers. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Context3D.html#setVertexBufferAt()" target="">Context3D.setVertexBufferAt()</a>
	 * </div><br><hr>
	 */
	public class Context3DVertexBufferFormat {
		/**
		 * <p> </p>
		 */
		public static const BYTES_4:String = "bytes4";
		/**
		 * <p> </p>
		 */
		public static const FLOAT_1:String = "float1";
		/**
		 * <p> </p>
		 */
		public static const FLOAT_2:String = "float2";
		/**
		 * <p> </p>
		 */
		public static const FLOAT_3:String = "float3";
		/**
		 * <p> </p>
		 */
		public static const FLOAT_4:String = "float4";
	}
}
