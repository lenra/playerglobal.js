package flash.display3D.textures {
	import flash.events.EventDispatcher;

	/**
	 *  The TextureBase class is the base class for Context3D texture objects. <p> <b>Note:</b> You cannot create your own texture classes using TextureBase. To add functionality to a texture class, extend either Texture or CubeTexture instead.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Texture.html" target="">Texture</a>
	 *  <br>
	 *  <a href="CubeTexture.html" target="">CubeTexture</a>
	 * </div><br><hr>
	 */
	public class TextureBase extends EventDispatcher {

		/**
		 * <p> Frees all GPU resources associated with this texture. After disposal, calling upload() or rendering with this object fails. </p>
		 */
		public function dispose():void {
			throw new Error("Not implemented");
		}
	}
}
