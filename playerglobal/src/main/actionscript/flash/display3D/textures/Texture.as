package flash.display3D.textures {
	import flash.display.BitmapData;
	import flash.utils.ByteArray;

	/**
	 *  The Texture class represents a 2-dimensional texture uploaded to a rendering context. <p>Defines a 2D texture for use during rendering.</p> <p>Texture cannot be instantiated directly. Create instances by using Context3D <code>createTexture()</code> method.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flash/display3D/Context3D.html#createTexture()" target="">flash.display3D.Context3D.createTexture()</a>
	 *  <br>
	 *  <a href="../../../flash/display3D/Context3D.html#setTextureAt()" target="">flash.display3D.Context3D.setTextureAt()</a>
	 * </div><br><hr>
	 */
	public class Texture extends TextureBase {

		/**
		 * <p> Uploads a compressed texture in Adobe Texture Format (ATF) from a ByteArray object. ATF file version 2 requires SWF version 21 or newer and ATF file version 3 requires SWF version 29 or newer. For ATF files created with png image without alpha the <code>format</code> string given during <code>Context3DObject::createTexture</code> should be "COMPRESSED" and for ATF files created with png image with alpha the <code>format</code> string given during <code>Context3DObject::createTexture</code> should be "COMPRESSED_ALPHA". </p>
		 * 
		 * @param data  — a byte array that contains a compressed texture including mipmaps. The ByteArray object must use the little endian format. 
		 * @param byteArrayOffset  — the position in the byte array at which to start reading the texture data. 
		 * @param async  — If true, this function returns immediately. Any draw method which attempts to use the texture will fail until the upload completes successfully. Upon successful upload, this <code>CubeTexture</code> object dispatches <code>Event.TEXTURE_READY</code>. Default value: false. 
		 */
		public function uploadCompressedTextureFromByteArray(data:ByteArray, byteArrayOffset:uint, async:Boolean = false):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Uploads a texture from a BitmapData object. </p>
		 * 
		 * @param source  — a bitmap. 
		 * @param miplevel  — the mip level to be loaded, level zero being the top-level full-resolution image. 
		 */
		public function uploadFromBitmapData(source:BitmapData, miplevel:uint = 0):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Uploads miplevel 0 for a texture from a BitmapData object asynchronously. Any draw method which attempts to use the texture fails until the upload completes successfully. Upon successful upload, this <code>Texture</code> object dispatches <code>Event.TEXTURE_READY</code>. Event.TEXTURE_READY is a callback to indicate that the asynchronous call received for the texture object have been executed successfully. Upon any error during the background upload , this <code>Texture</code> object dispatches <code>Event.ERROREVENT</code>. </p>
		 * 
		 * @param source  — a bitmap 
		 * @param miplevel
		 */
		public function uploadFromBitmapDataAsync(source:BitmapData, miplevel:uint = 0):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Uploads a texture from a ByteArray. </p>
		 * 
		 * @param data  — a byte array that is contains enough bytes in the textures internal format to fill the texture. rgba textures are read as bytes per texel component (1 or 4). float textures are read as floats per texel component (1 or 4). The ByteArray object must use the little endian format. 
		 * @param byteArrayOffset  — the position in the byte array object at which to start reading the texture data. 
		 * @param miplevel  — the mip level to be loaded, level zero is the top-level, full-resolution image. 
		 */
		public function uploadFromByteArray(data:ByteArray, byteArrayOffset:uint, miplevel:uint = 0):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Uploads miplevel 0 for a texture from a ByteArray asynchronously. Any draw method which attempts to use the texture fails until the upload completes successfully. Upon successful upload, this <code>Texture</code> object dispatches <code>Event.TEXTURE_READY</code>. Event.TEXTURE_READY is a callback to indicate that the asynchronous calls received for the texture object have been executed successfully. Upon any error during the background upload, this <code>Texture</code> object dispatches <code>Event.ERROREVENT</code>. </p>
		 * 
		 * @param data  — a byte array that contains enough bytes in the textures internal format to fill the texture. rgba textures are read as bytes per texel component (1 or 4). float textures are read as floats per texel component (1 or 4). The ByteArray object must use the little endian format. 
		 * @param byteArrayOffset  — the position in the byte array object at which to start reading the texture data. 
		 * @param miplevel
		 */
		public function uploadFromByteArrayAsync(data:ByteArray, byteArrayOffset:uint, miplevel:uint = 0):void {
			throw new Error("Not implemented");
		}
	}
}
