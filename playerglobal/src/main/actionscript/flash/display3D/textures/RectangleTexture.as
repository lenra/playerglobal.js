package flash.display3D.textures {
	import flash.display.BitmapData;
	import flash.utils.ByteArray;

	/**
	 *  The Rectangle Texture class represents a 2-dimensional texture uploaded to a rendering context. <p>Defines a 2D texture for use during rendering.</p> <p>Texture cannot be instantiated directly. Create instances by using Context3D <code>createRectangleTexture()</code> method.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flash/display3D/Context3D.html#createRectangleTexture()" target="">flash.display3D.Context3D.createRectangleTexture()</a>
	 *  <br>
	 *  <a href="../../../flash/display3D/Context3D.html#setTextureAt()" target="">flash.display3D.Context3D.setTextureAt()</a>
	 * </div><br><hr>
	 */
	public class RectangleTexture extends TextureBase {

		/**
		 * <p> Uploads a texture from a BitmapData object. </p>
		 * 
		 * @param source  — a bitmap. 
		 */
		public function uploadFromBitmapData(source:BitmapData):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Uploads a texture from a BitmapData object. </p>
		 * 
		 * @param source  — a bitmap. This function asynchronously uploads texture data. Any draw method which attempts to use the texture will fail until the upload completes successfully. Upon successful upload, this <code>Texture</code> object dispatches <code>Event.TEXTURE_UPLOADED</code>. Event.TEXTURE_READY is a callback to indicate that the asynchronous call received for the texture object have been executed successfully. Upon any error during the background upload , this <code>Texture</code> object dispatches <code>Event.ERROREVENT</code>. 
		 */
		public function uploadFromBitmapDataAsync(source:BitmapData):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Uploads a texture from a ByteArray. </p>
		 * 
		 * @param data  — a byte array that is contains enough bytes in the textures internal format to fill the texture. rgba textures are read as bytes per texel component (1 or 4). float textures are read as floats per texel component (1 or 4). The ByteArray object must use the little endian format. 
		 * @param byteArrayOffset  — the position in the byte array object at which to start reading the texture data. 
		 */
		public function uploadFromByteArray(data:ByteArray, byteArrayOffset:uint):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Uploads a texture from a ByteArray. </p>
		 * 
		 * @param data  — a byte array that is contains enough bytes in the textures internal format to fill the texture. rgba textures are read as bytes per texel component (1 or 4). float textures are read as floats per texel component (1 or 4). The ByteArray object must use the little endian format. 
		 * @param byteArrayOffset  — the position in the byte array object at which to start reading the texture data. This function asynchronously uploads texture data. Any draw method which attempts to use the texture will fail until the upload completes successfully. Upon successful upload, this <code>Texture</code> object dispatches <code>Event.TEXTURE_UPLOADED</code>. Event.TEXTURE_READY is a callback to indicate that the asynchronous call received for the texture object have been executed successfully. Upon any error during the background upload , this <code>Texture</code> object dispatches <code>Event.ERROREVENT</code>. 
		 */
		public function uploadFromByteArrayAsync(data:ByteArray, byteArrayOffset:uint):void {
			throw new Error("Not implemented");
		}
	}
}
