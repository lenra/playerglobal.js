package flash.display3D.textures {
	import flash.display.BitmapData;
	import flash.utils.ByteArray;

	/**
	 *  The CubeTexture class represents a cube texture uploaded to a rendering context. <p>Defines a cube map texture for use during rendering. Cube mapping is used for many rendering techniques, such as environment maps, skyboxes, and skylight illumination.</p> <p>You cannot create a CubeTexture object directly; use the Context3D <code>createCubeTexture()</code> instead.</p> <p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://en.wikipedia.org/wiki/Cube_mapping" target="_blank">http://en.wikipedia.org/wiki/Cube_mapping</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../../flash/display3D/Context3D.html#createCubeTexture()" target="">flash.display3D.Context3D.createCubeTexture()</a>
	 *  <br>
	 *  <a href="../../../flash/display3D/Context3D.html#setTextureAt()" target="">flash.display3D.Context3D.setTextureAt()</a>
	 * </div><br><hr>
	 */
	public class CubeTexture extends TextureBase {

		/**
		 * <p> Uploads a cube texture in Adobe Texture Format (ATF) from a byte array. </p>
		 * <p>The byte array must contain all faces and mipmaps for the texture.</p>
		 * 
		 * @param data  — a byte array that containing a compressed cube texture including mipmaps. The ByteArray object must use the little endian format. 
		 * @param byteArrayOffset  — an optional offset at which to start reading the texture data. 
		 * @param async  — If true, this function returns immediately. Any draw method which attempts to use the texture will fail until the upload completes successfully. Upon successful upload, this <code>Texture</code> object dispatches <code>Event.TEXTURE_READY</code>. Default value: false. 
		 */
		public function uploadCompressedTextureFromByteArray(data:ByteArray, byteArrayOffset:uint, async:Boolean = false):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Uploads a component of a cube map texture from a BitmapData object. </p>
		 * <p>This function uploads one mip level of one side of the cube map. Call <code>uploadFromBitmapData()</code> as necessary to upload each mip level and face of the cube map.</p>
		 * 
		 * @param source  — a bitmap. 
		 * @param side  — A code indicating which side of the cube to upload: <p></p><ul>
		 *  positive X : 0
		 * </ul> <ul>
		 *  negative X : 1
		 * </ul> <ul>
		 *  positive Y : 2
		 * </ul> <ul>
		 *  negative Y : 3
		 * </ul> <ul>
		 *  positive Z : 4
		 * </ul> <ul>
		 *  negative Z : 5
		 * </ul><p></p> 
		 * @param miplevel  — the mip level to be loaded, level zero being the top-level full-resolution image. The default value is zero. 
		 */
		public function uploadFromBitmapData(source:BitmapData, side:uint, miplevel:uint = 0):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Uploads a component of a cube map texture from a ByteArray object. </p>
		 * <p>This function uploads one mip level of one side of the cube map. Call <code>uploadFromByteArray()</code> as neccessary to upload each mip level and face of the cube map.</p>
		 * 
		 * @param data  — a byte array containing the image in the format specified when this CubeTexture object was created. The ByteArray object must use the little endian format. 
		 * @param byteArrayOffset  — reading of the byte array starts there. 
		 * @param side  — A code indicating which side of the cube to upload: <p></p><ul>
		 *  positive X : 0
		 * </ul> <ul>
		 *  negative X : 1
		 * </ul> <ul>
		 *  positive Y : 2
		 * </ul> <ul>
		 *  negative Y : 3
		 * </ul> <ul>
		 *  positive Z : 4
		 * </ul> <ul>
		 *  negative Z : 5
		 * </ul><p></p> 
		 * @param miplevel  — the mip level to be loaded, level zero is the top-level, full-resolution image. 
		 */
		public function uploadFromByteArray(data:ByteArray, byteArrayOffset:uint, side:uint, miplevel:uint = 0):void {
			throw new Error("Not implemented");
		}
	}
}
