package flash.display3D {
	import flash.utils.ByteArray;

	/**
	 *  The VertexBuffer3D class represents a set of vertex data uploaded to a rendering context. <p>Use a VertexBuffer3D object to define the data associated with each point in a set of vertexes. You can upload the vertex data either from a Vector array or a ByteArray. (Once uploaded, the data in the original array is no longer referenced; changing or discarding the source array does not change the vertex data.)</p> <p>The data associated with each vertex is in an application-defined format and is used as the input for the vertex shader program. Identify which values belong to which vertex program input using the Context3D <code>setVertexBufferAt()</code> function. A vertex program can use up to eight inputs (also known as vertex attribute registers). Each input can require between one and four 32-bit values. For example, the [x,y,z] position coordinates of a vertex can be passed to a vertex program as a vector containing three 32 bit values. The Context3DVertexBufferFormat class defines constants for the supported formats for shader inputs. You can supply up to sixty-four 32-bit values (256 bytes) of data for each point (but a single vertex shader cannot use all of the data in this case).</p> <p>The <code>setVertexBufferAt()</code> function also identifies which vertex buffer to use for rendering any subsequent <code>drawTriangles()</code> calls. To render data from a different vertex buffer, call <code>setVertexBufferAt()</code> again with the appropriate arguments. (You can store data for the same point in multiple vertex buffers, say position data in one buffer and texture coordinates in another, but typically rendering is more efficient if all the data for a point comes from a single buffer.)</p> <p>The Index3DBuffer object passed to the Context3D <code>drawTriangles()</code> method organizes the vertex data into triangles. Each value in the index buffer is the index to a vertex in the vertex buffer. A set of three indexes, in sequence, defines a triangle.</p> <p>You cannot create a VertexBuffer3D object directly. Use the Context3D <code>createVertexBuffer()</code> method instead.</p> <p>To free the render context resources associated with a vertex buffer, call the object's <code>dispose()</code> method.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Context3D.html#createVertexBuffer()" target="">Context3D.createVertexBuffer()</a>
	 *  <br>
	 *  <a href="Context3D.html#setVertexBufferAt()" target="">Context3D.setVertexBufferAt()</a>
	 *  <br>
	 *  <a href="Context3DVertexBufferFormat.html" target="">Context3DVertexBufferFormat</a>
	 * </div><br><hr>
	 */
	public class VertexBuffer3D {

		/**
		 * <p> Frees all resources associated with this object. After disposing a vertex buffer, calling upload() and rendering using this object will fail. </p>
		 */
		public function dispose():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Uploads the data for a set of points to the rendering context from a byte array. </p>
		 * 
		 * @param data  — a byte array containing the vertex data. Each data value is four bytes long. The number of values in a vertex is specified at buffer creation using the <code>data32PerVertex</code> parameter to the Context3D <code>createVertexBuffer3D()</code> method. The length of the data in bytes must be <code>byteArrayOffset</code> plus four times the number of values per vertex times the number of vertices. The ByteArray object must use the little endian format. 
		 * @param byteArrayOffset  — number of bytes to skip from the beginning of data 
		 * @param startVertex  — The index of the first vertex to be loaded. A value for startVertex not equal to zero may be used to load a sub-region of the vertex data. 
		 * @param numVertices  — The number of vertices to be loaded from <code>data</code>. 
		 */
		public function uploadFromByteArray(data:ByteArray, byteArrayOffset:int, startVertex:int, numVertices:int):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Uploads the data for a set of points to the rendering context from a vector array. </p>
		 * 
		 * @param data  — a vector of 32-bit values. A single vertex is comprised of a number of values stored sequentially in the vector. The number of values in a vertex is specified at buffer creation using the <code>data32PerVertex</code> parameter to the Context3D <code>createVertexBuffer3D()</code> method. The length of the vector must be the number of values per vertex times the number of vertexes. 
		 * @param startVertex  — The index of the first vertex to be loaded. A value for <code>startVertex</code> not equal to zero may be used to load a sub-region of the vertex data. 
		 * @param numVertices  — The number of vertices represented by <code>data</code>. 
		 */
		public function uploadFromVector(data:Vector.<Number>, startVertex:int, numVertices:int):void {
			throw new Error("Not implemented");
		}
	}
}
