package flash.display3D {
	/**
	 *  Defines the values to use for sampler mipmap filter mode <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Context3D.html#setSamplerStateAt()" target="">flash.display3D.Context3D.setSamplerStateAt()</a>
	 * </div><br><hr>
	 */
	public class Context3DMipFilter {
		/**
		 * <p> Select the two closest MIP levels and linearly blend between them (the highest quality mode, but has some performance cost). </p>
		 */
		public static const MIPLINEAR:String = "miplinear";
		/**
		 * <p> Use the nearest neighbor metric to select MIP levels (the fastest rendering method). </p>
		 */
		public static const MIPNEAREST:String = "mipnearest";
		/**
		 * <p> Always use the top level texture (has a performance penalty when downscaling). </p>
		 */
		public static const MIPNONE:String = "mipnone";
	}
}
