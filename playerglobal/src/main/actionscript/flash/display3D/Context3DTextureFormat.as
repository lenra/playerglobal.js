package flash.display3D {
	/**
	 *  Defines the values to use for specifying a texture format. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Context3D.html#createTexture()" target="">Context3D.createTexture()</a>
	 *  <br>
	 *  <a href="Context3D.html#createCubeTexture()" target="">Context3D.createCubeTexture()</a>
	 * </div><br><hr>
	 */
	public class Context3DTextureFormat {
		/**
		 * <p> </p>
		 */
		public static const BGRA:String = "bgra";
		/**
		 * <p> 16 bit, bgra packed as 4:4:4:4 </p>
		 */
		public static const BGRA_PACKED:String = "bgraPacked4444";
		/**
		 * <p> 16 bit, bgr packed as 5:6:5 </p>
		 */
		public static const BGR_PACKED:String = "bgrPacked565";
		/**
		 * <p> </p>
		 */
		public static const COMPRESSED:String = "compressed";
		/**
		 * <p> </p>
		 */
		public static const COMPRESSED_ALPHA:String = "compressedAlpha";
		/**
		 * <p> </p>
		 */
		public static const RGBA_HALF_FLOAT:String = "rgbaHalfFloat";
	}
}
