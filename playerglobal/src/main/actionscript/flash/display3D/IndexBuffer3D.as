package flash.display3D {
	import flash.utils.ByteArray;

	/**
	 *  IndexBuffer3D is used to represent lists of vertex indices comprising graphic elements retained by the graphics subsystem. <p>Indices managed by an IndexBuffer3D object may be used to select vertices from a vertex stream. Indices are 16-bit unsigned integers. The maximum allowable index value is 65535 (0xffff). The graphics subsystem does not retain a reference to vertices provided to this object. Data uploaded to this object may be modified or discarded without affecting the stored values.</p> <p>IndexBuffer3D cannot be instantiated directly. Create instances by using Context3D::CreateIndexBuffer()</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  flash.display.Context3D.createIndexBuffer()
	 *  <br>flash.display.Context3D.drawTriangles()
	 * </div><br><hr>
	 */
	public class IndexBuffer3D {

		/**
		 * <p> Free all native GPU resources associated with this object. No upload() calls on this object will work and using the object in rendering will also fail. </p>
		 */
		public function dispose():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Store in the graphics subsystem vertex indices. </p>
		 * 
		 * @param data  — a ByteArray containing index data. Each index is represented by 16-bits (two bytes) in the array. The number of bytes in <code>data</code> should be <code>byteArrayOffset</code> plus two times <code>count</code>. 
		 * @param byteArrayOffset  — offset, in bytes, into the data ByteArray from where to start reading. 
		 * @param startOffset  — The index in this IndexBuffer3D object of the first index to be loaded in this IndexBuffer3D object. A value for startIndex not equal to zero may be used to load a sub-region of the index data. 
		 * @param count  — The number of indices represented by <code>data</code>. 
		 */
		public function uploadFromByteArray(data:ByteArray, byteArrayOffset:int, startOffset:int, count:int):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Store in the graphics subsystem vertex indices. </p>
		 * 
		 * @param data  — a vector of vertex indices. Only the low 16 bits of each index value are used. The length of the vector must be greater than or equal to <code>count</code>. 
		 * @param startOffset  — The index in this IndexBuffer3D object of the first index to be loaded. A value for startOffset not equal to zero may be used to load a sub-region of the index data. 
		 * @param count  — The number of indices in <code>data</code>. 
		 */
		public function uploadFromVector(data:Vector.<uint>, startOffset:int, count:int):void {
			throw new Error("Not implemented");
		}
	}
}
