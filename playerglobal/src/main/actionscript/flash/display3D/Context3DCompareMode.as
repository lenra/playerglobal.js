package flash.display3D {
	/**
	 *  Defines the values to use for specifying 3D buffer comparisons in the setDepthTest() and setStencilAction() methods of a Context3D instance. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Context3D.html#setDepthTest()" target="">Context3D.setDepthTest()</a>
	 *  <br>
	 *  <a href="Context3D.html#setStencilActions()" target="">Context3D.setStencilActions()</a>
	 * </div><br><hr>
	 */
	public class Context3DCompareMode {
		/**
		 * <p> The comparison always evaluates as true. </p>
		 */
		public static const ALWAYS:String = "always";
		/**
		 * <p> Equal (==). </p>
		 */
		public static const EQUAL:String = "equal";
		/**
		 * <p> Greater than (&gt;). </p>
		 */
		public static const GREATER:String = "greater";
		/**
		 * <p> Greater than or equal (&gt;=). </p>
		 */
		public static const GREATER_EQUAL:String = "greaterEqual";
		/**
		 * <p> Less than (&lt;). </p>
		 */
		public static const LESS:String = "less";
		/**
		 * <p> Less than or equal (&lt;=). </p>
		 */
		public static const LESS_EQUAL:String = "lessEqual";
		/**
		 * <p> The comparison never evaluates as true. </p>
		 */
		public static const NEVER:String = "never";
		/**
		 * <p> Not equal (!=). </p>
		 */
		public static const NOT_EQUAL:String = "notEqual";
	}
}
