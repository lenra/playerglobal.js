package flash.display3D {
	/**
	 *  Defines the values to use for specifying the Context3D render mode. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../flash/display/Stage3D.html#requestContext3D()" target="">flash.display.Stage3D.requestContext3D()</a>
	 * </div><br><hr>
	 */
	public class Context3DRenderMode {
		/**
		 * <p> Automatically choose rendering engine. </p>
		 * <p>A hardware-accelerated rendering engine is used if available on the current device. Availability of hardware acceleration is influenced by the device capabilites, the wmode when running under Flash Player, and the render mode when running under AIR.</p>
		 */
		public static const AUTO:String = "auto";
		/**
		 * <p> Use software 3D rendering. </p>
		 * <p>Software rendering is not available on mobile devices.</p>
		 */
		public static const SOFTWARE:String = "software";
	}
}
