package flash.display3D {
	/**
	 *  Defines the values to use for specifying the source and destination blend factors. <p>A blend factor represents a particular four-value vector that is multiplied with the source or destination color in the blending formula. The blending formula is:</p> <p> <code>result = source * sourceFactor + destination * destinationFactor</code> </p> <p>In the formuls, the source color is the output color of the pixel shader program. The destination color is the color that currently exists in the color buffer, as set by previous clear and draw operations.</p> <p>For example, if the source color is (.6, .6, .6, .4) and the source blend factor is <code>Context3DBlendFactor.ONE_MINUS_SOURCE_ALPHA</code>, then the source part of the blending equation is calculated as:</p> <p> <code>(.6, .6, .6, .4) * (1-0.4, 1-0.4, 1-0.4, 1-0.4) = (.36, .36, .36, .24)</code> </p> <p>The final calculation is clamped to the range [0,1].</p> <p> <b>Examples</b> </p> <p>The following examples demonstrate the blending math using source color = (.6,.4,.2,.4), destination color = (.8,.8,.8,.5), and various blend factors.</p> <table class="innertable">
	 *  <tbody>
	 *   <tr>
	 *    <th>Purpose</th>
	 *    <th>Source factor</th>
	 *    <th>Destination factor</th>
	 *    <th>Blend formula</th>
	 *    <th>Result</th>
	 *   </tr>
	 *   <tr>
	 *    <td>No blending</td>
	 *    <td>ONE</td>
	 *    <td>ZERO</td>
	 *    <td>(.6,.4,.2,.4) * ( 1, 1, 1, 1) + (.8,.8,.8,.5) * ( 0, 0, 0, 0)</td>
	 *    <td>( .6, .4, .2, .4)</td>
	 *   </tr>
	 *   <tr>
	 *    <td>Alpha</td>
	 *    <td>SOURCE_ALPHA</td>
	 *    <td>ONE_MINUS_SOURCE_ALPHA</td>
	 *    <td>(.6,.4,.2,.4) * (.4,.4,.4,.4) + (.8,.8,.8,.5) * (.6,.6,.6,.6)</td>
	 *    <td>(.72,.64,.56,.46)</td>
	 *   </tr>
	 *   <tr>
	 *    <td>Additive</td>
	 *    <td>ONE</td>
	 *    <td>ONE</td>
	 *    <td>(.6,.4,.2,.4) * ( 1, 1, 1, 1) + (.8,.8,.8,.5) * ( 1, 1, 1, 1)</td>
	 *    <td>( 1, 1, 1, .9)</td>
	 *   </tr>
	 *   <tr>
	 *    <td>Multiply</td>
	 *    <td>DESTINATION_COLOR</td>
	 *    <td>ZERO</td>
	 *    <td>(.6,.4,.2,.4) * (.8,.8,.8,.5) + (.8,.8,.8,.5) * ( 0, 0, 0, 0)</td>
	 *    <td>(.48,.32,.16, .2)</td>
	 *   </tr>
	 *   <tr>
	 *    <td>Screen</td>
	 *    <td>ONE</td>
	 *    <td>ONE_MINUS_SOURCE_COLOR</td>
	 *    <td>(.6,.4,.2,.4) * ( 1, 1, 1, 1) + (.8,.8,.8,.5) * (.4,.6,.8,.6)</td>
	 *    <td>(.92,.88,.68, .7)</td>
	 *   </tr>
	 *  </tbody>
	 * </table> <p>Note that not all combinations of blend factors are useful and that you can sometimes achieve the same effect in different ways.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Context3D.html#setBlendFactors()" target="">Context3D.setBlendFactors()</a>
	 * </div><br><hr>
	 */
	public class Context3DBlendFactor {
		/**
		 * <p> The blend factor is <code>(D<sub>a</sub>,D<sub>a</sub>,D<sub>a</sub>,D<sub>a</sub>)</code>, where <code>D<sub>a</sub></code> is the alpha component of the fragment color computed by the pixel program. </p>
		 */
		public static const DESTINATION_ALPHA:String = "destinationAlpha";
		/**
		 * <p> The blend factor is <code>(D<sub>r</sub>,D<sub>g</sub>,D<sub>b</sub>,D<sub>a</sub>)</code>, where <code>D<sub>r/g/b/a</sub></code> is the corresponding component of the current color in the color buffer. </p>
		 */
		public static const DESTINATION_COLOR:String = "destinationColor";
		/**
		 * <p> The blend factor is <code>(1,1,1,1)</code>. </p>
		 */
		public static const ONE:String = "one";
		/**
		 * <p> The blend factor is <code>(1-D<sub>a</sub>,1-D<sub>a</sub>,1-D<sub>a</sub>,1-D<sub>a</sub>)</code>, where <code>D<sub>a</sub></code> is the alpha component of the current color in the color buffer. </p>
		 */
		public static const ONE_MINUS_DESTINATION_ALPHA:String = "oneMinusDestinationAlpha";
		/**
		 * <p> The blend factor is <code>(1-D<sub>r</sub>,1-D<sub>g</sub>,1-D<sub>b</sub>,1-D<sub>a</sub>)</code>, where <code>D<sub>r/g/b/a</sub></code> is the corresponding component of the current color in the color buffer. </p>
		 */
		public static const ONE_MINUS_DESTINATION_COLOR:String = "oneMinusDestinationColor";
		/**
		 * <p> The blend factor is <code>(1-S<sub>a</sub>,1-S<sub>a</sub>,1-S<sub>a</sub>,1-S<sub>a</sub>)</code>, where <code>S<sub>a</sub></code> is the alpha component of the fragment color computed by the pixel program. </p>
		 */
		public static const ONE_MINUS_SOURCE_ALPHA:String = "oneMinusSourceAlpha";
		/**
		 * <p> The blend factor is <code>(1-S<sub>r</sub>,1-S<sub>g</sub>,1-S<sub>b</sub>,1-S<sub>a</sub>)</code>, where <code>S<sub>r/g/b/a</sub></code> is the corresponding component of the fragment color computed by the pixel program. </p>
		 */
		public static const ONE_MINUS_SOURCE_COLOR:String = "oneMinusSourceColor";
		/**
		 * <p> The blend factor is <code>(S<sub>a</sub>,S<sub>a</sub>,S<sub>a</sub>,S<sub>a</sub>)</code>, where <code>S<sub>a</sub></code> is the alpha component of the fragment color computed by the pixel program. </p>
		 */
		public static const SOURCE_ALPHA:String = "sourceAlpha";
		/**
		 * <p> The blend factor is <code>(S<sub>r</sub>,S<sub>g</sub>,S<sub>b</sub>,S<sub>a</sub>)</code>, where <code>S<sub>r/g/b/a</sub></code> is the corresponding component of the fragment color computed by the pixel program. </p>
		 */
		public static const SOURCE_COLOR:String = "sourceColor";
		/**
		 * <p> The blend factor is <code>(0,0,0,0)</code>. </p>
		 */
		public static const ZERO:String = "zero";
	}
}
