package flash.display3D {
	/**
	 *  Defines the values to use for specifying whether a shader program is a fragment or a vertex program. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Context3D.html#setProgramConstantsFromMatrix()" target="">Context3D.setProgramConstantsFromMatrix()</a>
	 *  <br>
	 *  <a href="Context3D.html#setProgramConstantsFromVector()" target="">Context3D.setProgramConstantsFromVector()</a>
	 * </div><br><hr>
	 */
	public class Context3DProgramType {
		/**
		 * <p> A fragment (or pixel) program. </p>
		 */
		public static const FRAGMENT:String = "fragment";
		/**
		 * <p> A vertex program. </p>
		 */
		public static const VERTEX:String = "vertex";
	}
}
