package flash.display3D {
	import flash.utils.ByteArray;

	/**
	 *  The Program3D class represents a pair of rendering programs (also called "shaders") uploaded to the rendering context. <p>Programs managed by a Program3D object control the entire rendering of triangles during a Context3D <code>drawTriangles()</code> call. Upload the binary bytecode to the rendering context using the <code>upload</code> method. (Once uploaded, the program in the original byte array is no longer referenced; changing or discarding the source byte array does not change the program.)</p> <p>Programs always consist of two linked parts: A vertex and a fragment program. </p><ol> 
	 *  <li>The vertex program operates on data defined in VertexBuffer3D objects and is responsible for projecting vertices into clip space and passing any required vertex data, such as color, to the fragment shader.</li> 
	 *  <li>The fragment shader operates on the attributes passed to it by the vertex program and produces a color for every rasterized fragment of a triangle, resulting in pixel colors. Note that fragment programs have several names in 3D programming literature, including fragment shader and pixel shader.</li> 
	 * </ol>  <p>Designate which program pair to use for subsequent rendering operations by passing the corresponding Program3D instance to the Context3D <code>setProgram()</code> method.</p> <p>You cannot create a Program3D object directly; use the Context3D <code>createProgram()</code> method instead.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Context3D.html#createProgram()" target="">Context3D.createProgram()</a>
	 *  <br>
	 *  <a href="Context3D.html#setProgram()" target="">Context3D.setProgram()</a>
	 * </div><br><hr>
	 */
	public class Program3D {

		/**
		 * <p> Frees all resources associated with this object. After disposing a Program3D object, calling upload() and rendering using this object will fail. </p>
		 */
		public function dispose():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Uploads a pair of rendering programs expressed in AGAL (Adobe Graphics Assembly Language) bytecode. </p>
		 * <p>Program bytecode can be created using the Pixel Bender 3D offline tools. It can also be created dynamically. The AGALMiniAssembler class is a utility class that compiles AGAL assembly language programs to AGAL bytecode. The class is not part of the runtime. When you upload the shader programs, the bytecode is compiled into the native shader language for the current device (for example, OpenGL or Direct3D). The runtime validates the bytecode on upload.</p>
		 * <p>The programs run whenever the Context3D <code>drawTriangles()</code> method is invoked. The vertex program is executed once for each vertex in the list of triangles to be drawn. The fragment program is executed once for each pixel on a triangle surface. </p>
		 * <p>The "variables" used by a shader program are called <i>registers</i>. The following registers are defined:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Name</th>
		 *    <th>Number per Fragment program</th>
		 *    <th>Number per Vertex program</th>
		 *    <th>Purpose</th>
		 *   </tr>
		 *   <tr>
		 *    <td>Attribute</td>
		 *    <td>n/a</td>
		 *    <td>8</td>
		 *    <td>Vertex shader input; read from a vertex buffer specified using Context3D.setVertexBufferAt().</td>
		 *   </tr>
		 *   <tr>
		 *    <td>Constant</td>
		 *    <td>28</td>
		 *    <td>128</td>
		 *    <td>Shader input; set using the Context3D.setProgramConstants() family of functions.</td>
		 *   </tr>
		 *   <tr>
		 *    <td>Temporary</td>
		 *    <td>8</td>
		 *    <td>8</td>
		 *    <td>Temporary register for computation, not accessible outside program.</td>
		 *   </tr>
		 *   <tr>
		 *    <td>Output</td>
		 *    <td>1</td>
		 *    <td>1</td>
		 *    <td>Shader output: in a vertex program, the output is the clipspace position; in a fragment program, the output is a color.</td>
		 *   </tr>
		 *   <tr>
		 *    <td>Varying</td>
		 *    <td>8</td>
		 *    <td>8</td>
		 *    <td>Transfer interpolated data between vertex and fragment shaders. The varying registers from the vertex program are applied as input to the fragment program. Values are interpolated according to the distance from the triangle vertices.</td>
		 *   </tr>
		 *   <tr>
		 *    <td>Sampler</td>
		 *    <td>8</td>
		 *    <td>n/a</td>
		 *    <td>Fragment shader input; read from a texture specified using Context3D.setTextureAt()</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p>A vertex program receives input from two sources: vertex buffers and constant registers. Specify which vertex data to use for a particular vertex <i>attribute</i> register using the Context3D <code>setVertexBufferAt()</code> method. You can define up to eight input registers for vertex attributes. The vertex attribute values are read from the vertex buffer for each vertex in the triangle list and placed in the attribute register. Specify <i>constant</i> registers using the Context3D <code>setProgramConstantsFromMatrix()</code> or <code>setProgramConstantsFromVector()</code> methods. Constant registers retain the same value for every vertex in the triangle list. (You can only modify the constant values between calls to <code>drawTriangles()</code>.) </p>
		 * <p>The vertex program is responsible for projecting the triangle vertices into clip space (the canonical viewing area within ±1 on the x and y axes and 0-1 on the z axis) and placing the transformed coordinates in its output register. (Typically, the appropriate projection matrix is provided to the shader in a set of constant registers.) The vertex program must also copy any vertex attributes or computed values needed by the fragment program to a special set of variables called <i>varying</i> registers. When a fragment shader runs, the value supplied in a varying register is linearly interpolated according to the distance of the current fragment from each triangle vertex.</p>
		 * <p>A fragment program receives input from the varying registers and from a separate set of constant registers (set with <code>setProgramConstantsFromMatrix()</code> or <code>setProgramConstantsFromVector()</code>). You can also read texture data from textures uploaded to the rendering context using <i>sampler</i> registers. Specify which texture to access with a particular sampler register using the Context3D <code>setTextureAt()</code> method. The fragment program is responsible for setting its output register to a color value. </p>
		 * 
		 * @param vertexProgram  — AGAL bytecode for the Vertex program. The ByteArray object must use the little endian format. 
		 * @param fragmentProgram  — AGAL bytecode for the Fragment program. The ByteArray object must use the little endian format. 
		 */
		public function upload(vertexProgram:ByteArray, fragmentProgram:ByteArray):void {
			throw new Error("Not implemented");
		}
	}
}
