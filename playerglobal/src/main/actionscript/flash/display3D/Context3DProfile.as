package flash.display3D {
	/**
	 *  Defines the values to use for specifying the Context3D profile. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../flash/display/Stage3D.html#requestContext3D()" target="">flash.display.Stage3D.requestContext3D()</a>
	 * </div><br><hr>
	 */
	public class Context3DProfile {
		/**
		 * <p> Use the default feature support profile. </p>
		 * <p>This profile most closely resembles Stage3D support used in previous releases.</p>
		 */
		public static const BASELINE:String = "baseline";
		/**
		 * <p> Use a constrained feature support profile to target older GPUs </p>
		 * <p>This profile is primarily targeted at devices that only support PS_2.0 level shaders like the Intel GMA 9xx series. In addition, this mode tries to improve memory bandwidth usage by rendering directly into the back buffer. There are several side effects:</p>
		 * <ul>
		 *  <li>You are limited to 64 ALU and 32 texture instructions per shader.</li>
		 *  <li>Only four texture read instructions per shader.</li>
		 *  <li>No support for predicate register. This affects sln/sge/seq/sne, which you replace with compound mov/cmp instructions, available with ps_2_0.</li>
		 *  <li>The Context3D back buffer must always be within the bounds of the stage.</li>
		 *  <li>Only one instance of a Context3D running in Constrained profile is allowed within a Flash Player instance.</li>
		 *  <li>Standard display list list rendering is driven by <code>Context3D.present()</code> instead of being based on the SWF frame rate. That is, if a Context3D object is active and visible you must call <code>Context3D.present()</code> to render the standard display list.</li>
		 *  <li>Reading back from the back buffer through <code>Context3D.drawToBitmapData()</code> might include parts of the display list content. Alpha information will be lost.</li>
		 * </ul>
		 */
		public static const BASELINE_CONSTRAINED:String = "baselineConstrained";
		/**
		 * <p> Use an extended feature support profile to target newer GPUs which support larger textures </p>
		 * <p>This profile increases the maximum 2D Texture and RectangleTexture size to 4096x4096</p>
		 */
		public static const BASELINE_EXTENDED:String = "baselineExtended";
		/**
		 * <p> Use enhanced profile to target GPUs which support AGAL4. </p>
		 * <p>This profile supports Vertex Texture Fetch</p>
		 * <p> This profile is enabled from AIR 26.0 on mobile platforms and AIR 29.0 on Windows and Mac. </p>
		 */
		public static const ENHANCED:String = "enhanced";
		/**
		 * <p> Use an standard profile to target GPUs which support MRT, AGAL2 and float textures. </p>
		 * <p>This profile supports 4 render targets. Increase AGAL commands and register count. Add float textures.</p>
		 */
		public static const STANDARD:String = "standard";
		/**
		 * <p> Use an standard profile to target GPUs which support AGAL2 and float textures. </p>
		 * <p>This profile is an alternative to standard profile, which removes MRT and a few features in AGAL2 but can reach more GPUs.</p>
		 */
		public static const STANDARD_CONSTRAINED:String = "standardConstrained";
		/**
		 * <p> Use standard extended profile to target GPUs which support AGAL3 and instanced drawing feature. </p>
		 * <p>This profile extends the standard profile.</p>
		 * <p>This profile is enabled on mobile platforms from AIR 17.0 and on Windows and Mac from AIR 18.0.</p>
		 */
		public static const STANDARD_EXTENDED:String = "standardExtended";
	}
}
