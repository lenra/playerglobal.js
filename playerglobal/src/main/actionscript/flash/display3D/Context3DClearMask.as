package flash.display3D {
	/**
	 *  Defines the values to use for specifying Context3D clear masks. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Context3D.html#clear()" target="">Context3D.clear()</a>
	 * </div><br><hr>
	 */
	public class Context3DClearMask {
		/**
		 * <p> Clear all buffers. </p>
		 */
		public static const ALL:int = 0;
		/**
		 * <p> Clear only the color buffer. </p>
		 */
		public static const COLOR:int = 1;
		/**
		 * <p> Clear only the depth buffer. </p>
		 */
		public static const DEPTH:int = 2;
		/**
		 * <p> Clear only the stencil buffer. </p>
		 */
		public static const STENCIL:int = 3;
	}
}
