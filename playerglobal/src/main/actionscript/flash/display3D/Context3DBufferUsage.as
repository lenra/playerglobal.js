package flash.display3D {
	/**
	 *  Defines the values to use for specifying the buffer usage type. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  and flash.display.Context3D.createVextexBuffer()
	 * </div><br><hr>
	 */
	public class Context3DBufferUsage {
		/**
		 * <p> Indicates the buffer will be used for drawing and be updated frequently </p>
		 */
		public static const DYNAMIC_DRAW:String = "dynamicDraw";
		/**
		 * <p> Indicates the buffer will be used for drawing and be updated once </p>
		 * <p>This type is the default value for buffers in <code>Stage3D</code>.</p>
		 */
		public static const STATIC_DRAW:String = "staticDraw";
	}
}
