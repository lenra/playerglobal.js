package flash.display3D {
	/**
	 *  Defines the values to use for sampler wrap mode <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Context3D.html#setSamplerStateAt()" target="">flash.display3D.Context3D.setSamplerStateAt()</a>
	 * </div><br><hr>
	 */
	public class Context3DWrapMode {
		/**
		 * <p> Clamp texture coordinates outside the 0..1 range. </p>
		 * <p>The function is x = max(min(x,0),1)</p>
		 */
		public static const CLAMP:String = "clamp";
		public static const CLAMP_U_REPEAT_V:String = "clamp_u_repeat_v";
		/**
		 * <p> Repeat (tile) texture coordinates outside the 0..1 range. </p>
		 * <p>The function is x = x&lt;0?1.0-frac(abs(x)):frac(x)</p>
		 */
		public static const REPEAT:String = "repeat";
		public static const REPEAT_U_CLAMP_V:String = "repeat_u_clamp_v";
	}
}
