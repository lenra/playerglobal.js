package flash.display3D {
	/**
	 *  Defines the values to use for specifying stencil actions. <p>A stencil action specifies how the values in the stencil buffer should be changed.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Context3D.html#setStencilActions()" target="">Context3D.setStencilActions()</a>
	 * </div><br><hr>
	 */
	public class Context3DStencilAction {
		/**
		 * <p> Decrement the stencil buffer value, clamping at 0, the minimum value. </p>
		 */
		public static const DECREMENT_SATURATE:String = "decrementSaturate";
		/**
		 * <p> Decrement the stencil buffer value. If the result is less than 0, the minimum value, then the buffer value is "wrapped around" to 255. </p>
		 */
		public static const DECREMENT_WRAP:String = "decrementWrap";
		/**
		 * <p> Increment the stencil buffer value, clamping at 255, the maximum value. </p>
		 */
		public static const INCREMENT_SATURATE:String = "incrementSaturate";
		/**
		 * <p> Increment the stencil buffer value. If the result exceeds 255, the maximum value, then the buffer value is "wrapped around" to 0. </p>
		 */
		public static const INCREMENT_WRAP:String = "incrementWrap";
		/**
		 * <p> Invert the stencil buffer value, bitwise. </p>
		 * <p>For example, if the 8-bit binary number in the stencil buffer is: <code>11110000</code>, then the value is changed to: <code>00001111</code>.</p>
		 */
		public static const INVERT:String = "invert";
		/**
		 * <p> Keep the current stencil buffer value. </p>
		 */
		public static const KEEP:String = "keep";
		/**
		 * <p> Replace the stencil buffer value with the reference value. </p>
		 */
		public static const SET:String = "set";
		/**
		 * <p> Set the stencil buffer value to 0. </p>
		 */
		public static const ZERO:String = "zero";
	}
}
