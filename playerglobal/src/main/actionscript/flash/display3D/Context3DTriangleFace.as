package flash.display3D {
	/**
	 *  Constants to specify the orientation of a triangle relative to the view point. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Context3D.html#setCulling()" target="">Context3D.setCulling()</a>
	 *  <br>
	 *  <a href="Context3D.html#setStencilActions()" target="">Context3D.setStencilActions()</a>
	 * </div><br><hr>
	 */
	public class Context3DTriangleFace {
		/**
		 * <p> </p>
		 */
		public static const BACK:String = "back";
		/**
		 * <p> </p>
		 */
		public static const FRONT:String = "front";
		/**
		 * <p> </p>
		 */
		public static const FRONT_AND_BACK:String = "frontAndBack";
		/**
		 * <p> </p>
		 */
		public static const NONE:String = "none";
	}
}
