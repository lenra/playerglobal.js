package flash.display3D {
	import flash.display.BitmapData;
	import flash.display3D.textures.CubeTexture;
	import flash.display3D.textures.RectangleTexture;
	import flash.display3D.textures.Texture;
	import flash.display3D.textures.TextureBase;
	import flash.display3D.textures.VideoTexture;
	import flash.events.EventDispatcher;
	import flash.geom.Matrix3D;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.ByteArray;

	/**
	 *  The Context3D class provides a context for rendering geometrically defined graphics. <p>A rendering context includes a drawing surface and its associated resources and state. When possible, the rendering context uses the hardware graphics processing unit (GPU). Otherwise, the rendering context uses software. (If rendering through Context3D is not supported on a platform, the <code>stage3Ds</code> property of the Stage object contains an empty list.)</p> <p>The Context3D rendering context is a programmable pipeline that is very similar to OpenGL ES 2, but is abstracted so that it is compatible with a range of hardware and GPU interfaces. Although designed for 3D graphics, the rendering pipeline does not mandate that the rendering is three dimensional. Thus, you can create a 2D renderer by supplying the appropriate vertex and pixel fragment programs. In both the 3D and 2D cases, the only geometric primitive supported is the triangle.</p> <p>Get an instance of the Context3D class by calling the <code>requestContext3D()</code> method of a Stage3D object. A limited number of Context3D objects can exist per stage; one for each Stage3D in the <code>Stage.stage3Ds</code> list. When the context is created, the Stage3D object dispatches a <code>context3DCreate</code> event. A rendering context can be destroyed and recreated at any time, such as when another application that uses the GPU gains focus. Your code should anticipate receiving multiple <code>context3DCreate</code> events. Position the rendering area on the stage using the <code>x</code> and <code>y</code> properties of the associated Stage3D instance.</p> <p>To render and display a scene (after getting a Context3D object), the following steps are typical:</p> <ol> 
	 *  <li>Configure the main display buffer attributes by calling <code>configureBackBuffer()</code>.</li> 
	 *  <li>Create and initialize your rendering resources, including: 
	 *   <ul> 
	 *    <li>Vertex and index buffers defining the scene geometry</li> 
	 *    <li>Vertex and pixel programs (shaders) for rendering the scene</li> 
	 *    <li>Textures</li> 
	 *   </ul> </li> 
	 *  <li>Render a frame: 
	 *   <ul> 
	 *    <li>Set the render state as appropriate for an object or collection of objects in the scene.</li> 
	 *    <li>Call the <code>drawTriangles()</code> method to render a set of triangles.</li> 
	 *    <li>Change the rendering state for the next group of objects.</li> 
	 *    <li>Call <code>drawTriangles()</code> to draw the triangles defining the objects.</li> 
	 *    <li>Repeat until the scene is entirely rendered.</li> 
	 *    <li>Call the <code>present()</code> method to display the rendered scene on the stage.</li> 
	 *   </ul> </li> 
	 * </ol> <p>The following limits apply to rendering:</p> <p>Resource limits: </p><table class="innertable">
	 *  <tbody>
	 *   <tr>
	 *    <th>Resource</th>
	 *    <th>Number allowed</th>
	 *    <th>Total memory</th>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>Vertex buffers</code> </td>
	 *    <td>4096</td>
	 *    <td>256 MB</td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>Index buffers</code> </td>
	 *    <td>4096</td>
	 *    <td>128 MB</td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>Programs</code> </td>
	 *    <td>4096</td>
	 *    <td>16 MB</td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>Textures</code> </td>
	 *    <td>4096</td>
	 *    <td>128 MB</td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>Cube textures</code> </td>
	 *    <td>4096</td>
	 *    <td>256 MB</td>
	 *   </tr>
	 *  </tbody>
	 * </table>  <p>AGAL limits: 200 opcodes per program.</p> <p>Draw call limits: 32,768 <code>drawTriangles()</code> calls for each <code>present()</code> call.</p> <p>The following limits apply to textures:</p> <p>Texture limits for AIR 32 bit: </p><table class="innertable">
	 *  <tbody>
	 *   <tr>
	 *    <th>Texture</th>
	 *    <th>Maximum size</th>
	 *    <th>Total GPU memory</th>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>Normal Texture (below Baseline extended)</code> </td>
	 *    <td>2048x2048</td>
	 *    <td>512 MB</td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>Normal Texture (Baseline extended and above)</code> </td>
	 *    <td>4096x4096</td>
	 *    <td>512 MB</td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>Rectangular Texture (below Baseline extended)</code> </td>
	 *    <td>2048x2048</td>
	 *    <td>512 MB</td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>Rectangular Texture (Baseline extended and above)</code> </td>
	 *    <td>4096x4096</td>
	 *    <td>512 MB</td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>Cube Texture</code> </td>
	 *    <td>1024x1024</td>
	 *    <td>256 MB</td>
	 *   </tr>
	 *  </tbody>
	 * </table>  <p>Texture limits for AIR 64 bit (Desktop): </p><table class="innertable">
	 *  <tbody>
	 *   <tr>
	 *    <th>Texture</th>
	 *    <th>Maximum size</th>
	 *    <th>Total GPU memory</th>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>Normal Texture (below Baseline extended)</code> </td>
	 *    <td>2048x2048</td>
	 *    <td>512 MB</td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>Normal Texture (Baseline extended to Standard)</code> </td>
	 *    <td>4096x4096</td>
	 *    <td>512 MB</td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>Normal Texture (Standard extended and above)</code> </td>
	 *    <td>4096x4096</td>
	 *    <td>2048 MB</td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>Rectangular Texture (below Baseline extended)</code> </td>
	 *    <td>2048x2048</td>
	 *    <td>512 MB</td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>Rectangular Texture (Baseline extended to Standard)</code> </td>
	 *    <td>4096x4096</td>
	 *    <td>512 MB</td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>Rectangular Texture (Standard extended and above)</code> </td>
	 *    <td>4096x4096</td>
	 *    <td>2048 MB</td>
	 *   </tr>
	 *   <tr>
	 *    <td> <code>Cube Texture</code> </td>
	 *    <td>1024x1024</td>
	 *    <td>256 MB</td>
	 *   </tr>
	 *  </tbody>
	 * </table>  <p>512 MB is the absolute limit for textures, including the texture memory required for mipmaps. However, for Cube Textures, the memory limit is 256 MB.</p> <p>You cannot create Context3D objects with the Context3D constructor. It is constructed and available as a property of a Stage3D instance. The Context3D class can be used on both desktop and mobile platforms, both when running in Flash Player and AIR.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d5b.html" target="_blank">Taking advantage of mipmapping</a>
	 *  <br>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Context3DBlendFactor.html" target="">Context3DBlendFactor</a>
	 *  <br>
	 *  <a href="Context3DClearMask.html" target="">Context3DClearMask</a>
	 *  <br>
	 *  <a href="Context3DCompareMode.html" target="">Context3DCompareMode</a>
	 *  <br>
	 *  <a href="Context3DProgramType.html" target="">Context3DProgramType</a>
	 *  <br>
	 *  <a href="Context3DRenderMode.html" target="">Context3DRenderMode</a>
	 *  <br>
	 *  <a href="Context3DStencilAction.html" target="">Context3DStencilAction</a>
	 *  <br>
	 *  <a href="Context3DTextureFormat.html" target="">Context3DTextureFormat</a>
	 *  <br>
	 *  <a href="Context3DTriangleFace.html" target="">Context3DTriangleFace</a>
	 *  <br>
	 *  <a href="Context3DVertexBufferFormat.html" target="">Context3DVertexBufferFormat</a>
	 *  <br>
	 *  <a href="../../flash/display3D/textures/Texture.html" target="">flash.display3D.textures.Texture</a>
	 *  <br>
	 *  <a href="../../flash/display3D/textures/CubeTexture.html" target="">flash.display3D.textures.CubeTexture</a>
	 *  <br>
	 *  <a href="IndexBuffer3D.html" target="">IndexBuffer3D</a>
	 *  <br>
	 *  <a href="../../flash/geom/Matrix3D.html" target="">flash.geom.Matrix3D</a>
	 *  <br>
	 *  <a href="Program3D.html" target="">Program3D</a>
	 *  <br>
	 *  <a href="../../flash/display/Stage3D.html" target="">flash.display.Stage3D</a>
	 *  <br>
	 *  <a href="VertexBuffer3D.html" target="">VertexBuffer3D</a>
	 * </div><br><hr>
	 */
	public class Context3D extends EventDispatcher {
		private static var _supportsVideoTexture:Boolean;

		private var _enableErrorChecking:Boolean;
		private var _maxBackBufferHeight:int;
		private var _maxBackBufferWidth:int;

		private var _backBufferHeight:int;
		private var _backBufferWidth:int;
		private var _driverInfo:String;
		private var _profile:String;
		private var _totalGPUMemory:Number;

		/**
		 * <p> Specifies the height of the back buffer, which can be changed by a successful call to the <code>configureBackBuffer()</code> method. The height may be modified when the browser zoom factor changes if the <code>wantsBestResolutionOnBrowserZoom</code> is set to <code>true</code> in the last successful call to the <code>configureBackBuffer()</code> method. The change in height can be detected by registering an event listener for the browser zoom change event. </p>
		 * 
		 * @return 
		 */
		public function get backBufferHeight():int {
			return _backBufferHeight;
		}

		/**
		 * <p> Specifies the width of the back buffer, which can be changed by a successful call to the <code>configureBackBuffer()</code> method. The width may be modified when the browser zoom factor changes if the <code>wantsBestResolutionOnBrowserZoom</code> is set to <code>true</code> in the last successful call to the <code>configureBackBuffer()</code> method. The change in width can be detected by registering an event listener for the browser zoom change event. </p>
		 * 
		 * @return 
		 */
		public function get backBufferWidth():int {
			return _backBufferWidth;
		}

		/**
		 * <p> The type of graphics library driver used by this rendering context. Indicates whether the rendering is using software, a DirectX driver, or an OpenGL driver. Also indicates whether hardware rendering failed. If hardware rendering fails, Flash Player uses software rendering for Stage3D and <code>driverInfo</code> contains one of the following values: </p>
		 * <ul>
		 *  <li>"Software Hw_disabled=userDisabled" - The Enable hardware acceleration checkbox in the Adobe Flash Player Settings UI is not selected.</li>
		 *  <li>"Software Hw_disabled=oldDriver" - There are known problems with the hardware graphics driver. Updating the graphics driver may fix this problem.</li>
		 *  <li>"Software Hw_disabled=unavailable" - Known problems with the hardware graphics driver or hardware graphics initialization failure.</li>
		 *  <li>"Software Hw_disabled=explicit" - The content explicitly requested software rendering through requestContext3D.</li>
		 *  <li>"Software Hw_disabled=domainMemory" - The content uses domainMemory, which requires a license when used with Stage3D hardware rendering. Visit <a href="http://www.adobe.com/go/fpl" target="_new">adobe.com/go/fpl</a>.</li>
		 * </ul>
		 * 
		 * @return 
		 */
		public function get driverInfo():String {
			return _driverInfo;
		}

		/**
		 * <p> Specifies whether errors encountered by the renderer are reported to the application. </p>
		 * <p>When <code>enableErrorChecking</code> is <code>true</code>, the <code>clear()</code>, and <code>drawTriangles()</code> methods are synchronous and can throw errors. When <code>enableErrorChecking</code> is <code>false</code>, the default, the <code>clear()</code>, and <code>drawTriangles()</code> methods are asynchronous and errors are not reported. Enabling error checking reduces rendering performance. You should only enable error checking when debugging.</p>
		 * 
		 * @return 
		 */
		public function get enableErrorChecking():Boolean {
			return _enableErrorChecking;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set enableErrorChecking(value:Boolean):void {
			_enableErrorChecking = value;
		}

		/**
		 * <p> Specifies the maximum height of the back buffer. The inital value is the system limit in the platform. The property can be set to a value smaller than or equal to, but not greater than, the system limit. The property can be set to a value greater than or equal to, but not smaller than, the minimum limit. The minimum limit is a constant value, 32, when the back buffer is not configured. The minimum limit will be the value of the height parameter in the last successful call to the <code>configureBackBuffer()</code> method after the back buffer is configured. </p>
		 * 
		 * @return 
		 */
		public function get maxBackBufferHeight():int {
			return _maxBackBufferHeight;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set maxBackBufferHeight(value:int):void {
			_maxBackBufferHeight = value;
		}

		/**
		 * <p> Specifies the maximum width of the back buffer. The inital value is the system limit in the platform. The property can be set to a value smaller than or equal to, but not greater than, the system limit. The property can be set to a value greater than or equal to, but not smaller than, the minimum limit. The minimum limit is a constant value, 32, when the back buffer is not configured. The minimum limit will be the value of the width parameter in the last successful call to the <code>configureBackBuffer()</code> method after the back buffer is configured. </p>
		 * 
		 * @return 
		 */
		public function get maxBackBufferWidth():int {
			return _maxBackBufferWidth;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set maxBackBufferWidth(value:int):void {
			_maxBackBufferWidth = value;
		}

		/**
		 * <p> The feature-support profile in use by this Context3D object. </p>
		 * 
		 * @return 
		 */
		public function get profile():String {
			return _profile;
		}

		/**
		 * <p> </p>
		 * <p>Returns the total GPU memory allocated by Stage3D data structures of an application.</p>
		 * <p>Whenever a GPU resource object is created, memory utilized is stored in Context3D. This memory includes index buffers, vertex buffers, textures(excluding video texture), and programs that were created through this Context3D.</p>
		 * <p>API <code>totalGPUMemory</code> returns the total memory consumed by the above resources to the user. Default value returned is 0.The total GPU memory returned is in bytes. The information is only provided in Direct mode on mobile, and in Direct and GPU modes on desktop. (On desktop, using <code>&lt;renderMode&gt;gpu&lt;/renderMode&gt;</code> will fall back to <code>&lt;renderMode&gt;direct&lt;/renderMode&gt;</code>) </p>
		 * 
		 * @return 
		 */
		public function get totalGPUMemory():Number {
			return _totalGPUMemory;
		}

		/**
		 * <p> Clears the color, depth, and stencil buffers associated with this Context3D object and fills them with the specified values. </p>
		 * <p>Set the <code>mask</code> parameter to specify which buffers to clear. Use the constants defined in the Context3DClearMask class to set the <code>mask</code> parameter. Use the bitwise OR operator, "|", to add multiple buffers to the mask (or use <code>Context3DClearMask.ALL</code>). When rendering to the back buffer, the <code>configureBackBuffer()</code> method must be called before any <code>clear()</code> calls.</p>
		 * <p><b>Note:</b> If you specify a parameter value outside the allowed range, Numeric parameter values are silently clamped to the range zero to one. Likewise, if <code>stencil</code> is greater than 0xff it is set to 0xff. </p>
		 * 
		 * @param red  — the red component of the color with which to clear the color buffer, in the range zero to one. 
		 * @param green  — the green component of the color with which to clear the color buffer, in the range zero to one. 
		 * @param blue  — the blue component of the color with which to clear the color buffer, in the range zero to one. 
		 * @param alpha  — the alpha component of the color with which to clear the color buffer, in the range zero to one. The alpha component is not used for blending. It is written to the buffer alpha directly. 
		 * @param depth  — the value with which to clear the depth buffer, in the range zero to one. 
		 * @param stencil  — the 8-bit value with which to clear the stencil buffer, in a range of 0x00 to 0xff. 
		 * @param mask  — specifies which buffers to clear. 
		 */
		public function clear(red:Number = 0.0, green:Number = 0.0, blue:Number = 0.0, alpha:Number = 1.0, depth:Number = 1.0, stencil:uint = 0, mask:uint = 0xffffffff):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets the viewport dimensions and other attributes of the rendering buffer. </p>
		 * <p>Rendering is double-buffered. The back buffer is swapped with the visible, front buffer when the <code>present()</code> method is called. The minimum size of the buffer is 32x32 pixels. The maximum size of the back buffer is limited by the device capabilities and can also be set by the user through the properties <code>maxBackBufferWidth</code> and <code>maxBackBufferHeight</code>. Configuring the buffer is a slow operation. Avoid changing the buffer size or attributes during normal rendering operations.</p>
		 * 
		 * @param width  — width in pixels of the buffer. 
		 * @param height  — height in pixels of the buffer. 
		 * @param antiAlias  — an integer value specifying the requested antialiasing quality. The value correlates to the number of subsamples used when antialiasing. Using more subsamples requires more calculations to be performed, although the relative performance impact depends on the specific rendering hardware. The type of antialiasing and whether antialiasing is performed at all is dependent on the device and rendering mode. Antialiasing is not supported at all by the software rendering context. <table class="+ topic/table adobe-d/adobetable ">
		 *  <tbody>
		 *   <tr>
		 *    <td>0</td>
		 *    <td>No antialiasing</td>
		 *   </tr>
		 *   <tr>
		 *    <td>2</td>
		 *    <td>Minimal antialiasing.</td>
		 *   </tr>
		 *   <tr>
		 *    <td>4</td>
		 *    <td>High-quality antialiasing.</td>
		 *   </tr>
		 *   <tr>
		 *    <td>16</td>
		 *    <td>Very high-quality antialiasing.</td>
		 *   </tr>
		 *  </tbody>
		 * </table> 
		 * @param enableDepthAndStencil  — <code>false</code> indicates no depth or stencil buffer is created, <code>true</code> creates a depth and a stencil buffer. For an AIR 3.2 or later application compiled with SWF version 15 or higher, if the <code>renderMode</code> element in the application descriptor file is <code>direct</code>, then the <code>depthAndStencil</code> element in the application descriptor file must have the same value as this argument. By default, the value of the <code>depthAndStencil</code> element is <code>false</code>. 
		 * @param wantsBestResolution  — <code>true</code> indicates that if the device supports HiDPI screens it will attempt to allocate a larger back buffer than indicated with the width and height parameters. Since this add more pixels and potentially changes the result of shader operations this is turned off by default. Use Stage.contentsScaleFactor to determine by how much the native back buffer was scaled up. 
		 * @param wantsBestResolutionOnBrowserZoom  — <code>true</code> indicates that the size of the back buffer should increase in proportion to the increase in the browser zoom factor. The setting of this value is persistent across multiple browser zooms. The default value of the parameter is <code>false</code>. Set <code>maxBackBufferWidth</code> and <code>maxBackBufferHeight</code> properties to limit the back buffer size increase. Use <code>backBufferWidth</code> and <code>backBufferHeight</code> to determine the current size of the back buffer. 
		 */
		public function configureBackBuffer(width:int, height:int, antiAlias:int, enableDepthAndStencil:Boolean = true, wantsBestResolution:Boolean = false, wantsBestResolutionOnBrowserZoom:Boolean = false):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Creates a CubeTexture object. </p>
		 * <p>Use a CubeTexture object to upload cube texture bitmaps to the rendering context and to reference a cube texture during rendering. A cube texture consists of six equal-sized, square textures arranged in a cubic topology and are useful for describing environment maps.</p>
		 * <p>You cannot create CubeTexture objects with a CubeTexture constructor; use this method instead. After creating a CubeTexture object, upload the texture bitmap data using the CubeTexture <code>uploadFromBitmapData()</code>, <code>uploadFromByteArray()</code>, or <code>uploadCompressedTextureFromByteArray()</code> methods..</p>
		 * 
		 * @param size  — The texture edge length in texels. 
		 * @param format  — The texel format, of the Context3DTextureFormat enumerated list. <p> Texture compression lets you store texture images in compressed format directly on the GPU, which saves GPU memory and memory bandwidth. Typically, compressed textures are compressed offline and uploaded to the GPU in compressed form using the Texture.uploadCompressedTextureFromByteArray method. Flash Player 11.4 and AIR 3.4 on desktop platforms added support for runtime texture compression, which may be useful in certain situations, such as when rendering dynamic textures from vector art. Note that this feature is not currently available on mobile platforms and an ArgumentError (Texture Format Mismatch) will be thrown instead. To use runtime texture compression, perform the following steps: 1. Create the texture object by calling the Context3D.createCubeTexture() method, passing either flash.display3D.Context3DTextureFormat.COMPRESSED or flash.display3D.Context3DTextureFormat.COMPRESSED_ALPHA as the format parameter. 2. Using the flash.display3D.textures.Texture instance returned by createCubeTexture(), call either flash.display3D.textures.CubeTexture.uploadFromBitmapData() or flash.display3D.textures.CubeTexture.uploadFromByteArray() to upload and compress the texture in one step. </p> 
		 * @param optimizeForRenderToTexture  — Set to <code>true</code> if the texture is likely to be used as a render target. 
		 * @param streamingLevels  — The MIP map level that must be loaded before the image is rendered. Texture streaming offers the ability to load and display the smallest mip levels first, progressively displaying higher quality images as the textures are loaded. End users can view lower-quality images in an application while the higher quality images load. <p>By default, streamingLevels is 0, meaning that the highest quality image in the MIP map must be loaded before the image is rendered. This parameter was added in Flash Player 11.3 and AIR 3.3. Using the default value maintains the behavior of the previous versions of Flash Player and AIR. </p> <p>Set <code>streamingLevels</code> to a value between 1 and the number of images in the MIP map to enable texture streaming. For example, you have a MIP map that includes at the highest quality a main image at 64x64 pixels. Lower quality images in the MIP map are 32x32, 16x16, 8x8, 4x4, 2x2, and 1x1 pixels, for 7 images in total, or 7 levels. Level 0 is the highest quality image. The maximum value of this property is log2(min(width,height)). Therefore, for a main image that is 64x64 pixels, the maximum value of <code>streamingLevels</code> is 7. Set this property to 3 to render the image after the 8x8 pixel image loads.</p> <p><b>Note: </b>Setting this property to a value &gt; 0 can impact memory usage and performance. </p> 
		 * @return 
		 */
		public function createCubeTexture(size:int, format:String, optimizeForRenderToTexture:Boolean, streamingLevels:int = 0):CubeTexture {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Creates an IndexBuffer3D object. </p>
		 * <p>Use an IndexBuffer3D object to upload a set of triangle indices to the rendering context and to reference that set of indices for rendering. Each index in the index buffer references a corresponding vertex in a vertex buffer. Each set of three indices identifies a triangle. Pass the IndexBuffer3D object to the <code>drawTriangles()</code> method to render one or more triangles defined in the index buffer.</p>
		 * <p>You cannot create IndexBuffer3D objects with the IndexBuffer3D class constructor; use this method instead. After creating a IndexBuffer3D object, upload the indices using the IndexBuffer3D <code>uploadFromVector()</code> or <code>uploadFromByteArray()</code> methods.</p>
		 * 
		 * @param numIndices  — the number of vertices to be stored in the buffer. 
		 * @param bufferUsage  — the expected buffer usage. Use one of the constants defined in <code>Context3DBufferUsage</code>. The hardware driver can do appropriate optimization when you set it correctly. This parameter is only available after Flash 12/AIR 4. 
		 * @return 
		 */
		public function createIndexBuffer(numIndices:int, bufferUsage:String = "staticDraw"):IndexBuffer3D {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Creates a Program3D object. </p>
		 * <p>Use a Program3D object to upload shader programs to the rendering context and to reference uploaded programs during rendering. A Program3D object stores two programs, a vertex program and a fragment program (also known as a pixel program). The programs are written in a binary shader assembly language.</p>
		 * <p>You cannot create Program3D objects with a Program3D constructor; use this method instead. After creating a Program3D object, upload the programs using the Program3D <code>upload()</code> method.</p>
		 * 
		 * @return 
		 */
		public function createProgram():Program3D {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Creates a Rectangle Texture object. </p>
		 * <p>Use a RectangleTexture object to upload texture bitmaps to the rendering context and to reference a texture during rendering.</p>
		 * <p>You cannot create RectangleTexture objects with a RectangleTexture constructor; use this method instead. After creating a RectangleTexture object, upload the texture bitmaps using the Texture <code>uploadFromBitmapData()</code> or <code>uploadFromByteArray()</code> methods.</p>
		 * <p>Note that 32-bit integer textures are stored in a packed BGRA format to match the Flash <code>BitmapData</code> format. Floating point textures use a conventional RGBA format. </p>
		 * <p> Rectangle textures are different from regular 2D textures in that their width and height do not have to be powers of two. Also, they do not contain mip maps. They are most useful for use in render to texture cases. If a rectangle texture is used with a sampler that uses mip map filtering or repeat wrapping the drawTriangles call will fail. Rectangle texture also do not allow streaming. The only texture formats supported by Rectangle textures are BGRA, BGR_PACKED, BGRA_PACKED. The compressed texture formats are not supported by Rectangle Textures. </p>
		 * 
		 * @param width  — The texture width in texels. 
		 * @param height  — The texture height in texels. 
		 * @param format  — The texel format, of the Context3DTextureFormat enumerated list. 
		 * @param optimizeForRenderToTexture  — Set to <code>true</code> if the texture is likely to be used as a render target. 
		 * @return 
		 */
		public function createRectangleTexture(width:int, height:int, format:String, optimizeForRenderToTexture:Boolean):RectangleTexture {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Creates a Texture object. </p>
		 * <p>Use a Texture object to upload texture bitmaps to the rendering context and to reference a texture during rendering.</p>
		 * <p>You cannot create Texture objects with a Texture constructor; use this method instead. After creating a Texture object, upload the texture bitmaps using the Texture <code>uploadFromBitmapData()</code>, <code>uploadFromByteArray()</code>, or <code>uploadCompressedTextureFromByteArray()</code> methods.</p>
		 * <p>Note that 32-bit integer textures are stored in a packed BGRA format to match the Flash <code>BitmapData</code> format. Floating point textures use a conventional RGBA format. </p>
		 * 
		 * @param width  — The texture width in texels. 
		 * @param height  — The texture height in texels. 
		 * @param format  — The texel format, of the Context3DTextureFormat enumerated list. <p> Texture compression lets you store texture images in compressed format directly on the GPU, which saves GPU memory and memory bandwidth. Typically, compressed textures are compressed offline and uploaded to the GPU in compressed form using the Texture.uploadCompressedTextureFromByteArray method. Flash Player 11.4 and AIR 3.4 on desktop platforms added support for runtime texture compression, which may be useful in certain situations, such as when rendering dynamic textures from vector art. Note that this feature is not currently available on mobile platforms and an ArgumentError (Texture Format Mismatch) will be thrown instead. To use runtime texture compression, perform the following steps: 1. Create the texture object by calling the Context3D.createTexture() method, passing either flash.display3D.Context3DTextureFormat.COMPRESSED or flash.display3D.Context3DTextureFormat.COMPRESSED_ALPHA as the format parameter. 2. Using the flash.display3D.textures.Texture instance returned by createTexture(), call either flash.display3D.textures.Texture.uploadFromBitmapData() or flash.display3D.textures.Texture.uploadFromByteArray() to upload and compress the texture in one step. </p> 
		 * @param optimizeForRenderToTexture  — Set to <code>true</code> if the texture is likely to be used as a render target. 
		 * @param streamingLevels  — The MIP map level that must be loaded before the image is rendered. Texture streaming offers the ability to load and display the smallest mip levels first, progressively displaying higher quality images as the textures are loaded. End users can view lower-quality images in an application while the higher quality images load. <p>By default, streamingLevels is 0, meaning that the highest quality image in the MIP map must be loaded before the image is rendered. This parameter was added in Flash Player 11.3 and AIR 3.3. Using the default value maintains the behavior of the previous versions of Flash Player and AIR. </p> <p>Set <code>streamingLevels</code> to a value between 1 and the number of images in the MIP map to enable texture streaming. For example, you have a MIP map that includes at the highest quality a main image at 64x64 pixels. Lower quality images in the MIP map are 32x32, 16x16, 8x8, 4x4, 2x2, and 1x1 pixels, for 7 images in total, or 7 levels. Level 0 is the highest quality image. The maximum value of this property is log2(min(width,height)). Therefore, for a main image that is 64x64 pixels, the maximum value of <code>streamingLevels</code> is 7. Set this property to 3 to render the image after the 8x8 pixel image loads.</p> <p><b>Note: </b>Setting this property to a value &gt; 0 can impact memory usage and performance. </p> 
		 * @return 
		 */
		public function createTexture(width:int, height:int, format:String, optimizeForRenderToTexture:Boolean, streamingLevels:int = 0):Texture {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Creates a VertexBuffer3D object. </p>
		 * <p>Use a VertexBuffer3D object to upload a set of vertex data to the rendering context. A vertex buffer contains the data needed to render each point in the scene geometry. The data attributes associated with each vertex typically includes position, color, and texture coordinates and serve as the input to the vertex shader program. Identify the data values that correspond to one of the inputs of the vertex program using the <code>setVertexBufferAt()</code> method. You can specify up to sixty-four 32-bit values for each vertex.</p>
		 * <p>You cannot create VertexBuffer3D objects with a VertexBuffer3D constructor; use this method instead. After creating a VertexBuffer3D object, upload the vertex data using the VertexBuffer3D <code>uploadFromVector()</code> or <code>uploadFromByteArray()</code> methods.</p>
		 * 
		 * @param numVertices  — the number of vertices to be stored in the buffer. The maximum number of vertices in a single buffer is 65535. 
		 * @param data32PerVertex  — the number of 32-bit(4-byte) data values associated with each vertex. The maximum number of 32-bit data elements per vertex is 64 (or 256 bytes). Note that only eight attribute registers are accessible by a vertex shader program at any given time. Use <code>SetVertextBufferAt()</code> to select attributes from within a vertex buffer. 
		 * @param bufferUsage  — the expected buffer usage. Use one of the constants defined in <code>Context3DBufferUsage</code>. The hardware driver can do appropriate optimization when you set it correctly. This parameter is only available after Flash 12/AIR 4 
		 * @return 
		 */
		public function createVertexBuffer(numVertices:int, data32PerVertex:int, bufferUsage:String = "staticDraw"):VertexBuffer3D {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Creates a VertexBuffer3D object for instances data. </p>
		 * <p>Use a VertexBuffer3D object to upload a set of instance data to the rendering context. The vertex buffer contains the data needed to render each instance in the scene geometry. Vertex Buffers with instance data provide attributes that are common to all the vertices of an instance. and serve as the input to the vertex shader program. Identify the data values that correspond to one of the inputs of the vertex program using the <code>setVertexBufferAt()</code> method. You can specify up to sixty-four 32-bit values for each element of vertex buffer.</p>
		 * <p>You cannot create VertexBuffer3D objects with a VertexBuffer3D constructor; use this method instead. After creating a VertexBuffer3D object, upload the vertex data using the VertexBuffer3D <code>uploadFromVector()</code> or <code>uploadFromByteArray()</code> methods.</p>
		 * 
		 * @param numVertices  — the number of elements to be stored in the buffer. The maximum number of elements in a single buffer is 65535. 
		 * @param data32PerVertex  — the number of 32-bit(4-byte) data values associated with each element. The maximum number of 32-bit data elements per vertex is 64 (or 256 bytes). 
		 * @param instancesPerElement  — the number of instances which will use one element of the vertex buffer. 
		 * @param bufferUsage  — the expected buffer usage. Use one of the constants defined in <code>Context3DBufferUsage</code>. The hardware driver can do appropriate optimization when you set it correctly. This parameter is only available after Flash 12/AIR 4 
		 * @return 
		 */
		public function createVertexBufferForInstances(numVertices:int, data32PerVertex:int, instancesPerElement:int, bufferUsage:String = "staticDraw"):VertexBuffer3D {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Creates a VideoTexture object. </p>
		 * <p>Use a VideoTexture object to obtain video frames as texture from NetStream object or Camera object and to upload the video frames to the rendering context.</p>
		 * <p>The VideoTexture object cannot be created with the VideoTexture constructor; use this method instead. After creating a VideoTexture object, attach NetStream object or Camera Object to get the video frames with the VideoTexture <code>attachNetStream()</code> or <code>attachCamera()</code> methods.</p>
		 * <p>Note that this method returns null if the system doesn't support this feature. </p>
		 * <p> VideoTexture does not contain mipmaps. If VideoTexture is used with a sampler that uses mip map filtering or repeat wrapping, the drawTriangles call will fail. VideoTexture can be treated as BGRA texture by the shaders. <b>The attempt to instantiate the VideoTexture Object will fail if the Context3D was requested with sotfware rendering mode.</b> </p>
		 * <p> A maximum of 4 VideoTexture objects are available per Context3D instance. On mobile the actual number of supported VideoTexture objects may be less than 4 due to platform limitations. </p>
		 * 
		 * @return 
		 */
		public function createVideoTexture():VideoTexture {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Frees all resources and internal storage associated with this Context3D. </p>
		 * <p>All index buffers, vertex buffers, textures, and programs that were created through this Context3D are disposed just as if calling <code>dispose()</code> on each of them individually. In addition, the Context3D itself is disposed freeing all temporary buffers and the back buffer. If you call configureBackBuffer(), clear(), drawTriangles(), createCubeTexture(), createTexture(), createProgram(), createIndexBuffer(), createVertexBuffer(), or drawToBitmapData() after calling dispose(), the runtime throws an exception.</p>
		 * <p><b>Warning</b>: calling dispose() on a Context3D while there is still a event listener for Events.CONTEXT3D_CREATE set on the asociated Stage3D object the dispose() call will simulate a device loss. It will create a new Context3D on the Stage3D and issue the Events.CONTEXT3D_CREATE event again. If this is not desired remove the event listener from the Stage3D object before calling dispose() or set the recreate parameter to false.</p>
		 * 
		 * @param recreate
		 */
		public function dispose(recreate:Boolean = true):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Draws the current render buffer to a bitmap. </p>
		 * <p>The current contents of the back render buffer are copied to a BitmapData object. This is potentially a very slow operation that can take up to a second. Use with care. Note that this function does not copy the front render buffer (the one shown on stage), but the buffer being drawn to. To capture the rendered image as it appears on the stage, call <code>drawToBitmapData()</code> immediately before you calling <code>present()</code>.</p>
		 * <p>Beginning with AIR 25, two new parameters have been introduced in the API <code>drawToBitmapData()</code>. This API now takes three parameters. The first one is the existing parameter <code>destination:BitmapData</code>. The second parameter is <code>srcRect:Rectangle</code>, which is target rectangle on stage3D. The third parameter is <code>destPoint:Point</code>, which is the coordinate on the destination bitmap. The parameters srcRect and destPoint are optional and default to (0,0,bitmapWidth,bitmapHeight) and (0,0), respectively.</p>
		 * <p>When the image is drawn, it is not scaled to fit the bitmap. Instead, the contents are clipped to the size of the destination bitmap.</p>
		 * <p>Flash BitmapData objects store colors already multiplied by the alpha component. For example, if the "pure" rgb color components of a pixel are (0x0A, 0x12, 0xBB) and the alpha component is 0x7F (.5), then the pixel is stored in the BitmapData object with the rgba values: (0x05, 0x09, 0x5D, 0x7F). You can set the blend factors so that the colors rendered to the buffer are multiplied by alpha or perform the operation in the fragment shader. The rendering context does not validate that the colors are stored in premultiplied format. </p>
		 * 
		 * @param destination
		 * @param srcRect
		 * @param destPoint
		 */
		public function drawToBitmapData(destination:BitmapData, srcRect:Rectangle = null, destPoint:Point = null):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Render the specified triangles using the current buffers and state of this Context3D object. </p>
		 * <p>For each triangle, the triangle vertices are processed by the vertex shader program and the triangle surface is processed by the pixel shader program. The output color from the pixel program for each pixel is drawn to the render target depending on the stencil operations, depth test, source and destination alpha, and the current blend mode. The render destination can be the main render buffer or a texture.</p>
		 * <p>If culling is enabled, (with the <code>setCulling()</code> method), then triangles can be discarded from the scene before the pixel program is run. If stencil and depth testing are enabled, then output pixels from the pixel program can be discarded without updating the render destination. In addition, the pixel program can decide not to output a color for a pixel.</p>
		 * <p>The rendered triangles are not displayed in the viewport until you call the <code>present()</code> method. After each <code>present()</code> call, the <code>clear()</code> method must be called before the first <code>drawTriangles()</code> call or rendering fails.</p>
		 * <p>When <code>enableErrorChecking</code> is <code>false</code>, this function returns immediately, does not wait for results, and throws exceptions only if this Context3D instance has been disposed or there are too many draw calls. If the rendering context state is invalid rendering fails silently. When the <code>enableErrorChecking</code> property is <code>true</code>, this function returns after the triangles are drawn and throws exceptions for any drawing errors or invalid context state.</p>
		 * 
		 * @param indexBuffer  — a set of vertex indices referencing the vertices to render. 
		 * @param firstIndex  — the index of the first vertex index selected to render. Default 0. 
		 * @param numTriangles  — the number of triangles to render. Each triangle consumes three indices. Pass -1 to draw all triangles in the index buffer. Default -1. 
		 */
		public function drawTriangles(indexBuffer:IndexBuffer3D, firstIndex:int = 0, numTriangles:int = -1):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Render the specified instanced triangles using the current buffers and state of this Context3D object. </p>
		 * <p>For each triangle of each instance, the triangle vertices are processed by the vertex shader program and the triangle surface is processed by the pixel shader program. The output color from the pixel program for each pixel is drawn to the render target depending on the stencil operations, depth test, source and destination alpha, and the current blend mode. The render destination can be the main render buffer or a texture.</p>
		 * <p>If culling is enabled, (with the <code>setCulling()</code> method), then triangles can be discarded from the scene before the pixel program is run. If stencil and depth testing are enabled, then output pixels from the pixel program can be discarded without updating the render destination. In addition, the pixel program can decide not to output a color for a pixel.</p>
		 * <p>The rendered instanced traingles are not displayed in the viewport until you call the <code>present()</code> method. After each <code>present()</code> call, the <code>clear()</code> method must be called before the first <code>drawTrianglesInstanced()</code> call or rendering fails.</p>
		 * <p>When <code>enableErrorChecking</code> is <code>false</code>, this function returns immediately, does not wait for results, and throws exceptions only if this Context3D instance has been disposed or there are too many draw calls. If the rendering context state is invalid rendering fails silently. When the <code>enableErrorChecking</code> property is <code>true</code>, this function returns after the triangles are drawn and throws exceptions for any drawing errors or invalid context state.</p>
		 * <p>This method may throw an exception if the instanced buffer is improperly sequenced with <code>SetVertexAt()</code>. With Direct 3D 9, for example, the indexed geometry data and the number of instances to draw must always be set in stream zero with <code>SetStreamSourceFreq()</code> API.</p>
		 * <p>This means that the vertex buffer created using <code>CreateVertexBufferForInstance()</code> should not be placed with the minimal index number when arranged with <code>SetVertexBufferAt()</code> as input to the vertex shader program. The vertex buffer generated using <code>CreateVertexBuffer()</code> must be placed with a smaller index number than that for <code>CreateVertexBufferForInstance()</code>. In general, the geometry data must be placed before per-instance data, with <code>SetVertexBufferAt()</code>.</p>
		 * 
		 * @param indexBuffer  — a set of vertex indices referencing the vertices to render. 
		 * @param numInstances  — number of instances to render. 
		 * @param firstIndex  — the index of the first vertex index selected to render. Default 0. 
		 * @param numTriangles  — the number of triangles to render. Each triangle consumes three indices. Pass -1 to draw all triangles in the index buffer. Default -1. 
		 */
		public function drawTrianglesInstanced(indexBuffer:IndexBuffer3D, numInstances:int, firstIndex:int = 0, numTriangles:int = -1):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Displays the back rendering buffer. </p>
		 * <p>Calling the <code>present()</code> method makes the results of all rendering operations since the last <code>present()</code> call visible and starts a new rendering cycle. After calling <code>present</code>, you must call <code>clear()</code> before making another <code>drawTriangles()</code> call. Otherwise, this function will alternately clear the render buffer to yellow and green or, if <code>enableErrorChecking</code> has been set to <code>true</code>, an exception is thrown.</p>
		 * <p>Calling <code>present()</code> also resets the render target, just like calling <code>setRenderToBackBuffer()</code>. </p>
		 */
		public function present():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Specifies the factors used to blend the output color of a drawing operation with the existing color. </p>
		 * <p>The output (source) color of the pixel shader program is combined with the existing (destination) color at that pixel according to the following formula:</p>
		 * <p><code>result color = (source color * sourceFactor) + (destination color * destinationFactor)</code></p>
		 * <p>The destination color is the current color in the render buffer for that pixel. Thus it is the result of the most recent <code>clear()</code> call and any intervening <code>drawTriangles()</code> calls. </p>
		 * <p>Use <code>setBlendFactors()</code> to set the factors used to multiply the source and destination colors before they are added together. The default blend factors are, <code>sourceFactor = Context3DBlendFactor.ONE</code>, and <code>destinationFactor = Context3DBlendFactor.ZERO</code>, which results in the source color overwriting the destination color (in other words, no blending of the two colors occurs). For normal alpha blending, use <code>sourceFactor = Context3DBlendFactor.SOURCE_ALPHA</code> and <code>destinationFactor = Context3DBlendFactor.ONE_MINUS_SOURCE_ALPHA</code>. </p>
		 * <p>Use the constants defined in the Context3DBlendFactor class to set the parameters of this function.</p>
		 * 
		 * @param sourceFactor  — The factor with which to multiply the source color. Defaults to <code>Context3DBlendFactor.ONE</code>. 
		 * @param destinationFactor  — The factor with which to multiply the destination color. Defaults to <code>Context3DBlendFactor.ZERO</code>. 
		 */
		public function setBlendFactors(sourceFactor:String, destinationFactor:String):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets the mask used when writing colors to the render buffer. </p>
		 * <p>Only color components for which the corresponding color mask parameter is <code>true</code> are updated when a color is written to the render buffer. For example, if you call: <code>setColorMask( true, false, false, false )</code>, only the red component of a color is written to the buffer until you change the color mask again. The color mask does not affect the behavior of the <code>clear()</code> method.</p>
		 * 
		 * @param red  — set <code>false</code> to block changes to the red channel. 
		 * @param green  — set <code>false</code> to block changes to the green channel. 
		 * @param blue  — set <code>false</code> to block changes to the blue channel. 
		 * @param alpha  — set <code>false</code> to block changes to the alpha channel. 
		 */
		public function setColorMask(red:Boolean, green:Boolean, blue:Boolean, alpha:Boolean):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets triangle culling mode. </p>
		 * <p>Triangles may be excluded from the scene early in the rendering pipeline based on their orientation relative to the view plane. Specify vertex order consistently (clockwise or counter-clockwise) as seen from the outside of the model to cull correctly.</p>
		 * 
		 * @param triangleFaceToCull  — the culling mode. Use one of the constants defined in the Context3DTriangleFace class. 
		 */
		public function setCulling(triangleFaceToCull:String):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets type of comparison used for depth testing. </p>
		 * <p>The depth of the source pixel output from the pixel shader program is compared to the current value in the depth buffer. If the comparison evaluates as <code>false</code>, then the source pixel is discarded. If <code>true</code>, then the source pixel is processed by the next step in the rendering pipeline, the stencil test. In addition, the depth buffer is updated with the depth of the source pixel, as long as the <code>depthMask</code> parameter is set to <code>true</code>.</p>
		 * <p>Sets the test used to compare depth values for source and destination pixels. The source pixel is composited with the destination pixel when the comparison is true. The comparison operator is applied as an infix operator between the source and destination pixel values, in that order.</p>
		 * 
		 * @param depthMask  — the destination depth value will be updated from the source pixel when true. 
		 * @param passCompareMode  — the depth comparison test operation. One of the values of Context3DCompareMode. 
		 */
		public function setDepthTest(depthMask:Boolean, passCompareMode:String):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Set fill mode used for render. The interface is only available in AIR desktop. </p>
		 * 
		 * @param fillMode  — if the value is WIREFRAME, the object will be shown in a mesh of lines. if the value is SOLID, the object will be shown in solid shaded polygons. 
		 */
		public function setFillMode(fillMode:String):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets vertex and fragment shader programs to use for subsequent rendering. </p>
		 * 
		 * @param program  — the Program3D object representing the vertex and fragment programs to use. 
		 */
		public function setProgram(program:Program3D):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Set constants for use by shader programs using values stored in a <code>ByteArray</code>. </p>
		 * <p>Sets constants that can be accessed from the vertex or fragment program.</p>
		 * 
		 * @param programType  — one of Context3DProgramType. 
		 * @param firstRegister  — the index of the first shader program constant to set. 
		 * @param numRegisters  — the number of registers to set. Every register is read as four float values. 
		 * @param data  — the source ByteArray object 
		 * @param byteArrayOffset  — an offset into the ByteArray for reading 
		 */
		public function setProgramConstantsFromByteArray(programType:String, firstRegister:int, numRegisters:int, data:ByteArray, byteArrayOffset:uint):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets constants for use by shader programs using values stored in a <code>Matrix3D</code>. </p>
		 * <p>Use this function to pass a matrix to a shader program. The function sets 4 constant registers used by the vertex or fragment program. The matrix is assigned to registers row by row. The first constant register is assigned the top row of the matrix. You can set 128 registers for a vertex program and 28 for a fragment program.</p>
		 * 
		 * @param programType  — The type of shader program, either <code>Context3DProgramType.VERTEX</code> or <code>Context3DProgramType.FRAGMENT</code>. 
		 * @param firstRegister  — the index of the first constant register to set. Since a Matrix3D has 16 values, four registers are set. 
		 * @param matrix  — the matrix containing the constant values. 
		 * @param transposedMatrix  — if <code>true</code> the matrix entries are copied to registers in transposed order. The default value is <code>false</code>. 
		 */
		public function setProgramConstantsFromMatrix(programType:String, firstRegister:int, matrix:Matrix3D, transposedMatrix:Boolean = false):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets the constant inputs for the shader programs. </p>
		 * <p>Sets an array of constants to be accessed by a vertex or fragment shader program. Constants set in Program3D are accessed within the shader programs as constant registers. Each constant register is comprised of 4 floating point values (x, y, z, w). Therefore every register requires 4 entries in the data Vector. The number of registers that you can set for vertex program and fragment program depends on the <code>Context3DProfile</code>.</p>
		 * 
		 * @param programType  — The type of shader program, either <code>Context3DProgramType.VERTEX</code> or <code>Context3DProgramType.FRAGMENT</code>. 
		 * @param firstRegister  — the index of the first constant register to set. 
		 * @param data  — the floating point constant values. There must be at least <code>numRegisters</code> 4 elements in <code>data</code>. 
		 * @param numRegisters  — the number of constants to set. Specify -1, the default value, to set enough registers to use all of the available data. 
		 */
		public function setProgramConstantsFromVector(programType:String, firstRegister:int, data:Vector.<Number>, numRegisters:int = -1):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets the back rendering buffer as the render target. Subsequent calls to <code>drawTriangles()</code> and <code>clear()</code> methods result in updates to the back buffer. Use this method to resume normal rendering after using the <code>setRenderToTexture()</code> method. </p>
		 */
		public function setRenderToBackBuffer():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets the specified texture as the rendering target. </p>
		 * <p>Subsequent calls to <code>drawTriangles()</code> and <code>clear()</code> methods update the specified texture instead of the back buffer. Mip maps are created automatically. Use the <code>setRenderToBackBuffer()</code> to resume normal rendering to the back buffer.</p>
		 * <p>No clear is needed before drawing. If there is no clear operation, the render content will be retained. depth buffer and stencil buffer will also not be cleared. But it is forced to clear when first drawing. Calling <code>present()</code> resets the target to the back buffer.</p>
		 * 
		 * @param texture  — the target texture to render into. Set to <code>null</code> to resume rendering to the back buffer (<code>setRenderToBackBuffer()</code> and <code>present</code> also reset the target to the back buffer). 
		 * @param enableDepthAndStencil  — if <code>true</code>, depth and stencil testing are available. If <code>false</code>, all depth and stencil state is ignored for subsequent drawing operations. 
		 * @param antiAlias  — the antialiasing quality. Use 0 to disable antialiasing; higher values improve antialiasing quality, but require more calculations. The value is currently ignored by mobile platform and software rendering context. 
		 * @param surfaceSelector  — specifies which element of the texture to update. Texture objects have one surface, so you must specify 0, the default value. CubeTexture objects have six surfaces, so you can specify an integer from 0 through 5. 
		 * @param colorOutputIndex  — The output color register. Must be 0 for constrained or baseline mode. Otherwise specifies the output color register. 
		 */
		public function setRenderToTexture(texture:TextureBase, enableDepthAndStencil:Boolean = false, antiAlias:int = 0, surfaceSelector:int = 0, colorOutputIndex:int = 0):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Manually override texture sampler state. </p>
		 * <p>Texture sampling state is typically set at the time <code>setProgram</code> is called. However, you can override texture sampler state with this function. If you do not want the program to change sampler state, set the <code>ignoresamnpler</code> bit in AGAL and use this function. </p>
		 * 
		 * @param sampler  — sampler The sampler register to use. Maps to the sampler register in AGAL. 
		 * @param wrap  — Wrapping mode. Defined in <code>Context3DWrapMode</code>. The default is repeat. 
		 * @param filter  — Texture filtering mode. Defined in <code>Context3DTextureFilter</code>. The default is nearest. 
		 * @param mipfilter  — Mip map filter. Defined in <code>Context3DMipFilter</code>. The default is none. 
		 */
		public function setSamplerStateAt(sampler:int, wrap:String, filter:String, mipfilter:String):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets a scissor rectangle, which is type of drawing mask. The renderer only draws to the area inside the scissor rectangle. Scissoring does not affect clear operations. </p>
		 * <p>Pass <code>null</code> to turn off scissoring.</p>
		 * 
		 * @param rectangle  — The rectangle in which to draw. Specify the rectangle position and dimensions in pixels. The coordinate system origin is the top left corner of the viewport, with positive values increasing down and to the right (the same as the normal Flash display coordinate system). 
		 */
		public function setScissorRectangle(rectangle:Rectangle):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets stencil mode and operation. </p>
		 * <p>An 8-bit stencil reference value can be associated with each draw call. During rendering, the reference value can be tested against values stored previously in the frame buffer. The result of the test can control the draw action and whether or how the stored stencil value is updated. In addition, depth testing controls whether stencil testing is performed. A failed depth test can also be used to control the action taken on the stencil buffer.</p>
		 * <p>In the pixel processing pipeline, depth testing is performed first. If the depth test fails, a stencil buffer update action can be taken, but no further evaluation of the stencil buffer value can be made. If the depth test passes, then the stencil test is performed. Alternate actions can be taken depending on the outcome of the stencil test. </p>
		 * <p>The stencil reference value is set using <code>setStencilReferenceValue()</code>. </p>
		 * 
		 * @param triangleFace  — the triangle orientations allowed to contribute to the stencil operation. One of Context3DTriangleFace. 
		 * @param compareMode  — the test operator used to compare the current stencil reference value and the destination pixel stencil value. Destination pixel color and depth update is performed when the comparison is true. The stencil actions are performed as requested in the following action parameters. The comparison operator is applied as an infix operator between the current and destination reference values, in that order (in pseudocode: <code>if stencilReference OPERATOR stencilBuffer then pass</code>). Use one of the constants defined in the Context3DCompareMode class. 
		 * @param actionOnBothPass  — action to be taken when both depth and stencil comparisons pass. Use one of the constants defined in the Context3DStencilAction class. 
		 * @param actionOnDepthFail  — action to be taken when depth comparison fails. Use one of the constants defined in the Context3DStencilAction class. 
		 * @param actionOnDepthPassStencilFail  — action to be taken when depth comparison passes and the stencil comparison fails. Use one of the constants defined in the Context3DStencilAction class. 
		 */
		public function setStencilActions(triangleFace:String = "frontAndBack", compareMode:String = "always", actionOnBothPass:String = "keep", actionOnDepthFail:String = "keep", actionOnDepthPassStencilFail:String = "keep"):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets the stencil comparison value used for stencil tests. </p>
		 * <p>Only the lower 8 bits of the reference value are used. The stencil buffer value is also 8 bits in length. Use the <code>readMask</code> and <code>writeMask</code> to use the stencil buffer as a bit field.</p>
		 * 
		 * @param referenceValue  — an 8-bit reference value used in reference value comparison tests. 
		 * @param readMask  — an 8-bit mask for applied to both the current stencil buffer value and the reference value before the comparison. 
		 * @param writeMask  — an 8-bit mask applied to the reference value before updating the stencil buffer. 
		 */
		public function setStencilReferenceValue(referenceValue:uint, readMask:uint = 255, writeMask:uint = 255):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Specifies the texture to use for a texture input register of a fragment program. </p>
		 * <p>A fragment program can read information from up to eight texture objects. Use this function to assign a Texture or CubeTexture object to one of the sampler registers used by the fragment program. </p>
		 * <p><b>Note:</b> if you change the active fragment program (with <code>setProgram</code>) to a shader that uses fewer textures, set the unused registers to <code>null</code>:</p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *          setTextureAt( 7, null );
		 *          </pre>
		 * </div>
		 * 
		 * @param sampler  — the sampler register index, a value from 0 through 7. 
		 * @param texture  — the texture object to make available, either a Texture or a CubeTexture instance. 
		 */
		public function setTextureAt(sampler:int, texture:TextureBase):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Specifies which vertex data components correspond to a single vertex shader program input. </p>
		 * <p>Use the <code>setVertexBufferAt</code> method to identify which components of the data defined for each vertex in a VertexBuffer3D buffer belong to which inputs to the vertex program. The developer of the vertex program determines how much data is needed per vertex. That data is mapped from 1 or more <code>VertexBuffer3D</code> stream(s) to the attribute registers of the vertex shader program.</p>
		 * <p>The smallest unit of data consumed by the vertex shader is a 32-bit data. Offsets into the vertex stream are specified in multiples of 32-bits.</p>
		 * <pre>
		 * position:  x    float32
		 *            y    float32
		 *            z    float32
		 * color:     r    unsigned byte
		 *            g    unsigned byte
		 *            b    unsigned byte
		 *            a    unsigned byte
		 * </pre>
		 * <code>buffer</code>
		 * <pre>
		 * setVertexBufferAt( 0, buffer, 0, Context3DVertexBufferFormat.FLOAT_3 );   // attribute #0 will contain the position information
		 * setVertexBufferAt( 1, buffer, 3, Context3DVertexBufferFormat.BYTES_4 );    // attribute #1 will contain the color information
		 * </pre>
		 * 
		 * @param index  — the index of the attribute register in the vertex shader (0 through 7). 
		 * @param buffer  — the buffer that contains the source vertex data to be fed to the vertex shader. 
		 * @param bufferOffset  — an offset from the start of the data for a single vertex at which to start reading this attribute. In the example above, the position data has an offset of 0 because it is the first attribute; color has an offset of 3 because the color attribute follows the three 32-bit position values. The offset is specified in units of 32 bits. 
		 * @param format  — a value from the Context3DVertexBufferFormat class specifying the data type of this attribute. 
		 */
		public function setVertexBufferAt(index:int, buffer:VertexBuffer3D, bufferOffset:int = 0, format:String = "float4"):void {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Indicates if Context3D supports video texture. </p>
		 * 
		 * @return 
		 */
		public static function get supportsVideoTexture():Boolean {
			return _supportsVideoTexture;
		}
	}
}
