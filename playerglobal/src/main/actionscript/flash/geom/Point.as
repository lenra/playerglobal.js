package flash.geom {
	/**
	 *  The Point object represents a location in a two-dimensional coordinate system, where <i>x</i> represents the horizontal axis and <i>y</i> represents the vertical axis. <p>The following code creates a point at (0,0):</p> <div class="listing">
	 *  <div class="clipcopy">
	 *   <a href="#" class="copyText">Copy</a>
	 *  </div>
	 *  <pre>var myPoint:Point = new Point();</pre>
	 * </div> <p>Methods and properties of the following classes use Point objects:</p> <ul> 
	 *  <li>BitmapData</li> 
	 *  <li>DisplayObject</li> 
	 *  <li>DisplayObjectContainer</li> 
	 *  <li>DisplacementMapFilter</li> 
	 *  <li>NativeWindow</li> 
	 *  <li>Matrix</li> 
	 *  <li>Rectangle</li> 
	 * </ul> <p>You can use the <code>new Point()</code> constructor to create a Point object.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7dc5.html" target="_blank">Translating coordinate spaces</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7dc4.html" target="_blank">Moving a display object by a specified angle and distance</a>
	 * </div><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7dca.html" target="_blank">Using Point objects</a>
	 *  <br>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../flash/display/BitmapData.html" target="">flash.display.BitmapData</a>
	 *  <br>
	 *  <a href="../../flash/display/DisplayObject.html" target="">flash.display.DisplayObject</a>
	 *  <br>
	 *  <a href="../../flash/display/DisplayObjectContainer.html" target="">flash.display.DisplayObjectContainer</a>
	 *  <br>
	 *  <a href="../../flash/filters/DisplacementMapFilter.html" target="">flash.filters.DisplacementMapFilter</a>
	 *  <br>
	 *  <a href="Matrix.html" target="">flash.geom.Matrix</a>
	 *  <br>
	 *  <a href="../../flash/display/NativeWindow.html" target="">flash.display.NativeWindow</a>
	 *  <br>
	 *  <a href="Rectangle.html" target="">flash.geom.Rectangle</a>
	 * </div><br><hr>
	 */
	public class Point {
		private var _x:Number;
		private var _y:Number;

		private var _length:Number;

		/**
		 * <p> Creates a new point. If you pass no parameters to this method, a point is created at (0,0). </p>
		 * 
		 * @param x  — The horizontal coordinate. 
		 * @param y  — The vertical coordinate. 
		 */
		public function Point(x:Number = 0, y:Number = 0) {
			this.x = x;
			this.y = y;
		}

		/**
		 * <p> The length of the line segment from (0,0) to this point. </p>
		 * 
		 * @return 
		 */
		public function get length():Number {
			return _length;
		}

		/**
		 * <p> The horizontal coordinate of the point. The default value is 0. </p>
		 * <p id="moreExamples"><span class="label">More examples</span></p>
		 * <div class="seeAlso">
		 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7dca.html" target="_blank">Using Point objects</a>
		 * </div>
		 * 
		 * @return 
		 */
		public function get x():Number {
			return _x;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set x(value:Number):void {
			_x = value;
		}

		/**
		 * <p> The vertical coordinate of the point. The default value is 0. </p>
		 * <p id="moreExamples"><span class="label">More examples</span></p>
		 * <div class="seeAlso">
		 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7dca.html" target="_blank">Using Point objects</a>
		 * </div>
		 * 
		 * @return 
		 */
		public function get y():Number {
			return _y;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set y(value:Number):void {
			_y = value;
		}

		/**
		 * <p> Adds the coordinates of another point to the coordinates of this point to create a new point. </p>
		 * 
		 * @param v  — The point to be added. 
		 * @return  — The new point. 
		 */
		public function add(v:Point):Point {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Creates a copy of this Point object. </p>
		 * 
		 * @return  — The new Point object. 
		 */
		public function clone():Point {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Copies all of the point data from the source Point object into the calling Point object. </p>
		 * 
		 * @param sourcePoint  — The Point object from which to copy the data. 
		 */
		public function copyFrom(sourcePoint:Point):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Determines whether two points are equal. Two points are equal if they have the same <i>x</i> and <i>y</i> values. </p>
		 * 
		 * @param toCompare  — The point to be compared. 
		 * @return  — A value of  if the object is equal to this Point object;  if it is not equal. 
		 */
		public function equals(toCompare:Point):Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Scales the line segment between (0,0) and the current point to a set length. </p>
		 * 
		 * @param thickness  — The scaling value. For example, if the current point is (0,5), and you normalize it to 1, the point returned is at (0,1). 
		 */
		public function normalize(thickness:Number):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Offsets the Point object by the specified amount. The value of <code>dx</code> is added to the original value of <i>x</i> to create the new <i>x</i> value. The value of <code>dy</code> is added to the original value of <i>y</i> to create the new <i>y</i> value. </p>
		 * 
		 * @param dx  — The amount by which to offset the horizontal coordinate, <i>x</i>. 
		 * @param dy  — The amount by which to offset the vertical coordinate, <i>y</i>. 
		 */
		public function offset(dx:Number, dy:Number):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets the members of Point to the specified values </p>
		 * 
		 * @param xa  — the values to set the point to. 
		 * @param ya
		 */
		public function setTo(xa:Number, ya:Number):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Subtracts the coordinates of another point from the coordinates of this point to create a new point. </p>
		 * 
		 * @param v  — The point to be subtracted. 
		 * @return  — The new point. 
		 */
		public function subtract(v:Point):Point {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a string that contains the values of the <i>x</i> and <i>y</i> coordinates. The string has the form <code>"(x=<i>x</i>, y=<i>y</i>)"</code>, so calling the <code>toString()</code> method for a point at 23,17 would return <code>"(x=23, y=17)"</code>. </p>
		 * 
		 * @return  — The string representation of the coordinates. 
		 */
		public function toString():String {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Returns the distance between <code>pt1</code> and <code>pt2</code>. </p>
		 * 
		 * @param pt1  — The first point. 
		 * @param pt2  — The second point. 
		 * @return  — The distance between the first and second points. 
		 */
		public static function distance(pt1:Point, pt2:Point):Number {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Determines a point between two specified points. The parameter <code>f</code> determines where the new interpolated point is located relative to the two end points specified by parameters <code>pt1</code> and <code>pt2</code>. The closer the value of the parameter <code>f</code> is to <code>1.0</code>, the closer the interpolated point is to the first point (parameter <code>pt1</code>). The closer the value of the parameter <code>f</code> is to 0, the closer the interpolated point is to the second point (parameter <code>pt2</code>). </p>
		 * 
		 * @param pt1  — The first point. 
		 * @param pt2  — The second point. 
		 * @param f  — The level of interpolation between the two points. Indicates where the new point will be, along the line between <code>pt1</code> and <code>pt2</code>. If <code>f</code>=1, <code>pt1</code> is returned; if <code>f</code>=0, <code>pt2</code> is returned. 
		 * @return  — The new, interpolated point. 
		 */
		public static function interpolate(pt1:Point, pt2:Point, f:Number):Point {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Converts a pair of polar coordinates to a Cartesian point coordinate. </p>
		 * 
		 * @param len  — The length coordinate of the polar pair. 
		 * @param angle  — The angle, in radians, of the polar pair. 
		 * @return  — The Cartesian point. 
		 */
		public static function polar(len:Number, angle:Number):Point {
			throw new Error("Not implemented");
		}
	}
}
