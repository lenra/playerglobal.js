package flash.geom {
	/**
	 *  A Rectangle object is an area defined by its position, as indicated by its top-left corner point (<i>x</i>, <i>y</i>) and by its width and its height. <p>The <code>x</code>, <code>y</code>, <code>width</code>, and <code>height</code> properties of the Rectangle class are independent of each other; changing the value of one property has no effect on the others. However, the <code>right</code> and <code>bottom</code> properties are integrally related to those four properties. For example, if you change the value of the <code>right</code> property, the value of the <code>width</code> property changes; if you change the <code>bottom</code> property, the value of the <code>height</code> property changes. </p> <p>The following methods and properties use Rectangle objects:</p> <ul> 
	 *  <li>The <code>applyFilter()</code>, <code>colorTransform()</code>, <code>copyChannel()</code>, <code>copyPixels()</code>, <code>draw()</code>, <code>fillRect()</code>, <code>generateFilterRect()</code>, <code>getColorBoundsRect()</code>, <code>getPixels()</code>, <code>merge()</code>, <code>paletteMap()</code>, <code>pixelDisolve()</code>, <code>setPixels()</code>, and <code>threshold()</code> methods, and the <code>rect</code> property of the BitmapData class</li> 
	 *  <li>The <code>getBounds()</code> and <code>getRect()</code> methods, and the <code>scrollRect</code> and <code>scale9Grid</code> properties of the DisplayObject class</li> 
	 *  <li>The <code>getCharBoundaries()</code> method of the TextField class</li> 
	 *  <li>The <code>pixelBounds</code> property of the Transform class</li> 
	 *  <li>The <code>bounds</code> parameter for the <code>startDrag()</code> method of the Sprite class</li> 
	 *  <li>The <code>printArea</code> parameter of the <code>addPage()</code> method of the PrintJob class</li> 
	 * </ul> <p>You can use the <code>new Rectangle()</code> constructor to create a Rectangle object.</p> <p> <b>Note:</b> The Rectangle class does not define a rectangular Shape display object. To draw a rectangular Shape object onscreen, use the <code>drawRect()</code> method of the Graphics class.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7dc2.html" target="_blank">Resizing and repositioning Rectangle objects</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7dc1.html" target="_blank">Finding unions and intersections of Rectangle objects</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7cc6.html" target="_blank">Setting size, scale, and orientation</a>
	 * </div><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7dc9.html" target="_blank">Using Rectangle objects</a>
	 *  <br>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../flash/display/DisplayObject.html#scrollRect" target="">flash.display.DisplayObject.scrollRect</a>
	 *  <br>
	 *  <a href="../../flash/display/BitmapData.html" target="">flash.display.BitmapData</a>
	 *  <br>
	 *  <a href="../../flash/display/DisplayObject.html" target="">flash.display.DisplayObject</a>
	 *  <br>
	 *  <a href="../../flash/display/NativeWindow.html" target="">flash.display.NativeWindow</a>
	 *  <br>
	 *  <a href="../../flash/text/TextField.html#getCharBoundaries()" target="">flash.text.TextField.getCharBoundaries()</a>
	 *  <br>
	 *  <a href="Transform.html#pixelBounds" target="">flash.geom.Transform.pixelBounds</a>
	 *  <br>
	 *  <a href="../../flash/display/Sprite.html#startDrag()" target="">flash.display.Sprite.startDrag()</a>
	 *  <br>
	 *  <a href="../../flash/printing/PrintJob.html#addPage()" target="">flash.printing.PrintJob.addPage()</a>
	 * </div><br><hr>
	 */
	public class Rectangle {
		private var _height:Number;
		private var _width:Number;
		private var _x:Number;
		private var _y:Number;

		/**
		 * <p> Creates a new Rectangle object with the top-left corner specified by the <code>x</code> and <code>y</code> parameters and with the specified <code>width</code> and <code>height</code> parameters. If you call this function without parameters, a rectangle with <code>x</code>, <code>y</code>, <code>width</code>, and <code>height</code> properties set to 0 is created. </p>
		 * 
		 * @param x  — The <i>x</i> coordinate of the top-left corner of the rectangle. 
		 * @param y  — The <i>y</i> coordinate of the top-left corner of the rectangle. 
		 * @param width  — The width of the rectangle, in pixels. 
		 * @param height  — The height of the rectangle, in pixels. 
		 */
		public function Rectangle(x:Number = 0, y:Number = 0, width:Number = 0, height:Number = 0) {
			this.x = x;
			this.y = y;
			this.width = width;
			this.height = height;
		}

		/**
		 * <p> The sum of the <code>y</code> and <code>height</code> properties. </p>
		 * <p><img src="../../images/rectangle.jpg" alt="A rectangle image showing location and measurement properties."></p>
		 * 
		 * @return 
		 */
		public function get bottom():Number {
			return y + height;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set bottom(value:Number):void {
			height = value - y;
		}

		/**
		 * <p> The location of the Rectangle object's bottom-right corner, determined by the values of the <code>right</code> and <code>bottom</code> properties. </p>
		 * <p><img src="../../images/rectangle.jpg" alt="A rectangle image showing location and measurement properties."></p>
		 * 
		 * @return 
		 */
		public function get bottomRight():Point {
			return new Point(right, bottom);
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set bottomRight(value:Point):void {
			right = value.x;
			bottom = value.y;
		}

		/**
		 * <p> The height of the rectangle, in pixels. Changing the <code>height</code> value of a Rectangle object has no effect on the <code>x</code>, <code>y</code>, and <code>width</code> properties. </p>
		 * <p><img src="../../images/rectangle.jpg" alt="A rectangle image showing location and measurement properties."></p>
		 * <p id="moreExamples"><span class="label">More examples</span></p>
		 * <div class="seeAlso">
		 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7dc9.html" target="_blank">Using Rectangle objects</a>
		 * </div>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Rectangle.html#x" target="">x</a>
		 *  <br>
		 *  <a href="Rectangle.html#y" target="">y</a>
		 *  <br>
		 *  <a href="Rectangle.html#height" target="">height</a>
		 * </div>
		 * 
		 * @return 
		 */
		public function get height():Number {
			return _height;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set height(value:Number):void {
			_height = value;
		}

		/**
		 * <p> The <i>x</i> coordinate of the top-left corner of the rectangle. Changing the <code>left</code> property of a Rectangle object has no effect on the <code>y</code> and <code>height</code> properties. However it does affect the <code>width</code> property, whereas changing the <code>x</code> value does <i>not</i> affect the <code>width</code> property. </p>
		 * <p>The value of the <code>left</code> property is equal to the value of the <code>x</code> property.</p>
		 * <p><img src="../../images/rectangle.jpg" alt="A rectangle image showing location and measurement properties."></p>
		 * 
		 * @return 
		 */
		public function get left():Number {
			return x;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set left(value:Number):void {
			x = value;
		}

		/**
		 * <p> The sum of the <code>x</code> and <code>width</code> properties. </p>
		 * <p><img src="../../images/rectangle.jpg" alt="A rectangle image showing location and measurement properties."></p>
		 * 
		 * @return 
		 */
		public function get right():Number {
			return x + width;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set right(value:Number):void {
			width = value - x;
		}

		/**
		 * <p> The size of the Rectangle object, expressed as a Point object with the values of the <code>width</code> and <code>height</code> properties. </p>
		 * 
		 * @return 
		 */
		public function get size():Point {
			return new Point(width, height);
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set size(value:Point):void {
			width = value.x;
			height = value.y;
		}

		/**
		 * <p> The <i>y</i> coordinate of the top-left corner of the rectangle. Changing the <code>top</code> property of a Rectangle object has no effect on the <code>x</code> and <code>width</code> properties. However it does affect the <code>height</code> property, whereas changing the <code>y</code> value does <i>not</i> affect the <code>height</code> property. </p>
		 * <p>The value of the <code>top</code> property is equal to the value of the <code>y</code> property.</p>
		 * <p><img src="../../images/rectangle.jpg" alt="A rectangle image showing location and measurement properties."></p>
		 * 
		 * @return 
		 */
		public function get top():Number {
			return y;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set top(value:Number):void {
			y = value;
		}

		/**
		 * <p> The location of the Rectangle object's top-left corner, determined by the <i>x</i> and <i>y</i> coordinates of the point. </p>
		 * <p><img src="../../images/rectangle.jpg" alt="A rectangle image showing location and measurement properties."></p>
		 * 
		 * @return 
		 */
		public function get topLeft():Point {
			return new Point(left, top);
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set topLeft(value:Point):void {
			left = value.x;
			top = value.y;
		}

		/**
		 * <p> The width of the rectangle, in pixels. Changing the <code>width</code> value of a Rectangle object has no effect on the <code>x</code>, <code>y</code>, and <code>height</code> properties. </p>
		 * <p><img src="../../images/rectangle.jpg" alt="A rectangle image showing location and measurement properties."></p>
		 * <p id="moreExamples"><span class="label">More examples</span></p>
		 * <div class="seeAlso">
		 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7dc9.html" target="_blank">Using Rectangle objects</a>
		 * </div>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Rectangle.html#x" target="">x</a>
		 *  <br>
		 *  <a href="Rectangle.html#y" target="">y</a>
		 *  <br>
		 *  <a href="Rectangle.html#height" target="">height</a>
		 * </div>
		 * 
		 * @return 
		 */
		public function get width():Number {
			return _width;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set width(value:Number):void {
			_width = value;
		}

		/**
		 * <p> The <i>x</i> coordinate of the top-left corner of the rectangle. Changing the value of the <code>x</code> property of a Rectangle object has no effect on the <code>y</code>, <code>width</code>, and <code>height</code> properties. </p>
		 * <p>The value of the <code>x</code> property is equal to the value of the <code>left</code> property.</p>
		 * <p id="moreExamples"><span class="label">More examples</span></p>
		 * <div class="seeAlso">
		 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7dc9.html" target="_blank">Using Rectangle objects</a>
		 * </div>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Rectangle.html#left" target="">left</a>
		 * </div>
		 * 
		 * @return 
		 */
		public function get x():Number {
			return _x;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set x(value:Number):void {
			_x = value;
		}

		/**
		 * <p> The <i>y</i> coordinate of the top-left corner of the rectangle. Changing the value of the <code>y</code> property of a Rectangle object has no effect on the <code>x</code>, <code>width</code>, and <code>height</code> properties. </p>
		 * <p>The value of the <code>y</code> property is equal to the value of the <code>top</code> property.</p>
		 * <p id="moreExamples"><span class="label">More examples</span></p>
		 * <div class="seeAlso">
		 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7dc9.html" target="_blank">Using Rectangle objects</a>
		 * </div>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Rectangle.html#x" target="">x</a>
		 *  <br>
		 *  <a href="Rectangle.html#width" target="">width</a>
		 *  <br>
		 *  <a href="Rectangle.html#height" target="">height</a>
		 *  <br>
		 *  <a href="Rectangle.html#top" target="">top</a>
		 * </div>
		 * 
		 * @return 
		 */
		public function get y():Number {
			return _y;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set y(value:Number):void {
			_y = value;
		}

		/**
		 * <p> Returns a new Rectangle object with the same values for the <code>x</code>, <code>y</code>, <code>width</code>, and <code>height</code> properties as the original Rectangle object. </p>
		 * 
		 * @return  — A new Rectangle object with the same values for the , , , and  properties as the original Rectangle object. 
		 */
		public function clone():Rectangle {
			return new Rectangle(x, y, width, height);
		}

		/**
		 * <p> Determines whether the specified point is contained within the rectangular region defined by this Rectangle object. </p>
		 * 
		 * @param x  — The <i>x</i> coordinate (horizontal position) of the point. 
		 * @param y  — The <i>y</i> coordinate (vertical position) of the point. 
		 * @return  — A value of  if the Rectangle object contains the specified point; otherwise . 
		 */
		public function contains(x:Number, y:Number):Boolean {
			return this.x <= x && this.x + this.width >= x && this.y <= y && this.y + this.height >= y;
		}

		/**
		 * <p> Determines whether the specified point is contained within the rectangular region defined by this Rectangle object. This method is similar to the <code>Rectangle.contains()</code> method, except that it takes a Point object as a parameter. </p>
		 * 
		 * @param point  — The point, as represented by its <i>x</i> and <i>y</i> coordinates. 
		 * @return  — A value of  if the Rectangle object contains the specified point; otherwise . 
		 */
		public function containsPoint(point:Point):Boolean {
			return contains(point.x, point.y);
		}

		/**
		 * <p> Determines whether the Rectangle object specified by the <code>rect</code> parameter is contained within this Rectangle object. A Rectangle object is said to contain another if the second Rectangle object falls entirely within the boundaries of the first. </p>
		 * 
		 * @param rect  — The Rectangle object being checked. 
		 * @return  — A value of  if the Rectangle object that you specify is contained by this Rectangle object; otherwise . 
		 */
		public function containsRect(rect:Rectangle):Boolean {
			return containsPoint(rect.topLeft) && containsPoint(rect.bottomRight);
		}

		/**
		 * <p> Copies all of rectangle data from the source Rectangle object into the calling Rectangle object. </p>
		 * 
		 * @param sourceRect  — The Rectangle object from which to copy the data. 
		 */
		public function copyFrom(sourceRect:Rectangle):void {
			this.x = sourceRect.x;
			this.y = sourceRect.y;
			this.width = sourceRect.width;
			this.height = sourceRect.height;
		}

		/**
		 * <p> Determines whether the object specified in the <code>toCompare</code> parameter is equal to this Rectangle object. This method compares the <code>x</code>, <code>y</code>, <code>width</code>, and <code>height</code> properties of an object against the same properties of this Rectangle object. </p>
		 * 
		 * @param toCompare  — The rectangle to compare to this Rectangle object. 
		 * @return  — A value of  if the object has exactly the same values for the , , , and  properties as this Rectangle object; otherwise . 
		 */
		public function equals(toCompare:Rectangle):Boolean {
			return this.x == toCompare.x && this.y == toCompare.y && this.width == toCompare.width && this.height == toCompare.height;
		}

		/**
		 * <p> Increases the size of the Rectangle object by the specified amounts, in pixels. The center point of the Rectangle object stays the same, and its size increases to the left and right by the <code>dx</code> value, and to the top and the bottom by the <code>dy</code> value. </p>
		 * 
		 * @param dx  — The value to be added to the left and the right of the Rectangle object. The following equation is used to calculate the new width and position of the rectangle: <div class="listing">
		 *  <pre>
		 *     x -= dx;
		 *     width += 2 * dx;
		 *     </pre>
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 * </div> 
		 * @param dy  — The value to be added to the top and the bottom of the Rectangle. The following equation is used to calculate the new height and position of the rectangle: <div class="listing">
		 *  <pre>
		 *     y -= dy;
		 *     height += 2 * dy;
		 *     </pre>
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 * </div> 
		 */
		public function inflate(dx:Number, dy:Number):void {
			x -= dx;
			width += 2 * dx;
			y -= dy;
			height += 2 * dy;
		}

		/**
		 * <p> Increases the size of the Rectangle object. This method is similar to the <code>Rectangle.inflate()</code> method except it takes a Point object as a parameter. </p>
		 * <p>The following two code examples give the same result:</p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *      var rect1:Rectangle = new Rectangle(0,0,2,5);
		 *      rect1.inflate(2,2)
		 *      </pre>
		 * </div>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *      var rect1:Rectangle = new Rectangle(0,0,2,5);
		 *      var pt1:Point = new Point(2,2);
		 *      rect1.inflatePoint(pt1)
		 *      </pre>
		 * </div>
		 * 
		 * @param point  — The <code>x</code> property of this Point object is used to increase the horizontal dimension of the Rectangle object. The <code>y</code> property is used to increase the vertical dimension of the Rectangle object. 
		 */
		public function inflatePoint(point:Point):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> If the Rectangle object specified in the <code>toIntersect</code> parameter intersects with this Rectangle object, returns the area of intersection as a Rectangle object. If the rectangles do not intersect, this method returns an empty Rectangle object with its properties set to 0. </p>
		 * <p><img src="../../images/rectangle_intersect.jpg" alt="The resulting intersection rectangle."></p>
		 * 
		 * @param toIntersect  — The Rectangle object to compare against to see if it intersects with this Rectangle object. 
		 * @return  — A Rectangle object that equals the area of intersection. If the rectangles do not intersect, this method returns an empty Rectangle object; that is, a rectangle with its , , , and  properties set to 0. 
		 */
		public function intersection(toIntersect:Rectangle):Rectangle {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Determines whether the object specified in the <code>toIntersect</code> parameter intersects with this Rectangle object. This method checks the <code>x</code>, <code>y</code>, <code>width</code>, and <code>height</code> properties of the specified Rectangle object to see if it intersects with this Rectangle object. </p>
		 * 
		 * @param toIntersect  — The Rectangle object to compare against this Rectangle object. 
		 * @return  — A value of  if the specified object intersects with this Rectangle object; otherwise . 
		 */
		public function intersects(toIntersect:Rectangle):Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Determines whether or not this Rectangle object is empty. </p>
		 * 
		 * @return  — A value of  if the Rectangle object's width or height is less than or equal to 0; otherwise . 
		 */
		public function isEmpty():Boolean {
			return width <= 0 || height <= 0;
		}

		/**
		 * <p> Adjusts the location of the Rectangle object, as determined by its top-left corner, by the specified amounts. </p>
		 * 
		 * @param dx  — Moves the <i>x</i> value of the Rectangle object by this amount. 
		 * @param dy  — Moves the <i>y</i> value of the Rectangle object by this amount. 
		 */
		public function offset(dx:Number, dy:Number):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Adjusts the location of the Rectangle object using a Point object as a parameter. This method is similar to the <code>Rectangle.offset()</code> method, except that it takes a Point object as a parameter. </p>
		 * 
		 * @param point  — A Point object to use to offset this Rectangle object. 
		 */
		public function offsetPoint(point:Point):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets all of the Rectangle object's properties to 0. A Rectangle object is empty if its width or height is less than or equal to 0. </p>
		 * <p> This method sets the values of the <code>x</code>, <code>y</code>, <code>width</code>, and <code>height</code> properties to 0.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Rectangle.html#x" target="">x</a>
		 *  <br>
		 *  <a href="Rectangle.html#y" target="">y</a>
		 *  <br>
		 *  <a href="Rectangle.html#width" target="">width</a>
		 *  <br>
		 *  <a href="Rectangle.html#height" target="">height</a>
		 * </div>
		 */
		public function setEmpty():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets the members of Rectangle to the specified values </p>
		 * 
		 * @param xa  — the values to set the rectangle to. 
		 * @param ya
		 * @param widtha
		 * @param heighta
		 */
		public function setTo(xa:Number, ya:Number, widtha:Number, heighta:Number):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Builds and returns a string that lists the horizontal and vertical positions and the width and height of the Rectangle object. </p>
		 * 
		 * @return  — A string listing the value of each of the following properties of the Rectangle object: , , , and . 
		 */
		public function toString():String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Adds two rectangles together to create a new Rectangle object, by filling in the horizontal and vertical space between the two rectangles. </p>
		 * <p><img src="../../images/rectangle_union.jpg" alt="The resulting union rectangle."></p>
		 * <p><b>Note:</b> The <code>union()</code> method ignores rectangles with <code>0</code> as the height or width value, such as: <code>var rect2:Rectangle = new Rectangle(300,300,50,0);</code></p>
		 * 
		 * @param toUnion  — A Rectangle object to add to this Rectangle object. 
		 * @return  — A new Rectangle object that is the union of the two rectangles. 
		 */
		public function union(toUnion:Rectangle):Rectangle {
			throw new Error("Not implemented");
		}
	}
}
