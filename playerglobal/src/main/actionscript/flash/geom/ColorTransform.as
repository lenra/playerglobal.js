package flash.geom {
	/**
	 *  The ColorTransform class lets you adjust the color values in a display object. The color adjustment or <i>color transformation</i> can be applied to all four channels: red, green, blue, and alpha transparency. <p>When a ColorTransform object is applied to a display object, a new value for each color channel is calculated like this:</p> <ul> 
	 *  <li>New red value = (old red value * <code>redMultiplier</code>) + <code>redOffset</code> </li> 
	 *  <li>New green value = (old green value * <code>greenMultiplier</code>) + <code>greenOffset</code> </li> 
	 *  <li>New blue value = (old blue value * <code>blueMultiplier</code>) + <code>blueOffset</code> </li> 
	 *  <li>New alpha value = (old alpha value * <code>alphaMultiplier</code>) + <code>alphaOffset</code> </li> 
	 * </ul> <p>If any of the color channel values is greater than 255 after the calculation, it is set to 255. If it is less than 0, it is set to 0.</p> <p>You can use ColorTransform objects in the following ways:</p> <ul> 
	 *  <li>In the <code>colorTransform</code> parameter of the <code>colorTransform()</code> method of the BitmapData class</li> 
	 *  <li>As the <code>colorTransform</code> property of a Transform object (which can be used as the <code>transform</code> property of a display object)</li> 
	 * </ul> <p>You must use the <code>new ColorTransform()</code> constructor to create a ColorTransform object before you can call the methods of the ColorTransform object.</p> <p>Color transformations do not apply to the background color of a movie clip (such as a loaded SWF object). They apply only to graphics and symbols that are attached to the movie clip.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WSda78ed3a750d6b8f-6cd35b9c12528366210-8000.html" target="_blank">Color transformations in FXG and MXML graphics</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e16.html" target="_blank">Adjusting DisplayObject colors</a>
	 * </div><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WSda78ed3a750d6b8fee1b36612357de97a3-8000.html" target="_blank">MXML graphics</a>
	 *  <br>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Transform.html" target="">flash.geom.Transform</a>
	 *  <br>
	 *  <a href="../../flash/display/DisplayObject.html#transform" target="">flash.display.DisplayObject.transform</a>
	 *  <br>
	 *  <a href="../../flash/display/BitmapData.html#colorTransform()" target="">flash.display.BitmapData.colorTransform()</a>
	 * </div><br><hr>
	 */
	public class ColorTransform {
		private var _alphaMultiplier:Number;
		private var _alphaOffset:Number;
		private var _blueMultiplier:Number;
		private var _blueOffset:Number;
		private var _color:uint;
		private var _greenMultiplier:Number;
		private var _greenOffset:Number;
		private var _redMultiplier:Number;
		private var _redOffset:Number;

		/**
		 * <p> Creates a ColorTransform object for a display object with the specified color channel values and alpha values. </p>
		 * 
		 * @param redMultiplier  — The value for the red multiplier, in the range from 0 to 1. 
		 * @param greenMultiplier  — The value for the green multiplier, in the range from 0 to 1. 
		 * @param blueMultiplier  — The value for the blue multiplier, in the range from 0 to 1. 
		 * @param alphaMultiplier  — The value for the alpha transparency multiplier, in the range from 0 to 1. 
		 * @param redOffset  — The offset value for the red color channel, in the range from -255 to 255. 
		 * @param greenOffset  — The offset value for the green color channel, in the range from -255 to 255. 
		 * @param blueOffset  — The offset for the blue color channel value, in the range from -255 to 255. 
		 * @param alphaOffset  — The offset for alpha transparency channel value, in the range from -255 to 255. 
		 */
		public function ColorTransform(redMultiplier:Number = 1.0, greenMultiplier:Number = 1.0, blueMultiplier:Number = 1.0, alphaMultiplier:Number = 1.0, redOffset:Number = 0, greenOffset:Number = 0, blueOffset:Number = 0, alphaOffset:Number = 0) {
			throw new Error("Not implemented");
		}

		/**
		 * <p> A decimal value that is multiplied with the alpha transparency channel value. </p>
		 * <p>If you set the alpha transparency value of a display object directly by using the <code>alpha</code> property of the DisplayObject instance, it affects the value of the <code>alphaMultiplier</code> property of that display object's <code>transform.colorTransform</code> property.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/DisplayObject.html#alpha" target="">flash.display.DisplayObject.alpha</a>
		 * </div>
		 * 
		 * @return 
		 */
		public function get alphaMultiplier():Number {
			return _alphaMultiplier;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set alphaMultiplier(value:Number):void {
			_alphaMultiplier = value;
		}

		/**
		 * <p> A number from -255 to 255 that is added to the alpha transparency channel value after it has been multiplied by the <code>alphaMultiplier</code> value. </p>
		 * 
		 * @return 
		 */
		public function get alphaOffset():Number {
			return _alphaOffset;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set alphaOffset(value:Number):void {
			_alphaOffset = value;
		}

		/**
		 * <p> A decimal value that is multiplied with the blue channel value. </p>
		 * 
		 * @return 
		 */
		public function get blueMultiplier():Number {
			return _blueMultiplier;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set blueMultiplier(value:Number):void {
			_blueMultiplier = value;
		}

		/**
		 * <p> A number from -255 to 255 that is added to the blue channel value after it has been multiplied by the <code>blueMultiplier</code> value. </p>
		 * 
		 * @return 
		 */
		public function get blueOffset():Number {
			return _blueOffset;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set blueOffset(value:Number):void {
			_blueOffset = value;
		}

		/**
		 * <p> The RGB color value for a ColorTransform object. </p>
		 * <p>When you set this property, it changes the three color offset values (<code>redOffset</code>, <code>greenOffset</code>, and <code>blueOffset</code>) accordingly, and it sets the three color multiplier values (<code>redMultiplier</code>, <code>greenMultiplier</code>, and <code>blueMultiplier</code>) to 0. The alpha transparency multiplier and offset values do not change.</p>
		 * <p>When you pass a value for this property, use the format 0x<i>RRGGBB</i>. <i>RR</i>, <i>GG</i>, and <i>BB</i> each consist of two hexadecimal digits that specify the offset of each color component. The 0x tells the ActionScript compiler that the number is a hexadecimal value.</p>
		 * 
		 * @return 
		 */
		public function get color():uint {
			return _color;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set color(value:uint):void {
			_color = value;
		}

		/**
		 * <p> A decimal value that is multiplied with the green channel value. </p>
		 * 
		 * @return 
		 */
		public function get greenMultiplier():Number {
			return _greenMultiplier;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set greenMultiplier(value:Number):void {
			_greenMultiplier = value;
		}

		/**
		 * <p> A number from -255 to 255 that is added to the green channel value after it has been multiplied by the <code>greenMultiplier</code> value. </p>
		 * 
		 * @return 
		 */
		public function get greenOffset():Number {
			return _greenOffset;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set greenOffset(value:Number):void {
			_greenOffset = value;
		}

		/**
		 * <p> A decimal value that is multiplied with the red channel value. </p>
		 * 
		 * @return 
		 */
		public function get redMultiplier():Number {
			return _redMultiplier;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set redMultiplier(value:Number):void {
			_redMultiplier = value;
		}

		/**
		 * <p> A number from -255 to 255 that is added to the red channel value after it has been multiplied by the <code>redMultiplier</code> value. </p>
		 * 
		 * @return 
		 */
		public function get redOffset():Number {
			return _redOffset;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set redOffset(value:Number):void {
			_redOffset = value;
		}

		/**
		 * <p> Concatenates the ColorTranform object specified by the <code>second</code> parameter with the current ColorTransform object and sets the current object as the result, which is an additive combination of the two color transformations. When you apply the concatenated ColorTransform object, the effect is the same as applying the <code>second</code> color transformation after the <i>original</i> color transformation. </p>
		 * 
		 * @param second  — The ColorTransform object to be combined with the current ColorTransform object. 
		 */
		public function concat(second:ColorTransform):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Formats and returns a string that describes all of the properties of the ColorTransform object. </p>
		 * 
		 * @return  — A string that lists all of the properties of the ColorTransform object. 
		 */
		public function toString():String {
			throw new Error("Not implemented");
		}
	}
}
