package flash.geom {
	/**
	 *  The Matrix class represents a transformation matrix that determines how to map points from one coordinate space to another. You can perform various graphical transformations on a display object by setting the properties of a Matrix object, applying that Matrix object to the <code>matrix</code> property of a Transform object, and then applying that Transform object as the <code>transform</code> property of the display object. These transformation functions include translation (<i>x</i> and <i>y</i> repositioning), rotation, scaling, and skewing. <p>Together these types of transformations are known as <i>affine transformations</i>. Affine transformations preserve the straightness of lines while transforming, so that parallel lines stay parallel.</p> <p>To apply a transformation matrix to a display object, you create a Transform object, set its <code>matrix</code> property to the transformation matrix, and then set the <code>transform</code> property of the display object to the Transform object. Matrix objects are also used as parameters of some methods, such as the following:</p> <ul> 
	 *  <li>The <code>draw()</code> method of a BitmapData object</li> 
	 *  <li>The <code>beginBitmapFill()</code> method, <code>beginGradientFill()</code> method, or <code>lineGradientStyle()</code> method of a Graphics object</li> 
	 * </ul> <p>A transformation matrix object is a 3 x 3 matrix with the following contents:</p> <p> <img src="../../images/matrix_props1.jpg" alt="Matrix class properties in matrix notation"> </p> <p>In traditional transformation matrixes, the <code>u</code>, <code>v</code>, and <code>w</code> properties provide extra capabilities. The Matrix class can only operate in two-dimensional space, so it always assumes that the property values <code>u</code> and <code>v</code> are 0.0, and that the property value <code>w</code> is 1.0. The effective values of the matrix are as follows:</p> <p> <img src="../../images/matrix_props2.jpg" alt="Matrix class properties in matrix notation showing    assumed values for u, v, and w"> </p> <p>You can get and set the values of all six of the other properties in a Matrix object: <code>a</code>, <code>b</code>, <code>c</code>, <code>d</code>, <code>tx</code>, and <code>ty</code>.</p> <p>The Matrix class supports the four major types of transformations: translation, scaling, rotation, and skewing. You can set three of these transformations by using specialized methods, as described in the following table: </p> <table class="innertable">
	 *  <tbody>
	 *   <tr>
	 *    <th>Transformation</th>
	 *    <th>Method</th>
	 *    <th>Matrix values</th>
	 *    <th>Display result</th>
	 *    <th>Description</th>
	 *   </tr>
	 *   <tr>
	 *    <td>Translation (displacement)</td>
	 *    <td> <code>translate(tx, ty)</code> </td>
	 *    <td> <img src="../../images/matrix_translate.jpg" alt="Matrix notation of translate method parameters"> </td>
	 *    <td> <img src="../../images/matrix_translate_image.jpg" alt="Illustration of translate method effects"> </td>
	 *    <td>Moves the image <code>tx</code> pixels to the right and <code>ty</code> pixels down.</td>
	 *   </tr>
	 *   <tr>
	 *    <td>Scaling</td>
	 *    <td> <code>scale(sx, sy)</code> </td>
	 *    <td> <img src="../../images/matrix_scale.jpg" alt="Matrix notation of scale method parameters"> </td>
	 *    <td> <img src="../../images/matrix_scale_image.jpg" alt="Illustration of scale method effects"> </td>
	 *    <td>Resizes the image, multiplying the location of each pixel by <code>sx</code> on the <i>x</i> axis and <code>sy</code> on the <i>y</i> axis.</td>
	 *   </tr>
	 *   <tr>
	 *    <td>Rotation</td>
	 *    <td> <code>rotate(q)</code> </td>
	 *    <td> <img src="../../images/matrix_rotate.jpg" alt="Matrix notation of rotate method properties"> </td>
	 *    <td> <img src="../../images/matrix_rotate_image.jpg" alt="Illustration of rotate method effects"> </td>
	 *    <td>Rotates the image by an angle <code>q</code>, which is measured in radians.</td>
	 *   </tr>
	 *   <tr>
	 *    <td>Skewing or shearing </td>
	 *    <td>None; must set the properties <code>b</code> and <code>c</code> </td>
	 *    <td> <img src="../../images/matrix_skew.jpg" alt="Matrix notation of skew function properties"> </td>
	 *    <td> <img src="../../images/matrix_skew_image.jpg" alt="Illustration of skew function effects"> </td>
	 *    <td>Progressively slides the image in a direction parallel to the <i>x</i> or <i>y</i> axis. The <code>b</code> property of the Matrix object represents the tangent of the skew angle along the <i>y</i> axis; the <code>c</code> property of the Matrix object represents the tangent of the skew angle along the <i>x</i> axis.</td>
	 *   </tr>
	 *  </tbody>
	 * </table> <p>Each transformation function alters the current matrix properties so that you can effectively combine multiple transformations. To do this, you call more than one transformation function before applying the matrix to its display object target (by using the <code>transform</code> property of that display object).</p> <p>Use the <code>new Matrix()</code> constructor to create a Matrix object before you can call the methods of the Matrix object.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WSda78ed3a750d6b8fee1b36612357de97a3-8000.html" target="_blank">MXML graphics</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7ddb.html" target="_blank">Using Matrix objects</a>
	 *  <br>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../flash/display/DisplayObject.html#transform" target="">flash.display.DisplayObject.transform</a>
	 *  <br>
	 *  <a href="Transform.html" target="">flash.geom.Transform</a>
	 *  <br>
	 *  <a href="../../flash/display/BitmapData.html#draw()" target="">flash.display.BitmapData.draw()</a>
	 *  <br>
	 *  <a href="../../flash/display/Graphics.html#beginBitmapFill()" target="">flash.display.Graphics.beginBitmapFill()</a>
	 *  <br>
	 *  <a href="../../flash/display/Graphics.html#beginGradientFill()" target="">flash.display.Graphics.beginGradientFill()</a>
	 *  <br>
	 *  <a href="../../flash/display/Graphics.html#lineGradientStyle()" target="">flash.display.Graphics.lineGradientStyle()</a>
	 * </div><br><hr>
	 */
	public class Matrix {
		private var _a:Number;
		private var _b:Number;
		private var _c:Number;
		private var _d:Number;
		private var _tx:Number;
		private var _ty:Number;

		/**
		 * <p> Creates a new Matrix object with the specified parameters. In matrix notation, the properties are organized like this: </p>
		 * <p><img src="../../images/matrix_props2.jpg" alt="Matrix class properties in matrix notation showing assumed values for u, v, and w"></p>
		 * <p>If you do not provide any parameters to the <code>new Matrix()</code> constructor, it creates an <i>identity matrix</i> with the following values:</p>
		 * <pre>a = 1</pre>
		 * <pre>b = 0</pre>
		 * <pre>c = 0</pre>
		 * <pre>d = 1</pre>
		 * <pre>tx = 0</pre>
		 * <pre>ty = 0</pre>
		 * <p>In matrix notation, the identity matrix looks like this:</p>
		 * <p><img src="../../images/matrix_identity.jpg" alt="Matrix class properties in matrix notation"></p>
		 * 
		 * @param a  — The value that affects the positioning of pixels along the <i>x</i> axis when scaling or rotating an image. 
		 * @param b  — The value that affects the positioning of pixels along the <i>y</i> axis when rotating or skewing an image. 
		 * @param c  — The value that affects the positioning of pixels along the <i>x</i> axis when rotating or skewing an image. 
		 * @param d  — The value that affects the positioning of pixels along the <i>y</i> axis when scaling or rotating an image.. 
		 * @param tx  — The distance by which to translate each point along the <i>x</i> axis. 
		 * @param ty  — The distance by which to translate each point along the <i>y</i> axis. 
		 */
		public function Matrix(a:Number = 1, b:Number = 0, c:Number = 0, d:Number = 1, tx:Number = 0, ty:Number = 0) {
			this.a = a;
			this.b = b;
			this.c = c;
			this.d = d;
			this.tx = tx;
			this.ty = ty;
		}

		/**
		 * <p> The value that affects the positioning of pixels along the <i>x</i> axis when scaling or rotating an image. </p>
		 * <span id="pageFilter"><br><span class="label"> Example &nbsp;( <span class="usage"><a href="http://www.adobe.com/go/learn_as3_usingexamples_en"> How to use this example </a></span>) </span><br>
		 *  <div class="detailBody">
		 *    The following example creates the Matrix object 
		 *   <code>myMatrix</code> and sets its 
		 *   <code>a</code> value. 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>
		 * import flash.geom.Matrix;
		 * 
		 * var myMatrix:Matrix = new Matrix();
		 * trace(myMatrix.a);  // 1
		 * 
		 * myMatrix.a = 2;
		 * trace(myMatrix.a);  // 2
		 * </pre>
		 *   </div>
		 *  </div></span>
		 * 
		 * @return 
		 */
		public function get a():Number {
			return _a;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set a(value:Number):void {
			_a = value;
		}

		/**
		 * <p> The value that affects the positioning of pixels along the <i>y</i> axis when rotating or skewing an image. </p>
		 * <span id="pageFilter"><br><span class="label"> Example &nbsp;( <span class="usage"><a href="http://www.adobe.com/go/learn_as3_usingexamples_en"> How to use this example </a></span>) </span><br>
		 *  <div class="detailBody">
		 *    The following example creates the Matrix object 
		 *   <code>myMatrix</code> and sets its 
		 *   <code>b</code> value. 
		 *   <div class="listing">
		 *    <pre>
		 * import flash.geom.Matrix;
		 *  
		 * var myMatrix:Matrix = new Matrix();
		 * trace(myMatrix.b);  // 0
		 * 
		 * var degrees:Number = 30;
		 * var radians:Number = (degrees/180) * Math.PI;
		 * myMatrix.b = Math.tan(radians);
		 * trace(myMatrix.b);  // 0.5773502691896257
		 * </pre>
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *   </div>
		 *  </div></span>
		 * 
		 * @return 
		 */
		public function get b():Number {
			return _b;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set b(value:Number):void {
			_b = value;
		}

		/**
		 * <p> The value that affects the positioning of pixels along the <i>x</i> axis when rotating or skewing an image. </p>
		 * <span id="pageFilter"><br><span class="label"> Example &nbsp;( <span class="usage"><a href="http://www.adobe.com/go/learn_as3_usingexamples_en"> How to use this example </a></span>) </span><br>
		 *  <div class="detailBody">
		 *    The following example creates the Matrix object 
		 *   <code>myMatrix</code> and sets its 
		 *   <code>c</code> value. 
		 *   <div class="listing">
		 *    <pre>
		 * import flash.geom.Matrix;
		 * 
		 * var myMatrix:Matrix = new Matrix();
		 * trace(myMatrix.c);  // 0
		 * 
		 * var degrees:Number = 30;
		 * var radians:Number = (degrees/180) * Math.PI;
		 * myMatrix.c = Math.tan(radians);
		 * trace(myMatrix.c);  // 0.5773502691896257
		 * </pre>
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *   </div>
		 *  </div></span>
		 * 
		 * @return 
		 */
		public function get c():Number {
			return _c;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set c(value:Number):void {
			_c = value;
		}

		/**
		 * <p> The value that affects the positioning of pixels along the <i>y</i> axis when scaling or rotating an image. </p>
		 * <span id="pageFilter"><br><span class="label"> Example &nbsp;( <span class="usage"><a href="http://www.adobe.com/go/learn_as3_usingexamples_en"> How to use this example </a></span>) </span><br>
		 *  <div class="detailBody">
		 *    The following example creates the Matrix object 
		 *   <code>myMatrix</code> and sets its 
		 *   <code>d</code> value. 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>
		 * import flash.geom.Matrix;
		 * 
		 * var myMatrix:Matrix = new Matrix();
		 * trace(myMatrix.d);  // 1
		 * 
		 * myMatrix.d = 2;
		 * trace(myMatrix.d);  // 2
		 * </pre>
		 *   </div>
		 *  </div></span>
		 * 
		 * @return 
		 */
		public function get d():Number {
			return _d;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set d(value:Number):void {
			_d = value;
		}

		/**
		 * <p> The distance by which to translate each point along the <i>x</i> axis. </p>
		 * <span id="pageFilter"><br><span class="label"> Example &nbsp;( <span class="usage"><a href="http://www.adobe.com/go/learn_as3_usingexamples_en"> How to use this example </a></span>) </span><br>
		 *  <div class="detailBody">
		 *    The following example creates the Matrix object 
		 *   <code>myMatrix</code> and sets its 
		 *   <code>tx</code> value. 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>
		 * import flash.geom.Matrix;
		 * 
		 * var myMatrix:Matrix = new Matrix();
		 * trace(myMatrix.tx);  // 0
		 * 
		 * myMatrix.tx = 50;  // 50
		 * trace(myMatrix.tx);
		 * </pre>
		 *   </div>
		 *  </div></span>
		 * 
		 * @return 
		 */
		public function get tx():Number {
			return _tx;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set tx(value:Number):void {
			_tx = value;
		}

		/**
		 * <p> The distance by which to translate each point along the <i>y</i> axis. </p>
		 * <span id="pageFilter"><br><span class="label"> Example &nbsp;( <span class="usage"><a href="http://www.adobe.com/go/learn_as3_usingexamples_en"> How to use this example </a></span>) </span><br>
		 *  <div class="detailBody">
		 *    The following example creates the Matrix object 
		 *   <code>myMatrix</code> and sets its 
		 *   <code>ty</code> value. 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>
		 * import flash.geom.Matrix;
		 * 
		 * var myMatrix:Matrix = new Matrix();
		 * trace(myMatrix.ty);  // 0
		 * 
		 * myMatrix.ty = 50;
		 * trace(myMatrix.ty);  // 50
		 * </pre>
		 *   </div>
		 *  </div></span>
		 * 
		 * @return 
		 */
		public function get ty():Number {
			return _ty;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set ty(value:Number):void {
			_ty = value;
		}

		/**
		 * <p> Returns a new Matrix object that is a clone of this matrix, with an exact copy of the contained object. </p>
		 * 
		 * @return  — A Matrix object. 
		 */
		public function clone():Matrix {
			return new Matrix(a, b, c, d, tx, ty);
		}

		/**
		 * <p> Concatenates a matrix with the current matrix, effectively combining the geometric effects of the two. In mathematical terms, concatenating two matrixes is the same as combining them using matrix multiplication. </p>
		 * <p>For example, if matrix <code>m1</code> scales an object by a factor of four, and matrix <code>m2</code> rotates an object by 1.5707963267949 radians (<code>Math.PI/2</code>), then <code>m1.concat(m2)</code> transforms <code>m1</code> into a matrix that scales an object by a factor of four and rotates the object by <code>Math.PI/2</code> radians. </p>
		 * <p>This method replaces the source matrix with the concatenated matrix. If you want to concatenate two matrixes without altering either of the two source matrixes, first copy the source matrix by using the <code>clone()</code> method, as shown in the Class Examples section.</p>
		 * 
		 * @param m  — The matrix to be concatenated to the source matrix. 
		 */
		public function concat(m:Matrix):void {
			var a:Number =  this.a * m.a;
			var b:Number =  0.0;
			var c:Number =  0.0;
			var d:Number =  this.d * m.d
			var tx:Number = this.tx * m.a + m.tx;
			var ty:Number = this.ty * m.d + m.ty;

			if (this.b != 0 || this.c != 0 || m.b != 0 || m.c != 0) {
				a  += this.b * m.c;
				d  += this.c * m.b;
				b  += this.a * m.b + this.b * m.d;
				c  += this.c * m.a + this.d * m.c;
				tx += this.tx * m.c;
				ty += this.tx * m.b;
			}

			this.a = a;
			this.b = b;
			this.c = c;
			this.d = d;
			this.tx = tx;
			this.ty = ty;
		}

		/**
		 * <p> Copies a Vector3D object into specific column of the calling Matrix3D object. </p>
		 * 
		 * @param column  — The column from which to copy the data from. 
		 * @param vector3D  — The Vector3D object from which to copy the data. 
		 */
		public function copyColumnFrom(column:uint, vector3D:Vector3D):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Copies specific column of the calling Matrix object into the Vector3D object. The w element of the Vector3D object will not be changed. </p>
		 * 
		 * @param column  — The column from which to copy the data from. 
		 * @param vector3D  — The Vector3D object from which to copy the data. 
		 */
		public function copyColumnTo(column:uint, vector3D:Vector3D):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Copies all of the matrix data from the source Point object into the calling Matrix object. </p>
		 * 
		 * @param sourceMatrix  — The Matrix object from which to copy the data. 
		 */
		public function copyFrom(sourceMatrix:Matrix):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Copies a Vector3D object into specific row of the calling Matrix object. </p>
		 * 
		 * @param row  — The row from which to copy the data from. 
		 * @param vector3D  — The Vector3D object from which to copy the data. 
		 */
		public function copyRowFrom(row:uint, vector3D:Vector3D):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Copies specific row of the calling Matrix object into the Vector3D object. The w element of the Vector3D object will not be changed. </p>
		 * 
		 * @param row  — The row from which to copy the data from. 
		 * @param vector3D  — The Vector3D object from which to copy the data. 
		 */
		public function copyRowTo(row:uint, vector3D:Vector3D):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Includes parameters for scaling, rotation, and translation. When applied to a matrix it sets the matrix's values based on those parameters. </p>
		 * <p>Using the <code>createBox()</code> method lets you obtain the same matrix as you would if you applied the <code>identity()</code>, <code>rotate()</code>, <code>scale()</code>, and <code>translate()</code> methods in succession. For example, <code>mat1.createBox(2,2,Math.PI/4, 100, 100)</code> has the same effect as the following:</p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *      import flash.geom.Matrix;
		 *      
		 *      var mat1:Matrix = new Matrix();
		 *      mat1.identity();
		 *      mat1.rotate(Math.PI/4);
		 *      mat1.scale(2,2);
		 *      mat1.translate(10,20);
		 *      </pre>
		 * </div>
		 * 
		 * @param scaleX  — The factor by which to scale horizontally. 
		 * @param scaleY  — The factor by which scale vertically. 
		 * @param rotation  — The amount to rotate, in radians. 
		 * @param tx  — The number of pixels to translate (move) to the right along the <i>x</i> axis. 
		 * @param ty  — The number of pixels to translate (move) down along the <i>y</i> axis. 
		 */
		public function createBox(scaleX:Number, scaleY:Number, rotation:Number = 0, tx:Number = 0, ty:Number = 0):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Creates the specific style of matrix expected by the <code>beginGradientFill()</code> and <code>lineGradientStyle()</code> methods of the Graphics class. Width and height are scaled to a <code>scaleX</code>/<code>scaleY</code> pair and the <code>tx</code>/<code>ty</code> values are offset by half the width and height. </p>
		 * <p>For example, consider a gradient with the following characteristics:</p>
		 * <ul>
		 *  <li><code>GradientType.LINEAR</code></li>
		 *  <li>Two colors, green and blue, with the ratios array set to <code>[0, 255]</code></li>
		 *  <li><code>SpreadMethod.PAD</code></li>
		 *  <li><code>InterpolationMethod.LINEAR_RGB</code></li>
		 * </ul>
		 * <p>The following illustrations show gradients in which the matrix was defined using the <code>createGradientBox()</code> method with different parameter settings:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th><code>createGradientBox()</code> settings</th>
		 *    <th>Resulting gradient</th>
		 *   </tr>
		 *   <tr>
		 *    <td><pre>width = 25;
		 *      height = 25; 
		 *      rotation = 0; 
		 *      tx = 0; 
		 *      ty = 0;</pre></td>
		 *    <td align="center"><img src="../../images/createGradientBox-1.jpg" alt="resulting linear gradient"></td>
		 *   </tr>
		 *   <tr>
		 *    <td><pre>width = 25; 
		 *      height = 25; 
		 *      rotation = 0; 
		 *      tx = 25; 
		 *      ty = 0;</pre></td>
		 *    <td align="center"><img src="../../images/createGradientBox-2.jpg" alt="resulting linear gradient"></td>
		 *   </tr>
		 *   <tr>
		 *    <td><pre>width = 50; 
		 *      height = 50; 
		 *      rotation = 0; 
		 *      tx = 0; 
		 *      ty = 0;</pre></td>
		 *    <td align="center"><img src="../../images/createGradientBox-3.jpg" alt="resulting linear gradient"></td>
		 *   </tr>
		 *   <tr>
		 *    <td><pre>width = 50;
		 *      height = 50; 
		 *      rotation = Math.PI / 4; // 45 degrees
		 *      tx = 0; 
		 *      ty = 0;</pre></td>
		 *    <td align="center"><img src="../../images/createGradientBox-4.jpg" alt="resulting linear gradient"></td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * 
		 * @param width  — The width of the gradient box. 
		 * @param height  — The height of the gradient box. 
		 * @param rotation  — The amount to rotate, in radians. 
		 * @param tx  — The distance, in pixels, to translate to the right along the <i>x</i> axis. This value is offset by half of the <code>width</code> parameter. 
		 * @param ty  — The distance, in pixels, to translate down along the <i>y</i> axis. This value is offset by half of the <code>height</code> parameter. 
		 */
		public function createGradientBox(width:Number, height:Number, rotation:Number = 0, tx:Number = 0, ty:Number = 0):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Given a point in the pretransform coordinate space, returns the coordinates of that point after the transformation occurs. Unlike the standard transformation applied using the <code>transformPoint()</code> method, the <code>deltaTransformPoint()</code> method's transformation does not consider the translation parameters <code>tx</code> and <code>ty</code>. </p>
		 * 
		 * @param point  — The point for which you want to get the result of the matrix transformation. 
		 * @return  — The point resulting from applying the matrix transformation. 
		 */
		public function deltaTransformPoint(point:Point):Point {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets each matrix property to a value that causes a null transformation. An object transformed by applying an identity matrix will be identical to the original. </p>
		 * <p>After calling the <code>identity()</code> method, the resulting matrix has the following properties: <code>a</code>=1, <code>b</code>=0, <code>c</code>=0, <code>d</code>=1, <code>tx</code>=0, <code>ty</code>=0.</p>
		 * <p>In matrix notation, the identity matrix looks like this:</p>
		 * <p><img src="../../images/matrix_identity.jpg" alt="Matrix class properties in matrix notation"></p>
		 */
		public function identity():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Performs the opposite transformation of the original matrix. You can apply an inverted matrix to an object to undo the transformation performed when applying the original matrix. </p>
		 * <span id="pageFilter"><br><span class="label"> Example &nbsp;( <span class="usage"><a href="http://www.adobe.com/go/learn_as3_usingexamples_en"> How to use this example </a></span>) </span><br><br>
		 *  <div class="detailBody">
		 *    The following example creates a 
		 *   <code>halfScaleMatrix</code> by calling the 
		 *   <code>invert()</code> method of 
		 *   <code>doubleScaleMatrix</code>. It then demonstrates that the two are Matrix inverses of one another -- matrices that undo any transformations performed by the other -- by creating 
		 *   <code>originalAndInverseMatrix</code> which is equal to 
		 *   <code>noScaleMatrix</code>. 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>
		 * 
		 * package
		 * {
		 *     import flash.display.Shape;
		 *     import flash.display.Sprite;
		 *     import flash.geom.Matrix;
		 *     import flash.geom.Transform;
		 *     
		 *     public class Matrix_invert extends Sprite
		 *     {
		 *         public function Matrix_invert()
		 *         {
		 *             var rect0:Shape = createRectangle(20, 80, 0xFF0000);   
		 *             var rect1:Shape = createRectangle(20, 80, 0x00FF00);   
		 *             var rect2:Shape = createRectangle(20, 80, 0x0000FF);
		 *             var rect3:Shape = createRectangle(20, 80, 0x000000);
		 *             
		 *             var trans0:Transform = new Transform(rect0);
		 *             var trans1:Transform = new Transform(rect1);
		 *             var trans2:Transform = new Transform(rect2);
		 *             var trans3:Transform = new Transform(rect3);
		 *              
		 *             var doubleScaleMatrix:Matrix = new Matrix(2, 0, 0, 2, 0, 0);
		 *             trans0.matrix = doubleScaleMatrix;
		 *             trace(doubleScaleMatrix.toString());  // (a=2, b=0, c=0, d=2, tx=0, ty=0)
		 *              
		 *             var noScaleMatrix:Matrix = new Matrix(1, 0, 0, 1, 0, 0);
		 *             trans1.matrix = noScaleMatrix;
		 *             rect1.x = 50;
		 *             trace(noScaleMatrix.toString());  // (a=1, b=0, c=0, d=1, tx=0, ty=0)
		 *              
		 *             var halfScaleMatrix:Matrix = doubleScaleMatrix.clone();
		 *             halfScaleMatrix.invert();
		 *             trans2.matrix = halfScaleMatrix;
		 *             rect2.x = 100;
		 *             trace(halfScaleMatrix.toString());  // (a=0.5, b=0, c=0, d=0.5, tx=0, ty=0)
		 *              
		 *             var originalAndInverseMatrix:Matrix = doubleScaleMatrix.clone();
		 *             originalAndInverseMatrix.concat(halfScaleMatrix);
		 *             trans3.matrix = originalAndInverseMatrix;
		 *             rect3.x = 150;
		 *             trace(originalAndInverseMatrix.toString());  // (a=1, b=0, c=0, d=1, tx=0, ty=0)            
		 *         }
		 *         
		 *         public function createRectangle(w:Number, h:Number, color:Number):Shape 
		 *         {
		 *             var rect:Shape = new Shape();
		 *             rect.graphics.beginFill(color);
		 *             rect.graphics.drawRect(0, 0, w, h);
		 *             addChild(rect);
		 *             return rect;
		 *         }
		 *     }
		 * }
		 * </pre>
		 *   </div>
		 *  </div></span>
		 */
		public function invert():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Applies a rotation transformation to the Matrix object. </p>
		 * <p>The <code>rotate()</code> method alters the <code>a</code>, <code>b</code>, <code>c</code>, and <code>d</code> properties of the Matrix object. In matrix notation, this is the same as concatenating the current matrix with the following:</p>
		 * <p><img src="../../images/matrix_rotate.jpg" alt="Matrix notation of scale method parameters"></p>
		 * 
		 * @param angle  — The rotation angle in radians. 
		 */
		public function rotate(angle:Number):void {
			if (angle != 0) {
				var u:Number = Math.cos(angle);
				var v:Number = Math.sin(angle);
				var ta:Number = this.a;
				var tb:Number = this.b;
				var tc:Number = this.c;
				var td:Number = this.d;
				var ttx:Number = this.tx;
				var tty:Number = this.ty;
				this.a = ta  * u - tb  * v;
				this.b = ta  * v + tb  * u;
				this.c = tc  * u - td  * v;
				this.d = tc  * v + td  * u;
				this.tx = ttx * u - tty * v;
				this.ty = ttx * v + tty * u;
			}
		}

		/**
		 * <p> Applies a scaling transformation to the matrix. The <i>x</i> axis is multiplied by <code>sx</code>, and the <i>y</i> axis it is multiplied by <code>sy</code>. </p>
		 * <p>The <code>scale()</code> method alters the <code>a</code> and <code>d</code> properties of the Matrix object. In matrix notation, this is the same as concatenating the current matrix with the following matrix:</p>
		 * <p><img src="../../images/matrix_scale.jpg" alt="Matrix notation of scale method parameters"></p>
		 * 
		 * @param sx  — A multiplier used to scale the object along the <i>x</i> axis. 
		 * @param sy  — A multiplier used to scale the object along the <i>y</i> axis. 
		 */
		public function scale(sx:Number, sy:Number):void {
			a *= sx;
			d *= sy;
		}

		/**
		 * <p> Sets the members of Matrix to the specified values </p>
		 * 
		 * @param aa  — the values to set the matrix to. 
		 * @param ba
		 * @param ca
		 * @param da
		 * @param txa
		 * @param tya
		 */
		public function setTo(aa:Number, ba:Number, ca:Number, da:Number, txa:Number, tya:Number):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a text value listing the properties of the Matrix object. </p>
		 * 
		 * @return  — A string containing the values of the properties of the Matrix object: , , , , , and . 
		 */
		public function toString():String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the result of applying the geometric transformation represented by the Matrix object to the specified point. </p>
		 * 
		 * @param point  — The point for which you want to get the result of the Matrix transformation. 
		 * @return  — The point resulting from applying the Matrix transformation. 
		 */
		public function transformPoint(point:Point):Point {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Translates the matrix along the <i>x</i> and <i>y</i> axes, as specified by the <code>dx</code> and <code>dy</code> parameters. </p>
		 * 
		 * @param dx  — The amount of movement along the <i>x</i> axis to the right, in pixels. 
		 * @param dy  — The amount of movement down along the <i>y</i> axis, in pixels. 
		 */
		public function translate(dx:Number, dy:Number):void {
			tx += dx;
			ty += dy;
		}
	}
}
