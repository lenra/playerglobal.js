package flash.external {
	/**
	 *  The ExternalInterface class is an application programming interface that enables straightforward communication between ActionScript and the SWF container– for example, an HTML page with JavaScript or a desktop application that uses Flash Player to display a SWF file. <p>Using the ExternalInterface class, you can call an ActionScript function in the Flash runtime, using JavaScript in the HTML page. The ActionScript function can return a value, and JavaScript receives it immediately as the return value of the call.</p> <p>This functionality replaces the <code>fscommand()</code> method.</p> <p>Use the ExternalInterface class in the following combinations of browser and operating system:</p> <table class="innertable">
	 *  <tbody>
	 *   <tr>
	 *    <th>Browser</th>
	 *    <th>Operating System</th>
	 *    <th>Operating System</th>
	 *   </tr>
	 *   <tr>
	 *    <td>Internet Explorer 5.0 and later</td>
	 *    <td>&nbsp;Windows&nbsp;</td>
	 *    <td>&nbsp;</td>
	 *   </tr>
	 *   <tr>
	 *    <td>Netscape 8.0 and later</td>
	 *    <td>&nbsp;Windows&nbsp;</td>
	 *    <td>&nbsp;MacOS&nbsp;</td>
	 *   </tr>
	 *   <tr>
	 *    <td>Mozilla 1.7.5 and later</td>
	 *    <td>&nbsp;Windows&nbsp;</td>
	 *    <td>&nbsp;MacOS&nbsp;</td>
	 *   </tr>
	 *   <tr>
	 *    <td>Firefox 1.0 and later</td>
	 *    <td>&nbsp;Windows&nbsp;</td>
	 *    <td>&nbsp;MacOS&nbsp;</td>
	 *   </tr>
	 *   <tr>
	 *    <td>Safari 1.3 and later</td>
	 *    <td>&nbsp;</td>
	 *    <td>&nbsp;MacOS&nbsp;</td>
	 *   </tr>
	 *  </tbody>
	 * </table> <p>Flash Player for Linux version 9.0.31.0 and later supports the ExternalInterface class in the following browsers:</p> <table class="innertable">
	 *  <tbody>
	 *   <tr>
	 *    <th>Browser</th>
	 *   </tr>
	 *   <tr>
	 *    <td>Mozilla 1.7.x and later</td>
	 *   </tr>
	 *   <tr>
	 *    <td>Firefox 1.5.0.7 and later</td>
	 *   </tr>
	 *   <tr>
	 *    <td>SeaMonkey 1.0.5 and later </td>
	 *   </tr>
	 *  </tbody>
	 * </table> <p>The ExternalInterface class requires the user's web browser to support either ActiveX<sup>®</sup> or the NPRuntime API that is exposed by some browsers for plug-in scripting. Even if a browser and operating system combination are not listed above, they should support the ExternalInterface class if they support the NPRuntime API. See <a href="http://www.mozilla.org/projects/plugins/npruntime.html" target="external">http://www.mozilla.org/projects/plugins/npruntime.html</a>.</p> <p> <b>Note:</b> When embedding SWF files within an HTML page, make sure that the <code>id</code> attribute is set and the <code>id</code> and <code>name</code> attributes of the <code>object</code> and <code>embed</code> tags do not include the following characters:</p> <pre>
	 *  . - + * / \
	 *  </pre> <p> <b>Note for Flash Player applications:</b> Flash Player version 9.0.115.0 and later allows the <code>.</code> (period) character within the <code>id</code> and <code>name</code> attributes.</p> <p> <b>Note for Flash Player applications:</b> In Flash Player 10 and later running in a browser, using this class programmatically to open a pop-up window may not be successful. Various browsers (and browser configurations) may block pop-up windows at any time; it is not possible to guarantee any pop-up window will appear. However, for the best chance of success, use this class to open a pop-up window only in code that executes as a direct result of a user action (for example, in an event handler for a mouse click or key-press event.)</p> <p>From ActionScript, you can do the following on the HTML page: </p><ul> 
	 *  <li>Call any JavaScript function.</li> 
	 *  <li>Pass any number of arguments, with any names.</li> 
	 *  <li>Pass various data types (Boolean, Number, String, and so on).</li> 
	 *  <li>Receive a return value from the JavaScript function.</li> 
	 * </ul>  <p>From JavaScript on the HTML page, you can: </p><ul> 
	 *  <li>Call an ActionScript function.</li> 
	 *  <li>Pass arguments using standard function call notation.</li> 
	 *  <li>Return a value to the JavaScript function.</li> 
	 * </ul>  <p> <b>Note for Flash Player applications:</b> Flash Player does not currently support SWF files embedded within HTML forms.</p> <p> <b>Note for AIR applications:</b> In Adobe AIR, the ExternalInterface class can be used to communicate between JavaScript in an HTML page loaded in the HTMLLoader control and ActionScript in SWF content embedded in that HTML page.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS2db454920e96a9e51e63e3d11c0bf69084-7f31.html" target="_blank">Using the ExternalInterface API to access JavaScript</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS2db454920e96a9e51e63e3d11c0bf69084-7e92.html" target="_blank">Accessing Flex from JavaScript</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7cb2.html" target="_blank">Using the ExternalInterface class</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7cb1.html" target="_blank">External API example: Communicating between ActionScript and JavaScript in a web browser</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7c9b.html" target="_blank">Controlling outbound URL access</a>
	 * </div><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS2db454920e96a9e51e63e3d11c0bf69084-7ea6.html" target="_blank">About ExternalInterface API security in Flex</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b8fc4e-8000.html" target="_blank">Using the external API</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7caa.html" target="_blank">Basics of using the external API</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7cab.html" target="_blank">External API requirements and advantages</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7caf.html" target="_blank">The external API’s XML format</a>
	 *  <br>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../flash/system/package.html#fscommand()" target="">fscommand()</a>
	 * </div><br><hr>
	 */
	public class ExternalInterface {
		private static var _marshallExceptions:Boolean;

		private static var _available:Boolean;
		private static var _objectID:String;


		/**
		 * <p> Indicates whether this player is in a container that offers an external interface. If the external interface is available, this property is <code>true</code>; otherwise, it is <code>false</code>. </p>
		 * <p><b>Note:</b> When using the External API with HTML, always check that the HTML has finished loading before you attempt to call any JavaScript methods.</p>
		 * 
		 * @return 
		 */
		public static function get available():Boolean {
			return _available;
		}


		/**
		 * <p> Indicates whether the external interface should attempt to pass ActionScript exceptions to the current browser and JavaScript exceptions to the player. You must explicitly set this property to <code>true</code> to catch JavaScript exceptions in ActionScript and to catch ActionScript exceptions in JavaScript. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="ExternalInterface.html#addCallBack()" target="">addCallBack()</a>
		 *  <br>
		 *  <a href="../../statements.html#try..catch..finally" target="">try..catch..finally statement</a>
		 * </div>
		 * <span id="pageFilter"><br><span class="label"> Example &nbsp;( <span class="usage"><a href="http://www.adobe.com/go/learn_as3_usingexamples_en"> How to use this example </a></span>) </span><br>
		 *  <div class="detailBody">
		 *    The following example creates an ActionScript function and registers it with the containing browser by using the 
		 *   <code>addCallback()</code> method. The new function throws an exception so that JavaScript code running in the browser can catch it. This example also contains a 
		 *   <code>try..catch</code> statement to catch any exceptions thrown by the browser when the 
		 *   <code>throwit()</code> function is called. 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>
		 * 
		 * package
		 * {
		 *     import flash.external.*
		 *     import flash.net.*;
		 *     import flash.display.*;
		 *     import flash.system.System;
		 *     public class ext_test extends Sprite {
		 *     function ext_test():void {
		 *         ExternalInterface.marshallExceptions = true;
		 *         ExternalInterface.addCallback("g", g);
		 * 
		 *         try {
		 *         ExternalInterface.call("throwit");
		 *         } catch(e:Error) {
		 *         trace(e)
		 *         }
		 *     }
		 *     function g() { throw new Error("exception from actionscript!!!!") }
		 *     }
		 * }
		 * </pre>
		 *   </div>
		 *  </div></span>
		 * 
		 * @return 
		 */
		public static function get marshallExceptions():Boolean {
			return _marshallExceptions;
		}


		/**
		 * @param value
		 * @return 
		 */
		public static function set marshallExceptions(value:Boolean):void {
			_marshallExceptions = value;
		}


		/**
		 * <p> Returns the <code>id</code> attribute of the <code>object</code> tag in Internet Explorer, or the <code>name</code> attribute of the <code>embed</code> tag in Netscape. </p>
		 * 
		 * @return 
		 */
		public static function get objectID():String {
			return _objectID;
		}


		/**
		 * <p> Registers an ActionScript method as callable from the container. After a successful invocation of <code>addCallBack()</code>, the registered function in the player can be called by JavaScript or ActiveX code in the container. </p>
		 * <p><b>Note:</b> For <i>local</i> content running in a browser, calls to the <code>ExternalInterface.addCallback()</code> method work only if the SWF file and the containing web page are in the local-trusted security sandbox. For more information, see the Flash Player Developer Center Topic: <a href="http://www.adobe.com/go/devnet_security_en" target="external">Security</a>.</p>
		 * 
		 * @param functionName  — The name by which the container can invoke the function. 
		 * @param closure  — The function closure to invoke. This could be a free-standing function, or it could be a method closure referencing a method of an object instance. By passing a method closure, you can direct the callback at a method of a particular object instance. <p><b>Note:</b> Repeating <code>addCallback()</code> on an existing callback function with a <code>null</code> closure value removes the callback.</p> 
		 */
		public static function addCallback(functionName:String, closure:Function):void {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Calls a function exposed by the SWF container, passing zero or more arguments. If the function is not available, the call returns <code>null</code>; otherwise it returns the value provided by the function. Recursion is <i>not</i> permitted on Opera or Netscape browsers; on these browsers a recursive call produces a <code>null</code> response. (Recursion is supported on Internet Explorer and Firefox browsers.) </p>
		 * <p>If the container is an HTML page, this method invokes a JavaScript function in a <code>script</code> element.</p>
		 * <p>If the container is another ActiveX container, this method dispatches the FlashCall ActiveX event with the specified name, and the container processes the event.</p>
		 * <p>If the container is hosting the Netscape plug-in, you can either write custom support for the new NPRuntime interface or embed an HTML control and embed the player within the HTML control. If you embed an HTML control, you can communicate with the player through a JavaScript interface to the native container application.</p>
		 * <p><b>Note:</b> For <i>local</i> content running in a browser, calls to the <code>ExternalInterface.call()</code> method are permitted only if the SWF file and the containing web page (if there is one) are in the local-trusted security sandbox. Also, you can prevent a SWF file from using this method by setting the <code>allowNetworking</code> parameter of the <code>object</code> and <code>embed</code> tags in the HTML page that contains the SWF content. For more information, see the Flash Player Developer Center Topic: <a href="http://www.adobe.com/go/devnet_security_en" target="external">Security</a>.</p>
		 * <p><b>Note for Flash Player applications:</b> In Flash Player 10 and Flash Player 9 Update 5, some web browsers restrict this method if a pop-up blocker is enabled. In this scenario, you can only call this method successfully in response to a user event (for example, in an event handler for a mouse click or keypress event).</p>
		 * 
		 * @param functionName  — The alphanumeric name of the function to call in the container. Using a non-alphanumeric function name causes a runtime error (error 2155). You can use a <code>try..catch</code> block to handle the error. 
		 * @param arguments  — The arguments to pass to the function in the container. You can specify zero or more parameters, separating them with commas. They can be of any ActionScript data type. When the call is to a JavaScript function, the ActionScript types are automatically converted into JavaScript types; when the call is to some other ActiveX container, the parameters are encoded in the request message. 
		 * @return  — The response received from the container. If the call failed– for example, if there is no such function in the container, the interface is not available, a recursion occurred (with a Netscape or Opera browser), or there is a security issue–  is returned and an error is thrown. 
		 */
		public static function call(functionName:String, ...arguments):* {
			throw new Error("Not implemented");
		}
	}
}
