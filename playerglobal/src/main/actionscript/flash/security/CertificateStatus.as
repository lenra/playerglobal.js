package flash.security {
	/**
	 *  The CertificateStatus class defines constants used to report the results of certificate validation processing by a SecureSocket object. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../flash/net/SecureSocket.html#serverCertificateStatus" target="">SecureSocket.serverCertificateStatus</a>
	 * </div><br><hr>
	 */
	public class CertificateStatus {
		/**
		 * <p> The certificate is outside its valid period. </p>
		 * <p>Indicates that certificate validation processing was attempted, but failed because the validity period of the certificate is either before or after the current date. On some operating systems, the <code>notYetValid</code> status is reported when the current date is before the validity period of the cerificate. On other operating systems, the <code>expired</code> status is reported in both cases.</p>
		 */
		public static const EXPIRED:String = "expired";
		/**
		 * <p> An invalid certificate. </p>
		 * <p>Indicates that certificate validation processing was attempted, but failed. This is the generic faliure status that is reported when a more specific certificate status cannot be determined.</p>
		 */
		public static const INVALID:String = "invalid";
		/**
		 * <p> A root or intermediate certificate in this certificate's chain is invalid. </p>
		 * <p>Indicates that certificate validation processing was attempted, but failed because the certificate's trust chain was invalid.</p>
		 */
		public static const INVALID_CHAIN:String = "invalidChain";
		/**
		 * <p> The certificate is not yet valid. </p>
		 * <p>Indicates that a certificate is not yet valid. The current date is before the notBefore date/time of the certificate</p>
		 */
		public static const NOT_YET_VALID:String = "notYetValid";
		/**
		 * <p> The certificate common name does not match the expected host name. </p>
		 * <p>Indicates that certificate validation processing was attempted, but failed because the certificate's common name does not match the fully qualified domain name of the host.</p>
		 */
		public static const PRINCIPAL_MISMATCH:String = "principalMismatch";
		/**
		 * <p> The certificate has been revoked. </p>
		 * <p>Indicates that certificate validation processing was attempted, but failed because the certificate has been revoked. On some operating systems, the <code>revoked</code> status is also reported when the certificate (or its root certificate) has been added to the list of untrusted certificates on the client computer.</p>
		 */
		public static const REVOKED:String = "revoked";
		/**
		 * <p> A valid, trusted certificate. </p>
		 * <p>Indicates that a certificate has not expired, has not failed a revocation check, and chains to a trusted root certificate.</p>
		 */
		public static const TRUSTED:String = "trusted";
		/**
		 * <p> The validity of the certificate is not known. </p>
		 * <p>Indicates that certificate validation processing has not been performed yet on a certificate.</p>
		 */
		public static const UNKNOWN:String = "unknown";
		/**
		 * <p> The certificate does not chain to a trusted root certificate. </p>
		 * <p>Indicates that certificate validation processing was attempted, but that the certificate does not chain to any of the root certificates in the client trust store. On some operating systems, the <code>untrustedSigners</code> is also reported if the certificate is in the list of untrusted certificates on the client computer.</p>
		 */
		public static const UNTRUSTED_SIGNERS:String = "untrustedSigners";
	}
}
