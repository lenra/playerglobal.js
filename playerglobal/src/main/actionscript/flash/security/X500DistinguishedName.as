package flash.security {
	/**
	 *  The X500DistinguishedName class defines Distinguished Name (DN) properties for use in the X509Certificate class. The Distinguished Name protocol is specified in <a href="http://tools.ietf.org/rfc/rfc1779" target="external">RFC1779</a>. <p>This class is useful for any code that needs to examine a server certificate's Subject and Issuer DN after a secure socket connection has been established. Subject and Issuer DN data is accessible in the X509Certificate class's <code>subject</code> and <code>issuer</code> properties. These properties are populated with X500DistinguishedName objects after <code>SecureSocket.connect()</code> establishes a connection with the server.</p> <p>This class stores DN attributes as string properties. You can use the <code>toString()</code> method to get all of the individual DN properties in one string. Properties with no DN value are set to null.</p> <p>Note: The X500DistinguishedName properties store only the first occurrence of each DN attribute, although the DN protocol allows for multiple attributes of the same type.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../flash/net/SecureSocket.html" target="">SecureSocket class</a>
	 * </div><br><hr>
	 */
	public class X500DistinguishedName {
		private var _commonName:String;
		private var _countryName:String;
		private var _localityName:String;
		private var _organizationName:String;
		private var _organizationalUnitName:String;
		private var _stateOrProvinceName:String;

		/**
		 * <p> Returns the DN CommonName attribute. </p>
		 * 
		 * @return 
		 */
		public function get commonName():String {
			return _commonName;
		}

		/**
		 * <p> Returns the DN CountryName attribute. </p>
		 * 
		 * @return 
		 */
		public function get countryName():String {
			return _countryName;
		}

		/**
		 * <p> Returns the DN LocalityName attribute. </p>
		 * 
		 * @return 
		 */
		public function get localityName():String {
			return _localityName;
		}

		/**
		 * <p> Returns the DN OrganizationName attribute. </p>
		 * 
		 * @return 
		 */
		public function get organizationName():String {
			return _organizationName;
		}

		/**
		 * <p> Returns the DN OrganizationalUnitName attribute. </p>
		 * 
		 * @return 
		 */
		public function get organizationalUnitName():String {
			return _organizationalUnitName;
		}

		/**
		 * <p> Returns the DN StateOrProvinceName attribute. </p>
		 * 
		 * @return 
		 */
		public function get stateOrProvinceName():String {
			return _stateOrProvinceName;
		}

		/**
		 * <p> Returns all DN properties in one string. </p>
		 * 
		 * @return 
		 */
		public function toString():String {
			throw new Error("Not implemented");
		}
	}
}
