package flash.security {
	import flash.utils.ByteArray;

	/**
	 *  The X509Certificate class represents an X.509 certificate. This class defines X.509 properties specified in <a href="http://tools.ietf.org/rfc/rfc2459" target="external">RFC2459</a>. After you make a successful call to <code>SecureSocket.connect()</code>, the server's certificate data is stored as an X509Certificate instance in the <code>SecureSocket.serverCertificate</code> property. <p> Use this class to examine a server certificate after establishing a secure socket connection. The properties in this class provide access to the most used attributes of an X.509 certificate. If you must access other parts of a server certificate (for example, its extensions), the complete certificate is available in the <code>encoded</code> property. The certificate stored in the <code>encoded</code> property is DER-encoded. </p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../flash/net/SecureSocket.html" target="">SecureSocket class</a>
	 * </div><br><hr>
	 */
	public class X509Certificate {
		private var _encoded:ByteArray;
		private var _issuer:X500DistinguishedName;
		private var _issuerUniqueID:String;
		private var _serialNumber:String;
		private var _signatureAlgorithmOID:String;
		private var _signatureAlgorithmParams:ByteArray;
		private var _subject:X500DistinguishedName;
		private var _subjectPublicKey:String;
		private var _subjectPublicKeyAlgorithmOID:String;
		private var _subjectUniqueID:String;
		private var _validNotAfter:Date;
		private var _validNotBefore:Date;
		private var _version:uint;

		/**
		 * <p> Provides the whole certificate in encoded form. Client code can decode this value to process certificate extensions. X.509 certificate extensions are not represented in the other properties in this class. Decoding the <code>encoded</code> property is the only way to access a certificate's extensions. </p>
		 * 
		 * @return 
		 */
		public function get encoded():ByteArray {
			return _encoded;
		}

		/**
		 * <p> Provides the issuer's Distinguished Name (DN). </p>
		 * 
		 * @return 
		 */
		public function get issuer():X500DistinguishedName {
			return _issuer;
		}

		/**
		 * <p> Provides the issuer's unique identifier. </p>
		 * 
		 * @return 
		 */
		public function get issuerUniqueID():String {
			return _issuerUniqueID;
		}

		/**
		 * <p> Provides the serial number of the certificate as a hexadecimal string. The issuer assigns this number, and the number is unique within the issuer's list of issued certificates. </p>
		 * 
		 * @return 
		 */
		public function get serialNumber():String {
			return _serialNumber;
		}

		/**
		 * <p> Provides the signature algorithm Object Identifier (OID). </p>
		 * 
		 * @return 
		 */
		public function get signatureAlgorithmOID():String {
			return _signatureAlgorithmOID;
		}

		/**
		 * <p> Provides the signature algorithm's parameters. If there are no signature algorithm parameters, this value is set to null. </p>
		 * 
		 * @return 
		 */
		public function get signatureAlgorithmParams():ByteArray {
			return _signatureAlgorithmParams;
		}

		/**
		 * <p> Provides the subject's Distinguished Name (DN). </p>
		 * 
		 * @return 
		 */
		public function get subject():X500DistinguishedName {
			return _subject;
		}

		/**
		 * <p> Provides the subject's public key. </p>
		 * 
		 * @return 
		 */
		public function get subjectPublicKey():String {
			return _subjectPublicKey;
		}

		/**
		 * <p> Provides the algorithm OID for the subject's public key. </p>
		 * 
		 * @return 
		 */
		public function get subjectPublicKeyAlgorithmOID():String {
			return _subjectPublicKeyAlgorithmOID;
		}

		/**
		 * <p> Provides the subject's unique identifier. </p>
		 * 
		 * @return 
		 */
		public function get subjectUniqueID():String {
			return _subjectUniqueID;
		}

		/**
		 * <p> Indicates the date on which the certificate's validity period ends. </p>
		 * 
		 * @return 
		 */
		public function get validNotAfter():Date {
			return _validNotAfter;
		}

		/**
		 * <p> Indicates the date on which the certificate's validity period begins. </p>
		 * 
		 * @return 
		 */
		public function get validNotBefore():Date {
			return _validNotBefore;
		}

		/**
		 * <p> Provides the version number of the certificate format. This property indicates whether the certificate has extensions, a unique identifier, or only the basic fields. </p>
		 * <ul>
		 *  <li> <code>version</code> = 2: Indicates X.509 Version 3 - Extensions are present </li>
		 *  <li> <code>version</code> = 1: Indicates X.509 Version 2 - Extensions are not present, but a unique identifier is present. </li>
		 *  <li> <code>version</code> = null: Indicates X.509 Version 1 - Only the basic certificate fields are present </li>
		 * </ul>
		 * 
		 * @return 
		 */
		public function get version():uint {
			return _version;
		}
	}
}
