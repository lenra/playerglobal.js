package flash.xml {
	/**
	 *  The XMLNodeType class contains constants used with <code>XMLNode.nodeType</code>. The values are defined by the NodeType enumeration in the W3C DOM Level 1 recommendation: <a href="http://www.w3.org/TR/1998/REC-DOM-Level-1-19981001/level-one-core.html" target="external">http://www.w3.org/TR/1998/REC-DOM-Level-1-19981001/level-one-core.html</a> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="XMLNode.html#nodeType" target="">XMLNode.nodeType</a>
	 * </div><br><hr>
	 */
	public class XMLNodeType {
		/**
		 * <p> Specifies that the node is an element. This constant is used with <code>XMLNode.nodeType</code>. The value is defined by the NodeType enumeration in the W3C DOM Level 1 recommendation: <a href="http://www.w3.org/TR/1998/REC-DOM-Level-1-19981001/level-one-core.html" target="external">http://www.w3.org/TR/1998/REC-DOM-Level-1-19981001/level-one-core.html</a> </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="XMLNode.html#nodeType" target="">XMLNode.nodeType</a>
		 * </div>
		 */
		public static const ELEMENT_NODE:uint = 1;
		/**
		 * <p> Specifies that the node is a text node. This constant is used with <code>XMLNode.nodeType</code>. The value is defined by the NodeType enumeration in the W3C DOM Level 1 recommendation: <a href="http://www.w3.org/TR/1998/REC-DOM-Level-1-19981001/level-one-core.html" target="external">http://www.w3.org/TR/1998/REC-DOM-Level-1-19981001/level-one-core.html</a> </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="XMLNode.html#nodeType" target="">XMLNode.nodeType</a>
		 * </div>
		 */
		public static const TEXT_NODE:uint = 3;
	}
}
