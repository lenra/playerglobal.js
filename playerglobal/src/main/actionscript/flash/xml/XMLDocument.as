package flash.xml {
	/**
	 *  The XMLDocument class represents the legacy XML object that was present in ActionScript 2.0. It was renamed in ActionScript 3.0 to XMLDocument to avoid name conflicts with the new XML class in ActionScript 3.0. In ActionScript 3.0, it is recommended that you use the new <a href="../../XML.html">XML</a> class and related classes, which support E4X (ECMAScript for XML). <p>The XMLDocument class, as well as XMLNode and XMLNodeType, are present for backward compatibility. The functionality for loading XML documents can now be found in the URLLoader class.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../flash/net/URLLoader.html" target="">flash.net.URLLoader</a>
	 *  <br>
	 *  <a href="../../XML.html" target="">XML class</a>
	 * </div><br><hr>
	 */
	public class XMLDocument extends XMLNode {
		private var _docTypeDecl:Object;
		private var _idMap:Object;
		private var _ignoreWhite:Boolean;
		private var _xmlDecl:Object;

		/**
		 * <p> Creates a new XMLDocument object. You must use the constructor to create an XMLDocument object before you call any of the methods of the XMLDocument class. </p>
		 * <p><b>Note: </b>Use the <code>createElement()</code> and <code>createTextNode()</code> methods to add elements and text nodes to an XML document tree.</p>
		 * 
		 * @param source  — The XML text parsed to create the new XMLDocument object. 
		 */
		public function XMLDocument(source:String = null) {
			super(null, null);
			throw new Error("Not implemented");
		}

		/**
		 * <p> Specifies information about the XML document's <code>DOCTYPE</code> declaration. After the XML text has been parsed into an XMLDocument object, the <code>XMLDocument.docTypeDecl</code> property of the XMLDocument object is set to the text of the XML document's <code>DOCTYPE</code> declaration (for example, <code>&lt;!DOCTYPE</code> <code>greeting SYSTEM "hello.dtd"&gt;</code>). This property is set using a string representation of the <code>DOCTYPE</code> declaration, not an XMLNode object. </p>
		 * <p>The legacy ActionScript XML parser is not a validating parser. The <code>DOCTYPE</code> declaration is read by the parser and stored in the <code>XMLDocument.docTypeDecl</code> property, but no DTD validation is performed.</p>
		 * <p>If no <code>DOCTYPE</code> declaration was encountered during a parse operation, the <code>XMLDocument.docTypeDecl</code> property is set to <code>null</code>. The <code>XML.toString()</code> method outputs the contents of <code>XML.docTypeDecl</code> immediately after the XML declaration stored in <code>XML.xmlDecl</code>, and before any other text in the XML object. If <code>XMLDocument.docTypeDecl</code> is null, no <code>DOCTYPE</code> declaration is output.</p>
		 * 
		 * @return 
		 */
		public function get docTypeDecl():Object {
			return _docTypeDecl;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set docTypeDecl(value:Object):void {
			_docTypeDecl = value;
		}

		/**
		 * <p> An Object containing the nodes of the XML that have an <code>id</code> attribute assigned. The names of the properties of the object (each containing a node) match the values of the <code>id</code> attributes. </p>
		 * <p>Consider the following XMLDocument object:</p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *      &lt;employee id='41'&gt;
		 *          &lt;name&gt;
		 *              John Doe
		 *          &lt;/name&gt;
		 *          &lt;address&gt;
		 *              601 Townsend St.
		 *          &lt;/address&gt;
		 *      &lt;/employee&gt;
		 *      
		 *      &lt;employee id='42'&gt;
		 *          &lt;name&gt;
		 *              Jane Q. Public
		 *          &lt;/name&gt;
		 *      &lt;/employee&gt;
		 *      &lt;department id="IT"&gt;
		 *          Information Technology
		 *      &lt;/department&gt;
		 *      </pre>
		 * </div>
		 * <p>In this example, the <code>idMap</code> property for this XMLDocument object is an Object with three properties: <code>41</code>, <code>42</code>, and <code>IT</code>. Each of these properties is an XMLNode that has the matching <code>id</code> value. For example, the <code>IT</code> property of the <code>idMap</code> object is this node:</p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *      &lt;department id="IT"&gt;
		 *          Information Technology
		 *      &lt;/department&gt;
		 *      </pre>
		 * </div>
		 * <p>You must use the <code>parseXML()</code> method on the XMLDocument object for the <code>idMap</code> property to be instantiated.</p>
		 * <p>If there is more than one XMLNode with the same <code>id</code> value, the matching property of the <code>idNode</code> object is that of the last node parsed. For example:</p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *      var x1:XML = new XMLDocument("&lt;a id='1'&gt;&lt;b id='2' /&gt;&lt;c id='1' /&gt;&lt;/a&gt;");
		 *      x2 = new XMLDocument();
		 *      x2.parseXML(x1);
		 *      trace(x2.idMap['1']);
		 *      </pre>
		 * </div>
		 * <code>&lt;c&gt;</code>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *      &lt;c id='1' /&gt;
		 *      </pre>
		 * </div>
		 * 
		 * @return 
		 */
		public function get idMap():Object {
			return _idMap;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set idMap(value:Object):void {
			_idMap = value;
		}

		/**
		 * <p> When set to <code>true</code>, text nodes that contain only white space are discarded during the parsing process. Text nodes with leading or trailing white space are unaffected. The default setting is <code>false</code>. </p>
		 * <p>You can set the <code>ignoreWhite</code> property for individual XMLDocument objects, as the following code shows:</p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *      my_xml.ignoreWhite = true;
		 *      </pre>
		 * </div>
		 * 
		 * @return 
		 */
		public function get ignoreWhite():Boolean {
			return _ignoreWhite;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set ignoreWhite(value:Boolean):void {
			_ignoreWhite = value;
		}

		/**
		 * <p> A string that specifies information about a document's XML declaration. After the XML document is parsed into an XMLDocument object, this property is set to the text of the document's XML declaration. This property is set using a string representation of the XML declaration, not an XMLNode object. If no XML declaration is encountered during a parse operation, the property is set to <code>null</code>. The <code>XMLDocument.toString()</code> method outputs the contents of the <code>XML.xmlDecl</code> property before any other text in the XML object. If the <code>XML.xmlDecl</code> property contains <code>null</code>, no XML declaration is output. </p>
		 * 
		 * @return 
		 */
		public function get xmlDecl():Object {
			return _xmlDecl;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set xmlDecl(value:Object):void {
			_xmlDecl = value;
		}

		/**
		 * <p> Creates a new XMLNode object with the name specified in the parameter. The new node initially has no parent, no children, and no siblings. The method returns a reference to the newly created XMLNode object that represents the element. This method and the <code>XMLDocument.createTextNode()</code> method are the constructor methods for creating nodes for an XMLDocument object. </p>
		 * 
		 * @param name  — The tag name of the XMLDocument element being created. 
		 * @return  — An XMLNode object. 
		 */
		public function createElement(name:String):XMLNode {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Creates a new XML text node with the specified text. The new node initially has no parent, and text nodes cannot have children or siblings. This method returns a reference to the XMLDocument object that represents the new text node. This method and the <code>XMLDocument.createElement()</code> method are the constructor methods for creating nodes for an XMLDocument object. </p>
		 * 
		 * @param text  — The text used to create the new text node. 
		 * @return  — An XMLNode object. 
		 */
		public function createTextNode(text:String):XMLNode {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Parses the XML text specified in the <code>value</code> parameter and populates the specified XMLDocument object with the resulting XML tree. Any existing trees in the XMLDocument object are discarded. </p>
		 * 
		 * @param source  — The XML text to be parsed and passed to the specified XMLDocument object. 
		 */
		public function parseXML(source:String):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a string representation of the XML object. </p>
		 * 
		 * @return  — A string representation of the XML object. 
		 */
		override public function toString():String {
			throw new Error("Not implemented");
		}
	}
}
