package flash.media {
	/**
	 *  The H264Profile class is an enumeration of constant values used in setting the profile of <code>H264VideoStreamSettings</code> class. <br><hr>
	 */
	public class H264Profile {
		/**
		 * <p> Constant for H.264/AVC baseline profile. This is the default value for <code>H264VideoStreamSettings</code> class. </p>
		 */
		public static const BASELINE:String = "baseline";
		/**
		 * <p> Constant for H.264/AVC main profile. </p>
		 */
		public static const MAIN:String = "main";
	}
}
