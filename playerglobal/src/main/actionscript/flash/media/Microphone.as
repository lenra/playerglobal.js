package flash.media {
	import flash.events.EventDispatcher;

	/**
	 *  Use the Microphone class to monitor or capture audio from a microphone. <p> To get access to the device microphone, you can use <code>Microphone.getMicrophone()</code> method. However, this method returns a simple microphone, which does not have the ability to eliminate acoustic echo. In order to eliminate acoustic echo, you need to get an instance of microphone using <code>Microphone.getEnhancedMicrophone()</code> method. This method returns a device microphone that has the acoustic echo cancellation feature enabled for mobile. Use acoustic echo cancellation to create real-time audio/video applications that don't require headsets. </p> <p> <b>Create a real-time chat application</b> </p> <p>To create a real-time chat application, capture audio and send it to Flash Media Server. Use the NetConnection and NetStream classes to send the audio stream to Flash Media Server. Flash Media Server can broadcast the audio to other clients. To create a chat application that doesn't require headsets, use acoustic echo cancellation. Acoustic echo cancellation prevents the feedback loop that occurs when audio enters a microphone, travels out the speakers, and enters the microphone again. To use acoustic echo cancellation, call the <code>Microphone.getEnhancedMicrophone()</code> method to get a reference to a Microphone instance. Set <code>Microphone.enhancedOptions</code> to an instance of the <code>MicrophoneEnhancedOptions</code> class to configure settings.</p> <p> <b>Play microphone audio locally</b> </p> <p>Call the Microphone <code>setLoopback()</code> method to route the microphone audio directly to the local computer or device audio output. Uncontrolled audio feedback is an inherent danger and is likely to occur whenever the audio output can be picked up by the microphone input. The <code>setUseEchoSuppression()</code> method can reduce, but not eliminate, the risk of feedback amplification.</p> <p> <b>Capture microphone audio for local recording or processing</b> </p> <p>To capture microphone audio, listen for the <code>sampleData</code> events dispatched by a Microphone instance. The SampleDataEvent object dispatched for this event contains the audio data.</p> <p>For information about capturing video, see the Camera class.</p> <p> <b>Runtime microphone support</b> </p> <p>The Microphone class is not supported in Flash Player running in a mobile browser.</p> <p> <i>AIR profile support:</i> The Microphone class is supported on desktop operating systems, and iOS and Android mobile devices. It is not supported on AIR for TV devices. See <a href="http://help.adobe.com/en_US/air/build/WS144092a96ffef7cc16ddeea2126bb46b82f-8000.html"> AIR Profile Support</a> for more information regarding API support across multiple profiles.</p> <p>You can test for support at run time using the <code>Microphone.isSupported</code> property. Note that for AIR for TV devices, <code>Microphone.isSupported</code> is <code>true</code> but <code>Microphone.getMicrophone()</code> always returns <code>null</code>.</p> <p> <b>Privacy controls</b> </p> <p> Flash Player displays a Privacy dialog box that lets the user choose whether to allow or deny access to the microphone. Your application window size must be at least 215 x 138 pixels, the minimum size required to display the dialog box, or access is denied automatically. </p> <p>Content running in the AIR application sandbox does not need permission to access the microphone and no dialog is displayed. AIR content running outside the application sandbox does require permission and the Privacy dialog is displayed.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d25.html" target="_blank">Loading external sound files</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d1d.html" target="_blank">Capturing sound input</a>
	 * </div><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d27.html" target="_blank">Basics of working with sound</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d26.html" target="_blank">Understanding the sound architecture</a>
	 *  <br>
	 *  <a href="http://mrbinitie.blogspot.com/2011/03/implementing-acoustic-echo-suppression.html" target="_blank">aYo Binitie: Implementing Acoustic Echo Suppression in Flash/Flex applications</a>
	 *  <br>
	 *  <a href="http://coenraets.org/blog/air-for-android-samples/voice-notes-for-android/" target="_blank">Cristophe Coenraets: Voice Notes for Android</a>
	 *  <br>
	 *  <a href="http://www.riagora.com/2010/08/air-android-and-the-microphone/" target="_blank">Michael Chaize: AIR, Android, and the Microphone</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Camera.html" target="">flash.media.Camera</a>
	 *  <br>
	 *  <a href="MicrophoneEnhancedMode.html" target="">flash.media.MicrophoneEnhancedMode</a>
	 *  <br>
	 *  <a href="MicrophoneEnhancedOptions.html" target="">flash.media.MicrophoneEnhancedOptions</a>
	 * </div><br><hr>
	 */
	public class Microphone extends EventDispatcher {
		private static var _isSupported:Boolean;
		private static var _names:Array;
		private static var _permissionStatus:String;

		private var _codec:String;
		private var _enableVAD:Boolean;
		private var _encodeQuality:int;
		private var _enhancedOptions:MicrophoneEnhancedOptions;
		private var _framesPerPacket:int;
		private var _gain:Number;
		private var _noiseSuppressionLevel:int;
		private var _rate:int;
		private var _soundTransform:SoundTransform;

		private var _activityLevel:Number;
		private var _index:int;
		private var _muted:Boolean;
		private var _name:String;
		private var _silenceLevel:Number;
		private var _silenceTimeout:int;
		private var _useEchoSuppression:Boolean;

		/**
		 * <p> The amount of sound the microphone is detecting. Values range from 0 (no sound is detected) to 100 (very loud sound is detected). The value of this property can help you determine a good value to pass to the <code>Microphone.setSilenceLevel()</code> method. </p>
		 * <p>If the microphone <code>muted</code> property is <code>true</code>, the value of this property is always -1.</p>
		 * 
		 * @return 
		 */
		public function get activityLevel():Number {
			return _activityLevel;
		}

		/**
		 * <p> The codec to use for compressing audio. Available codecs are Nellymoser (the default) and Speex. The enumeration class <code>SoundCodec</code> contains the various values that are valid for the <code>codec</code> property. </p>
		 * <p>If you use the Nellymoser codec, you can set the sample rate using <code>Microphone.rate()</code>. If you use the Speex codec, the sample rate is set to 16 kHz.</p>
		 * <p>Speex includes voice activity detection (VAD) and automatically reduces bandwidth when no voice is detected. When using the Speex codec, Adobe recommends that you set the silence level to 0. To set the silence level, use the <code>Microphone.setSilenceLevel()</code> method.</p>
		 * 
		 * @return 
		 */
		public function get codec():String {
			return _codec;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set codec(value:String):void {
			_codec = value;
		}

		/**
		 * <p> Enable Speex voice activity detection. </p>
		 * 
		 * @return 
		 */
		public function get enableVAD():Boolean {
			return _enableVAD;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set enableVAD(value:Boolean):void {
			_enableVAD = value;
		}

		/**
		 * <p> The encoded speech quality when using the Speex codec. Possible values are from 0 to 10. The default value is 6. Higher numbers represent higher quality but require more bandwidth, as shown in the following table. The bit rate values that are listed represent net bit rates and do not include packetization overhead. </p>
		 * <p> </p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Quality value</th>
		 *    <th>Required bit rate (kilobits per second)</th>
		 *   </tr>
		 *   <tr>
		 *    <td>0</td>
		 *    <td> 3.95</td>
		 *   </tr>
		 *   <tr>
		 *    <td>1</td>
		 *    <td>5.75</td>
		 *   </tr>
		 *   <tr>
		 *    <td>2</td>
		 *    <td>7.75</td>
		 *   </tr>
		 *   <tr>
		 *    <td>3</td>
		 *    <td>9.80</td>
		 *   </tr>
		 *   <tr>
		 *    <td>4</td>
		 *    <td>12.8</td>
		 *   </tr>
		 *   <tr>
		 *    <td>5</td>
		 *    <td>16.8</td>
		 *   </tr>
		 *   <tr>
		 *    <td>6</td>
		 *    <td>20.6</td>
		 *   </tr>
		 *   <tr>
		 *    <td>7</td>
		 *    <td>23.8</td>
		 *   </tr>
		 *   <tr>
		 *    <td>8</td>
		 *    <td>27.8</td>
		 *   </tr>
		 *   <tr>
		 *    <td>9</td>
		 *    <td>34.2</td>
		 *   </tr>
		 *   <tr>
		 *    <td>10</td>
		 *    <td>42.2</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * 
		 * @return 
		 */
		public function get encodeQuality():int {
			return _encodeQuality;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set encodeQuality(value:int):void {
			_encodeQuality = value;
		}

		/**
		 * <p> Controls enhanced microphone options. For more information, see <code>MicrophoneEnhancedOptions</code> class. This property is ignored for non-enhanced Microphone instances. </p>
		 * 
		 * @return 
		 */
		public function get enhancedOptions():MicrophoneEnhancedOptions {
			return _enhancedOptions;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set enhancedOptions(value:MicrophoneEnhancedOptions):void {
			_enhancedOptions = value;
		}

		/**
		 * <p> Number of Speex speech frames transmitted in a packet (message). Each frame is 20 ms long. The default value is two frames per packet. </p>
		 * <p>The more Speex frames in a message, the lower the bandwidth required but the longer the delay in sending the message. Fewer Speex frames increases bandwidth required but reduces delay.</p>
		 * 
		 * @return 
		 */
		public function get framesPerPacket():int {
			return _framesPerPacket;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set framesPerPacket(value:int):void {
			_framesPerPacket = value;
		}

		/**
		 * <p> The amount by which the microphone boosts the signal. Valid values are 0 to 100. The default value is 50. </p>
		 * 
		 * @return 
		 */
		public function get gain():Number {
			return _gain;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set gain(value:Number):void {
			_gain = value;
		}

		/**
		 * <p> The index of the microphone, as reflected in the array returned by <code>Microphone.names</code>. </p>
		 * 
		 * @return 
		 */
		public function get index():int {
			return _index;
		}

		/**
		 * <p> Specifies whether the user has denied access to the microphone (<code>true</code>) or allowed access (<code>false</code>). When this value changes, a <code>status</code> event is dispatched. For more information, see <code>Microphone.getMicrophone()</code>. </p>
		 * 
		 * @return 
		 */
		public function get muted():Boolean {
			return _muted;
		}

		/**
		 * <p> The name of the current sound capture device, as returned by the sound capture hardware. </p>
		 * 
		 * @return 
		 */
		public function get name():String {
			return _name;
		}

		/**
		 * <p> Maximum attenuation of the noise in dB (negative number) used for Speex encoder. If enabled, noise suppression is applied to sound captured from Microphone before Speex compression. Set to 0 to disable noise suppression. Noise suppression is enabled by default with maximum attenuation of -30 dB. Ignored when Nellymoser codec is selected. </p>
		 * 
		 * @return 
		 */
		public function get noiseSuppressionLevel():int {
			return _noiseSuppressionLevel;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set noiseSuppressionLevel(value:int):void {
			_noiseSuppressionLevel = value;
		}

		/**
		 * <p> The rate at which the microphone is capturing sound, in kHz. Acceptable values are 5, 8, 11, 22, and 44. The default value is 8 kHz if your sound capture device supports this value. Otherwise, the default value is the next available capture level above 8 kHz that your sound capture device supports, usually 11 kHz. </p>
		 * <p><b>Note:</b> The actual rate differs slightly from the <code>rate</code> value, as noted in the following table:</p>
		 * <table class="+ topic/table adobe-d/adobetable ">
		 *  <tbody>
		 *   <tr>
		 *    <th><code>rate</code> value</th>
		 *    <th>Actual frequency</th>
		 *   </tr>
		 *   <tr>
		 *    <td>44</td>
		 *    <td>44,100 Hz</td>
		 *   </tr>
		 *   <tr>
		 *    <td>22</td>
		 *    <td>22,050 Hz</td>
		 *   </tr>
		 *   <tr>
		 *    <td>11</td>
		 *    <td>11,025 Hz</td>
		 *   </tr>
		 *   <tr>
		 *    <td>8</td>
		 *    <td>8,000 Hz</td>
		 *   </tr>
		 *   <tr>
		 *    <td>5</td>
		 *    <td>5,512 Hz</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * 
		 * @return 
		 */
		public function get rate():int {
			return _rate;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set rate(value:int):void {
			_rate = value;
		}

		/**
		 * <p> The amount of sound required to activate the microphone and dispatch the <code>activity</code> event. The default value is 10. </p>
		 * 
		 * @return 
		 */
		public function get silenceLevel():Number {
			return _silenceLevel;
		}

		/**
		 * <p> The number of milliseconds between the time the microphone stops detecting sound and the time the <code>activity</code> event is dispatched. The default value is 2000 (2 seconds). </p>
		 * <p>To set this value, use the <code>Microphone.setSilenceLevel()</code> method.</p>
		 * 
		 * @return 
		 */
		public function get silenceTimeout():int {
			return _silenceTimeout;
		}

		/**
		 * <p> Controls the sound of this microphone object when it is in loopback mode. </p>
		 * 
		 * @return 
		 */
		public function get soundTransform():SoundTransform {
			return _soundTransform;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set soundTransform(value:SoundTransform):void {
			_soundTransform = value;
		}

		/**
		 * <p> Set to <code>true</code> if echo suppression is enabled; <code>false</code> otherwise. The default value is <code>false</code> unless the user has selected Reduce Echo in the Flash Player Microphone Settings panel. </p>
		 * 
		 * @return 
		 */
		public function get useEchoSuppression():Boolean {
			return _useEchoSuppression;
		}

		/**
		 * <p> Requests Microphone permission for the application. </p>
		 */
		public function requestPermission():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Routes audio captured by a microphone to the local speakers. </p>
		 * 
		 * @param state
		 */
		public function setLoopBack(state:Boolean = true):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets the minimum input level that should be considered sound and (optionally) the amount of silent time signifying that silence has actually begun. </p>
		 * <ul>
		 *  <li>To prevent the microphone from detecting sound at all, pass a value of 100 for <code>silenceLevel</code>; the <code>activity</code> event is never dispatched. </li>
		 *  <li>To determine the amount of sound the microphone is currently detecting, use <code>Microphone.activityLevel</code>. </li>
		 * </ul>
		 * <p>Speex includes voice activity detection (VAD) and automatically reduces bandwidth when no voice is detected. When using the Speex codec, Adobe recommends that you set the silence level to 0.</p>
		 * <p>Activity detection is the ability to detect when audio levels suggest that a person is talking. When someone is not talking, bandwidth can be saved because there is no need to send the associated audio stream. This information can also be used for visual feedback so that users know they (or others) are silent.</p>
		 * <p>Silence values correspond directly to activity values. Complete silence is an activity value of 0. Constant loud noise (as loud as can be registered based on the current gain setting) is an activity value of 100. After gain is appropriately adjusted, your activity value is less than your silence value when you're not talking; when you are talking, the activity value exceeds your silence value.</p>
		 * <p>This method is similar to <code>Camera.setMotionLevel()</code>; both methods are used to specify when the <code>activity</code> event is dispatched. However, these methods have a significantly different impact on publishing streams:</p>
		 * <ul>
		 *  <li><code>Camera.setMotionLevel()</code> is designed to detect motion and does not affect bandwidth usage. Even if a video stream does not detect motion, video is still sent.</li>
		 *  <li><code>Microphone.setSilenceLevel()</code> is designed to optimize bandwidth. When an audio stream is considered silent, no audio data is sent. Instead, a single message is sent, indicating that silence has started. </li>
		 * </ul>
		 * 
		 * @param silenceLevel  — The amount of sound required to activate the microphone and dispatch the <code>activity</code> event. Acceptable values range from 0 to 100. 
		 * @param timeout  — The number of milliseconds that must elapse without activity before Flash Player or Adobe AIR considers sound to have stopped and dispatches the <code>dispatch</code> event. The default value is 2000 (2 seconds). (<b>Note</b>: The default value shown in the signature, -1, is an internal value that indicates to Flash Player or Adobe AIR to use 2000.) 
		 */
		public function setSilenceLevel(silenceLevel:Number, timeout:int = -1):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Specifies whether to use the echo suppression feature of the audio codec. The default value is <code>false</code> unless the user has selected Reduce Echo in the Flash Player Microphone Settings panel. </p>
		 * <p>Echo suppression is an effort to reduce the effects of audio feedback, which is caused when sound going out the speaker is picked up by the microphone on the same system. (This is different from acoustic echo cancellation, which completely removes the feedback. The <code>setUseEchoSuppression()</code> method is ignored when you call the <code>getEnhancedMicrophone()</code> method to use acoustic echo cancellation.)</p>
		 * <p>Generally, echo suppression is advisable when the sound being captured is played through speakers — instead of a headset —. If your SWF file allows users to specify the sound output device, you may want to call <code>Microphone.setUseEchoSuppression(true)</code> if they indicate they are using speakers and will be using the microphone as well. </p>
		 * <p>Users can also adjust these settings in the Flash Player Microphone Settings panel.</p>
		 * 
		 * @param useEchoSuppression  — A Boolean value indicating whether to use echo suppression (<code>true</code>) or not (<code>false</code>). 
		 */
		public function setUseEchoSuppression(useEchoSuppression:Boolean):void {
			throw new Error("Not implemented");
		}


		/**
		 * <p> The <code>isSupported</code> property is set to <code>true</code> if the Microphone class is supported on the current platform, otherwise it is set to <code>false</code>. </p>
		 * 
		 * @return 
		 */
		public static function get isSupported():Boolean {
			return _isSupported;
		}


		/**
		 * <p> An array of strings containing the names of all available sound capture devices. The names are returned without having to display the Flash Player Privacy Settings panel to the user. This array provides the zero-based index of each sound capture device and the number of sound capture devices on the system, through the <code>Microphone.names.length</code> property. For more information, see the Array class entry. </p>
		 * <p>Calling <code>Microphone.names</code> requires an extensive examination of the hardware, and it may take several seconds to build the array. In most cases, you can just use the default microphone.</p>
		 * <p><b>Note:</b> To determine the name of the current microphone, use the <code>name</code> property.</p>
		 * 
		 * @return 
		 */
		public static function get names():Array {
			return _names;
		}


		/**
		 * <p> Determine whether the application has been granted the permission to use Microphone. </p>
		 * 
		 * @return 
		 */
		public static function get permissionStatus():String {
			return _permissionStatus;
		}


		/**
		 * <p> Returns a reference to an enhanced Microphone object that can perform acoustic echo cancellation. Use acoustic echo cancellation to create audio/video chat applications that don't require headsets. </p>
		 * <p>The <code>index</code> parameter for the <code>Microphone.getEnhancedMicrophone()</code> method and the <code>Microphone.getMicrophone()</code> method work the same way.</p>
		 * <p>To use this method on Android, add <code>MODIFY_AUDIO_SETTINGS</code> permission under Android manifest additions in the application descriptor.</p>
		 * <p><b>Important:</b> At any given time you can have only a single instance of enhanced microphone device. All other Microphone instances stop providing audio data and receive a <code>StatusEvent</code> with the <code>code</code> property <code>Microphone.Unavailable</code>. When enhanced audio fails to initialize, calls to this method return <code>null</code>, setting a value for <code>Microphone.enhancedOptions</code> has no effect, and all existing Microphone instances function as before.</p>
		 * <p>To configure an enhanced Microphone object, set the <code>Microphone.enhancedOptions</code> property. The following code uses an enhanced Microphone object and full-duplex acoustic echo cancellation in a local test:</p>
		 * <pre>
		 * 	     var mic:Microphone = Microphone.getEnhancedMicrophone();
		 * 	     var options:MicrophoneEnhancedOptions = new MicrophoneEnhancedOptions();
		 * 	     options.mode = MicrophoneEnhancedMode.FULL_DUPLEX;
		 * 	     mic.enhancedOptions = options;
		 * 	     mic.setLoopBack(true);
		 * 	 </pre>
		 * <p>The <code>setUseEchoSuppression()</code> method is ignored when using acoustic echo cancellation. </p>
		 * <p> When a SWF file tries to access the object returned by <code>Microphone.getEnhancedMicrophone()</code> —for example, when you call <code>NetStream.attachAudio()</code>— Flash Player displays a Privacy dialog box that lets the user choose whether to allow or deny access to the microphone. (Make sure your Stage size is at least 215 x 138 pixels; this is the minimum size Flash Player requires to display the dialog box.) </p>
		 * 
		 * @param index  — The index value of the microphone. 
		 * @return  — A reference to a Microphone object for capturing audio. If enhanced audio fails to initialize, returns . 
		 */
		public static function getEnhancedMicrophone(index:int = -1):Microphone {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Returns a reference to a Microphone object for capturing audio. To begin capturing the audio, you must attach the Microphone object to a NetStream object (see <code>NetStream.attachAudio()</code>). </p>
		 * <p> Multiple calls to <code>Microphone.getMicrophone()</code> reference the same microphone. Thus, if your code contains the lines <code>mic1 = Microphone.getMicrophone()</code> and <code>mic2 = Microphone.getMicrophone()</code> , both <code>mic1</code> and <code>mic2</code> reference the same (default) microphone.</p>
		 * <p> In general, you should not pass a value for <code>index</code>. Simply call <code>air.Microphone.getMicrophone()</code> to return a reference to the default microphone. Using the Microphone Settings section in the Flash Player settings panel, the user can specify the default microphone the application should use. (The user access the Flash Player settings panel by right-clicking Flash Player content running in a web browser.) If you pass a value for <code>index</code>, you can reference a microphone other than the one the user chooses. You can use <code>index</code> in rare cases—for example, if your application is capturing audio from two microphones at the same time. Content running in Adobe AIR also uses the Flash Player setting for the default microphone.</p>
		 * <p> Use the <code>Microphone.index</code> property to get the index value of the current Microphone object. You can then pass this value to other methods of the Microphone class. </p>
		 * <p> When a SWF file tries to access the object returned by <code>Microphone.getMicrophone()</code> —for example, when you call <code>NetStream.attachAudio()</code>— Flash Player displays a Privacy dialog box that lets the user choose whether to allow or deny access to the microphone. (Make sure your Stage size is at least 215 x 138 pixels; this is the minimum size Flash Player requires to display the dialog box.) </p>
		 * <p> When the user responds to this dialog box, a <code>status</code> event is dispatched that indicates the user's response. You can also check the <code>Microphone.muted</code> property to determine if the user has allowed or denied access to the microphone. </p>
		 * <p> If <code>Microphone.getMicrophone()</code> returns <code>null</code>, either the microphone is in use by another application, or there are no microphones installed on the system. To determine whether any microphones are installed, use <code>Microphones.names.length</code>. To display the Flash Player Microphone Settings panel, which lets the user choose the microphone to be referenced by <code>Microphone.getMicrophone</code>, use <code>Security.showSettings()</code>. </p>
		 * 
		 * @param index  — The index value of the microphone. 
		 * @return  — A reference to a Microphone object for capturing audio. 
		 */
		public static function getMicrophone(index:int = -1):Microphone {
			throw new Error("Not implemented");
		}
	}
}
