package flash.media {
	/**
	 *  The MicrophoneEnhancedOptions class provides configuration options for enhanced audio (acoustic echo cancellation). Acoustic echo cancellation allows multiple parties to communicate in an audio/video chat application without using headsets. <p>To use acoustic echo cancellation, call <code>Microphone.getEnhancedMicrophone()</code> to get a reference to an enhanced Microphone object. Set the <code>Microphone.enhancedOptions</code> property to an instance of the <code>MicrophoneEnhancedOptions</code> class. </p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Microphone.html#enhancedOptions" target="">flash.media.Microphone.enhancedOptions</a>
	 *  <br>
	 *  <a href="Microphone.html#getEnhancedMicrophone()" target="">flash.media.Microphone.getEnhancedMicrophone()</a>
	 * </div><br><hr>
	 */
	public class MicrophoneEnhancedOptions {
		private var _echoPath:int;
		private var _isVoiceDetected:int;
		private var _mode:String;
		private var _nonLinearProcessing:Boolean;

		public function MicrophoneEnhancedOptions() {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Specifies the echo path (in milliseconds) used for acoustic echo cancellation. A longer echo path results in better echo cancellation. A longer echo path also causes a longer delay and requires more computational complexity. The default value is 128 (recommended). The other possible value is 256. </p>
		 * 
		 * @return 
		 */
		public function get echoPath():int {
			return _echoPath;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set echoPath(value:int):void {
			_echoPath = value;
		}

		/**
		 * <p> Indicates whether the Microphone input detected a voice. </p>
		 * <p> Possible values are: <code>-1</code>, not enabled; <code>0</code>, a voice is not detected; <code>1</code>, a voice is detected. </p>
		 * 
		 * @return 
		 */
		public function get isVoiceDetected():int {
			return _isVoiceDetected;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set isVoiceDetected(value:int):void {
			_isVoiceDetected = value;
		}

		/**
		 * <p> Controls enhanced microphone mode. The default value is <code>FULL_DUPLEX</code> for all microphones that aren't USB. The default value for USB microphones is <code>HALF_DUPLEX</code>. See <code>MicrophoneEnhancedMode</code> for possible values and descriptions. </p>
		 * <p><b>Note:</b> This feature is not available on iOS. </p>
		 * 
		 * @return 
		 */
		public function get mode():String {
			return _mode;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set mode(value:String):void {
			_mode = value;
		}

		/**
		 * <p> Enable non-linear processing. Non-linear processing suppresses the residual echo when one person is talking. The time-domain non-linear processing technique is used. Turn off non-linear processing for music sources. The default value is <code>true</code> which turns on non-linear processing. </p>
		 * 
		 * @return 
		 */
		public function get nonLinearProcessing():Boolean {
			return _nonLinearProcessing;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set nonLinearProcessing(value:Boolean):void {
			_nonLinearProcessing = value;
		}
	}
}
