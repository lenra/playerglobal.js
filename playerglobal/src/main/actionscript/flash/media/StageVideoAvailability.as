package flash.media {
	/**
	 *  This class defines an enumeration that indicates whether stage video is currently available. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../flash/events/StageVideoAvailabilityEvent.html" target="">flash.events.StageVideoAvailabilityEvent</a>
	 * </div><br><hr>
	 */
	public class StageVideoAvailability {
		/**
		 * <p> Stage video is currently available. </p>
		 */
		public static const AVAILABLE:String = "available";
		/**
		 * <p> Stage video is not currently available. </p>
		 */
		public static const UNAVAILABLE:String = "unavailable";
	}
}
