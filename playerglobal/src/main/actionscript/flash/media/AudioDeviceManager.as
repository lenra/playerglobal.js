package flash.media {
	import flash.events.AudioOutputChangeEvent;
	import flash.events.EventDispatcher;

	[Event(name="audioOutputChange", type="flash.events.AudioOutputChangeEvent")]
	/**
	 *  Use the AudioDeviceManager class to get audio device information of the system, and select a device for audio playback. User can change audio output device either through Flash Player's Settings UI, or the AudioDeviceManager API. Both of them are in sync with audio output settings. AudioDeviceManager API was enabled for Flash Player 27. It is now enabled for AIR Desktop from AIR 28. <p> The audio device selected from one AIR application does not affect the audio from other AIR applications or Flash Player instances.</p> <p> <b>Privacy Restriction</b> </p> <p> AudioDeviceManager API is under User Invoked Action (UIA) restriction, that is, it can only be invoked with some user interaction. If this API is not invoked by user interaction, Flash Player throws runtime error IllegalOperationError with error code set to 2176. In case of AIR applications, the UIA check will be applied when we load an external SWF/HTML hosted over a network. If the externally loaded SWF/HTML tries to change the audio output device without any user invoked action, then AIR runtime throws an error IllegalOperationError with error code 2176. </p> <p> <b>Access AudioDeviceManager instance</b> </p> <p> AudioDeviceManager instance is a singleton object, it is in sync with Flash Player's Audio Output Settings. Client should use <code>AudioDeviceManager.audioDeviceManager</code> to get a reference to this singleton object. </p> <p> <b>Get the current audio devices available on the system</b> </p> <p> Use <code>AudioDeviceManager.deviceNames</code> to get all the available audio output devices in the system. </p> <p> <b>Get the current selected audio device</b> </p> <p> Use <code>AudioDeviceManager.selectedDeviceIndex</code> to find the index of the current used audio output device. Use this index to find the device name the device list returned from <code>AudioDeviceManager.deviceNames</code> </p> <p> <b>Select an audio output device</b> </p> <p> Set <code>AudioDeviceManager.selectedDeviceIndex</code> to a different value can make that device to be the current selected audio playback device. </p> <p> <b>Monitor audio output device change</b> </p> <p> Audio output device may change because of user selecting a different device from Flash Player's Settings UI, Content setting <code>AudioDeviceManager.selectedDeviceIndex</code>, audio device being added/removed from the system. Client application can register listener to event: <code>AudioOutputChangeEvent.AUDIO_OUTPUT_CHANGE</code> to receive notification when audio output device change happens. The <code>reason</code> property of the event object indicates how this change is triggered. There are 2 possible values for <code>reason</code> property: <code>AudioOutputChangeReason.USER_SELECTION</code> indicates that user selects a different audio output device through Flash Player's Settings UI, or Content sets <code>AudioDeviceManager.selectedDeviceIndex</code>. <code>AudioOutputChangeReason.DEVICE_CHANGE</code> indicates that audio output device has been added or removed from the system. </p> <br><hr>
	 */
	public class AudioDeviceManager extends EventDispatcher {
		private static var _audioDeviceManager:AudioDeviceManager;
		private static var _isSupported:Boolean;

		private var _selectedDeviceIndex:int;

		private var _deviceNames:Array;

		/**
		 * @return 
		 */
		public function get deviceNames():Array {
			return _deviceNames;
		}

		/**
		 * @return 
		 */
		public function get selectedDeviceIndex():int {
			return _selectedDeviceIndex;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set selectedDeviceIndex(value:int):void {
			_selectedDeviceIndex = value;
		}


		/**
		 * @return 
		 */
		public static function get audioDeviceManager():AudioDeviceManager {
			return _audioDeviceManager;
		}


		/**
		 * @return 
		 */
		public static function get isSupported():Boolean {
			return _isSupported;
		}
	}
}
