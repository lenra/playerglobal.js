package flash.media {
	import flash.display.BitmapData;
	import flash.events.ActivityEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.PermissionEvent;
	import flash.events.StatusEvent;
	import flash.geom.Rectangle;
	import flash.utils.ByteArray;

	[Event(name="activity", type="flash.events.ActivityEvent")]
	[Event(name="permissionStatus", type="flash.events.PermissionEvent")]
	[Event(name="status", type="flash.events.StatusEvent")]
	[Event(name="videoFrame", type="flash.events.Event")]
	/**
	 *  Use the Camera class to capture video from the client system or device camera. Use the Video class to monitor the video locally. Use the NetConnection and NetStream classes to transmit the video to Flash Media Server. Flash Media Server can send the video stream to other servers and broadcast it to other clients running Flash Player or AIR. <p>A Camera instance captures video in landscape aspect ratio. On devices that can change the screen orientation, such as mobile phones, a Video object attached to the camera will only show upright video in a landscape-aspect orientation. Thus, mobile apps should use a landscape orientation when displaying video and should not auto-rotate.</p> <p>On iOS, the video from the front camera is mirrored. On Android, it is not.</p> <p>On mobile devices with an autofocus camera, autofocus is enabled automatically. If the camera does not support continuous autofocus, and many mobile device cameras do not, then the camera is focused when the Camera object is attached to a video stream and whenever the <code>setMode()</code> method is called. On desktop computers, autofocus behavior is dependent on the camera driver and settings.</p> <p>In an AIR application on Android and iOS, the camera does not capture video while an AIR app is not the active, foreground application. In addition, streaming connections can be lost when the application is in the background. On iOS, the camera video cannot be displayed when an application uses the GPU rendering mode. The camera video can still be streamed to a server.</p> <p> <i>AIR profile support:</i> This feature is not supported on AIR for TV devices. See <a href="http://help.adobe.com/en_US/air/build/WS144092a96ffef7cc16ddeea2126bb46b82f-8000.html"> AIR Profile Support</a> for more information regarding API support across multiple profiles. Note that for AIR for TV devices, <code>Camera.isSupported</code> is <code>true</code> but <code>Camera.getCamera()</code> always returns <code>null</code>. Camera access is not supported in mobile browsers.</p> <p> For information about capturing audio, see the Microphone class. </p> <p> <b>Important: </b>The runtime displays a Privacy dialog box that lets the user choose whether to allow or deny access to the camera. Make sure your application window size is at least 215 x 138 pixels; this is the minimum size required to display the dialog box. </p> <p>To create or reference a Camera object, use the <code>getCamera()</code> method.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b8f074-7ff3.html" target="_blank">Connecting to a user’s camera</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d38.html" target="_blank">Verifying that cameras are installed</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d37.html" target="_blank">Detecting permissions for camera access</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b8f074-7feb.html" target="_blank">Maximizing camera video quality</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d2c.html" target="_blank">Monitoring camera status</a>
	 * </div><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d50.html" target="_blank">Basics of video</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d46.html" target="_blank">Understanding video formats</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WSF5341774-D3CE-4749-93AE-3BA62B73733C.html" target="_blank">Flash Player and AIR compatibility with encoded video files</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS513F633F-460F-43aa-9A10-839853A1ECA4.html" target="_blank">Understanding the Adobe F4V and FLV video file formats</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d48.html" target="_blank">Advanced topics for video files</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d47.html" target="_blank">Video example: Video Jukebox</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d2e.html" target="_blank">Understanding the Camera class</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d2d.html" target="_blank">Displaying camera content on screen</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b8f074-7fdf.html" target="_blank">Designing your camera application</a>
	 *  <br>
	 *  <a href="http://coenraets.org/blog/2010/07/video-chat-for-android-in-30-lines-of-code/" target="_blank">Cristophe Coenraets: Video Chat for Android in 30 Lines of Code</a>
	 *  <br>
	 *  <a href="http://www.riagora.com/2010/07/android-air-and-the-camera/" target="_blank">Michael Chaize: Android, AIR, and the Camera</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Microphone.html" target="">flash.media.Microphone</a>
	 * </div><br><hr>
	 */
	public class Camera extends EventDispatcher {
		private static var _isSupported:Boolean;
		private static var _names:Array;
		private static var _permissionStatus:String;

		private var _activityLevel:Number;
		private var _bandwidth:int;
		private var _currentFPS:Number;
		private var _fps:Number;
		private var _height:int;
		private var _index:int;
		private var _keyFrameInterval:int;
		private var _loopback:Boolean;
		private var _motionLevel:int;
		private var _motionTimeout:int;
		private var _muted:Boolean;
		private var _name:String;
		private var _position:String;
		private var _quality:int;
		private var _width:int;

		/**
		 * <p> The amount of motion the camera is detecting. Values range from 0 (no motion is being detected) to 100 (a large amount of motion is being detected). The value of this property can help you determine if you need to pass a setting to the <code>setMotionLevel()</code> method. </p>
		 * <p>If the camera is available but is not yet being used because the <code>Video.attachCamera()</code> method has not been called, this property is set to -1.</p>
		 * <p>If you are streaming only uncompressed local video, this property is set only if you have assigned a function to the event handler. Otherwise, it is undefined.</p>
		 * 
		 * @return 
		 */
		public function get activityLevel():Number {
			return _activityLevel;
		}

		/**
		 * <p> The maximum amount of bandwidth the current outgoing video feed can use, in bytes. A value of 0 means the feed can use as much bandwidth as needed to maintain the desired frame quality. </p>
		 * <p>To set this property, use the <code>setQuality()</code> method.</p>
		 * 
		 * @return 
		 */
		public function get bandwidth():int {
			return _bandwidth;
		}

		/**
		 * <p> The rate at which the camera is capturing data, in frames per second. This property cannot be set; however, you can use the <code>setMode()</code> method to set a related property—<code>fps</code>—which specifies the maximum frame rate at which you would like the camera to capture data. </p>
		 * 
		 * @return 
		 */
		public function get currentFPS():Number {
			return _currentFPS;
		}

		/**
		 * <p> The maximum rate at which the camera can capture data, in frames per second. The maximum rate possible depends on the capabilities of the camera; this frame rate may not be achieved. </p>
		 * <ul>
		 *  <li>To set a desired value for this property, use the <code>setMode()</code> method.</li>
		 *  <li>To determine the rate at which the camera is currently capturing data, use the <code>currentFPS</code> property.</li>
		 * </ul>
		 * 
		 * @return 
		 */
		public function get fps():Number {
			return _fps;
		}

		/**
		 * <p> The current capture height, in pixels. To set a value for this property, use the <code>setMode()</code> method. </p>
		 * 
		 * @return 
		 */
		public function get height():int {
			return _height;
		}

		/**
		 * <p> A zero-based index that specifies the position of the camera in the <code>Camera.names</code> array, which lists all available cameras. </p>
		 * 
		 * @return 
		 */
		public function get index():int {
			return _index;
		}

		/**
		 * <p> The number of video frames transmitted in full (called <i>keyframes</i>) instead of being interpolated by the video compression algorithm. The default value is 15, which means that every 15th frame is a keyframe. A value of 1 means that every frame is a keyframe. The allowed values are 1 through 300. </p>
		 * 
		 * @return 
		 */
		public function get keyFrameInterval():int {
			return _keyFrameInterval;
		}

		/**
		 * <p> Indicates whether a local view of what the camera is capturing is compressed and decompressed (<code>true</code>), as it would be for live transmission using Flash Media Server, or uncompressed (<code>false</code>). The default value is <code>false</code>. </p>
		 * <p> Although a compressed stream is useful for testing, such as when previewing video quality settings, it has a significant processing cost. The local view is compressed, edited for transmission as it would be over a live connection, and then decompressed for local viewing. </p>
		 * <p>To set this value, use <code>Camera.setLoopback()</code>. To set the amount of compression used when this property is true, use <code>Camera.setQuality()</code>.</p>
		 * 
		 * @return 
		 */
		public function get loopback():Boolean {
			return _loopback;
		}

		/**
		 * <p> The amount of motion required to invoke the <code>activity</code> event. Acceptable values range from 0 to 100. The default value is 50. </p>
		 * <p>Video can be displayed regardless of the value of the <code>motionLevel</code> property. For more information, see <code> setMotionLevel()</code>.</p>
		 * 
		 * @return 
		 */
		public function get motionLevel():int {
			return _motionLevel;
		}

		/**
		 * <p> The number of milliseconds between the time the camera stops detecting motion and the time the <code>activity</code> event is invoked. The default value is 2000 (2 seconds). </p>
		 * <p>To set this value, use <code>setMotionLevel()</code>. </p>
		 * 
		 * @return 
		 */
		public function get motionTimeout():int {
			return _motionTimeout;
		}

		/**
		 * <p> A Boolean value indicating whether the user has denied access to the camera (<code>true</code>) or allowed access (<code>false</code>) in the Flash Player Privacy dialog box. When this value changes, the <code>status</code>event is dispatched. </p>
		 * 
		 * @return 
		 */
		public function get muted():Boolean {
			return _muted;
		}

		/**
		 * <p> The name of the current camera, as returned by the camera hardware. </p>
		 * 
		 * @return 
		 */
		public function get name():String {
			return _name;
		}

		/**
		 * <p> Specifies the side of a device on which the camera is located. </p>
		 * <p>Use the <code>position</code> property to determine whether a camera on a mobile device is on the front or back face of the device. The following function checks every available camera until it finds a camera with the desired position. If no camera has the desired position, then the default camera is returned.</p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *      function getCamera( position:String ):Camera
		 *      {
		 *        var camera:Camera;
		 *        var cameraCount:uint = Camera.names.length;
		 *       for ( var i:uint = 0; i &lt; cameraCount; ++i )
		 *       {
		 *         camera = Camera.getCamera( String(i) );
		 *         if ( camera.position == position ) return camera;
		 *       }
		 *       return Camera.getCamera();
		 *      
		 *      </pre>
		 * </div>
		 * <p><span>On mobile devices the camera position is normally either <code>CameraPosition.FRONT</code> or <code>CameraPosition.BACK</code>. If the position of the Camera cannot be determined then the position is reported as <code>CameraPosition.UNKNOWN</code>.</span> On desktop platforms, the position is always <code>CameraPosition.UNKNOWN</code>.</p>
		 * <p>Constants for the valid values of this property are defined in the CameraPosition class </p>
		 * 
		 * @return 
		 */
		public function get position():String {
			return _position;
		}

		/**
		 * <p> The required level of picture quality, as determined by the amount of compression being applied to each video frame. Acceptable quality values range from 1 (lowest quality, maximum compression) to 100 (highest quality, no compression). The default value is 0, which means that picture quality can vary as needed to avoid exceeding available bandwidth. </p>
		 * <p>To set this property, use the <code>setQuality()</code> method.</p>
		 * 
		 * @return 
		 */
		public function get quality():int {
			return _quality;
		}

		/**
		 * <p> The current capture width, in pixels. To set a desired value for this property, use the <code>setMode()</code> method. </p>
		 * 
		 * @return 
		 */
		public function get width():int {
			return _width;
		}

		/**
		 * <p> Fills a byte array from a rectangular region of pixel data </p>
		 * <p>Writes an unsigned integer (a 32-bit unmultiplied pixel value) for each pixel into the byte array. The array is resized to the necessary number of bytes to hold all the pixels.</p>
		 * 
		 * @param rect  — A rectangular area in the current BitmapData object. 
		 * @param destination  — A ByteArray representing the pixels in the given rectangle. 
		 */
		public function copyToByteArray(rect:Rectangle, destination:ByteArray):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Fills a vector from a rectangular region of pixel data. </p>
		 * <p>Writes an unsigned integer (a 32-bit unmultiplied pixel value) for each pixel into the vector. The vector is resized to the necessary number of entries to hold all the pixels.</p>
		 * 
		 * @param rect  — A rectangular area in the current BitmapData object. 
		 * @param destination  — A Vector.&lt;uint&gt; representing the pixels in the given rectangle. 
		 */
		public function copyToVector(rect:Rectangle, destination:Vector.<uint>):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Copies the last frame to a bitmap. </p>
		 * <p>This method copies the contents of the last frame to a BitmapData instance.</p>
		 * 
		 * @param destination  — A BitmapData instance object to contain the last frame. 
		 */
		public function drawToBitmapData(destination:BitmapData):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Requests camera permission for the application. </p>
		 */
		public function requestPermission():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Specifies which video frames are transmitted in full (called <i>keyframes</i>) instead of being interpolated by the video compression algorithm. This method is applicable only if you are transmitting video using Flash Media Server. </p>
		 * <p>The Flash Video compression algorithm compresses video by transmitting only what has changed since the last frame of the video; these portions are considered to be interpolated frames. Frames of a video can be interpolated according to the contents of the previous frame. A keyframe, however, is a video frame that is complete; it is not interpolated from prior frames.</p>
		 * <p>To determine how to set a value for the <code>keyFrameInterval</code> parameter, consider both bandwidth use and video playback accessibility. For example, specifying a higher value for <code>keyFrameInterval</code> (sending keyframes less frequently) reduces bandwidth use. However, this may increase the amount of time required to position the playhead at a particular point in the video; more prior video frames may have to be interpolated before the video can resume.</p>
		 * <p>Conversely, specifying a lower value for <code>keyFrameInterval</code> (sending keyframes more frequently) increases bandwidth use because entire video frames are transmitted more often, but may decrease the amount of time required to seek a particular video frame within a recorded video.</p>
		 * 
		 * @param keyFrameInterval  — A value that specifies which video frames are transmitted in full (as keyframes) instead of being interpolated by the video compression algorithm. A value of 1 means that every frame is a keyframe, a value of 3 means that every third frame is a keyframe, and so on. Acceptable values are 1 through 48. 
		 */
		public function setKeyFrameInterval(keyFrameInterval:int):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Specifies whether to use a compressed video stream for a local view of the camera. This method is applicable only if you are transmitting video using Flash Media Server; setting <code>compress</code> to <code>true</code> lets you see more precisely how the video will appear to users when they view it in real time. </p>
		 * <p>Although a compressed stream is useful for testing purposes, such as previewing video quality settings, it has a significant processing cost, because the local view is not simply compressed; it is compressed, edited for transmission as it would be over a live connection, and then decompressed for local viewing.</p>
		 * <p>To set the amount of compression used when you set <code>compress</code> to <code>true</code>, use <code>Camera.setQuality()</code>.</p>
		 * 
		 * @param compress  — Specifies whether to use a compressed video stream (<code>true</code>) or an uncompressed stream (<code>false</code>) for a local view of what the camera is receiving. 
		 */
		public function setLoopback(compress:Boolean = false):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets the camera capture mode to the native mode that best meets the specified requirements. If the camera does not have a native mode that matches all the parameters you pass, the runtime selects a capture mode that most closely synthesizes the requested mode. This manipulation may involve cropping the image and dropping frames. </p>
		 * <p>By default, the runtime drops frames as needed to maintain image size. To minimize the number of dropped frames, even if this means reducing the size of the image, pass <code>false</code> for the <code>favorArea</code> parameter.</p>
		 * <p>When choosing a native mode, the runtime tries to maintain the requested aspect ratio whenever possible. For example, if you issue the command <code>myCam.setMode(400, 400, 30)</code>, and the maximum width and height values available on the camera are 320 and 288, the runtime sets both the width and height at 288; by setting these properties to the same value, the runtime maintains the 1:1 aspect ratio you requested.</p>
		 * <p>To determine the values assigned to these properties after the runtime selects the mode that most closely matches your requested values, use the <code>width</code>, <code>height</code>, and <code>fps</code> properties.</p>
		 * <p> If you are using Flash Media Server, you can also capture single frames or create time-lapse photography. For more information, see <code>NetStream.attachCamera()</code>. </p>
		 * 
		 * @param width  — The requested capture width, in pixels. The default value is 160. 
		 * @param height  — The requested capture height, in pixels. The default value is 120. 
		 * @param fps  — The requested rate at which the camera should capture data, in frames per second. The default value is 15. 
		 * @param favorArea  — Specifies whether to manipulate the width, height, and frame rate if the camera does not have a native mode that meets the specified requirements. The default value is <code>true</code>, which means that maintaining capture size is favored; using this parameter selects the mode that most closely matches <code>width</code> and <code>height</code> values, even if doing so adversely affects performance by reducing the frame rate. To maximize frame rate at the expense of camera height and width, pass <code>false</code> for the <code>favorArea</code> parameter. 
		 */
		public function setMode(width:int, height:int, fps:Number, favorArea:Boolean = true):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Specifies how much motion is required to dispatch the <code>activity</code> event. Optionally sets the number of milliseconds that must elapse without activity before the runtime considers motion to have stopped and dispatches the event. </p>
		 * <p><b>Note: </b>Video can be displayed regardless of the value of the <code>motionLevel</code> parameter. This parameter determines only when and under what circumstances the event is dispatched—not whether video is actually being captured or displayed.</p>
		 * <p> To prevent the camera from detecting motion at all, pass a value of 100 for the <code>motionLevel</code> parameter; the <code>activity</code> event is never dispatched. (You would probably use this value only for testing purposes—for example, to temporarily disable any handlers that would normally be triggered when the event is dispatched.) </p>
		 * <p> To determine the amount of motion the camera is currently detecting, use the <code>activityLevel</code> property. Motion sensitivity values correspond directly to activity values. Complete lack of motion is an activity value of 0. Constant motion is an activity value of 100. Your activity value is less than your motion sensitivity value when you're not moving; when you are moving, activity values frequently exceed your motion sensitivity value. </p>
		 * <p> This method is similar in purpose to the <code>Microphone.setSilenceLevel()</code> method; both methods are used to specify when the <code>activity</code> event should be dispatched. However, these methods have a significantly different impact on publishing streams: </p>
		 * <ul>
		 *  <li><code>Microphone.setSilenceLevel()</code> is designed to optimize bandwidth. When an audio stream is considered silent, no audio data is sent. Instead, a single message is sent, indicating that silence has started. </li>
		 *  <li><code>Camera.setMotionLevel()</code> is designed to detect motion and does not affect bandwidth usage. Even if a video stream does not detect motion, video is still sent.</li>
		 * </ul>
		 * 
		 * @param motionLevel  — Specifies the amount of motion required to dispatch the <code>activity</code> event. Acceptable values range from 0 to 100. The default value is 50. 
		 * @param timeout  — Specifies how many milliseconds must elapse without activity before the runtime considers activity to have stopped and dispatches the <code>activity</code> event. The default value is 2000 milliseconds (2 seconds). 
		 */
		public function setMotionLevel(motionLevel:int, timeout:int = 2000):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets the maximum amount of bandwidth per second or the required picture quality of the current outgoing video feed. This method is generally applicable only if you are transmitting video using Flash Media Server. </p>
		 * <p>Use this method to specify which element of the outgoing video feed is more important to your application—bandwidth use or picture quality.</p>
		 * <ul>
		 *  <li>To indicate that bandwidth use takes precedence, pass a value for <code>bandwidth</code> and 0 for <code>quality</code>. The runtime transmits video at the highest quality possible within the specified bandwidth. If necessary, the runtime reduces picture quality to avoid exceeding the specified bandwidth. In general, as motion increases, quality decreases.</li>
		 *  <li>To indicate that quality takes precedence, pass 0 for <code>bandwidth</code> and a numeric value for <code>quality</code>. The runtime uses as much bandwidth as required to maintain the specified quality. If necessary, the runtime reduces the frame rate to maintain picture quality. In general, as motion increases, bandwidth use also increases.</li>
		 *  <li>To specify that both bandwidth and quality are equally important, pass numeric values for both parameters. The runtime transmits video that achieves the specified quality and that doesn't exceed the specified bandwidth. If necessary, the runtime reduces the frame rate to maintain picture quality without exceeding the specified bandwidth.</li>
		 * </ul>
		 * 
		 * @param bandwidth  — Specifies the maximum amount of bandwidth that the current outgoing video feed can use, in bytes per second. To specify that the video can use as much bandwidth as needed to maintain the value of <code>quality</code>, pass 0 for <code>bandwidth</code>. The default value is 16384. 
		 * @param quality  — An integer that specifies the required level of picture quality, as determined by the amount of compression being applied to each video frame. Acceptable values range from 1 (lowest quality, maximum compression) to 100 (highest quality, no compression). To specify that picture quality can vary as needed to avoid exceeding bandwidth, pass 0 for <code>quality</code>. 
		 */
		public function setQuality(bandwidth:int, quality:int):void {
			throw new Error("Not implemented");
		}


		/**
		 * <p> The <code>isSupported</code> property is set to <code>true</code> if the Camera class is supported on the current platform, otherwise it is set to <code>false</code>. </p>
		 * 
		 * @return 
		 */
		public static function get isSupported():Boolean {
			return _isSupported;
		}


		/**
		 * <p> An array of strings containing the names of all available cameras. Accessing this array does not display the Flash Player Privacy dialog box. This array provides the zero-based index of each camera and the number of cameras on the system (by means of <code>names.length</code>). </p>
		 * <p>Calling the <code>names</code> property requires an extensive examination of the hardware. In most cases, you can just use the default camera.</p>
		 * 
		 * @return 
		 */
		public static function get names():Array {
			return _names;
		}


		/**
		 * <p> Determine whether the application has been granted the permission to use camera. </p>
		 * 
		 * @return 
		 */
		public static function get permissionStatus():String {
			return _permissionStatus;
		}


		/**
		 * <p> Returns a reference to a Camera object for capturing video. To begin capturing the video, you must attach the Camera object to a Video object (see <code>Video.attachCamera() </code>). To transmit video to Flash Media Server, call <code>NetStream.attachCamera()</code> to attach the Camera object to a NetStream object. </p>
		 * <p>Multiple calls to the <code>getCamera()</code> method reference the same camera driver. Thus, if your code contains code like <code>firstCam:Camera = getCamera()</code> and <code>secondCam:Camera = getCamera()</code>, both <code>firstCam</code> and <code>secondCam</code> reference the same camera, which is the user's default camera.</p>
		 * <p>On mobile devices with a both a front- and a rear-facing camera, you can only capture video from one camera at a time.</p>
		 * <p>In general, you shouldn't pass a value for the <code>name</code> parameter; simply use <code>getCamera()</code> to return a reference to the default camera. By means of the Camera settings panel (discussed later in this section), the user can specify the default camera to use. </p>
		 * <p>You can't use ActionScript to set a user's Allow or Deny permission setting for access to the camera, but you can display the Adobe Flash Player Settings camera setting dialog box where the user can set the camera permission. When a SWF file using the <code>attachCamera()</code> method tries to attach the camera returned by the <code>getCamera()</code> method to a Video or NetStream object, Flash Player displays a dialog box that lets the user choose to allow or deny access to the camera. (Make sure your application window size is at least 215 x 138 pixels; this is the minimum size Flash Player requires to display the dialog box.) When the user responds to the camera setting dialog box, Flash Player returns an information object in the <code>status</code> event that indicates the user's response: <code>Camera.Muted</code> indicates the user denied access to a camera; <code>Camera.Unmuted</code> indicates the user allowed access to a camera. To determine whether the user has denied or allowed access to the camera without handling the <code>status</code> event, use the <code>muted</code> property.</p>
		 * <p>In Flash Player, the user can specify permanent privacy settings for a particular domain by right-clicking (Windows and Linux) or Control-clicking (Macintosh) while a SWF file is playing, selecting Settings, opening the Privacy dialog, and selecting Remember. If the user selects Remember, Flash Player no longer asks the user whether to allow or deny SWF files from this domain access to your camera.</p>
		 * <p><b>Note:</b> The <code>attachCamera()</code> method will not invoke the dialog box to Allow or Deny access to the camera if the user has denied access by selecting Remember in the Flash Player Settings dialog box. In this case, you can prompt the user to change the Allow or Deny setting by displaying the Flash Player Privacy panel for the user using <code>Security.showSettings(SecurityPanel.PRIVACY)</code>.</p>
		 * <p>If <code>getCamera()</code> returns <code>null</code>, either the camera is in use by another application, or there are no cameras installed on the system. To determine whether any cameras are installed, use the <code>names.length</code> property. To display the Flash Player Camera Settings panel, which lets the user choose the camera to be referenced by <code>getCamera()</code>, use <code>Security.showSettings(SecurityPanel.CAMERA)</code>. </p>
		 * <p>Scanning the hardware for cameras takes time. When the runtime finds at least one camera, the hardware is not scanned again for the lifetime of the player instance. However, if the runtime doesn't find any cameras, it will scan each time <code>getCamera</code> is called. This is helpful if the camera is present but is disabled; if your SWF file provides a Try Again button that calls <code>getCamera</code>, Flash Player can find the camera without the user having to restart the SWF file.</p>
		 * 
		 * @param name  — Specifies which camera to get, as determined from the array returned by the <code>names</code> property. For most applications, get the default camera by omitting this parameter. To specify a value for this parameter, use the string representation of the zero-based index position within the Camera.names array. For example, to specify the third camera in the array, use <code>Camera.getCamera("2")</code>. 
		 * @return  — If the  parameter is not specified, this method returns a reference to the default camera or, if it is in use by another application, to the first available camera. <span>(If there is more than one camera installed, the user may specify the default camera in the Flash Player Camera Settings panel.)</span> If no cameras are available or installed, the method returns . 
		 */
		public static function getCamera(name:String = null):Camera {
			throw new Error("Not implemented");
		}
	}
}
