package flash.media {
	/**
	 *  The SoundCodec class is an enumeration of constant values used in setting the <code>codec</code> property of the <code>Microphone</code> class. <br><hr>
	 */
	public class SoundCodec {
		/**
		 * <p> Specifies that the Nellymoser codec be used for compressing audio. This constant is the default value of the <code>Microphone.codec</code> property. </p>
		 */
		public static const NELLYMOSER:String = "NellyMoser";
		/**
		 * <p> Specifies that the G711 A-law codec be used for compressing audio. </p>
		 */
		public static const PCMA:String = "pcma";
		/**
		 * <p> Specifies that the G711 u-law codec be used for compressing audio. </p>
		 */
		public static const PCMU:String = "pcmu";
		/**
		 * <p> Specifies that the Speex codec be used for compressing audio. </p>
		 */
		public static const SPEEX:String = "Speex";
	}
}
