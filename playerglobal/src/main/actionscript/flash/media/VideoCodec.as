package flash.media {
	/**
	 *  The VideoCodec class is an enumeration of constant values of supported video codecs. <br><hr>
	 */
	public class VideoCodec {
		/**
		 * <p> Constant value indicating that H.264/AVC codec is used for compressing video. </p>
		 */
		public static const H264AVC:String = "H264Avc";
		/**
		 * <p> Constant value indicating that Sorenson Spark codec is used for compressing video. </p>
		 */
		public static const SORENSON:String = "Sorenson";
		/**
		 * <p> Constant value indicating that On2Vp6 codec is used for compressing video. </p>
		 */
		public static const VP6:String = "VP6";
	}
}
