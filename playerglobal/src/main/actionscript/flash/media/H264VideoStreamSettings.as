package flash.media {
	/**
	 *  The H264VideoStreamSettings class enables specifying video compression settings for each NetStream. Properties will be validated once Camera is attached to NetStream and compression has started. <br><hr>
	 */
	public class H264VideoStreamSettings extends VideoStreamSettings {
		private var _codec:String;
		private var _level:String;
		private var _profile:String;

		/**
		 * <p> Creates a setting object that specifies to use H.264/AVC codec for video compression. </p>
		 */
		public function H264VideoStreamSettings() {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Video codec used for compression. </p>
		 * 
		 * @return 
		 */
		override public function get codec():String {
			return _codec;
		}

		/**
		 * <p> Level used for H.264/AVC encoding. To set the desired value for this property, please use <code>setProfileLevel()</code> method. This property can be increased by H.264 codec if the selected level is not high enough for the specified resolution and frame rate. </p>
		 * 
		 * @return 
		 */
		public function get level():String {
			return _level;
		}

		/**
		 * <p> Profile used for H.264/AVC encoding. To set the desired value for this property, please use <code>setProfileLevel()</code> method. </p>
		 * 
		 * @return 
		 */
		public function get profile():String {
			return _profile;
		}

		/**
		 * <p> Set profile and level for video encoding. Possible values for profile are <code>H264Profile.BASELINE</code> and <code>H264Profile.MAIN</code>. Default value is <code>H264Profile.BASELINE</code>. Other values are ignored and results in an error. Please see <code>H264Profile</code> class for more information. Supported levels are 1, 1b, 1.1, 1.2, 1.3, 2, 2.1, 2.2, 3, 3.1, 3.2, 4, 4.1, 4.2, 5, and 5.1. Level may be increased if required by resolution and frame rate. Please see <code>H264Level</code> class for more information. </p>
		 * 
		 * @param profile  — The requested encoder profile as a String. Please see <code>H264Profile</code> class for possible values. 
		 * @param level  — The requested encoder level as a String. Please see <code>H264Level</code> class for possible values. 
		 */
		public function setProfileLevel(profile:String, level:String):void {
			throw new Error("Not implemented");
		}
	}
}
