package flash.media {
	/**
	 *  The SoundTransform class contains properties for volume and panning. <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../flash/display/SimpleButton.html#soundTransform" target="">flash.display.SimpleButton.soundTransform</a>
	 *  <br>
	 *  <a href="../../flash/display/Sprite.html#soundTransform" target="">flash.display.Sprite.soundTransform</a>
	 *  <br>
	 *  <a href="Microphone.html#soundTransform" target="">flash.media.Microphone.soundTransform</a>
	 *  <br>
	 *  <a href="SoundChannel.html#soundTransform" target="">flash.media.SoundChannel.soundTransform</a>
	 *  <br>
	 *  <a href="SoundMixer.html#soundTransform" target="">flash.media.SoundMixer.soundTransform</a>
	 *  <br>
	 *  <a href="../../flash/net/NetStream.html#soundTransform" target="">flash.net.NetStream.soundTransform</a>
	 * </div><br><hr>
	 */
	public class SoundTransform {
		private var _leftToLeft:Number;
		private var _leftToRight:Number;
		private var _pan:Number;
		private var _rightToLeft:Number;
		private var _rightToRight:Number;
		private var _volume:Number;

		/**
		 * <p> Creates a SoundTransform object. </p>
		 * 
		 * @param vol  — The volume, ranging from 0 (silent) to 1 (full volume). 
		 * @param panning  — The left-to-right panning of the sound, ranging from -1 (full pan left) to 1 (full pan right). A value of 0 represents no panning (center). 
		 */
		public function SoundTransform(vol:Number = 1, panning:Number = 0) {
			throw new Error("Not implemented");
		}

		/**
		 * <p> A value, from 0 (none) to 1 (all), specifying how much of the left input is played in the left speaker. </p>
		 * 
		 * @return 
		 */
		public function get leftToLeft():Number {
			return _leftToLeft;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set leftToLeft(value:Number):void {
			_leftToLeft = value;
		}

		/**
		 * <p> A value, from 0 (none) to 1 (all), specifying how much of the left input is played in the right speaker. </p>
		 * 
		 * @return 
		 */
		public function get leftToRight():Number {
			return _leftToRight;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set leftToRight(value:Number):void {
			_leftToRight = value;
		}

		/**
		 * <p> The left-to-right panning of the sound, ranging from -1 (full pan left) to 1 (full pan right). A value of 0 represents no panning (balanced center between right and left). </p>
		 * 
		 * @return 
		 */
		public function get pan():Number {
			return _pan;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set pan(value:Number):void {
			_pan = value;
		}

		/**
		 * <p> A value, from 0 (none) to 1 (all), specifying how much of the right input is played in the left speaker. </p>
		 * 
		 * @return 
		 */
		public function get rightToLeft():Number {
			return _rightToLeft;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set rightToLeft(value:Number):void {
			_rightToLeft = value;
		}

		/**
		 * <p> A value, from 0 (none) to 1 (all), specifying how much of the right input is played in the right speaker. </p>
		 * 
		 * @return 
		 */
		public function get rightToRight():Number {
			return _rightToRight;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set rightToRight(value:Number):void {
			_rightToRight = value;
		}

		/**
		 * <p> The volume, ranging from 0 (silent) to 1 (full volume). </p>
		 * 
		 * @return 
		 */
		public function get volume():Number {
			return _volume;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set volume(value:Number):void {
			_volume = value;
		}
	}
}
