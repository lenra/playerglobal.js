package flash.media {
	import flash.utils.ByteArray;

	/**
	 *  The SoundMixer class contains static properties and methods for global sound control in the application. The SoundMixer class controls embedded and streaming sounds in the application, as well as dynamically created sounds (that is, sounds generated in response to a Sound object dispatching a <code>sampleData</code> event). <br><hr>
	 */
	public class SoundMixer {
		private static var _audioPlaybackMode:String;
		private static var _bufferTime:int;
		private static var _soundTransform:SoundTransform;
		private static var _useSpeakerphoneForVoice:Boolean;


		/**
		 * <p> Specifies the audio playback mode of all Sound objects. On mobile devices, this property sets sound priorities and defaults according to platform idioms. In desktop and TV environments, no functional difference exists between audio playback modes. </p>
		 * <p>Valid values for this property are defined in the AudioPlaybackMode class. </p>
		 * <p><b>Note</b> On iOS, if one application sets <code>audioPlaybackMode=AudioPlaybackMode.VOICE</code>, other applications cannot change it to <code>AudioPlaybackMode.MEDIA</code>. </p>
		 * <p>Make the minimum usage of <code>AudioPlaybackMode.VOICE</code> mode, and try to switch to <code>AudioPlaybackMode.MEDIA</code> mode as soon as you can after the voice call ends to allow other applications to play in media mode.</p>
		 * <p>When you change audio play mode on iOS, mative apps playing music pause briefly. </p>
		 * <p> The default value is <code>AudioPlaybackMode.MEDIA.</code></p>
		 * 
		 * @return 
		 */
		public static function get audioPlaybackMode():String {
			return _audioPlaybackMode;
		}


		/**
		 * @param value
		 * @return 
		 */
		public static function set audioPlaybackMode(value:String):void {
			_audioPlaybackMode = value;
		}


		/**
		 * <p> The number of seconds to preload an embedded streaming sound into a buffer before it starts to stream. The data in a loaded sound, including its buffer time, cannot be accessed by a SWF file that is in a different domain unless you implement a cross-domain policy file. For more information about security and sound, see the Sound class description. <span>The data in a loaded sound, including its buffer time, cannot be accessed by code in a file that is in a different domain unless you implement a cross-domain policy file. However, in the application sandbox in an AIR application, code can access data in sound files from any source. For more information about security and sound, see the Sound class description.</span> </p>
		 * <p>The <code>SoundMixer.bufferTime</code> property only affects the buffer time for embedded streaming sounds in a SWF and is independent of dynamically created Sound objects (that is, Sound objects created in ActionScript). The value of <code>SoundMixer.bufferTime</code> cannot override or set the default of the buffer time specified in the SoundLoaderContext object that is passed to the <code>Sound.load()</code> method.</p>
		 * 
		 * @return 
		 */
		public static function get bufferTime():int {
			return _bufferTime;
		}


		/**
		 * @param value
		 * @return 
		 */
		public static function set bufferTime(value:int):void {
			_bufferTime = value;
		}


		/**
		 * <p> The SoundTransform object that controls global sound properties. A SoundTransform object includes properties for setting volume, panning, left speaker assignment, and right speaker assignment. The SoundTransform object used in this property provides final sound settings that are applied to all sounds after any individual sound settings are applied. </p>
		 * 
		 * @return 
		 */
		public static function get soundTransform():SoundTransform {
			return _soundTransform;
		}


		/**
		 * @param value
		 * @return 
		 */
		public static function set soundTransform(value:SoundTransform):void {
			_soundTransform = value;
		}


		/**
		 * <p> Toggles the speakerphone when the device is in voice mode. By default, smartphones use the phone earpiece for audio output when <code>SoundMixer.audioPlaybackMode</code> is set to <code>AudioPlaybackMode.VOICE</code>. The <code>useSpeakerphoneForVoice</code> property lets you override the default output so that you can implement a speakerphone button in a phone application. This property has no effect in modes other than <code>AudioPlaybackMode.VOICE</code>. In desktop and TV environments, this property has no effect. </p>
		 * <p><b>Note</b> On iOS, if your application has set <code>audioPlaybackMode=VOICE</code> and another application is also playing in voice mode, you cannot set <code>useSpeakerphoneForVoice=true</code>. </p>
		 * <p><b>Note</b> On Android, you must set the <code>android.permission.MODIFY_AUDIO_SETTINGS</code> in the AIR application descriptor or changing this value has no effect. In addition, the setting is global device setting. Other applications running on the device can change the underlying device setting at any time.</p>
		 * <p> The default value is <code>false.</code></p>
		 * 
		 * @return 
		 */
		public static function get useSpeakerphoneForVoice():Boolean {
			return _useSpeakerphoneForVoice;
		}


		/**
		 * @param value
		 * @return 
		 */
		public static function set useSpeakerphoneForVoice(value:Boolean):void {
			_useSpeakerphoneForVoice = value;
		}


		/**
		 * <p> Determines whether any sounds are not accessible due to security restrictions. For example, a sound loaded from a domain other than that of the content calling this method is not accessible if the server for the sound has no URL policy file that grants access to the domain of that domain. The sound can still be loaded and played, but low-level operations, such as getting ID3 metadata for the sound, cannot be performed on inaccessible sounds. </p>
		 * <p>For AIR application content in the application security sandbox, calling this method always returns <code>false</code>. All sounds, including those loaded from other domains, are accessible to content in the application security sandbox.</p>
		 * 
		 * @return  — The string representation of the boolean. 
		 */
		public static function areSoundsInaccessible():Boolean {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Takes a snapshot of the current sound wave and places it into the specified ByteArray object. The values are formatted as normalized floating-point values, in the range -1.0 to 1.0. The ByteArray object passed to the <code>outputArray</code> parameter is overwritten with the new values. The size of the ByteArray object created is fixed to 512 floating-point values, where the first 256 values represent the left channel, and the second 256 values represent the right channel. </p>
		 * <p><b>Note:</b> This method is subject to local file security restrictions and restrictions on cross-domain loading. If you are working with local files or sounds loaded from a server in a different domain than the calling content, you might need to address sandbox restrictions through a cross-domain policy file. For more information, see the Sound class description. In addition, this method cannot be used to extract data from RTMP streams, even when it is called by content that reside in the same domain as the RTMP server.</p>
		 * <p>This method is supported over RTMP in Flash Player 9.0.115.0 and later and in Adobe AIR. You can control access to streams on Flash Media Server in a server-side script. For more information, see the <code>Client.audioSampleAccess</code> and <code>Client.videoSampleAccess</code> properties in <a href="http://www.adobe.com/go/documentation" target="external"><i> Server-Side ActionScript Language Reference for Adobe Flash Media Server</i></a>.</p>
		 * 
		 * @param outputArray  — A ByteArray object that holds the values associated with the sound. If any sounds are not available due to security restrictions (<code>areSoundsInaccessible == true</code>), the <code>outputArray</code> object is left unchanged. If all sounds are stopped, the <code>outputArray</code> object is filled with zeros. 
		 * @param FFTMode  — A Boolean value indicating whether a Fourier transformation is performed on the sound data first. Setting this parameter to <code>true</code> causes the method to return a frequency spectrum instead of the raw sound wave. In the frequency spectrum, low frequencies are represented on the left and high frequencies are on the right. 
		 * @param stretchFactor  — The resolution of the sound samples. If you set the <code>stretchFactor</code> value to 0, data is sampled at 44.1 KHz; with a value of 1, data is sampled at 22.05 KHz; with a value of 2, data is sampled 11.025 KHz; and so on. 
		 */
		public static function computeSpectrum(outputArray:ByteArray, FFTMode:Boolean = false, stretchFactor:int = 0):void {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Stops all sounds currently playing. </p>
		 * <p>&gt;In Flash Professional, this method does not stop the playhead. Sounds set to stream will resume playing as the playhead moves over the frames in which they are located.</p>
		 * <p>When using this property, consider the following security model:</p>
		 * <ul>
		 *  <li> By default, calling the <code>SoundMixer.stopAll()</code> method stops only sounds in the same security sandbox as the object that is calling the method. Any sounds whose playback was not started from the same sandbox as the calling object are not stopped.</li>
		 *  <li>When you load the sound, using the <code>load()</code> method of the Sound class, you can specify a <code>context</code> parameter, which is a SoundLoaderContext object. If you set the <code>checkPolicyFile</code> property of the SoundLoaderContext object to <code>true</code>, <span>Flash Player or</span> Adobe AIR checks for a cross-domain policy file on the server from which the sound is loaded. If the server has a cross-domain policy file, and the file permits the domain of the calling content, then the file can stop the loaded sound by using the <code>SoundMixer.stopAll()</code> method; otherwise it cannot.</li>
		 * </ul>
		 * <p>However, in Adobe AIR, content in the <code>application</code> security sandbox (content installed with the AIR application) are not restricted by these security limitations.</p>
		 * <p>For more information related to security, see the Flash Player Developer Center Topic: <a href="http://www.adobe.com/go/devnet_security_en" target="external">Security</a>.</p>
		 * <span id="pageFilter"><br><span class="label"> Example &nbsp;( <span class="usage"><a href="http://www.adobe.com/go/learn_as3_usingexamples_en"> How to use this example </a></span>) </span><br><br>
		 *  <div class="detailBody">
		 *    In the following example, the 
		 *   <code>stopAll()</code> method is used to mute two sounds that are playing at the same time. 
		 *   <p>In the constructor, two different sound files are loaded and set to play. The first sound is loaded locally and is assigned to a sound channel. (It is assumed that the file is in the same directory as the SWF file.) The second file is loaded and streamed from the Adobe site. In order to use the <code>SoundMixer.stopAll()</code> method, all sound must be accessible. (A SoundLoaderContext object can be used to check for the cross-domain policy file.) Each sound also has an event listener that is invoked if an IO error occurred while loading the sound file. A <code>muteButton</code> text field is also created. It listens for a click event, which will invoke the <code>muteButtonClickHandler()</code> method.</p> 
		 *   <p>In the <code>muteButtonClickHandler()</code> method, if the text field content is "MUTE," the <code>areSoundsInaccessible()</code> method checks if the sound mixer has access to the files. If the files are accessible, the <code>stopAll()</code> method stops the sounds. By selecting the text field again, the first sound begins playing and the text field's content changes to "MUTE" again. This time, the <code>stopAll()</code> method mutes the one sound that is running. Note that sound channel <code>stop()</code> method can also be used to stop a specific sound assigned to the channel. (To use the channel functionally, the sound needs to be reassigned to the channel each time the <code>play()</code> method is invoked.)</p> 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>
		 * package {
		 *     import flash.display.Sprite;
		 *     import flash.net.URLRequest;
		 *     import flash.media.Sound;
		 *     import flash.media.SoundLoaderContext;
		 *     import flash.media.SoundChannel;
		 *     import flash.media.SoundMixer;
		 *     import flash.text.TextField;
		 *     import flash.text.TextFieldAutoSize;
		 *     import flash.events.MouseEvent;
		 *     import flash.events.IOErrorEvent;
		 * 
		 *     public class SoundMixer_stopAllExample extends Sprite  {
		 *         private var firstSound:Sound = new Sound();
		 *         private var secondSound:Sound = new Sound();
		 *         private var muteButton:TextField = new TextField();
		 *         private var channel1:SoundChannel = new SoundChannel();
		 *         
		 *         public function SoundMixer_stopAllExample() {
		 *             firstSound.load(new URLRequest("mySound.mp3"));
		 *             secondSound.load(new URLRequest("http://av.adobe.com/podcast/csbu_dev_podcast_epi_2.mp3"));
		 * 
		 *             firstSound.addEventListener(IOErrorEvent.IO_ERROR, firstSoundErrorHandler);
		 *             secondSound.addEventListener(IOErrorEvent.IO_ERROR, secondSoundErrorHandler);
		 *             
		 *             channel1 = firstSound.play();
		 *             secondSound.play();
		 *             
		 *             muteButton.autoSize = TextFieldAutoSize.LEFT;
		 *             muteButton.border = true;
		 *             muteButton.background = true;
		 *             muteButton.text = "MUTE";
		 *         
		 *             muteButton.addEventListener(MouseEvent.CLICK, muteButtonClickHandler);         
		 *         
		 *             this.addChild(muteButton);
		 *         }
		 * 
		 *         private function muteButtonClickHandler(event:MouseEvent):void {
		 * 
		 *             if(muteButton.text == "MUTE") {        
		 *   
		 *                 if(SoundMixer.areSoundsInaccessible() == false) {
		 *                     SoundMixer.stopAll();
		 *                     muteButton.text = "click to play only one of sound.";
		 *                 }
		 *                 else {
		 *                     muteButton.text = "The sounds are not accessible.";
		 *                 }
		 *             }
		 *            else {
		 *                 firstSound.play();        
		 *                 muteButton.text = "MUTE";
		 *            }
		 *         } 
		 * 
		 *         private function firstSoundErrorHandler(errorEvent:IOErrorEvent):void {
		 *             trace(errorEvent.text);
		 *         }
		 * 
		 *         private function secondSoundErrorHandler(errorEvent:IOErrorEvent):void {
		 *             trace(errorEvent.text);
		 *         }
		 *     }
		 * }
		 * </pre>
		 *   </div>
		 *  </div></span>
		 */
		public static function stopAll():void {
			throw new Error("Not implemented");
		}
	}
}
