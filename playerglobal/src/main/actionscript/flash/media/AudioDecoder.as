package flash.media {
	/**
	 *  The AudioDecoder class enumerates the types of multichannel audio that a system can support. <p>Use one of the constants defined in this class as the parameter to the <code>hasMultiChannelAudio()</code> method of the Capabilities class.</p> <p> <i>AIR profile support:</i> Multichannel audio is supported only on AIR for TV devices. On all other devices, <code>hasMultiChannelAudio()</code> always returns <code>false</code>. See <a href="http://help.adobe.com/en_US/air/build/WS144092a96ffef7cc16ddeea2126bb46b82f-8000.html"> AIR Profile Support</a> for more information regarding API support across multiple profiles.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../flash/system/Capabilities.html#hasMultiChannelAudio()" target="">Capabilities.hasMultiChannelAudio()</a>
	 * </div><br><hr>
	 */
	public class AudioDecoder {
		/**
		 * <p> Dolby Digital Audio, which is also known as AC-3. </p>
		 */
		public static const DOLBY_DIGITAL:String = "DolbyDigital";
		/**
		 * <p> Dolby Digital Plus Audio, which is also known as Enhanced AC-3 and Enhanced Dolby Digital. </p>
		 */
		public static const DOLBY_DIGITAL_PLUS:String = "DolbyDigitalPlus";
		/**
		 * <p> DTS Audio, which is also known as DTS Coherent Acoustics, DTS Digital Surround, and DTS core. </p>
		 */
		public static const DTS:String = "DTS";
		/**
		 * <p> DTS Express Audio, which is also known as DTS Low Bit Rate (LBR). </p>
		 */
		public static const DTS_EXPRESS:String = "DTSExpress";
		/**
		 * <p> DTS-HD High Resolution Audio, which is also known as DTS-HD HR. </p>
		 */
		public static const DTS_HD_HIGH_RESOLUTION_AUDIO:String = "DTSHDHighResolutionAudio";
		/**
		 * <p> DTS-HD Master Audio, which is also known as DTS-HD MA. </p>
		 */
		public static const DTS_HD_MASTER_AUDIO:String = "DTSHDMasterAudio";
	}
}
