package flash.media {
	/**
	 *  The VideoStreamSettings class enables specifying video compression settings for each NetStream. All parameters (resolution, frame rate, bandwidth, etc.) are gated by <code>Camera</code> capture parameters. You can use methods (<code>setMode()</code>, etc.) to specify desired encoder parameters and you can use the properties (<code>width</code>, etc.) to retrieve the actual compression parameters used. Properties will be validated once <code>Camera</code> is attached to <code>NetStream</code> object and compression has started. <p> <b>Note</b> Current implementation does not support setting properties per <code>NetStream</code> and <code>Camera</code> parameters will be used instead for each publishing <code>NetStream</code>.</p> <br><hr>
	 */
	public class VideoStreamSettings {
		private var _bandwidth:int;
		private var _codec:String;
		private var _fps:Number;
		private var _height:int;
		private var _keyFrameInterval:int;
		private var _quality:int;
		private var _width:int;

		/**
		 * <p> Creates a setting object that specifies to use Sorenson Spark video codec for compresion. </p>
		 */
		public function VideoStreamSettings() {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Retrieve the maximum amount of bandwidth that the current outgoing video feed can use, in bytes per second. To set this property, use the <code>setQuality()</code> method. </p>
		 * 
		 * @return 
		 */
		public function get bandwidth():int {
			return _bandwidth;
		}

		/**
		 * <p> Video codec used for compression. </p>
		 * 
		 * @return 
		 */
		public function get codec():String {
			return _codec;
		}

		/**
		 * <p> The maximum frame rate at which the video frames are encoded, in frames per second. To set a desired value for this property, use the <code>setMode()</code> method. This value is validated once <code>Camera</code> is attached to <code>NetStream</code>. </p>
		 * 
		 * @return 
		 */
		public function get fps():Number {
			return _fps;
		}

		/**
		 * <p> The current encoded height, in pixels. To set a desired value for this property, use the <code>setMode()</code> method. This value is validated once <code>Camera</code> is attached to <code>NetStream</code> and compression has started. </p>
		 * 
		 * @return 
		 */
		public function get height():int {
			return _height;
		}

		/**
		 * <p> The number of video frames transmitted in full (called keyframes or IDR frames) instead of being interpolated by the video compression algorithm. To set a value for this property, use the <code>setKeyFrameInterval()</code> method. </p>
		 * 
		 * @return 
		 */
		public function get keyFrameInterval():int {
			return _keyFrameInterval;
		}

		/**
		 * <p> The required level of picture quality, as determined by the amount of compression being applied to each video frame. This value ranges from 1 (lowest quality, maximum compression) to 100 (highest quality, small compression). To set this property, use the <code>setQuality()</code> method. </p>
		 * 
		 * @return 
		 */
		public function get quality():int {
			return _quality;
		}

		/**
		 * <p> The current encoded width, in pixels. To set a desired value for this property, use the <code>setMode()</code> method. This value is validated once <code>Camera</code> is attached to <code>NetStream</code> and compression has started. </p>
		 * 
		 * @return 
		 */
		public function get width():int {
			return _width;
		}

		/**
		 * <p> The number of video frames transmitted in full (called keyframes or Instantaneous Decoding Refresh (IDR) frames) instead of being interpolated by the video compression algorithm. The default value is 15, which means that every 15th frame is a keyframe. A value of 1 means that every frame is a keyframe. The allowed values are 1 through 300. Set to -1 to use the same value as specified for <code>Camera</code> object. This value is capped by <code>Camera</code> value. </p>
		 * <p><b>Note</b> This feature will be supported in future releases of Flash Player and AIR, for now, <code>Camera</code> parameters are used.</p>
		 * 
		 * @param keyFrameInterval  — A value that specifies which video frames are transmitted in full (as keyframes or IDR frames) instead of being interpolated by the video compression algorithm. 
		 */
		public function setKeyFrameInterval(keyFrameInterval:int):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets the resolution and frame rate used for video encoding. Set each parameter to -1 to use same encoding value as capture value. Encoding values must be less than equal than capture values specified on <code>Camera</code> object. If invalid values are specified, capture values will be used. Currently, only integer downsampling is supported for both resolution and frame rate. Captured video is downsampled to desired resolution and frame rate. The specified values are validated once <code>Camera</code> is attached to <code>NetStream</code>. You can use <code>width</code>, <code>height</code> and <code>fps</code> properties to retrieve actual compressed width, height and frame rate, respectively. </p>
		 * <p><b>Note</b> This feature will be supported in future releases of Flash Player and AIR, for now, <code>Camera</code> parameters are used.</p>
		 * 
		 * @param width  — The requested encode width, in pixels. The default value is -1 (same as capture width). 
		 * @param height  — The requested encode height, in pixels. The default value is -1 (same as capture height). 
		 * @param fps  — The requested frame rate at which frames should be encoded, in frames per second. The default value is -1 (same as capture fps). 
		 */
		public function setMode(width:int, height:int, fps:Number):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets maximum amount of bandwidth per second or the required picture quality that the current outgoing video feed can use. To specify bandwidth, pass a value for bandwidth and 0 for quality. To specify quality, pass 0 for bandwidth and a value for quality. Both bandwidth and quality values are capped by <code>Camera</code> parameters. </p>
		 * <p><b>Note</b> This feature will be supported in future releases of Flash Player and AIR, for now, <code>Camera</code> parameters are used.</p>
		 * 
		 * @param bandwidth  — Specifies the maximum amount of bandwidth that the current outgoing video feed can use, in bytes per second. The default value is 16384 (128k bits-per-second (bps), which is very low for high quality video). 
		 * @param quality  — An integer that specifies the required level of picture quality, as determined by the amount of compression being applied to each video frame. Acceptable values range from 1 (lowest quality, maximum compression) to 100 (highest quality, small compression). The default value is 0. 
		 */
		public function setQuality(bandwidth:int, quality:int):void {
			throw new Error("Not implemented");
		}
	}
}
