package flash.media {
	/**
	 *  This class defines an enumeration that describes possible levels of video decoding. <br><hr>
	 */
	public class VideoStatus {
		/**
		 * <p> Indicates hardware-accelerated (GPU) video decoding. </p>
		 */
		public static const ACCELERATED:String = "accelerated";
		/**
		 * <p> Indicates software video decoding. </p>
		 */
		public static const SOFTWARE:String = "software";
		/**
		 * <p> Video decoding is not supported. </p>
		 */
		public static const UNAVAILABLE:String = "unavailable";
	}
}
