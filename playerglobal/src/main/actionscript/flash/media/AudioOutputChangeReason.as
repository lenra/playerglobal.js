package flash.media {
	/**
	 *  This class defines an enumeration that indicates the reason for AudioOutputChangeEvent. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../flash/events/AudioOutputChangeEvent.html" target="">flash.events.AudioOutputChangeEvent</a>
	 * </div><br><hr>
	 */
	public class AudioOutputChangeReason {
		/**
		 * <p> Audio Output is changed because system device has been added or removed. </p>
		 */
		public static const DEVICE_CHANGE:String = "deviceChange";
		/**
		 * <p> Audio Output is changed by user selecting a different device. </p>
		 */
		public static const USER_SELECTION:String = "userSelection";
	}
}
