package flash.media {
	/**
	 *  Provides information about a cue point from a period in an HLS stream. <br><hr>
	 */
	public class AVTagData {
		private var _data:String;
		private var _localTime:Number;

		/**
		 * <p> Data in the tag. </p>
		 * 
		 * @return 
		 */
		public function get data():String {
			return _data;
		}

		/**
		 * <p> The timestamp of the tag data </p>
		 * 
		 * @return 
		 */
		public function get localTime():Number {
			return _localTime;
		}
	}
}
