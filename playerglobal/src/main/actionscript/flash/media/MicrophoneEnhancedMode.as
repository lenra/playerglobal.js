package flash.media {
	/**
	 *  The MicrophoneEnhancedMode class is an enumeration of constant values used in setting the <code>mode</code> property of <code>MicrophoneEnhancedOptions</code> class. <p> <b>Note:</b> This feature is not available on iOS. </p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="MicrophoneEnhancedOptions.html" target="">flash.media.MicrophoneEnhancedOptions</a>
	 * </div><br><hr>
	 */
	public class MicrophoneEnhancedMode {
		/**
		 * <p> Use this mode to allow both parties to talk at the same time. Acoustic echo cancellation operates in full-duplex mode. Full-duplex mode is the highest quality echo cancellation. This mode requires high-quality microphones and speakers and the most computing power. Do not use this mode with a USB microphone. </p>
		 */
		public static const FULL_DUPLEX:String = "fullDuplex";
		/**
		 * <p> Use this mode for older and lower-quality speakers and microphones. Acoustic echo cancellation operates in half-duplex mode. In half-duplex mode, only one party can speak at a time. Half-duplex mode requires simpler processing than full-duplex mode. Half-duplex mode is the default mode for USB microphone devices. </p>
		 * <p>If the application uses the default <code>enhancedOptions</code> setting and a USB mic, Flash Player automatically switches to <code>halfDuplex</code> mode. If the application uses the default <code>enhancedOptions</code> setting and the built-in microphone, Flash Player uses <code>fullDuplex</code> mode.</p>
		 */
		public static const HALF_DUPLEX:String = "halfDuplex";
		/**
		 * <p> Use this mode when both parties are using headsets. Acoustic echo cancellation operates in low-echo mode. This mode requires the least amount of computing power. </p>
		 */
		public static const HEADSET:String = "headset";
		/**
		 * <p> All enhanced microphone functionality is off. </p>
		 */
		public static const OFF:String = "off";
		/**
		 * <p> Use this mode when the speaker is muted. In this mode, acoustic echo cancellation is off. </p>
		 */
		public static const SPEAKER_MUTE:String = "speakerMute";
	}
}
