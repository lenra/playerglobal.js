package flash.media {
	/**
	 *  The H264Level class is an enumeration of constant values used in setting the level of <code>H264VideoStreamSettings</code> class. <br><hr>
	 */
	public class H264Level {
		/**
		 * <p> Constant for H.264 level 1. </p>
		 */
		public static const LEVEL_1:String = "1";
		/**
		 * <p> Constant for H.264 level 1b. </p>
		 */
		public static const LEVEL_1B:String = "1b";
		/**
		 * <p> Constant for H.264 level 1.1. </p>
		 */
		public static const LEVEL_1_1:String = "1.1";
		/**
		 * <p> Constant for H.264 level 1.2. </p>
		 */
		public static const LEVEL_1_2:String = "1.2";
		/**
		 * <p> Constant for H.264 level 1.3. </p>
		 */
		public static const LEVEL_1_3:String = "1.3";
		/**
		 * <p> Constant for H.264 level 2. </p>
		 */
		public static const LEVEL_2:String = "2";
		/**
		 * <p> Constant for H.264 level 2.1. </p>
		 */
		public static const LEVEL_2_1:String = "2.1";
		/**
		 * <p> Constant for H.264 level 2.2. </p>
		 */
		public static const LEVEL_2_2:String = "2.2";
		/**
		 * <p> Constant for H.264 level 3. </p>
		 */
		public static const LEVEL_3:String = "3";
		/**
		 * <p> Constant for H.264 level 3.1. </p>
		 */
		public static const LEVEL_3_1:String = "3.1";
		/**
		 * <p> Constant for H.264 level 3.2. </p>
		 */
		public static const LEVEL_3_2:String = "3.2";
		/**
		 * <p> Constant for H.264 level 4. </p>
		 */
		public static const LEVEL_4:String = "4";
		/**
		 * <p> Constant for H.264 level 4.1. </p>
		 */
		public static const LEVEL_4_1:String = "4.1";
		/**
		 * <p> Constant for H.264 level 4.2. </p>
		 */
		public static const LEVEL_4_2:String = "4.2";
		/**
		 * <p> Constant for H.264 level 5. </p>
		 */
		public static const LEVEL_5:String = "5";
		/**
		 * <p> Constant for H.264 level 5.1. </p>
		 */
		public static const LEVEL_5_1:String = "5.1";
	}
}
