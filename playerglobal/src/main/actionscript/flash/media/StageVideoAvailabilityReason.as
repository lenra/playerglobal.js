package flash.media {
	/**
	 *  This class defines an enumeration that indicates whether stage video is currently available. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../flash/events/StageVideoAvailabilityEvent.html" target="">flash.events.StageVideoAvailabilityEvent</a>
	 * </div><br><hr>
	 */
	public class StageVideoAvailabilityReason {
		/**
		 * <p> Stage video is not currently available, the driver is too old or black listed </p>
		 */
		public static const DRIVER_TOO_OLD:String = "driverTooOld";
		/**
		 * <p> Stage video is currently available, no errors occurred </p>
		 */
		public static const NO_ERROR:String = "noError";
		/**
		 * <p> Stage video is not currently available, no hardware was available </p>
		 */
		public static const UNAVAILABLE:String = "unavailable";
		/**
		 * <p> Stage video is not currently available, the user disabled HW acceleration </p>
		 */
		public static const USER_DISABLED:String = "userDisabled";
		/**
		 * <p> Stage video is not currently available, the wmode doesn't support Stage video. </p>
		 */
		public static const WMODE_INCOMPATIBLE:String = "wModeIncompatible";
	}
}
