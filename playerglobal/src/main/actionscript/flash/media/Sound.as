package flash.media {
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.SampleDataEvent;
	import flash.net.URLRequest;
	import flash.utils.ByteArray;

	[Event(name="complete", type="flash.events.Event")]
	[Event(name="id3", type="flash.events.Event")]
	[Event(name="ioError", type="flash.events.IOErrorEvent")]
	[Event(name="open", type="flash.events.Event")]
	[Event(name="progress", type="flash.events.ProgressEvent")]
	[Event(name="sampleData", type="flash.events.SampleDataEvent")]
	/**
	 *  The Sound class lets you work with sound in an application. The Sound class lets you create a Sound object, load and play an external MP3 file into that object, close the sound stream, and access data about the sound, such as information about the number of bytes in the stream and ID3 metadata. More detailed control of the sound is performed through the sound source — the SoundChannel or Microphone object for the sound — and through the properties in the SoundTransform class that control the output of the sound to the computer's speakers. <p>In Flash Player 10 and later and AIR 1.5 and later, you can also use this class to work with sound that is generated dynamically. In this case, the Sound object uses the function you assign to a <code>sampleData</code> event handler to poll for sound data. The sound is played as it is retrieved from a ByteArray object that you populate with sound data. You can use <code>Sound.extract()</code> to extract sound data from a Sound object, after which you can manipulate it before writing it back to the stream for playback.</p> <p>To control sounds that are embedded in a SWF file, use the properties in the SoundMixer class.</p> <p> <b>Note</b>: The ActionScript 3.0 Sound API differs from ActionScript 2.0. In ActionScript 3.0, you cannot take sound objects and arrange them in a hierarchy to control their properties.</p> <p>When you use this class, consider the following security model: </p> <ul> 
	 *  <li>Loading and playing a sound is not allowed if the calling file is in a network sandbox and the sound file to be loaded is local.</li> 
	 *  <li>By default, loading and playing a sound is not allowed if the calling file is local and tries to load and play a remote sound. A user must grant explicit permission to allow this type of access.</li> 
	 *  <li>Certain operations dealing with sound are restricted. The data in a loaded sound cannot be accessed by a file in a different domain unless you implement a cross-domain policy file. Sound-related APIs that fall under this restriction are <code>Sound.id3</code>, <code>SoundMixer.computeSpectrum()</code>, <code>SoundMixer.bufferTime</code>, and the <code>SoundTransform</code> class.</li> 
	 * </ul> <p>However, in Adobe AIR, content in the <code>application</code> security sandbox (content installed with the AIR application) are not restricted by these security limitations.</p> <p>For more information related to security, see the Flash Player Developer Center Topic: <a href="http://www.adobe.com/go/devnet_security_en" target="external">Security</a>.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d25.html" target="_blank">Loading external sound files</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d21.html" target="_blank">Playing sounds</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d1f.html" target="_blank">Controlling sound volume and panning</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d18.html" target="_blank">Working with sound metadata</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d17.html" target="_blank">Accessing raw sound data</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d15.html" target="_blank">Sound example: Podcast Player</a>
	 * </div><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d27.html" target="_blank">Basics of working with sound</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d26.html" target="_blank">Understanding the sound architecture</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d24.html" target="_blank">Working with embedded sounds</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d22.html" target="_blank">Working with streaming sound files</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WSE523B839-C626-4983-B9C0-07CF1A087ED7.html" target="_blank">Working with dynamically generated audio</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d20.html" target="_blank">Security considerations when loading and playing sounds</a>
	 *  <br>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../flash/net/NetStream.html" target="">flash.net.NetStream</a>
	 *  <br>
	 *  <a href="Microphone.html" target="">Microphone</a>
	 *  <br>
	 *  <a href="SoundChannel.html" target="">SoundChannel</a>
	 *  <br>
	 *  <a href="SoundMixer.html" target="">SoundMixer</a>
	 *  <br>
	 *  <a href="SoundTransform.html" target="">SoundTransform</a>
	 * </div><br><hr>
	 */
	public class Sound extends EventDispatcher {
		private var _bytesLoaded:uint;
		private var _bytesTotal:int;
		private var _id3:ID3Info;
		private var _isBuffering:Boolean;
		private var _isURLInaccessible:Boolean;
		private var _length:Number;
		private var _url:String;

		/**
		 * <p> Creates a new Sound object. If you pass a valid URLRequest object to the Sound constructor, the constructor automatically calls the <code>load()</code> function for the Sound object. If you do not pass a valid URLRequest object to the Sound constructor, you must call the <code>load()</code> function for the Sound object yourself, or the stream will not load. </p>
		 * <p>Once <code>load()</code> is called on a Sound object, you can't later load a different sound file into that Sound object. To load a different sound file, create a new Sound object.</p>
		 * <code>load()</code>
		 * <code>sampleData</code>
		 * 
		 * @param stream  — The URL that points to an external MP3 file. 
		 * @param context  — An optional SoundLoader context object, which can define the buffer time (the minimum number of milliseconds of MP3 data to hold in the Sound object's buffer) and can specify whether the application should check for a cross-domain policy file prior to loading the sound. 
		 */
		public function Sound(stream:URLRequest = null, context:SoundLoaderContext = null) {
			super();
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the currently available number of bytes in this sound object. This property is usually useful only for externally loaded files. </p>
		 * 
		 * @return 
		 */
		public function get bytesLoaded():uint {
			return _bytesLoaded;
		}

		/**
		 * <p> Returns the total number of bytes in this sound object. </p>
		 * 
		 * @return 
		 */
		public function get bytesTotal():int {
			return _bytesTotal;
		}

		/**
		 * <p> Provides access to the metadata that is part of an MP3 file. </p>
		 * <p>MP3 sound files can contain ID3 tags, which provide metadata about the file. If an MP3 sound that you load using the <code>Sound.load()</code> method contains ID3 tags, you can query these properties. Only ID3 tags that use the UTF-8 character set are supported.</p>
		 * <p><span>Flash Player 9 and later and AIR support</span> ID3 2.0 tags, specifically 2.3 and 2.4. The following tables list the standard ID3 2.0 tags and the type of content the tags represent. The <code>Sound.id3</code> property provides access to these tags through the format <code>my_sound.id3.COMM</code>, <code>my_sound.id3.TIME</code>, and so on. The first table describes tags that can be accessed either through the ID3 2.0 property name or the ActionScript property name. The second table describes ID3 tags that are supported but do not have predefined properties in ActionScript. </p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <td><b>ID3 2.0 tag</b></td>
		 *    <td><b>Corresponding Sound class property</b></td>
		 *   </tr>
		 *   <tr>
		 *    <td>COMM</td>
		 *    <td>Sound.id3.comment</td>
		 *   </tr>
		 *   <tr>
		 *    <td>TALB</td>
		 *    <td>Sound.id3.album </td>
		 *   </tr>
		 *   <tr>
		 *    <td>TCON</td>
		 *    <td>Sound.id3.genre</td>
		 *   </tr>
		 *   <tr>
		 *    <td>TIT2</td>
		 *    <td>Sound.id3.songName </td>
		 *   </tr>
		 *   <tr>
		 *    <td>TPE1</td>
		 *    <td>Sound.id3.artist</td>
		 *   </tr>
		 *   <tr>
		 *    <td>TRCK</td>
		 *    <td>Sound.id3.track </td>
		 *   </tr>
		 *   <tr>
		 *    <td>TYER</td>
		 *    <td>Sound.id3.year </td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p>The following table describes ID3 tags that are supported but do not have predefined properties in the Sound class. You access them by calling <code>mySound.id3.TFLT</code>, <code>mySound.id3.TIME</code>, and so on. <b>NOTE:</b> None of these tags are supported in Flash Lite 4.</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <td><b>Property</b></td>
		 *    <td><b>Description</b></td>
		 *   </tr>
		 *   <tr>
		 *    <td>TFLT</td>
		 *    <td>File type</td>
		 *   </tr>
		 *   <tr>
		 *    <td>TIME</td>
		 *    <td>Time</td>
		 *   </tr>
		 *   <tr>
		 *    <td>TIT1</td>
		 *    <td>Content group description</td>
		 *   </tr>
		 *   <tr>
		 *    <td>TIT2</td>
		 *    <td>Title/song name/content description</td>
		 *   </tr>
		 *   <tr>
		 *    <td>TIT3</td>
		 *    <td>Subtitle/description refinement</td>
		 *   </tr>
		 *   <tr>
		 *    <td>TKEY</td>
		 *    <td>Initial key</td>
		 *   </tr>
		 *   <tr>
		 *    <td>TLAN</td>
		 *    <td>Languages</td>
		 *   </tr>
		 *   <tr>
		 *    <td>TLEN</td>
		 *    <td>Length</td>
		 *   </tr>
		 *   <tr>
		 *    <td>TMED</td>
		 *    <td>Media type</td>
		 *   </tr>
		 *   <tr>
		 *    <td>TOAL</td>
		 *    <td>Original album/movie/show title</td>
		 *   </tr>
		 *   <tr>
		 *    <td>TOFN</td>
		 *    <td>Original filename</td>
		 *   </tr>
		 *   <tr>
		 *    <td>TOLY</td>
		 *    <td>Original lyricists/text writers</td>
		 *   </tr>
		 *   <tr>
		 *    <td>TOPE</td>
		 *    <td>Original artists/performers</td>
		 *   </tr>
		 *   <tr>
		 *    <td>TORY</td>
		 *    <td>Original release year</td>
		 *   </tr>
		 *   <tr>
		 *    <td>TOWN</td>
		 *    <td>File owner/licensee</td>
		 *   </tr>
		 *   <tr>
		 *    <td>TPE1</td>
		 *    <td>Lead performers/soloists</td>
		 *   </tr>
		 *   <tr>
		 *    <td>TPE2</td>
		 *    <td>Band/orchestra/accompaniment</td>
		 *   </tr>
		 *   <tr>
		 *    <td>TPE3</td>
		 *    <td>Conductor/performer refinement</td>
		 *   </tr>
		 *   <tr>
		 *    <td>TPE4</td>
		 *    <td>Interpreted, remixed, or otherwise modified by</td>
		 *   </tr>
		 *   <tr>
		 *    <td>TPOS</td>
		 *    <td>Part of a set</td>
		 *   </tr>
		 *   <tr>
		 *    <td>TPUB</td>
		 *    <td>Publisher</td>
		 *   </tr>
		 *   <tr>
		 *    <td>TRCK</td>
		 *    <td>Track number/position in set</td>
		 *   </tr>
		 *   <tr>
		 *    <td>TRDA</td>
		 *    <td>Recording dates</td>
		 *   </tr>
		 *   <tr>
		 *    <td>TRSN</td>
		 *    <td>Internet radio station name</td>
		 *   </tr>
		 *   <tr>
		 *    <td>TRSO</td>
		 *    <td>Internet radio station owner</td>
		 *   </tr>
		 *   <tr>
		 *    <td>TSIZ</td>
		 *    <td>Size</td>
		 *   </tr>
		 *   <tr>
		 *    <td>TSRC</td>
		 *    <td>ISRC (international standard recording code)</td>
		 *   </tr>
		 *   <tr>
		 *    <td>TSSE</td>
		 *    <td>Software/hardware and settings used for encoding</td>
		 *   </tr>
		 *   <tr>
		 *    <td>TYER</td>
		 *    <td>Year</td>
		 *   </tr>
		 *   <tr>
		 *    <td>WXXX</td>
		 *    <td>URL link frame</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p>When using this property, consider the Flash Player security model:</p>
		 * <ul>
		 *  <li>The <code>id3</code> property of a Sound object is always permitted for SWF files that are in the same security sandbox as the sound file. For files in other sandboxes, there are security checks.</li>
		 *  <li>When you load the sound, using the <code>load()</code> method of the Sound class, you can specify a <code>context</code> parameter, which is a SoundLoaderContext object. If you set the <code>checkPolicyFile</code> property of the SoundLoaderContext object to <code>true</code>, Flash Player checks for a URL policy file on the server from which the sound is loaded. If a policy file exists and permits access from the domain of the loading SWF file, then the file is allowed to access the <code>id3</code> property of the Sound object; otherwise it is not.</li>
		 * </ul>
		 * <p>However, in Adobe AIR, content in the <code>application</code> security sandbox (content installed with the AIR application) are not restricted by these security limitations.</p>
		 * <p>For more information related to security, see the Flash Player Developer Center Topic: <a href="http://www.adobe.com/go/devnet_security_en" target="external">Security</a>.</p>
		 * 
		 * @return 
		 */
		public function get id3():ID3Info {
			return _id3;
		}

		/**
		 * <p> Returns the buffering state of external MP3 files. If the value is <code>true</code>, any playback is currently suspended while the object waits for more data. </p>
		 * 
		 * @return 
		 */
		public function get isBuffering():Boolean {
			return _isBuffering;
		}

		/**
		 * <p> Indicates if the <code>Sound.url</code> property has been truncated. When the <code>isURLInaccessible</code> value is <code>true</code> the <code>Sound.url</code> value is only the domain of the final URL from which the sound loaded. For example, the property is truncated if the sound is loaded from <code>http://www.adobe.com/assets/hello.mp3</code>, and the <code>Sound.url</code> property has the value <code>http://www.adobe.com</code>. The <code>isURLInaccessible</code> value is <code>true</code> only when all of the following are also true: </p>
		 * <ul>
		 *  <li>An HTTP redirect occurred while loading the sound file.</li>
		 *  <li>The SWF file calling <code>Sound.load()</code> is from a different domain than the sound file's final URL.</li>
		 *  <li>The SWF file calling <code>Sound.load()</code> does not have permission to access the sound file. Permission is granted to access the sound file the same way permission is granted for the <code>Sound.id3</code> property: establish a policy file and use the <code>SoundLoaderContext.checkPolicyFile</code> property.</li>
		 * </ul>
		 * <p><b>Note:</b> The <code>isURLInaccessible</code> property was added for Flash Player 10.1 and AIR 2.0. However, this property is made available to SWF files of all versions when the Flash runtime supports it. So, using some authoring tools in "strict mode" causes a compilation error. To work around the error use the indirect syntax <code>mySound["isURLInaccessible"]</code>, or disable strict mode. If you are using Flash Professional CS5 or Flex SDK 4.1, you can use and compile this API for runtimes released before Flash Player 10.1 and AIR 2.</p>
		 * <p>For application content in AIR, the value of this property is always <code>false</code>.</p>
		 * 
		 * @return 
		 */
		public function get isURLInaccessible():Boolean {
			return _isURLInaccessible;
		}

		/**
		 * <p> The length of the current sound in milliseconds. </p>
		 * 
		 * @return 
		 */
		public function get length():Number {
			return _length;
		}

		/**
		 * <p> The URL from which this sound was loaded. This property is applicable only to Sound objects that were loaded using the <code>Sound.load()</code> method. For Sound objects that are associated with a sound asset from a SWF file's library, the value of the <code>url</code> property is <code>null</code>. </p>
		 * <p>When you first call <code>Sound.load()</code>, the <code>url</code> property initially has a value of <code>null</code>, because the final URL is not yet known. The <code>url</code> property will have a non-null value as soon as an <code>open</code> event is dispatched from the Sound object.</p>
		 * <p>The <code>url</code> property contains the final, absolute URL from which a sound was loaded. The value of <code>url</code> is usually the same as the value passed to the <code>stream</code> parameter of <code>Sound.load()</code>. However, if you passed a relative URL to <code>Sound.load()</code> the value of the <code>url</code> property represents the absolute URL. Additionally, if the original URL request is redirected by an HTTP server, the value of the <code>url</code> property reflects the final URL from which the sound file was actually downloaded. This reporting of an absolute, final URL is equivalent to the behavior of <code>LoaderInfo.url</code>.</p>
		 * <p>In some cases, the value of the <code>url</code> property is truncated; see the <code>isURLInaccessible</code> property for details.</p>
		 * 
		 * @return 
		 */
		public function get url():String {
			return _url;
		}

		/**
		 * <p> Closes the stream, causing any download of data to cease. No data may be read from the stream after the <code>close()</code> method is called. </p>
		 */
		public function close():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Extracts raw sound data from a Sound object. </p>
		 * <p>This method is designed to be used when you are working with dynamically generated audio, using a function you assign to the <code>sampleData</code> event for a different Sound object. That is, you can use this method to extract sound data from a Sound object. Then you can write the data to the byte array that another Sound object is using to stream dynamic audio.</p>
		 * <p>The audio data is placed in the target byte array starting from the current position of the byte array. The audio data is always exposed as 44100 Hz Stereo. The sample type is a 32-bit floating-point value, which can be converted to a Number using <code>ByteArray.readFloat()</code>. </p>
		 * 
		 * @param target  — A ByteArray object in which the extracted sound samples are placed. 
		 * @param length  — The number of sound samples to extract. A sample contains both the left and right channels — that is, two 32-bit floating-point values. 
		 * @param startPosition  — The sample at which extraction begins. If you don't specify a value, the first call to <code>Sound.extract()</code> starts at the beginning of the sound; subsequent calls without a value for <code>startPosition</code> progress sequentially through the file. 
		 * @return  — The number of samples written to the ByteArray specified in the  parameter. 
		 */
		public function extract(target:ByteArray, length:Number, startPosition:Number = -1):Number {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Initiates loading of an external MP3 file from the specified URL. If you provide a valid URLRequest object to the Sound constructor, the constructor calls <code>Sound.load()</code> for you. You only need to call <code>Sound.load()</code> yourself if you don't pass a valid URLRequest object to the Sound constructor or you pass a <code>null</code> value. </p>
		 * <p>Once <code>load()</code> is called on a Sound object, you can't later load a different sound file into that Sound object. To load a different sound file, create a new Sound object.</p>
		 * <p>When using this method, consider the following security model:</p>
		 * <ul>
		 *  <li>Calling <code>Sound.load()</code> is not allowed if the calling file is in the local-with-file-system sandbox and the sound is in a network sandbox.</li>
		 *  <li>Access from the local-trusted or local-with-networking sandbox requires permission from a website through a URL policy file.</li>
		 *  <li>You cannot connect to commonly reserved ports. For a complete list of blocked ports, see "Restricting Networking APIs" in the <i>ActionScript 3.0 Developer's Guide</i>.</li>
		 *  <li>You can prevent a SWF file from using this method by setting the <code>allowNetworking</code> parameter of the <code>object</code> and <code>embed</code> tags in the HTML page that contains the SWF content.</li>
		 * </ul>
		 * <p> In Flash Player 10 and later, if you use a multipart Content-Type (for example "multipart/form-data") that contains an upload (indicated by a "filename" parameter in a "content-disposition" header within the POST body), the POST operation is subject to the security rules applied to uploads:</p>
		 * <ul>
		 *  <li>The POST operation must be performed in response to a user-initiated action, such as a mouse click or key press.</li>
		 *  <li>If the POST operation is cross-domain (the POST target is not on the same server as the SWF file that is sending the POST request), the target server must provide a URL policy file that permits cross-domain access.</li>
		 * </ul>
		 * <p>Also, for any multipart Content-Type, the syntax must be valid (according to the RFC2046 standards). If the syntax appears to be invalid, the POST operation is subject to the security rules applied to uploads.</p>
		 * <p>In Adobe AIR, content in the <code>application</code> security sandbox (content installed with the AIR application) are not restricted by these security limitations.</p>
		 * <p>For more information related to security, see the Flash Player Developer Center Topic: <a href="http://www.adobe.com/go/devnet_security_en" target="external">Security</a>.</p>
		 * 
		 * @param stream  — A URL that points to an external MP3 file. 
		 * @param context  — An optional SoundLoader context object, which can define the buffer time (the minimum number of milliseconds of MP3 data to hold in the Sound object's buffer) and can specify whether the application should check for a cross-domain policy file prior to loading the sound. 
		 */
		public function load(stream:URLRequest, context:SoundLoaderContext = null):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> load MP3 sound data from a ByteArray object into a Sound object. The data will be read from the current ByteArray position and will leave the ByteArray position at the end of the specified bytes length once finished. If the MP3 sound data contains ID3 data ID3 events will be dispatched during this function call. This function will throw an exception if the ByteArray object does not contain enough data. </p>
		 * 
		 * @param bytes
		 * @param bytesLength
		 */
		public function loadCompressedDataFromByteArray(bytes:ByteArray, bytesLength:uint):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Load PCM 32-bit floating point sound data from a ByteArray object into a Sound object. The data will be read from the current ByteArray position and will leave the ByteArray position at the end of the specified sample length multiplied by either 1 channel or 2 channels if the stereo flag is set once finished. </p>
		 * <p>Starting with Flash Player 11.8, the amount of audio data that can be passed to this function is limited. For SWF versions &gt;= 21, this function throws an exception if the amount of audio data passed into this function is more than 1800 seconds. That is, samples / sampleRate should be less than or equal to 1800. For swf versions &lt; 21, the runtime fails silently if the amount of audio data passed in is more than 12000 seconds. This is provided only for backward compatibility.</p>
		 * <p>This function throws an exception if the ByteArray object does not contain enough data.</p>
		 * 
		 * @param bytes
		 * @param samples
		 * @param format
		 * @param stereo
		 * @param sampleRate
		 */
		public function loadPCMFromByteArray(bytes:ByteArray, samples:uint, format:String = "float", stereo:Boolean = true, sampleRate:Number = 44100.0):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Generates a new SoundChannel object to play back the sound. This method returns a SoundChannel object, which you access to stop the sound and to monitor volume. (To control the volume, panning, and balance, access the SoundTransform object assigned to the sound channel.) </p>
		 * 
		 * @param startTime  — The initial position in milliseconds at which playback should start. 
		 * @param loops  — Defines the number of times a sound loops back to the <code>startTime</code> value before the sound channel stops playback. 
		 * @param sndTransform  — The initial SoundTransform object assigned to the sound channel. 
		 * @return  — A SoundChannel object, which you use to control the sound. This method returns  if you have no sound card or if you run out of available sound channels. The maximum number of sound channels available at once is 32. 
		 */
		public function play(startTime:Number = 0, loops:int = 0, sndTransform:SoundTransform = null):SoundChannel {
			throw new Error("Not implemented");
		}
	}
}
