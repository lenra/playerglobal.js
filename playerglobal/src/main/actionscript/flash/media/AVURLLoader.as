package flash.media {
	import flash.events.Event;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;

	[Event(name="complete", type="flash.events.Event")]
	[Event(name="httpResponseStatus", type="flash.events.HTTPStatusEvent")]
	[Event(name="httpStatus", type="flash.events.HTTPStatusEvent")]
	[Event(name="ioError", type="flash.events.IOErrorEvent")]
	[Event(name="open", type="flash.events.Event")]
	[Event(name="progress", type="flash.events.ProgressEvent")]
	[Event(name="securityError", type="flash.events.SecurityErrorEvent")]
	/**
	 *  The URLLoader class downloads data from a URL as text, binary data, or URL-encoded variables. It is useful for downloading text files, XML, or other information to be used in a dynamic, data-driven application. <p>A URLLoader object downloads all of the data from a URL before making it available to code in the applications. It sends out notifications about the progress of the download, which you can monitor through the <code>bytesLoaded</code> and <code>bytesTotal</code> properties, as well as through dispatched events.</p> <p>When loading very large video files, such as FLV's, out-of-memory errors may occur. </p> <p>When you use this class <span>in Flash Player and</span> in AIR application content in security sandboxes other than then application security sandbox, consider the following security model:</p> <ul> 
	 *  <li>A SWF file in the local-with-filesystem sandbox may not load data from, or provide data to, a resource that is in the network sandbox. </li> 
	 *  <li> By default, the calling SWF file and the URL you load must be in exactly the same domain. For example, a SWF file at www.adobe.com can load data only from sources that are also at www.adobe.com. To load data from a different domain, place a URL policy file on the server hosting the data.</li> 
	 * </ul> <p>For more information related to security, see the Flash Player Developer Center Topic: <a href="http://www.adobe.com/go/devnet_security_en" target="external">Security</a>.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  URLRequest
	 *  <br>URLVariables
	 *  <br>
	 *  <a href="AVURLStream.html" target="">AVURLStream</a>
	 * </div><br><hr>
	 */
	public class AVURLLoader extends URLLoader {
		private var _cookieHeader:String;

		/**
		 * <p> Creates a URLLoader object. </p>
		 * 
		 * @param request  — A URLRequest object specifying the URL to download. If this parameter is omitted, no load operation begins. If specified, the load operation begins immediately (see the <code>load</code> entry for more information). 
		 */
		public function AVURLLoader(request:URLRequest = null) {
			super(request);
			throw new Error("Not implemented");
		}

		/**
		 * @return 
		 */
		public function get cookieHeader():String {
			return _cookieHeader;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set cookieHeader(value:String):void {
			_cookieHeader = value;
		}

		/**
		 * @param type
		 * @param listener
		 * @param useCapture
		 * @param priority
		 * @param useWeakReference
		 */
		override public function addEventListener(type:String, listener:Function, useCapture:Boolean = false, priority:int = 0, useWeakReference:Boolean = false):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Closes the load operation in progress. Any load operation in progress is immediately terminated. If no URL is currently being streamed, an invalid stream error is thrown. </p>
		 */
		override public function close():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sends and loads data from the specified URL. The data can be received as text, raw binary data, or URL-encoded variables, depending on the value you set for the <code>dataFormat</code> property. Note that the default value of the <code>dataFormat</code> property is text. If you want to send data to the specified URL, you can set the <code>data</code> property in the URLRequest object. </p>
		 * <p><b>Note:</b> If a file being loaded contains non-ASCII characters (as found in many non-English languages), it is recommended that you save the file with UTF-8 or UTF-16 encoding as opposed to a non-Unicode format like ASCII.</p>
		 * <p> A SWF file in the local-with-filesystem sandbox may not load data from, or provide data to, a resource that is in the network sandbox.</p>
		 * <p> By default, the calling SWF file and the URL you load must be in exactly the same domain. For example, a SWF file at www.adobe.com can load data only from sources that are also at www.adobe.com. To load data from a different domain, place a URL policy file on the server hosting the data.</p>
		 * <p>You cannot connect to commonly reserved ports. For a complete list of blocked ports, see "Restricting Networking APIs" in the <i>ActionScript 3.0 Developer's Guide</i>.</p>
		 * <p> In Flash Player 10 and later, if you use a multipart Content-Type (for example "multipart/form-data") that contains an upload (indicated by a "filename" parameter in a "content-disposition" header within the POST body), the POST operation is subject to the security rules applied to uploads:</p>
		 * <ul>
		 *  <li>The POST operation must be performed in response to a user-initiated action, such as a mouse click or key press.</li>
		 *  <li>If the POST operation is cross-domain (the POST target is not on the same server as the SWF file that is sending the POST request), the target server must provide a URL policy file that permits cross-domain access.</li>
		 * </ul>
		 * <p>Also, for any multipart Content-Type, the syntax must be valid (according to the RFC2046 standards). If the syntax appears to be invalid, the POST operation is subject to the security rules applied to uploads.</p>
		 * <p>For more information related to security, see the Flash Player Developer Center Topic: <a href="http://www.adobe.com/go/devnet_security_en" target="external">Security</a>.</p>
		 * 
		 * @param request  — A URLRequest object specifying the URL to download. 
		 */
		override public function load(request:URLRequest):void {
			throw new Error("Not implemented");
		}
	}
}
