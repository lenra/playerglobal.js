package flash.media {
	import flash.net.URLRequest;
	import flash.net.URLStream;

	/**
	 *  The URLStream class provides low-level access to downloading URLs. Data is made available to application code immediately as it is downloaded, instead of waiting until the entire file is complete as with URLLoader. The URLStream class also lets you close a stream before it finishes downloading. The contents of the downloaded file are made available as raw binary data. <p>The read operations in URLStream are nonblocking. This means that you must use the <code>bytesAvailable</code> property to determine whether sufficient data is available before reading it. An <code>EOFError</code> exception is thrown if insufficient data is available.</p> <p>All binary data is encoded by default in big-endian format, with the most significant byte first.</p> <p>The security rules that apply to URL downloading with the URLStream class are identical to the rules applied to URLLoader objects. Policy files may be downloaded as needed. Local file security rules are enforced, and security warnings are raised as needed.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  URLLoader
	 *  <br>URLRequest
	 * </div><br><hr>
	 */
	public class AVURLStream extends URLStream {
		private var _cookieHeader:String;

		/**
		 * @return 
		 */
		public function get cookieHeader():String {
			return _cookieHeader;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set cookieHeader(value:String):void {
			_cookieHeader = value;
		}

		/**
		 * <p> Begins downloading the URL specified in the <code>request</code> parameter. </p>
		 * <p><b>Note</b>: If a file being loaded contains non-ASCII characters (as found in many non-English languages), it is recommended that you save the file with UTF-8 or UTF-16 encoding, as opposed to a non-Unicode format like ASCII.</p>
		 * <p>If the loading operation fails immediately, an IOError or SecurityError (including the local file security error) exception is thrown describing the failure. Otherwise, an <code>open</code> event is dispatched if the URL download starts downloading successfully, or an error event is dispatched if an error occurs.</p>
		 * <p>By default, the calling SWF file and the URL you load must be in exactly the same domain. For example, a SWF file at www.adobe.com can load data only from sources that are also at www.adobe.com. To load data from a different domain, place a URL policy file on the server hosting the data.</p>
		 * <p>In Flash Player, you cannot connect to commonly reserved ports. For a complete list of blocked ports, see "Restricting Networking APIs" in the <i>ActionScript 3.0 Developer's Guide</i>.</p>
		 * <p>In Flash Player, you can prevent a SWF file from using this method by setting the <code>allowNetworking</code> parameter of the the <code>object</code> and <code>embed</code> tags in the HTML page that contains the SWF content.</p>
		 * <p> In Flash Player 10 and later, and in AIR 1.5 and later, if you use a multipart Content-Type (for example "multipart/form-data") that contains an upload (indicated by a "filename" parameter in a "content-disposition" header within the POST body), the POST operation is subject to the security rules applied to uploads:</p>
		 * <ul>
		 *  <li>The POST operation must be performed in response to a user-initiated action, such as a mouse click or key press.</li>
		 *  <li>If the POST operation is cross-domain (the POST target is not on the same server as the SWF file that is sending the POST request), the target server must provide a URL policy file that permits cross-domain access.</li>
		 * </ul>
		 * <p>Also, for any multipart Content-Type, the syntax must be valid (according to the RFC2046 standards). If the syntax appears to be invalid, the POST operation is subject to the security rules applied to uploads.</p>
		 * <p>These rules also apply to AIR content in non-application sandboxes. However, in Adobe AIR, content in the application sandbox (content installed with the AIR application) are not restricted by these security limitations.</p>
		 * <p>For more information related to security, see The Flash Player Developer Center Topic: <a href="http://www.adobe.com/go/devnet_security_en" target="external">Security</a>.</p>
		 * <p>In AIR, a URLRequest object can register for the <code>httpResponse</code> status event. Unlike the <code>httpStatus</code> event, the <code>httpResponseStatus</code> event is delivered before any response data. Also, the <code>httpResponseStatus</code> event includes values for the <code>responseHeaders</code> and <code>responseURL</code> properties (which are undefined for an <code>httpStatus</code> event. Note that the <code>httpResponseStatus</code> event (if any) will be sent before (and in addition to) any <code>complete</code> or <code>error</code> event. </p>
		 * <p>If there <i>is</i> an <code>httpResponseStatus</code> event listener, the body of the response message is <i>always</i> sent; and HTTP status code responses always results in a <code>complete</code> event. This is true in spite of whether the HTTP response status code indicates a success or an error.</p>
		 * <p><span>In AIR, if</span> there is <i>no</i> <code>httpResponseStatus</code> event listener, the behavior differs based on the <span>SWF</span> version:</p>
		 * <ul>
		 *  <li><span>For SWF 9 content</span>, the body of the HTTP response message is sent <i>only if</i> the HTTP response status code indicates success. Otherwise (if there is an error), no body is sent and the URLRequest object dispatches an IOError event.</li>
		 *  <li><span>For SWF 10 content</span>, the body of the HTTP response message is <i>always</i> sent. If there is an error, the URLRequest object dispatches an IOError event.</li>
		 * </ul>
		 * 
		 * @param request  — A URLRequest object specifying the URL to download. If the value of this parameter or the <code>URLRequest.url</code> property of the URLRequest object passed are <code>null</code>, the application throws a null pointer error. 
		 */
		override public function load(request:URLRequest):void {
			throw new Error("Not implemented");
		}
	}
}
