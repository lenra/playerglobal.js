package flash.media {
	/**
	 *  AVStream dispatch AVPlayStateEvent during playback to indicate changes in state. <br><hr>
	 */
	public class AVNetworkingParams {
		private var _appendRandomQueryParameter:String;
		private var _forceNativeNetworking:Boolean;
		private var _networkDownVerificationUrl:String;
		private var _readSetCookieHeader:Boolean;
		private var _useCookieHeaderForAllRequests:Boolean;

		/**
		 * @param init_forceNativeNetworking  — whether to always use native networking when possible 
		 * @param init_readSetCookieHeader  — whether to read the set-cookie header in responses. 
		 * @param init_useCookieHeaderForAllRequests  — whether to use the cookieHeader for all requests or only for Key-server requests 
		 * @param init_networkDownVerificationUrl
		 */
		public function AVNetworkingParams(init_forceNativeNetworking:Boolean = false, init_readSetCookieHeader:Boolean = true, init_useCookieHeaderForAllRequests:Boolean = false, init_networkDownVerificationUrl:String = "") {
			throw new Error("Not implemented");
		}

		/**
		 * @return 
		 */
		public function get appendRandomQueryParameter():String {
			return _appendRandomQueryParameter;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set appendRandomQueryParameter(value:String):void {
			_appendRandomQueryParameter = value;
		}

		/**
		 * @return 
		 */
		public function get forceNativeNetworking():Boolean {
			return _forceNativeNetworking;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set forceNativeNetworking(value:Boolean):void {
			_forceNativeNetworking = value;
		}

		/**
		 * @return 
		 */
		public function get networkDownVerificationUrl():String {
			return _networkDownVerificationUrl;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set networkDownVerificationUrl(value:String):void {
			_networkDownVerificationUrl = value;
		}

		/**
		 * @return 
		 */
		public function get readSetCookieHeader():Boolean {
			return _readSetCookieHeader;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set readSetCookieHeader(value:Boolean):void {
			_readSetCookieHeader = value;
		}

		/**
		 * @return 
		 */
		public function get useCookieHeaderForAllRequests():Boolean {
			return _useCookieHeaderForAllRequests;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set useCookieHeaderForAllRequests(value:Boolean):void {
			_useCookieHeaderForAllRequests = value;
		}
	}
}
