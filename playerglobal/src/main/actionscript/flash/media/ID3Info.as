package flash.media {
	/**
	 *  The ID3Info class contains properties that reflect ID3 metadata. You can get additional metadata for MP3 files by accessing the <code>id3</code> property of the Sound class; for example, <code>mySound.id3.TIME</code>. For more information, see the entry for <code>Sound.id3</code> and the ID3 tag definitions at <a href="http://www.id3.org">http://www.id3.org</a>. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Sound.html#id3" target="">Sound.id3</a>
	 * </div><br><hr>
	 */
	public class ID3Info {
		private var _album:String;
		private var _artist:String;
		private var _comment:String;
		private var _genre:String;
		private var _songName:String;
		private var _track:String;
		private var _year:String;

		/**
		 * <p> The name of the album; corresponds to the ID3 2.0 tag TALB. </p>
		 * 
		 * @return 
		 */
		public function get album():String {
			return _album;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set album(value:String):void {
			_album = value;
		}

		/**
		 * <p> The name of the artist; corresponds to the ID3 2.0 tag TPE1. </p>
		 * 
		 * @return 
		 */
		public function get artist():String {
			return _artist;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set artist(value:String):void {
			_artist = value;
		}

		/**
		 * <p> A comment about the recording; corresponds to the ID3 2.0 tag COMM. </p>
		 * 
		 * @return 
		 */
		public function get comment():String {
			return _comment;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set comment(value:String):void {
			_comment = value;
		}

		/**
		 * <p> The genre of the song; corresponds to the ID3 2.0 tag TCON. </p>
		 * 
		 * @return 
		 */
		public function get genre():String {
			return _genre;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set genre(value:String):void {
			_genre = value;
		}

		/**
		 * <p> The name of the song; corresponds to the ID3 2.0 tag TIT2. </p>
		 * 
		 * @return 
		 */
		public function get songName():String {
			return _songName;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set songName(value:String):void {
			_songName = value;
		}

		/**
		 * <p> The track number; corresponds to the ID3 2.0 tag TRCK. </p>
		 * 
		 * @return 
		 */
		public function get track():String {
			return _track;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set track(value:String):void {
			_track = value;
		}

		/**
		 * <p> The year of the recording; corresponds to the ID3 2.0 tag TYER. </p>
		 * 
		 * @return 
		 */
		public function get year():String {
			return _year;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set year(value:String):void {
			_year = value;
		}
	}
}
