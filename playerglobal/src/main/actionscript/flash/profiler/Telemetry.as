package flash.profiler {
	/**
	 *  The Telemetry class lets an application profile ActionScript code and register handlers for commands <br><hr>
	 */
	public class Telemetry {
		private static var _connected:Boolean;
		private static var _spanMarker:Number;


		/**
		 * <p> Indicates whether Telemetry is connected to a server </p>
		 * 
		 * @return 
		 */
		public static function get connected():Boolean {
			return _connected;
		}


		/**
		 * <p> Returns a marker for use with <code>Telemetry.sendSpanMetric</code> </p>
		 * 
		 * @return 
		 */
		public static function get spanMarker():Number {
			return _spanMarker;
		}


		/**
		 * <p> Register a function that can be called by issuing a command over a socket </p>
		 * <p>Returns true if the registration is successful. If registration fails, there is already a handler registered for the command used (or the command name starts with '.', which is reserved for player internal use) Already registered handlers may be unregistered using unregisterCommandHandler before registering another handler.</p>
		 * <p>The handler function's return value is sent as the result of the command (<code>tlm-response.result</code>). The handler function can throw Error, if it wants to send an error response. In this case, <code>Error.message</code> and <code>Error.id</code> are sent as <code>tlm-response.tlm-error.message</code> and <code>tlm-response.tlm-error.code</code>, respectively. (<code>tlm-response.result</code> and <code>tlm-response.tlm-error.data</code> are sent as null)</p>
		 * 
		 * @param commandName  — String specifying a unique name (The command over the socket should specify this string as the method name). The guideline is to follow reverse DNS notation, which helps to avoid namespace collisions. Additionally, and names starting with . are reserved for native use. 
		 * @param handler  — Function to be called when a command is received by Telemetry over the socket with the method name, as specified in functionId argument. The handler should accept only one argument of type Array (as defined by <code>tlm-method.params</code> in Telemetry Protocol), which has to be sent by Telemetry server along with method name. 
		 * @return 
		 */
		public static function registerCommandHandler(commandName:String, handler:Function):Boolean {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Requests a custom metric from Telemetry. The metric name and object are sent as per the Telemetry protocol format. </p>
		 * <p>The guideline for custom metric namespaces is to follow reverse DNS notation, which helps to avoid namespace collisions.</p>
		 * 
		 * @param metric  — Metric name 
		 * @param value  — Any primitive value/object containing the metric details 
		 */
		public static function sendMetric(metric:String, value:*):void {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Requests a custom span metric from Telemetry </p>
		 * <p>Use <code>Telemetry.spanMarker</code> to get a marker at the start of function to be profiled and call <code>Telemetry.sendSpanMetric</code> at the end of function with the marker. Telemetry sends the name, starting marker, and duration of the function plus the optional value as per the Telemetry protocol.</p>
		 * <p>The guideline for custom metric namespaces is to follow reverse DNS notation, which helps to avoid namespace collisions.</p>
		 * <p>Span metrics for durations less than a specified threshold, which could be controlled from the Telemetry Server using Telemetry Protocol, would be ignored by Telemetry (will not be sent to Telemetry Server).</p>
		 * 
		 * @param metric  — Metric name 
		 * @param startSpanMarker  — Start marker. 
		 * @param value  — Optional parameter. Any primitive value/object to be sent along with name, marker and duration 
		 */
		public static function sendSpanMetric(metric:String, startSpanMarker:Number, value:* = null):void {
			throw new Error("Not implemented");
		}


		/**
		 * @param commandName
		 * @return 
		 */
		public static function unregisterCommandHandler(commandName:String):Boolean {
			throw new Error("Not implemented");
		}
	}
}
