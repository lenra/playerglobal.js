package flash.globalization {
	/**
	 *  The CollatorMode class enumerates constant values that govern the behavior of string comparisons performed by a Collator object. These constants represent the values that can be passed in the <code>initialMode</code> parameter of the <code>Collator()</code> constructor. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Collator.html" target="">Collator</a>
	 * </div><br><hr>
	 */
	public class CollatorMode {
		/**
		 * <p> Initializes a Collator object so that the compare method is optimized for determining whether two strings are equivalent. In this mode, string comparisons ignore differences in uppercase and lower case letters, accented characters, etc. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Collator.html#Collator()" target="">Collator() constructor</a>
		 * </div>
		 */
		public static const MATCHING:String = "matching";
		/**
		 * <p> Initializes a Collator object so that the compare method is optimized for sorting a list of text strings to be displayed to an end user. In this mode, string comparisons consider differences in uppercase and lowercase letters, accented characters, and so on, according to the language and sorting rules required by the locale. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Collator.html#Collator()" target="">Collator() constructor</a>
		 * </div>
		 */
		public static const SORTING:String = "sorting";
	}
}
