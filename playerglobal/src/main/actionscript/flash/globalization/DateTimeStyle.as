package flash.globalization {
	/**
	 *  Enumerates constants that determine a locale-specific date and time formatting pattern. These constants are used when constructing a DateTimeFormatter object or when calling the <code>DateTimeFormatter.setDateTimeStyles()</code> method. <p>The <code>CUSTOM</code> constant cannot be used in the DateTimeFormatter constructor or the <code>DateFormatter.setDateTimeStyles()</code> method. This constant is instead set as the <code>timeStyle</code> and <code>dateStyle</code> property as a side effect of calling the <code>DateTimeFormatter.setDateTimePattern()</code> method. </p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="DateTimeFormatter.html" target="">DateTimeFormatter</a>
	 * </div><br><hr>
	 */
	public class DateTimeStyle {
		/**
		 * <p> Specifies that a custom pattern string is used to specify the date or time style. </p>
		 */
		public static const CUSTOM:String = "custom";
		/**
		 * <p> Specifies the long style of a date or time. </p>
		 */
		public static const LONG:String = "long";
		/**
		 * <p> Specifies the medium style of a date or time. </p>
		 */
		public static const MEDIUM:String = "medium";
		/**
		 * <p> Specifies that the date or time should not be included in the formatted string. </p>
		 */
		public static const NONE:String = "none";
		/**
		 * <p> Specifies the short style of a date or time. </p>
		 */
		public static const SHORT:String = "short";
	}
}
