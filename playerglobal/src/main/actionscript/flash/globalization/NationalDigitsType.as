package flash.globalization {
	/**
	 *  The NationalDigitsType class enumerates constants that indicate digit sets used by the NumberFormatter class. The value of each constant represents the Unicode value for the zero digit in the specified decimal digit set. <br><hr>
	 */
	public class NationalDigitsType {
		/**
		 * <p> Represents the Unicode value for the zero digit of the Arabic-Indic digit set. </p>
		 */
		public static const ARABIC_INDIC:uint = 0x0660;
		/**
		 * <p> Represents the Unicode value for the zero digit of the Balinese digit set. </p>
		 */
		public static const BALINESE:uint = 0x1B50;
		/**
		 * <p> Represents the Unicode value for the zero digit of the Bengali digit set. </p>
		 */
		public static const BENGALI:uint = 0x09E6;
		/**
		 * <p> Represents the Unicode value for the zero digit of the Cham digit set. </p>
		 */
		public static const CHAM:uint = 0xAA50;
		/**
		 * <p> Represents the Unicode value for the zero digit of the Devanagari digit set. </p>
		 */
		public static const DEVANAGARI:uint = 0x0966;
		/**
		 * <p> Represents the Unicode value for the zero digit of the Latin-1 (European) digit set. </p>
		 */
		public static const EUROPEAN:uint = 0x0030;
		/**
		 * <p> Represents the Unicode value for the zero digit of the Extended Arabic-Indic digit set. </p>
		 */
		public static const EXTENDED_ARABIC_INDIC:uint = 0x06F0;
		/**
		 * <p> Represents the Unicode value for the zero digit of the Fullwidth digit set. </p>
		 */
		public static const FULL_WIDTH:uint = 0xFF10;
		/**
		 * <p> Represents the Unicode value for the zero digit of the Gujarati digit set. </p>
		 */
		public static const GUJARATI:uint = 0x0AE6;
		/**
		 * <p> Represents the Unicode value for the zero digit of the Gurmukhi digit set. </p>
		 */
		public static const GURMUKHI:uint = 0x0A66;
		/**
		 * <p> Represents the Unicode value for the zero digit of the Kannada digit set. </p>
		 */
		public static const KANNADA:uint = 0x0CE6;
		/**
		 * <p> Represents the Unicode value for the zero digit of the Kayah Li digit set. </p>
		 */
		public static const KAYAH_LI:uint = 0xA900;
		/**
		 * <p> Represents the Unicode value for the zero digit of the Khmer digit set. </p>
		 */
		public static const KHMER:uint = 0x17E0;
		/**
		 * <p> Represents the Unicode value for the zero digit of the Lao digit set. </p>
		 */
		public static const LAO:uint = 0x0ED0;
		/**
		 * <p> Represents the Unicode value for the zero digit of the Lepcha digit set. </p>
		 */
		public static const LEPCHA:uint = 0x1C40;
		/**
		 * <p> Represents the Unicode value for the zero digit of the Limbu digit set. </p>
		 */
		public static const LIMBU:uint = 0x1946;
		/**
		 * <p> Represents the Unicode value for the zero digit of the Malayalam digit set. </p>
		 */
		public static const MALAYALAM:uint = 0x0D66;
		/**
		 * <p> Represents the Unicode value for the zero digit of the Mongolian digit set. </p>
		 */
		public static const MONGOLIAN:uint = 0x1810;
		/**
		 * <p> Represents the Unicode value for the zero digit of the Myanmar digit set. </p>
		 */
		public static const MYANMAR:uint = 0x1040;
		/**
		 * <p> Represents the Unicode value for the zero digit of the Myanmar Shan digit set. </p>
		 */
		public static const MYANMAR_SHAN:uint = 0x1090;
		/**
		 * <p> Represents the Unicode value for the zero digit of the New Tai Lue digit set. </p>
		 */
		public static const NEW_TAI_LUE:uint = 0x19D0;
		/**
		 * <p> Represents the Unicode value for the zero digit of the Nko digit set. </p>
		 */
		public static const NKO:uint = 0x07C0;
		/**
		 * <p> Represents the Unicode value for the zero digit of the Ol Chiki digit set. </p>
		 */
		public static const OL_CHIKI:uint = 0x1C50;
		/**
		 * <p> Represents the Unicode value for the zero digit of the Oriya digit set. </p>
		 */
		public static const ORIYA:uint = 0x0B66;
		/**
		 * <p> Represents the Unicode value for the zero digit of the Osmanya digit set. </p>
		 */
		public static const OSMANYA:uint = 0x104A0;
		/**
		 * <p> Represents the Unicode value for the zero digit of the Saurashtra digit set. </p>
		 */
		public static const SAURASHTRA:uint = 0xA8D0;
		/**
		 * <p> Represents the Unicode value for the zero digit of the Sundanese digit set. </p>
		 */
		public static const SUNDANESE:uint = 0x1BB0;
		/**
		 * <p> Represents the Unicode value for the zero digit of the Tamil digit set. </p>
		 */
		public static const TAMIL:uint = 0x0BE6;
		/**
		 * <p> Represents the Unicode value for the zero digit of the Telugu digit set. </p>
		 */
		public static const TELUGU:uint = 0x0C66;
		/**
		 * <p> Represents the Unicode value for the zero digit of the Thai digit set. </p>
		 */
		public static const THAI:uint = 0x0E50;
		/**
		 * <p> Represents the Unicode value for the zero digit of the Tibetan digit set. </p>
		 */
		public static const TIBETAN:uint = 0x0F20;
		/**
		 * <p> Represents the Unicode value for the zero digit of the Vai digit set. </p>
		 */
		public static const VAI:uint = 0xA620;
	}
}
