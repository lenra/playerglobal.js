package flash.globalization {
	/**
	 *  The LocaleID class provides methods for parsing and using locale ID names. This class supports locale ID names that use the syntax defined by the Unicode Technical Standard #35 (<a href="http://unicode.org/reports/tr35/">http://unicode.org/reports/tr35/</a>). <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS9b644acd4ebe59993a5b57f812214f2074b-7ffd.html" target="_blank">Choosing a locale</a>
	 * </div><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS19f279b149e7481c-99797f112bd080033f-8000.html" target="_blank">Setting the locale</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS19f279b149e7481c-11cc159f12ea10f6efe-7ffa.html" target="_blank">Using the LocaleID class</a>
	 *  <br>
	 *  <a href="http://unicode.org/reports/tr35/" target="_blank">Unicode Technical Standard #35</a>
	 * </div><br><hr>
	 */
	public class LocaleID {
		/**
		 * <p> Indicates that the user's default linguistic preferences should be used, as specified in the user's operating system settings. For example, such preferences are typically set using the "Control Panel" for Windows, or the "System Preferences" in Mac OS X. </p>
		 * <p>Using the <code>LocaleID.DEFAULT</code> setting can result in the use of a different locale ID name for different kinds of operations. For example, one locale could be used for sorting and a different one for formatting. This flexibility respects the user preferences, and the class behaves this way by design.</p>
		 * <p>This locale identifier is not always the most appropriate one to use. For applications running in the browser, the browser's preferred locale could be a better choice. It is often a good idea to let the user alter the preferred locale ID name setting and preserve that preference in a user profile, cookie, or shared object.</p>
		 */
		public static const DEFAULT:String = "i-default";

		private var _lastOperationStatus:String;
		private var _name:String;

		/**
		 * <p> Constructs a new LocaleID object, given a locale name. The locale name must conform to the syntax defined by the Unicode Technical Standard #35 (<a href="http://unicode.org/reports/tr35/">http://unicode.org/reports/tr35/</a>). </p>
		 * <p>When the constructor completes successfully the <code>lastOperationStatus</code> property is set to:</p>
		 * <ul>
		 *  <li><code>LastOperationStatus.NO_ERROR</code></li>
		 * </ul>
		 * <p>When the requested locale ID name is not available then the <code>lastOperationStatus</code> is set to one of the following:</p>
		 * <ul>
		 *  <li><code>LastOperationStatus.USING_FALLBACK_WARNING</code></li>
		 *  <li><code>LastOperationStatus.USING_DEFAULT_WARNING</code></li>
		 * </ul>
		 * <p>Otherwise the <code>lastOperationStatus</code> property is set to one of the constants defined in the LastOperationStatus class.</p>
		 * <p>For details on the warnings listed above and other possible values of the <code>lastOperationStatus</code> property see the descriptions in the <code>LastOperationStatus</code> class.</p>
		 * 
		 * @param name  — A locale ID name, which can also include an optional collation string. For example: <code>"en-US"</code> or <code>"de-DE@collation=phonebook"</code> 
		 */
		public function LocaleID(name:String) {
			throw new Error("Not implemented");
		}

		/**
		 * <p> The status of the most recent operation that this LocaleID object performed. The <code>lastOperationStatus</code> property is set whenever the constructor or a method of this class is called or another property is set. For the possible values see the description for each method. </p>
		 * 
		 * @return 
		 */
		public function get lastOperationStatus():String {
			return _lastOperationStatus;
		}

		/**
		 * <p> Returns a slightly more "canonical" locale identifier. </p>
		 * <p>This method performs the following conversion to the locale ID name to give it a more canonical form.</p>
		 * <ul>
		 *  <li>Proper casing is applied to all of the components.</li>
		 *  <li>Underscores are converted to dashes.</li>
		 * </ul>
		 * <p>No additional processing is performed. For example, aliases are not replaced, and no elements are added or removed.</p>
		 * <p>When this method is called and it completes successfully, the <code>lastOperationStatus</code> property is set to:</p>
		 * <ul>
		 *  <li><code>LastOperationStatus.NO_ERROR</code></li>
		 * </ul>
		 * <p>Otherwise the <code>lastOperationStatus</code> property is set to one of the constants defined in the <code>LastOperationStatus</code> class.</p>
		 * 
		 * @return 
		 */
		public function get name():String {
			return _name;
		}

		/**
		 * <p> Returns an object containing all of the key and value pairs from the LocaleID object. </p>
		 * <p>The returned object is structured as a hash table or associative array, where each property name represents a key and the value of the property is value for that key. For example, the following code lists all of the keys and values obtained from the LocaleID object using the <code>getKeysAndValues()</code> method:</p>
		 * <div class="listing" version="3.0">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *          var myLocale:LocaleID = new LocaleID("fr-CA");
		 *          var localeData:Object = myLocale.getKeysAndValues();
		 *          for (var propertyName:String in localeData)
		 *          {
		 *            trace(propertyName + " = " + localeData[propertyName]);
		 *          }
		 *          </pre>
		 * </div>
		 * <p>When this method is called and it completes successfully, the <code>lastOperationStatus</code> property is set to:</p>
		 * <ul>
		 *  <li><code>LastOperationStatus.NO_ERROR</code></li>
		 * </ul>
		 * <p>Otherwise the <code>lastOperationStatus</code> property is set to one of the constants defined in the <code>LastOperationStatus</code> class.</p>
		 * 
		 * @return  — An Object containing all the keys and values in the LocaleID object, structured as an associative array or hashtable. 
		 */
		public function getKeysAndValues():Object {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the language code specified by the locale ID name. </p>
		 * <p>If the locale name cannot be properly parsed then the language code is the same as the full locale name.</p>
		 * <p>When this method is called and it completes successfully, the <code>lastOperationStatus</code> property is set to:</p>
		 * <ul>
		 *  <li><code>LastOperationStatus.NO_ERROR</code></li>
		 * </ul>
		 * <p>Otherwise the <code>lastOperationStatus</code> property is set to one of the constants defined in the <code>LastOperationStatus</code> class.</p>
		 * 
		 * @return  — A two-character language code obtained by parsing the locale ID name. 
		 */
		public function getLanguage():String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the region code specified by the locale ID name. </p>
		 * <p>This method returns an empty string if the region code cannot be parsed or guessed This could occur if an unknown or incomplete locale ID name like "xy" is used. The region code is not validated against a fixed list. For example, the region code returned for a locale ID name of "xx-YY" is "YY".</p>
		 * <p>When this method is called and it completes successfully, the <code>lastOperationStatus</code> property is set to:</p>
		 * <ul>
		 *  <li><code>LastOperationStatus.NO_ERROR</code></li>
		 * </ul>
		 * <p>If the region is not part of the specified locale name, the most likely region code for the locale is "guessed" and <code>lastOperationStatus</code> property is set to <code>LastOperationStatus.USING_FALLBACK_WARNING</code></p>
		 * <p>Otherwise the <code>lastOperationStatus</code> property is set to one of the constants defined in the <code>LastOperationStatus</code> class.</p>
		 * 
		 * @return  — A two-character region code, or an empty string if the region code cannot be parsed or otherwise determined from the locale name. 
		 */
		public function getRegion():String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the script code specified by the locale ID name. </p>
		 * <p>This method returns an empty string if the script code cannot be parsed or guessed This could occur if an unknown or incomplete locale ID name like "xy" is used. The script code is not validated against a fixed list. For example, the script code returned for a locale ID name of "xx-Abcd-YY" is "Abcd".</p>
		 * <p>The region, as well as the language, can also affect the return value. For example, the script code for "mn-MN" (Mongolian-Mongolia) is "Cyrl" (Cyrillic), while the script code for "mn-CN" (Mongolian-China) is "Mong" (Mongolian).</p>
		 * <p>When this method is called and it completes successfully, the <code>lastOperationStatus</code> property is set to:</p>
		 * <ul>
		 *  <li><code>LastOperationStatus.NO_ERROR</code></li>
		 * </ul>
		 * <p>If the script code is not part of the specified locale name, the most likely script code is "guessed" and <code>lastOperationStatus</code> property is set to <code>LastOperationStatus.USING_FALLBACK_WARNING</code>.</p>
		 * <p>Otherwise the <code>lastOperationStatus</code> property is set to one of the constants defined in the <code>LastOperationStatus</code> class.</p>
		 * 
		 * @return  — A four-character script code, or an empty string if the script code cannot be parsed or otherwise determined from the locale name. 
		 */
		public function getScript():String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the language variant code specified by the locale ID name. </p>
		 * <p>This method returns an empty string if there is no language variant code in the given locale ID name. (No guessing is necessary because few locales have or need a language variant.)</p>
		 * <p>When this method is called and it completes successfully, the <code>lastOperationStatus</code> property is set to:</p>
		 * <ul>
		 *  <li><code>LastOperationStatus.NO_ERROR</code></li>
		 * </ul>
		 * <p>Otherwise the <code>lastOperationStatus</code> property is set to one of the constants defined in the <code>LastOperationStatus</code> class.</p>
		 * 
		 * @return  — A language variant code, or an empty string if the locale ID name does not contain a language variant code. 
		 */
		public function getVariant():String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Specifies whtehr the text direction for the specified locale is right to left. </p>
		 * <p>The result can be used to determine the direction of the text in the Flash text engine, and to decide whether to mirror the user interface to support the current text direction.</p>
		 * <p>When this method is called and it completes successfully, the <code>lastOperationStatus</code> property is set to:</p>
		 * <ul>
		 *  <li><code>LastOperationStatus.NO_ERROR</code></li>
		 * </ul>
		 * <p>Otherwise the <code>lastOperationStatus</code> property is set to one of the constants defined in the <code>LastOperationStatus</code> class.</p>
		 * 
		 * @return  —  if the general text flows in a line of text should go from right to left; otherwise ; 
		 */
		public function isRightToLeft():Boolean {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Returns a list of acceptable locales based on a list of desired locales and a list of the locales that are currently available. </p>
		 * <p>The resulting list is sorted according in order of preference.</p>
		 * <p>Here is a typical use case for this method:</p>
		 * <ul>
		 *  <li>A user specifies a list of languages that she understands (stored in a user profile, a browser setting, or a cookie). The user lists the languages that she understands best first, so the order of the languages in the list is relevant. This list is the "want" list.</li>
		 *  <li>The application is localized into a number of different languages. This list is the "have" list.</li>
		 *  <li>The <code>determinePreferredLocales()</code> method returns an intersection of the two lists, sorted so that the user's preferred languages come first.</li>
		 * </ul>
		 * <p>If this feature is not supported on the current operating system, this method returns a null value.</p>
		 * <p>When this method is called and it completes successfully, the <code>lastOperationStatus</code> property is set to:</p>
		 * <ul>
		 *  <li><code>LastOperationStatus.NO_ERROR</code></li>
		 * </ul>
		 * <p>Otherwise the <code>lastOperationStatus</code> property is set to one of the constants defined in the <code>LastOperationStatus</code> class.</p>
		 * 
		 * @param want  — A list of the user's preferred locales sorted in order of preference. 
		 * @param have  — A list of locales available to the application. The order of this list is not important. 
		 * @param keyword  — A keyword to use to help determine the best fit. 
		 * @return  — A subset of the available locales, sorted according to the user's preferences. 
		 */
		public static function determinePreferredLocales(want:Vector.<String>, have:Vector.<String>, keyword:String = "userinterface"):Vector.<String> {
			throw new Error("Not implemented");
		}
	}
}
