package flash.globalization {
	/**
	 *  A data structure that represents a currency amount and currency symbol or string that were extracted by parsing a currency value. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="CurrencyFormatter.html#parse()" target="">CurrencyFormatter.parse()</a>
	 * </div><br><hr>
	 */
	public class CurrencyParseResult {
		private var _currencyString:String;
		private var _value:Number;

		/**
		 * <p> Constructs a currency parse result object. </p>
		 * 
		 * @param value  — A number representing the currency amount value. 
		 * @param symbol  — A string representing the currency symbol. 
		 */
		public function CurrencyParseResult(value:Number = NaN, symbol:String = "") {
			throw new Error("Not implemented");
		}

		/**
		 * <p> The portion of the input string that corresponds to the currency symbol or currency string. </p>
		 * 
		 * @return 
		 */
		public function get currencyString():String {
			return _currencyString;
		}

		/**
		 * <p> The currency amount value that was extracted from the input string. </p>
		 * 
		 * @return 
		 */
		public function get value():Number {
			return _value;
		}
	}
}
