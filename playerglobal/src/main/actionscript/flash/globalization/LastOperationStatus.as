package flash.globalization {
	/**
	 *  The LastOperationStatus class enumerates constant values that represent the status of the most recent globalization service operation. These values can be retrieved through the read-only property <code>lastOperationStatus</code> available in most globalization classes. <br><hr>
	 */
	public class LastOperationStatus {
		/**
		 * <p> Indicates that given buffer is not enough to hold the result. </p>
		 */
		public static const BUFFER_OVERFLOW_ERROR:String = "bufferOverflowError";
		/**
		 * <p> Indicates that the return error code is not known. Any non-static method or read/write properties can return this error when the operation is not successful and the return error code is not known. </p>
		 */
		public static const ERROR_CODE_UNKNOWN:String = "errorCodeUnknown";
		/**
		 * <p> Indicates that an argument passed to a method was illegal. </p>
		 * <p>For example, the following code shows that an invalid argument error status is set when <code>CurrencyFormatter.grouping</code> property is set to the invalid value "3;". </p>
		 * <div class="listing" version="3.0">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *         var cf:CurrencyFormatter = new CurrencyFormatter("en-US");
		 *         cf.groupingPattern = "3;";
		 *         trace(cf.lastOperationStatus); // "illegalArgumentError"
		 *         </pre>
		 * </div>
		 */
		public static const ILLEGAL_ARGUMENT_ERROR:String = "illegalArgumentError";
		/**
		 * <p> Indicates that an iterator went out of range or an invalid parameter was specified for month, day, or time. </p>
		 */
		public static const INDEX_OUT_OF_BOUNDS_ERROR:String = "indexOutOfBoundsError";
		/**
		 * <p> Indicates that a given attribute value is out of the expected range. </p>
		 * <p>The following example shows that setting the <code>NumberFormatter.negativeNumberFormat</code> property to an out-of-range value results in an invalid attribute value status. </p>
		 * <div class="listing" version="3.0">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *         var nf:NumberFormatter = new NumberFormatter(LocaleID.DEFAULT);
		 *         nf.negativeNumberFormat = 9;
		 *         nf.lastOperationStatus; // "invalidAttrValue" 
		 *         </pre>
		 * </div>
		 */
		public static const INVALID_ATTR_VALUE:String = "invalidAttrValue";
		/**
		 * <p> Indicates that invalid Unicode value was found. </p>
		 */
		public static const INVALID_CHAR_FOUND:String = "invalidCharFound";
		/**
		 * <p> Indicates that memory allocation has failed. </p>
		 */
		public static const MEMORY_ALLOCATION_ERROR:String = "memoryAllocationError";
		/**
		 * <p> Indicates that the last operation succeeded without any errors. This status can be returned by all constructors, non-static methods, static methods and read/write properties. </p>
		 */
		public static const NO_ERROR:String = "noError";
		/**
		 * <p> Indicates that an operation resulted a value that exceeds a specified numeric type. </p>
		 */
		public static const NUMBER_OVERFLOW_ERROR:String = "numberOverflowError";
		/**
		 * <p> Indicates that the parsing of a number failed. This status can be returned by parsing methods of the formatter classes, such as <code>CurrencyFormatter.parse()</code> and <code>NumberFormatter.parseNumber()</code>. For example, if the value "12abc34" is passed as the parameter to the <code>CurrencyFormatter.parse()</code> method, the method returns "NaN" and sets the <code>lastOperationStatus</code> value to <code>LastOperationStatus.PARSE_ERROR</code>. </p>
		 */
		public static const PARSE_ERROR:String = "parseError";
		/**
		 * <p> Indicates that the pattern for formatting a number, date, or time is invalid. This status is set when the user's operating system does not support the given pattern. </p>
		 * <p>For example, the following code shows the value of the <code>lastOperationStatus</code> property after an invalid "xx" pattern is used for date formatting:</p>
		 * <div class="listing" version="3.0">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *         var df:DateTimeFormatter = new DateTimeFormatter("en-US");
		 *         df.setDateTimePattern("xx");
		 *         trace(df.lastOperationStatus); // "patternSyntaxError" 
		 *         </pre>
		 * </div>
		 */
		public static const PATTERN_SYNTAX_ERROR:String = "patternSyntaxError";
		/**
		 * <p> Indicates that an underlying platform API failed for an operation. </p>
		 */
		public static const PLATFORM_API_FAILED:String = "platformAPIFailed";
		/**
		 * <p> Indicates that a truncated Unicode character value was found. </p>
		 */
		public static const TRUNCATED_CHAR_FOUND:String = "truncatedCharFound";
		/**
		 * <p> Indicates that an unexpected token was detected in a Locale ID string. </p>
		 * <p>For example, the following code shows the value of the <code>lastOperationStatus</code> property after an incomplete string is used when requesting a locale ID. As a result the <code>lastOperationStatus</code> property is set to the value <code>UNEXPECTED_TOKEN</code> after a call to the <code>LocaleID.getKeysAndValues()</code> method. </p>
		 * <div class="listing" version="3.0">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *         var locale:LocaleID = new LocaleID("en-US@Collation");
		 *         var kav:Object = locale.getKeysAndValues();
		 *         trace(locale.lastOperationStatus); // "unexpectedToken"
		 *         </pre>
		 * </div>
		 */
		public static const UNEXPECTED_TOKEN:String = "unexpectedToken";
		/**
		 * <p> Indicates that the requested operation or option is not supported. This status can be returned by methods like <code>DateTimeFormatter.setDateTimePattern()</code> and when retrieving properties like <code>Collator.ignoreCase</code>. </p>
		 */
		public static const UNSUPPORTED_ERROR:String = "unsupportedError";
		/**
		 * <p> Indicates that an operating system default value was used during the most recent operation. Class constructors can return this status. </p>
		 */
		public static const USING_DEFAULT_WARNING:String = "usingDefaultWarning";
		/**
		 * <p> Indicates that a fallback value was set during the most recent operation. This status can be returned by constructors and methods like <code>DateTimeFormatter.setDateTimeStyles()</code>, and when retrieving properties like <code>CurrencyFormatter.groupingPattern</code>. </p>
		 */
		public static const USING_FALLBACK_WARNING:String = "usingFallbackWarning";
	}
}
