package flash.globalization {
	/**
	 *  A data structure that holds information about a number that was extracted by parsing a string. <p>The number string can contain a prefix and suffix surrounding a number. In such cases the <code>startIndex</code> property is set to the first character of the number. Also, the <code>endIndex</code> property is set to the index of the character that follows the last character of the number. </p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="NumberFormatter.html#parse()" target="">NumberFormatter.parse()</a>
	 *  <br>
	 *  <a href="NumberFormatter.html#parseNumber()" target="">NumberFormatter.parseNumber()</a>
	 * </div><br><hr>
	 */
	public class NumberParseResult {
		private var _endIndex:int;
		private var _startIndex:int;
		private var _value:Number;

		/**
		 * <p> Constructs a number parse result object. NumberParseResult objects are usually created by the <code>NumberFormatter.parse()</code> and <code>NumberFormatter.parseNumber()</code> methods, rather than by calling this constructor directly. </p>
		 * 
		 * @param value  — The value of the numeric portion of the input string. 
		 * @param startIndex  — The index of the first character of the number in the input string. 
		 * @param endIndex  — The index of the character after the last character of the number in the input string. 
		 */
		public function NumberParseResult(value:Number = NaN, startIndex:int = 0x7fffffff, endIndex:int = 0x7fffffff) {
			throw new Error("Not implemented");
		}

		/**
		 * <p> The index of the character after the last character of the numeric portion of the input string. </p>
		 * 
		 * @return 
		 */
		public function get endIndex():int {
			return _endIndex;
		}

		/**
		 * <p> The index of the first character of the numeric portion of the input string. </p>
		 * 
		 * @return 
		 */
		public function get startIndex():int {
			return _startIndex;
		}

		/**
		 * <p> The value of the numeric portion of the input string. </p>
		 * 
		 * @return 
		 */
		public function get value():Number {
			return _value;
		}
	}
}
