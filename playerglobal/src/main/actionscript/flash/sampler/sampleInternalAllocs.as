package flash.sampler {

	/**
	 * <p> Tells the sampler if it should create NewObjectSamples for internal allocations from the flash player. If this is set to true, then every allocation will generate a NewObjectSample. These internal allocs will not have a type, or a reference to the Object. They will have the ActionScript stack trace that triggered the allocation. Defaults to false, which only collects allocations for ActionScript objects. </p>
	 * 
	 * @param b
	 */
	public function sampleInternalAllocs(b:Boolean):void {
		throw new Error("Not implemented");
	}
}
