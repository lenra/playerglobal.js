package flash.sampler {
	/**
	 *  The DeleteObjectSample class represents objects that are created within a <code>getSamples()</code> stream; each DeleteObjectSample object corresponds to a NewObjectSample object. For Flash Player debugger version only. <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingFlashBuilder/WS6f97d7caa66ef6eb1e63e3d11b6c4d0d21-7edf.html" target="_blank">How the profiler works</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="package.html#getSamples()" target="">flash.sampler.getSamples()</a>
	 * </div><br><hr>
	 */
	public class DeleteObjectSample extends Sample {
		/**
		 * <p> The unique identification number that matches up with a NewObjectSample's identification number. For Flash Player debugger version only. </p>
		 */
		public const id:Number = 0;//TODO: manage the unique id
		/**
		 * <p> The size of the DeleteObjectSample object before it is deleted. For Flash Player debugger version only. </p>
		 */
		public const size:Number = 0;//TODO: manage the size
	}
}
