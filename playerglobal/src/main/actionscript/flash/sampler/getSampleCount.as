package flash.sampler {

	/**
	 * <p> Returns the number of samples collected. For Flash Player debugger version only. </p>
	 * 
	 * @return  — An iterator of Sample instances. 
	 */
	public function getSampleCount():Number {
		throw new Error("Not implemented");
	}
}
