package flash.sampler {

	/**
	 * <p> Returns the number of times a get function was executed. Use <code>isGetterSetter()</code> to verify that you have a get/set function before you use <code>getGetterInvocationCount()</code>. For Flash Player debugger version only. </p>
	 * 
	 * @param obj  — A method instance or a class. 
	 * @param qname  — If qname is <code>undefined</code> return the number of iterations of the constructor function. 
	 * @return  — The number of times a get method was executed. 
	 */
	public function getGetterInvocationCount(obj:Object, qname:QName):Number {
		throw new Error("Not implemented");
	}
}
