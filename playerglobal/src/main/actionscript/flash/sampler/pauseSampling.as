package flash.sampler {

	/**
	 * <p> Stops the sampling process momentarily. Restart the sampling process using <code>startSampling()</code>. For Flash Player debugger version only. </p>
	 * <p><span class="label">Related API Elements</span></p>
	 * <div class="seeAlso">
	 *  <a href="package.html#startSampling()" target="">startSampling()</a>
	 * </div>
	 */
	public function pauseSampling():void {
		throw new Error("Not implemented");
	}
}
