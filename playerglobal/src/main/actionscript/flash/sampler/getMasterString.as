package flash.sampler {

	/**
	 * <p> Returns the master string upon which this string depends, or null if this string does not depend on another string. For example, if you call <code>String.substr()</code>, the returned string will often actually be implemented as just a pointer into the original string, for the sake of efficiency. In normal usage, this is an implementation detail which is not visible to the user; however, it can be confusing when using a profiler to analyze your program's memory consumption, because the string may be shown as taking less memory than would be needed for the string's value. In addition, a string might be retained in memory solely because it is the master for other strings. <code>getMasterString()</code> allows profilers to show the user an accurate graph of string dependencies. </p>
	 * 
	 * @param str  — A string 
	 * @return  — The string upon which the passed-in string depends, or null if the passed-in string does not depend on another string 
	 */
	public function getMasterString(str:String):String {
		throw new Error("Not implemented");
	}
}
