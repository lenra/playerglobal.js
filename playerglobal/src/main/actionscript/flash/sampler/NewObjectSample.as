package flash.sampler {
	/**
	 *  The NewObjectSample class represents objects that are created within a <code>getSamples()</code> stream. For Flash Player debugger version only. <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingFlashBuilder/WS6f97d7caa66ef6eb1e63e3d11b6c4d0d21-7edf.html" target="_blank">How the profiler works</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="package.html#getSamples()" target="">flash.sampler.getSamples()</a>
	 * </div><br><hr>
	 */
	public class NewObjectSample extends Sample {
		/**
		 * <p> The Class object corresponding to the object created within a <code>getSamples()</code> stream. For Flash Player debugger version only. </p>
		 */
		public const type:Class = null;//TODO: manage this

		private var _object:*;
		private var _size:Number;

		/**
		 * <p> The NewObjectSample object if it still exists. If the object has been garbage collected, this property is undefined and a corresponding DeleteObjectSample exists. For Flash Player debugger version only. </p>
		 * 
		 * @return 
		 */
		public function get object():* {
			return _object;
		}

		/**
		 * <p> The NewObjectSample object size. If the object has been garbage collected, this property is undefined and a corresponding DeleteObjectSample exists. For FlashPlayer debugger version only. </p>
		 * 
		 * @return 
		 */
		public function get size():Number {
			return _size;
		}
	}
}
