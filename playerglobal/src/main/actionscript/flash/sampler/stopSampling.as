package flash.sampler {

	/**
	 * <p> Ends the process of collecting memory usage Sample objects and frees resources dedicated to the sampling process. You start the sampling process with <code>startSampling()</code>. For Flash Player debugger version only. </p>
	 * <p><span class="label">Related API Elements</span></p>
	 * <div class="seeAlso">
	 *  <a href="Sample.html" target="">Sample class</a>
	 * </div>
	 */
	public function stopSampling():void {
		throw new Error("Not implemented");
	}
}
