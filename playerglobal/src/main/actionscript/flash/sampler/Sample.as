package flash.sampler {
	/**
	 *  The Sample class creates objects that hold memory analysis information over distinct durations. For Flash Player debugger version only. <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingFlashBuilder/WS6f97d7caa66ef6eb1e63e3d11b6c4d0d21-7edf.html" target="_blank">How the profiler works</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="package.html#getSamples()" target="">flash.sampler.getSamples()</a>
	 * </div><br><hr>
	 */
	public class Sample {
		/**
		 * <p> Contains information about the methods executed by Flash Player over a specified period of time. The format for the stack trace is similiar to the content shown in the exception dialog box of the Flash Player debugger version. For Flash Player debugger version only. </p>
		 */
		public const stack:Array = [];//TODO: manage this
		/**
		 * <p> The microseconds that define the duration of the Sample instance. For Flash Player debugger version only. </p>
		 */
		public const time:Number = 0;//TODO: manage this
	}
}
