package flash.sampler {

	/**
	 * <p> Checks to see if a property is defined by a get/set function. If you want to use <code>getInvocationCount()</code> on a get/set function for a property, first call <code>isGetterSetter()</code> to check to see if it is a get/set function, and then use either <code>getSetterInvocationCount</code> or <code>getGetterInvocationCount</code> to get the respective counts. For Flash Player debugger version only. </p>
	 * 
	 * @param obj  — A method instance or a class. 
	 * @param qname  — If qname is <code>undefined</code> return the number of iterations of the constructor function. 
	 * @return  — A Boolean value indicating if the property is defined by a get/set function () or not (). 
	 */
	public function isGetterSetter(obj:Object, qname:QName):Boolean {
		throw new Error("Not implemented");
	}
}
