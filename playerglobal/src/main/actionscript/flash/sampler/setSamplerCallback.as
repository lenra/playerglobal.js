package flash.sampler {

	/**
	 * <p> Sets a callback function for the sampler - this function will be called when the sample stream is almost exhausted. This should be used to process samples before the sample buffer is filled. pauseSampling will be called before the callback is called, and startSampling will be called after the callback has been executed. </p>
	 * 
	 * @param f
	 */
	public function setSamplerCallback(f:Function):void {
		throw new Error("Not implemented");
	}
}
