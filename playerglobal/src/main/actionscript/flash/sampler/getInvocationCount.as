package flash.sampler {

	/**
	 * <p> Returns the number of times a method was executed. If the parameter <code>obj</code> is a Class and the parameter <code>qname</code> is <code>undefined</code> then this method returns the number of iterations of the constructor function. For Flash Player debugger version only. </p>
	 * 
	 * @param obj  — A method instance or a class. A class can be used to get the invocation count of instance functions when a method instance isn't available. If <code>obj</code> is <code>undefined</code>, this method returns the count of the package-scoped function named by <code>qname</code>. 
	 * @param qname  — If qname is <code>undefined</code> return the number of iterations of the constructor function. 
	 * @return  — The number of times a method was executed. 
	 */
	public function getInvocationCount(obj:Object, qname:QName):Number {
		throw new Error("Not implemented");
	}
}
