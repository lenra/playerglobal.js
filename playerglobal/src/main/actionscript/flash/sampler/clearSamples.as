package flash.sampler {

	/**
	 * <p> Clears the current set of Sample objects. This method is usually called after calling <code>getSamples()</code> and iterating over the Sample objects. For Flash Player debugger version only. </p>
	 * <p><span class="label">Related API Elements</span></p>
	 * <div class="seeAlso">
	 *  <a href="package.html#getSamples()" target="">getSamples()</a>
	 * </div>
	 */
	public function clearSamples():void {
		throw new Error("Not implemented");
	}
}
