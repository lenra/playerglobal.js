package flash.sampler {

	/**
	 * <p> Returns the size in memory of a specified object when used with the Flash Player 9.0.115.0 or later debugger version. If used with a Flash Player that is not the debugger version, this method returns <code>0</code>. </p>
	 * 
	 * @param o  — The object to analyze for memory usage. 
	 * @return  — The byte count of memory used by the specified object. 
	 */
	public function getSize(o:*):Number {
		throw new Error("Not implemented");
	}
}
