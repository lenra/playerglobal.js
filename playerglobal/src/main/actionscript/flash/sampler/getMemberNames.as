package flash.sampler {

	/**
	 * <p> Returns an object containing all members of a specified object, including private members. You can then iterate over the returned object to see all values. This method is similar to the flash.utils.describeType() method but also allows you to see private members and skips the intermediate step of creating an XML object. For Flash Player debugger version only. </p>
	 * 
	 * @param o  — The object to analyze. 
	 * @param instanceNames  — If object is a Class and instanceNames is true report the instance names as if o was an instance of class instead of the class's member names. 
	 * @return  — An Object that you must iterate over with a  loop to retrieve the QNames for each property. 
	 */
	public function getMemberNames(o:Object, instanceNames:Boolean = false):Object {
		throw new Error("Not implemented");
	}
}
