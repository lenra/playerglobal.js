package flash.sampler {

	/**
	 * <p> Returns the number of times a set function was executed. Use <code>isGetterSetter()</code> to verify that you have a get/set function before you use <code>getSetterInvocationCount()</code>. For Flash Player debugger version only. </p>
	 * 
	 * @param obj  — A method instance or a class. 
	 * @param qname  — If qname is <code>undefined</code> return the number of iterations of the constructor function. 
	 * @return  — The number of times a set method was executed. 
	 */
	public function getSetterInvocationCount(obj:Object, qname:QName):Number {
		throw new Error("Not implemented");
	}
}
