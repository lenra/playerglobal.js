package flash.sampler {
	/**
	 *  The StackFrame class provides access to the properties of a data block containing a function. For Flash Player debugger version only. <p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingFlashBuilder/WS6f97d7caa66ef6eb1e63e3d11b6c4d0d21-7edf.html" target="_blank">How the profiler works</a>
	 * </div><br><hr>
	 */
	public class StackFrame {
		/**
		 * <p> The file name of the SWF file being debugged. For Flash Player debugger version only. </p>
		 */
		public const file:String = "";//TODO: manage this
		/**
		 * <p> The line number for the function in the SWF file being debugged. For Flash Player debugger version only. </p>
		 */
		public const line:uint = 0;//TODO: manage this
		/**
		 * <p> The function name in the stack frame. For Flash Player debugger version only. </p>
		 */
		public const name:String = "";//TODO: manage this
		/**
		 * <p> The identifier for the script function in the application being profiled. </p>
		 */
		public const scriptID:Number = 0;//TODO: manage this

		/**
		 * <p> Converts the StackFrame to a string of its properties. </p>
		 * 
		 * @return  — A string containing the  property, and optionally the  and  properties (if a SWF file is being debugged) of the StackFrame object. For Flash Player debugger version only. 
		 */
		public function toString():String {
			throw new Error("Not implemented");
		}
	}
}
