package flash.sampler {

	/**
	 * <p> Begins the process of collecting memory usage Sample objects. For Flash Player debugger version only. </p>
	 * <p><span class="label">Related API Elements</span></p>
	 * <div class="seeAlso">
	 *  <a href="Sample.html" target="">Sample class</a>
	 * </div>
	 * <span id="pageFilter"><br><span class="label"> Example &nbsp;( <span class="usage"><a href="http://www.adobe.com/go/learn_as3_usingexamples_en"> How to use this example </a></span>) </span><br><br>
	 *  <div class="detailBody">
	 *    The following example initiates the sampling process and iterates over the collected objects. To use the memory profiler, you need to have Flash Player debugger version 9.0.115.0 or later. 
	 *   <div class="listing">
	 *    <div class="clipcopy">
	 *     <a href="#" class="copyText">Copy</a>
	 *    </div>
	 *    <pre>
	 * package 
	 * {
	 *     import flash.sampler.*
	 *     import flash.system.*
	 *     import flash.display.Sprite
	 *     public class startSampling extends Sprite
	 *     {
	 *         public function startSampling()
	 *         {
	 *             flash.sampler.startSampling();
	 *             for(var i:int=0;i&lt;1000;i++)
	 *                 new Object()
	 *             trace(getSampleCount() &gt; 0)
	 *         }
	 *     }
	 * }
	 * </pre>
	 *   </div>
	 *  </div></span>
	 */
	public function startSampling():void {
		throw new Error("Not implemented");
	}
}
