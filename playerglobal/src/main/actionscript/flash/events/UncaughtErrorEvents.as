package flash.events {
	[Event(name="uncaughtError", type="flash.events.UncaughtErrorEvent")]
	/**
	 *  The UncaughtErrorEvents class provides a way to receive uncaught error events. An instance of this class dispatches an <code>uncaughtError</code> event when a runtime error occurs and the error isn't detected and handled in your code. <p>Use the following properties to access an UncaughtErrorEvents instance:</p> <ul> 
	 *  <li> <code>LoaderInfo.uncaughtErrorEvents</code>: to detect uncaught errors in code defined in the same SWF.</li> 
	 *  <li> <code>Loader.uncaughtErrorEvents</code>: to detect uncaught errors in code defined in the SWF loaded by a Loader object.</li> 
	 * </ul> <p>To catch an error directly and prevent an uncaught error event, do the following:</p> <ul> 
	 *  <li>Use a <code> <a href="../../statements.html#try..catch..finally">try..catch</a> </code> block to isolate code that potentially throws a synchronous error</li> 
	 *  <li>When performing an operation that dispatches an event when an error occurs, register a listener for that error event</li> 
	 * </ul> <p>If the content loaded by a Loader object is an AVM1 (ActionScript 2) SWF file, uncaught errors in the AVM1 SWF file do not result in an <code>uncaughtError</code> event. In addition, JavaScript errors in HTML content loaded in an HTMLLoader object (including a Flex HTML control) do not result in an <code>uncaughtError</code> event.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../flash/display/LoaderInfo.html#uncaughtErrorEvents" target="">LoaderInfo.uncaughtErrorEvents</a>
	 *  <br>
	 *  <a href="../../flash/display/Loader.html#uncaughtErrorEvents" target="">Loader.uncaughtErrorEvents</a>
	 *  <br>
	 *  <a href="UncaughtErrorEvent.html" target="">UncaughtErrorEvent</a>
	 * </div><br><hr>
	 */
	public class UncaughtErrorEvents extends EventDispatcher {

		/**
		 * <p> Creates an UncaughtErrorEvents instance. Developer code shouldn't create UncaughtErrorEvents instances directly. To access an UncaughtErrorEvents object, use one of the following properties: </p>
		 * <ul>
		 *  <li><code>LoaderInfo.uncaughtErrorEvents</code>: to detect uncaught errors in code defined in the same SWF.</li>
		 *  <li><code>Loader.uncaughtErrorEvents</code>: to detect uncaught errors in code defined in the SWF loaded by a Loader object.</li>
		 * </ul>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/LoaderInfo.html#uncaughtErrorEvents" target="">LoaderInfo.uncaughtErrorEvents</a>
		 *  <br>
		 *  <a href="../../flash/display/Loader.html#uncaughtErrorEvents" target="">Loader.uncaughtErrorEvents</a>
		 * </div>
		 */
		public function UncaughtErrorEvents() {
			super(this);
			throw new Error("Not implemented");
		}
	}
}
