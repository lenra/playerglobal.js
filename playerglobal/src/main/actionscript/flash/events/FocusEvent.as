package flash.events {
	import flash.display.InteractiveObject;

	/**
	 *  An object dispatches a FocusEvent object when the user changes the focus from one object in the display list to another. There are four types of focus events: <ul> 
	 *  <li> <code>FocusEvent.FOCUS_IN</code> </li> 
	 *  <li> <code>FocusEvent.FOCUS_OUT</code> </li> 
	 *  <li> <code>FocusEvent.KEY_FOCUS_CHANGE</code> </li> 
	 *  <li> <code>FocusEvent.MOUSE_FOCUS_CHANGE</code> </li> 
	 * </ul> <p><a href="#includeExamplesSummary">View the examples</a></p><br><hr>
	 */
	public class FocusEvent extends flash.events.Event {
		/**
		 * <p> Defines the value of the <code>type</code> property of a <code>focusIn</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>true</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>keyCode</code></td>
		 *    <td>0; applies only to <code>keyFocusChange</code> events.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>relatedObject</code></td>
		 *    <td>The complementary InteractiveObject instance that is affected by the change in focus.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>shiftKey</code></td>
		 *    <td><code>false</code>; applies only to <code>keyFocusChange</code> events.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The InteractiveObject instance that has just received focus. The <code>target</code> is not always the object in the display list that registered the event listener. Use the <code>currentTarget</code> property to access the object in the display list that is currently processing the event. </td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>direction</code></td>
		 *    <td>The direction from which focus was assigned. This property reports the value of the <code>direction</code> parameter of the <code>assignFocus()</code> method of the stage. If the focus changed through some other means, the value will always be <code>FocusDirection.NONE</code>. Applies only to <code>focusIn</code> events. For all other focus events the value will be <code>FocusDirection.NONE</code>.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/InteractiveObject.html#event:focusIn" target="">flash.display.InteractiveObject.focusIn</a>
		 * </div>
		 */
		public static const FOCUS_IN:String = "focusIn";
		/**
		 * <p> Defines the value of the <code>type</code> property of a <code>focusOut</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>true</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>keyCode</code></td>
		 *    <td>0; applies only to <code>keyFocusChange</code> events.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>relatedObject</code></td>
		 *    <td>The complementary InteractiveObject instance that is affected by the change in focus.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>shiftKey</code></td>
		 *    <td><code>false</code>; applies only to <code>keyFocusChange</code> events.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The InteractiveObject instance that has just lost focus. The <code>target</code> is not always the object in the display list that registered the event listener. Use the <code>currentTarget</code> property to access the object in the display list that is currently processing the event. </td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/InteractiveObject.html#event:focusOut" target="">flash.display.InteractiveObject.focusOut</a>
		 * </div>
		 */
		public static const FOCUS_OUT:String = "focusOut";
		/**
		 * <p> Defines the value of the <code>type</code> property of a <code>keyFocusChange</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>true</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>true</code>; call the <code>preventDefault()</code> method to cancel default behavior.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>keyCode</code></td>
		 *    <td>The key code value of the key pressed to trigger a <code>keyFocusChange</code> event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>relatedObject</code></td>
		 *    <td>The complementary InteractiveObject instance that is affected by the change in focus.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>shiftKey</code></td>
		 *    <td><code>true</code> if the Shift key modifier is activated; <code>false</code> otherwise.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The InteractiveObject instance that currently has focus. The <code>target</code> is not always the object in the display list that registered the event listener. Use the <code>currentTarget</code> property to access the object in the display list that is currently processing the event. </td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/InteractiveObject.html#event:keyFocusChange" target="">flash.display.InteractiveObject.keyFocusChange</a>
		 * </div>
		 */
		public static const KEY_FOCUS_CHANGE:String = "keyFocusChange";
		/**
		 * <p> Defines the value of the <code>type</code> property of a <code>mouseFocusChange</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>true</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>true</code>; call the <code>preventDefault()</code> method to cancel default behavior.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>keyCode</code></td>
		 *    <td>0; applies only to <code>keyFocusChange</code> events.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>relatedObject</code></td>
		 *    <td>The complementary InteractiveObject instance that is affected by the change in focus.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>shiftKey</code></td>
		 *    <td><code>false</code>; applies only to <code>keyFocusChange</code> events.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The InteractiveObject instance that currently has focus. The <code>target</code> is not always the object in the display list that registered the event listener. Use the <code>currentTarget</code> property to access the object in the display list that is currently processing the event. </td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/InteractiveObject.html#event:mouseFocusChange" target="">flash.display.InteractiveObject.mouseFocusChange</a>
		 * </div>
		 */
		public static const MOUSE_FOCUS_CHANGE:String = "mouseFocusChange";

		private var _direction:String;
		private var _isRelatedObjectInaccessible:Boolean;
		private var _keyCode:uint;
		private var _relatedObject:InteractiveObject;
		private var _shiftKey:Boolean;

		/**
		 * <p> Creates an Event object with specific information relevant to focus events. Event objects are passed as parameters to event listeners. </p>
		 * 
		 * @param type  — The type of the event. Possible values are: <code>FocusEvent.FOCUS_IN</code>, <code>FocusEvent.FOCUS_OUT</code>, <code>FocusEvent.KEY_FOCUS_CHANGE</code>, and <code>FocusEvent.MOUSE_FOCUS_CHANGE</code>. 
		 * @param bubbles  — Determines whether the Event object participates in the bubbling stage of the event flow. 
		 * @param cancelable  — Determines whether the Event object can be canceled. 
		 * @param relatedObject  — Indicates the complementary InteractiveObject instance that is affected by the change in focus. For example, when a <code>focusIn</code> event occurs, <code>relatedObject</code> represents the InteractiveObject that has lost focus. 
		 * @param shiftKey  — Indicates whether the Shift key modifier is activated. 
		 * @param keyCode  — Indicates the code of the key pressed to trigger a <code>keyFocusChange</code> event. 
		 * @param direction  — Indicates from which direction the target interactive object is being activated. Set to <code>FocusDirection.NONE</code> (the default value) for all events other than <code>focusIn</code>. 
		 */
		public function FocusEvent(type:String, bubbles:Boolean = true, cancelable:Boolean = false, relatedObject:InteractiveObject = null, shiftKey:Boolean = false, keyCode:uint = 0, direction:String = "none") {
			super(type, bubbles, cancelable);
		}

		/**
		 * <p> Specifies direction of focus for a <code>focusIn</code> event. </p>
		 * 
		 * @return 
		 */
		public function get direction():String {
			return _direction;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set direction(value:String):void {
			_direction = value;
		}

		/**
		 * <p> If <code>true</code>, the <code>relatedObject</code> property is set to <code>null</code> for reasons related to security sandboxes. If the nominal value of <code>relatedObject</code> is a reference to a DisplayObject in another sandbox, <code>relatedObject</code> is set to <code>null</code> unless there is permission in both directions across this sandbox boundary. Permission is established by calling <code>Security.allowDomain()</code> from a SWF file, or by providing a policy file from the server of an image file, and setting the <code>LoaderContext.checkPolicyFile</code> property when loading the image. </p>
		 * 
		 * @return 
		 */
		public function get isRelatedObjectInaccessible():Boolean {
			return _isRelatedObjectInaccessible;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set isRelatedObjectInaccessible(value:Boolean):void {
			_isRelatedObjectInaccessible = value;
		}

		/**
		 * <p> The key code value of the key pressed to trigger a <code>keyFocusChange</code> event. </p>
		 * 
		 * @return 
		 */
		public function get keyCode():uint {
			return _keyCode;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set keyCode(value:uint):void {
			_keyCode = value;
		}

		/**
		 * <p> A reference to the complementary InteractiveObject instance that is affected by the change in focus. For example, when a <code>focusOut</code> event occurs, the <code>relatedObject</code> represents the InteractiveObject instance that has gained focus. </p>
		 * <p>The value of this property can be <code>null</code> in two circumstances: if there no related object, or there is a related object, but it is in a security sandbox to which you don't have access. Use the <code>isRelatedObjectInaccessible()</code> property to determine which of these reasons applies.</p>
		 * 
		 * @return 
		 */
		public function get relatedObject():InteractiveObject {
			return _relatedObject;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set relatedObject(value:InteractiveObject):void {
			_relatedObject = value;
		}

		/**
		 * <p> Indicates whether the Shift key modifier is activated, in which case the value is <code>true</code>. Otherwise, the value is <code>false</code>. This property is used only if the FocusEvent is of type <code>keyFocusChange</code>. </p>
		 * 
		 * @return 
		 */
		public function get shiftKey():Boolean {
			return _shiftKey;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set shiftKey(value:Boolean):void {
			_shiftKey = value;
		}

		/**
		 * <p> Creates a copy of the FocusEvent object and sets the value of each property to match that of the original. </p>
		 * 
		 * @return  — A new FocusEvent object with property values that match those of the original. 
		 */
		override public function clone():flash.events.Event {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a string that contains all the properties of the FocusEvent object. The string is in the following format: </p>
		 * <p><code>[FocusEvent type=<i>value</i> bubbles=<i>value</i> cancelable=<i>value</i> relatedObject=<i>value</i> shiftKey=<i>value</i>] keyCode=<i>value</i>]</code></p>
		 * 
		 * @return  — A string that contains all the properties of the FocusEvent object. 
		 */
		override public function toString():String {
			throw new Error("Not implemented");
		}
	}
}
