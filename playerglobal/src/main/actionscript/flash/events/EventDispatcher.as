package flash.events {
	import flash.geom.Vector3D;
	[Event(name="activate", type="flash.events.Event")]
	[Event(name="deactivate", type="flash.events.Event")]
	/**
	 *  The EventDispatcher class is the base class for all classes that dispatch events. <span>The EventDispatcher class implements the IEventDispatcher interface and is the base class for the DisplayObject class. The EventDispatcher class allows any object on the display list to be an event target and as such, to use the methods of the IEventDispatcher interface.</span> <p>Event targets are an important part of the Flash<sup>®</sup> Player and Adobe<sup>®</sup> AIR<sup>®</sup> event model. The event target serves as the focal point for how events flow through the display list hierarchy. When an event such as a mouse click or a keypress occurs, Flash Player or the AIR application dispatches an event object into the event flow from the root of the display list. The event object then makes its way through the display list until it reaches the event target, at which point it begins its return trip through the display list. This round-trip journey to the event target is conceptually divided into three phases: the capture phase comprises the journey from the root to the last node before the event target's node, the target phase comprises only the event target node, and the bubbling phase comprises any subsequent nodes encountered on the return trip to the root of the display list.</p> <p>In general, the easiest way for a user-defined class to gain event dispatching capabilities is to extend EventDispatcher. If this is impossible (that is, if the class is already extending another class), you can instead implement the IEventDispatcher interface, create an EventDispatcher member, and write simple hooks to route calls into the aggregated EventDispatcher.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS2db454920e96a9e51e63e3d11c0bf69084-7ce1.html" target="_blank">Manually dispatching events</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS2db454920e96a9e51e63e3d11c0bf69084-7ce0.html" target="_blank">Using event subclasses</a>
	 * </div><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS2db454920e96a9e51e63e3d11c0bf64a29-7fe5.html" target="_blank">About the EventDispatcher class</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS2db454920e96a9e51e63e3d11c0bf69084-7cdb.html" target="_blank">Event propagation</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS2db454920e96a9e51e63e3d11c0bf69084-7cda.html" target="_blank">Event priorities</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS2db454920e96a9e51e63e3d11c0bf64a29-7fdb.html" target="_blank">About keyboard events</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7dfb.html" target="_blank">Handling events for display objects</a>
	 *  <br>
	 * </div><br><hr>
	 */
	public class EventDispatcher implements IEventDispatcher {
		private const listners:Object = {};
		
		private var _target:IEventDispatcher;
		
		/**
		 * <p> Aggregates an instance of the EventDispatcher class. </p>
		 * <p>The EventDispatcher class is generally used as a base class, which means that <span>most</span> developers do not need to use this constructor function. <span>However, advanced developers who are implementing the IEventDispatcher interface need to use this constructor. If you are unable to extend the EventDispatcher class and must instead implement the IEventDispatcher interface, use this constructor to aggregate an instance of the EventDispatcher class.</span></p>
		 * 
		 * @param target  — The target object for events dispatched to the EventDispatcher object. <span>This parameter is used when the EventDispatcher instance is aggregated by a class that implements IEventDispatcher; it is necessary so that the containing object can be the target for events. Do not use this parameter in simple cases in which a class extends EventDispatcher.</span> 
		 */
		public function EventDispatcher(target:IEventDispatcher = null) {
			if (target == null)
				target = this;
			this._target = target;
		}

		/**
		 * <p> Registers an event listener object with an EventDispatcher object so that the listener receives notification of an event. <span>You can register event listeners on all nodes in the display list for a specific type of event, phase, and priority.</span> </p>
		 * <p>After you successfully register an event listener, you cannot change its priority through additional calls to <code>addEventListener()</code>. To change a listener's priority, you must first call <code>removeListener()</code>. Then you can register the listener again with the new priority level. </p>
		 * <p>Keep in mind that after the listener is registered, subsequent calls to <code>addEventListener()</code> with a different <code>type</code> or <code>useCapture</code> value result in the creation of a separate listener registration. <span>For example, if you first register a listener with <code>useCapture</code> set to <code>true</code>, it listens only during the capture phase. If you call <code>addEventListener()</code> again using the same listener object, but with <code>useCapture</code> set to <code>false</code>, you have two separate listeners: one that listens during the capture phase and another that listens during the target and bubbling phases.</span> </p>
		 * <p>You cannot register an event listener for only the target phase or the bubbling phase. Those phases are coupled during registration because bubbling applies only to the ancestors of the target node.</p>
		 * <p>If you no longer need an event listener, remove it by calling <code>removeEventListener()</code>, or memory problems could result. Event listeners are not automatically removed from memory because the garbage collector does not remove the listener as long as the dispatching object exists (unless the <code>useWeakReference</code> parameter is set to <code>true</code>).</p>
		 * <p>Copying an EventDispatcher instance does not copy the event listeners attached to it. (If your newly created node needs an event listener, you must attach the listener after creating the node.) However, if you move an EventDispatcher instance, the event listeners attached to it move along with it.</p>
		 * <p>If the event listener is being registered on a node while an event is being processed on this node, the event listener is not triggered during the current phase but can be triggered during a later phase in the event flow, such as the bubbling phase.</p>
		 * <p>If an event listener is removed from a node while an event is being processed on the node, it is still triggered by the current actions. After it is removed, the event listener is never invoked again (unless registered again for future processing). </p>
		 * 
		 * @param type  — The type of event. 
		 * @param listener  — The listener function that processes the event. This function must accept an Event object as its only parameter and must return nothing<span>, as this example shows:</span> <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>function(evt:Event):void</pre>
		 * </div> <p>The function can have any name.</p> 
		 * @param useCapture  — <span>Determines whether the listener works in the capture phase or the target and bubbling phases. If <code>useCapture</code> is set to <code>true</code>, the listener processes the event only during the capture phase and not in the target or bubbling phase. If <code>useCapture</code> is <code>false</code>, the listener processes the event only during the target or bubbling phase. To listen for the event in all three phases, call <code>addEventListener</code> twice, once with <code>useCapture</code> set to <code>true</code>, then again with <code>useCapture</code> set to <code>false</code>.</span> 
		 * @param priority  — The priority level of the event listener. The priority is designated by a signed 32-bit integer. The higher the number, the higher the priority. All listeners with priority <i>n</i> are processed before listeners of priority <i>n</i>-1. If two or more listeners share the same priority, they are processed in the order in which they were added. The default priority is 0. 
		 * @param useWeakReference  — Determines whether the reference to the listener is strong or weak. A strong reference (the default) prevents your listener from being garbage-collected. A weak reference does not. <p>Class-level member functions are not subject to garbage collection, so you can set <code>useWeakReference</code> to <code>true</code> for class-level member functions without subjecting them to garbage collection. If you set <code>useWeakReference</code> to <code>true</code> for a listener that is a nested inner function, the function will be garbage-collected and no longer persistent. If you create references to the inner function (save it in another variable) then it is not garbage-collected and stays persistent.</p> 
		 */
		public function addEventListener(type:String, listener:Function, useCapture:Boolean = false, priority:int = 0, useWeakReference:Boolean = false):void {
			if (type == null || listener == null)
				return;
			if (!listners[type])
				listners[type] = new Vector.<Function>();
			//TODO: implement useCapture, priority ans useWeakReference
			listners[type].push(listener);
		}

		/**
		 * <p> Dispatches an event into the event flow. The event target is the EventDispatcher object upon which the <code>dispatchEvent()</code> method is called. </p>
		 * 
		 * @param event  — The Event object that is dispatched into the event flow. If the event is being redispatched, a clone of the event is created automatically. After an event is dispatched, its <code>target</code> property cannot be changed, so you must create a new copy of the event for redispatching to work. 
		 * @return  — A value of  if the event was successfully dispatched. A value of  indicates failure or that  was called on the event. 
		 */
		public function dispatchEvent(event:flash.events.Event):Boolean {
			var l:Array = listners[event.type];
			event.setTarget(_target);
			event.setCurrentTarget(_target);
			event.setEventPhase(EventPhase.AT_TARGET);
			if (l) {
				for each (var fx:Function in l) {
					try {
						fx(event);
					}
					catch (err:Error) {
						console.error(err);
					}
				}
			}
			return event.isDefaultPrevented;
		}

		/**
		 * <p> Checks whether the EventDispatcher object has any listeners registered for a specific type of event. This allows you to determine where an EventDispatcher object has altered handling of an event type in the event flow hierarchy. To determine whether a specific event type actually triggers an event listener, use <code>willTrigger()</code>. </p>
		 * <p>The difference between <code>hasEventListener()</code> and <code>willTrigger()</code> is that <code>hasEventListener()</code> examines only the object to which it belongs, whereas <code>willTrigger()</code> examines the entire event flow for the event specified by the <code>type</code> parameter. </p>
		 * <p>When <code>hasEventListener()</code> is called from a LoaderInfo object, only the listeners that the caller can access are considered.</p>
		 * 
		 * @param type  — The type of event. 
		 * @return  — A value of  if a listener of the specified type is registered;  otherwise. 
		 */
		public function hasEventListener(type:String):Boolean {
			return listners[type] && listners[type].length;
		}

		/**
		 * <p> Removes a listener from the EventDispatcher object. If there is no matching listener registered with the EventDispatcher object, a call to this method has no effect. </p>
		 * 
		 * @param type  — The type of event. 
		 * @param listener  — The listener object to remove. 
		 * @param useCapture  — <span>Specifies whether the listener was registered for the capture phase or the target and bubbling phases. If the listener was registered for both the capture phase and the target and bubbling phases, two calls to <code>removeEventListener()</code> are required to remove both, one call with <code>useCapture()</code> set to <code>true</code>, and another call with <code>useCapture()</code> set to <code>false</code>.</span> 
		 */
		public function removeEventListener(type:String, listener:Function, useCapture:Boolean = false):void {
			if (type == null || listener == null || !listners[type])
				return;
			var pos:int = listners[type].indexOf(listener);
			if (pos==-1)
				return;
			//TODO: implement useCapture, priority ans useWeakReference
			listners[type].splice(pos, 1);
		}

		/**
		 * <p> Checks whether an event listener is registered with this EventDispatcher object or any of its ancestors for the specified event type. This method returns <code>true</code> if an event listener is triggered during any phase of the event flow when an event of the specified type is dispatched to this EventDispatcher object or any of its descendants. </p>
		 * <p>The difference between the <code>hasEventListener()</code> and the <code>willTrigger()</code> methods is that <code>hasEventListener()</code> examines only the object to which it belongs, whereas the <code>willTrigger()</code> method examines the entire event flow for the event specified by the <code>type</code> parameter. </p>
		 * <p>When <code>willTrigger()</code> is called from a LoaderInfo object, only the listeners that the caller can access are considered.</p>
		 * 
		 * @param type  — The type of event. 
		 * @return  — A value of  if a listener of the specified type will be triggered;  otherwise. 
		 */
		public function willTrigger(type:String):Boolean {
			throw new Error("Not implemented");
		}

		public function toString():String {
			throw new Error("Not implemented");
		}
	}
}
