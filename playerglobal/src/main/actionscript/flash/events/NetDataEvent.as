package flash.events {
	/**
	 *  A NetStream object dispatches a NetDataEvent object when a data message is encountered in the media stream. <p>A NetDataEvent is dispatched for the following messages:</p> <ul> 
	 *  <li>onCuePoint</li> 
	 *  <li>onImageData</li> 
	 *  <li>onMetaData</li> 
	 *  <li>onPlayStatus (for code NetStream.Play.Complete)</li> 
	 *  <li>onTextData</li> 
	 *  <li>onXMPData</li> 
	 * </ul> <br><hr>
	 */
	public class NetDataEvent extends flash.events.Event {
		/**
		 * <p> The <code>NetDataEvent.MEDIA_TYPE_DATA</code> constant defines the value of the <code>type</code> property of the NetDataEvent object dispatched when a data message in the media stream is encountered by the NetStream object. </p>
		 */
		public static const MEDIA_TYPE_DATA:String = "mediaTypeData";

		private var _info:Object;
		private var _timestamp:Number;

		/**
		 * <p> Creates an event object that contains information about media data events. Event objects are passed as parameters to Event listeners. </p>
		 * 
		 * @param type  — The type of the event. Event listeners can access this information through the inherited <code>type</code> property. 
		 * @param bubbles  — Determines whether the Event object participates in the bubbling phase of the event flow. Event listeners can access this information through the inherited <code>bubbles</code> property. 
		 * @param cancelable  — Determines whether the Event object can be canceled. Event listeners can access this information through the inherited <code>cancelable</code> property. 
		 * @param timestamp  — timestamp of the data message 
		 * @param info  — data message object 
		 */
		public function NetDataEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, timestamp:Number = 0, info:Object = null) {
			super(type, bubbles, cancelable);
			throw new Error("Not implemented");
		}

		/**
		 * <p> A data object describing the message. The <code>info</code> object has two properties: <code>info.handler</code> and <code>info.args</code>. <code>info.handler</code> is the handler name, such as "onMetaData" or "onXMPData". <code>info.args</code> is an array of arguments. </p>
		 * 
		 * @return 
		 */
		public function get info():Object {
			return _info;
		}

		/**
		 * <p> The timestamp of the data message in the media stream. </p>
		 * 
		 * @return 
		 */
		public function get timestamp():Number {
			return _timestamp;
		}

		/**
		 * <p> Creates a copy of an NetDataEvent object and sets the value of each property to match that of the original. </p>
		 * 
		 * @return  — A new NetDataEvent object with property values that match those of the original. 
		 */
		override public function clone():flash.events.Event {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a string that contains all the properties of the NetDataEvent object. The following format is used: </p>
		 * <p><code>[NetDataEvent type=<i>value</i> bubbles=<i>value</i> cancelable=<i>value</i> timestamp=<i>value</i>]</code></p>
		 * 
		 * @return  — A string that contains all the properties of the NetMediaEvent object. 
		 */
		override public function toString():String {
			throw new Error("Not implemented");
		}
	}
}
