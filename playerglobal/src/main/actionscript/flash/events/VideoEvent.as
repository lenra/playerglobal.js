package flash.events {
	/**
	 *  <p>This event class reports the current video rendering status. Use this event for the following purposes:</p> <ul> 
	 *  <li>To find out when size of the Video display changes or is initialized. Use this event instead of polling for size changes. When you receive this event you can access <code>Video.videoSize</code> and <code>Video.videoHeight</code> to get the pixel dimensions of the video that is currently playing.</li> 
	 *  <li>To find out whether the video is decoded by software or the GPU. If the <code>status</code> property returns "accelerated", you should switch to using the StageVideo class, if possible. </li> 
	 * </ul> <p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e1a.html" target="_blank">Working with Video</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="StageVideoEvent.html" target="">flash.events.StageVideoEvent</a>
	 *  <br>
	 *  <a href="StageVideoAvailabilityEvent.html" target="">flash.events.StageVideoAvailabilityEvent</a>
	 *  <br>
	 *  <a href="../../flash/display/Stage.html#stageVideos" target="">flash.display.Stage.stageVideos</a>
	 *  <br>
	 *  <a href="../../flash/media/Video.html" target="">flash.media.Video</a>
	 *  <br>
	 *  <a href="../../flash/net/NetStream.html" target="">flash.net.NetStream</a>
	 * </div><br><hr>
	 */
	public class VideoEvent extends flash.events.Event {
		/**
		 * <p> Defines the value of the <code>type</code> property of a <code>renderState</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>status</code></td>
		 *    <td>The rendering status reported by the event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The Video object reporting rendering status.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 */
		public static const RENDER_STATE:String = "renderState";
		/**
		 * <p> </p>
		 * <p>For internal use only. Use flash.media.VideoStatus.ACCELERATED instead.</p>
		 */
		public static const RENDER_STATUS_ACCELERATED:String = "accelerated";
		/**
		 * <p> </p>
		 * <p>For internal use only. Use flash.media.VideoStatus.SOFTWARE instead.</p>
		 */
		public static const RENDER_STATUS_SOFTWARE:String = "software";
		/**
		 * <p> </p>
		 * <p>For internal use only. Use flash.media.VideoStatus.UNAVAILABLE instead.</p>
		 */
		public static const RENDER_STATUS_UNAVAILABLE:String = "unavailable";

		public const codecInfo:String = "condecInfo";

		private var _status:String;

		/**
		 * <p> </p>
		 * <p>Constructor.</p>
		 * 
		 * @param type  — The type of event. Possible values are: <code>VideoEvent.RENDER_STATE</code>. 
		 * @param bubbles  — Indicates whether this Event object participates in the bubbling stage of the event flow. 
		 * @param cancelable  — Indicates whether you can cancel the action that triggers this event. 
		 * @param status  — The rendering state of the video. 
		 */
		public function VideoEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, status:String = null) {
			super(type, bubbles, cancelable);
			throw new Error("Not implemented");
		}

		/**
		 * <p> </p>
		 * <p>Returns the rendering status of the VideoEvent object. Possible values include "unavailable", "software", and "accelerated".</p>
		 * 
		 * @return 
		 */
		public function get status():String {
			return _status;
		}
	}
}
