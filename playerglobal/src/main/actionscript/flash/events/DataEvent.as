package flash.events {
	/**
	 *  An object dispatches a DataEvent object when raw data has completed loading. There are two types of data event: <ul> 
	 *  <li> <code>DataEvent.DATA</code>: dispatched for data sent or received.</li> 
	 *  <li> <code>DataEvent.UPLOAD_COMPLETE_DATA</code>: dispatched when data is sent and the server has responded.</li> 
	 * </ul> <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../flash/net/FileReference.html" target="">flash.net.FileReference</a>
	 *  <br>
	 *  <a href="../../flash/net/XMLSocket.html" target="">flash.net.XMLSocket</a>
	 * </div><br><hr>
	 */
	public class DataEvent extends TextEvent {
		/**
		 * <p> Defines the value of the <code>type</code> property of a <code>data</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>data</code></td>
		 *    <td>The raw data loaded into Flash Player or Adobe AIR.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The XMLSocket object receiving data.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/net/XMLSocket.html#event:data" target="">flash.net.XMLSocket.data</a>
		 * </div>
		 */
		public static const DATA:String = "data";
		/**
		 * <p> Defines the value of the <code>type</code> property of an <code>uploadCompleteData</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>data</code></td>
		 *    <td>The raw data returned from the server after a successful file upload.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The FileReference object receiving data after a successful upload.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/net/FileReference.html#event:uploadCompleteData" target="">flash.net.FileReference.uploadCompleteData</a>
		 * </div>
		 */
		public static const UPLOAD_COMPLETE_DATA:String = "uploadCompleteData";

		private var _data:String;

		/**
		 * <p> Creates an event object that contains information about data events. Event objects are passed as parameters to event listeners. </p>
		 * 
		 * @param type  — The type of the event. Event listeners can access this information through the inherited <code>type</code> property. There is only one type of data event: <code>DataEvent.DATA</code>. 
		 * @param bubbles  — Determines whether the Event object participates in the bubbling phase of the event flow. Event listeners can access this information through the inherited <code>bubbles</code> property. 
		 * @param cancelable  — Determines whether the Event object can be canceled. Event listeners can access this information through the inherited <code>cancelable</code> property. 
		 * @param data  — The raw data loaded into Flash Player or Adobe AIR. Event listeners can access this information through the <code>data</code> property. 
		 */
		public function DataEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, data:String = "") {
			super(type, bubbles, cancelable, text);
			throw new Error("Not implemented");
		}

		/**
		 * <p> The raw data loaded into Flash Player or Adobe AIR. </p>
		 * 
		 * @return 
		 */
		public function get data():String {
			return _data;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set data(value:String):void {
			_data = value;
		}

		/**
		 * <p> Creates a copy of the DataEvent object and sets the value of each property to match that of the original. </p>
		 * 
		 * @return  — A new DataEvent object with property values that match those of the original. 
		 */
		override public function clone():flash.events.Event {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a string that contains all the properties of the DataEvent object. The string is in the following format: </p>
		 * <p><code>[DataEvent type=<i>value</i> bubbles=<i>value</i> cancelable=<i>value</i> data=<i>value</i>]</code></p>
		 * 
		 * @return  — A string that contains all the properties of the DataEvent object. 
		 */
		override public function toString():String {
			throw new Error("Not implemented");
		}
	}
}
