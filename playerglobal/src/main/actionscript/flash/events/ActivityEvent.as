package flash.events {
	/**
	 *  A Camera or Microphone object dispatches an ActivityEvent object whenever a camera or microphone reports that it has become active or inactive. There is only one type of activity event: <code>ActivityEvent.ACTIVITY</code>. <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="ActivityEvent.html#ACTIVITY" target="">ActivityEvent.ACTIVITY</a>
	 * </div><br><hr>
	 */
	public class ActivityEvent extends flash.events.Event {
		/**
		 * <p> The <code>ActivityEvent.ACTIVITY</code> constant defines the value of the <code>type</code> property of an <code>activity</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>activating</code></td>
		 *    <td><code>true</code> if the device is activating or <code>false</code> if it is deactivating.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The object beginning or ending a session, such as a Camera or Microphone object.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/media/Camera.html#event:activity" target="">flash.media.Camera.activity</a>
		 *  <br>
		 *  <a href="../../flash/media/Microphone.html#event:activity" target="">flash.media.Microphone.activity</a>
		 * </div>
		 */
		public static const ACTIVITY:String = "activity";

		private var _activating:Boolean;

		/**
		 * <p> Creates an event object that contains information about activity events. Event objects are passed as parameters to Event listeners. </p>
		 * 
		 * @param type  — The type of the event. Event listeners can access this information through the inherited <code>type</code> property. There is only one type of activity event: <code>ActivityEvent.ACTIVITY</code>. 
		 * @param bubbles  — Determines whether the Event object participates in the bubbling phase of the event flow. Event listeners can access this information through the inherited <code>bubbles</code> property. 
		 * @param cancelable  — Determines whether the Event object can be canceled. Event listeners can access this information through the inherited <code>cancelable</code> property. 
		 * @param activating  — Indicates whether the device is activating (<code>true</code>) or deactivating (<code>false</code>). Event listeners can access this information through the <code>activating</code> property. 
		 */
		public function ActivityEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, activating:Boolean = false) {
			super(type, bubbles, cancelable);
			throw new Error("Not implemented");
		}

		/**
		 * <p> Indicates whether the device is activating (<code>true</code>) or deactivating (<code>false</code>). </p>
		 * 
		 * @return 
		 */
		public function get activating():Boolean {
			return _activating;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set activating(value:Boolean):void {
			_activating = value;
		}

		/**
		 * <p> Creates a copy of an ActivityEvent object and sets the value of each property to match that of the original. </p>
		 * 
		 * @return  — A new ActivityEvent object with property values that match those of the original. 
		 */
		override public function clone():flash.events.Event {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a string that contains all the properties of the ActivityEvent object. The following format is used: </p>
		 * <p><code>[ActivityEvent type=<i>value</i> bubbles=<i>value</i> cancelable=<i>value</i> activating=<i>value</i>]</code></p>
		 * 
		 * @return  — A string that contains all the properties of the ActivityEvent object. 
		 */
		override public function toString():String {
			throw new Error("Not implemented");
		}
	}
}
