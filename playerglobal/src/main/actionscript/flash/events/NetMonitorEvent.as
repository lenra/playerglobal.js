package flash.events {
	import flash.net.NetStream;

	/**
	 *  A NetMonitor object dispatches NetMonitorEvent objects when a NetStream object is created. <br><hr>
	 */
	public class NetMonitorEvent extends flash.events.Event {
		/**
		 * <p> The <code>NetMonitorEvent.NET_STREAM_CREATE</code> constant defines the value of the <code>type</code> property of an <code>netStreamCreate</code> event object. </p>
		 * <p>The <code>netStreamCreate</code> event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>netStream</code></td>
		 *    <td>NetStream object that has been created.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The object beginning or ending a session.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 */
		public static const NET_STREAM_CREATE:String = "netStreamCreate";

		private var _netStream:NetStream;

		/**
		 * <p> Creates an event object that contains information about netStreamCreate events. Event objects are passed as parameters to Event listeners. </p>
		 * 
		 * @param type  — The type of the event. Event listeners can access this information through the inherited <code>type</code> property. There is only one type of event: <code>NetMonitorEvent.NET_STREAM_CREATE</code>. 
		 * @param bubbles  — Determines whether the Event object participates in the bubbling phase of the event flow. Event listeners can access this information through the inherited <code>bubbles</code> property. 
		 * @param cancelable  — Determines whether the Event object can be canceled. Event listeners can access this information through the inherited <code>cancelable</code> property. 
		 * @param netStream  — The new NetStream object that has been created. Event listeners can access this information through the <code>netStream</code> property. 
		 */
		public function NetMonitorEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, netStream:NetStream = null) {
			super(type, bubbles, cancelable);
			throw new Error("Not implemented");
		}

		/**
		 * <p> The new NetStream object. </p>
		 * 
		 * @return 
		 */
		public function get netStream():NetStream {
			return _netStream;
		}

		/**
		 * <p> Creates a copy of an NetMonitorEvent object and sets the value of each property to match that of the original. </p>
		 * 
		 * @return  — A new NetMonitorEvent object with property values that match those of the original. 
		 */
		override public function clone():flash.events.Event {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a string that contains all the properties of the NetMonitorEvent object. The following format is used: </p>
		 * <p><code>[NetMonitorEvent type=<i>value</i> bubbles=<i>value</i> cancelable=<i>value</i> netStream=<i>value</i>]</code></p>
		 * 
		 * @return  — A string that contains all the properties of NetMonitorEvent object. 
		 */
		override public function toString():String {
			throw new Error("Not implemented");
		}
	}
}
