package flash.events {
	/**
	 *  The ThrottleType class provides values for the playback <code>state</code> property of the flash.event.ThrottleEvent class. <p>The platforms that support throttling and pausing are currently the following: Flash Player Desktop Mac and Windows, AIR Mobile, and Flash Player Android. The following platforms do not dispatch the ThrottleEvent automatically because they do not yet support pausing or throttling: AIR for TV devices, AIR for desktop, and Flash Player Linux Desktop.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="ThrottleEvent.html" target="">flash.events.ThrottleEvent</a>
	 * </div><br><hr>
	 */
	public class ThrottleType {
		/**
		 * <p> This constant is used for the <code>status</code> property in the ThrottleEvent class. Use the syntax <code>ThrottleType.PAUSE</code>. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="ThrottleEvent.html" target="">flash.events.ThrottleEvent</a>
		 * </div>
		 */
		public static const PAUSE:String = "pause";
		/**
		 * <p> This constant is used for the <code>status</code> property in the ThrottleEvent class. Use the syntax <code>ThrottleType.RESUME</code>. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="ThrottleEvent.html" target="">flash.events.ThrottleEvent</a>
		 * </div>
		 */
		public static const RESUME:String = "resume";
		/**
		 * <p> This constant is used for the <code>state</code> property in the ThrottleEvent class. Use the syntax <code>ThrottleType.THROTTLE</code>. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="ThrottleEvent.html" target="">flash.events.ThrottleEvent</a>
		 * </div>
		 */
		public static const THROTTLE:String = "throttle";
	}
}
