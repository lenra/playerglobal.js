package flash.events {
	/**
	 *  Almost exactly StageVideoEvent. <br><hr>
	 */
	public class VideoTextureEvent extends flash.events.Event {
		/**
		 * <p> The <code>VideoTextureEvent.RENDER_STATE</code> constant defines the value of the <code>type</code> property of a <code>renderState</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>colorSpace</code></td>
		 *    <td>The available color spaces for displaying the video.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the StageVideoEvent object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>status</code></td>
		 *    <td>Indicates whether the video is being rendered (decoded and displayed) by hardware or software, or not at all.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The VideoTexture object that changed state.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="StageVideoEvent.html#RENDER_STATE" target="">flash.events.StageVideoEvent.RENDER_STATE</a>
		 *  <br>
		 *  <a href="StageVideoEvent.html#RENDER_STATUS_UNAVAILABLE" target="">flash.events.StageVideoEvent.RENDER_STATUS_UNAVAILABLE</a>
		 *  <br>
		 *  <a href="StageVideoEvent.html#RENDER_STATUS_SOFTWARE" target="">flash.events.StageVideoEvent.RENDER_STATUS_SOFTWARE</a>
		 *  <br>
		 *  <a href="StageVideoEvent.html#RENDER_STATUS_ACCELERATED" target="">flash.events.StageVideoEvent.RENDER_STATUS_ACCELERATED</a>
		 * </div>
		 */
		public static const RENDER_STATE:String = "renderState";

		private var _colorSpace:String;
		private var _status:String;

		/**
		 * @param type
		 * @param bubbles
		 * @param cancelable
		 * @param status
		 * @param colorSpace
		 */
		public function VideoTextureEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, status:String = null, colorSpace:String = null) {
			super(type, bubbles, cancelable);
			throw new Error("Not implemented");
		}

		/**
		 * <p> The color space used by the video being displayed in the VideoTexture object. </p>
		 * 
		 * @return 
		 */
		public function get colorSpace():String {
			return _colorSpace;
		}

		/**
		 * <p> The status of the VideoTexture object. </p>
		 * 
		 * @return 
		 */
		public function get status():String {
			return _status;
		}
	}
}
