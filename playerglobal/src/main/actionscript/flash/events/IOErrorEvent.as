package flash.events {
	/**
	 *  An IOErrorEvent object is dispatched when an error causes input or output operations to fail. <p>You can check for error events that do not have any listeners by using the debugger version of Flash Player or the AIR Debug Launcher (ADL). The string defined by the <code>text</code> parameter of the IOErrorEvent constructor is displayed.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="IOErrorEvent.html#IO_ERROR" target="">IO_ERROR</a>
	 * </div><br><hr>
	 */
	public class IOErrorEvent extends ErrorEvent {
		/**
		 * <p> Defines the value of the <code>type</code> property of an <code>ioError</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>errorID</code></td>
		 *    <td>A reference number associated with the specific error (AIR only).</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The network object experiencing the input/output error.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>text</code></td>
		 *    <td>Text to be displayed as an error message.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/LoaderInfo.html#event:ioError" target="">flash.display.LoaderInfo.ioError</a>
		 *  <br>
		 *  <a href="../../flash/media/Sound.html#event:ioError" target="">flash.media.Sound.ioError</a>
		 *  <br>
		 *  <a href="../../flash/net/SecureSocket.html#event:ioError" target="">flash.net.SecureSocket.ioError</a>
		 *  <br>
		 *  <a href="../../flash/net/Socket.html#event:ioError" target="">flash.net.Socket.ioError</a>
		 *  <br>
		 *  <a href="../../flash/net/FileReference.html#event:ioError" target="">flash.net.FileReference.ioError</a>
		 *  <br>
		 *  <a href="../../flash/net/NetConnection.html#event:ioError" target="">flash.net.NetConnection.ioError</a>
		 *  <br>
		 *  <a href="../../flash/net/NetStream.html#event:ioError" target="">flash.net.NetStream.ioError</a>
		 *  <br>
		 *  <a href="../../flash/net/URLLoader.html#event:ioError" target="">flash.net.URLLoader.ioError</a>
		 *  <br>
		 *  <a href="../../flash/net/URLStream.html#event:ioError" target="">flash.net.URLStream.ioError</a>
		 *  <br>
		 *  <a href="../../flash/net/XMLSocket.html#event:ioError" target="">flash.net.XMLSocket.ioError</a>
		 * </div>
		 */
		public static const IO_ERROR:String = "ioError";
		/**
		 * <p> The <code>standardErrorIoError</code> event is dispatched when an error occurs while reading data from the standardError stream of a NativeProcess object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td>No.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td>No. There is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>errorID</code></td>
		 *    <td>The reference number associated with the specific error.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The object on which the error occurred.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>text</code></td>
		 *    <td>Text to be displayed as an error message.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 */
		public static const STANDARD_ERROR_IO_ERROR:String = "standardErrorIoError";
		/**
		 * <p> The <code>standardInputIoError</code> event is dispatched when an error occurs while writing data to the standardInput of a NativeProcess object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td>No.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td>No. There is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>errorID</code></td>
		 *    <td>The reference number associated with the specific error.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The object on which the error occurred.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>text</code></td>
		 *    <td>Text to be displayed as an error message.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 */
		public static const STANDARD_INPUT_IO_ERROR:String = "standardInputIoError";
		/**
		 * <p> The <code>standardOutputIoError</code> event is dispatched when an error occurs while reading data from the standardOutput stream of a NativeProcess object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td>No.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td>No. There is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>errorID</code></td>
		 *    <td>The reference number associated with the specific error.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The object on which the error occurred.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>text</code></td>
		 *    <td>Text to be displayed as an error message.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 */
		public static const STANDARD_OUTPUT_IO_ERROR:String = "standardOutputIoError";


		public static const NETWORK_ERROR:String = "networkError";

		/**
		 * <p> Creates an Event object that contains specific information about <code>ioError</code> events. Event objects are passed as parameters to Event listeners. </p>
		 * 
		 * @param type  — The type of the event. Event listeners can access this information through the inherited <code>type</code> property. There is only one type of input/output error event: <code>IOErrorEvent.IO_ERROR</code>. 
		 * @param bubbles  — Determines whether the Event object participates in the bubbling stage of the event flow. Event listeners can access this information through the inherited <code>bubbles</code> property. 
		 * @param cancelable  — Determines whether the Event object can be canceled. Event listeners can access this information through the inherited <code>cancelable</code> property. 
		 * @param text  — Text to be displayed as an error message. Event listeners can access this information through the <code>text</code> property. 
		 * @param id  — A reference number to associate with the specific error (supported in Adobe AIR only). 
		 */
		public function IOErrorEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, text:String = "", id:int = 0) {
			super(type, bubbles, cancelable, text, id);
			throw new Error("Not implemented");
		}

		/**
		 * <p> Creates a copy of the IOErrorEvent object and sets the value of each property to match that of the original. </p>
		 * 
		 * @return  — A new IOErrorEvent object with property values that match those of the original. 
		 */
		override public function clone():flash.events.Event {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a string that contains all the properties of the IOErrorEvent object. The string is in the following format: </p>
		 * <p><code>[IOErrorEvent type=<i>value</i> bubbles=<i>value</i> cancelable=<i>value</i> text=<i>value</i> errorID=<i>value</i>]</code> The <code>errorId</code> is only available in Adobe AIR</p>
		 * 
		 * @return  — A string that contains all the properties of the IOErrorEvent object. 
		 */
		override public function toString():String {
			throw new Error("Not implemented");
		}
	}
}
