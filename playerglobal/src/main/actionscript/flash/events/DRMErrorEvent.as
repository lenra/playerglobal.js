package flash.events {
	import flash.net.drm.DRMContentData;

	/**
	 *  The DRMErrorEvent class provides information about errors that occur when playing digital rights management (DRM) encrypted files. <p>The runtime dispatches a DRMErrorEvent object when a NetStream object, trying to play a digital rights management (DRM) encrypted file, encounters a DRM-related error. For example, a DRMErrorEvent object is dispatched when the content provider does not support the viewing application, or when the user authorization fails, possibly because the user has not purchased the content.</p> <p> In the case of invalid user credentials, the DRMAuthenticateEvent object handles the error by repeatedly dispatching until the user enters valid credentials, or the application denies further attempts. The application should listen to any other DRM error events in order to detect, identify, and handle the DRM-related errors. </p> <p> This class provides properties containing the object throwing the exception, the error code, and, where applicable, a suberror code and text message containing information related to the error. For a description of DRM-related error codes, see the <a href="../../runtimeErrors.html">Runtime error codes</a>. The DRM-related error codes start at error 3300. </p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118666ade46-7cd8.html" target="_blank">Using the DRMErrorEvent class</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../flash/net/NetStream.html" target="">flash.net.NetStream</a>
	 *  <br>
	 *  <a href="DRMErrorEvent.html#DRM_ERROR" target="">DRMErrorEvent.DRM_ERROR</a>
	 *  <br>
	 *  <a href="../../runtimeErrors.html" target="">Runtime error codes</a>
	 * </div><br><hr>
	 */
	public class DRMErrorEvent extends ErrorEvent {
		/**
		 * <p> The <code>DRMErrorEvent.DRM_ERROR</code> constant defines the value of the <code>type</code> property of a <code>drmError</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>errorID</code></td>
		 *    <td>A numerical error code assigned to the problem.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>subErrorID</code></td>
		 *    <td>An error code that indicates more detailed information about the underlying problem.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The NetStream object.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/net/NetStream.html#event:drmError" target="">flash.net.NetStream.drmError</a>
		 * </div>
		 */
		public static const DRM_ERROR:String = "drmError";
		public static const DRM_LOAD_DEVICEID_ERROR:String = "drmLoadDeviceIdError";

		private var _contentData:DRMContentData;

		private var _drmUpdateNeeded:Boolean;
		private var _subErrorID:int;
		private var _systemUpdateNeeded:Boolean;

		/**
		 * <p> Creates an Event object that contains specific information about DRM error events. Event objects are passed as parameters to event listeners. </p>
		 * 
		 * @param type  — The type of the event. Event listeners can access this information through the inherited <code>type</code> property. There is only one type of DRMAuthenticate event: <code>DRMAuthenticateEvent.DRM_AUTHENTICATE</code>. 
		 * @param bubbles  — Determines whether the Event object participates in the bubbling stage of the event flow. Event listeners can access this information through the inherited <code>bubbles</code> property. 
		 * @param cancelable  — Determines whether the Event object can be canceled. Event listeners can access this information through the inherited <code>cancelable</code> property. 
		 * @param inErrorDetail  — Where applicable, the specific syntactical details of the error. 
		 * @param inErrorCode  — The major error code. 
		 * @param insubErrorID  — The minor error ID. 
		 * @param inMetadata
		 * @param inSystemUpdateNeeded
		 * @param inDrmUpdateNeeded
		 */
		public function DRMErrorEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, inErrorDetail:String = "", inErrorCode:int = 0, insubErrorID:int = 0, inMetadata:DRMContentData = null, inSystemUpdateNeeded:Boolean = false, inDrmUpdateNeeded:Boolean = false) {
			super(type, bubbles, cancelable, text);
			throw new Error("Not implemented");
		}

		/**
		 * <p> The DRMContentData for the media file. </p>
		 * <p>You can use the object referenced by the <code>contentData</code> property to retrieve the related DRM voucher from the DRMManager voucher cache. The voucher properties describe the license available to the user and may explain why the DRM-protected content cannot be viewed.</p>
		 * 
		 * @return 
		 */
		public function get contentData():DRMContentData {
			return _contentData;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set contentData(value:DRMContentData):void {
			_contentData = value;
		}

		/**
		 * <p> Indicates whether a DRM update is needed to play the DRM-protected content. </p>
		 * 
		 * @return 
		 */
		public function get drmUpdateNeeded():Boolean {
			return _drmUpdateNeeded;
		}

		/**
		 * <p> An error ID that indicates more detailed information about the underlying problem. </p>
		 * 
		 * @return 
		 */
		public function get subErrorID():int {
			return _subErrorID;
		}

		/**
		 * <p> Indicates whether a system update is needed to play the DRM-protected content. </p>
		 * 
		 * @return 
		 */
		public function get systemUpdateNeeded():Boolean {
			return _systemUpdateNeeded;
		}

		/**
		 * <p> Creates a copy of the DRMErrorEvent object and sets the value of each property to match that of the original. </p>
		 * 
		 * @return  — A new DRMErrorEvent object with property values that match those of the original. 
		 */
		override public function clone():flash.events.Event {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a string that contains all the properties of the DRMErrorEvent object. The string is in the following format: </p>
		 * <p><code>[DRMErrorEvent type=<i>value</i> bubbles=<i>value</i> cancelable=<i>value</i> eventPhase=<i>value</i> errroID=<i>value</i> subErrorID=<i>value</i> text=<i>value</i></code></p>
		 * 
		 * @return  — A string that contains all the properties of the DRMErrorEvent object. 
		 */
		override public function toString():String {
			throw new Error("Not implemented");
		}
	}
}
