package flash.events {
	/**
	 *  The Stage object dispatches a FullScreenEvent object whenever the Stage enters or leaves full-screen display mode. There are two types of <code>fullScreen</code> events: <code>FullScreenEvent.FULL_SCREEN</code> and <code>FullScreenEvent.FULL_SCREEN_INTERACTIVE_ACCEPTED</code>. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../flash/display/Stage.html#displayState" target="">flash.display.Stage.displayState</a>
	 * </div><br><hr>
	 */
	public class FullScreenEvent extends ActivityEvent {
		/**
		 * <p> The <code>FullScreenEvent.FULL_SCREEN</code> constant defines the value of the <code>type</code> property of a <code>fullScreen</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>fullScreen</code></td>
		 *    <td><code>true</code> if the display state is full screen or <code>false</code> if it is normal.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The Stage object.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/Stage.html#displayState" target="">flash.display.Stage.displayState</a>
		 * </div>
		 */
		public static const FULL_SCREEN:String = "fullScreen";
		/**
		 * <p> The <code>FULL_SCREEN_INTERACTIVE_ACCEPTED:String</code> constant defines the value of the <code>type</code> property of a <code>fullScreenInteractiveAccepted</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>fullScreen</code></td>
		 *    <td><code>true</code> if the display state is full screen or <code>false</code> if it is normal.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The Stage object.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/Stage.html#displayState" target="">flash.display.Stage.displayState</a>
		 * </div>
		 */
		public static const FULL_SCREEN_INTERACTIVE_ACCEPTED:String = "fullScreenInteractiveAccepted";

		private var _fullScreen:Boolean;
		private var _interactive:Boolean;

		/**
		 * <p> Creates an event object that contains information about <code>fullScreen</code> events. Event objects are passed as parameters to event listeners. </p>
		 * 
		 * @param type  — The type of the event. Event listeners can access this information through the inherited <code>type</code> property. 
		 * @param bubbles  — Determines whether the Event object participates in the bubbling phase of the event flow. Event listeners can access this information through the inherited <code>bubbles</code> property. 
		 * @param cancelable  — Determines whether the Event object can be canceled. Event listeners can access this information through the inherited <code>cancelable</code> property. 
		 * @param fullScreen  — Indicates whether the device is activating (<code>true</code>) or deactivating (<code>false</code>). Event listeners can access this information through the <code>activating</code> property. 
		 * @param interactive
		 */
		public function FullScreenEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, fullScreen:Boolean = false, interactive:Boolean = false) {
			super(type, bubbles, cancelable, activating);
			throw new Error("Not implemented");
		}

		/**
		 * <p> Indicates whether the Stage object is in full-screen mode (<code>true</code>) or not (<code>false</code>). </p>
		 * 
		 * @return 
		 */
		public function get fullScreen():Boolean {
			return _fullScreen;
		}

		/**
		 * <p> Indicates whether the Stage object is in full-screen interactive mode (<code>true</code>) or not (<code>false</code>). </p>
		 * 
		 * @return 
		 */
		public function get interactive():Boolean {
			return _interactive;
		}

		/**
		 * <p> Creates a copy of a FullScreenEvent object and sets the value of each property to match that of the original. </p>
		 * 
		 * @return  — A new FullScreenEvent object with property values that match those of the original. 
		 */
		override public function clone():flash.events.Event {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a string that contains all the properties of the FullScreenEvent object. The following format is used: </p>
		 * <p><code>[FullScreenEvent type=<i>value</i> bubbles=<i>value</i> cancelable=<i>value</i> activating=<i>value</i>]</code></p>
		 * 
		 * @return  — A string that contains all the properties of the FullScreenEvent object. 
		 */
		override public function toString():String {
			throw new Error("Not implemented");
		}
	}
}
