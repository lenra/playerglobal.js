package flash.events {
	import flash.net.drm.DRMContentData;
	import flash.utils.ByteArray;

	/**
	 *  AVSegmentedSource dispatches DRMMetadataEvent when it loads and parses files. <br><hr>
	 */
	public class DRMMetadataEvent extends flash.events.Event {
		public static const DRM_METADATA:String = "drmMetadata";

		private var _drmMetadata:DRMContentData;
		private var _timestamp:Number;

		/**
		 * @param type  — the event type string 
		 * @param bubbles  — whether the event bubbles up the display list 
		 * @param cancelable  — whether the event can be canceled 
		 * @param inMetadata  — Raw metadata bytes. 
		 * @param inTimestamp  — inTimestamp. 
		 */
		public function DRMMetadataEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, inMetadata:ByteArray = null, inTimestamp:Number = 0) {
			super(type, bubbles, cancelable);
			throw new Error("Not implemented");
		}

		/**
		 * @return 
		 */
		public function get drmMetadata():DRMContentData {
			return _drmMetadata;
		}

		/**
		 * @return 
		 */
		public function get timestamp():Number {
			return _timestamp;
		}
	}
}
