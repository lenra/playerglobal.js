package flash.events {
	/**
	 *  A StageVideo object dispatches a StageVideoEvent object after the <code>attachNetStream()</code> method of the StageVideo object and the <code>play()</code> method of the attached NetStream object have both been called. Also, depending on the platform, any change in the playing status can result in dispatching the event. The one type of StageVideoEvent is <code>StageVideoEvent.RENDER_STATE</code>. <p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WSe9ecd9e6b89aefd2-2046179612dc3d8edf2-7fff.html" target="_blank">Using the StageVideoEvent.RENDER_STATE and VideoEvent.RENDER_STATE events</a>
	 * </div><br><hr>
	 */
	public class StageVideoEvent extends flash.events.Event {
		/**
		 * <p> The <code>StageVideoEvent.RENDER_STATE</code> constant defines the value of the <code>type</code> property of a <code>renderState</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>colorSpace</code></td>
		 *    <td>The available color spaces for displaying the video.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the StageVideoEvent object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>status</code></td>
		 *    <td>Indicates whether the video is being rendered (decoded and displayed) by hardware or software, or not at all.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The StageVideo object that changed state.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="StageVideoEvent.html#RENDER_STATE" target="">flash.events.StageVideoEvent.RENDER_STATE</a>
		 *  <br>
		 *  <a href="StageVideoEvent.html#RENDER_STATUS_UNAVAILABLE" target="">flash.events.StageVideoEvent.RENDER_STATUS_UNAVAILABLE</a>
		 *  <br>
		 *  <a href="StageVideoEvent.html#RENDER_STATUS_SOFTWARE" target="">flash.events.StageVideoEvent.RENDER_STATUS_SOFTWARE</a>
		 *  <br>
		 *  <a href="StageVideoEvent.html#RENDER_STATUS_ACCELERATED" target="">flash.events.StageVideoEvent.RENDER_STATUS_ACCELERATED</a>
		 * </div>
		 */
		public static const RENDER_STATE:String = "renderState";
		/**
		 * <p> Indicates that hardware is decoding and displaying the video. </p>
		 * <p>This value is one of the possible values of the StageVideoEvent object <code>status</code> property.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="StageVideoEvent.html#status" target="">status</a>
		 * </div>
		 */
		public static const RENDER_STATUS_ACCELERATED:String = "accelerated";
		/**
		 * <p> Indicates that software is decoding and displaying the video. </p>
		 * <p>This value is one of the possible values of the StageVideoEvent object <code>status</code> property.</p>
		 * <p>For example, if the platform does not support hardware decoding and display of the audio or video codec of the video, the StageVideoEvent object has this status value.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="StageVideoEvent.html#status" target="">status</a>
		 * </div>
		 */
		public static const RENDER_STATUS_SOFTWARE:String = "software";
		/**
		 * <p> Indicates that displaying the video using the StageVideo object is not possible. </p>
		 * <p>This value is one of the possible values of the StageVideoEvent object <code>status</code> property.</p>
		 * <p>For example, consider a platform that does not support decoding and displaying the video's audio or video codec with either software or hardware. In this case, the StageVideoEvent object has this status value.</p>
		 * <p>This status value also is used if no hardware decoders are available. This situation can occur in AIR for TV. For backward compatibility with its previous releases, AIR for TV allows you to use a Video object for hardware decoding and display. By using a Video object, you are using the underlying hardware decoder and therefore you have one less StageVideo object available for use. Note that using a StageVideo object for hardware video decoding and display is recommended.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="StageVideoEvent.html#status" target="">status</a>
		 * </div>
		 */
		public static const RENDER_STATUS_UNAVAILABLE:String = "unavailable";

		public const codecInfo:String = "codecInfo";

		private var _colorSpace:String;
		private var _status:String;

		/**
		 * <p> Creates an Event object that contains information about StageVideo events. Event objects are passed as parameters to event listeners. </p>
		 * 
		 * @param type  — The type of the event. Event listeners can access this information through the inherited <code>type</code> property. The one type of StageVideoEvent is <code>StageVideoEvent.RENDER_STATE</code>. 
		 * @param bubbles  — Determines whether the Event object participates in the bubbling stage of the event flow. Event listeners can access this information through the inherited <code>bubbles</code> property. 
		 * @param cancelable  — Determines whether the Event object can be canceled. Event listeners can access this information through the inherited <code>cancelable</code> property. 
		 * @param status  — Indicates the status of the target StageVideo object. 
		 * @param colorSpace  — The color space used by the video being displayed. 
		 */
		public function StageVideoEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, status:String = null, colorSpace:String = null) {
			super(type, bubbles, cancelable);
			throw new Error("Not implemented");
		}

		/**
		 * <p> The color space used by the video being displayed in the StageVideo object. </p>
		 * 
		 * @return 
		 */
		public function get colorSpace():String {
			return _colorSpace;
		}

		/**
		 * <p> The status of the StageVideo object. </p>
		 * 
		 * @return 
		 */
		public function get status():String {
			return _status;
		}
	}
}
