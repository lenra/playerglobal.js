package flash.events {
	/**
	 *  An object dispatches a StatusEvent object when a device, such as a camera or microphone, or an object such as a LocalConnection object reports its status. There is only one type of status event: <code>StatusEvent.STATUS</code>. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../flash/media/Camera.html" target="">flash.media.Camera</a>
	 *  <br>
	 *  <a href="../../flash/media/Microphone.html" target="">flash.media.Microphone</a>
	 *  <br>
	 *  <a href="../../flash/net/LocalConnection.html" target="">flash.net.LocalConnection</a>
	 *  <br>
	 *  <a href="../../flash/sensors/Accelerometer.html" target="">flash.sensors.Accelerometer</a>
	 *  <br>
	 *  <a href="../../flash/sensors/Geolocation.html" target="">flash.sensors.Geolocation</a>
	 *  <br>
	 *  <a href="../../flash/sensors/DeviceRotation.html" target="">flash.sensors.DeviceRotation</a>
	 *  <br>
	 *  <a href="../../air/net/ServiceMonitor.html" target="">air.net.ServiceMonitor</a>
	 * </div><br><hr>
	 */
	public class StatusEvent extends flash.events.Event {
		/**
		 * <p> Defines the value of the <code>type</code> property of a <code>status</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>code</code></td>
		 *    <td>A description of the object's status.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>level</code></td>
		 *    <td>The category of the message, such as <code>"status"</code>, <code>"warning"</code> or <code>"error"</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The object reporting its status.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/media/Camera.html#event:status" target="">flash.media.Camera.status</a>
		 *  <br>
		 *  <a href="../../flash/media/Microphone.html#event:status" target="">flash.media.Microphone.status</a>
		 *  <br>
		 *  <a href="../../flash/net/LocalConnection.html#event:status" target="">flash.net.LocalConnection.status</a>
		 *  <br>
		 *  <a href="../../flash/net/NetStream.html#event:status" target="">flash.net.NetStream.status</a>
		 *  <br>
		 *  <a href="../../flash/sensors/Geolocation.html#event:status" target="">flash.sensors.Geolocation.status</a>
		 *  <br>
		 *  <a href="../../flash/sensors/Accelerometer.html#event:status" target="">flash.sensors.Accelerometer.status</a>
		 *  <br>
		 *  <a href="../../flash/sensors/DeviceRotation.html#event:status" target="">flash.sensors.DeviceRotation.status</a>
		 * </div>
		 */
		public static const STATUS:String = "status";

		private var _code:String;
		private var _level:String;

		/**
		 * <p> Creates an Event object that contains information about status events. Event objects are passed as parameters to event listeners. </p>
		 * 
		 * @param type  — The type of the event. Event listeners can access this information through the inherited <code>type</code> property. There is only one type of status event: <code>StatusEvent.STATUS</code>. 
		 * @param bubbles  — Determines whether the Event object participates in the bubbling stage of the event flow. Event listeners can access this information through the inherited <code>bubbles</code> property. 
		 * @param cancelable  — Determines whether the Event object can be canceled. Event listeners can access this information through the inherited <code>cancelable</code> property. 
		 * @param code  — A description of the object's status. Event listeners can access this information through the <code>code</code> property. 
		 * @param level  — The category of the message, such as <code>"status"</code>, <code>"warning"</code> or <code>"error"</code>. Event listeners can access this information through the <code>level</code> property. 
		 */
		public function StatusEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, code:String = "", level:String = "") {
			super(type, bubbles, cancelable);
			throw new Error("Not implemented");
		}

		/**
		 * <p> A description of the object's status. </p>
		 * 
		 * @return 
		 */
		public function get code():String {
			return _code;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set code(value:String):void {
			_code = value;
		}

		/**
		 * <p> The category of the message, such as <code>"status"</code>, <code>"warning"</code> or <code>"error"</code>. </p>
		 * 
		 * @return 
		 */
		public function get level():String {
			return _level;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set level(value:String):void {
			_level = value;
		}

		/**
		 * <p> Creates a copy of the StatusEvent object and sets the value of each property to match that of the original. </p>
		 * 
		 * @return  — A new StatusEvent object with property values that match those of the original. 
		 */
		override public function clone():flash.events.Event {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a string that contains all the properties of the StatusEvent object. The string is in the following format: </p>
		 * <p><code>[StatusEvent type=<i>value</i> bubbles=<i>value</i> cancelable=<i>value</i> code=<i>value</i> level=<i>value</i>]</code></p>
		 * 
		 * @return  — A string that contains all the properties of the StatusEvent object. 
		 */
		override public function toString():String {
			throw new Error("Not implemented");
		}
	}
}
