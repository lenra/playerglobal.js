package flash.events {
	/**
	 *  <p>This event fires when user selects a different audio output device from Flash Player's settings UI, or an audio device gets added to / removed from the system.</p> <br><hr>
	 */
	public class AudioOutputChangeEvent extends flash.events.Event {
		public static const AUDIO_OUTPUT_CHANGE:String = "audioOutputChange";

		private var _reason:String;

		/**
		 * @param type  — The type of event. There is only one type of AudioOutputchangeEvent event: <code>AudioOutputchangeEvent.AUDIO_OUTPUT_CHANGE</code>. 
		 * @param bubbles  — Indicates whether this Event object participates in the bubbling stage of the event flow. 
		 * @param cancelable  — Indicates whether you can cancel the action that triggers this event. 
		 * @param reason  — The reason that triggers the audio output change event. <code>AudioOutputChangeReason.USER_SELECTION</code>, indicates that the audio output change is initiated by the user selecting a different device from Flash Player Settings UI. <code>AudioOutputChangeReason.DEVICE_CHANGE</code>, indicates that audio device has either changed, added or removed. 
		 */
		public function AudioOutputChangeEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, reason:String = null) {
			super(type, bubbles, cancelable);
			throw new Error("Not implemented");
		}

		/**
		 * @return 
		 */
		public function get reason():String {
			return _reason;
		}
	}
}
