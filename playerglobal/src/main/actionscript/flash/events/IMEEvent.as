package flash.events {
	import flash.text.ime.IIMEClient;

	/**
	 *  An IMEEvent object is dispatched when the user enters text using an input method editor (IME). IMEs are generally used to enter text from languages that have ideographs instead of letters, such as Japanese, Chinese, and Korean. There are two IME events: <code>IMEEvent.IME_COMPOSITION</code> and <code>IMEEvent.IME_START_COMPOSITION</code>. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../flash/system/IME.html" target="">flash.system.IME</a>
	 *  <br>
	 *  <a href="IMEEvent.html#IME_COMPOSITION" target="">flash.events.IMEEvent.IME_COMPOSITION</a>
	 *  <br>
	 *  <a href="IMEEvent.html#IME_START_COMPOSITION" target="">flash.events.IMEEvent.IME_START_COMPOSITION</a>
	 * </div><br><hr>
	 */
	public class IMEEvent extends TextEvent {
		/**
		 * <p> Defines the value of the <code>type</code> property of an <code>imeComposition</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The IME object.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p id="moreExamples"><span class="label">More examples</span></p>
		 * <div class="seeAlso">
		 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7cd5.html" target="_blank">Using the IME class</a>
		 * </div>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/system/IME.html#event:imeComposition" target="">flash.system.IME.imeComposition</a>
		 * </div>
		 */
		public static const IME_COMPOSITION:String = "imeComposition";
		/**
		 * <p> To handle IME text input, the receiver must set the <code>imeClient</code> field of the event to an object that implements the IIMEClient interface. If <code>imeClient</code> is unset, the runtime uses out-of-line IME composition instead, and sends the final composition as a TEXT_INPUT event. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The IME object.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/system/IME.html#event:imeComposition" target="">flash.system.IME.imeComposition</a>
		 * </div>
		 */
		public static const IME_START_COMPOSITION:String = "imeStartComposition";

		private var _imeClient:IIMEClient;

		/**
		 * <p> Creates an Event object with specific information relevant to IME events. Event objects are passed as parameters to event listeners. </p>
		 * 
		 * @param type  — The type of the event. Event listeners can access this information through the inherited <code>type</code> property. There is only one IME event: <code>IMEEvent.IME_COMPOSITION</code>. 
		 * @param bubbles  — Determines whether the Event object participates in the bubbling stage of the event flow. Event listeners can access this information through the inherited <code>bubbles</code> property. 
		 * @param cancelable  — Determines whether the Event object can be canceled. Event listeners can access this information through the inherited <code>cancelable</code> property. 
		 * @param text  — The reading string from the IME. This is the initial string as typed by the user, before selection of any candidates. The final composition string is delivered to the object with keyboard focus in a <code>TextEvent.TEXT_INPUT</code> event. Event listeners can access this information through the <code>text</code> property. 
		 * @param imeClient  — A set of callbacks used by the text engine to communicate with the IME. Useful if your code has its own text engine and is rendering lines of text itself, rather than using TextField objects or the TextLayoutFramework. 
		 */
		public function IMEEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, text:String = "", imeClient:IIMEClient = null) {
			super(type, bubbles, cancelable, text);
			throw new Error("Not implemented");
		}

		/**
		 * <p> Specifies an object that implements the IMEClient interface. Components based on the flash.text.engine package must implement this interface to support editing text inline using an IME. </p>
		 * 
		 * @return 
		 */
		public function get imeClient():IIMEClient {
			return _imeClient;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set imeClient(value:IIMEClient):void {
			_imeClient = value;
		}

		/**
		 * <p> Creates a copy of the IMEEvent object and sets the value of each property to match that of the original. </p>
		 * 
		 * @return  — A new IMEEvent object with property values that match those of the original. 
		 */
		override public function clone():flash.events.Event {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a string that contains all the properties of the IMEEvent object. The string is in the following format: </p>
		 * <p><code>[IMEEvent type=<i>value</i> bubbles=<i>value</i> cancelable=<i>value</i> text=<i>value</i>]</code></p>
		 * 
		 * @return  — A string that contains all the properties of the IMEEvent object. 
		 */
		override public function toString():String {
			throw new Error("Not implemented");
		}
	}
}
