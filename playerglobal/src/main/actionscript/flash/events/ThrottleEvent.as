package flash.events {
	/**
	 *  A ThrottleEvent is dispatched when the Flash Player throttles, pauses, or resumes content. There is only one type of ThrottleEvent event: <code>ThrottleEvent.THROTTLE</code>. <p>This event is a broadcast event, which means that it is dispatched by all EventDispatcher objects with a listener registered for this event. For more information about broadcast events, see the DisplayObject class.</p> <p> <b>Note:</b> This event has neither a "capture phase" nor a "bubble phase", which means that event listeners must be added directly to any potential targets, whether the target is on the display list or not.</p> <p>The Flash Player can throttle the content to a low frame rate (meaning the frame rate is reduced to a value typically between 2 and 8 fps). Content can be throttled when its tab is hidden or minimized. On a mobile device, content can be throttled when the backlight goes off or screensaver mode comes on. Prior to throttling the content, a ThrottleEvent is dispatched with <code>ThrottleEvent.state=ThrottleType.THROTTLE</code>. The <code>ThrottleEvent.targetFrameRate</code> property contains the value of the new target frame rate. </p> <p>The content can run code in the event listener to prepare for the throttle. This is an opportunity to alert external content that the throttled content is much less responsive. For instance, an active <code>FileReference.upload()</code> or <code>FileReference.download()</code> method could be canceled. Or, if content is communicating using LocalConnection with another SWF, this is an opportunity to inform that SWF to expect less responsiveness. Note that the throttled content may not be able to complete asynchronous actions prior to entering the throttle. Content enters the throttled state when the event listener returns.</p> <p>The Flash Player can pause content. For example, content can be paused when it is scrolled offscreen on a mobile device at a time when no audio or video is playing. Prior to pausing the content, a ThrottleEvent is dispatched with <code>ThrottleEvent.state=ThrottleType.PAUSE</code> and <code>ThrottleEvent.targetFrameRate=0</code>. Similar to when the content receives a <code>ThrottleType.THROTTLE</code> event, the content can run code in the event listener to prepare for the pause. When the event listener returns, the content enters the paused state. While paused, the content does not respond to user interaction, such as mouse clicks or keyboard entry. However, ActionScript network events can still be received.</p> <p>When the Flash Player resumes the content from a throttled or paused state, a ThrottleEvent is dispatched with <code>ThrottleEvent.state=ThrottleType.RESUME</code>. <code>ThrottleEvent.targetFrameRate</code> describes the frame rate and is normally equal to <code>Stage.frameRate</code>. Content may be resumed when any part of the stage becomes visible or when the user makes a request for the content to be resumed.</p> <p>The platforms that support throttling and pausing are currently the following: Flash Player Desktop Mac and Windows, AIR Mobile, and Flash Player Android. The following platforms do not dispatch the ThrottleEvent automatically because they do not yet support pausing or throttling: AIR for TV devices, AIR for desktop, and Flash Player Linux Desktop.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../flash/display/DisplayObject.html" target="">flash.display.DisplayObject</a>
	 *  <br>
	 *  <a href="../../flash/display/Stage.html" target="">flash.display.Stage</a>
	 *  <br>
	 *  <a href="ThrottleType.html" target="">flash.events.ThrottleType</a>
	 * </div><br><hr>
	 */
	public class ThrottleEvent extends flash.events.Event {
		/**
		 * <p> Defines the value of the <code>type</code> property of a <code>ThrottleEvent</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>Any DisplayObject instance with a listener registered for the throttle event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>state</code></td>
		 *    <td>ThrottleType.THROTTLE, ThrottleType.PAUSE, or ThrottleType.RESUME.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>targetFrameRate</code></td>
		 *    <td>Describes the frame rate that Flash Player or AIR will target after the ThrottleEvent is dispatched. For example, if the content is being paused, targetFrameRate will be 0. If the content is being throttled, then targetFrameRate is typically between 2 and 8, depending on which platform the content is running on, whether audio is playing, and other factors. If the content is returned to the expected frame rate after being paused or throttled, targetFrameRate equals Stage.frameRate.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/Stage.html" target="">flash.display.Stage</a>
		 * </div>
		 */
		public static const THROTTLE:String = "throttle";

		private var _state:String;
		private var _targetFrameRate:Number;

		/**
		 * <p> Creates an Event object that contains information about the ThrottleEvent. Event objects are passed as parameters to event listeners. </p>
		 * 
		 * @param type  — The type of the event. Event listeners can access this information through the inherited <code>type</code> property. There is only one type of ThrottleEvent event: <code>ThrottleEvent.THROTTLE</code>. 
		 * @param bubbles  — Determines whether the Event object participates in the bubbling stage of the event flow. Event listeners can access this information through the inherited <code>bubbles</code> property. 
		 * @param cancelable  — Determines whether the Event object can be canceled. Event listeners can access this information through the inherited <code>cancelable</code> property. 
		 * @param state  — <code>ThrottleType.THROTTLE</code>, <code>ThrottleType.PAUSE</code>, or <code>ThrottleType.RESUME</code>. 
		 * @param targetFrameRate  — Describes the frame rate that Flash Player or AIR will target after the ThrottleEvent is dispatched. For example, if the content is being paused, <code>targetFrameRate</code> is 0. If the content is being throttled, then <code>targetFrameRate</code> is typically between 2 and 8, depending on which platform the content is running on, whether audio is playing, and other factors. If the content is returned to the expected frame rate after being paused or throttled, <code>targetFrameRate</code> equals <code>Stage.frameRate</code>. 
		 */
		public function ThrottleEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, state:String = null, targetFrameRate:Number = 0) {
			super(type, bubbles, cancelable);
			throw new Error("Not implemented");
		}

		/**
		 * <p> Describes the state that the player is entering: <code>ThrottleType.THROTTLE</code>, <code>ThrottleType.PAUSE</code>, or <code>ThrottleType.RESUME</code>. </p>
		 * 
		 * @return 
		 */
		public function get state():String {
			return _state;
		}

		/**
		 * <p> The frame rate that Flash Player or AIR targets after the ThrottleEvent is dispatched. </p>
		 * <p>For example, if the content is being paused, <code>targetFrameRate</code> is 0. If the content is being throttled, then <code>targetFrameRate</code> is typically between 2 and 8, depending on which platform the content is running on, whether audio is playing, and other factors. If the content is returned to the expected frame rate after being paused or throttled, <code>targetFrameRate</code> equals <code>Stage.frameRate</code>.</p>
		 * 
		 * @return 
		 */
		public function get targetFrameRate():Number {
			return _targetFrameRate;
		}

		/**
		 * <p> Creates a copy of the ThrottleEvent object and sets the value of each property to match that of the original. </p>
		 * 
		 * @return  — A new ThrottleEvent object with property values that match those of the original. 
		 */
		override public function clone():flash.events.Event {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a string that contains all the properties of the ThrottleEvent object. The string is in the following format: </p>
		 * <p><code>[ThrottleEvent type=<i>value</i> bubbles=<i>value</i> cancelable=<i>value</i> state=<i>value</i> targetFrameRate=<i>value</i>]</code></p>
		 * 
		 * @return  — A string that contains all the properties of the ThrottleEvent object. 
		 */
		override public function toString():String {
			throw new Error("Not implemented");
		}
	}
}
