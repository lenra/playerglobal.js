package flash.events {
	import flash.net.drm.DRMContentData;
	import flash.net.drm.DRMVoucher;

	/**
	 *  A NetStream object dispatches a DRMStatusEvent object when the content protected using digital rights management (DRM) begins playing successfully (when the voucher is verified, and when the user is authenticated and authorized to view the content). The DRMStatusEvent object contains information related to the voucher, such as whether the content can be made available offline or when the voucher will expire and the content can no longer be viewed. The application can use this data to inform the user of the status of her policy and permissions. <p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118666ade46-7cda.html" target="_blank">Using the DRMStatusEvent class</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../flash/net/NetStream.html" target="">flash.net.NetStream</a>
	 *  <br>
	 *  <a href="DRMStatusEvent.html#DRM_STATUS" target="">DRMStatusEvent.DRM_STATUS</a>
	 *  <br>
	 *  <a href="../../flash/net/drm/DRMManager.html" target="">flash.net.drm.DRMManager</a>
	 *  <br>
	 *  <a href="../../flash/net/drm/DRMVoucher.html" target="">flash.net.drm.DRMVoucher</a>
	 * </div><br><hr>
	 */
	public class DRMStatusEvent extends flash.events.Event {
		/**
		 * <p> The <code>DRMStatusEvent.DRM_STATUS</code> constant defines the value of the <code>type</code> property of a <code>drmStatus</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>detail</code></td>
		 *    <td>A string explaining the context of the status event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>isAnonymous</code></td>
		 *    <td>Indicates whether the content protected with DRM encryption is available without requiring a user to provide authentication credentials.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>isAvailableOffline</code></td>
		 *    <td>Indicates whether the content protected with DRM encryption is available offline.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>offlineLeasePeriod</code></td>
		 *    <td>The remaining number of days that content can be viewed offline.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>policies</code></td>
		 *    <td>A custom object of the DRM status event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The NetStream object.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>voucherEndDate</code></td>
		 *    <td>The absolute date on which the voucher expires and the content can no longer be viewed by users</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>contentData</code></td>
		 *    <td>The DRMContentData for the content</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>voucher</code></td>
		 *    <td>The DRMVoucher object for the content.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>isLocal</code></td>
		 *    <td>Indicates whether the content is stored on the local file system</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 */
		public static const DRM_STATUS:String = "drmStatus";

		private var _contentData:DRMContentData;
		private var _isLocal:Boolean;
		private var _voucher:DRMVoucher;

		private var _detail:String;
		private var _isAnonymous:Boolean;
		private var _isAvailableOffline:Boolean;
		private var _offlineLeasePeriod:uint;
		private var _policies:Object;
		private var _voucherEndDate:Date;

		/**
		 * <p> Creates an Event object that contains specific information about DRM status events. Event objects are passed as parameters to event listeners. </p>
		 * 
		 * @param type  — The type of the event. Event listeners can access this information through the inherited <code>type</code> property. There is only one type of DRMAuthenticate event: <code>DRMAuthenticateEvent.DRM_AUTHENTICATE</code>. 
		 * @param bubbles  — Determines whether the Event object participates in the bubbling stage of the event flow. Event listeners can access this information through the inherited <code>bubbles</code> property. 
		 * @param cancelable  — Determines whether the Event object can be canceled. Event listeners can access this information through the inherited <code>cancelable</code> property. 
		 * @param inMetadata  — The custom object that contains custom DRM properties. 
		 * @param inVoucher  — The context of the Event. 
		 * @param inLocal  — Indicates if content can be viewed offline. 
		 */
		public function DRMStatusEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, inMetadata:DRMContentData = null, inVoucher:DRMVoucher = null, inLocal:Boolean = false) {
			super(type, bubbles, cancelable);
			throw new Error("Not implemented");
		}

		/**
		 * <p> A DRMContentData object containing the information necessary to obtain a voucher for viewing the DRM-protected content. </p>
		 * 
		 * @return 
		 */
		public function get contentData():DRMContentData {
			return _contentData;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set contentData(value:DRMContentData):void {
			_contentData = value;
		}

		/**
		 * <p> A string explaining the context of the status event. </p>
		 * 
		 * @return 
		 */
		public function get detail():String {
			return _detail;
		}

		/**
		 * <p> Indicates whether the content, protected with digital rights management (DRM) encryption, is available without requiring a user to provide authentication credentials. If so, the value is <code>true</code>. Otherwise, the value is <code>false</code>, and a user must provide a username and password that matches the one known and expected by the content provider. </p>
		 * 
		 * @return 
		 */
		public function get isAnonymous():Boolean {
			return _isAnonymous;
		}

		/**
		 * <p> Indicates whether the content, protected with digital rights management (DRM) encryption, is available offline. If so, the value is <code>true</code>. Otherwise, the value is <code>false</code>. </p>
		 * <p> In order for digitally protected content to be available offline, its voucher must be cached to the user's local machine. (The application decides where to store the content locally in order for it to be available offline.) </p>
		 * 
		 * @return 
		 */
		public function get isAvailableOffline():Boolean {
			return _isAvailableOffline;
		}

		/**
		 * <p> Indicates whether the voucher is cached in the local voucher store. </p>
		 * 
		 * @return 
		 */
		public function get isLocal():Boolean {
			return _isLocal;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set isLocal(value:Boolean):void {
			_isLocal = value;
		}

		/**
		 * <p> The remaining number of days that content can be viewed offline. </p>
		 * 
		 * @return 
		 */
		public function get offlineLeasePeriod():uint {
			return _offlineLeasePeriod;
		}

		/**
		 * <p> A custom object of the DRM status event. </p>
		 * 
		 * @return 
		 */
		public function get policies():Object {
			return _policies;
		}

		/**
		 * <p> A DRMVoucher object for the content. </p>
		 * 
		 * @return 
		 */
		public function get voucher():DRMVoucher {
			return _voucher;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set voucher(value:DRMVoucher):void {
			_voucher = value;
		}

		/**
		 * <p> The absolute date on which the voucher expires and the content can no longer be viewed by users. </p>
		 * 
		 * @return 
		 */
		public function get voucherEndDate():Date {
			return _voucherEndDate;
		}

		/**
		 * <p> Creates a copy of the DRMStatusEvent object and sets the value of each property to match that of the original. </p>
		 * 
		 * @return  — A new DRMStatusEvent object with property values that match those of the original. 
		 */
		override public function clone():flash.events.Event {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a string that contains all the properties of the DRMStatusEvent object. </p>
		 * 
		 * @return  — A string that contains all the properties of the DRMStatusEvent object. 
		 */
		override public function toString():String {
			throw new Error("Not implemented");
		}
	}
}
