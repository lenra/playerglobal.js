package flash.events {
	/**
	 *  The DRMManager dispatches a DRMVoucherReturnCompleteEvent object when a call to the <code>returnVoucher()</code> method of the DRMManager object succeeds. <br><hr>
	 */
	public class DRMReturnVoucherCompleteEvent extends flash.events.Event {
		/**
		 * <p> The string constant to use for the return voucher complete event in the type parameter when adding and removing event listeners. </p>
		 */
		public static const RETURN_VOUCHER_COMPLETE:String = "returnVoucherComplete";

		private var _licenseID:String;
		private var _numberOfVouchersReturned:int;
		private var _policyID:String;
		private var _serverURL:String;

		/**
		 * <p> Creates a new instance of a DRMReturnVoucherCompleteEvent object. </p>
		 * 
		 * @param type  — the event type string 
		 * @param bubbles  — whether the event bubbles up the display list 
		 * @param cancelable  — whether the event can be canceled 
		 * @param inServerURL  — the URL of the logged-in server 
		 * @param inLicenseID  — the authenticated domain on the logged-in server 
		 * @param inPolicyID  — the authentication token 
		 * @param inNumberOfVouchersReturned
		 */
		public function DRMReturnVoucherCompleteEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, inServerURL:String = null, inLicenseID:String = null, inPolicyID:String = null, inNumberOfVouchersReturned:int = 0) {
			super(type, bubbles, cancelable);
			throw new Error("Not implemented");
		}

		/**
		 * <p> The license ID that was passed into the DRMManager.returnVoucher() call. </p>
		 * 
		 * @return 
		 */
		public function get licenseID():String {
			return _licenseID;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set licenseID(value:String):void {
			_licenseID = value;
		}

		/**
		 * <p> The number of vouchers that matches the criterion passed into DRMManager.returnVoucher() and subsequently returned. </p>
		 * 
		 * @return 
		 */
		public function get numberOfVouchersReturned():int {
			return _numberOfVouchersReturned;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set numberOfVouchersReturned(value:int):void {
			_numberOfVouchersReturned = value;
		}

		/**
		 * <p> The policyID that was passed into the DRMManager.returnVoucher() call. </p>
		 * 
		 * @return 
		 */
		public function get policyID():String {
			return _policyID;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set policyID(value:String):void {
			_policyID = value;
		}

		/**
		 * <p> The URL of the media rights server. </p>
		 * 
		 * @return 
		 */
		public function get serverURL():String {
			return _serverURL;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set serverURL(value:String):void {
			_serverURL = value;
		}

		/**
		 * @return  — A new Event object that is identical to the original. 
		 */
		override public function clone():flash.events.Event {
			throw new Error("Not implemented");
		}
	}
}
