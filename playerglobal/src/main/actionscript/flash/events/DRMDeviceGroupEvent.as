package flash.events {
	import flash.net.drm.DRMDeviceGroup;

	/**
	 *  <p>Issued by the DRMManager when a device group related call successfully completes. Multiple devices can be registered to a device group using the <code>DRMManager.addToDeviceGroup()</code> method. If there is a device with a valid domain-bound voucher for a given content, the application can then extract the serialized DRM vouchers using the <code>DRMVoucher.toByteArray()</code> method.</p> <p>If the content metadata specifies that domain registration is required, the application can invoke an API to join the device group. This action triggers a domain registration request to be sent to the domain server. Once a license is issued to a device group, the license can be exported and shared with other devices that have joined the device group.</p> <br><hr>
	 */
	public class DRMDeviceGroupEvent extends flash.events.Event {
		public static const ADD_TO_DEVICE_GROUP_COMPLETE:String = "addToDeviceGroupComplete";
		public static const REMOVE_FROM_DEVICE_GROUP_COMPLETE:String = "removeFromDeviceGroupComplete";

		private var _deviceGroup:DRMDeviceGroup;

		/**
		 * @param type
		 * @param bubbles
		 * @param cancelable
		 * @param deviceGroup
		 */
		public function DRMDeviceGroupEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, deviceGroup:DRMDeviceGroup = null) {
			super(type, bubbles, cancelable);
			throw new Error("Not implemented");
		}

		/**
		 * <p> The DRMDeviceGroup object for this event. </p>
		 * 
		 * @return 
		 */
		public function get deviceGroup():DRMDeviceGroup {
			return _deviceGroup;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set deviceGroup(value:DRMDeviceGroup):void {
			_deviceGroup = value;
		}

		/**
		 * @return 
		 */
		override public function clone():flash.events.Event {
			throw new Error("Not implemented");
		}

		/**
		 * @return 
		 */
		override public function toString():String {
			throw new Error("Not implemented");
		}
	}
}
