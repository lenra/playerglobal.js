package flash.events {
	import flash.net.drm.DRMDeviceGroup;

	/**
	 *  <p>Issued by the DRMManager when any error occurs during any device group related calls.</p> <p>It is the application's responsibility to explicitly handle the error events.These events include cases where the user inputs valid credentials, but the voucher protecting the encrypted content restricts the access to the content. For example, an authenticated user cannot access content if the rights have not been paid for. This error can also occur when two registered members of the same publisher attempt to share content that only one of them has paid for.</p> <br><hr>
	 */
	public class DRMDeviceGroupErrorEvent extends ErrorEvent {
		public static const ADD_TO_DEVICE_GROUP_ERROR:String = "addToDeviceGroupError";
		public static const REMOVE_FROM_DEVICE_GROUP_ERROR:String = "removeFromDeviceGroupError";

		private var _deviceGroup:DRMDeviceGroup;
		private var _subErrorID:int;

		private var _drmUpdateNeeded:Boolean;
		private var _systemUpdateNeeded:Boolean;

		/**
		 * @param type
		 * @param bubbles
		 * @param cancelable
		 * @param errorDetail
		 * @param errorCode
		 * @param subErrorID
		 * @param deviceGroup
		 * @param systemUpdateNeeded
		 * @param drmUpdateNeeded
		 */
		public function DRMDeviceGroupErrorEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, errorDetail:String = "", errorCode:int = 0, subErrorID:int = 0, deviceGroup:DRMDeviceGroup = null, systemUpdateNeeded:Boolean = false, drmUpdateNeeded:Boolean = false) {
			super(type, bubbles, cancelable, text);
			throw new Error("Not implemented");
		}

		/**
		 * <p> The DRMDeviceGroup object for this error event. </p>
		 * 
		 * @return 
		 */
		public function get deviceGroup():DRMDeviceGroup {
			return _deviceGroup;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set deviceGroup(value:DRMDeviceGroup):void {
			_deviceGroup = value;
		}

		/**
		 * @return 
		 */
		public function get drmUpdateNeeded():Boolean {
			return _drmUpdateNeeded;
		}

		/**
		 * @return 
		 */
		public function get subErrorID():int {
			return _subErrorID;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set subErrorID(value:int):void {
			_subErrorID = value;
		}

		/**
		 * @return 
		 */
		public function get systemUpdateNeeded():Boolean {
			return _systemUpdateNeeded;
		}

		/**
		 * @return 
		 */
		override public function clone():flash.events.Event {
			throw new Error("Not implemented");
		}

		/**
		 * @return 
		 */
		override public function toString():String {
			throw new Error("Not implemented");
		}
	}
}
