package flash.events {
	/**
	 *  The SoftKeyboardTrigger class provides enumerator values for the <code>triggerType</code> property of the SoftKeyboardEvent class. These values indicate what type of action triggered a SoftKeyboard activation event. <br><hr>
	 */
	public class SoftKeyboardTrigger {
		/**
		 * <p> Indicates that ActionScript invoked the event. </p>
		 */
		public static const CONTENT_TRIGGERED:String = "contentTriggered";
		/**
		 * <p> Indicates that user action invoked the event. Typical user actions that can trigger this event include explicitly closing the keyboard, or pressing the Back key. </p>
		 */
		public static const USER_TRIGGERED:String = "userTriggered";
	}
}
