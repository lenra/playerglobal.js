package flash.events {
	/**
	 *  An object dispatches a TextEvent object when a user enters text in a text field or clicks a hyperlink in an HTML-enabled text field. There are two types of text events: <code>TextEvent.LINK</code> and <code>TextEvent.TEXT_INPUT</code>. <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../flash/text/TextField.html" target="">flash.text.TextField</a>
	 * </div><br><hr>
	 */
	public class TextEvent extends flash.events.Event {
		/**
		 * <p> Defines the value of the <code>type</code> property of a <code>link</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>true</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The text field containing the hyperlink that has been clicked. The <code>target</code> is not always the object in the display list that registered the event listener. Use the <code>currentTarget</code> property to access the object in the display list that is currently processing the event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>text</code></td>
		 *    <td>The remainder of the URL after "event:"</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="TextEvent.html#text" target="">text</a>
		 *  <br>
		 *  <a href="../../flash/text/TextField.html#event:link" target="">flash.text.TextField.link</a>
		 * </div>
		 * <span id="pageFilter"><br><span class="label"> Example &nbsp;( <span class="usage"><a href="http://www.adobe.com/go/learn_as3_usingexamples_en"> How to use this example </a></span>) </span><br><br>
		 *  <div class="detailBody">
		 *    In this example, when a user clicks a hyperlink in HTML text, it triggers a text event. Depending on the link, the user is sent to a designated website based on the system's operating system, or a circle is drawn based on the user's selected radius. 
		 *   <p>A text field is created and its content is set to an HTML-formatted string by using the <code>htmlText</code> property. The links are underlined for easier identification by the user. (Adobe Flash Player changes the mouse pointer only after the pointer is over the link.) To make sure that the user's click invokes an ActionScript method, the URL of the link begins with the <code>"event:"</code> string and a listener is added for the <code>TextEvent.LINK</code> event.</p> 
		 *   <p>The <code>linkHandler()</code> method that is triggered after the user clicks a link manages all the link events for the text field. The first if statement checks the <code>text</code> property of the event, which holds the remainder of the URL after the <code>"event:"</code> string. If the user clicked the link for the operating system, the name of the user's current operating system, taken from the system's <code>Capabilities.os</code> property, is used to send the user to the designated website. Otherwise, the selected radius size, passed by the event's <code>text</code> property, is used to draw a circle below the text field. Each time the user clicks the radius link, the previously drawn circle is cleared and a new red circle with the selected radius size is drawn.</p> 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>
		 * package {
		 *     import flash.display.Sprite;
		 *     import flash.events.TextEvent;
		 *     import flash.errors.IOError;
		 *     import flash.events.IOErrorEvent;
		 *     import flash.system.Capabilities;
		 *     import flash.net.navigateToURL;
		 *     import flash.net.URLRequest;
		 *     import flash.text.TextField;
		 *     import flash.text.TextFieldAutoSize;
		 *     import flash.display.Shape;
		 *     import flash.display.Graphics;
		 * 
		 *     public class TextEvent_LINKExample extends Sprite {
		 *         private  var myCircle:Shape = new Shape();
		 *         
		 *         public function TextEvent_LINKExample() {
		 *             var myTextField:TextField = new TextField();
		 *             myTextField.autoSize = TextFieldAutoSize.LEFT;
		 *             myTextField.multiline = true;
		 *             myTextField.background = true;
		 *             myTextField.htmlText = "Draw a circle with the radius of &lt;u&gt;&lt;a href=\"event:20\"&gt;20 pixels&lt;/a&gt;&lt;/u&gt;.&lt;br&gt;" 
		 *                          +  "Draw a circle with the radius of &lt;u&gt;&lt;a href=\"event:50\"&gt;50 pixels&lt;/a&gt;&lt;/u&gt;.&lt;br&gt;&lt;br&gt;"
		 *                          +  "&lt;u&gt;&lt;a href=\"event:os\"&gt;Learn about your operating system.&lt;/a&gt;&lt;/u&gt;&lt;br&gt;";
		 * 
		 *             myTextField.addEventListener(TextEvent.LINK, linkHandler);
		 * 
		 *             this.addChild(myTextField);
		 *             this.addChild(myCircle);
		 *         }
		 *         
		 *         private function linkHandler(e:TextEvent):void {
		 *             var osString:String = Capabilities.os;
		 *             
		 *             if(e.text == "os") {
		 *        
		 *                 if (osString.search(/Windows/) != -1 ){
		 *                     navigateToURL(new URLRequest("http://www.microsoft.com/"), "_self");
		 *                 }else if (osString.search(/Mac/) != -1 ) {
		 *                     navigateToURL(new URLRequest("http://www.apple.com/"), "_self");
		 *                 } else if (osString.search(/linux/i)!= -1) {
		 *                     navigateToURL(new URLRequest("http://www.tldp.org/"), "_self");
		 *                 }
		 *             
		 *             } else {
		 *                 myCircle.graphics.clear();    
		 *                 myCircle.graphics.beginFill(0xFF0000);
		 *                 myCircle.graphics.drawCircle(100, 150, Number(e.text));
		 *                 myCircle.graphics.endFill();
		 *             }             
		 *         }
		 *     }
		 * }
		 * </pre>
		 *   </div>
		 *  </div></span>
		 */
		public static const LINK:String = "link";
		/**
		 * <p> Defines the value of the <code>type</code> property of a <code>textInput</code> event object. </p>
		 * <p><b>Note:</b> This event is not dispatched for the Delete or Backspace keys.</p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>true</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>true</code>; call the <code>preventDefault()</code> method to cancel default behavior.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The text field into which characters are being entered. The target is not always the object in the display list that registered the event listener. Use the <code>currentTarget</code> property to access the object in the display list that is currently processing the event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>text</code></td>
		 *    <td>The character or sequence of characters entered by the user.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/text/TextField.html#event:textInput" target="">flash.text.TextField.textInput</a>
		 *  <br>
		 *  <a href="TextEvent.html#text" target="">text</a>
		 * </div>
		 * <span id="pageFilter"><br><span class="label"> Example &nbsp;( <span class="usage"><a href="http://www.adobe.com/go/learn_as3_usingexamples_en"> How to use this example </a></span>) </span><br><br>
		 *  <div class="detailBody">
		 *    The following example guides the user in generating a special combination key (similar to a password). This combination key has seven alphanumeric characters, where the second and fifth characters are numeric. 
		 *   <p>Three text fields for the preliminary instructions, the user input, and the warning (error) messages are created. An event listener is added to respond to the user's text input by triggering the <code>textInputHandler()</code> method. (Every time the user enters text, a <code>TextEvent.TEXT_INPUT</code> event is dispatched. </p> 
		 *   <p><b>Note:</b> The text events are dispatched when a user enters characters and not as a response to any keyboard input, such as backspace. To catch all keyboard events, use a listener for the <code>KeyboardEvent</code> event.)</p> 
		 *   <p>The <code>textInputHandler()</code> method controls and manages the user input. The <code>preventDefault()</code> method is used to prevent Adobe Flash Player from immediately displaying the text in the input text field. The application is responsible for updating the field. To undo the user's deletion or modification to the characters already entered (the <code>result</code> string), the content of the input text field is reassigned to the <code>result</code> string when a user enters new characters. Also, to produce a consistent user experience, the <code>setSelection()</code> method places the insertion point (a caret) after the last selected character in the text field.</p> 
		 *   <p>The first if statement in the <code>textInputHandler()</code> method checks the input for the second and fifth character positions of the combination key, which must be numbers. If the user input is correct, the <code>updateCombination()</code> method is called and the (<code>result</code>) combination key string is appended with the user input. The <code>updateCombination()</code> method also moves the insertion point after the selected character. After the seven characters are entered, the last if statement in the <code>textInputHandler()</code> method changes type of the <code>inputTextField</code> text field from <code>INPUT</code> to <code>DYNAMIC</code>, which means that the user can no longer enter or change any characters.</p> 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>
		 * package {
		 *     import flash.display.Sprite;
		 *     import flash.text.TextField;
		 *     import flash.text.TextFieldType;
		 *     import flash.text.TextFieldAutoSize;
		 *     import flash.events.TextEvent;
		 * 
		 *     public class TextEvent_TEXT_INPUTExample extends Sprite {
		 *         private var instructionTextField:TextField = new TextField();
		 *         private var inputTextField:TextField = new TextField(); 
		 *         private var warningTextField:TextField = new TextField();
		 *         private var result:String = "";
		 * 
		 *         public function TextEvent_TEXT_INPUTExample() {
		 *             instructionTextField.x = 10;
		 *             instructionTextField.y = 10;
		 *             instructionTextField.background = true; 
		 *             instructionTextField.autoSize = TextFieldAutoSize.LEFT;
		 *             instructionTextField.text = "Please enter a value in the format A#AA#AA,\n" 
		 *                                         + "where 'A' represents a letter and '#' represents a number.\n" +
		 *                                         "(Note that once you input a character you can't change it.)" ;
		 *         
		 *             inputTextField.x = 10;
		 *             inputTextField.y = 70;
		 *             inputTextField.height = 20;
		 *             inputTextField.width = 75;
		 *             inputTextField.background = true;
		 *             inputTextField.border = true;
		 *             inputTextField.type = TextFieldType.INPUT; 
		 *             
		 *             warningTextField.x = 10;
		 *             warningTextField.y = 100;
		 *             warningTextField.autoSize = TextFieldAutoSize.LEFT;
		 *  
		 *             inputTextField.addEventListener(TextEvent.TEXT_INPUT, textInputHandler);   
		 *            
		 *             this.addChild(instructionTextField);
		 *             this.addChild(inputTextField);
		 *             this.addChild(warningTextField);
		 *         }
		 * 
		 *         private function textInputHandler(event:TextEvent):void {
		 *             var charExp:RegExp = /[a-zA-z]/;   
		 *             var numExp:RegExp = /[0-9]/;
		 * 
		 *             event.preventDefault();  
		 *             
		 *             inputTextField.text = result;                
		 *             inputTextField.setSelection(result.length + 1, result.length + 1);
		 *  
		 *             if (inputTextField.text.length == 1 || inputTextField.text.length == 4) {
		 *             
		 *                 if(numExp.test(event.text) == true) {
		 *                     updateCombination(event.text);
		 *                 } else {
		 *                     warningTextField.text = "You need a single digit number.";
		 *                 }
		 *                
		 *             }else {
		 *                 
		 *                 if(charExp.test(event.text) == true) { 
		 *                     updateCombination(event.text);
		 *                 } else {
		 *                     warningTextField.text = "You need an alphabet character.";
		 *                 }
		 *             }
		 *  
		 *             if(inputTextField.text.length == 7) {
		 *                 inputTextField.type = TextFieldType.DYNAMIC;
		 *                 instructionTextField.text = "CONGRATULATIONS. You've done.";                
		 *             }          
		 *         }
		 * 
		 *         private function updateCombination(s:String):void {
		 *                     warningTextField.text = "";
		 *                     result += s;           
		 *                     inputTextField.text = result;
		 *                     inputTextField.setSelection(result.length + 1, result.length + 1);
		 *         }
		 *     }
		 * }
		 * </pre>
		 *   </div>
		 *  </div></span>
		 */
		public static const TEXT_INPUT:String = "textInput";

		private var _text:String;

		/**
		 * <p> Creates an Event object that contains information about text events. Event objects are passed as parameters to event listeners. </p>
		 * 
		 * @param type  — The type of the event. Event listeners can access this information through the inherited <code>type</code> property. Possible values are: <code>TextEvent.LINK</code> and <code>TextEvent.TEXT_INPUT</code>. 
		 * @param bubbles  — Determines whether the Event object participates in the bubbling phase of the event flow. Event listeners can access this information through the inherited <code>bubbles</code> property. 
		 * @param cancelable  — Determines whether the Event object can be canceled. Event listeners can access this information through the inherited <code>cancelable</code> property. 
		 * @param text  — One or more characters of text entered by the user. Event listeners can access this information through the <code>text</code> property. 
		 */
		public function TextEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, text:String = "") {
			super(type, bubbles, cancelable);
			throw new Error("Not implemented");
		}

		/**
		 * <p> For a <code>textInput</code> event, the character or sequence of characters entered by the user. For a <code>link</code> event, the text of the <code>event</code> attribute of the <code>href</code> attribute of the <code>&lt;a&gt;</code> tag. </p>
		 * 
		 * @return 
		 */
		public function get text():String {
			return _text;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set text(value:String):void {
			_text = value;
		}

		/**
		 * <p> Creates a copy of the TextEvent object and sets the value of each property to match that of the original. </p>
		 * 
		 * @return  — A new TextEvent object with property values that match those of the original. 
		 */
		override public function clone():flash.events.Event {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a string that contains all the properties of the TextEvent object. The string is in the following format: </p>
		 * <p><code>[TextEvent type=<i>value</i> bubbles=<i>value</i> cancelable=<i>value</i> text=<i>value</i>]</code></p>
		 * 
		 * @return  — A string that contains all the properties of the TextEvent object. 
		 */
		override public function toString():String {
			throw new Error("Not implemented");
		}
	}
}
