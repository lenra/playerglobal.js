package flash.events {
	import flash.display.InteractiveObject;
	import flash.utils.ByteArray;

	/**
	 *  The TouchEvent class lets you handle events on devices that detect user contact with the device (such as a finger on a touch screen). <p>When a user interacts with a device such as a mobile phone or tablet with a touch screen, the user typically touches the screen with his or her fingers or a pointing device. You can develop applications that respond to basic touch events (such as a single finger tap) with the TouchEvent class. Create event listeners using the event types defined in this class. For user interaction with multiple points of contact (such as several fingers moving across a touch screen at the same time) use the related GestureEvent, PressAndTapGestureEvent, and TransformGestureEvent classes. And, use the properties and methods of these classes to construct event handlers that respond to the user touching the device.</p> <p>Use the Multitouch class to determine the current environment's support for touch interaction, and to manage the support of touch interaction if the current environment supports it.</p> <p> <b>Note:</b> When objects are nested on the display list, touch events target the deepest possible nested object that is visible in the display list. This object is called the target node. To have a target node's ancestor (an object containing the target node in the display list) receive notification of a touch event, use <code>EventDispatcher.addEventListener()</code> on the ancestor node with the type parameter set to the specific touch event you want to detect.</p> <p>In AIR 3, and above, you can listen for proximity events on supported Android devices that have an active stylus. On such devices, <code>proximityMove</code> and <code>touchMove</code> event objects provide a byte array containing path and pressure samples taken since the previous move event. You can use these samples to construct the path of the stylus between touch events. (Note that hit-testing for interaction of the stylus input with the display list only occurs at the end of a path segment.)</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WSb2ba3b1aad8a27b0-6ffb37601221e58cc29-8000.html" target="_blank">Touch, multitouch and gesture input</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../flash/ui/Multitouch.html" target="">flash.ui.Multitouch</a>
	 *  <br>
	 *  <a href="GestureEvent.html" target="">flash.events.GestureEvent</a>
	 *  <br>
	 *  <a href="TransformGestureEvent.html" target="">flash.events.TransformGestureEvent</a>
	 *  <br>
	 *  <a href="PressAndTapGestureEvent.html" target="">flash.events.PressAndTapGestureEvent</a>
	 *  <br>
	 *  <a href="MouseEvent.html" target="">flash.events.MouseEvent</a>
	 *  <br>
	 *  <a href="EventDispatcher.html#addEventListener()" target="">flash.events.EventDispatcher.addEventListener()</a>
	 * </div><br><hr>
	 */
	public class TouchEvent extends flash.events.Event {
		/**
		 * <p> Defines the value of the <code>type</code> property of a <code>PROXIMITY_BEGIN</code> touch event object. </p>
		 * <p>The dispatched TouchEvent object has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>altKey</code></td>
		 *    <td><code>true</code> if the Alt key is active (Windows or Linux).</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>true</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>commandKey</code></td>
		 *    <td><code>true</code> on the Mac if the Command key is active; <code>false</code> if it is inactive. Always <code>false</code> on Windows.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>controlKey</code></td>
		 *    <td><code>true</code> if the Ctrl or Control key is active; <code>false</code> if it is inactive.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>ctrlKey</code></td>
		 *    <td><code>true</code> on Windows or Linux if the Ctrl key is active. <code>true</code> on Mac if either the Ctrl key or the Command key is active. Otherwise, <code>false</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>eventPhase</code></td>
		 *    <td>The current phase in the event flow.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>isRelatedObjectInaccessible</code></td>
		 *    <td><code>true</code> if the relatedObject property is set to <code>null</code> because of security sandbox rules.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>localX</code></td>
		 *    <td>The horizontal coordinate at which the event occurred relative to the containing sprite.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>localY</code></td>
		 *    <td>The vertical coordinate at which the event occurred relative to the containing sprite.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>pressure</code></td>
		 *    <td>A value between <code>0.0</code> and <code>1.0</code> indicating force of the contact with the device. If the device does not support detecting the pressure, the value is <code>1.0</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>relatedObject</code></td>
		 *    <td>A reference to a display list object related to the event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>shiftKey</code></td>
		 *    <td><code>true</code> if the Shift key is active; <code>false</code> if it is inactive.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>sizeX</code></td>
		 *    <td>Width of the contact area.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>sizeY</code></td>
		 *    <td>Height of the contact area.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>stageX</code></td>
		 *    <td>The horizontal coordinate at which the event occurred in global stage coordinates.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>stageY</code></td>
		 *    <td>The vertical coordinate at which the event occurred in global stage coordinates.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The InteractiveObject instance under the touching device. The <code>target</code> is not always the object in the display list that registered the event listener. Use the <code>currentTarget</code> property to access the object in the display list that is currently processing the event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>touchPointID</code></td>
		 *    <td>A unique identification number (as an int) assigned to the touch point.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>timestamp</code></td>
		 *    <td>(AIR only) The timestamp of the event in milliseconds relative to the start of the application.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>touchIntent</code></td>
		 *    <td>(AIR only) A value from the TouchEventIntent class.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>isTouchPointCanceled</code></td>
		 *    <td>(AIR only) <code>true</code> if the touch event is canceled because of the device's touch-rejection logic.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/InteractiveObject.html#event:proximityBegin" target="">flash.display.InteractiveObject.proximityBegin</a>
		 * </div>
		 */
		public static const PROXIMITY_BEGIN:String = "proximityBegin";
		/**
		 * <p> Defines the value of the <code>type</code> property of a <code>PROXIMITY_END</code> touch event object. </p>
		 * <p>The dispatched TouchEvent object has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>altKey</code></td>
		 *    <td><code>true</code> if the Alt key is active (Windows or Linux).</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>true</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>commandKey</code></td>
		 *    <td><code>true</code> on the Mac if the Command key is active; <code>false</code> if it is inactive. Always <code>false</code> on Windows.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>controlKey</code></td>
		 *    <td><code>true</code> if the Ctrl or Control key is active; <code>false</code> if it is inactive.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>ctrlKey</code></td>
		 *    <td><code>true</code> on Windows or Linux if the Ctrl key is active. <code>true</code> on Mac if either the Ctrl key or the Command key is active. Otherwise, <code>false</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>eventPhase</code></td>
		 *    <td>The current phase in the event flow.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>isRelatedObjectInaccessible</code></td>
		 *    <td><code>true</code> if the relatedObject property is set to <code>null</code> because of security sandbox rules.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>localX</code></td>
		 *    <td>The horizontal coordinate at which the event occurred relative to the containing sprite.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>localY</code></td>
		 *    <td>The vertical coordinate at which the event occurred relative to the containing sprite.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>pressure</code></td>
		 *    <td>A value between <code>0.0</code> and <code>1.0</code> indicating force of the contact with the device. If the device does not support detecting the pressure, the value is <code>1.0</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>relatedObject</code></td>
		 *    <td>A reference to a display list object related to the event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>shiftKey</code></td>
		 *    <td><code>true</code> if the Shift key is active; <code>false</code> if it is inactive.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>sizeX</code></td>
		 *    <td>Width of the contact area.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>sizeY</code></td>
		 *    <td>Height of the contact area.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>stageX</code></td>
		 *    <td>The horizontal coordinate at which the event occurred in global stage coordinates.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>stageY</code></td>
		 *    <td>The vertical coordinate at which the event occurred in global stage coordinates.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The InteractiveObject instance under the touching device. The <code>target</code> is not always the object in the display list that registered the event listener. Use the <code>currentTarget</code> property to access the object in the display list that is currently processing the event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>touchPointID</code></td>
		 *    <td>A unique identification number (as an int) assigned to the touch point.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>timestamp</code></td>
		 *    <td>(AIR only) The timestamp of the event in milliseconds relative to the start of the application.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>touchIntent</code></td>
		 *    <td>(AIR only) A value from the TouchEventIntent class.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>isTouchPointCanceled</code></td>
		 *    <td>(AIR only) <code>true</code> if the touch event is canceled because of the device's touch-rejection logic.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/InteractiveObject.html#event:proximityEnd" target="">flash.display.InteractiveObject.proximityEnd</a>
		 * </div>
		 */
		public static const PROXIMITY_END:String = "proximityEnd";
		/**
		 * <p> Defines the value of the <code>type</code> property of a <code>PROXIMITY_MOVE</code> touch event object. </p>
		 * <p>The dispatched TouchEvent object has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>altKey</code></td>
		 *    <td><code>true</code> if the Alt key is active (Windows or Linux).</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>true</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>commandKey</code></td>
		 *    <td><code>true</code> on the Mac if the Command key is active; <code>false</code> if it is inactive. Always <code>false</code> on Windows.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>controlKey</code></td>
		 *    <td><code>true</code> if the Ctrl or Control key is active; <code>false</code> if it is inactive.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>ctrlKey</code></td>
		 *    <td><code>true</code> on Windows or Linux if the Ctrl key is active. <code>true</code> on Mac if either the Ctrl key or the Command key is active. Otherwise, <code>false</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>eventPhase</code></td>
		 *    <td>The current phase in the event flow.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>isRelatedObjectInaccessible</code></td>
		 *    <td><code>true</code> if the relatedObject property is set to <code>null</code> because of security sandbox rules.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>localX</code></td>
		 *    <td>The horizontal coordinate at which the event occurred relative to the containing sprite.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>localY</code></td>
		 *    <td>The vertical coordinate at which the event occurred relative to the containing sprite.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>pressure</code></td>
		 *    <td>A value between <code>0.0</code> and <code>1.0</code> indicating force of the contact with the device. If the device does not support detecting the pressure, the value is <code>1.0</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>relatedObject</code></td>
		 *    <td>A reference to a display list object related to the event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>shiftKey</code></td>
		 *    <td><code>true</code> if the Shift key is active; <code>false</code> if it is inactive.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>sizeX</code></td>
		 *    <td>Width of the contact area.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>sizeY</code></td>
		 *    <td>Height of the contact area.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>stageX</code></td>
		 *    <td>The horizontal coordinate at which the event occurred in global stage coordinates.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>stageY</code></td>
		 *    <td>The vertical coordinate at which the event occurred in global stage coordinates.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The InteractiveObject instance under the touching device. The <code>target</code> is not always the object in the display list that registered the event listener. Use the <code>currentTarget</code> property to access the object in the display list that is currently processing the event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>touchPointID</code></td>
		 *    <td>A unique identification number (as an int) assigned to the touch point.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>timestamp</code></td>
		 *    <td>(AIR only) The timestamp of the event in milliseconds relative to the start of the application.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>touchIntent</code></td>
		 *    <td>(AIR only) A value from the TouchEventIntent class.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>isTouchPointCanceled</code></td>
		 *    <td>(AIR only) <code>true</code> if the touch event is canceled because of the device's touch-rejection logic.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/InteractiveObject.html#event:proximityMove" target="">flash.display.InteractiveObject.proximityMove</a>
		 * </div>
		 */
		public static const PROXIMITY_MOVE:String = "proximityMove";
		/**
		 * <p> Defines the value of the <code>type</code> property of a <code>PROXIMITY_OUT</code> touch event object. </p>
		 * <p>The dispatched TouchEvent object has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>altKey</code></td>
		 *    <td><code>true</code> if the Alt key is active (Windows or Linux).</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>true</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>commandKey</code></td>
		 *    <td><code>true</code> on the Mac if the Command key is active; <code>false</code> if it is inactive. Always <code>false</code> on Windows.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>controlKey</code></td>
		 *    <td><code>true</code> if the Ctrl or Control key is active; <code>false</code> if it is inactive.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>ctrlKey</code></td>
		 *    <td><code>true</code> on Windows or Linux if the Ctrl key is active. <code>true</code> on Mac if either the Ctrl key or the Command key is active. Otherwise, <code>false</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>eventPhase</code></td>
		 *    <td>The current phase in the event flow.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>isRelatedObjectInaccessible</code></td>
		 *    <td><code>true</code> if the relatedObject property is set to <code>null</code> because of security sandbox rules.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>localX</code></td>
		 *    <td>The horizontal coordinate at which the event occurred relative to the containing sprite.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>localY</code></td>
		 *    <td>The vertical coordinate at which the event occurred relative to the containing sprite.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>pressure</code></td>
		 *    <td>A value between <code>0.0</code> and <code>1.0</code> indicating force of the contact with the device. If the device does not support detecting the pressure, the value is <code>1.0</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>relatedObject</code></td>
		 *    <td>A reference to a display list object related to the event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>shiftKey</code></td>
		 *    <td><code>true</code> if the Shift key is active; <code>false</code> if it is inactive.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>sizeX</code></td>
		 *    <td>Width of the contact area.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>sizeY</code></td>
		 *    <td>Height of the contact area.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>stageX</code></td>
		 *    <td>The horizontal coordinate at which the event occurred in global stage coordinates.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>stageY</code></td>
		 *    <td>The vertical coordinate at which the event occurred in global stage coordinates.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The InteractiveObject instance under the touching device. The <code>target</code> is not always the object in the display list that registered the event listener. Use the <code>currentTarget</code> property to access the object in the display list that is currently processing the event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>touchPointID</code></td>
		 *    <td>A unique identification number (as an int) assigned to the touch point.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>timestamp</code></td>
		 *    <td>(AIR only) The timestamp of the event in milliseconds relative to the start of the application.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>touchIntent</code></td>
		 *    <td>(AIR only) A value from the TouchEventIntent class.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>isTouchPointCanceled</code></td>
		 *    <td>(AIR only) <code>true</code> if the touch event is canceled because of the device's touch-rejection logic.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/InteractiveObject.html#event:proximityOUT" target="">flash.display.InteractiveObject.proximityOUT</a>
		 * </div>
		 */
		public static const PROXIMITY_OUT:String = "proximityOut";
		/**
		 * <p> Defines the value of the <code>type</code> property of a <code>PROXIMITY_OVER</code> touch event object. </p>
		 * <p>The dispatched TouchEvent object has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>altKey</code></td>
		 *    <td><code>true</code> if the Alt key is active (Windows or Linux).</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>true</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>commandKey</code></td>
		 *    <td><code>true</code> on the Mac if the Command key is active; <code>false</code> if it is inactive. Always <code>false</code> on Windows.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>controlKey</code></td>
		 *    <td><code>true</code> if the Ctrl or Control key is active; <code>false</code> if it is inactive.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>ctrlKey</code></td>
		 *    <td><code>true</code> on Windows or Linux if the Ctrl key is active. <code>true</code> on Mac if either the Ctrl key or the Command key is active. Otherwise, <code>false</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>eventPhase</code></td>
		 *    <td>The current phase in the event flow.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>isRelatedObjectInaccessible</code></td>
		 *    <td><code>true</code> if the relatedObject property is set to <code>null</code> because of security sandbox rules.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>localX</code></td>
		 *    <td>The horizontal coordinate at which the event occurred relative to the containing sprite.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>localY</code></td>
		 *    <td>The vertical coordinate at which the event occurred relative to the containing sprite.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>pressure</code></td>
		 *    <td>A value between <code>0.0</code> and <code>1.0</code> indicating force of the contact with the device. If the device does not support detecting the pressure, the value is <code>1.0</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>relatedObject</code></td>
		 *    <td>A reference to a display list object related to the event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>shiftKey</code></td>
		 *    <td><code>true</code> if the Shift key is active; <code>false</code> if it is inactive.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>sizeX</code></td>
		 *    <td>Width of the contact area.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>sizeY</code></td>
		 *    <td>Height of the contact area.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>stageX</code></td>
		 *    <td>The horizontal coordinate at which the event occurred in global stage coordinates.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>stageY</code></td>
		 *    <td>The vertical coordinate at which the event occurred in global stage coordinates.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The InteractiveObject instance under the touching device. The <code>target</code> is not always the object in the display list that registered the event listener. Use the <code>currentTarget</code> property to access the object in the display list that is currently processing the event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>touchPointID</code></td>
		 *    <td>A unique identification number (as an int) assigned to the touch point.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>timestamp</code></td>
		 *    <td>(AIR only) The timestamp of the event in milliseconds relative to the start of the application.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>touchIntent</code></td>
		 *    <td>(AIR only) A value from the TouchEventIntent class.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>isTouchPointCanceled</code></td>
		 *    <td>(AIR only) <code>true</code> if the touch event is canceled because of the device's touch-rejection logic.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/InteractiveObject.html#event:proximityOver" target="">flash.display.InteractiveObject.proximityOver</a>
		 * </div>
		 */
		public static const PROXIMITY_OVER:String = "proximityOver";
		/**
		 * <p> Defines the value of the <code>type</code> property of a <code>PROXIMITY_ROLL_OUT</code> touch event object. </p>
		 * <p>The dispatched TouchEvent object has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>altKey</code></td>
		 *    <td><code>true</code> if the Alt key is active (Windows or Linux).</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>true</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>commandKey</code></td>
		 *    <td><code>true</code> on the Mac if the Command key is active; <code>false</code> if it is inactive. Always <code>false</code> on Windows.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>controlKey</code></td>
		 *    <td><code>true</code> if the Ctrl or Control key is active; <code>false</code> if it is inactive.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>ctrlKey</code></td>
		 *    <td><code>true</code> on Windows or Linux if the Ctrl key is active. <code>true</code> on Mac if either the Ctrl key or the Command key is active. Otherwise, <code>false</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>eventPhase</code></td>
		 *    <td>The current phase in the event flow.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>isRelatedObjectInaccessible</code></td>
		 *    <td><code>true</code> if the relatedObject property is set to <code>null</code> because of security sandbox rules.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>localX</code></td>
		 *    <td>The horizontal coordinate at which the event occurred relative to the containing sprite.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>localY</code></td>
		 *    <td>The vertical coordinate at which the event occurred relative to the containing sprite.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>pressure</code></td>
		 *    <td>A value between <code>0.0</code> and <code>1.0</code> indicating force of the contact with the device. If the device does not support detecting the pressure, the value is <code>1.0</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>relatedObject</code></td>
		 *    <td>A reference to a display list object related to the event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>shiftKey</code></td>
		 *    <td><code>true</code> if the Shift key is active; <code>false</code> if it is inactive.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>sizeX</code></td>
		 *    <td>Width of the contact area.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>sizeY</code></td>
		 *    <td>Height of the contact area.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>stageX</code></td>
		 *    <td>The horizontal coordinate at which the event occurred in global stage coordinates.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>stageY</code></td>
		 *    <td>The vertical coordinate at which the event occurred in global stage coordinates.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The InteractiveObject instance under the touching device. The <code>target</code> is not always the object in the display list that registered the event listener. Use the <code>currentTarget</code> property to access the object in the display list that is currently processing the event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>touchPointID</code></td>
		 *    <td>A unique identification number (as an int) assigned to the touch point.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>timestamp</code></td>
		 *    <td>(AIR only) The timestamp of the event in milliseconds relative to the start of the application.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>touchIntent</code></td>
		 *    <td>(AIR only) A value from the TouchEventIntent class.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>isTouchPointCanceled</code></td>
		 *    <td>(AIR only) <code>true</code> if the touch event is canceled because of the device's touch-rejection logic.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/InteractiveObject.html#event:proximityRollOut" target="">flash.display.InteractiveObject.proximityRollOut</a>
		 * </div>
		 */
		public static const PROXIMITY_ROLL_OUT:String = "proximityRollOut";
		/**
		 * <p> Defines the value of the <code>type</code> property of a <code>PROXIMITY_ROLL_OVER</code> touch event object. </p>
		 * <p>The dispatched TouchEvent object has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>altKey</code></td>
		 *    <td><code>true</code> if the Alt key is active (Windows or Linux).</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>true</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>commandKey</code></td>
		 *    <td><code>true</code> on the Mac if the Command key is active; <code>false</code> if it is inactive. Always <code>false</code> on Windows.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>controlKey</code></td>
		 *    <td><code>true</code> if the Ctrl or Control key is active; <code>false</code> if it is inactive.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>ctrlKey</code></td>
		 *    <td><code>true</code> on Windows or Linux if the Ctrl key is active. <code>true</code> on Mac if either the Ctrl key or the Command key is active. Otherwise, <code>false</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>eventPhase</code></td>
		 *    <td>The current phase in the event flow.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>isRelatedObjectInaccessible</code></td>
		 *    <td><code>true</code> if the relatedObject property is set to <code>null</code> because of security sandbox rules.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>localX</code></td>
		 *    <td>The horizontal coordinate at which the event occurred relative to the containing sprite.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>localY</code></td>
		 *    <td>The vertical coordinate at which the event occurred relative to the containing sprite.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>pressure</code></td>
		 *    <td>A value between <code>0.0</code> and <code>1.0</code> indicating force of the contact with the device. If the device does not support detecting the pressure, the value is <code>1.0</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>relatedObject</code></td>
		 *    <td>A reference to a display list object related to the event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>shiftKey</code></td>
		 *    <td><code>true</code> if the Shift key is active; <code>false</code> if it is inactive.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>sizeX</code></td>
		 *    <td>Width of the contact area.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>sizeY</code></td>
		 *    <td>Height of the contact area.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>stageX</code></td>
		 *    <td>The horizontal coordinate at which the event occurred in global stage coordinates.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>stageY</code></td>
		 *    <td>The vertical coordinate at which the event occurred in global stage coordinates.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The InteractiveObject instance under the touching device. The <code>target</code> is not always the object in the display list that registered the event listener. Use the <code>currentTarget</code> property to access the object in the display list that is currently processing the event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>touchPointID</code></td>
		 *    <td>A unique identification number (as an int) assigned to the touch point.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>timestamp</code></td>
		 *    <td>(AIR only) The timestamp of the event in milliseconds relative to the start of the application.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>touchIntent</code></td>
		 *    <td>(AIR only) A value from the TouchEventIntent class.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>isTouchPointCanceled</code></td>
		 *    <td>(AIR only) <code>true</code> if the touch event is canceled because of the device's touch-rejection logic.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/InteractiveObject.html#event:proximityRollOver" target="">flash.display.InteractiveObject.proximityRollOver</a>
		 * </div>
		 */
		public static const PROXIMITY_ROLL_OVER:String = "proximityRollOver";
		/**
		 * <p> Defines the value of the <code>type</code> property of a <code>TOUCH_BEGIN</code> touch event object. </p>
		 * <p>The dispatched TouchEvent object has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>altKey</code></td>
		 *    <td><code>true</code> if the Alt key is active (Windows or Linux).</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>true</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>commandKey</code></td>
		 *    <td><code>true</code> on the Mac if the Command key is active; <code>false</code> if it is inactive. Always <code>false</code> on Windows.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>controlKey</code></td>
		 *    <td><code>true</code> if the Ctrl or Control key is active; <code>false</code> if it is inactive.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>ctrlKey</code></td>
		 *    <td><code>true</code> on Windows or Linux if the Ctrl key is active. <code>true</code> on Mac if either the Ctrl key or the Command key is active. Otherwise, <code>false</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>eventPhase</code></td>
		 *    <td>The current phase in the event flow.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>isRelatedObjectInaccessible</code></td>
		 *    <td><code>true</code> if the relatedObject property is set to <code>null</code> because of security sandbox rules.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>localX</code></td>
		 *    <td>The horizontal coordinate at which the event occurred relative to the containing sprite.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>localY</code></td>
		 *    <td>The vertical coordinate at which the event occurred relative to the containing sprite.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>pressure</code></td>
		 *    <td>A value between <code>0.0</code> and <code>1.0</code> indicating force of the contact with the device. If the device does not support detecting the pressure, the value is <code>1.0</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>relatedObject</code></td>
		 *    <td>A reference to a display list object related to the event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>shiftKey</code></td>
		 *    <td><code>true</code> if the Shift key is active; <code>false</code> if it is inactive.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>sizeX</code></td>
		 *    <td>Width of the contact area.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>sizeY</code></td>
		 *    <td>Height of the contact area.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>stageX</code></td>
		 *    <td>The horizontal coordinate at which the event occurred in global stage coordinates.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>stageY</code></td>
		 *    <td>The vertical coordinate at which the event occurred in global stage coordinates.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The InteractiveObject instance under the touching device. The <code>target</code> is not always the object in the display list that registered the event listener. Use the <code>currentTarget</code> property to access the object in the display list that is currently processing the event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>touchPointID</code></td>
		 *    <td>A unique identification number (as an int) assigned to the touch point.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/InteractiveObject.html#event:touchBegin" target="">flash.display.InteractiveObject.touchBegin</a>
		 * </div>
		 */
		public static const TOUCH_BEGIN:String = "touchBegin";
		/**
		 * <p> Defines the value of the <code>type</code> property of a <code>TOUCH_END</code> touch event object. </p>
		 * <p>The dispatched TouchEvent object has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>altKey</code></td>
		 *    <td><code>true</code> if the Alt key is active (Windows or Linux).</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>true</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>commandKey</code></td>
		 *    <td><code>true</code> on the Mac if the Command key is active; <code>false</code> if it is inactive. Always <code>false</code> on Windows.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>controlKey</code></td>
		 *    <td><code>true</code> if the Ctrl or Control key is active; <code>false</code> if it is inactive.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>ctrlKey</code></td>
		 *    <td><code>true</code> on Windows or Linux if the Ctrl key is active. <code>true</code> on Mac if either the Ctrl key or the Command key is active. Otherwise, <code>false</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>eventPhase</code></td>
		 *    <td>The current phase in the event flow.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>isRelatedObjectInaccessible</code></td>
		 *    <td><code>true</code> if the relatedObject property is set to <code>null</code> because of security sandbox rules.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>localX</code></td>
		 *    <td>The horizontal coordinate at which the event occurred relative to the containing sprite.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>localY</code></td>
		 *    <td>The vertical coordinate at which the event occurred relative to the containing sprite.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>pressure</code></td>
		 *    <td>A value between <code>0.0</code> and <code>1.0</code> indicating force of the contact with the device. If the device does not support detecting the pressure, the value is <code>1.0</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>relatedObject</code></td>
		 *    <td>A reference to a display list object related to the event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>shiftKey</code></td>
		 *    <td><code>true</code> if the Shift key is active; <code>false</code> if it is inactive.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>sizeX</code></td>
		 *    <td>Width of the contact area.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>sizeY</code></td>
		 *    <td>Height of the contact area.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>stageX</code></td>
		 *    <td>The horizontal coordinate at which the event occurred in global stage coordinates.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>stageY</code></td>
		 *    <td>The vertical coordinate at which the event occurred in global stage coordinates.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The InteractiveObject instance under the touching device. The <code>target</code> is not always the object in the display list that registered the event listener. Use the <code>currentTarget</code> property to access the object in the display list that is currently processing the event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>touchPointID</code></td>
		 *    <td>A unique identification number (as an int) assigned to the touch point.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/InteractiveObject.html#event:touchEnd" target="">flash.display.InteractiveObject.touchEnd</a>
		 * </div>
		 */
		public static const TOUCH_END:String = "touchEnd";
		/**
		 * <p> Defines the value of the <code>type</code> property of a <code>TOUCH_MOVE</code> touch event object. </p>
		 * <p>The dispatched TouchEvent object has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>altKey</code></td>
		 *    <td><code>true</code> if the Alt key is active (Windows or Linux).</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>true</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>commandKey</code></td>
		 *    <td><code>true</code> on the Mac if the Command key is active; <code>false</code> if it is inactive. Always <code>false</code> on Windows.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>controlKey</code></td>
		 *    <td><code>true</code> if the Ctrl or Control key is active; <code>false</code> if it is inactive.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>ctrlKey</code></td>
		 *    <td><code>true</code> on Windows or Linux if the Ctrl key is active. <code>true</code> on Mac if either the Ctrl key or the Command key is active. Otherwise, <code>false</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>eventPhase</code></td>
		 *    <td>The current phase in the event flow.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>isRelatedObjectInaccessible</code></td>
		 *    <td><code>true</code> if the relatedObject property is set to <code>null</code> because of security sandbox rules.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>localX</code></td>
		 *    <td>The horizontal coordinate at which the event occurred relative to the containing sprite.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>localY</code></td>
		 *    <td>The vertical coordinate at which the event occurred relative to the containing sprite.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>pressure</code></td>
		 *    <td>A value between <code>0.0</code> and <code>1.0</code> indicating force of the contact with the device. If the device does not support detecting the pressure, the value is <code>1.0</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>relatedObject</code></td>
		 *    <td>A reference to a display list object related to the event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>shiftKey</code></td>
		 *    <td><code>true</code> if the Shift key is active; <code>false</code> if it is inactive.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>sizeX</code></td>
		 *    <td>Width of the contact area.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>sizeY</code></td>
		 *    <td>Height of the contact area.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>stageX</code></td>
		 *    <td>The horizontal coordinate at which the event occurred in global stage coordinates.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>stageY</code></td>
		 *    <td>The vertical coordinate at which the event occurred in global stage coordinates.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The InteractiveObject instance under the touching device. The <code>target</code> is not always the object in the display list that registered the event listener. Use the <code>currentTarget</code> property to access the object in the display list that is currently processing the event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>touchPointID</code></td>
		 *    <td>A unique identification number (as an int) assigned to the touch point.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/InteractiveObject.html#event:touchMove" target="">flash.display.InteractiveObject.touchMove</a>
		 * </div>
		 */
		public static const TOUCH_MOVE:String = "touchMove";
		/**
		 * <p> Defines the value of the <code>type</code> property of a <code>TOUCH_OUT</code> touch event object. </p>
		 * <p>The dispatched TouchEvent object has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>altKey</code></td>
		 *    <td><code>true</code> if the Alt key is active (Windows or Linux).</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>true</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>commandKey</code></td>
		 *    <td><code>true</code> on the Mac if the Command key is active; <code>false</code> if it is inactive. Always <code>false</code> on Windows.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>controlKey</code></td>
		 *    <td><code>true</code> if the Ctrl or Control key is active; <code>false</code> if it is inactive.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>ctrlKey</code></td>
		 *    <td><code>true</code> on Windows or Linux if the Ctrl key is active. <code>true</code> on Mac if either the Ctrl key or the Command key is active. Otherwise, <code>false</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>eventPhase</code></td>
		 *    <td>The current phase in the event flow.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>isRelatedObjectInaccessible</code></td>
		 *    <td><code>true</code> if the relatedObject property is set to <code>null</code> because of security sandbox rules.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>localX</code></td>
		 *    <td>The horizontal coordinate at which the event occurred relative to the containing sprite.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>localY</code></td>
		 *    <td>The vertical coordinate at which the event occurred relative to the containing sprite.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>pressure</code></td>
		 *    <td>A value between <code>0.0</code> and <code>1.0</code> indicating force of the contact with the device. If the device does not support detecting the pressure, the value is <code>1.0</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>relatedObject</code></td>
		 *    <td>A reference to a display list object related to the event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>shiftKey</code></td>
		 *    <td><code>true</code> if the Shift key is active; <code>false</code> if it is inactive.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>sizeX</code></td>
		 *    <td>Width of the contact area.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>sizeY</code></td>
		 *    <td>Height of the contact area.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>stageX</code></td>
		 *    <td>The horizontal coordinate at which the event occurred in global stage coordinates.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>stageY</code></td>
		 *    <td>The vertical coordinate at which the event occurred in global stage coordinates.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The InteractiveObject instance under the touching device. The <code>target</code> is not always the object in the display list that registered the event listener. Use the <code>currentTarget</code> property to access the object in the display list that is currently processing the event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>touchPointID</code></td>
		 *    <td>A unique identification number (as an int) assigned to the touch point.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/InteractiveObject.html#event:touchOut" target="">flash.display.InteractiveObject.touchOut</a>
		 * </div>
		 */
		public static const TOUCH_OUT:String = "touchOut";
		/**
		 * <p> Defines the value of the <code>type</code> property of a <code>TOUCH_OVER</code> touch event object. </p>
		 * <p>The dispatched TouchEvent object has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>altKey</code></td>
		 *    <td><code>true</code> if the Alt key is active (Windows or Linux).</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>true</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>commandKey</code></td>
		 *    <td><code>true</code> on the Mac if the Command key is active; <code>false</code> if it is inactive. Always <code>false</code> on Windows.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>controlKey</code></td>
		 *    <td><code>true</code> if the Ctrl or Control key is active; <code>false</code> if it is inactive.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>ctrlKey</code></td>
		 *    <td><code>true</code> on Windows or Linux if the Ctrl key is active. <code>true</code> on Mac if either the Ctrl key or the Command key is active. Otherwise, <code>false</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>eventPhase</code></td>
		 *    <td>The current phase in the event flow.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>isRelatedObjectInaccessible</code></td>
		 *    <td><code>true</code> if the relatedObject property is set to <code>null</code> because of security sandbox rules.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>localX</code></td>
		 *    <td>The horizontal coordinate at which the event occurred relative to the containing sprite.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>localY</code></td>
		 *    <td>The vertical coordinate at which the event occurred relative to the containing sprite.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>pressure</code></td>
		 *    <td>A value between <code>0.0</code> and <code>1.0</code> indicating force of the contact with the device. If the device does not support detecting the pressure, the value is <code>1.0</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>relatedObject</code></td>
		 *    <td>A reference to a display list object related to the event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>shiftKey</code></td>
		 *    <td><code>true</code> if the Shift key is active; <code>false</code> if it is inactive.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>sizeX</code></td>
		 *    <td>Width of the contact area.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>sizeY</code></td>
		 *    <td>Height of the contact area.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>stageX</code></td>
		 *    <td>The horizontal coordinate at which the event occurred in global stage coordinates.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>stageY</code></td>
		 *    <td>The vertical coordinate at which the event occurred in global stage coordinates.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The InteractiveObject instance under the touching device. The <code>target</code> is not always the object in the display list that registered the event listener. Use the <code>currentTarget</code> property to access the object in the display list that is currently processing the event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>touchPointID</code></td>
		 *    <td>A unique identification number (as an int) assigned to the touch point.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/InteractiveObject.html#event:touchOver" target="">flash.display.InteractiveObject.touchOver</a>
		 * </div>
		 */
		public static const TOUCH_OVER:String = "touchOver";
		/**
		 * <p> Defines the value of the <code>type</code> property of a <code>TOUCH_ROLL_OUT</code> touch event object. </p>
		 * <p>The dispatched TouchEvent object has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>altKey</code></td>
		 *    <td><code>true</code> if the Alt key is active (Windows or Linux).</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>true</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>commandKey</code></td>
		 *    <td><code>true</code> on the Mac if the Command key is active; <code>false</code> if it is inactive. Always <code>false</code> on Windows.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>controlKey</code></td>
		 *    <td><code>true</code> if the Ctrl or Control key is active; <code>false</code> if it is inactive.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>ctrlKey</code></td>
		 *    <td><code>true</code> on Windows or Linux if the Ctrl key is active. <code>true</code> on Mac if either the Ctrl key or the Command key is active. Otherwise, <code>false</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>eventPhase</code></td>
		 *    <td>The current phase in the event flow.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>isRelatedObjectInaccessible</code></td>
		 *    <td><code>true</code> if the relatedObject property is set to <code>null</code> because of security sandbox rules.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>localX</code></td>
		 *    <td>The horizontal coordinate at which the event occurred relative to the containing sprite.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>localY</code></td>
		 *    <td>The vertical coordinate at which the event occurred relative to the containing sprite.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>pressure</code></td>
		 *    <td>A value between <code>0.0</code> and <code>1.0</code> indicating force of the contact with the device. If the device does not support detecting the pressure, the value is <code>1.0</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>relatedObject</code></td>
		 *    <td>A reference to a display list object related to the event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>shiftKey</code></td>
		 *    <td><code>true</code> if the Shift key is active; <code>false</code> if it is inactive.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>sizeX</code></td>
		 *    <td>Width of the contact area.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>sizeY</code></td>
		 *    <td>Height of the contact area.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>stageX</code></td>
		 *    <td>The horizontal coordinate at which the event occurred in global stage coordinates.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>stageY</code></td>
		 *    <td>The vertical coordinate at which the event occurred in global stage coordinates.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The InteractiveObject instance under the touching device. The <code>target</code> is not always the object in the display list that registered the event listener. Use the <code>currentTarget</code> property to access the object in the display list that is currently processing the event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>touchPointID</code></td>
		 *    <td>A unique identification number (as an int) assigned to the touch point.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/InteractiveObject.html#event:touchRollOut" target="">flash.display.InteractiveObject.touchRollOut</a>
		 * </div>
		 */
		public static const TOUCH_ROLL_OUT:String = "touchRollOut";
		/**
		 * <p> Defines the value of the <code>type</code> property of a <code>TOUCH_ROLL_OVER</code> touch event object. </p>
		 * <p>The dispatched TouchEvent object has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>altKey</code></td>
		 *    <td><code>true</code> if the Alt key is active (Windows or Linux).</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>true</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>commandKey</code></td>
		 *    <td><code>true</code> on the Mac if the Command key is active; <code>false</code> if it is inactive. Always <code>false</code> on Windows.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>controlKey</code></td>
		 *    <td><code>true</code> if the Ctrl or Control key is active; <code>false</code> if it is inactive.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>ctrlKey</code></td>
		 *    <td><code>true</code> on Windows or Linux if the Ctrl key is active. <code>true</code> on Mac if either the Ctrl key or the Command key is active. Otherwise, <code>false</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>eventPhase</code></td>
		 *    <td>The current phase in the event flow.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>isRelatedObjectInaccessible</code></td>
		 *    <td><code>true</code> if the relatedObject property is set to <code>null</code> because of security sandbox rules.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>localX</code></td>
		 *    <td>The horizontal coordinate at which the event occurred relative to the containing sprite.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>localY</code></td>
		 *    <td>The vertical coordinate at which the event occurred relative to the containing sprite.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>pressure</code></td>
		 *    <td>A value between <code>0.0</code> and <code>1.0</code> indicating force of the contact with the device. If the device does not support detecting the pressure, the value is <code>1.0</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>relatedObject</code></td>
		 *    <td>A reference to a display list object related to the event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>shiftKey</code></td>
		 *    <td><code>true</code> if the Shift key is active; <code>false</code> if it is inactive.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>sizeX</code></td>
		 *    <td>Width of the contact area.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>sizeY</code></td>
		 *    <td>Height of the contact area.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>stageX</code></td>
		 *    <td>The horizontal coordinate at which the event occurred in global stage coordinates.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>stageY</code></td>
		 *    <td>The vertical coordinate at which the event occurred in global stage coordinates.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The InteractiveObject instance under the touching device. The <code>target</code> is not always the object in the display list that registered the event listener. Use the <code>currentTarget</code> property to access the object in the display list that is currently processing the event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>touchPointID</code></td>
		 *    <td>A unique identification number (as an int) assigned to the touch point.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/InteractiveObject.html#event:touchRollOver" target="">flash.display.InteractiveObject.touchRollOver</a>
		 * </div>
		 */
		public static const TOUCH_ROLL_OVER:String = "touchRollOver";
		/**
		 * <p> Defines the value of the <code>type</code> property of a <code>TOUCH_TAP</code> touch event object. </p>
		 * <p>The dispatched TouchEvent object has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>altKey</code></td>
		 *    <td><code>true</code> if the Alt key is active (Windows or Linux).</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>true</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>commandKey</code></td>
		 *    <td><code>true</code> on the Mac if the Command key is active; <code>false</code> if it is inactive. Always <code>false</code> on Windows.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>controlKey</code></td>
		 *    <td><code>true</code> if the Ctrl or Control key is active; <code>false</code> if it is inactive.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>ctrlKey</code></td>
		 *    <td><code>true</code> on Windows or Linux if the Ctrl key is active. <code>true</code> on Mac if either the Ctrl key or the Command key is active. Otherwise, <code>false</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>eventPhase</code></td>
		 *    <td>The current phase in the event flow.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>isRelatedObjectInaccessible</code></td>
		 *    <td><code>true</code> if the relatedObject property is set to <code>null</code> because of security sandbox rules.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>localX</code></td>
		 *    <td>The horizontal coordinate at which the event occurred relative to the containing sprite.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>localY</code></td>
		 *    <td>The vertical coordinate at which the event occurred relative to the containing sprite.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>pressure</code></td>
		 *    <td>A value between <code>0.0</code> and <code>1.0</code> indicating force of the contact with the device. If the device does not support detecting the pressure, the value is <code>1.0</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>relatedObject</code></td>
		 *    <td>A reference to a display list object related to the event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>shiftKey</code></td>
		 *    <td><code>true</code> if the Shift key is active; <code>false</code> if it is inactive.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>sizeX</code></td>
		 *    <td>Width of the contact area.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>sizeY</code></td>
		 *    <td>Height of the contact area.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>stageX</code></td>
		 *    <td>The horizontal coordinate at which the event occurred in global stage coordinates.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>stageY</code></td>
		 *    <td>The vertical coordinate at which the event occurred in global stage coordinates.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The InteractiveObject instance under the touching device. The <code>target</code> is not always the object in the display list that registered the event listener. Use the <code>currentTarget</code> property to access the object in the display list that is currently processing the event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>touchPointID</code></td>
		 *    <td>A unique identification number (as an int) assigned to the touch point.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/InteractiveObject.html#event:touchTap" target="">flash.display.InteractiveObject.touchTap</a>
		 * </div>
		 * <span id="pageFilter"><br><span class="label"> Example &nbsp;( <span class="usage"><a href="http://www.adobe.com/go/learn_as3_usingexamples_en"> How to use this example </a></span>) </span><br><br>
		 *  <div class="detailBody">
		 *    The following example displays a message when the square drawn on mySprite is tapped on a touch-enabled screen: 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>
		 * Multitouch.inputMode=MultitouchInputMode.TOUCH_POINT;
		 * 
		 * var mySprite:Sprite = new Sprite();
		 * var myTextField:TextField = new TextField();
		 * 
		 * mySprite.graphics.beginFill(0x336699);
		 * mySprite.graphics.drawRect(0,0,40,40);
		 * addChild(mySprite);
		 * 
		 * mySprite.addEventListener(TouchEvent.TOUCH_TAP, taphandler);
		 * 
		 * function taphandler(e:TouchEvent): void {
		 *     myTextField.text = "I've been tapped";
		 *     myTextField.y = 50;
		 *     addChild(myTextField);
		 * }
		 * </pre>
		 *   </div>
		 *  </div></span>
		 */
		public static const TOUCH_TAP:String = "touchTap";

		private var _altKey:Boolean;
		private var _commandKey:Boolean;
		private var _controlKey:Boolean;
		private var _ctrlKey:Boolean;
		private var _isPrimaryTouchPoint:Boolean;
		private var _isRelatedObjectInaccessible:Boolean;
		private var _isTouchPointCanceled:Boolean;
		private var _localX:Number;
		private var _localY:Number;
		private var _pressure:Number;
		private var _relatedObject:InteractiveObject;
		private var _shiftKey:Boolean;
		private var _sizeX:Number;
		private var _sizeY:Number;
		private var _timestamp:Number;
		private var _touchIntent:String;
		private var _touchPointID:int;

		private var _stageX:Number;
		private var _stageY:Number;

		/**
		 * <p> Creates an Event object that contains information about touch events. Event objects are passed as parameters to event listeners. </p>
		 * 
		 * @param type  — The type of the event. Possible values are: <code>TouchEvent.PROXIMITY_BEGIN</code>, <code>TouchEvent.PROXIMITY_END</code>, <code>TouchEvent.PROXIMITY_MOVE</code>, <code>TouchEvent.PROXIMITY_OUT</code>, <code>TouchEvent.PROXIMITY_OVER</code>, <code>TouchEvent.PROXIMITY_ROLL_OUT</code>, <code>TouchEvent.PROXIMITY_ROLL_OVER</code>, <code>TouchEvent.TOUCH_BEGIN</code>, <code>TouchEvent.TOUCH_END</code>, <code>TouchEvent.TOUCH_MOVE</code>, <code>TouchEvent.TOUCH_OUT</code>, <code>TouchEvent.TOUCH_OVER</code>, <code>TouchEvent.TOUCH_ROLL_OUT</code>, <code>TouchEvent.TOUCH_ROLL_OVER</code>, and <code>TouchEvent.TOUCH_TAP</code>. 
		 * @param bubbles  — Determines whether the Event object participates in the bubbling phase of the event flow. 
		 * @param cancelable  — Determines whether the Event object can be canceled. 
		 * @param touchPointID  — A unique identification number (as an int) assigned to the touch point. 
		 * @param isPrimaryTouchPoint  — Indicates whether the first point of contact is mapped to mouse events. 
		 * @param localX  — The horizontal coordinate at which the event occurred relative to the containing sprite. 
		 * @param localY  — The vertical coordinate at which the event occurred relative to the containing sprite. 
		 * @param sizeX  — Width of the contact area. 
		 * @param sizeY  — Height of the contact area. 
		 * @param pressure  — A value between <code>0.0</code> and <code>1.0</code> indicating force of the contact with the device. If the device does not support detecting the pressure, the value is <code>1.0</code>. 
		 * @param relatedObject  — The complementary InteractiveObject instance that is affected by the event. For example, when a <code>touchOut</code> event occurs, <code>relatedObject</code> represents the display list object to which the pointing device now points. 
		 * @param ctrlKey  — On Windows or Linux, indicates whether the Ctrl key is activated. On Mac, indicates whether either the Ctrl key or the Command key is activated. 
		 * @param altKey  — Indicates whether the Alt key is activated (Windows or Linux only). 
		 * @param shiftKey  — Indicates whether the Shift key is activated. 
		 * @param commandKey  — (AIR only) Indicates whether the Command key is activated (Mac only). This parameter is for Adobe AIR only; do not set it for Flash Player content. 
		 * @param controlKey  — (AIR only) Indicates whether the Control or Ctrl key is activated. This parameter is for Adobe AIR only; do not set it for Flash Player content. 
		 * @param timestamp  — (AIR only) The timestamp of the event in milliseconds relative to the start of the application. 
		 * @param touchIntent  — (AIR only) A value from the TouchEventIntent class. 
		 * @param samples  — (AIR only) A ByteArray object containing position and pressure readings for each intermediate sample recorded since the last touch or proximity event in a sequence. 
		 * @param isTouchPointCanceled  — (AIR only) <code>true</code> if the touch event is canceled because of a rejected touch type. 
		 */
		public function TouchEvent(type:String, bubbles:Boolean = true, cancelable:Boolean = false, touchPointID:int = 0, isPrimaryTouchPoint:Boolean = false, localX:Number = NaN, localY:Number = NaN, sizeX:Number = NaN, sizeY:Number = NaN, pressure:Number = NaN, relatedObject:InteractiveObject = null, ctrlKey:Boolean = false, altKey:Boolean = false, shiftKey:Boolean = false, commandKey:Boolean = false, controlKey:Boolean = false, timestamp:Number = NaN, touchIntent:String = null, samples:ByteArray = null, isTouchPointCanceled:Boolean = false) {
			super(type, bubbles, cancelable);
			throw new Error("Not implemented");
		}

		/**
		 * <p> Indicates whether the Alt key is active (<code>true</code>) or inactive (<code>false</code>). Supported for Windows and Linux operating systems only. </p>
		 * 
		 * @return 
		 */
		public function get altKey():Boolean {
			return _altKey;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set altKey(value:Boolean):void {
			_altKey = value;
		}

		/**
		 * <p> Indicates whether the command key is activated (Mac only). </p>
		 * <p>On a Mac OS, the value of the <code>commandKey</code> property is the same value as the <code>ctrlKey</code> property. This property is always false on Windows or Linux.</p>
		 * 
		 * @return 
		 */
		public function get commandKey():Boolean {
			return _commandKey;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set commandKey(value:Boolean):void {
			_commandKey = value;
		}

		/**
		 * <p> Indicates whether the Control key is activated on Mac and whether the Ctrl key is activated on Windows or Linux. </p>
		 * 
		 * @return 
		 */
		public function get controlKey():Boolean {
			return _controlKey;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set controlKey(value:Boolean):void {
			_controlKey = value;
		}

		/**
		 * <p> On Windows or Linux, indicates whether the Ctrl key is active (<code>true</code>) or inactive (<code>false</code>). On Macintosh, indicates whether either the Control key or the Command key is activated. </p>
		 * 
		 * @return 
		 */
		public function get ctrlKey():Boolean {
			return _ctrlKey;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set ctrlKey(value:Boolean):void {
			_ctrlKey = value;
		}

		/**
		 * <p> Indicates whether the first point of contact is mapped to mouse events. </p>
		 * 
		 * @return 
		 */
		public function get isPrimaryTouchPoint():Boolean {
			return _isPrimaryTouchPoint;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set isPrimaryTouchPoint(value:Boolean):void {
			_isPrimaryTouchPoint = value;
		}

		/**
		 * <p> If <code>true</code>, the <code>relatedObject</code> property is set to <code>null</code> for reasons related to security sandboxes. If the nominal value of <code>relatedObject</code> is a reference to a DisplayObject in another sandbox, <code>relatedObject</code> is set to <code>null</code> unless there is permission in both directions across this sandbox boundary. Permission is established by calling <code>Security.allowDomain()</code> from a SWF file, or by providing a policy file from the server of an image file, and setting the <code>LoaderContext.checkPolicyFile</code> property when loading the image. </p>
		 * 
		 * @return 
		 */
		public function get isRelatedObjectInaccessible():Boolean {
			return _isRelatedObjectInaccessible;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set isRelatedObjectInaccessible(value:Boolean):void {
			_isRelatedObjectInaccessible = value;
		}

		/**
		 * <p> Reports that this touch input sequence was canceled by the operating system. </p>
		 * <p>Touch events can be canceled for a variety of reasons, such as when a palm-generated touch is rejected because a pen stylus is in use. <code>isTouchPointCanceled</code> can only be <code>true</code> in a <code>touchEnd</code> event.</p>
		 * 
		 * @return 
		 */
		public function get isTouchPointCanceled():Boolean {
			return _isTouchPointCanceled;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set isTouchPointCanceled(value:Boolean):void {
			_isTouchPointCanceled = value;
		}

		/**
		 * <p> The horizontal coordinate at which the event occurred relative to the containing sprite. </p>
		 * 
		 * @return 
		 */
		public function get localX():Number {
			return _localX;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set localX(value:Number):void {
			_localX = value;
		}

		/**
		 * <p> The vertical coordinate at which the event occurred relative to the containing sprite. </p>
		 * 
		 * @return 
		 */
		public function get localY():Number {
			return _localY;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set localY(value:Number):void {
			_localY = value;
		}

		/**
		 * <p> A value between <code>0.0</code> and <code>1.0</code> indicating force of the contact with the device. If the device does not support detecting the pressure, the value is <code>1.0</code>. </p>
		 * 
		 * @return 
		 */
		public function get pressure():Number {
			return _pressure;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set pressure(value:Number):void {
			_pressure = value;
		}

		/**
		 * <p> A reference to a display list object that is related to the event. For example, when a <code>touchOut</code> event occurs, <code>relatedObject</code> represents the display list object to which the pointing device now points. This property applies to the <code>touchOut</code>, <code>touchOver</code>, <code>touchRollOut</code>, and <code>touchRollOver</code> events. </p>
		 * <p>The value of this property can be <code>null</code> in two circumstances: if there is no related object, or there is a related object, but it is in a security sandbox to which you don't have access. Use the <code>isRelatedObjectInaccessible()</code> property to determine which of these reasons applies.</p>
		 * 
		 * @return 
		 */
		public function get relatedObject():InteractiveObject {
			return _relatedObject;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set relatedObject(value:InteractiveObject):void {
			_relatedObject = value;
		}

		/**
		 * <p> Indicates whether the Shift key is active (<code>true</code>) or inactive (<code>false</code>). </p>
		 * 
		 * @return 
		 */
		public function get shiftKey():Boolean {
			return _shiftKey;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set shiftKey(value:Boolean):void {
			_shiftKey = value;
		}

		/**
		 * <p> Width of the contact area. </p>
		 * 
		 * @return 
		 */
		public function get sizeX():Number {
			return _sizeX;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set sizeX(value:Number):void {
			_sizeX = value;
		}

		/**
		 * <p> Height of the contact area. </p>
		 * 
		 * @return 
		 */
		public function get sizeY():Number {
			return _sizeY;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set sizeY(value:Number):void {
			_sizeY = value;
		}

		/**
		 * <p> The horizontal coordinate at which the event occurred in global Stage coordinates. This property is calculated when the <code>localX</code> property is set. </p>
		 * 
		 * @return 
		 */
		public function get stageX():Number {
			return _stageX;
		}

		/**
		 * <p> The vertical coordinate at which the event occurred in global Stage coordinates. This property is calculated when the <code>localY</code> property is set. </p>
		 * 
		 * @return 
		 */
		public function get stageY():Number {
			return _stageY;
		}

		/**
		 * <p> Reports the time of the event in relative milliseconds. </p>
		 * <p>The times reported are relative to the time the application started execution. For events that return a sequence of points in the samples buffer, this timestamp reflects the elapsed time of the last sample in the buffer.</p>
		 * 
		 * @return 
		 */
		public function get timestamp():Number {
			return _timestamp;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set timestamp(value:Number):void {
			_timestamp = value;
		}

		/**
		 * <p> Reports whether the touch was generated by the primary or the eraser end of a stylus. </p>
		 * <p>If a touch event is generated by a finger or other non-stylus input method, or a stylus for which high-frequency motion tracking is not supported, this property reports the value: TouchEventIntent.UNKNOWN.</p>
		 * <p>The TouchEventIntent class defines constants for the possible values of this property, which include:</p>
		 * <ul>
		 *  <li>TouchEventIntent.ERASER</li>
		 *  <li>TouchEventIntent.PEN</li>
		 *  <li>TouchEventIntent.UNKNOWN</li>
		 * </ul>
		 * 
		 * @return 
		 */
		public function get touchIntent():String {
			return _touchIntent;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set touchIntent(value:String):void {
			_touchIntent = value;
		}

		/**
		 * <p> A unique identification number (as an int) assigned to the touch point. </p>
		 * 
		 * @return 
		 */
		public function get touchPointID():int {
			return _touchPointID;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set touchPointID(value:int):void {
			_touchPointID = value;
		}

		/**
		 * <p> Creates a copy of the TouchEvent object and sets the value of each property to match that of the original. </p>
		 * 
		 * @return  — A new TouchEvent object with property values that match those of the original. 
		 */
		override public function clone():flash.events.Event {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Updates the specified ByteArray object with the high-frequency data points for a multi-point touch event. </p>
		 * <p>The ByteArray object referenced by the <code>buffer</code> parameter is updated with values for the intermediate data points between this and the previous touch event. Three floating point values are added for each sample: an x coordinate, a y coordinate, and a pressure value. The position values reported in stage coordinates. The pressure is reported as a value between 0 and 1. Samples are recorded at regular time intervals. The exact frequency depends on the device hardware and hardware drivers. Typical sample frequencies are around 133 Hz. Note that touch events are not dispatched at exact intervals. Thus, the buffer for similar events can contain a different number of samples. The last sample in the buffer has the same data as the <code>stageX</code>, <code>stageY</code>, and <code>pressure</code> properties of this touch event object.</p>
		 * <p>A sample buffer is included in the event objects dispatched for <code>proximityMove</code> and <code>touchMove</code> events.</p>
		 * 
		 * @param buffer  — the ByteArray object to receive the sample data. 
		 * @param append  — when <code>true</code>, the samples are added to the <code>buffer</code> byte array starting at the array's current <code>position</code> property. When <code>false</code>, the default, existing data in the buffer object is discarded before the samples are added. 
		 * @return  — uint the number of samples added to the buffer. Each sample consists of three floating point Numbers. 
		 */
		public function getSamples(buffer:ByteArray, append:Boolean = false):uint {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Reports that the hardware button at the specified index is pressed. </p>
		 * 
		 * @param index  — the zero-based index of the buttons supported by a stylus-type input device. 
		 * @return 
		 */
		public function isToolButtonDown(index:int):Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a string that contains all the properties of the TouchEvent object. The string is in the following format: </p>
		 * <p><code>[TouchEvent type=<i>value</i> bubbles=<i>value</i> cancelable=<i>value</i> ... ]</code></p>
		 * 
		 * @return  — A string that contains all the properties of the TouchEvent object. 
		 */
		override public function toString():String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Instructs Flash Player or Adobe AIR to render after processing of this event completes, if the display list has been modified. </p>
		 */
		public function updateAfterEvent():void {
			throw new Error("Not implemented");
		}
	}
}
