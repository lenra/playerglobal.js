package flash.events {
	/**
	 *  The DRMManager dispatches a DRMAuthenticationErrorEvent object when a call to the <code>authenticate()</code> method of the DRMManager object fails. <br><hr>
	 */
	public class DRMAuthenticationErrorEvent extends ErrorEvent {
		/**
		 * <p> The string constant to use for the authentication error event in the type parameter when adding and removing event listeners. </p>
		 */
		public static const AUTHENTICATION_ERROR:String = "authenticationError";

		private var _domain:String;
		private var _serverURL:String;
		private var _subErrorID:int;

		/**
		 * <p> Creates a new instance of a DRMAuthenticationErrorEvent object. </p>
		 * 
		 * @param type  — the event type string 
		 * @param bubbles  — whether the event bubbles up the display list 
		 * @param cancelable  — whether the event can be canceled 
		 * @param inDetail  — The error description 
		 * @param inErrorID  — The ID of the general type of error 
		 * @param inSubErrorID  — The ID indicating the specific error within its type 
		 * @param inServerURL  — the URL of the logged-in server 
		 * @param inDomain  — the authenticated domain on the logged-in server 
		 */
		public function DRMAuthenticationErrorEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, inDetail:String = "", inErrorID:int = 0, inSubErrorID:int = 0, inServerURL:String = null, inDomain:String = null) {
			super(type, bubbles, cancelable, text);
			throw new Error("Not implemented");
		}

		/**
		 * <p> The content domain of the media rights server. Here, domain is not a network or Internet domain name. </p>
		 * 
		 * @return 
		 */
		public function get domain():String {
			return _domain;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set domain(value:String):void {
			_domain = value;
		}

		/**
		 * <p> The URL of the media rights server that rejected the authentication attempt. </p>
		 * 
		 * @return 
		 */
		public function get serverURL():String {
			return _serverURL;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set serverURL(value:String):void {
			_serverURL = value;
		}

		/**
		 * <p> A more detailed error code. </p>
		 * 
		 * @return 
		 */
		public function get subErrorID():int {
			return _subErrorID;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set subErrorID(value:int):void {
			_subErrorID = value;
		}

		/**
		 * @return  — A new ErrorEvent object with property values that match those of the original. 
		 */
		override public function clone():flash.events.Event {
			throw new Error("Not implemented");
		}
	}
}
