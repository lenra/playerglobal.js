package flash.events {
	import flash.utils.Dictionary;

	/**
	 *  AVStream dispatch AVDictionaryDataEvent to signal ID3 tag information <br><hr>
	 */
	public class AVDictionaryDataEvent extends flash.events.Event {
		public static const AV_DICTIONARY_DATA:String = "avDictionaryData";

		private var _dictionary:Dictionary;
		private var _time:Number;

		/**
		 * @param type
		 * @param bubbles
		 * @param cancelable
		 * @param init_dictionary
		 * @param init_dataTime
		 */
		public function AVDictionaryDataEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, init_dictionary:Dictionary = null, init_dataTime:Number = 0) {
			super(type, bubbles, cancelable);
			throw new Error("Not implemented");
		}

		/**
		 * <p> Contains a dictionary of keys and values for the ID3 tags. </p>
		 * 
		 * @return 
		 */
		public function get dictionary():Dictionary {
			return _dictionary;
		}

		/**
		 * <p> The timestamp for the ID3 tag. </p>
		 * 
		 * @return 
		 */
		public function get time():Number {
			return _time;
		}
	}
}
