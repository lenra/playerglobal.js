package flash.events {
	/**
	 *  The Event class is used as the base class for the creation of Event objects, which are passed as parameters to event listeners when an event occurs. <p>The properties of the Event class carry basic information about an event, such as the event's type or whether the event's default behavior can be canceled. For many events, such as the events represented by the Event class constants, this basic information is sufficient. Other events, however, may require more detailed information. <span>Events associated with a mouse click, for example, need to include additional information about the location of the click event and whether any keys were pressed during the click event. You can pass such additional information to event listeners by extending the Event class, which is what the MouseEvent class does. ActionScript 3.0</span> API defines several Event subclasses for common events that require additional information. Events associated with each of the Event subclasses are described in the documentation for each class.</p> <p>The methods of the Event class can be used in event listener functions to affect the behavior of the event object. Some events have an associated default behavior.<span> For example, the <code>doubleClick</code> event has an associated default behavior that highlights the word under the mouse pointer at the time of the event.</span> Your event listener can cancel this behavior by calling the <code>preventDefault()</code> method. <span>You can also make the current event listener the last one to process an event by calling the <code>stopPropagation()</code> or <code>stopImmediatePropagation()</code> method.</span> </p> <p>Other sources of information include:</p> <ul> 
	 *  <li>A useful description about the timing of events, code execution, and rendering at runtime in Ted Patrick's blog entry: <a href="http://www.onflex.org/ted/2005/07/flash-player-mental-model-elastic.php" target="external">Flash Player Mental Model - The Elastic Racetrack</a>.</li> 
	 *  <li>A blog entry by Johannes Tacskovics about the timing of frame events, such as ENTER_FRAME, EXIT_FRAME: <a href="http://blog.johannest.com/2009/06/15/the-movieclip-life-cycle-revisited-from-event-added-to-event-removed_from_stage/" target="external">The MovieClip Lifecycle</a>.</li> 
	 *  <li>An article by Trevor McCauley about the order of ActionScript operations: <a href="http://www.senocular.com/flash/tutorials/orderofoperations/" target="external">Order of Operations in ActionScript</a>.</li> 
	 *  <li>A blog entry by Matt Przybylski on creating custom events: <a href="http://evolve.reintroducing.com/2007/10/23/as3/as3-custom-events/" target="external">AS3: Custom Events</a>.</li> 
	 * </ul> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS2db454920e96a9e51e63e3d11c0bf69084-7cdf.html" target="_blank">Using events</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS2db454920e96a9e51e63e3d11c0bf69084-7ce1.html" target="_blank">Manually dispatching events</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS2db454920e96a9e51e63e3d11c0bf69084-7ce0.html" target="_blank">Using event subclasses</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e53.html" target="_blank">Event handling example: Alarm Clock</a>
	 * </div><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS2db454920e96a9e51e63e3d11c0bf64a29-7fff.html" target="_blank">About Flex events</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS2db454920e96a9e51e63e3d11c0bf64a29-7ffe.html" target="_blank">About the Event class</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS2db454920e96a9e51e63e3d11c0bf69084-7cdb.html" target="_blank">Event propagation</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS2db454920e96a9e51e63e3d11c0bf69084-7cda.html" target="_blank">Event priorities</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS2db454920e96a9e51e63e3d11c0bf64a29-7fdb.html" target="_blank">About keyboard events</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e4e.html" target="_blank">Basics of handling events</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e50.html" target="_blank">How ActionScript 3.0 event handling differs from earlier versions</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e4f.html" target="_blank">The event flow</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e55.html" target="_blank">Event objects</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7dfb.html" target="_blank">Handling events for display objects</a>
	 *  <br>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="EventDispatcher.html" target="">flash.events.EventDispatcher</a>
	 * </div><br><hr>
	 */
	public class Event {
		/**
		 * <p> The <code>ACTIVATE</code> constant defines the value of the <code>type</code> property of an <code>activate</code> event object. </p>
		 * <p><b>Note:</b> This event has neither a "capture phase" nor a "bubble phase", which means that event listeners must be added directly to any potential targets, whether the target is on the display list or not.</p>
		 * <p>AIR for TV devices never automatically dispatch this event. You can, however, dispatch it manually.</p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>Any DisplayObject instance with a listener registered for the <code>activate</code> event.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="EventDispatcher.html#event:activate" target="">flash.events.EventDispatcher.activate</a>
		 *  <br>
		 *  <a href="Event.html#DEACTIVATE" target="">DEACTIVATE</a>
		 * </div>
		 */
		public static const ACTIVATE:String = "activate";
		/**
		 * <p> The <code>Event.ADDED</code> constant defines the value of the <code>type</code> property of an <code>added</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>true</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The DisplayObject instance being added to the display list. The <code>target</code> is not always the object in the display list that registered the event listener. Use the <code>currentTarget</code> property to access the object in the display list that is currently processing the event.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/DisplayObject.html#event:added" target="">flash.display.DisplayObject.added</a>
		 *  <br>
		 *  <a href="Event.html#ADDED_TO_STAGE" target="">ADDED_TO_STAGE</a>
		 *  <br>
		 *  <a href="Event.html#REMOVED" target="">REMOVED</a>
		 *  <br>
		 *  <a href="Event.html#REMOVED_FROM_STAGE" target="">REMOVED_FROM_STAGE</a>
		 * </div>
		 */
		public static const ADDED:String = "added";
		/**
		 * <p> The <code>Event.ADDED_TO_STAGE</code> constant defines the value of the <code>type</code> property of an <code>addedToStage</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The DisplayObject instance being added to the on stage display list, either directly or through the addition of a sub tree in which the DisplayObject instance is contained. If the DisplayObject instance is being directly added, the <code>added</code> event occurs before this event.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/DisplayObject.html#event:addedToStage" target="">flash.display.DisplayObject.addedToStage</a>
		 *  <br>
		 *  <a href="Event.html#ADDED" target="">ADDED</a>
		 *  <br>
		 *  <a href="Event.html#REMOVED" target="">REMOVED</a>
		 *  <br>
		 *  <a href="Event.html#REMOVED_FROM_STAGE" target="">REMOVED_FROM_STAGE</a>
		 * </div>
		 */
		public static const ADDED_TO_STAGE:String = "addedToStage";
		/**
		 * <p> The <code>Event.BROWSER_ZOOM_CHANGE</code> constant defines the value of the <code>type</code> property of an <code>browserZoomChange</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td> The Stage instance.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/Stage.html#event:browserZoomChange" target="">flash.display.Stage.browserZoomChange</a>
		 * </div>
		 */
		public static const BROWSER_ZOOM_CHANGE:String = "browserZoomChange";
		/**
		 * <p> The <code>Event.CANCEL</code> constant defines the value of the <code>type</code> property of a <code>cancel</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>A reference to the object on which the operation is canceled.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/net/FileReference.html#event:cancel" target="">flash.net.FileReference.cancel</a>
		 * </div>
		 */
		public static const CANCEL:String = "cancel";
		/**
		 * <p> The <code>Event.CHANGE</code> constant defines the value of the <code>type</code> property of a <code>change</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>true</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The object that has had its value modified. The <code>target</code> is not always the object in the display list that registered the event listener. Use the <code>currentTarget</code> property to access the object in the display list that is currently processing the event.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/text/TextField.html#event:change" target="">flash.text.TextField.change</a>
		 *  <br>
		 *  <a href="TextEvent.html#TEXT_INPUT" target="">flash.events.TextEvent.TEXT_INPUT</a>
		 * </div>
		 */
		public static const CHANGE:String = "change";
		/**
		 * <p> The <code>Event.CHANNEL_MESSAGE</code> constant defines the value of the <code>type</code> property of a <code>channelMessage</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The object that dispatched this event.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/system/MessageChannel.html#event:channelMessage" target="">flash.system.MessageChannel.channelMessage</a>
		 * </div>
		 */
		public static const CHANNEL_MESSAGE:String = "channelMessage";
		/**
		 * <p> The <code>Event.CHANNEL_STATE</code> constant defines the value of the <code>type</code> property of a <code>channelState</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The object that dispatched this event.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/system/Worker.html#event:channelState" target="">flash.system.Worker.channelState</a>
		 * </div>
		 */
		public static const CHANNEL_STATE:String = "channelState";
		/**
		 * <p> The <code>Event.CLEAR</code> constant defines the value of the <code>type</code> property of a <code>clear</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>Any InteractiveObject instance with a listener registered for the <code>clear</code> event.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><b>Note:</b> TextField objects do <i>not</i> dispatch <code>clear</code>, <code>copy</code>, <code>cut</code>, <code>paste</code>, or <code>selectAll</code> events. TextField objects always include Cut, Copy, Paste, Clear, and Select All commands in the context menu. You cannot remove these commands from the context menu for TextField objects. For TextField objects, selecting these commands (or their keyboard equivalents) does not generate <code>clear</code>, <code>copy</code>, <code>cut</code>, <code>paste</code>, or <code>selectAll</code> events. However, other classes that extend the InteractiveObject class, including components built using the Flash Text Engine (FTE), will dispatch these events in response to user actions such as keyboard shortcuts and context menus.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/InteractiveObject.html#event:clear" target="">flash.display.InteractiveObject.clear</a>
		 * </div>
		 */
		public static const CLEAR:String = "clear";
		/**
		 * <p> The <code>Event.CLOSE</code> constant defines the value of the <code>type</code> property of a <code>close</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The object whose connection has been closed.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/net/Socket.html#event:close" target="">flash.net.Socket.close</a>
		 *  <br>
		 *  <a href="../../flash/net/XMLSocket.html#event:close" target="">flash.net.XMLSocket.close</a>
		 *  <br>
		 *  <a href="../../flash/display/NativeWindow.html#event:close" target="">flash.display.NativeWindow.close</a>
		 * </div>
		 */
		public static const CLOSE:String = "close";
		/**
		 * <p> The <code>Event.CLOSING</code> constant defines the value of the <code>type</code> property of a <code>closing</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>true</code>; canceling this event object stops the close operation.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The object whose connection is to be closed.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/NativeWindow.html#event:closing" target="">flash.display.NativeWindow.closing</a>
		 * </div>
		 */
		public static const CLOSING:String = "closing";
		/**
		 * <p> The <code>Event.COMPLETE</code> constant defines the value of the <code>type</code> property of a <code>complete</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The network object that has completed loading. </td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p id="learnMore"><span class="label">Learn more</span></p>
		 * <div class="seeAlso">
		 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7cfd.html" target="_blank">Loading external data</a>
		 *  <br>
		 * </div>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/LoaderInfo.html#event:complete" target="">flash.display.LoaderInfo.complete</a>
		 *  <br>
		 *  <a href="../../flash/html/HTMLLoader.html#event:complete" target="">flash.html.HTMLLoader.complete</a>
		 *  <br>
		 *  <a href="../../flash/media/Sound.html#event:complete" target="">flash.media.Sound.complete</a>
		 *  <br>
		 *  <a href="../../flash/net/FileReference.html#event:complete" target="">flash.net.FileReference.complete</a>
		 *  <br>
		 *  <a href="../../flash/net/URLLoader.html#event:complete" target="">flash.net.URLLoader.complete</a>
		 *  <br>
		 *  <a href="../../flash/net/URLStream.html#event:complete" target="">flash.net.URLStream.complete</a>
		 * </div>
		 */
		public static const COMPLETE:String = "complete";
		/**
		 * <p> The <code>Event.CONNECT</code> constant defines the value of the <code>type</code> property of a <code>connect</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The Socket or XMLSocket object that has established a network connection.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/net/Socket.html#event:connect" target="">flash.net.Socket.connect</a>
		 *  <br>
		 *  <a href="../../flash/net/XMLSocket.html#event:connect" target="">flash.net.XMLSocket.connect</a>
		 * </div>
		 */
		public static const CONNECT:String = "connect";
		/**
		 * <p> The <code>Event.CONTEXT3D_CREATE</code> constant defines the value of the <code>type</code> property of a <code>context3Dcreate</code> event object. This event is raised only by Stage3D objects in response to either a call to Stage3D.requestContext3D or in response to an OS triggered reset of the Context3D bound to the Stage3D object. Inspect the Stage3D.context3D property to get the newly created Context3D object. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  Stage3D
		 * </div>
		 */
		public static const CONTEXT3D_CREATE:String = "context3DCreate";
		/**
		 * <p> Defines the value of the <code>type</code> property of a <code>copy</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>Any InteractiveObject instance with a listener registered for the <code>copy</code> event.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><b>Note:</b> TextField objects do <i>not</i> dispatch <code>clear</code>, <code>copy</code>, <code>cut</code>, <code>paste</code>, or <code>selectAll</code> events. TextField objects always include Cut, Copy, Paste, Clear, and Select All commands in the context menu. You cannot remove these commands from the context menu for TextField objects. For TextField objects, selecting these commands (or their keyboard equivalents) does not generate <code>clear</code>, <code>copy</code>, <code>cut</code>, <code>paste</code>, or <code>selectAll</code> events. However, other classes that extend the InteractiveObject class, including components built using the Flash Text Engine (FTE), will dispatch these events in response to user actions such as keyboard shortcuts and context menus.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/InteractiveObject.html#event:copy" target="">flash.display.InteractiveObject.copy</a>
		 * </div>
		 */
		public static const COPY:String = "copy";
		/**
		 * <p> Defines the value of the <code>type</code> property of a <code>cut</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>Any InteractiveObject instance with a listener registered for the <code>cut</code> event.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><b>Note:</b> TextField objects do <i>not</i> dispatch <code>clear</code>, <code>copy</code>, <code>cut</code>, <code>paste</code>, or <code>selectAll</code> events. TextField objects always include Cut, Copy, Paste, Clear, and Select All commands in the context menu. You cannot remove these commands from the context menu for TextField objects. For TextField objects, selecting these commands (or their keyboard equivalents) does not generate <code>clear</code>, <code>copy</code>, <code>cut</code>, <code>paste</code>, or <code>selectAll</code> events. However, other classes that extend the InteractiveObject class, including components built using the Flash Text Engine (FTE), will dispatch these events in response to user actions such as keyboard shortcuts and context menus.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/InteractiveObject.html#event:cut" target="">flash.display.InteractiveObject.cut</a>
		 * </div>
		 */
		public static const CUT:String = "cut";
		/**
		 * <p> The <code>Event.DEACTIVATE</code> constant defines the value of the <code>type</code> property of a <code>deactivate</code> event object. </p>
		 * <p><b>Note:</b> This event has neither a "capture phase" nor a "bubble phase", which means that event listeners must be added directly to any potential targets, whether the target is on the display list or not.</p>
		 * <p>AIR for TV devices never automatically dispatch this event. You can, however, dispatch it manually.</p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>Any DisplayObject instance with a listener registered for the <code>deactivate</code> event.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="EventDispatcher.html#event:deactivate" target="">flash.events.EventDispatcher.deactivate</a>
		 *  <br>
		 *  <a href="Event.html#ACTIVATE" target="">ACTIVATE</a>
		 * </div>
		 */
		public static const DEACTIVATE:String = "deactivate";
		/**
		 * <p> The <code>Event.DISPLAYING</code> constant defines the value of the <code>type</code> property of a <code>displaying</code> event object. </p>
		 * <p><b>Note:</b> This event does not go through a "capture phase" and is dispatched directly to the target, whether the target is on the display list or not.</p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The object that is about to be displayed.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/NativeMenu.html#event:displaying" target="">flash.display.NativeMenu.displaying</a>
		 *  <br>
		 *  <a href="../../flash/display/NativeMenuItem.html#event:displaying" target="">flash.display.NativeMenuItem.displaying</a>
		 * </div>
		 */
		public static const DISPLAYING:String = "displaying";
		/**
		 * <p> The <code>Event.ENTER_FRAME</code> constant defines the value of the <code>type</code> property of an <code>enterFrame</code> event object. </p>
		 * <p><b>Note:</b> This event has neither a "capture phase" nor a "bubble phase", which means that event listeners must be added directly to any potential targets, whether the target is on the display list or not.</p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>Any DisplayObject instance with a listener registered for the <code>enterFrame</code> event.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/DisplayObject.html#event:enterFrame" target="">flash.display.DisplayObject.enterFrame</a>
		 * </div>
		 */
		public static const ENTER_FRAME:String = "enterFrame";
		/**
		 * <p> The <code>Event.EXITING</code> constant defines the value of the <code>type</code> property of an <code>exiting</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>true</code>; canceling this event object stops the exit operation.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The NativeApplication object.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The NativeApplication object.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/desktop/NativeApplication.html#event:exiting" target="">flash.desktop.NativeApplication.exiting</a>
		 * </div>
		 */
		public static const EXITING:String = "exiting";
		/**
		 * <p> The <code>Event.EXIT_FRAME</code> constant defines the value of the <code>type</code> property of an <code>exitFrame</code> event object. </p>
		 * <p><b>Note:</b> This event has neither a "capture phase" nor a "bubble phase", which means that event listeners must be added directly to any potential targets, whether the target is on the display list or not.</p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>Any DisplayObject instance with a listener registered for the <code>enterFrame</code> event.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/DisplayObject.html#event:exitFrame" target="">flash.display.DisplayObject.exitFrame</a>
		 * </div>
		 */
		public static const EXIT_FRAME:String = "exitFrame";
		/**
		 * <p> The <code>Event.FRAME_CONSTRUCTED</code> constant defines the value of the <code>type</code> property of an <code>frameConstructed</code> event object. </p>
		 * <p><b>Note:</b> This event has neither a "capture phase" nor a "bubble phase", which means that event listeners must be added directly to any potential targets, whether the target is on the display list or not.</p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>Any DisplayObject instance with a listener registered for the <code>frameConstructed</code> event.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/DisplayObject.html#event:frameConstructed" target="">flash.display.DisplayObject.frameConstructed</a>
		 * </div>
		 */
		public static const FRAME_CONSTRUCTED:String = "frameConstructed";
		/**
		 * <p> The <code>Event.FRAME_LABEL</code> constant defines the value of the <code>type</code> property of an <code>frameLabel</code> event object. </p>
		 * <p><b>Note:</b> This event has neither a "capture phase" nor a "bubble phase", which means that event listeners must be added directly to FrameLabel objects.</p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The FrameLabel object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>Any FrameLabel instance with a listener registered for the <code>frameLabel</code> event.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/FrameLabel.html#event:frameLabel" target="">flash.display.FrameLabel.frameLabel</a>
		 * </div>
		 */
		public static const FRAME_LABEL:String = "frameLabel";
		/**
		 * <p> The <code>Event.FULL_SCREEN</code> constant defines the value of the <code>type</code> property of a <code>fullScreen</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The Stage object.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/Stage.html#event:fullScreen" target="">flash.display.Stage.fullScreen</a>
		 * </div>
		 */
		public static const FULLSCREEN:String = "fullScreen";
		/**
		 * <p> The <code>Event.HTML_BOUNDS_CHANGE</code> constant defines the value of the <code>type</code> property of an <code>htmlBoundsChange</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The HTMLLoader object.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The HTMLLoader object.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/html/HTMLLoader.html#event:htmlBoundsChange" target="">htmlBoundsChange event</a>
		 * </div>
		 */
		public static const HTML_BOUNDS_CHANGE:String = "htmlBoundsChange";
		/**
		 * <p> The <code>Event.HTML_DOM_INITIALIZE</code> constant defines the value of the <code>type</code> property of an <code>htmlDOMInitialize</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The HTMLLoader object.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The HTMLLoader object.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/html/HTMLLoader.html#event:htmlDOMInitialize" target="">htmlDOMInitialize event</a>
		 * </div>
		 */
		public static const HTML_DOM_INITIALIZE:String = "htmlDOMInitialize";
		/**
		 * <p> The <code>Event.HTML_RENDER</code> constant defines the value of the <code>type</code> property of an <code>htmlRender</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The HTMLLoader object.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The HTMLLoader object.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/html/HTMLLoader.html#event:htmlRender" target="">htmlRender event</a>
		 * </div>
		 */
		public static const HTML_RENDER:String = "htmlRender";
		/**
		 * <p> The <code>Event.ID3</code> constant defines the value of the <code>type</code> property of an <code>id3</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The Sound object loading the MP3 for which ID3 data is now available. The <code>target</code> is not always the object in the display list that registered the event listener. Use the <code>currentTarget</code> property to access the object in the display list that is currently processing the event.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/media/Sound.html#event:id3" target="">flash.media.Sound.id3</a>
		 * </div>
		 */
		public static const ID3:String = "id3";
		/**
		 * <p> The <code>Event.INIT</code> constant defines the value of the <code>type</code> property of an <code>init</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The LoaderInfo object associated with the SWF file being loaded.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/LoaderInfo.html#event:init" target="">flash.display.LoaderInfo.init</a>
		 * </div>
		 */
		public static const INIT:String = "init";
		/**
		 * <p> The <code>Event.LOCATION_CHANGE</code> constant defines the value of the <code>type</code> property of a <code>locationChange</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The HTMLLoader object.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The HTMLLoader object.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/html/HTMLLoader.html#event:locationChange" target="">locationChange event</a>
		 * </div>
		 */
		public static const LOCATION_CHANGE:String = "locationChange";
		/**
		 * <p> The <code>Event.MOUSE_LEAVE</code> constant defines the value of the <code>type</code> property of a <code>mouseLeave</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The Stage object. The <code>target</code> is not always the object in the display list that registered the event listener. Use the <code>currentTarget</code> property to access the object in the display list that is currently processing the event.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/Stage.html#event:mouseLeave" target="">flash.display.Stage.mouseLeave</a>
		 *  <br>
		 *  <a href="MouseEvent.html" target="">flash.events.MouseEvent</a>
		 * </div>
		 */
		public static const MOUSE_LEAVE:String = "mouseLeave";
		/**
		 * <p> The <code>Event.NETWORK_CHANGE</code> constant defines the value of the <code>type</code> property of a <code>networkChange</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The NativeApplication object.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/desktop/NativeApplication.html#event:networkChange" target="">flash.desktop.NativeApplication.networkChange</a>
		 * </div>
		 */
		public static const NETWORK_CHANGE:String = "networkChange";
		/**
		 * <p> The <code>Event.OPEN</code> constant defines the value of the <code>type</code> property of an <code>open</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The network object that has opened a connection. </td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/LoaderInfo.html#event:open" target="">flash.display.LoaderInfo.open</a>
		 *  <br>
		 *  <a href="../../flash/media/Sound.html#event:open" target="">flash.media.Sound.open</a>
		 *  <br>
		 *  <a href="../../flash/net/FileReference.html#event:open" target="">flash.net.FileReference.open</a>
		 *  <br>
		 *  <a href="../../flash/net/URLLoader.html#event:open" target="">flash.net.URLLoader.open</a>
		 *  <br>
		 *  <a href="../../flash/net/URLStream.html#event:open" target="">flash.net.URLStream.open</a>
		 * </div>
		 */
		public static const OPEN:String = "open";
		/**
		 * <p> The <code>Event.PASTE</code> constant defines the value of the <code>type</code> property of a <code>paste</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>true</code> (<code>false</code> for AIR runtime.)</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>Any InteractiveObject instance with a listener registered for the <code>paste</code> event.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><b>Note:</b> TextField objects do <i>not</i> dispatch <code>clear</code>, <code>copy</code>, <code>cut</code>, <code>paste</code>, or <code>selectAll</code> events. TextField objects always include Cut, Copy, Paste, Clear, and Select All commands in the context menu. You cannot remove these commands from the context menu for TextField objects. For TextField objects, selecting these commands (or their keyboard equivalents) does not generate <code>clear</code>, <code>copy</code>, <code>cut</code>, <code>paste</code>, or <code>selectAll</code> events. However, other classes that extend the InteractiveObject class, including components built using the Flash Text Engine (FTE), will dispatch these events in response to user actions such as keyboard shortcuts and context menus.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/InteractiveObject.html#event:paste" target="">flash.display.InteractiveObject.paste</a>
		 * </div>
		 */
		public static const PASTE:String = "paste";
		/**
		 * <p> The <code>Event.PREPARING</code> constant defines the value of the <code>type</code> property of a <code>preparing</code> event object. </p>
		 * <p><b>Note:</b> This event does not go through a "capture phase" and is dispatched directly to the target, whether the target is on the display list or not.</p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that dispatched this event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The object that dispatched this event.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/NativeMenu.html#event:preparing" target="">flash.display.NativeMenu.preparing</a>
		 *  <br>
		 *  <a href="../../flash/display/NativeMenuItem.html#event:preparing" target="">flash.display.NativeMenuItem.preparing</a>
		 * </div>
		 */
		public static const PREPARING:String = "preparing";
		/**
		 * <p> The <code>Event.REMOVED</code> constant defines the value of the <code>type</code> property of a <code>removed</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>true</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The DisplayObject instance to be removed from the display list. The <code>target</code> is not always the object in the display list that registered the event listener. Use the <code>currentTarget</code> property to access the object in the display list that is currently processing the event.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/DisplayObject.html#event:removed" target="">flash.display.DisplayObject.removed</a>
		 *  <br>
		 *  <a href="Event.html#ADDED" target="">ADDED</a>
		 *  <br>
		 *  <a href="Event.html#ADDED_TO_STAGE" target="">ADDED_TO_STAGE</a>
		 *  <br>
		 *  <a href="Event.html#REMOVED_FROM_STAGE" target="">REMOVED_FROM_STAGE</a>
		 * </div>
		 */
		public static const REMOVED:String = "removed";
		/**
		 * <p> The <code>Event.REMOVED_FROM_STAGE</code> constant defines the value of the <code>type</code> property of a <code>removedFromStage</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The DisplayObject instance being removed from the on stage display list, either directly or through the removal of a sub tree in which the DisplayObject instance is contained. If the DisplayObject instance is being directly removed, the <code>removed</code> event occurs before this event.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/DisplayObject.html#event:removedFromStage" target="">flash.display.DisplayObject.removedFromStage</a>
		 *  <br>
		 *  <a href="Event.html#ADDED" target="">ADDED</a>
		 *  <br>
		 *  <a href="Event.html#REMOVED" target="">REMOVED</a>
		 *  <br>
		 *  <a href="Event.html#ADDED_TO_STAGE" target="">ADDED_TO_STAGE</a>
		 * </div>
		 */
		public static const REMOVED_FROM_STAGE:String = "removedFromStage";
		/**
		 * <p> The <code>Event.RENDER</code> constant defines the value of the <code>type</code> property of a <code>render</code> event object. </p>
		 * <p><b>Note:</b> This event has neither a "capture phase" nor a "bubble phase", which means that event listeners must be added directly to any potential targets, whether the target is on the display list or not.</p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; the default behavior cannot be canceled.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>Any DisplayObject instance with a listener registered for the <code>render</code> event.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/DisplayObject.html#event:render" target="">flash.display.DisplayObject.render</a>
		 *  <br>
		 *  <a href="../../flash/display/Stage.html#invalidate()" target="">flash.display.Stage.invalidate()</a>
		 * </div>
		 */
		public static const RENDER:String = "render";
		/**
		 * <p> The <code>Event.RESIZE</code> constant defines the value of the <code>type</code> property of a <code>resize</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The Stage object.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/Stage.html#event:resize" target="">flash.display.Stage.resize</a>
		 * </div>
		 */
		public static const RESIZE:String = "resize";
		/**
		 * <p> The <code>Event.SCROLL</code> constant defines the value of the <code>type</code> property of a <code>scroll</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The TextField object that has been scrolled. The <code>target</code> property is not always the object in the display list that registered the event listener. Use the <code>currentTarget</code> property to access the object in the display list that is currently processing the event.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/text/TextField.html#event:scroll" target="">flash.text.TextField.scroll</a>
		 *  <br>
		 *  <a href="../../flash/html/HTMLLoader.html#event:scroll" target="">flash.html.HTMLLoader.scroll</a>
		 * </div>
		 */
		public static const SCROLL:String = "scroll";
		/**
		 * <p> The <code>Event.SELECT</code> constant defines the value of the <code>type</code> property of a <code>select</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The object on which an item has been selected.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/net/FileReference.html#event:select" target="">flash.net.FileReference.select</a>
		 *  <br>
		 *  <a href="../../flash/display/NativeMenu.html#event:select" target="">flash.display.NativeMenu.select</a>
		 *  <br>
		 *  <a href="../../flash/display/NativeMenuItem.html#event:select" target="">flash.display.NativeMenuItem.select</a>
		 * </div>
		 */
		public static const SELECT:String = "select";
		/**
		 * <p> The <code>Event.SELECT_ALL</code> constant defines the value of the <code>type</code> property of a <code>selectAll</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>Any InteractiveObject instance with a listener registered for the <code>selectAll</code> event.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><b>Note:</b> TextField objects do <i>not</i> dispatch <code>clear</code>, <code>copy</code>, <code>cut</code>, <code>paste</code>, or <code>selectAll</code> events. TextField objects always include Cut, Copy, Paste, Clear, and Select All commands in the context menu. You cannot remove these commands from the context menu for TextField objects. For TextField objects, selecting these commands (or their keyboard equivalents) does not generate <code>clear</code>, <code>copy</code>, <code>cut</code>, <code>paste</code>, or <code>selectAll</code> events. However, other classes that extend the InteractiveObject class, including components built using the Flash Text Engine (FTE), will dispatch these events in response to user actions such as keyboard shortcuts and context menus.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/InteractiveObject.html#event:selectAll" target="">flash.display.InteractiveObject.selectAll</a>
		 * </div>
		 */
		public static const SELECT_ALL:String = "selectAll";
		/**
		 * <p> The <code>Event.SOUND_COMPLETE</code> constant defines the value of the <code>type</code> property of a <code>soundComplete</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The SoundChannel object in which a sound has finished playing.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/media/SoundChannel.html#event:soundComplete" target="">flash.media.SoundChannel.soundComplete</a>
		 * </div>
		 */
		public static const SOUND_COMPLETE:String = "soundComplete";
		/**
		 * <p> The <code>Event.STANDARD_ERROR_CLOSE</code> constant defines the value of the <code>type</code> property of a <code>standardErrorClose</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The NativeProcess object.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 */
		public static const STANDARD_ERROR_CLOSE:String = "standardErrorClose";
		/**
		 * <p> The <code>Event.STANDARD_INPUT_CLOSE</code> constant defines the value of the <code>type</code> property of a <code>standardInputClose</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The NativeProcess object.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 */
		public static const STANDARD_INPUT_CLOSE:String = "standardInputClose";
		/**
		 * <p> The <code>Event.STANDARD_OUTPUT_CLOSE</code> constant defines the value of the <code>type</code> property of a <code>standardOutputClose</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The NativeProcess object.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 */
		public static const STANDARD_OUTPUT_CLOSE:String = "standardOutputClose";
		/**
		 * <p> The <code>Event.SUSPEND</code> constant defines the value of the <code>type</code> property of an <code>suspend</code> event object. This event is dispatched only on AIR iOS. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The NativeApplication object.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The NativeApplication object.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/desktop/NativeApplication.html#event:suspend" target="">flash.desktop.NativeApplication.suspend</a>
		 * </div>
		 */
		public static const SUSPEND:String = "suspend";
		/**
		 * <p> The <code>Event.TAB_CHILDREN_CHANGE</code> constant defines the value of the <code>type</code> property of a <code>tabChildrenChange</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>true</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The object whose tabChildren flag has changed. The <code>target</code> is not always the object in the display list that registered the event listener. Use the <code>currentTarget</code> property to access the object in the display list that is currently processing the event.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/InteractiveObject.html#event:tabChildrenChange" target="">flash.display.InteractiveObject.tabChildrenChange</a>
		 * </div>
		 */
		public static const TAB_CHILDREN_CHANGE:String = "tabChildrenChange";
		/**
		 * <p> The <code>Event.TAB_ENABLED_CHANGE</code> constant defines the value of the <code>type</code> property of a <code>tabEnabledChange</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>true</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The InteractiveObject whose tabEnabled flag has changed. The <code>target</code> is not always the object in the display list that registered the event listener. Use the <code>currentTarget</code> property to access the object in the display list that is currently processing the event.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/InteractiveObject.html#event:tabEnabledChange" target="">flash.display.InteractiveObject.tabEnabledChange</a>
		 * </div>
		 */
		public static const TAB_ENABLED_CHANGE:String = "tabEnabledChange";
		/**
		 * <p> The <code>Event.TAB_INDEX_CHANGE</code> constant defines the value of the <code>type</code> property of a <code>tabIndexChange</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>true</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The object whose tabIndex has changed. The <code>target</code> is not always the object in the display list that registered the event listener. Use the <code>currentTarget</code> property to access the object in the display list that is currently processing the event.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/InteractiveObject.html#event:tabIndexChange" target="">flash.display.InteractiveObject.tabIndexChange</a>
		 * </div>
		 */
		public static const TAB_INDEX_CHANGE:String = "tabIndexChange";
		/**
		 * <p> The <code>Event.TEXTURE_READY</code> constant defines the value of the <code>type</code> property of a <code>textureReady</code> event object. This event is dispatched by Texture and CubeTexture objects to signal the completion of an asynchronous upload. Request an asynchronous upload by using the <code>uploadCompressedTextureFromByteArray()</code> method on Texture or CubeTexture. This event neither bubbles nor is cancelable. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  Texture.uploadCompressedTextureFromByteArray()
		 *  <br>CubeTexture.uploadCompressedTextureFromByteArray()
		 * </div>
		 */
		public static const TEXTURE_READY:String = "textureReady";
		/**
		 * <p> The <code>Event.TEXT_INTERACTION_MODE_CHANGE</code> constant defines the value of the <code>type</code> property of a <code>interaction mode</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The TextField object whose interaction mode property is changed. For example on Android, one can change the interaction mode to SELECTION via context menu. The <code>target</code> property is not always the object in the display list that registered the event listener. Use the <code>currentTarget</code> property to access the object in the display list that is currently processing the event.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/text/TextField.html#event:textInteractionModeChange" target="">flash.text.TextField.textInteractionModeChange</a>
		 * </div>
		 */
		public static const TEXT_INTERACTION_MODE_CHANGE:String = "textInteractionModeChange";
		/**
		 * <p> The <code>Event.UNLOAD</code> constant defines the value of the <code>type</code> property of an <code>unload</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The LoaderInfo object associated with the SWF file being unloaded or replaced.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/LoaderInfo.html#event:unload" target="">flash.display.LoaderInfo.unload</a>
		 * </div>
		 */
		public static const UNLOAD:String = "unload";
		/**
		 * <p> The <code>Event.USER_IDLE</code> constant defines the value of the <code>type</code> property of a <code>userIdle</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The NativeApplication object.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/desktop/NativeApplication.html#event:userIdle" target="">flash.desktop.NativeApplication.userIdle</a>
		 * </div>
		 */
		public static const USER_IDLE:String = "userIdle";
		/**
		 * <p> The <code>Event.USER_PRESENT</code> constant defines the value of the <code>type</code> property of a <code>userPresent</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The NativeApplication object.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/desktop/NativeApplication.html#event:userPresent" target="">flash.desktop.NativeApplication.userPresent</a>
		 * </div>
		 */
		public static const USER_PRESENT:String = "userPresent";
		/**
		 * <p> The <code>Event.VIDEO_FRAME</code> constant defines the value of the <code>type</code> property of a <code>videoFrame</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The Camera object that dispatched this event.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/media/Camera.html" target="">flash.media.Camera</a>
		 * </div>
		 */
		public static const VIDEO_FRAME:String = "videoFrame";
		/**
		 * <p> The <code>Event.WORKER_STATE</code> constant defines the value of the <code>type</code> property of a <code>workerState</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The object that dispatched this event.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/system/Worker.html#event:workerState" target="">flash.system.Worker.workerState</a>
		 * </div>
		 */
		public static const WORKER_STATE:String = "workerState";

		private var _bubbles:Boolean;
		private var _cancelable:Boolean;
		private var _currentTarget:Object;
		private var _eventPhase:uint = EventPhase.CAPTURING_PHASE;
		private var _target:Object;
		private var _type:String;

		/**
		 * <p> Creates an Event object to pass as a parameter to event listeners. </p>
		 * 
		 * @param type  — The type of the event, accessible as <code>Event.type</code>. 
		 * @param bubbles  — Determines whether the Event object participates in the bubbling stage of the event flow. The default value is <code>false</code>. 
		 * @param cancelable  — Determines whether the Event object can be canceled. The default values is <code>false</code>. 
		 */
		public function Event(type:String, bubbles:Boolean = false, cancelable:Boolean = false):void {
			this._type = type;
			this._bubbles = bubbles;
			this._cancelable = cancelable;
		}

		/**
		 * <p> Indicates whether an event is a bubbling event. If the event can bubble, this value is <code>true</code>; otherwise it is <code>false</code>. </p>
		 * <p>When an event occurs, it moves through the three phases of the event flow: the capture phase, which flows from the top of the display list hierarchy to the node just before the target node; the target phase, which comprises the target node; and the bubbling phase, which flows from the node subsequent to the target node back up the display list hierarchy.</p>
		 * <p>Some events, such as the <code>activate</code> and <code>unload</code> events, do not have a bubbling phase. The <code>bubbles</code> property has a value of <code>false</code> for events that do not have a bubbling phase.</p>
		 * 
		 * @return 
		 */
		public function get bubbles():Boolean {
			return _bubbles;
		}

		/**
		 * <p> Indicates whether the behavior associated with the event can be prevented. If the behavior can be canceled, this value is <code>true</code>; otherwise it is <code>false</code>. </p>
		 * 
		 * @return 
		 */
		public function get cancelable():Boolean {
			return _cancelable;
		}

		/**
		 * <p> The object that is actively processing the Event object with an event listener. For example, if a user clicks an OK button, the current target could be the node containing that button or one of its ancestors that has registered an event listener for that event. </p>
		 * 
		 * @return 
		 */
		public function get currentTarget():Object {
			return _currentTarget;
		}
		
		internal function setCurrentTarget(value:Object):void {
			_currentTarget = value;
		}

		/**
		 * <p> The current phase in the event flow. This property can contain the following numeric values: </p>
		 * <ul>
		 *  <li> The capture phase (<code>EventPhase.CAPTURING_PHASE</code>).</li>
		 *  <li> The target phase (<code>EventPhase.AT_TARGET</code>).</li>
		 *  <li> The bubbling phase (<code>EventPhase.BUBBLING_PHASE</code>).</li>
		 * </ul>
		 * 
		 * @return 
		 */
		public function get eventPhase():uint {
			return _eventPhase;
		}
		
		internal function setEventPhase(value:uint):void {
			_eventPhase = value;
		}

		/**
		 * <p> The event target. This property contains the target node. For example, if a user clicks an OK button, the target node is the display list node containing that button. </p>
		 * 
		 * @return 
		 */
		public function get target():Object {
			return _target;
		}
		
		internal function setTarget(value:Object):void {
			_target = value;
		}

		/**
		 * <p> The type of event. The type is case-sensitive. </p>
		 * 
		 * @return 
		 */
		public function get type():String {
			return _type;
		}

		/**
		 * <p> Duplicates an instance of an Event subclass. </p>
		 * <p>Returns a new Event object that is a copy of the original instance of the Event object. You do not normally call <code>clone()</code>; the EventDispatcher class calls it automatically when you redispatch an event—that is, when you call <code>dispatchEvent(event)</code> from a handler that is handling <code>event</code>.</p>
		 * <p>The new Event object includes all the properties of the original.</p>
		 * <p>When creating your own custom Event class, you must override the inherited <code>Event.clone()</code> method in order for it to duplicate the properties of your custom class. If you do not set all the properties that you add in your event subclass, those properties will not have the correct values when listeners handle the redispatched event.</p>
		 * <p>In this example, <code>PingEvent</code> is a subclass of <code>Event</code> and therefore implements its own version of <code>clone()</code>.</p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *      class PingEvent extends Event {
		 *          var URL:String;
		 *          
		 *      public override function clone():Event {
		 *               return new PingEvent(type, bubbles, cancelable, URL);
		 *         }
		 *      }
		 *      </pre>
		 * </div>
		 * 
		 * @return  — A new Event object that is identical to the original. 
		 */
		public function clone():flash.events.Event {
			throw new Error("Not implemented");
		}

		/**
		 * <p> A utility function for implementing the <code>toString()</code> method in custom ActionScript 3.0 Event classes. <span>Overriding the <code>toString()</code> method is recommended, but not required.</span> </p>
		 * <pre>
		 * 	 class PingEvent extends Event {
		 * 	  var URL:String;
		 * 	 
		 * 	 public override function toString():String { 
		 * 	  return formatToString("PingEvent", "type", "bubbles", "cancelable", "eventPhase", "URL"); 
		 * 	    }
		 * 	 }
		 * 	 </pre>
		 * 
		 * @param className  — The name of your custom Event class. <span>In the previous example, the <code>className</code> parameter is <code>PingEvent</code>.</span> 
		 * @param arguments  — The properties of the Event class<span> and the properties that you add in your custom Event class. In the previous example, the <code>...arguments</code> parameter includes <code>type</code>, <code>bubbles</code>, <code>cancelable</code>, <code>eventPhase</code>, and <code>URL</code></span>. 
		 * @return  — The name of your custom Event class and the String value of your  parameter. 
		 */
		public function formatToString(className:String, ...arguments):String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Checks whether the <code>preventDefault()</code> method has been called on the event. If the <code>preventDefault()</code> method has been called, returns <code>true</code>; otherwise, returns <code>false</code>. </p>
		 * 
		 * @return  — If  has been called, returns ; otherwise, returns . 
		 */
		public function isDefaultPrevented():Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Cancels an event's default behavior if that behavior can be canceled. </p>
		 * <p>Many events have associated behaviors that are carried out by default. <span>For example, if a user types a character into a text field, the default behavior is that the character is displayed in the text field. Because the <code>TextEvent.TEXT_INPUT</code> event's default behavior can be canceled, you can use the <code>preventDefault()</code> method to prevent the character from appearing.</span> </p>
		 * <p>An example of a behavior that is not cancelable is the default behavior associated with the <code>Event.REMOVED</code> event, which is generated whenever Flash Player is about to remove a display object from the display list. The default behavior (removing the element) cannot be canceled, so the <code>preventDefault()</code> method has no effect on this default behavior. </p>
		 * <p>You can use the <code>Event.cancelable</code> property to check whether you can prevent the default behavior associated with a particular event. If the value of <code>Event.cancelable</code> is <code>true</code>, then <code>preventDefault()</code> can be used to cancel the event; otherwise, <code>preventDefault()</code> has no effect.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Event.html#isDefaultPrevented()" target="">flash.events.Event.isDefaultPrevented()</a>
		 *  <br>
		 *  <a href="Event.html#cancelable" target="">Event.cancelable</a>
		 * </div>
		 */
		public function preventDefault():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Prevents processing of any event listeners in the current node and any subsequent nodes in the event flow. <span>This method takes effect immediately, and it affects event listeners in the current node. In contrast, the <code>stopPropagation()</code> method doesn't take effect until all the event listeners in the current node finish processing.</span> </p>
		 * <p><b>Note: </b> This method does not cancel the behavior associated with this event; see <code>preventDefault()</code> for that functionality.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Event.html#stopPropagation()" target="">flash.events.Event.stopPropagation()</a>
		 *  <br>
		 *  <a href="Event.html#preventDefault()" target="">flash.events.Event.preventDefault()</a>
		 * </div>
		 */
		public function stopImmediatePropagation():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Prevents processing of any event listeners in nodes subsequent to the current node in the event flow. <span>This method does not affect any event listeners in the current node (<code>currentTarget</code>). In contrast, the <code>stopImmediatePropagation()</code> method prevents processing of event listeners in both the current node and subsequent nodes. Additional calls to this method have no effect. This method can be called in any phase of the event flow.</span> </p>
		 * <p><b>Note: </b> This method does not cancel the behavior associated with this event; see <code>preventDefault()</code> for that functionality.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Event.html#stopImmediatePropagation()" target="">flash.events.Event.stopImmediatePropagation()</a>
		 *  <br>
		 *  <a href="Event.html#preventDefault()" target="">flash.events.Event.preventDefault()</a>
		 * </div>
		 */
		public function stopPropagation():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a string containing all the properties of the Event object. The string is in the following format: </p>
		 * <p><code>[Event type=<i>value</i> bubbles=<i>value</i> cancelable=<i>value</i>]</code></p>
		 * 
		 * @return  — A string containing all the properties of the Event object. 
		 */
		public function toString():String {
			throw new Error("Not implemented");
		}
		
		public static function isOfType(type:String, cl:Class):Boolean {
			for (var name:String in cl) {
				var index:int = name.indexOf("_");
				if (index == 0 || index == name.length - 1)
					continue;
				if (cl[name] == type)
					return true;
			}
			return false;
		}
		
		public static function getType(type:String, classes:Vector.<Class>):Class {
			for each (var c:Class in classes) {
				if (isOfType(type, c))
					return c;
			}
			return null;
		}
	}
}