package flash.events {
	/**
	 * <br><hr>
	 */
	public class PermissionEvent extends flash.events.Event {
		public static const PERMISSION_STATUS:String = "permissionStatus";

		private var _status:String;

		/**
		 * <p> Creates an PermissionEvent object that contains information about the name of the permission and its status. </p>
		 * 
		 * @param type
		 * @param bubbles
		 * @param cancelable
		 * @param status
		 */
		public function PermissionEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, status:String = "denied") {
			super(type, bubbles, cancelable);
			throw new Error("Not implemented");
		}

		/**
		 * <p> Check whether the permission has been granted or denied. </p>
		 * 
		 * @return 
		 */
		public function get status():String {
			return _status;
		}

		/**
		 * <p> Creates a copy of an PermissionEvent object and sets the value of each property to match that of the original. </p>
		 * 
		 * @return  — A new PermissionEvent object with property values that match those of the original. 
		 */
		override public function clone():flash.events.Event {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a string that contains all the properties of the PermissionEvent object. The following format is used: </p>
		 * <p><code>[PermissionEvent type=<i>value</i> bubbles=<i>value</i> cancelable=<i>value</i> permission=<i>value</i> status=<i>value</i>] </code></p>
		 * 
		 * @return  — A string that contains all the properties of the PermissionEvent object. 
		 */
		override public function toString():String {
			throw new Error("Not implemented");
		}
	}
}
