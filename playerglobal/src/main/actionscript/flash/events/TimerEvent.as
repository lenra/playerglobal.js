package flash.events {
	/**
	 *  A Timer object dispatches a TimerEvent objects whenever the Timer object reaches the interval specified by the <code>Timer.delay</code> property. <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../flash/utils/Timer.html" target="">flash.utils.Timer</a>
	 * </div><br><hr>
	 */
	public class TimerEvent extends flash.events.Event {
		/**
		 * <p> Defines the value of the <code>type</code> property of a <code>timer</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The Timer object that has reached its interval.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/utils/Timer.html#event:timer" target="">flash.utils.Timer.timer</a>
		 * </div>
		 */
		public static const TIMER:String = "timer";
		/**
		 * <p> Defines the value of the <code>type</code> property of a <code>timerComplete</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The Timer object that has completed its requests.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/utils/Timer.html#event:timerComplete" target="">flash.utils.Timer.timerComplete</a>
		 * </div>
		 */
		public static const TIMER_COMPLETE:String = "timerComplete";

		/**
		 * <p> Creates an Event object with specific information relevant to <code>timer</code> events. Event objects are passed as parameters to event listeners. </p>
		 * 
		 * @param type  — The type of the event. Event listeners can access this information through the inherited <code>type</code> property. 
		 * @param bubbles  — Determines whether the Event object bubbles. Event listeners can access this information through the inherited <code>bubbles</code> property. 
		 * @param cancelable  — Determines whether the Event object can be canceled. Event listeners can access this information through the inherited <code>cancelable</code> property. 
		 */
		public function TimerEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false) {
			super(type, bubbles, cancelable);
		}

		/**
		 * <p> Creates a copy of the TimerEvent object and sets each property's value to match that of the original. </p>
		 * 
		 * @return  — A new TimerEvent object with property values that match those of the original. 
		 */
		override public function clone():flash.events.Event {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a string that contains all the properties of the TimerEvent object. The string is in the following format: </p>
		 * <p><code>[TimerEvent type=<i>value</i> bubbles=<i>value</i> cancelable=<i>value</i>]</code></p>
		 * 
		 * @return  — A string that contains all the properties of the TimerEvent object. 
		 */
		override public function toString():String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Instructs <span>Flash Player or</span> the AIR runtime to render after processing of this event completes, if the display list has been modified. </p>
		 * <span id="pageFilter"><br><span class="label"> Example &nbsp;( <span class="usage"><a href="http://www.adobe.com/go/learn_as3_usingexamples_en"> How to use this example </a></span>) </span><br><br>
		 *  <div class="detailBody">
		 *    The following is an example for the 
		 *   <code>TimerEvent.updateAfterEvent()</code> method. 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>
		 * function onTimer(event:TimerEvent):void {
		 *     if (40 &lt; my_mc.x &amp;&amp; my_mc.x &lt; 375) {
		 *         my_mc.x-= 50;
		 *     } else {
		 *         my_mc.x=374;
		 *     }
		 *     event.updateAfterEvent();
		 * }
		 * 
		 * var moveTimer:Timer=new Timer(50,250);
		 * moveTimer.addEventListener(TimerEvent.TIMER,onTimer);
		 * moveTimer.start();
		 * </pre>
		 *   </div>
		 *  </div></span>
		 */
		public function updateAfterEvent():void {
			throw new Error("Not implemented");
		}
	}
}
