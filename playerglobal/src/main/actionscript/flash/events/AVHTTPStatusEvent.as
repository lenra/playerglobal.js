package flash.events {
	/**
	 *  The application dispatches AVHTTPStatusEvent objects when a network request returns an HTTP status code. <p>AVHTTPStatusEvent objects are always sent before error or completion events. An AVHTTPStatusEvent object does not necessarily indicate an error condition; it simply reflects the HTTP status code (if any) that is provided by the networking stack. <span>Some Flash Player environments may be unable to detect HTTP status codes; a status code of 0 is always reported in these cases.</span> </p> <p> <span>In Flash Player, there is only one type of HTTPStatus event: <code>httpStatus</code>.</span> In the AIR runtime, a FileReference, URLLoader, or URLStream can register to listen for an <code>httpResponseStatus</code>, which includes <code>responseURL</code> and <code>responseHeaders</code> properties. These properties are undefined in a <code>httpStatus</code> event.</p> <br><hr>
	 */
	public class AVHTTPStatusEvent extends flash.events.Event {
		/**
		 * <p> Unlike the <code>httpStatus</code> event, the <code>httpResponseStatus</code> event is delivered before any response data. Also, the <code>httpResponseStatus</code> event includes values for the <code>responseHeaders</code> and <code>responseURL</code> properties (which are undefined for an <code>httpStatus</code> event. Note that the <code>httpResponseStatus</code> event (if any) will be sent before (and in addition to) any <code>complete</code> or <code>error</code> event. </p>
		 * <p>The <code>AVHTTPStatusEvent.HTTP_RESPONSE_STATUS</code> constant defines the value of the <code>type</code> property of a <code>httpResponseStatus</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>responseURL</code></td>
		 *    <td>The URL from which the response was returned.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>responseHeaders</code></td>
		 *    <td>The response headers that the response returned, as an array of URLRequestHeader objects.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>status</code></td>
		 *    <td>The HTTP status code returned by the server.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The network object receiving an HTTP status code. </td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/net/URLStream.html#event:httpResponseStatus" target="">flash.net.URLStream.httpResponseStatus</a>
		 *  <br>
		 *  <a href="../../flash/net/FileReference.html#event:httpResponseStatus" target="">flash.net.FileReference.httpResponseStatus</a>
		 * </div>
		 */
		public static const AV_HTTP_RESPONSE_STATUS:String = "avHttpResponseStatus";

		private var _responseHeaders:Array;
		private var _responseURL:String;

		private var _status:int;

		/**
		 * <p> Creates an Event object that contains specific information about HTTP status events. Event objects are passed as parameters to event listeners. </p>
		 * 
		 * @param type  — The type of the event. Event listeners can access this information through the inherited <code>type</code> property. There is only one type of HTTPStatus event: <code>AVHTTPStatusEvent.HTTP_STATUS</code>. 
		 * @param bubbles  — Determines whether the Event object participates in the bubbling stage of the event flow. Event listeners can access this information through the inherited <code>bubbles</code> property. 
		 * @param cancelable  — Determines whether the Event object can be canceled. Event listeners can access this information through the inherited <code>cancelable</code> property. 
		 * @param status  — Numeric status. Event listeners can access this information through the <code>status</code> property. 
		 * @param responseUrl
		 * @param responseHeaders
		 */
		public function AVHTTPStatusEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, status:int = 0, responseUrl:String = null, responseHeaders:Array = null) {
			super(type, bubbles, cancelable);
			throw new Error("Not implemented");
		}

		/**
		 * <p> The response headers that the response returned, as an array of URLRequestHeader objects. </p>
		 * 
		 * @return 
		 */
		public function get responseHeaders():Array {
			return _responseHeaders;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set responseHeaders(value:Array):void {
			_responseHeaders = value;
		}

		/**
		 * <p> The URL that the response was returned from. In the case of redirects, this will be different from the request URL. </p>
		 * 
		 * @return 
		 */
		public function get responseURL():String {
			return _responseURL;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set responseURL(value:String):void {
			_responseURL = value;
		}

		/**
		 * <p> The HTTP status code returned by the server. For example, a value of 404 indicates that the server has not found a match for the requested URI. HTTP status codes can be found in sections 10.4 and 10.5 of the HTTP specification at <a href="http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html" target="external">http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html</a>. </p>
		 * <p>If <span>Flash Player or</span> AIR cannot get a status code from the server, or if it cannot communicate with the server, the default value of 0 is passed to your code. <span>A value of 0 can be generated in any player (for example, if a malformed URL is requested), and a value of 0 is always generated by the Flash Player plug-in when it is run in the following browsers, which do not pass HTTP status codes to the player: Netscape, Mozilla, Safari, Opera, and Internet Explorer for the Macintosh.</span></p>
		 * 
		 * @return 
		 */
		public function get status():int {
			return _status;
		}

		/**
		 * <p> Creates a copy of the AVHTTPStatusEvent object and sets the value of each property to match that of the original. </p>
		 * 
		 * @return  — A new AVHTTPStatusEvent object with property values that match those of the original. 
		 */
		override public function clone():flash.events.Event {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a string that contains all the properties of the AVHTTPStatusEvent object. The string is in the following format: </p>
		 * <p><code>[AVHTTPStatusEvent type=<i>value</i> bubbles=<i>value</i> cancelable=<i>value</i> status=<i>value</i>]</code></p>
		 * 
		 * @return  — A string that contains all the properties of the AVHTTPStatusEvent object. 
		 */
		override public function toString():String {
			throw new Error("Not implemented");
		}
	}
}
