package flash.events {
	/**
	 *  AVStream dispatches AVPauseAtPeriodEndEvent when the period end is reached for the period to which this event is requested. <br><hr>
	 */
	public class AVPauseAtPeriodEndEvent extends flash.events.Event {
		public static const AV_PAUSE_AT_PERIOD_END:String = "avPauseAtPeriodEnd";

		private var _userData:int;

		/**
		 * @param type
		 * @param bubbles
		 * @param cancelable
		 * @param userData
		 */
		public function AVPauseAtPeriodEndEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, userData:int = 0) {
			super(type, bubbles, cancelable);
			throw new Error("Not implemented");
		}

		/**
		 * @return 
		 */
		public function get userData():int {
			return _userData;
		}
	}
}
