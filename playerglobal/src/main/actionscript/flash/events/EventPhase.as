package flash.events {
	/**
	 *  The EventPhase class provides values for the <code>eventPhase</code> property of the Event class. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Event.html" target="">Event class</a>
	 *  <br>
	 *  <a href="EventDispatcher.html" target="">EventDispatcher class</a>
	 * </div><br><hr>
	 */
	public class EventPhase {
		/**
		 * <p> The target phase, which is the second phase of the event flow. </p>
		 */
		public static const AT_TARGET:uint = 2;
		/**
		 * <p> The bubbling phase, which is the third phase of the event flow. </p>
		 */
		public static const BUBBLING_PHASE:uint = 3;
		/**
		 * <p> The capturing phase, which is the first phase of the event flow. </p>
		 */
		public static const CAPTURING_PHASE:uint = 1;
	}
}
