package flash.events {
	/**
	 *  The TransformGestureEvent class lets you handle complex movement input events (such as moving fingers across a touch screen) that the device or operating system interprets as a gesture. A gesture can have one or more touch points. When a user interacts with a device such as a mobile phone or tablet with a touch screen, the user typically touches and moves across the screen with his or her fingers or a pointing device. You can develop applications that respond to this user interaction with the GestureEvent, PressAndTapGestureEvent, and TransformGestureEvent classes. Create event listeners using the event types defined here, or in the related GestureEvent and TouchEvent classes. And, use the properties and methods of these classes to construct event handlers that respond to the user touching the device. <p>A device or operating system interprets gesture input. So, different devices or operating systems have different requirements for individual gesture types. A swipe on one device might require different input movement than a swipe on another device. Refer to the hardware or operating system documentation to discover how the device or operating system interprets contact as a specific gesture.</p> <p>Use the Multitouch class to determine the current environment's support for touch interaction, and to manage the support of touch interaction if the current environment supports it.</p> <p> <b>Note:</b> When objects are nested on the display list, touch events target the deepest possible nested object that is visible in the display list. This object is called the target node. To have a target node's ancestor (an object containing the target node in the display list) receive notification of a touch event, use <code>EventDispatcher.addEventListener()</code> on the ancestor node with the type parameter set to the specific touch event you want to detect.</p> <p>While the user is in contact with the device, the TransformGestureEvent object's scale, rotation, and offset properties are incremental values from the previous gesture event. For example, as a gesture increases the size of a display object, the scale values might go in sequence <code>1.03</code>, <code>1.01</code>, <code>1.01</code>, <code>1.02</code> indicating the display object scaled 1.0717 times its original size by the end of the gesture.</p> <p>For TransformGestureEvent objects, properties not modified by the current gesture are set to identity values. For example, a pan gesture does not have a rotation or scale transformation, so the <code>rotation</code> value of the event object is <code>0</code>, the <code>scaleX</code> and <code>scaleY</code> properties are <code>1</code>.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WSb2ba3b1aad8a27b0-6ffb37601221e58cc29-8000.html" target="_blank">Touch, multitouch and gesture input</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../flash/ui/Multitouch.html" target="">flash.ui.Multitouch</a>
	 *  <br>
	 *  <a href="TouchEvent.html" target="">flash.events.TouchEvent</a>
	 *  <br>
	 *  <a href="GestureEvent.html" target="">flash.events.GestureEvent</a>
	 *  <br>
	 *  <a href="PressAndTapGestureEvent.html" target="">flash.events.PressAndTapGestureEvent</a>
	 *  <br>
	 *  <a href="MouseEvent.html" target="">flash.events.MouseEvent</a>
	 *  <br>
	 *  <a href="EventDispatcher.html#addEventListener()" target="">flash.events.EventDispatcher.addEventListener()</a>
	 * </div><br><hr>
	 */
	public class TransformGestureEvent extends GestureEvent {
		/**
		 * <p> Defines the value of the <code>type</code> property of a <code>GESTURE_DIRECTIONAL_TAP</code> touch event object. </p>
		 * <p>The dispatched TransformGestureEvent object has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>altKey</code></td>
		 *    <td><code>true</code> if the Alt key is active (Windows or Linux).</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>true</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>commandKey</code></td>
		 *    <td><code>true</code> on the Mac if the Command key is active; <code>false</code> if it is inactive. Always <code>false</code> on Windows.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>controlKey</code></td>
		 *    <td><code>true</code> if the Ctrl or Control key is active; <code>false</code> if it is inactive.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>ctrlKey</code></td>
		 *    <td><code>true</code> on Windows or Linux if the Ctrl key is active. <code>true</code> on Mac if either the Ctrl key or the Command key is active. Otherwise, <code>false</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>phase</code></td>
		 *    <td>The current phase in the event flow. For swipe events, this value is always <code>all</code> corresponding to the value <code>GesturePhase.ALL</code> once the event is dispatched.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>localX</code></td>
		 *    <td>The horizontal coordinate at which the event occurred relative to the containing sprite.For directional tap gestures this value is centre of the screen.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>localY</code></td>
		 *    <td>The vertical coordinate at which the event occurred relative to the containing sprite.For directional tap gestures this value is centre of the screen.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>scaleX</code></td>
		 *    <td>The horizontal scale of the display object. For directional tap gestures this value is <code>1</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>scaleY</code></td>
		 *    <td>The vertical scale of the display object. For directional tap gestures this value is <code>1</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>rotation</code></td>
		 *    <td>The current rotation angle, in degrees, of the display object along the z-axis. For directional tap gestures this value is <code>0</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>offsetX</code></td>
		 *    <td>Indicates horizontal direction: 1 for right and -1 for left.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>offsetY</code></td>
		 *    <td>Indicates vertical direction: 1 for down and -1 for up.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>velocity</code></td>
		 *    <td>Indicates velocity of the swipe gesture in pixels per second (AIR only). </td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>shiftKey</code></td>
		 *    <td><code>true</code> if the Shift key is active; <code>false</code> if it is inactive.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The InteractiveObject instance under the touching device. The <code>target</code> is not always the object in the display list that registered the event listener. Use the <code>currentTarget</code> property to access the object in the display list that is currently processing the event.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/InteractiveObject.html#event:gestureDirectionalTap" target="">flash.display.InteractiveObject.gestureDirectionalTap</a>
		 *  <br>
		 *  <a href="GesturePhase.html" target="">flash.events.GesturePhase</a>
		 * </div>
		 */
		public static const GESTURE_DIRECTIONAL_TAP:String = "gestureDirectionalTap";
		/**
		 * <p> Defines the value of the <code>type</code> property of a <code>GESTURE_PAN</code> touch event object. </p>
		 * <p>The dispatched TransformGestureEvent object has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>altKey</code></td>
		 *    <td><code>true</code> if the Alt key is active (Windows or Linux).</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>true</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>commandKey</code></td>
		 *    <td><code>true</code> on the Mac if the Command key is active; <code>false</code> if it is inactive. Always <code>false</code> on Windows.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>controlKey</code></td>
		 *    <td><code>true</code> if the Ctrl or Control key is active; <code>false</code> if it is inactive.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>ctrlKey</code></td>
		 *    <td><code>true</code> on Windows or Linux if the Ctrl key is active. <code>true</code> on Mac if either the Ctrl key or the Command key is active. Otherwise, <code>false</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>phase</code></td>
		 *    <td>The current phase in the event flow; a value from the GesturePhase class.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>localX</code></td>
		 *    <td>The horizontal coordinate at which the event occurred relative to the containing display object.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>localY</code></td>
		 *    <td>The vertical coordinate at which the event occurred relative to the containing display object.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>scaleX</code></td>
		 *    <td>The horizontal scale of the display object since the previous gesture event. For pan gestures this value is <code>1</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>scaleY</code></td>
		 *    <td>The vertical scale of the display object since the previous gesture event. For pan gestures this value is <code>1</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>rotation</code></td>
		 *    <td>The current rotation angle, in degrees, of the display object along the z-axis, since the previous gesture event. For pan gestures this value is <code>0</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>offsetX</code></td>
		 *    <td>The horizontal translation of the display object from its position at the previous gesture event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>offsetY</code></td>
		 *    <td>The vertical translation of the display object from its position at the previous gesture event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>velocity</code></td>
		 *    <td>The velocity of the gesture event in pixels per second (AIR only). For pan gesture this value is <code>0</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>shiftKey</code></td>
		 *    <td><code>true</code> if the Shift key is active; <code>false</code> if it is inactive.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The InteractiveObject instance under the touching device. The <code>target</code> is not always the object in the display list that registered the event listener. Use the <code>currentTarget</code> property to access the object in the display list that is currently processing the event.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/InteractiveObject.html#event:gesturePan" target="">flash.display.InteractiveObject.gesturePan</a>
		 *  <br>
		 *  <a href="GesturePhase.html" target="">flash.events.GesturePhase</a>
		 * </div>
		 * <span id="pageFilter"><br><span class="label"> Example &nbsp;( <span class="usage"><a href="http://www.adobe.com/go/learn_as3_usingexamples_en"> How to use this example </a></span>) </span><br><br>
		 *  <div class="detailBody">
		 *    The following example shows event handling for the 
		 *   <code>GESTURE_PAN</code> events. While the user performs a pan gesture on the touch-enabled device, myTextField populates with the current phase. 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>
		 * Multitouch.inputMode = MultitouchInputMode.GESTURE;
		 * 
		 * var mySprite = new Sprite();
		 * mySprite.addEventListener(TransformGestureEvent.GESTURE_PAN , onPan);
		 * mySprite.graphics.beginFill(0x336699);
		 * mySprite.graphics.drawRect(0, 0, 100, 80);
		 * var myTextField = new TextField();
		 * myTextField.y = 200;
		 * addChild(mySprite);
		 * addChild(myTextField);
		 * 
		 * function onPan(evt:TransformGestureEvent):void {
		 * 
		 *     evt.target.localX++;
		 * 
		 *     if (evt.phase==GesturePhase.BEGIN) {
		 *         myTextField.text = "Begin";
		 *     }
		 *     if (evt.phase==GesturePhase.UPDATE) {
		 *         myTextField.text = "Update";
		 *     }
		 *     if (evt.phase==GesturePhase.END) {
		 *         myTextField.text = "End";
		 *     }
		 * }
		 * </pre>
		 *   </div>
		 *  </div></span>
		 */
		public static const GESTURE_PAN:String = "gesturePan";
		/**
		 * <p> Defines the value of the <code>type</code> property of a <code>GESTURE_ROTATE</code> touch event object. </p>
		 * <p>During this event, the <code>rotation</code> property contains the rotation angle. The rotation values are as follows:</p>
		 * <ul>
		 *  <li>0 to 180 degrees for clockwise direction</li>
		 *  <li>- 180 to 0 degrees for counter-clockwise direction</li>
		 * </ul>
		 * <p>The dispatched TransformGestureEvent object has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>altKey</code></td>
		 *    <td><code>true</code> if the Alt key is active (Windows or Linux).</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>true</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>commandKey</code></td>
		 *    <td><code>true</code> on the Mac if the Command key is active; <code>false</code> if it is inactive. Always <code>false</code> on Windows.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>controlKey</code></td>
		 *    <td><code>true</code> if the Ctrl or Control key is active; <code>false</code> if it is inactive.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>ctrlKey</code></td>
		 *    <td><code>true</code> on Windows or Linux if the Ctrl key is active. <code>true</code> on Mac if either the Ctrl key or the Command key is active. Otherwise, <code>false</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>phase</code></td>
		 *    <td>The current phase in the event flow; a value from the GesturePhase class.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>localX</code></td>
		 *    <td>The horizontal coordinate at which the event occurred relative to the containing display object.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>localY</code></td>
		 *    <td>The vertical coordinate at which the event occurred relative to the containing display object.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>scaleX</code></td>
		 *    <td>The horizontal scale of the display object since the previous gesture event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>scaleY</code></td>
		 *    <td>The vertical scale of the display object since the previous gesture event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>rotation</code></td>
		 *    <td>The current rotation angle, in degrees, of the display object along the z-axis, since the previous gesture event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>offsetX</code></td>
		 *    <td>The horizontal translation of the display object from its position at the previous gesture event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>offsetY</code></td>
		 *    <td>The vertical translation of the display object from its position at the previous gesture event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>velocity</code></td>
		 *    <td>The velocity of the gesture event in pixels per second (AIR only). For rotate gesture this value is <code>0</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>shiftKey</code></td>
		 *    <td><code>true</code> if the Shift key is active; <code>false</code> if it is inactive.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The InteractiveObject instance under the touching device. The <code>target</code> is not always the object in the display list that registered the event listener. Use the <code>currentTarget</code> property to access the object in the display list that is currently processing the event.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/InteractiveObject.html#event:gestureRotate" target="">flash.display.InteractiveObject.gestureRotate</a>
		 *  <br>
		 *  <a href="GesturePhase.html" target="">flash.events.GesturePhase</a>
		 * </div>
		 * <span id="pageFilter"><br><span class="label"> Example &nbsp;( <span class="usage"><a href="http://www.adobe.com/go/learn_as3_usingexamples_en"> How to use this example </a></span>) </span><br><br>
		 *  <div class="detailBody">
		 *    The following example shows event handling for the 
		 *   <code>GESTURE_ROTATE</code> events. While the user performs a rotation gesture on the touch-enabled device, mySprite rotates and myTextField populates with the current phase. 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>
		 * Multitouch.inputMode = MultitouchInputMode.GESTURE;
		 * 
		 * var mySprite = new Sprite();
		 * mySprite.addEventListener(TransformGestureEvent.GESTURE_ROTATE , onRotate );
		 * mySprite.graphics.beginFill(0x336699);
		 * mySprite.graphics.drawRect(0, 0, 100, 80);
		 * var myTextField = new TextField();
		 * myTextField.y = 200;
		 * addChild(mySprite);
		 * addChild(myTextField);
		 * 
		 * function onRotate(evt:TransformGestureEvent):void {
		 * 
		 *     evt.target.rotation -= 45;
		 * 
		 *     if (evt.phase==GesturePhase.BEGIN) {
		 *         myTextField.text = "Begin";
		 *     }
		 *     if (evt.phase==GesturePhase.UPDATE) {
		 *         myTextField.text = "Update";
		 *     }
		 *     if (evt.phase==GesturePhase.END) {
		 *         myTextField.text = "End";
		 *     }
		 * }
		 * </pre>
		 *   </div>
		 *  </div></span>
		 */
		public static const GESTURE_ROTATE:String = "gestureRotate";
		/**
		 * <p> Defines the value of the <code>type</code> property of a <code>GESTURE_SWIPE</code> touch event object. </p>
		 * <p>The dispatched TransformGestureEvent object has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>altKey</code></td>
		 *    <td><code>true</code> if the Alt key is active (Windows or Linux).</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>true</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>commandKey</code></td>
		 *    <td><code>true</code> on the Mac if the Command key is active; <code>false</code> if it is inactive. Always <code>false</code> on Windows.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>controlKey</code></td>
		 *    <td><code>true</code> if the Ctrl or Control key is active; <code>false</code> if it is inactive.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>ctrlKey</code></td>
		 *    <td><code>true</code> on Windows or Linux if the Ctrl key is active. <code>true</code> on Mac if either the Ctrl key or the Command key is active. Otherwise, <code>false</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>phase</code></td>
		 *    <td>The current phase in the event flow. For swipe events, this value is always <code>all</code> corresponding to the value <code>GesturePhase.ALL</code> once the event is dispatched.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>localX</code></td>
		 *    <td>The horizontal coordinate at which the event occurred relative to the containing sprite.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>localY</code></td>
		 *    <td>The vertical coordinate at which the event occurred relative to the containing sprite.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>scaleX</code></td>
		 *    <td>The horizontal scale of the display object. For swipe gestures this value is <code>1</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>scaleY</code></td>
		 *    <td>The vertical scale of the display object. For swipe gestures this value is <code>1</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>rotation</code></td>
		 *    <td>The current rotation angle, in degrees, of the display object along the z-axis. For swipe gestures this value is <code>0</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>offsetX</code></td>
		 *    <td>Indicates horizontal direction: 1 for right and -1 for left.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>offsetY</code></td>
		 *    <td>Indicates vertical direction: 1 for down and -1 for up.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>velocity</code></td>
		 *    <td>Indicates velocity of the swipe gesture in pixels per second (AIR only). For rest, this value is <code>0</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>shiftKey</code></td>
		 *    <td><code>true</code> if the Shift key is active; <code>false</code> if it is inactive.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The InteractiveObject instance under the touching device. The <code>target</code> is not always the object in the display list that registered the event listener. Use the <code>currentTarget</code> property to access the object in the display list that is currently processing the event.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/InteractiveObject.html#event:gestureSwipe" target="">flash.display.InteractiveObject.gestureSwipe</a>
		 *  <br>
		 *  <a href="GesturePhase.html" target="">flash.events.GesturePhase</a>
		 * </div>
		 * <span id="pageFilter"><br><span class="label"> Example &nbsp;( <span class="usage"><a href="http://www.adobe.com/go/learn_as3_usingexamples_en"> How to use this example </a></span>) </span><br><br>
		 *  <div class="detailBody">
		 *    The following example shows event handling for the 
		 *   <code>GESTURE_SWIPE</code> events. While the user performs a swipe gesture on the touch-enabled device, myTextField populates with the phase 
		 *   <code>all</code>, which is the only phase for swipe events. 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>
		 * Multitouch.inputMode = MultitouchInputMode.GESTURE;
		 * 
		 * var mySprite = new Sprite();
		 * mySprite.addEventListener(TransformGestureEvent.GESTURE_SWIPE , onSwipe);
		 * mySprite.graphics.beginFill(0x336699);
		 * mySprite.graphics.drawRect(0, 0, 100, 80);
		 * var myTextField = new TextField();
		 * myTextField.y = 200;
		 * addChild(mySprite);
		 * addChild(myTextField);
		 * 
		 * function onSwipe(evt:TransformGestureEvent):void {
		 * 
		 *     if (evt.offsetX == 1 ) {
		 *     myTextField.text = "right";
		 *     }
		 *     if (evt.offsetY == -1) {
		 *     myTextField.text = "up";
		 *     }
		 *     myTextField.text = evt.phase;
		 * 
		 * }
		 * </pre>
		 *   </div>
		 *  </div></span>
		 */
		public static const GESTURE_SWIPE:String = "gestureSwipe";
		/**
		 * <p> Defines the value of the <code>type</code> property of a <code>GESTURE_ZOOM</code> touch event object. </p>
		 * <p>The dispatched TransformGestureEvent object has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>altKey</code></td>
		 *    <td><code>true</code> if the Alt key is active (Windows or Linux).</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>true</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>commandKey</code></td>
		 *    <td><code>true</code> on the Mac if the Command key is active; <code>false</code> if it is inactive. Always <code>false</code> on Windows.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>controlKey</code></td>
		 *    <td><code>true</code> if the Ctrl or Control key is active; <code>false</code> if it is inactive.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>ctrlKey</code></td>
		 *    <td><code>true</code> on Windows or Linux if the Ctrl key is active. <code>true</code> on Mac if either the Ctrl key or the Command key is active. Otherwise, <code>false</code>.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>phase</code></td>
		 *    <td>The current phase in the event flow; a value from the GesturePhase class.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>localX</code></td>
		 *    <td>The horizontal coordinate at which the event occurred relative to the containing display object.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>localY</code></td>
		 *    <td>The vertical coordinate at which the event occurred relative to the containing display object.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>scaleX</code></td>
		 *    <td>The horizontal scale of the display object since the previous gesture event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>scaleY</code></td>
		 *    <td>The vertical scale of the display object since the previous gesture event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>rotation</code></td>
		 *    <td>The current rotation angle, in degrees, of the display object along the z-axis, since the previous gesture event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>offsetX</code></td>
		 *    <td>The horizontal translation of the display object from its position at the previous gesture event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>offsetY</code></td>
		 *    <td>The vertical translation of the display object from its position at the previous gesture event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>velocity</code></td>
		 *    <td>The velocity of the gesture event in pixels per second (AIR only). For zoom gesture this value is <code>0</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>shiftKey</code></td>
		 *    <td><code>true</code> if the Shift key is active; <code>false</code> if it is inactive.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The InteractiveObject instance under the touching device. The <code>target</code> is not always the object in the display list that registered the event listener. Use the <code>currentTarget</code> property to access the object in the display list that is currently processing the event.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/InteractiveObject.html#event:gestureZoom" target="">flash.display.InteractiveObject.gestureZoom</a>
		 *  <br>
		 *  <a href="GesturePhase.html" target="">flash.events.GesturePhase</a>
		 * </div>
		 * <span id="pageFilter"><br><span class="label"> Example &nbsp;( <span class="usage"><a href="http://www.adobe.com/go/learn_as3_usingexamples_en"> How to use this example </a></span>) </span><br><br>
		 *  <div class="detailBody">
		 *    The following example shows event handling for the 
		 *   <code>GESTURE_ZOOM</code> events. While the user performs a zoom gesture on the touch-enabled device, myTextField populates with the current phase. 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>
		 * Multitouch.inputMode = MultitouchInputMode.GESTURE;
		 * 
		 * var mySprite = new Sprite();
		 * mySprite.addEventListener(TransformGestureEvent.GESTURE_ZOOM , onZoom);
		 * mySprite.graphics.beginFill(0x336699);
		 * mySprite.graphics.drawRect(0, 0, 100, 80);
		 * var myTextField = new TextField();
		 * myTextField.y = 200;
		 * addChild(mySprite);
		 * addChild(myTextField);
		 * 
		 * function onZoom(evt:TransformGestureEvent):void {
		 * 
		 *     evt.target.scaleX++;
		 * 
		 *     if (evt.phase==GesturePhase.BEGIN) {
		 *         myTextField.text = "Begin";
		 *     }
		 *     if (evt.phase==GesturePhase.UPDATE) {
		 *         myTextField.text = "Update";
		 *     }
		 *     if (evt.phase==GesturePhase.END) {
		 *         myTextField.text = "End";
		 *     }
		 * }
		 * </pre>
		 *   </div>
		 *  </div></span>
		 */
		public static const GESTURE_ZOOM:String = "gestureZoom";

		private var _offsetX:Number;
		private var _offsetY:Number;
		private var _rotation:Number;
		private var _scaleX:Number;
		private var _scaleY:Number;
		private var _velocity:Number;

		/**
		 * <p> Creates an Event object that contains information about complex multi-touch events, such as a user sliding his or her finger across a screen. Event objects are passed as parameters to event listeners. </p>
		 * 
		 * @param type  — The type of the event. Possible values are: <code>TransformGestureEvent.GESTURE_PAN</code>, <code>TransformGestureEvent.GESTURE_ROTATE</code>, <code>TransformGestureEvent.GESTURE_SWIPE</code> and <code>TransformGestureEvent.GESTURE_ZOOM</code>. 
		 * @param bubbles  — Determines whether the Event object participates in the bubbling phase of the event flow. 
		 * @param cancelable  — Determines whether the Event object can be canceled. 
		 * @param phase  — This values tracks the beginning, progress, and end of a touch gesture. Possible values are: <code>GesturePhase.BEGIN</code>, <code>GesturePhase.END</code>, and <code>GesturePhase.UPDATE</code>. 
		 * @param localX  — The horizontal coordinate at which the event occurred relative to the containing display object. 
		 * @param localY  — The vertical coordinate at which the event occurred relative to the containing display object. 
		 * @param scaleX  — The horizontal scale of the display object. 
		 * @param scaleY  — The vertical scale of the display object. 
		 * @param rotation  — The current rotation angle, in degrees, of the display object along the z-axis. 
		 * @param offsetX  — The horizontal translation of the display object from its original position. 
		 * @param offsetY  — The vertical translation of the display object from its original position. 
		 * @param ctrlKey  — (AIR only) The velocity of transform gesture in pixels per second. 
		 * @param altKey  — On Windows or Linux, indicates whether the Ctrl key is activated. On Mac, indicates whether either the Ctrl key or the Command key is activated. 
		 * @param shiftKey  — Indicates whether the Alt key is activated (Windows or Linux only). 
		 * @param commandKey  — Indicates whether the Shift key is activated. 
		 * @param controlKey  — (AIR only) Indicates whether the Command key is activated (Mac only). This parameter is for Adobe AIR only; do not set it for Flash Player content. 
		 * @param velocity  — (AIR only) Indicates whether the Control or Ctrl key is activated. This parameter is for Adobe AIR only; do not set it for Flash Player content. 
		 */
		public function TransformGestureEvent(type:String, bubbles:Boolean = true, cancelable:Boolean = false, phase:String = null, localX:Number = 0, localY:Number = 0, scaleX:Number = 1.0, scaleY:Number = 1.0, rotation:Number = 0, offsetX:Number = 0, offsetY:Number = 0, ctrlKey:Boolean = false, altKey:Boolean = false, shiftKey:Boolean = false, commandKey:Boolean = false, controlKey:Boolean = false, velocity:Number = 0) {
			super(type, bubbles, cancelable, phase, localX, localY, ctrlKey, altKey, shiftKey, commandKey, controlKey);
			throw new Error("Not implemented");
		}

		/**
		 * <p> The horizontal translation of the display object, since the previous gesture event. </p>
		 * 
		 * @return 
		 */
		public function get offsetX():Number {
			return _offsetX;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set offsetX(value:Number):void {
			_offsetX = value;
		}

		/**
		 * <p> The vertical translation of the display object, since the previous gesture event. </p>
		 * 
		 * @return 
		 */
		public function get offsetY():Number {
			return _offsetY;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set offsetY(value:Number):void {
			_offsetY = value;
		}

		/**
		 * <p> The current rotation angle, in degrees, of the display object along the z-axis, since the previous gesture event. </p>
		 * 
		 * @return 
		 */
		public function get rotation():Number {
			return _rotation;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set rotation(value:Number):void {
			_rotation = value;
		}

		/**
		 * <p> The horizontal scale of the display object, since the previous gesture event. </p>
		 * 
		 * @return 
		 */
		public function get scaleX():Number {
			return _scaleX;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set scaleX(value:Number):void {
			_scaleX = value;
		}

		/**
		 * <p> The vertical scale of the display object, since the previous gesture event. </p>
		 * 
		 * @return 
		 */
		public function get scaleY():Number {
			return _scaleY;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set scaleY(value:Number):void {
			_scaleY = value;
		}

		/**
		 * <p> The velocity, in pixels per second, of the transform gesture event. </p>
		 * 
		 * @return 
		 */
		public function get velocity():Number {
			return _velocity;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set velocity(value:Number):void {
			_velocity = value;
		}

		/**
		 * <p> Creates a copy of the TransformGestureEvent object and sets the value of each property to match that of the original. </p>
		 * 
		 * @return  — A new TransformGestureEvent object with property values that match those of the original. 
		 */
		override public function clone():flash.events.Event {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a string that contains all the properties of the TransformGestureEvent object. The string is in the following format: </p>
		 * <p><code>[TransformGestureEvent type=<i>value</i> bubbles=<i>value</i> cancelable=<i>value</i> ... ]</code></p>
		 * 
		 * @return  — A string that contains all the properties of the TransformGestureEvent object. 
		 */
		override public function toString():String {
			throw new Error("Not implemented");
		}
	}
}
