package flash.events {
	import flash.utils.ByteArray;

	/**
	 *  The DRMManager dispatches a DRMAuthenticationCompleteEvent object when a call to the <code>authenticate()</code> method of the DRMManager object succeeds. <br><hr>
	 */
	public class DRMAuthenticationCompleteEvent extends flash.events.Event {
		/**
		 * <p> The string constant to use for the authentication complete event in the type parameter when adding and removing event listeners. </p>
		 */
		public static const AUTHENTICATION_COMPLETE:String = "authenticationComplete";

		private var _domain:String;
		private var _serverURL:String;
		private var _token:ByteArray;

		/**
		 * <p> Creates a new instance of a DRMAuthenticationCompleteEvent object. </p>
		 * 
		 * @param type  — the event type string 
		 * @param bubbles  — whether the event bubbles up the display list 
		 * @param cancelable  — whether the event can be canceled 
		 * @param inServerURL  — the URL of the logged-in server 
		 * @param inDomain  — the authenticated domain on the logged-in server 
		 * @param inToken  — the authentication token 
		 */
		public function DRMAuthenticationCompleteEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, inServerURL:String = null, inDomain:String = null, inToken:ByteArray = null) {
			super(type, bubbles, cancelable);
			throw new Error("Not implemented");
		}

		/**
		 * <p> The content domain of the media rights server. Here, domain is not a network or Internet domain name. </p>
		 * 
		 * @return 
		 */
		public function get domain():String {
			return _domain;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set domain(value:String):void {
			_domain = value;
		}

		/**
		 * <p> The URL of the media rights server. </p>
		 * 
		 * @return 
		 */
		public function get serverURL():String {
			return _serverURL;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set serverURL(value:String):void {
			_serverURL = value;
		}

		/**
		 * <p> The authentication token. </p>
		 * <p>The authentication is automatically added to the DRMManager session cache. You can save the token and use it to authenticate the user in a future session. Reuse a token with the <code>setAuthenticationToken()</code> method of the DRMManager. Token expiration and other properties are determined by the server generating the token.</p>
		 * 
		 * @return 
		 */
		public function get token():ByteArray {
			return _token;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set token(value:ByteArray):void {
			_token = value;
		}

		/**
		 * @return  — A new Event object that is identical to the original. 
		 */
		override public function clone():flash.events.Event {
			throw new Error("Not implemented");
		}
	}
}
