package flash.events {
	/**
	 *  A OutputProgressEvent object is dispatched when a load operation has begun or a socket has received data. These events are usually generated when SWF files, images or data are loaded into an application. There are two types of progress events: <code>ProgressEvent.PROGRESS</code> and <code>ProgressEvent.SOCKET_DATA</code>. Additionally, in AIR ProgressEvent objects are dispatched when a data is sent to or from a child process using the NativeProcess class. <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../flash/filesystem/package-detail.html" target="">FileStream class</a>
	 *  <br>
	 *  <a href="../../flash/display/LoaderInfo.html" target="">LoaderInfo class</a>
	 *  <br>
	 *  <a href="../../flash/net/Socket.html" target="">Socket class</a>
	 * </div><br><hr>
	 */
	public class OutputProgressEvent extends flash.events.Event {
		/**
		 * <p> Defines the value of the <code>type</code> property of a <code>progress</code> event object. </p>
		 * <p>This event has the following properties:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Value</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bubbles</code></td>
		 *    <td><code>false</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bytesPending</code></td>
		 *    <td>The number of items or bytes loaded at the time the listener processes the event.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>bytesTotal</code></td>
		 *    <td>The total number of items or bytes that ultimately will be loaded if the loading process succeeds.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>cancelable</code></td>
		 *    <td><code>false</code>; there is no default behavior to cancel.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>currentTarget</code></td>
		 *    <td>The object that is actively processing the Event object with an event listener.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>target</code></td>
		 *    <td>The object reporting progress. </td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/display/LoaderInfo.html#event:progress" target="">flash.display.LoaderInfo.progress</a>
		 *  <br>
		 *  <a href="../../flash/media/Sound.html#event:progress" target="">flash.media.Sound.progress</a>
		 *  <br>
		 *  <a href="../../flash/net/FileReference.html#event:progress" target="">flash.net.FileReference.progress</a>
		 *  <br>
		 *  <a href="../../flash/net/URLLoader.html#event:progress" target="">flash.net.URLLoader.progress</a>
		 *  <br>
		 *  <a href="../../flash/net/URLStream.html#event:progress" target="">flash.net.URLStream.progress</a>
		 * </div>
		 */
		public static const OUTPUT_PROGRESS:String = "outputProgress";

		private var _bytesPending:Number;
		private var _bytesTotal:Number;

		/**
		 * <p> Creates an Event object that contains information about progress events. Event objects are passed as parameters to event listeners. </p>
		 * 
		 * @param type  — The type of the event. Possible values are:<code>ProgressEvent.PROGRESS</code>, <code>ProgressEvent.SOCKET_DATA</code>, <code>ProgressEvent.STANDARD_ERROR_DATA</code>, <code>ProgressEvent.STANDARD_INPUT_PROGRESS</code>, and <code>ProgressEvent.STANDARD_OUTPUT_DATA</code>. 
		 * @param bubbles  — Determines whether the Event object participates in the bubbling stage of the event flow. 
		 * @param cancelable  — Determines whether the Event object can be canceled. 
		 * @param bytesPending  — The number of items or bytes loaded at the time the listener processes the event. 
		 * @param bytesTotal  — The total number of items or bytes that will be loaded if the loading process succeeds. 
		 */
		public function OutputProgressEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, bytesPending:Number = 0, bytesTotal:Number = 0) {
			super(type, bubbles, cancelable);
			_bytesPending = bytesPending;
			_bytesTotal = bytesTotal;
		}

		/**
		 * <p> The number of items or bytes loaded when the listener processes the event. </p>
		 * 
		 * @return 
		 */
		public function get bytesPending():Number {
			return _bytesPending;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set bytesPending(value:Number):void {
			_bytesPending = value;
		}

		/**
		 * <p> The total number of items or bytes that will be loaded if the loading process succeeds. If the progress event is dispatched/attached to a Socket object, the bytesTotal will always be 0 unless a value is specified in the bytesTotal parameter of the constructor. The actual number of bytes sent back or forth is not set and is up to the application developer. </p>
		 * 
		 * @return 
		 */
		public function get bytesTotal():Number {
			return _bytesTotal;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set bytesTotal(value:Number):void {
			_bytesTotal = value;
		}

		/**
		 * <p> Creates a copy of the ProgressEvent object and sets each property's value to match that of the original. </p>
		 * 
		 * @return  — A new ProgressEvent object with property values that match those of the original. 
		 */
		override public function clone():flash.events.Event {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a string that contains all the properties of the ProgressEvent object. The string is in the following format: </p>
		 * <p><code>[ProgressEvent type=<i>value</i> bubbles=<i>value</i> cancelable=<i>value</i> bytesPending=<i>value</i> bytesTotal=<i>value</i>]</code></p>
		 * 
		 * @return  — A string that contains all the properties of the  object. 
		 */
		override public function toString():String {
			throw new Error("Not implemented");
		}
	}
}
