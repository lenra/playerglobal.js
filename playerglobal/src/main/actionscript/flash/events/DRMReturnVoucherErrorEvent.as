package flash.events {
	/**
	 *  The DRMManager dispatches a DRMReturnVoucherErrorEvent object when a call to the <code>returnVoucher()</code> method of the DRMManager object fails. <br><hr>
	 */
	public class DRMReturnVoucherErrorEvent extends ErrorEvent {
		/**
		 * <p> The string constant to use for the return voucher error event in the type parameter when adding and removing event listeners. </p>
		 */
		public static const RETURN_VOUCHER_ERROR:String = "returnVoucherError";

		private var _licenseID:String;
		private var _policyID:String;
		private var _serverURL:String;
		private var _subErrorID:int;

		/**
		 * <p> Creates a new instance of a DRMReturnVoucherErrorEvent object. </p>
		 * 
		 * @param type  — the event type string 
		 * @param bubbles  — whether the event bubbles up the display list 
		 * @param cancelable  — whether the event can be canceled 
		 * @param inDetail  — The error description 
		 * @param inErrorID  — The ID of the general type of error 
		 * @param inSubErrorID  — The ID indicating the specific error within its type 
		 * @param inServerURL  — the URL of the logged-in server 
		 * @param inLicenseID  — the authenticated domain on the logged-in server 
		 * @param inPolicyID
		 */
		public function DRMReturnVoucherErrorEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, inDetail:String = "", inErrorID:int = 0, inSubErrorID:int = 0, inServerURL:String = null, inLicenseID:String = null, inPolicyID:String = null) {
			super(type, bubbles, cancelable, text);
			throw new Error("Not implemented");
		}

		/**
		 * <p> The license ID that was passed into the returnVoucher() call that resulted in this error. </p>
		 * 
		 * @return 
		 */
		public function get licenseID():String {
			return _licenseID;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set licenseID(value:String):void {
			_licenseID = value;
		}

		/**
		 * <p> The policy ID that was passed into the returnVoucher() call that resulted in this error. </p>
		 * 
		 * @return 
		 */
		public function get policyID():String {
			return _policyID;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set policyID(value:String):void {
			_policyID = value;
		}

		/**
		 * <p> The URL of the media rights server for this return Voucher attempt. </p>
		 * 
		 * @return 
		 */
		public function get serverURL():String {
			return _serverURL;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set serverURL(value:String):void {
			_serverURL = value;
		}

		/**
		 * <p> A more detailed error code. </p>
		 * 
		 * @return 
		 */
		public function get subErrorID():int {
			return _subErrorID;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set subErrorID(value:int):void {
			_subErrorID = value;
		}

		/**
		 * @return  — A new ErrorEvent object with property values that match those of the original. 
		 */
		override public function clone():flash.events.Event {
			throw new Error("Not implemented");
		}
	}
}
