package flash.events {
	/**
	 *  The DRMManager dispatches a DRMLicenseRequestEvent object before each call to the <code>loadVoucher()</code> or <code>loadPreviewVoucher()</code> methods of the DRMManager object succeeds. This can also get called during playback of protected video content. <br><hr>
	 */
	public class DRMLicenseRequestEvent extends flash.events.Event {
		/**
		 * <p> The string constant to use for the license request event in the type parameter when adding and removing event listeners. </p>
		 */
		public static const LICENSE_REQUEST:String = "licenseRequest";

		private var _serverURL:String;

		/**
		 * <p> Creates a new instance of a DRMLicenseRequestEvent object. </p>
		 * 
		 * @param type  — the event type string 
		 * @param bubbles  — whether the event bubbles up the display list 
		 * @param cancelable  — whether the event can be canceled 
		 * @param inServerURL  — the URL of the license server 
		 */
		public function DRMLicenseRequestEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, inServerURL:String = null) {
			super(type, bubbles, cancelable);
			throw new Error("Not implemented");
		}

		/**
		 * <p> The URL which will be used to communicate with the license server </p>
		 * 
		 * @return 
		 */
		public function get serverURL():String {
			return _serverURL;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set serverURL(value:String):void {
			_serverURL = value;
		}

		/**
		 * @return  — A new Event object that is identical to the original. 
		 */
		override public function clone():flash.events.Event {
			throw new Error("Not implemented");
		}
	}
}
