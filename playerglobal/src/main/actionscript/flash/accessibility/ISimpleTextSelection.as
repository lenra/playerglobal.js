package flash.accessibility {
	/**
	 *  The ISimpleTextSelection class can be used to add support for the MSAA ISimpleTextSelection interface to an AccessibilityImplementation. <p>If an AccessibilityImplementation subclass implements the two getters in this class, a screen reader such as JAWS can determine the text selection range by calling them. The AccessibilityImplementation subclass does not have to formally declare that it implements this interface; you can simply declare getters for these two properties, as follows:</p> <div class="listing">
	 *  <div class="clipcopy">
	 *   <a href="#" class="copyText">Copy</a>
	 *  </div>
	 *  <pre>
	 *     class TextAreaAccImpl extends AccesibilityImplementation
	 *     {
	 *     ...
	 *         public function get selectionAnchorIndex():int
	 *         {
	 *         ...
	 *         }
	 *         public function get selectionActiveIndex():int
	 *         {
	 *         ...
	 *     }
	 *     }
	 *     </pre>
	 * </div> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="AccessibilityImplementation.html" target="">flash.accessibility.AccessibilityImplementation</a>
	 * </div><br><hr>
	 */
	public interface ISimpleTextSelection {

		/**
		 * <p> The zero-based character index value of the last character in the current selection. If you want a component to support inline IME or accessibility, override this method. </p>
		 * 
		 * @return 
		 */
		function get selectionActiveIndex():int;

		/**
		 * <p> The zero-based character index value of the first character in the current selection. If you want a component to support inline IME or accessibility, override this method. </p>
		 * 
		 * @return 
		 */
		function get selectionAnchorIndex():int;
	}
}
