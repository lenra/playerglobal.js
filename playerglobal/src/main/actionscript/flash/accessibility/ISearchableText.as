package flash.accessibility {
	/**
	 *  The ISearchableText interface can be implemented by objects that contain text which should be searchable on the web. <br><hr>
	 */
	public interface ISearchableText {

		/**
		 * <p> Gets the search text from a component implementing ISearchableText. </p>
		 * 
		 * @return 
		 */
		function get searchText():String;
	}
}
