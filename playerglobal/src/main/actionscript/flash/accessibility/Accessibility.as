package flash.accessibility {
	/**
	 *  The Accessibility class manages communication with screen readers. Screen readers are a type of assistive technology for visually impaired users that provides an audio version of screen content. The methods of the Accessibility class are static—that is, you don't have to create an instance of the class to use its methods. <p> <b>Mobile Browser Support:</b> This class is not supported in mobile browsers.</p> <p> <i>AIR profile support:</i> This feature is supported on all desktop operating systems, but is not supported on mobile devices or on AIR for TV devices. See <a href="http://help.adobe.com/en_US/air/build/WS144092a96ffef7cc16ddeea2126bb46b82f-8000.html"> AIR Profile Support</a> for more information regarding API support across multiple profiles.</p> <p>To get and set accessible properties for a specific object, such as a button, movie clip, or text field, use the <code>DisplayObject.accessibilityProperties</code> property. To determine whether the player or runtime is running in an environment that supports accessibility aids, use the <code>Capabilities.hasAccessibility</code> property. </p> <p> <b>Note:</b> AIR 2 supports the JAWS 11 (or higher) screen reader software. For additional information, please see http://www.adobe.com/accessibility/.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://www.adobe.com/accessibility/" target="_blank">http://www.adobe.com/accessibility/</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="../../flash/display/DisplayObject.html#accessibilityProperties" target="">flash.display.DisplayObject.accessibilityProperties</a>
	 *  <br>
	 *  <a href="../../flash/system/Capabilities.html#hasAccessibility" target="">flash.system.Capabilities.hasAccessibility</a>
	 *  <br>
	 *  <a href="../../flash/net/Socket.html" target="">Socket</a>
	 * </div><br><hr>
	 */
	public class Accessibility {
		private static var _active:Boolean;


		/**
		 * <p> Indicates whether a screen reader is active and the application is communicating with it. Use this method when you want your application to behave differently in the presence of a screen reader. </p>
		 * <p>Once this property is set to <code>true</code>, it remains <code>true</code> for the duration of the application. (It is unusual for a user to turn off the screen reader once it is started.)</p>
		 * <p><b>Note: </b>Before calling this method, wait 1 or 2 seconds after launching your AIR application or after the first appearance of the Flash<sup>®</sup> Player window in which your document is playing. Otherwise, you might get a return value of <code>false</code> even if there is an active accessibility client. This happens because of an asynchronous communication mechanism between accessibility clients and Flash Player or AIR.</p>
		 * <code>Capabilities.hasAccessibility</code>
		 * 
		 * @return 
		 */
		public static function get active():Boolean {
			return _active;
		}


		/**
		 * <p> Tells Flash Player to apply any accessibility changes made by using the <code>DisplayObject.accessibilityProperties</code> property. You need to call this method for your changes to take effect. </p>
		 * <p>If you modify the accessibility properties for multiple objects, only one call to the <code>Accessibility.updateProperties()</code> method is necessary; multiple calls can result in reduced performance and erroneous screen reader output.</p>
		 */
		public static function updateProperties():void {
			throw new Error("Not implemented");
		}
	}
}
