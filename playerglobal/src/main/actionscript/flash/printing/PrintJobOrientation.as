package flash.printing {
	/**
	 *  This class provides values that are used by the <code>PrintJob.orientation</code> property for the image position of a printed page. <p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7cc6.html" target="_blank">Setting size, scale, and orientation</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7cb4.html" target="_blank">Printing for landscape or portrait orientation </a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="PrintJob.html#orientation" target="">PrintJob.orientation</a>
	 * </div><br><hr>
	 */
	public class PrintJobOrientation {
		/**
		 * <p> The landscape (horizontal) image orientation for printing. This constant is used with the <code>PrintJob.orientation</code> property. Use the syntax <code>PrintJobOrientation.LANDSCAPE</code>. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="PrintJob.html#orientation" target="">PrintJob.orientation</a>
		 *  <br>
		 *  <a href="PrintJobOrientation.html#PORTRAIT" target="">PORTRAIT</a>
		 * </div>
		 */
		public static const LANDSCAPE:String = "landscape";
		/**
		 * <p> The portrait (vertical) image orientation for printing. This constant is used with the <code>PrintJob.orientation</code> property. Use the syntax <code>PrintJobOrientation.PORTRAIT</code>. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="PrintJob.html#orientation" target="">PrintJob.orientation</a>
		 *  <br>
		 *  <a href="PrintJobOrientation.html#LANDSCAPE" target="">LANDSCAPE</a>
		 * </div>
		 */
		public static const PORTRAIT:String = "portrait";
	}
}
