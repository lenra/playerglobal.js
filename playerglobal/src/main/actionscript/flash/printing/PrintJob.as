package flash.printing {
	import flash.display.Sprite;
	import flash.events.EventDispatcher;
	import flash.geom.Rectangle;

	/**
	 *  The PrintJob class lets you create content and print it to one or more pages. This class lets you render content that is visible, dynamic or offscreen to the user, prompt users with a single Print dialog box, and print an unscaled document with proportions that map to the proportions of the content. This capability is especially useful for rendering and printing dynamic content, such as database content and dynamic text. <p> <b>Mobile Browser Support:</b> This class is not supported in mobile browsers.</p> <p> <i>AIR profile support:</i> This feature is supported on all desktop operating systems, but it is not supported on mobile devices or AIR for TV devices. You can test for support at run time using the <code>PrintJob.isSupported</code> property. See <a href="http://help.adobe.com/en_US/air/build/WS144092a96ffef7cc16ddeea2126bb46b82f-8000.html"> AIR Profile Support</a> for more information regarding API support across multiple profiles.</p> <p>Use the <code>PrintJob()</code> constructor to create a print job.</p> <p>Additionally, with the PrintJob class's properties, you can read your user's printer settings, such as page height, width, and image orientation, and you can configure your document to dynamically format Flash content that is appropriate for the printer settings.</p> <p> <b>Note:</b> ActionScript 3.0 does not restrict a PrintJob object to a single frame (as did previous versions of ActionScript). However, since the operating system displays print status information to the user after the user has clicked the OK button in the Print dialog box, you should call <code>PrintJob.addPage()</code> and <code>PrintJob.send()</code> as soon as possible to send pages to the spooler. A delay reaching the frame containing the <code>PrintJob.send()</code> call delays the printing process.</p> <p>Additionally, a 15 second script timeout limit applies to the following intervals:</p> <ul> 
	 *  <li> <code>PrintJob.start()</code> and the first <code>PrintJob.addPage()</code> </li> 
	 *  <li> <code>PrintJob.addPage()</code> and the next <code>PrintJob.addPage()</code> </li> 
	 *  <li>The last <code>PrintJob.addPage()</code> and <code>PrintJob.send()</code> </li> 
	 * </ul> <p>If any of the above intervals span more than 15 seconds, the next call to <code>PrintJob.start()</code> on the PrintJob instance returns <code>false</code>, and the next <code>PrintJob.addPage()</code> on the PrintJob instance causes the Flash Player or Adobe AIR to throw a runtime exception.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7cba.html" target="_blank">Basics of printing</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7cc7.html" target="_blank">Printing a page</a>
	 * </div><br><hr>
	 */
	public class PrintJob extends EventDispatcher {
		private static var _active:Boolean;
		private static var _isSupported:Boolean;
		private static var _printers:Vector.<String>;
		private static var _supportsPageSetupDialog:Boolean;

		private var _copies:int;
		private var _jobName:String;
		private var _orientation:String;
		private var _printer:String;

		private var _firstPage:int;
		private var _isColor:Boolean;
		private var _lastPage:int;
		private var _maxPixelsPerInch:Number;
		private var _pageHeight:int;
		private var _pageWidth:int;
		private var _paperArea:Rectangle;
		private var _paperHeight:int;
		private var _paperWidth:int;
		private var _printableArea:Rectangle;

		/**
		 * <p> Creates a PrintJob object that you can use to print one or more pages. After you create a PrintJob object, you need to use (in the following sequence) the <code>PrintJob.start()</code>, <code>PrintJob.addPage()</code>, and then <code>PrintJob.send()</code> methods to send the print job to the printer. </p>
		 * <p>For example, you can replace the <code>[params]</code> placeholder text for the <code>myPrintJob.addPage()</code> method calls with custom parameters as shown in the following code:</p>
		 * <pre>
		 *  // create PrintJob object
		 *  var myPrintJob:PrintJob = new PrintJob();
		 *   
		 *  // display Print dialog box, but only initiate the print job
		 *  // if start returns successfully.
		 *  if (myPrintJob.start()) {
		 *   
		 *     // add specified page to print job
		 *     // repeat once for each page to be printed
		 *     try {
		 *       myPrintJob.addPage([params]);
		 *     }
		 *     catch(e:Error) {
		 *       // handle error 
		 *     }
		 *     try {
		 *       myPrintJob.addPage([params]);
		 *     }
		 *     catch(e:Error) {
		 *       // handle error 
		 *     }
		 *  
		 *     // send pages from the spooler to the printer, but only if one or more
		 *     // calls to addPage() was successful. You should always check for successful 
		 *     // calls to start() and addPage() before calling send().
		 *     myPrintJob.send();
		 *  }
		 *  </pre>
		 * <p>In AIR 2 or later, you can create and use multiple PrintJob instances. Properties set on the PrintJob instance are retained after printing completes. This allows you to re-use a PrintJob instance and maintain a user's selected printing preferences, while offering different printing preferences for other content in your application. For content in Flash Player and in AIR prior to version 2, you cannot create a second PrintJob object while the first one is still active. If you create a second PrintJob object (by calling <code>new&nbsp;PrintJob()</code>) while the first PrintJob object is still active, the second PrintJob object will not be created. So, you may check for the <code>myPrintJob</code> value before creating a second PrintJob.</p>
		 */
		public function PrintJob() {
			super();
			throw new Error("Not implemented");
		}

		/**
		 * <p> The number of copies that the print system prints of any pages subsequently added to the print job. This value is the number of copies entered by the user in the operating system's Print dialog. If the the number of copies was not displayed in the Print dialog, or the dialog was not presented to the user, the value is 1 (unless it has been changed by application code). </p>
		 * 
		 * @return 
		 */
		public function get copies():int {
			return _copies;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set copies(value:int):void {
			_copies = value;
		}

		/**
		 * <p> The page number of the first page of the range entered by the user in the operating system's Print dialog. This property is zero if the user requests that all pages be printed, or if the page range was not displayed in the Print dialog, or if the Print dialog has not been presented to the user. </p>
		 * 
		 * @return 
		 */
		public function get firstPage():int {
			return _firstPage;
		}

		/**
		 * <p> Indicates whether the currently selected printer at the current print settings prints using color (<code>true</code>) or grayscale (<code>false</code>). </p>
		 * <p>If a color-or-grayscale value can't be determined, the value is <code>true</code>.</p>
		 * 
		 * @return 
		 */
		public function get isColor():Boolean {
			return _isColor;
		}

		/**
		 * <p> The name or title of the print job. The job name is typically used by the operating system as the title of the job in the print queue, or as the default name of a job that is printed to a file. </p>
		 * <p>If you have not called <code>start()</code> or <code>start2()</code> and you haven't set a value for the property, this property's value is <code>null</code>.</p>
		 * <p>For each print job you execute with a PrintJob instance, set this property before calling the <code>start()</code> or <code>start2()</code> method.</p>
		 * <p> The default value is <code><code>null</code>.</code></p>
		 * 
		 * @return 
		 */
		public function get jobName():String {
			return _jobName;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set jobName(value:String):void {
			_jobName = value;
		}

		/**
		 * <p> The page number of the last page of the range entered by the user in the operating system's Print dialog. This property is zero if the user requests that all pages be printed, or if the page range was not displayed in the Print dialog, or if the Print dialog has not been presented to the user. </p>
		 * 
		 * @return 
		 */
		public function get lastPage():int {
			return _lastPage;
		}

		/**
		 * <p> The physical resolution of the selected printer, in pixels per inch. The value is calculated according to the current print settings as reported by the operating system. </p>
		 * <p>If the resolution cannot be determined, the value is a standard default value. The default value is 600 ppi on Linux and 360 ppi on Mac OS. On Windows, the printer resolution is always available, so no default value is necessary.</p>
		 * 
		 * @return 
		 */
		public function get maxPixelsPerInch():Number {
			return _maxPixelsPerInch;
		}

		/**
		 * <p> The image orientation for printing. The acceptable values are defined as constants in the PrintJobOrientation class. </p>
		 * <p><b>Note:</b> For AIR 2 or later, set this property before starting a print job to set the default orientation in the Page Setup and Print dialogs. Set the property while a print job is in progress (after calling <code>start()</code> or <code>start2()</code> to set the orientation for a range of pages within the job.</p>
		 * 
		 * @return 
		 */
		public function get orientation():String {
			return _orientation;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set orientation(value:String):void {
			_orientation = value;
		}

		/**
		 * <p> The height of the largest area which can be centered in the actual printable area on the page, in points. Any user-set margins are ignored. This property is available only after a call to the <code>PrintJob.start()</code> method has been made. </p>
		 * <p><b>Note:</b> For AIR 2 or later, this property is deprecated. Use <code>printableArea</code> instead, which measures the printable area in fractional points and describes off-center printable areas accurately.</p>
		 * 
		 * @return 
		 */
		public function get pageHeight():int {
			return _pageHeight;
		}

		/**
		 * <p> The width of the largest area which can be centered in the actual printable area on the page, in points. Any user-set margins are ignored. This property is available only after a call to the <code>PrintJob.start()</code> method has been made. </p>
		 * <p><b>Note:</b> For AIR 2 or later, this property is deprecated. Use <code>printableArea</code> instead, which measures the printable area in fractional points and describes off-center printable areas accurately.</p>
		 * 
		 * @return 
		 */
		public function get pageWidth():int {
			return _pageWidth;
		}

		/**
		 * <p> The bounds of the printer media in points. This value uses the same coordinate system that is used for subsequent <code>addPage()</code> calls. </p>
		 * 
		 * @return 
		 */
		public function get paperArea():Rectangle {
			return _paperArea;
		}

		/**
		 * <p> The overall paper height, in points. This property is available only after a call to the <code>PrintJob.start()</code> method has been made. </p>
		 * <p><b>Note:</b> For AIR 2 or later, this property is deprecated. Use <code>paperArea</code> instead, which measures the paper dimensions in fractional points.</p>
		 * 
		 * @return 
		 */
		public function get paperHeight():int {
			return _paperHeight;
		}

		/**
		 * <p> The overall paper width, in points. This property is available only after a call to the <code>PrintJob.start()</code> method has been made. </p>
		 * <p><b>Note:</b> For AIR 2 or later, this property is deprecated. Use <code>paperArea</code> instead, which measures the paper dimensions in fractional points.</p>
		 * 
		 * @return 
		 */
		public function get paperWidth():int {
			return _paperWidth;
		}

		/**
		 * <p> The bounds of the printer media's printable area in points. This value uses the same coordinate system that is used for subsequent <code>addPage()</code> calls. </p>
		 * 
		 * @return 
		 */
		public function get printableArea():Rectangle {
			return _printableArea;
		}

		/**
		 * <p> Gets or sets the printer to use for the current print job. The String passed to the setter and returned by the getter should match one of the strings in the Array returned by the <code>printers()</code> method. To indicate that the default printer should be used, set the value to <code>null</code>. On operating systems where the default printer cannot be determined, this property's value is <code>null</code>. </p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *      import flash.printing.PrintJob;
		 *      
		 *      var myPrintJob:PrintJob = new PrintJob();
		 *      myPrintJob.printer = "HP_LaserJet_1";
		 *      myPrintJob.start();
		 *      </pre>
		 * </div>
		 * <p>Setting the value of this property attempts to select the printer immediately. If the printer selection fails, this property's value resets to the previous value. You can determine if setting the printer value succeeds by reading the value after attempting to set it, and confirming that it matches the value that was set.</p>
		 * <p>The <code>printer</code> property of an active print job cannot be changed. Attempting to change it after calling the <code>start()</code> or <code>start2()</code> method successfully and before calling <code>send()</code> or <code>terminate()</code> fails.</p>
		 * 
		 * @return 
		 */
		public function get printer():String {
			return _printer;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set printer(value:String):void {
			_printer = value;
		}

		/**
		 * <p> Sends the specified Sprite object as a single page to the print spooler. Before using this method, you must create a PrintJob object and then use <code>start()</code> or <code>start2()</code>. Then, after calling <code>addPage()</code> one or more times for a print job, use <code>send()</code> to send the spooled pages to the printer. In other words, after you create a PrintJob object, use (in the following sequence) <code>start()</code> or <code>start2()</code>, <code>addPage()</code>, and then <code>send()</code> to send the print job to the printer. You can call <code>addPage()</code> multiple times after a single call to <code>start()</code> to print several pages in a print job. </p>
		 * <p>If <code>addPage()</code> causes Flash Player to throw an exception (for example, if you haven't called <code>start()</code> or the user cancels the print job), any subsequent calls to <code>addPage()</code> fail. However, if previous calls to <code>addPage()</code> are successful, the concluding <code>send()</code> command sends the successfully spooled pages to the printer.</p>
		 * <p>If the print job takes more than 15 seconds to complete an <code>addPage()</code> operation, Flash Player throws an exception on the next <code>addPage()</code> call.</p>
		 * <p>If you pass a value for the <code>printArea</code> parameter, the <code><i>x</i></code> and <code><i>y</i></code> coordinates of the <code>printArea</code> Rectangle map to the upper-left corner (0, 0 coordinates) of the printable area on the page. The read-only properties <code>pageHeight</code> and <code>pageWidth</code> describe the printable area set by <code>start()</code>. Because the printout aligns with the upper-left corner of the printable area on the page, when the area defined in <code>printArea</code> is bigger than the printable area on the page, the printout is cropped to the right or bottom (or both) of the area defined by <code>printArea</code>. In Flash Professional, if you don't pass a value for <code>printArea</code> and the Stage is larger than the printable area, the same type of clipping occurs. In Flex or Flash Builder, if you don't pass a value for <code>printArea</code> and the screen is larger than the printable area, the same type of clipping takes place.</p>
		 * <p>If you want to scale a Sprite object before you print it, set scale properties (see <code>flash.display.DisplayObject.scaleX</code> and <code>flash.display.DisplayObject.scaleY</code>) before calling this method, and set them back to their original values after printing. The scale of a Sprite object has no relation to <code>printArea</code>. That is, if you specify a print area that is 50 x 50 pixels, 2500 pixels are printed. If you scale the Sprite object, the same 2500 pixels are printed, but the Sprite object is printed at the scaled size.</p>
		 * <p>The Flash Player printing feature supports PostScript and non-PostScript printers. Non-PostScript printers convert vectors to bitmaps.</p>
		 * 
		 * @param sprite  — The Sprite containing the content to print. 
		 * @param printArea  — A Rectangle object that specifies the area to print. <p>A rectangle's width and height are pixel values. A printer uses points as print units of measurement. Points are a fixed physical size (1/72 inch), but the size of a pixel, onscreen, depends on the resolution of the particular screen. So, the conversion rate between pixels and points depends on the printer settings and whether the sprite is scaled. An unscaled sprite that is 72 pixels wide prints out one inch wide, with one point equal to one pixel, independent of screen resolution.</p> <p>You can use the following equivalencies to convert inches or centimeters to twips or points (a twip is 1/20 of a point):</p> <ul>
		 *  <li>1 point = 1/72 inch = 20 twips</li>
		 *  <li>1 inch = 72 points = 1440 twips</li>
		 *  <li>1 cm = 567 twips</li>
		 * </ul> <p>If you omit the <code>printArea</code> parameter, or if it is passed incorrectly, the full area of the <code>sprite</code> parameter is printed.</p> <p>If you don't want to specify a value for <code>printArea</code> but want to specify a value for <code>options</code> or <code>frameNum</code>, pass <code>null</code> for <code>printArea</code>.</p> 
		 * @param options  — An optional parameter that specifies whether to print as vector or bitmap. The default value is <code>null</code>, which represents a request for vector printing. To print <code>sprite</code> as a bitmap, set the <code>printAsBitmap</code> property of the PrintJobOptions object to <code>true</code>. Remember the following suggestions when determining whether to set <code>printAsBitmap</code> to <code>true</code>: <ul>
		 *  <li>If the content you're printing includes a bitmap image, set <code>printAsBitmap</code> to <code>true</code> to include any alpha transparency and color effects.</li>
		 *  <li>If the content does not include bitmap images, omit this parameter to print the content in higher quality vector format.</li>
		 * </ul> <p>If <code>options</code> is omitted or is passed incorrectly, vector printing is used. If you don't want to specify a value for <code>options</code> but want to specify a value for <code>frameNumber</code>, pass <code>null</code> for <code>options</code>.</p> 
		 * @param frameNum  — An optional number that lets you specify which frame of a MovieClip object to print. Passing a <code>frameNum</code> does not invoke ActionScript on that frame. If you omit this parameter and the <code>sprite</code> parameter is a MovieClip object, the current frame in <code>sprite</code> is printed. 
		 */
		public function addPage(sprite:Sprite, printArea:Rectangle = null, options:PrintJobOptions = null, frameNum:int = 0):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Set the paper size. The acceptable values for the <code>paperSize</code> parameter are constants in the PaperSize class. Calling this method affects print settings as if the user chooses a paper size in the Page Setup or Print dialogs. </p>
		 * <p>You can call this method at any time. Call this method before starting a print job to set the default paper size in the Page Setup and Print dialogs. Call this method while a print job is in progress to set the paper size for a range of pages within the job.</p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *      import flash.printing.PrintJob;
		 *      import flash.printing.PaperSize;
		 *      
		 *      var myPrintJob:PrintJob = new PrintJob();
		 *      myPrintJob.selectPaperSize(PaperSize.ENV_10);
		 *      </pre>
		 * </div>
		 * 
		 * @param paperSize  — The paper size to use for subsequent pages in the print job 
		 */
		public function selectPaperSize(paperSize:String):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sends spooled pages to the printer after successful calls to the <code>start()</code> or <code>start2()</code> and <code>addPage()</code> methods. </p>
		 * <p>This method does not succeed if the call to the <code>start()</code> or <code>start2()</code> method fails, or if a call to the <code>addPage()</code> method throws an exception. To avoid an error, check that the <code>start()</code> or <code>start2()</code> method returns <code>true</code> and catch any <code>addPage()</code> exceptions before calling this method. The following example demonstrates how to properly check for errors before calling this method:</p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *      var myPrintJob:PrintJob = new PrintJob();
		 *      if (myPrintJob.start()) {
		 *        try {
		 *          myPrintJob.addPage([params]);
		 *        }
		 *        catch(e:Error) {
		 *           // handle error 
		 *        }
		 *      
		 *        myPrintJob.send();
		 *      }
		 *      </pre>
		 * </div>
		 * <p id="moreExamples"><span class="label">More examples</span></p>
		 * <div class="seeAlso">
		 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7cc7.html" target="_blank">Printing a page</a>
		 * </div>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="PrintJob.html#addPage()" target="">PrintJob.addPage()</a>
		 *  <br>
		 *  <a href="PrintJob.html#start()" target="">PrintJob.start()</a>
		 *  <br>
		 *  <a href="PrintJob.html#start2()" target="">PrintJob.start2()</a>
		 * </div>
		 */
		public function send():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Displays the operating system's Page Setup dialog if the current environment supports it. Use the <code>supportsPageSetupDialog</code> property to determine if Page Setup is supported. </p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *      import flash.printing.PrintJob;
		 *      
		 *      var myPrintJob:PrintJob = new PrintJob();
		 *      if (myPrintJob.supportsPageSetupDialog)
		 *      {
		 *          myPrintJob.showPageSetupDialog();
		 *      }
		 *      </pre>
		 * </div>
		 * 
		 * @return  —  if the user chooses "OK" in the Page Setup dialog. This indicates that some PrintJob properties may have changed. Returns  if the user chooses "Cancel" in the Page Setup dialog. 
		 */
		public function showPageSetupDialog():Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Displays the operating system's Print dialog box and starts spooling. The Print dialog box lets the user change print settings. When the <code>PrintJob.start()</code> method returns successfully (the user clicks OK in the Print dialog box), the following properties are populated, representing the user's chosen print settings: </p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Property</th>
		 *    <th>Type</th>
		 *    <th>Units</th>
		 *    <th>Notes</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>PrintJob.paperHeight</code></td>
		 *    <td>Number</td>
		 *    <td>Points</td>
		 *    <td>Overall paper height.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>PrintJob.paperWidth</code></td>
		 *    <td>Number</td>
		 *    <td>Points</td>
		 *    <td>Overall paper width.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>PrintJob.pageHeight</code></td>
		 *    <td>Number</td>
		 *    <td>Points</td>
		 *    <td>Height of actual printable area on the page; any user-set margins are ignored.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>PrintJob.pageWidth</code></td>
		 *    <td>Number</td>
		 *    <td>Points</td>
		 *    <td>Width of actual printable area on the page; any user-set margins are ignored.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>PrintJob.orientation</code></td>
		 *    <td>String</td>
		 *    <td></td>
		 *    <td><code>"portrait"</code> (<code>flash.printing.PrintJobOrientation.PORTRAIT</code>) or <code>"landscape"</code> (<code>flash.printing.PrintJobOrientation.LANDSCAPE</code>).</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><b>Note:</b> If the user cancels the Print dialog box, the properties are not populated.</p>
		 * <p>After the user clicks OK in the Print dialog box, the player begins spooling a print job to the operating system. Because the operating system then begins displaying information to the user about the printing progress, you should call the <code>PrintJob.addPage()</code> and <code>PrintJob.send()</code> calls as soon as possible to send pages to the spooler. You can use the read-only height, width, and orientation properties this method populates to format the printout.</p>
		 * <p>Test to see if this method returns <code>true</code> (when the user clicks OK in the operating system's Print dialog box) before any subsequent calls to <code>PrintJob.addPage()</code> and <code>PrintJob.send()</code>:</p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *      var myPrintJob:PrintJob = new PrintJob();
		 *         if(myPrintJob.start()) {
		 *           // addPage() and send() statements here
		 *         }
		 *      </pre>
		 * </div>
		 * <p>For the given print job instance, if any of the following intervals last more than 15 seconds the next call to <code>PrintJob.start()</code> will return <code>false</code>:</p>
		 * <ul>
		 *  <li><code>PrintJob.start()</code> and the first <code>PrintJob.addPage()</code></li>
		 *  <li>One <code>PrintJob.addPage()</code> and the next <code>PrintJob.addPage()</code></li>
		 *  <li>The last <code>PrintJob.addPage()</code> and <code>PrintJob.send()</code></li>
		 * </ul>
		 * 
		 * @return  — A value of  if the user clicks OK when the Print dialog box appears;  if the user clicks Cancel or if an error occurs. 
		 */
		public function start():Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Optionally displays the operating system's Print dialog box, starts spooling, and possibly modifies the PrintJob read-only property values. </p>
		 * <p>The <code>uiOptions</code> parameter allows the caller to control which options are displayed in the Print dialog. See the <code>PrintUIOptions</code> class. This parameter is ignored if <code>showPrintDialog</code> is false.</p>
		 * <p>Even when <code>showPrintDialog</code> is <code>true</code>, this method's behavior can differ from the <code>start()</code> method. On some operating systems, <code>start()</code> shows the Page Setup dialog followed by the Print dialog. In contrast, <code>start2()</code> never shows the Page Setup dialog.</p>
		 * <p>In the following example, the min and max page settings in the Print dialog are set before the dialog is displayed to the user:</p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *      import flash.printing.PrintJob;
		 *      import flash.printing.PrintUIOptions;
		 *      
		 *      var myPrintJob:PrintJob = new PrintJob();
		 *      var uiOpt:PrintUIOptions = new PrintUIOptions();
		 *      uiOpt.minPage = 1;
		 *      uiOpt.maxPage = 3;
		 *      var accepted:Boolean = myPrintJob.start2(uiOpt);
		 *      </pre>
		 * </div>
		 * 
		 * @param uiOptions  — An object designating which options are displayed in the Print dialog that is shown to the user. If the <code>showPrintDialog</code> parameter is <code>false</code>, this value is ignored. 
		 * @param showPrintDialog  — Whether or not the Print dialog is shown to the user before starting the print job 
		 * @return  — A value of  if the user clicks OK when the Print dialog box appears, or if the Print dialog is not shown and there is no error;  if the user clicks Cancel or if an error occurs. 
		 */
		/*public function start2(uiOptions:PrintUIOptions = null, showPrintDialog:Boolean = true):Boolean {
			throw new Error("Not implemented");
		}*/

		/**
		 * <p> Signals that the print job should be terminated without sending. Use this method when a print job has already been initiated by a call to <code>start()</code> or <code>start2()</code>, but when it is not appropriate to send any pages to the printer. Typically, <code>terminate()</code> is only used to recover from errors. </p>
		 * <p>After calling this method, the PrintJob instance can be reused. Wherever possible, the job's print settings are retained for subsequent use.</p>
		 */
		public function terminate():void {
			throw new Error("Not implemented");
		}


		/**
		 * <p> Indicates whether a print job is currently active. A print job is active (the property value is <code>true</code>) in either of two conditions: </p>
		 * <ul>
		 *  <li>A Page Setup or Print dialog is being displayed.</li>
		 *  <li>The <code>start()</code> or <code>start2()</code> method has been called with a <code>true</code> return value, and the <code>send()</code> or <code>terminate()</code> method has not been called.</li>
		 * </ul>
		 * <p>If this property is <code>true</code> and you call the <code>showPageSetupDialog()</code>, <code>start()</code>, or <code>start2()</code> method, the runtime throws an exception.</p>
		 * 
		 * @return 
		 */
		public static function get active():Boolean {
			return _active;
		}


		/**
		 * <p> Indicates whether the PrintJob class is supported on the current platform (<code>true</code>) or not (<code>false</code>). </p>
		 * 
		 * @return 
		 */
		public static function get isSupported():Boolean {
			return _isSupported;
		}


		/**
		 * <p> Provides a list of the available printers as String name values. The list is not precalculated; it is generated when the function is called. If no printers are available or if the system does not support printing, the value is <code>null</code>. If the system supports printing but is not capable of returning a list of printers, the value is a Vector with a single element (its <code>length</code> property is 1). In that case, the single element is the actual printer name or a default name if the current printer name cannot be determined. </p>
		 * 
		 * @return 
		 */
		public static function get printers():Vector.<String> {
			return _printers;
		}


		/**
		 * <p> Indicates whether the Flash runtime environment supports a separate Page Setup dialog. If this property is <code>true</code>, you can call the <code>showPageSetupDialog()</code> method to display the operating system's page setup dialog box. </p>
		 * 
		 * @return 
		 */
		public static function get supportsPageSetupDialog():Boolean {
			return _supportsPageSetupDialog;
		}
	}
}
