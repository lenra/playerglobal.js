package flash.desktop {
	/**
	 *  The ClipboardFormats class defines constants for the names of the standard data formats used with the Clipboard class. Flash Player 10 only supports TEXT_FORMAT, RICH_TEXT_FORMAT, and HTML_FORMAT. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Clipboard.html" target="">flash.desktop.Clipboard</a>
	 * </div><br><hr>
	 */
	public class ClipboardFormats {
		/**
		 * <p> Image data (AIR only). </p>
		 */
		public static const BITMAP_FORMAT:String = "air:bitmap";
		/**
		 * <p> An array of files (AIR only). </p>
		 */
		public static const FILE_LIST_FORMAT:String = "air:file list";
		/**
		 * <p> File promise list (AIR only). </p>
		 */
		public static const FILE_PROMISE_LIST_FORMAT:String = "air:file promise list";
		/**
		 * <p> HTML data. </p>
		 */
		public static const HTML_FORMAT:String = "air:html";
		/**
		 * <p> Rich Text Format data. </p>
		 */
		public static const RICH_TEXT_FORMAT:String = "air:rtf";
		/**
		 * <p> String data. </p>
		 */
		public static const TEXT_FORMAT:String = "air:text";
		/**
		 * <p> A URL string (AIR only). </p>
		 */
		public static const URL_FORMAT:String = "air:url";
	}
}
