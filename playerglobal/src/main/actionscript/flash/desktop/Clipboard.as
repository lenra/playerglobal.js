package flash.desktop {
	/**
	 *  The Clipboard class provides a container for transferring data and objects through the clipboard. The operating system clipboard can be accessed through the static <code>generalClipboard</code> property. <p>A Clipboard object can contain the same information in more than one format. By supplying information in multiple formats, you increase the chances that another application will be able to use that information. Add data to a Clipboard object with the <code>setData()</code> or <code>setDataHandler()</code> method.</p> <p>The standard formats are:</p> <ul> 
	 *  <li>BITMAP_FORMAT: a BitmapData object (AIR only)</li> 
	 *  <li>FILE_LIST_FORMAT: an array of File objects (AIR only)</li> 
	 *  <li>HTML_FORMAT: HTML-formatted string data</li> 
	 *  <li>TEXT_FORMAT: string data</li> 
	 *  <li>RICH_TEXT_FORMAT: a ByteArray containing Rich Text Format data</li> 
	 *  <li>URL_FORMAT: a URL string (AIR only)</li> 
	 * </ul> <p>These constants for the names of the standard formats are defined in the ClipboardFormats class.</p> <p>When a transfer to or from the operating system occurs, the standard formats are automatically translated between <span>ActionScript</span> data types and the native operating system clipboard types.</p> <p>You can use application-defined formats to add <span>ActionScript</span> objects to a Clipboard object. If an object is serializable, both a reference and a clone of the object can be made available. Object references are valid only within the originating application.</p> <p>When it is computationally expensive to convert the information to be transferred into a particular format, you can supply the name of a function that performs the conversion. The function is called if and only if that format is read by the receiving component or application. Add a deferred rendering function to a Clipboard object with the <code>setDataHandler()</code> method. Note that in some cases, the operating system calls the function before a drop occurs. For example, when you use a handler function to provide the data for a file dragged from an AIR application to the file system, the operating system calls the data handler function as soon as the drag gesture leaves the AIR application—typically resulting in an undesireable pause as the file data is downloaded or created.</p> <p> <b>Note for AIR applications:</b> The clipboard object referenced by the event objects dispatched for HTML drag-and-drop and copy-and-paste events are not the same type as the AIR Clipboard object. The JavaScript clipboard object is described in the AIR developer's guide.</p> <p> <b>Note for Flash Player applications:</b> In Flash Player 10, a paste operation from the clipboard first requires a user event (such as a keyboard shortcut for the Paste command or a mouse click on the Paste command in a context menu). <code>Clipboard.getData()</code> will return the contents of the clipboard only if the InteractiveObject has received and is acting on a paste event. Calling <code>Clipboard.getData()</code> under any other circumstances will be unsuccessful. The same restriction applies in AIR for content outside the application sandbox.</p> <p>On Linux, clipboard data does not persist when an AIR application closes.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS88A6374C-1D6C-4faa-968C-5354C78587E2.html" target="_blank">Copy and paste</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118666ade46-7e8a.html" target="_blank">Drag and drop in AIR</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="NativeDragManager.html" target="">flash.desktop.NativeDragManager</a>
	 *  <br>
	 *  <a href="ClipboardFormats.html" target="">flash.desktop.ClipboardFormats</a>
	 *  <br>
	 *  <a href="ClipboardTransferMode.html" target="">flash.desktop.ClipboardTransferMode</a>
	 * </div><br><hr>
	 */
	public class Clipboard {
		private static var _generalClipboard:Clipboard;

		private var _formats:Array;
		private var _supportsFilePromise:Boolean;

		/**
		 * <p> Creates an empty Clipboard object. </p>
		 * <p>Create Clipboard objects to hold the data of a native drag-and-drop gesture in Adobe AIR. Clipboard objects can be used for only one drag-and-drop gesture; they cannot be reused.</p>
		 * <p>Do not create a Clipboard object for copy-and-paste operations. Use the single <code>Clipboard.generalClipboard</code> object instead.</p>
		 */
		public function Clipboard() {
			throw new Error("Not implemented");
		}

		/**
		 * <p> An array of strings containing the names of the data formats available in this Clipboard object. </p>
		 * <p>String constants for the names of the standard formats are defined in the ClipboardFormats class. Other, application-defined, strings may also be used as format names to transfer data as an object.</p>
		 * 
		 * @return 
		 */
		public function get formats():Array {
			return _formats;
		}

		/**
		 * <p> Indicates whether the file promise clipboard format is supported on the client system. </p>
		 * 
		 * @return 
		 */
		public function get supportsFilePromise():Boolean {
			return _supportsFilePromise;
		}

		/**
		 * <p> Deletes all data representations from this Clipboard object. </p>
		 */
		public function clear():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Deletes the data representation for the specified format. </p>
		 * 
		 * @param format  — The data format to remove. 
		 */
		public function clearData(format:String):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Gets the clipboard data if data in the specified format is present. </p>
		 * <p>Flash Player requires that the <code>getData()</code> be called in a <code>paste</code> event handler. In AIR, this restriction only applies to content outside of the application security sandbox.</p>
		 * <p>When a standard data format is accessed, the data is returned as a new object of the corresponding Flash data type.</p>
		 * <p>When an application-defined format is accessed, the value of the <code>transferMode</code> parameter determines whether a reference to the original object or an anonymous object containing a serialized copy of the original object is returned. When an <code>originalPreferred</code> or <code>clonePreferred</code> mode is specified, Flash Player or AIR returns the alternate version if the preferred version is not available. When an <code>originalOnly</code> or <code>cloneOnly</code> mode is specified, Flash Player or AIR returns <code>null</code> if the requested version is not available.</p>
		 * 
		 * @param format  — The data format to return. The format string can contain one of the standard names defined in the ClipboardFormats class, or an application-defined name. 
		 * @param transferMode  — Specifies whether to return a reference or serialized copy when an application-defined data format is accessed. The value must be one of the names defined in the ClipboardTransferMode class. This value is ignored for the standard data formats; a copy is always returned. 
		 * @return  — An object of the type corresponding to the data format. 
		 */
		public function getData(format:String, transferMode:String = "originalPreferred"):Object {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Checks whether data in the specified format exists in this Clipboard object. </p>
		 * <p>Use the constants in the ClipboardFormats class to reference the standard format names.</p>
		 * 
		 * @param format  — The format type to check. 
		 * @return  — , if data in the specified format is present. 
		 */
		public function hasFormat(format:String):Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Adds a representation of the information to be transferred in the specified data format. </p>
		 * <p>In the application sandbox of Adobe AIR, <code>setData()</code> can be called anytime. In other contexts, <code>setData()</code> can only be called in response to a user-generated event such as a key press or mouse click.</p>
		 * <p>Different representations of the same information can be added to the clipboard as different formats, which increases the ability of other components or applications to make use of the available data. For example, an image could be added as bitmap data for use by image editing applications, as a URL, and as an encoded PNG file for transfer to the native file system.</p>
		 * <p>The data parameter must be the appropriate data type for the specified format:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Format</th>
		 *    <th>Type</th>
		 *    <th>Description</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>ClipboardFormats.TEXT_FORMAT</code></td>
		 *    <td><code>String</code></td>
		 *    <td>string data</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>ClipboardFormats.HTML_FORMAT</code></td>
		 *    <td><code>String</code></td>
		 *    <td>HTML string data</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>ClipboardFormats.URL_FORMAT</code></td>
		 *    <td><code>String</code></td>
		 *    <td>URL string (AIR only)</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>ClipboardFormats.RICH_TEXT_FORMAT</code></td>
		 *    <td><code>ByteArray</code></td>
		 *    <td>Rich Text Format data</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>ClipboardFormats.BITMAP_FORMAT</code></td>
		 *    <td><code>BitmapData</code></td>
		 *    <td>bitmap data (AIR only)</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>ClipboardFormats.FILE_LIST_FORMAT</code></td>
		 *    <td>array of <code>File</code></td>
		 *    <td>an array of files (AIR only)</td>
		 *   </tr>
		 *   <tr>
		 *    <td>Custom format name</td>
		 *    <td>any</td>
		 *    <td>object reference and serialized clone</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p>Custom format names cannot begin with "air:" or "flash:". To prevent name collisions when using custom formats, you may want to use your application ID or a package name as a prefix to the format, such as "com.example.applicationName.dataPacket".</p>
		 * <p>When transferring within or between applications, the <code>serializable</code> parameter determines whether both a reference and a copy are available, or whether only a reference to an object is available. Set <code>serializable</code> to <code>true</code> to make both the reference and a copy of the data object available. Set <code>serializable</code> to <code>false</code> to make only the object reference available. Object references are valid only within the current application so setting <code>serializable</code> to <code>false</code> also means that the data in that format is not available to other Flash Player or AIR applications. A component can choose to get the reference or the copy of the object by setting the appropriate clipboard transfer mode when accessing the data for that format.</p>
		 * <p><b>Note:</b> The standard formats are always converted to native formats when data is pasted or dragged outside a supported application, so the value of the <code>serializable</code> parameter does not affect the availability of data in the standard formats to non-Flash-based applications.</p>
		 * <p>To defer rendering of the data for a format, use the <code>setDataHandler()</code> method instead. If both the <code>setData()</code> and the <code>setDataHandler()</code> methods are used to add a data representation with the same format name, then the handler function will never be called.</p>
		 * <p><b>Note:</b> On Mac OS, when you set the <code>format</code> parameter to <code>ClipboardFormats.URL_FORMAT</code>, the URL is transferred only if it is a valid URL. Otherwise, the Clipboard object is emptied (and calling <code>getData()</code> returns <code>null</code>).</p>
		 * 
		 * @param format  — The format of the data. 
		 * @param data  — The information to add. 
		 * @param serializable  — Specify <code>true</code> for objects that can be serialized (and deserialized). 
		 * @return  —  if the data was succesfully set;  otherwise. In Flash Player, returns  when  is an unsupported member of ClipboardFormats. (Flash Player does not support , , , or ). 
		 */
		public function setData(format:String, data:Object, serializable:Boolean = true):Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Adds a reference to a handler function that produces the data to be transfered. </p>
		 * <p>Use a handler function to defer creation or rendering of the data until it is actually accessed.</p>
		 * <p>The handler function must return the appropriate data type for the specified format:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Format</th>
		 *    <th>Return Type</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>ClipboardFormats.TEXT_FORMAT</code></td>
		 *    <td><code>String</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>ClipboardFormats.HTML_FORMAT</code></td>
		 *    <td><code>String</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>ClipboardFormats.URL_FORMAT</code></td>
		 *    <td><code>String</code> (AIR only)</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>ClipboardFormats.RICH_TEXT_FORMAT</code></td>
		 *    <td><code>ByteArray</code></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>ClipboardFormats.BITMAP_FORMAT</code></td>
		 *    <td><code>BitmapData</code> (AIR only)</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>ClipboardFormats.FILE_LIST_FORMAT</code></td>
		 *    <td>Array of <code>File</code> (AIR only)</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>ClipboardFormats.FILE_PROMISE_LIST_FORMAT</code></td>
		 *    <td>Array of <code>File</code> (AIR only)</td>
		 *   </tr>
		 *   <tr>
		 *    <td>Custom format name</td>
		 *    <td>Non-void</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p>The handler function is called when and only when the data in the specified format is read. Note that in some cases, the operating system calls the function before a drop occurs. For example, when you use a handler function to provide the data for a file dragged from an AIR application to the file system, the operating system calls the data handler function as soon as the drag gesture leaves the AIR application—typically resulting in an undesireable pause as the file data is downloaded or created. You can use a URLFilePromise for this purpose instead.</p>
		 * <p>Note that the underlying data can change between the time the handler is added and the time the data is read unless your application takes steps to protect the data. The behavior that occurs when data on the clipboard represented by a handler function is read more than once is not guaranteed. The clipboard might return the data produced by the first function call or it might call the function again. Do not rely on either behavior.</p>
		 * <p>In the application sandbox of Adobe AIR, <code>setDataHandler()</code> can be called anytime. In other contexts, <code>setDataHandler()</code> can only be called in response to a user-generated event such as a key press or mouse click.</p>
		 * <p>To add data directly to this Clipboard object, use the <code>setData()</code> method instead. If both the <code>setData()</code> and the <code>setDataHandler()</code> methods are called with the same format name, then the handler function is never called.</p>
		 * <p><b>Note:</b> On Mac OS, when you set the <code>format</code> parameter to <code>ClipboardFormats.URL_FORMAT</code>, the URL is transferred only if the handler function returns a valid URL. Otherwise, the Clipboard object is emptied (and calling <code>getData()</code> returns <code>null</code>).</p>
		 * 
		 * @param format  — A function that returns the data to be transferred. 
		 * @param handler  — The format of the data. 
		 * @param serializable  — Specify <code>true</code> if the object returned by <code>handler</code> can be serialized (and deserialized). 
		 * @return  —  if the handler was succesfully set;  otherwise. 
		 */
		public function setDataHandler(format:String, handler:Function, serializable:Boolean = true):Boolean {
			throw new Error("Not implemented");
		}


		/**
		 * <p> The operating system clipboard. </p>
		 * <p>Any data pasted to the system clipboard is available to other applications. This may include insecure remote code running in a web browser.</p>
		 * <p><b>Note:</b> In Flash Player 10 applications, a paste operation from the clipboard first requires a user event (such as a keyboard shortcut for the Paste command or a mouse click on the Paste command in a context menu). <code>Clipboard.getData()</code> will return the contents of the clipboard only if the InteractiveObject has received and is acting on a paste event. Calling <code>Clipboard.getData()</code> under any other circumstances will be unsuccessful. The same restriction applies in AIR for content outside the application sandbox.</p>
		 * <p>The <code>generalClipboard</code> object is created automatically. You cannot assign another instance of a Clipboard to this property. Instead, you use the <code>getData()</code> and <code>setData()</code> methods to read and write data to the existing object.</p>
		 * <p>You should always clear the clipboard before writing new data to it to ensure that old data in all formats is erased.</p>
		 * <p>The <code>generalClipboard</code> object cannot be passed to the AIR NativeDragManager. Create a new Clipboard object for native drag-and-drop operations in an AIR application.</p>
		 * 
		 * @return 
		 */
		public static function get generalClipboard():Clipboard {
			return _generalClipboard;
		}
	}
}
