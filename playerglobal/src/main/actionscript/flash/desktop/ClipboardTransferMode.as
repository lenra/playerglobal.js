package flash.desktop {
	/**
	 *  The ClipboardTransferMode class defines constants for the modes used as values of the <code>transferMode</code> parameter of the <code>Clipboard.getData()</code> method. <p>The transfer mode provides a hint about whether to return a reference or a copy when accessing an object contained on a clipboard.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Clipboard.html#getData()" target="">flash.desktop.Clipboard.getData()</a>
	 * </div><br><hr>
	 */
	public class ClipboardTransferMode {
		/**
		 * <p> The Clipboard object should only return a copy. </p>
		 */
		public static const CLONE_ONLY:String = "cloneOnly";
		/**
		 * <p> The Clipboard object should return a copy if available and a reference if not. </p>
		 */
		public static const CLONE_PREFERRED:String = "clonePreferred";
		/**
		 * <p> The Clipboard object should only return a reference. </p>
		 */
		public static const ORIGINAL_ONLY:String = "originalOnly";
		/**
		 * <p> The Clipboard object should return a reference if available and a copy if not. </p>
		 */
		public static const ORIGINAL_PREFERRED:String = "originalPreferred";
	}
}
