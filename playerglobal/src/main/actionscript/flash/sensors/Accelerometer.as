package flash.sensors {
	import flash.events.AccelerometerEvent;
	import flash.events.EventDispatcher;
	import flash.events.StatusEvent;

	[Event(name="status", type="flash.events.StatusEvent")]
	[Event(name="update", type="flash.events.AccelerometerEvent")]
	/**
	 *  The Accelerometer class dispatches events based on activity detected by the device's motion sensor. This data represents the device's location or movement along a 3-dimensional axis. When the device moves, the sensor detects this movement and returns acceleration data. The Accelerometer class provides methods to query whether or not accelerometer is supported, and also to set the rate at which acceleration events are dispatched. <p> <b>Note:</b> Use the <code>Accelerometer.isSupported</code> property to test the runtime environment for the ability to use this feature. While the Accelerometer class and its members are accessible to the Runtime Versions listed for each API entry, the current environment for the runtime determines the availability of this feature. For example, you can compile code using the Accelerometer class properties for Flash Player 10.1, but you need to use the <code>Accelerometer.isSupported</code> property to test for the availability of the Accelerometer feature in the current deployment environment for the Flash Player runtime. If <code>Accelerometer.isSupported</code> is <code>true</code> at runtime, then Accelerometer support currently exists.</p> <p> <i>AIR profile support:</i> This feature is supported only on mobile devices. It is not supported on desktop or AIR for TV devices. See <a href="http://help.adobe.com/en_US/air/build/WS144092a96ffef7cc16ddeea2126bb46b82f-8000.html"> AIR Profile Support</a> for more information regarding API support across multiple profiles. </p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://www.riagora.com/2010/04/air-and-the-accelerometer/" target="_blank">Michael Chaize: AIR and the Accelerometer</a>
	 *  <br>
	 *  <a href="http://www.riagora.com/2010/05/become-an-air-pilot/" target="_blank">Michael Chaize: Become an AIR Pilot</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Accelerometer.html#isSupported" target="">isSupported</a>
	 *  <br>
	 *  <a href="../../flash/events/AccelerometerEvent.html" target="">flash.events.AccelerometerEvent</a>
	 * </div><br><hr>
	 */
	public class Accelerometer extends EventDispatcher {
		private static var _isSupported:Boolean;

		private var _muted:Boolean;

		/**
		 * <p> Creates a new Accelerometer instance. </p>
		 */
		public function Accelerometer() {
			super();
			throw new Error("Not implemented");
		}

		/**
		 * <p> Specifies whether the user has denied access to the accelerometer (<code>true</code>) or allowed access (<code>false</code>). When this value changes, a <code>status</code> event is dispatched. </p>
		 * 
		 * @return 
		 */
		public function get muted():Boolean {
			return _muted;
		}

		/**
		 * <p> The <code>setRequestedUpdateInterval</code> method is used to set the desired time interval for updates. The time interval is measured in milliseconds. The update interval is only used as a hint to conserve the battery power. The actual time between acceleration updates may be greater or lesser than this value. Any change in the update interval affects all registered listeners. You can use the Accelerometer class without calling the <code>setRequestedUpdateInterval()</code> method. In this case, the application receives updates based on the device's default interval. </p>
		 * 
		 * @param interval  — The requested update interval. If <code>interval</code> is set to 0, then the minimum supported update interval is used. 
		 */
		public function setRequestedUpdateInterval(interval:Number):void {
			throw new Error("Not implemented");
		}


		/**
		 * <p> The <code>isSupported</code> property is set to <code>true</code> if the accelerometer sensor is available on the device, otherwise it is set to <code>false</code>. </p>
		 * 
		 * @return 
		 */
		public static function get isSupported():Boolean {
			return _isSupported;
		}
	}
}
