package flash.display {
	import fr.lenra.playerglobal.utils.ColorUtil;
	
	/**
	 *  Defines a solid fill. <p> Use a GraphicsSolidFill object with the <code>Graphics.drawGraphicsData()</code> method. Drawing a GraphicsSolidFill object is the equivalent of calling the <code>Graphics.beginFill()</code> method. </p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Graphics.html#beginFill()" target="">flash.display.Graphics.beginFill()</a>
	 *  <br>
	 *  <a href="Graphics.html#drawGraphicsData()" target="">flash.display.Graphics.drawGraphicsData()</a>
	 * </div><br><hr>
	 */
	public class GraphicsSolidFill implements IGraphicsFill, IGraphicsData {
		private var _alpha:Number;
		private var _color:uint;

		/**
		 * <p> Creates a new GraphicsSolidFill object. </p>
		 * 
		 * @param color  — The color value. Valid values are in the hexadecimal format 0xRRGGBB. 
		 * @param alpha  — The alpha transparency value. Valid values are 0 (fully transparent) to 1 (fully opaque). 
		 */
		public function GraphicsSolidFill(color:uint = 0, alpha:Number = 1.0) {
			this.color = color;
			this.alpha = alpha;
		}

		/**
		 * <p> Indicates the alpha transparency value of the fill. Valid values are 0 (fully transparent) to 1 (fully opaque). The default value is 1. Display objects with alpha set to 0 are active, even though they are invisible. </p>
		 * 
		 * @return 
		 */
		public function get alpha():Number {
			return _alpha;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set alpha(value:Number):void {
			_alpha = value;
		}

		/**
		 * <p> The color of the fill. Valid values are in the hexadecimal format 0xRRGGBB. The default value is 0xFF0000 (or the uint 0). </p>
		 * 
		 * @return 
		 */
		public function get color():uint {
			return _color;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set color(value:uint):void {
			_color = value;
		}
		
		public function draw(el:Element):void {
			el.setAttribute("fill", ColorUtil.toColorString(this.color, this.alpha));
		}
	}
}
