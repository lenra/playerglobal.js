package flash.display {
	import flash.geom.Matrix;

	/**
	 *  Defines a bitmap fill. The bitmap can be smoothed, repeated or tiled to fill the area; or manipulated using a transformation matrix. <p> Use a GraphicsBitmapFill object with the <code>Graphics.drawGraphicsData()</code> method. Drawing a GraphicsBitmapFill object is the equivalent of calling the <code>Graphics.beginBitmapFill()</code> method. </p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Graphics.html#drawGraphicsData()" target="">flash.display.Graphics.drawGraphicsData()</a>
	 *  <br>
	 *  <a href="Graphics.html#beginBitmapFill()" target="">flash.display.Graphics.beginBitmapFill()</a>
	 * </div><br><hr>
	 */
	public class GraphicsBitmapFill implements IGraphicsFill, IGraphicsData {
		private var _bitmapData:BitmapData;
		private var _matrix:Matrix;
		private var _repeat:Boolean;
		private var _smooth:Boolean;

		/**
		 * <p> Creates a new GraphicsBitmapFill object. </p>
		 * 
		 * @param bitmapData  — A transparent or opaque bitmap image that contains the bits to display. 
		 * @param matrix  — A matrix object (of the flash.geom.Matrix class), which you use to define transformations on the bitmap. 
		 * @param repeat  — If <code>true</code>, the bitmap image repeats in a tiled pattern. If <code>false</code>, the bitmap image does not repeat, and the edges of the bitmap are used for any fill area that extends beyond the bitmap. 
		 * @param smooth  — If <code>false</code>, upscaled bitmap images are rendered using a nearest-neighbor algorithm and appear pixelated. If <code>true</code>, upscaled bitmap images are rendered using a bilinear algorithm. Rendering that uses the nearest-neighbor algorithm is usually faster. 
		 */
		public function GraphicsBitmapFill(bitmapData:BitmapData = null, matrix:Matrix = null, repeat:Boolean = true, smooth:Boolean = false) {
			throw new Error("Not implemented");
		}

		/**
		 * <p> A transparent or opaque bitmap image. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="BitmapData.html" target="">flash.display.BitmapData</a>
		 * </div>
		 * 
		 * @return 
		 */
		public function get bitmapData():BitmapData {
			return _bitmapData;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set bitmapData(value:BitmapData):void {
			_bitmapData = value;
		}

		/**
		 * <p> A matrix object (of the flash.geom.Matrix class) that defines transformations on the bitmap. For example, the following matrix rotates a bitmap by 45 degrees (pi/4 radians): </p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *      matrix = new flash.geom.Matrix(); 
		 *      matrix.rotate(Math.PI / 4);
		 *      </pre>
		 * </div>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/geom/Matrix.html" target="">flash.geom.Matrix</a>
		 * </div>
		 * 
		 * @return 
		 */
		public function get matrix():Matrix {
			return _matrix;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set matrix(value:Matrix):void {
			_matrix = value;
		}

		/**
		 * <p> Specifies whether to repeat the bitmap image in a tiled pattern. </p>
		 * <p> If <code>true</code>, the bitmap image repeats in a tiled pattern. If <code>false</code>, the bitmap image does not repeat, and the outermost pixels along the edges of the bitmap are used for any fill area that extends beyond the bounds of the bitmap.</p>
		 * <p>For example, consider the following bitmap (a 20 x 20-pixel checkerboard pattern):</p>
		 * <p><img src="../../images/movieClip_beginBitmapFill_repeat_1.jpg" alt="20 by 20 pixel checkerboard"></p>
		 * <p>When <code>repeat</code> is set to <code>true</code> (as in the following example), the bitmap fill repeats the bitmap:</p>
		 * <p><img src="../../images/movieClip_beginBitmapFill_repeat_2.jpg" alt="60 by 60 pixel checkerboard"></p>
		 * <p>When <code>repeat</code> is set to <code>false</code>, the bitmap fill uses the edge pixels for the fill area outside the bitmap:</p>
		 * <p><img src="../../images/movieClip_beginBitmapFill_repeat_3.jpg" alt="60 by 60 pixel image with no repeating"></p>
		 * 
		 * @return 
		 */
		public function get repeat():Boolean {
			return _repeat;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set repeat(value:Boolean):void {
			_repeat = value;
		}

		/**
		 * <p> Specifies whether to apply a smoothing algorithm to the bitmap image. </p>
		 * <p> If <code>false</code>, upscaled bitmap images are rendered by using a nearest-neighbor algorithm and look pixelated. If <code>true</code>, upscaled bitmap images are rendered by using a bilinear algorithm. Rendering by using the nearest neighbor algorithm is usually faster. </p>
		 * 
		 * @return 
		 */
		public function get smooth():Boolean {
			return _smooth;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set smooth(value:Boolean):void {
			_smooth = value;
		}
		
		public function draw(el:Element):void {
			throw new Error("Not implemented");
		}
	}
}
