package flash.display {
	/**
	 *  The SWFVersion class is an enumeration of constant values that indicate the file format version of a loaded SWF file. The SWFVersion constants are provided for use in checking the <code>swfVersion</code> property of a flash.display.LoaderInfo object. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="LoaderInfo.html#swfVersion" target="">flash.display.LoaderInfo.swfVersion</a>
	 * </div><br><hr>
	 */
	public class SWFVersion {
		/**
		 * <p> SWF file format version 1.0. </p>
		 */
		public static const FLASH1:uint = 1;
		/**
		 * <p> SWF file format version 10.0. </p>
		 */
		public static const FLASH10:uint = 10;
		/**
		 * <p> SWF file format version 11.0. </p>
		 */
		public static const FLASH11:uint = 11;
		/**
		 * <p> SWF file format version 12.0. </p>
		 */
		public static const FLASH12:uint = 12;
		/**
		 * <p> SWF file format version 2.0. </p>
		 */
		public static const FLASH2:uint = 2;
		/**
		 * <p> SWF file format version 3.0. </p>
		 */
		public static const FLASH3:uint = 3;
		/**
		 * <p> SWF file format version 4.0. </p>
		 */
		public static const FLASH4:uint = 4;
		/**
		 * <p> SWF file format version 5.0. </p>
		 */
		public static const FLASH5:uint = 5;
		/**
		 * <p> SWF file format version 6.0. </p>
		 */
		public static const FLASH6:uint = 6;
		/**
		 * <p> SWF file format version 7.0. </p>
		 */
		public static const FLASH7:uint = 7;
		/**
		 * <p> SWF file format version 8.0. </p>
		 */
		public static const FLASH8:uint = 8;
		/**
		 * <p> SWF file format version 9.0. </p>
		 */
		public static const FLASH9:uint = 9;
	}
}
