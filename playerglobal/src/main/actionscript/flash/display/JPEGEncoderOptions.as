package flash.display {
	/**
	 *  The JPEGEncoderOptions class defines a compression algorithm for the <code>flash.display.BitmapData.encode()</code> method. <p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS4768145595f94108-17913eb4136eaab51c7-8000.html" target="_blank">Compressing bitmap data</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="BitmapData.html" target="">flash.display.BitmapData</a>
	 *  <br>
	 *  <a href="BitmapData.html#encode()" target="">flash.display.BitmapData.encode()</a>
	 * </div><br><hr>
	 */
	public class JPEGEncoderOptions {
		private var _quality:uint;

		/**
		 * <p> Creates a JPEGEncoderOptions object with the specified setting. </p>
		 * 
		 * @param quality  — The initial quality value. 
		 */
		public function JPEGEncoderOptions(quality:uint = 80) {
			throw new Error("Not implemented");
		}

		/**
		 * <p> A value between 1 and 100, where 1 means the lowest quality and 100 means the highest quality. The higher the value, the larger the size of the output of the compression, and the smaller the compression ratio. </p>
		 * 
		 * @return 
		 */
		public function get quality():uint {
			return _quality;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set quality(value:uint):void {
			_quality = value;
		}
	}
}
