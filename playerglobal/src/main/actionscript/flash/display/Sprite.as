package flash.display {
	import flash.geom.Rectangle;
	import flash.media.SoundTransform;

	/**
	 *  The Sprite class is a basic display list building block: a display list node that can display graphics and can also contain children. <p>A Sprite object is similar to a movie clip, but does not have a timeline. Sprite is an appropriate base class for objects that do not require timelines. For example, Sprite would be a logical base class for user interface (UI) components that typically do not use the timeline.</p> <p>The Sprite class is new in ActionScript 3.0. It provides an alternative to the functionality of the MovieClip class, which retains all the functionality of previous ActionScript releases to provide backward compatibility.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS2db454920e96a9e51e63e3d11c0bf62d75-7feb.html" target="_blank">Creating a subclass of Sprite as a download progress bar</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7f37.html" target="_blank">Display object example: SpriteArranger</a>
	 * </div><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e58.html" target="_blank">Display programming</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e3e.html" target="_blank">Basics of display programming</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e3c.html" target="_blank">Core display classes</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e3d.html" target="_blank">Working with display objects</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7dfa.html" target="_blank">Choosing a DisplayObject subclass</a>
	 *  <br>
	 * </div><br><hr>
	 */
	public class Sprite extends DisplayObjectContainer {
		private var _buttonMode:Boolean;
		private var _hitArea:Sprite;
		private var _soundTransform:SoundTransform;
		private var _useHandCursor:Boolean;

		private var _dropTarget:DisplayObject;
		private var _graphics:Graphics;
		
		/**
		 * <p> Creates a new Sprite instance. After you create the Sprite instance, call the <code>DisplayObjectContainer.addChild()</code> or <code>DisplayObjectContainer.addChildAt()</code> method to add the Sprite to a parent DisplayObjectContainer. </p>
		 */
		public function Sprite() {
			super();
		}

		/**
		 * <p> Specifies the button mode of this sprite. If <code>true</code>, this sprite behaves as a button, which means that it triggers the display of the hand cursor when the pointer passes over the sprite and can receive a <code>click</code> event if the enter or space keys are pressed when the sprite has focus. You can suppress the display of the hand cursor by setting the <code>useHandCursor</code> property to <code>false</code>, in which case the pointer is displayed. </p>
		 * <p>Although it is better to use the SimpleButton class to create buttons, you can use the <code>buttonMode</code> property to give a sprite some button-like functionality. To include a sprite in the tab order, set the <code>tabEnabled</code> property (inherited from the InteractiveObject class and <code>false</code> by default) to <code>true</code>. Additionally, consider whether you want the children of your sprite to be user input enabled. Most buttons do not enable user input interactivity for their child objects because it confuses the event flow. To disable user input interactivity for all child objects, you must set the <code>mouseChildren</code> property (inherited from the DisplayObjectContainer class) to <code>false</code>.</p>
		 * <p>If you use the <code>buttonMode</code> property with the MovieClip class (which is a subclass of the Sprite class), your button might have some added functionality. If you include frames labeled _up, _over, and _down, Flash Player provides automatic state changes (functionality similar to that provided in previous versions of ActionScript for movie clips used as buttons). These automatic state changes are not available for sprites, which have no timeline, and thus no frames to label. </p>
		 * 
		 * @return 
		 */
		public function get buttonMode():Boolean {
			return _buttonMode;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set buttonMode(value:Boolean):void {
			_buttonMode = value;
		}

		/**
		 * <p> Specifies the display object over which the sprite is being dragged, or on which the sprite was dropped. </p>
		 * 
		 * @return 
		 */
		public function get dropTarget():DisplayObject {
			return _dropTarget;
		}

		/**
		 * <p> Specifies the Graphics object that belongs to this sprite where vector drawing commands can occur. </p>
		 * 
		 * @return 
		 */
		public function get graphics():Graphics {
			if (!_graphics) {
				_graphics = new Graphics();
				_graphics.svgParent = this.svgElement;
			}
			return _graphics;
		}

		/**
		 * <p> Designates another sprite to serve as the hit area for a sprite. If the <code>hitArea</code> property does not exist or the value is <code>null</code> or <code>undefined</code>, the sprite itself is used as the hit area. The value of the <code>hitArea</code> property can be a reference to a Sprite object. </p>
		 * <p>You can change the <code>hitArea</code> property at any time; the modified sprite immediately uses the new hit area behavior. The sprite designated as the hit area does not need to be visible; its graphical shape, although not visible, is still detected as the hit area.</p>
		 * <p><b>Note:</b> You must set to <code>false</code> the <code>mouseEnabled</code> property of the sprite designated as the hit area. Otherwise, your sprite button might not work because the sprite designated as the hit area receives the user input events instead of your sprite button.</p>
		 * 
		 * @return 
		 */
		public function get hitArea():Sprite {
			return _hitArea;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set hitArea(value:Sprite):void {
			_hitArea = value;
		}

		/**
		 * <p> Controls sound within this sprite. </p>
		 * <p><b>Note:</b> This property does not affect HTML content in an HTMLControl object (in Adobe AIR).</p>
		 * 
		 * @return 
		 */
		public function get soundTransform():SoundTransform {
			return _soundTransform;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set soundTransform(value:SoundTransform):void {
			_soundTransform = value;
		}

		/**
		 * <p> A Boolean value that indicates whether the pointing hand (hand cursor) appears when the pointer rolls over a sprite in which the <code>buttonMode</code> property is set to <code>true</code>. The default value of the <code>useHandCursor</code> property is <code>true</code>. If <code>useHandCursor</code> is set to <code>true</code>, the pointing hand used for buttons appears when the pointer rolls over a button sprite. If <code>useHandCursor</code> is <code>false</code>, the arrow pointer is used instead. </p>
		 * <p>You can change the <code>useHandCursor</code> property at any time; the modified sprite immediately takes on the new cursor appearance. </p>
		 * <p><b>Note:</b> In Flex or Flash Builder, if your sprite has child sprites, you might want to set the <code>mouseChildren</code> property to <code>false</code>. For example, if you want a hand cursor to appear over a Flex &lt;mx:Label&gt; control, set the <code>useHandCursor</code> and <code>buttonMode</code> properties to <code>true</code>, and the <code>mouseChildren</code> property to <code>false</code>.</p>
		 * 
		 * @return 
		 */
		public function get useHandCursor():Boolean {
			return _useHandCursor;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set useHandCursor(value:Boolean):void {
			_useHandCursor = value;
		}
		
		override protected function createSVGElement():SVGElement {
			return document.createElementNS(SVG_NAMESPACE, "g") as SVGGElement;
		}

		/**
		 * <p> Lets the user drag the specified sprite. The sprite remains draggable until explicitly stopped through a call to the <code>Sprite.stopDrag()</code> method, or until another sprite is made draggable. Only one sprite is draggable at a time. </p>
		 * <p>Three-dimensional display objects follow the pointer and <code>Sprite.startDrag()</code> moves the object within the three-dimensional plane defined by the display object. Or, if the display object is a two-dimensional object and the child of a three-dimensional object, the two-dimensional object moves within the three dimensional plane defined by the three-dimensional parent object.</p>
		 * 
		 * @param lockCenter  — Specifies whether the draggable sprite is locked to the center of the pointer position (<code>true</code>), or locked to the point where the user first clicked the sprite (<code>false</code>). 
		 * @param bounds  — Value relative to the coordinates of the Sprite's parent that specify a constraint rectangle for the Sprite. 
		 */
		public function startDrag(lockCenter:Boolean = false, bounds:Rectangle = null):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Lets the user drag the specified sprite on a touch-enabled device. The sprite remains draggable until explicitly stopped through a call to the <code>Sprite.stopTouchDrag()</code> method, or until another sprite is made draggable. Only one sprite is draggable at a time. </p>
		 * <p>Three-dimensional display objects follow the pointer and <code>Sprite.startTouchDrag()</code> moves the object within the three-dimensional plane defined by the display object. Or, if the display object is a two-dimensional object and the child of a three-dimensional object, the two-dimensional object moves within the three dimensional plane defined by the three-dimensional parent object.</p>
		 * 
		 * @param touchPointID  — An integer to assign to the touch point. 
		 * @param lockCenter  — Specifies whether the draggable sprite is locked to the center of the pointer position (<code>true</code>), or locked to the point where the user first clicked the sprite (<code>false</code>). 
		 * @param bounds  — Value relative to the coordinates of the Sprite's parent that specify a constraint rectangle for the Sprite. 
		 */
		public function startTouchDrag(touchPointID:int, lockCenter:Boolean = false, bounds:Rectangle = null):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Ends the <code>startDrag()</code> method. A sprite that was made draggable with the <code>startDrag()</code> method remains draggable until a <code>stopDrag()</code> method is added, or until another sprite becomes draggable. Only one sprite is draggable at a time. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Sprite.html#dropTarget" target="">dropTarget</a>
		 *  <br>
		 *  <a href="Sprite.html#startDrag()" target="">startDrag()</a>
		 * </div>
		 * <span id="pageFilter"><br><span class="label"> Example &nbsp;( <span class="usage"><a href="http://www.adobe.com/go/learn_as3_usingexamples_en"> How to use this example </a></span>) </span><br><br>
		 *  <div class="detailBody">
		 *    The following example creates a 
		 *   <code>circle</code> sprite and two 
		 *   <code>target</code> sprites. The 
		 *   <code>startDrag()</code> method is called on the 
		 *   <code>circle</code> sprite when the user positions the cursor over the sprite and presses the mouse button, and the 
		 *   <code>stopDrag()</code> method is called when the user releases the mouse button. This lets the user drag the sprite. On release of the mouse button, the 
		 *   <code>mouseRelease()</code> method is called, which in turn traces the 
		 *   <code>name</code> of the 
		 *   <code>dropTarget</code> object — the one to which the user dragged the 
		 *   <code>circle</code> sprite: 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>
		 * import flash.display.Sprite;
		 * import flash.events.MouseEvent;
		 * 
		 * var circle:Sprite = new Sprite();
		 * circle.graphics.beginFill(0xFFCC00);
		 * circle.graphics.drawCircle(0, 0, 40);
		 * 
		 * var target1:Sprite = new Sprite();
		 * target1.graphics.beginFill(0xCCFF00);
		 * target1.graphics.drawRect(0, 0, 100, 100);
		 * target1.name = "target1";
		 * 
		 * var target2:Sprite = new Sprite();
		 * target2.graphics.beginFill(0xCCFF00);
		 * target2.graphics.drawRect(0, 200, 100, 100);
		 * target2.name = "target2";
		 * 
		 * addChild(target1);
		 * addChild(target2);
		 * addChild(circle);
		 * 
		 * circle.addEventListener(MouseEvent.MOUSE_DOWN, mouseDown) 
		 * 
		 * function mouseDown(event:MouseEvent):void {
		 *     circle.startDrag();
		 * }
		 * circle.addEventListener(MouseEvent.MOUSE_UP, mouseReleased);
		 * 
		 * function mouseReleased(event:MouseEvent):void {
		 *     circle.stopDrag();
		 *     trace(circle.dropTarget.name);
		 * }
		 * </pre>
		 *   </div>
		 *  </div></span>
		 */
		public function stopDrag():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Ends the <code>startTouchDrag()</code> method, for use with touch-enabled devices. A sprite that was made draggable with the <code>startTouchDrag()</code> method remains draggable until a <code>stopTouchDrag()</code> method is added, or until another sprite becomes draggable. Only one sprite is draggable at a time. </p>
		 * 
		 * @param touchPointID  — The integer assigned to the touch point in the <code>startTouchDrag</code> method. 
		 */
		public function stopTouchDrag(touchPointID:int):void {
			throw new Error("Not implemented");
		}
	}
}
