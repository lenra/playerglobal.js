package flash.display {
	/**
	 *  This interface is used to define objects that can be used as stroke parameters in the flash.display.Graphics methods and drawing classes. Use the implementor classes of this interface to create and manage stroke property data, and to reuse the same data for different instances. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Graphics.html#drawGraphicsData()" target="">flash.display.Graphics.drawGraphicsData()</a>
	 * </div><br><hr>
	 */
	public interface IGraphicsStroke {
	}
}
