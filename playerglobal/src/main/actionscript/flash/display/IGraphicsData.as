package flash.display {
	/**
	 *  This interface is used to define objects that can be used as parameters in the flash.display.Graphics methods, including fills, strokes, and paths. Use the implementor classes of this interface to create and manage drawing property data, and to reuse the same data for different instances. Then, use the methods of the Graphics class to render the drawing objects. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Graphics.html#drawGraphicsData()" target="">flash.display.Graphics.drawGraphicsData()</a>
	 *  <br>
	 *  <a href="Graphics.html#readGraphicsData()" target="">flash.display.Graphics.readGraphicsData()</a>
	 * </div><br><hr>
	 */
	public interface IGraphicsData {
		function draw(el:Element):void;
	}
}
