package flash.display {
	/**
	 *  Indicates the end of a graphics fill. Use a GraphicsEndFill object with the <code>Graphics.drawGraphicsData()</code> method. <p> Drawing a GraphicsEndFill object is the equivalent of calling the <code>Graphics.endFill()</code> method. </p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Graphics.html#drawGraphicsData()" target="">flash.display.Graphics.drawGraphicsData()</a>
	 *  <br>
	 *  <a href="Graphics.html#endFill()" target="">flash.display.Graphics.endFill()</a>
	 * </div><br><hr>
	 */
	public class GraphicsEndFill implements IGraphicsFill, IGraphicsData {

		/**
		 * <p> Creates an object to use with the <code>Graphics.drawGraphicsData()</code> method to end the fill, explicitly. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Graphics.html#drawGraphicsData()" target="">flash.display.Graphics.drawGraphicsData()</a>
		 *  <br>
		 *  <a href="Graphics.html#endFill()" target="">flash.display.Graphics.endFill()</a>
		 * </div>
		 */
		public function GraphicsEndFill() {
			
		}
		
		public function draw(el:Element):void {
			el.setAttribute("fill", "");
		}
	}
}
