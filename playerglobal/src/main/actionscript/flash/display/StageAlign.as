package flash.display {
	/**
	 *  The StageAlign class provides constant values to use for the <code>Stage.align</code> property. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Stage.html#align" target="">flash.display.Stage.align</a>
	 * </div><br><hr>
	 */
	public class StageAlign {
		/**
		 * <p> Specifies that the Stage is aligned at the bottom. </p>
		 */
		public static const BOTTOM:String = "B";
		/**
		 * <p> Specifies that the Stage is aligned in the bottom-left corner. </p>
		 */
		public static const BOTTOM_LEFT:String = "BL";
		/**
		 * <p> Specifies that the Stage is aligned in the bottom-right corner. </p>
		 */
		public static const BOTTOM_RIGHT:String = "BR";
		/**
		 * <p> Specifies that the Stage is aligned on the left. </p>
		 */
		public static const LEFT:String = "L";
		/**
		 * <p> Specifies that the Stage is aligned to the right. </p>
		 */
		public static const RIGHT:String = "R";
		/**
		 * <p> Specifies that the Stage is aligned at the top. </p>
		 */
		public static const TOP:String = "T";
		/**
		 * <p> Specifies that the Stage is aligned in the top-left corner. </p>
		 */
		public static const TOP_LEFT:String = "TL";
		/**
		 * <p> Specifies that the Stage is aligned in the top-right corner. </p>
		 */
		public static const TOP_RIGHT:String = "TR";
	}
}
