package flash.display {
	import flash.display3D.Context3D;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;

	[Event(name="context3DCreate", type="flash.events.Event")]
	[Event(name="error", type="flash.events.ErrorEvent")]
	/**
	 *  The Stage3D class provides a display area and a programmable rendering context for drawing 2D and 3D graphics. <p>Stage3D provides a high-performance rendering surface for content rendered using the <code>Context3D</code> class. This surface uses the graphics processing unit (GPU) when possible. The runtime stage provides a fixed number of <code>Stage3D</code> objects. The number of instances varies by the type of device. Desktop computers typically provide four Stage3D instances.</p> <p>Content drawn to the <code>Stage3D</code> viewport is composited with other visible graphics objects in a predefined order. The most distant are all <code>StageVideo</code> surfaces. <code>Stage3D</code> comes next, with traditional Flash display object content being rendered last, on top of all others. StageVideo and Stage3D layers are rendered with no transparency; thus a viewport completely obscures any other Stage3D or StageVideo viewports positioned underneath it. Display list content is rendered with transparency.</p> <p> <b>Note:</b> You can use the <code>visible</code> property of a Stage3D object to remove it from the display temporarily, such as when playing a video using the StageVideo class.</p> <p>A <code>Stage3D</code> object is retrieved from the Player stage using its <code>stage3Ds</code> member. Use the Stage3D instance to request an associated rendering context and to position the display on the runtime stage.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Stage.html" target="">Stage</a>
	 *  <br>
	 *  <a href="../../flash/display3D/Context3D.html" target="">flash.display3D.Context3D</a>
	 * </div><br><hr>
	 */
	public class Stage3D extends EventDispatcher {
		private var _visible:Boolean;
		private var _x:Number;
		private var _y:Number;

		private var _context3D:Context3D;

		public function Stage3D() {
			super(this);
			throw new Error("Not implemented");
		}

		/**
		 * <p> The Context3D object associated with this Stage3D instance. </p>
		 * <p>This property is initially <code>null</code>. To create the Context3D instance for this Stage3D object, add an event listener for the <code>context3DCreate</code> event and then call the <code>requestContext3D</code> method. The listener is called once the Context3D object has been created. </p>
		 * 
		 * @return 
		 */
		public function get context3D():Context3D {
			return _context3D;
		}

		/**
		 * <p> Specifies whether this Stage3D object is visible. </p>
		 * <p>Use this property to temporarily hide a Stage3D object on the Stage. This property defaults to <code>true</code>.</p>
		 * 
		 * @return 
		 */
		public function get visible():Boolean {
			return _visible;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set visible(value:Boolean):void {
			_visible = value;
		}

		/**
		 * <p> The horizontal coordinate of the Stage3D display on the stage, in pixels. </p>
		 * <p>This property defaults to zero.</p>
		 * 
		 * @return 
		 */
		public function get x():Number {
			return _x;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set x(value:Number):void {
			_x = value;
		}

		/**
		 * <p> The vertical coordinate of the Stage3D display on the stage, in pixels. </p>
		 * <p>This property defaults to zero.</p>
		 * 
		 * @return 
		 */
		public function get y():Number {
			return _y;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set y(value:Number):void {
			_y = value;
		}

		/**
		 * <p> Request the creation of a Context3D object for this Stage3D instance. </p>
		 * <p>Before calling this function, add an event listener for the <code>context3DCreate</code> event. If you do not, the runtime throws an exception. </p>
		 * <p><b>Important note on device loss:</b> GPU device loss occurs when the GPU hardware becomes unavailable to the application. The Context3D object is disposed when the GPU device is lost. GPU device loss can happen for various reasons, such as, when a mobile device runs out of battery power or a Windows device goes to a "lock screen." When the GPU becomes available again, the runtime creates a new Context3D instance and dispatches another <code>context3DCreate</code> event. Your application must reload all assets and reset the rendering context state whenever device loss occurs.</p>
		 * <p>Design your application logic to handle the possibility of device loss and context regeneration. Do not remove the <code>context3DCreate</code> event listener. Do not perform actions in response to the event that should not be repeated in the application. For example, do not add anonymous functions to handle timer events because they would be duplicated after device loss. To test your application's handling of device loss, you can simulate device loss by calling the <code>dispose()</code> method of the Context3D object. </p>
		 * <p>The following example illustrates how to request a Context3d rendering context:</p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *         if( stage.stage3Ds.length &gt; 0 )
		 *         {
		 *             var stage3D:Stage3D = stage.stage3Ds[0];    
		 *             stage3D.addEventListener( Event.CONTEXT3D_CREATE, myContext3DHandler ); 
		 *             stage3D.requestContext3D( ); 
		 *         } 
		 *         
		 *         function myContext3DHandler ( event : Event ) : void 
		 *         {
		 *             var targetStage3D : Stage3D = event.target as Stage3D; 
		 *             InitAll3DResources( targetStage3D.context3D );
		 *             StartRendering( targetStage3D.context3D ); 
		 *         }
		 *         </pre>
		 * </div>
		 * 
		 * @param context3DRenderMode  — The type of rendering context to request. The default is <code>Context3DRenderMode.AUTO</code> for which the runtime will create a hardware-accelerated context if possible and fall back to software otherwise. Use <code>Context3DRenderMode.SOFTWARE</code> to request a software rendering context. Software rendering is not available on mobile devices. Software rendering is available only for <code>Context3DProfile.BASELINE</code> and <code>Context3DProfile.BASELINE_CONSTRAINED</code>. 
		 * @param profile  — (AIR 3.4 and higher) Specifies the extent to which Flash Player supports lower-level GPUs. The default is <code>Context3DProfile.BASELINE</code>, which returns a <code>Context3D</code> instance similar to that used in previous releases. To get details of all available profiles, see <code>flash.display3D.Context3DProfile</code>. 
		 */
		public function requestContext3D(context3DRenderMode:String = "auto", profile:String = "baseline"):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Request the creation of a Context3D object for this Stage3D instance. </p>
		 * <p>Before calling this function, add an event listener for the <code>context3DCreate</code> event. If you do not, the runtime throws an exception. </p>
		 * <p><b>Important note on device loss:</b> GPU device loss occurs when the GPU hardware becomes unavailable to the application. The Context3D object is disposed when the GPU device is lost. GPU device loss can happen for various reasons, such as, when a mobile device runs out of battery power or a Windows device goes to a "lock screen." When the GPU becomes available again, the runtime creates a new Context3D instance and dispatches another <code>context3DCreate</code> event. Your application must reload all assets and reset the rendering context state whenever device loss occurs.</p>
		 * <p>Design your application logic to handle the possibility of device loss and context regeneration. Do not remove the <code>context3DCreate</code> event listener. Do not perform actions in response to the event that should not be repeated in the application. For example, do not add anonymous functions to handle timer events because they would be duplicated after device loss. To test your application's handling of device loss, you can simulate device loss by calling the <code>dispose()</code> method of the Context3D object. </p>
		 * <p>The following example illustrates how to request a Context3d rendering context:</p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *         if( stage.stage3Ds.length &gt; 0 )
		 *         {
		 *           var stage3D:Stage3D = stage.stage3Ds[0];   
		 *           stage3D.addEventListener( Event.CONTEXT3D_CREATE, myContext3DHandler );
		 *           stage3D.requestContext3DMatchingProfiles(Vector.&lt;string&gt;([Context3DProfile.BASELINE, Context3DProfile.BASELINE_EXTENDED]));
		 *         }
		 *         
		 *         function myContext3DHandler ( event : Event ) : void
		 *         {
		 *                var targetStage3D : Stage3D = event.target as Stage3D;
		 *             if(targetStage3D.context3D.profile.localeCompare(Context3DProfile.BASELINE) == 0)
		 *             {
		 *                   InitAll3DResources( targetStage3D.context3D );          
		 *             }
		 *             StartRendering( targetStage3D.context3D );        
		 *         </pre>
		 * </div>
		 * 
		 * @param profiles  — (AIR 3.4 and higher) a profile arrays that developer want to use in their flash program. When developer pass profile array to Stage3D.requestContext3DMatchingProfiles, he will get a Context3D based on the high level profile in that array according to their hardware capability. The rendermode is setted to AUTO, so the param is omitted. 
		 */
		public function requestContext3DMatchingProfiles(profiles:Vector.<String>):void {
			throw new Error("Not implemented");
		}
	}
}
