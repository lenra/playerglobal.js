package flash.display {
	/**
	 *  Defines the values to use for specifying path-drawing commands. <p>The values in this class are used by the <code>Graphics.drawPath()</code> method, or stored in the <code>commands</code> vector of a GraphicsPath object.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Graphics.html#drawPath()" target="">flash.display.Graphics.drawPath()</a>
	 *  <br>
	 *  <a href="GraphicsPath.html#commands" target="">flash.display.GraphicsPath.commands</a>
	 * </div><br><hr>
	 */
	public class GraphicsPathCommand {
		/**
		 * <p> Specifies a drawing command that draws a curve from the current drawing position to the x- and y-coordinates specified in the data vector, using a 2 control points. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Graphics.html#cubicCurveTo()" target="">flash.display.Graphics.cubicCurveTo()</a>
		 * </div>
		 */
		public static const CUBIC_CURVE_TO:int = 6;
		/**
		 * <p> Specifies a drawing command that draws a curve from the current drawing position to the x- and y-coordinates specified in the data vector, using a control point. This command produces the same effect as the <code>Graphics.lineTo()</code> method, and uses two points in the data vector control and anchor: (cx, cy, ax, ay ). </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Graphics.html#curveTo()" target="">flash.display.Graphics.curveTo()</a>
		 * </div>
		 */
		public static const CURVE_TO:int = 3;
		/**
		 * <p> Specifies a drawing command that draws a line from the current drawing position to the x- and y-coordinates specified in the data vector. This command produces the same effect as the <code>Graphics.lineTo()</code> method, and uses one point in the data vector: (x,y). </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Graphics.html#lineTo()" target="">flash.display.Graphics.lineTo()</a>
		 * </div>
		 */
		public static const LINE_TO:int = 2;
		/**
		 * <p> Specifies a drawing command that moves the current drawing position to the x- and y-coordinates specified in the data vector. This command produces the same effect as the <code>Graphics.moveTo()</code> method, and uses one point in the data vector: (x,y). </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Graphics.html#moveTo()" target="">flash.display.Graphics.moveTo()</a>
		 * </div>
		 */
		public static const MOVE_TO:int = 1;
		/**
		 * <p> Represents the default "do nothing" command. </p>
		 */
		public static const NO_OP:int = 0;
		/**
		 * <p> Specifies a "line to" drawing command, but uses two sets of coordinates (four values) instead of one set. This command allows you to switch between "line to" and "curve to" commands without changing the number of data values used per command. This command uses two sets in the data vector: one dummy location and one (x,y) location. </p>
		 * <p>The <code>WIDE_LINE_TO</code> and <code>WIDE_MOVE_TO</code> command variants consume the same number of parameters as does the <code>CURVE_TO</code> command.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="GraphicsPathCommand.html#LINE_TO" target="">LINE_TO</a>
		 *  <br>
		 *  <a href="Graphics.html#lineTo()" target="">flash.display.Graphics.lineTo()</a>
		 * </div>
		 */
		public static const WIDE_LINE_TO:int = 5;
		/**
		 * <p> Specifies a "move to" drawing command, but uses two sets of coordinates (four values) instead of one set. This command allows you to switch between "move to" and "curve to" commands without changing the number of data values used per command. This command uses two sets in the data vector: one dummy location and one (x,y) location. </p>
		 * <p>The <code>WIDE_LINE_TO</code> and <code>WIDE_MOVE_TO</code> command variants consume the same number of parameters as does the <code>CURVE_TO</code> command.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="GraphicsPathCommand.html#MOVE_TO" target="">MOVE_TO</a>
		 *  <br>
		 *  <a href="Graphics.html#moveTo()" target="">flash.display.Graphics.moveTo()</a>
		 * </div>
		 */
		public static const WIDE_MOVE_TO:int = 4;
	}
}
