package flash.display {
	/**
	 *  The GraphicsPathWinding class provides values for the <code>flash.display.GraphicsPath.winding</code> property and the <code>flash.display.Graphics.drawPath()</code> method to determine the direction to draw a path. A clockwise path is positively wound, and a counter-clockwise path is negatively wound: <p> <img src="../../images/winding_positive_negative.gif" alt="positive and negative winding directions"> </p> <p> When paths intersect or overlap, the winding direction determines the rules for filling the areas created by the intersection or overlap:</p> <p> <img src="../../images/winding_rules_evenodd_nonzero.gif" alt="a comparison of even-odd and non-zero winding rules"> </p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="GraphicsPath.html#winding" target="">flash.display.GraphicsPath.winding</a>
	 *  <br>
	 *  <a href="Graphics.html#drawPath()" target="">flash.display.Graphics.drawPath()</a>
	 * </div><br><hr>
	 */
	public class GraphicsPathWinding {
		/**
		 * <p> Establishes the even-odd winding type. The even-odd winding type is the rule used by all of the original drawing API and is the default type for the <code>flash.display.Graphics.drawPath()</code> method. Any overlapping paths will alternate between open and closed fills. If two squares drawn with the same fill intersect, the area of the intersection is not filled. Adjacent areas are not the same (neither both filled nor both unfilled). </p>
		 */
		public static const EVEN_ODD:String = "evenOdd";
		/**
		 * <p> Establishes the non-zero winding type. The non-zero winding type determines that when paths of opposite winding intersect, the intersection area is unfilled (as with the even-odd winding type). For paths of the same winding, the intersection area is filled. </p>
		 */
		public static const NON_ZERO:String = "nonZero";
	}
}
