package flash.display {
	/**
	 *  The JointStyle class is an enumeration of constant values that specify the joint style to use in drawing lines. These constants are provided for use as values in the <code>joints</code> parameter of the <code>flash.display.Graphics.lineStyle()</code> method. The method supports three types of joints: miter, round, and bevel, as the following example shows: <p> <img src="../../images/linejoin.jpg" alt="MITER, ROUND, and BEVEL"> </p> <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Graphics.html#lineStyle()" target="">flash.display.Graphics.lineStyle()</a>
	 * </div><br><hr>
	 */
	public class JointStyle {
		/**
		 * <p> Specifies beveled joints in the <code>joints</code> parameter of the <code>flash.display.Graphics.lineStyle()</code> method. </p>
		 */
		public static const BEVEL:String = "bevel";
		/**
		 * <p> Specifies mitered joints in the <code>joints</code> parameter of the <code>flash.display.Graphics.lineStyle()</code> method. </p>
		 */
		public static const MITER:String = "miter";
		/**
		 * <p> Specifies round joints in the <code>joints</code> parameter of the <code>flash.display.Graphics.lineStyle()</code> method. </p>
		 */
		public static const ROUND:String = "round";
	}
}
