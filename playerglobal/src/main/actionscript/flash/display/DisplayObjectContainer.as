package flash.display {
	import flash.geom.Point;
	import flash.geom.Vector3D;
	import flash.text.TextSnapshot;
	import flash.events.Event;

	/**
	 *  The DisplayObjectContainer class is the base class for all objects that can serve as display object containers on the display list. The display list manages all objects displayed in the Flash runtimes. Use the DisplayObjectContainer class to arrange the display objects in the display list. Each DisplayObjectContainer object has its own child list for organizing the z-order of the objects. The z-order is the front-to-back order that determines which object is drawn in front, which is behind, and so on. <p>DisplayObject is an abstract base class; therefore, you cannot call DisplayObject directly. Invoking <code>new DisplayObject()</code> throws an <code>ArgumentError</code> exception.</p> The DisplayObjectContainer class is an abstract base class for all objects that can contain child objects. It cannot be instantiated directly; calling the <code>new DisplayObjectContainer()</code> constructor throws an <code>ArgumentError</code> exception. <p>For more information, see the "Display Programming" chapter of the <i>ActionScript 3.0 Developer's Guide</i>.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7dff.html" target="_blank">Adding display objects to the display list</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e26.html" target="_blank">Traversing the display list </a>
	 * </div><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e58.html" target="_blank">Display programming</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e3e.html" target="_blank">Basics of display programming</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e3c.html" target="_blank">Core display classes</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e40.html" target="_blank">Advantages of the display list approach</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e04.html" target="_blank">Improved depth management</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e02.html" target="_blank">Off-list display objects</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e3d.html" target="_blank">Working with display objects</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e36.html" target="_blank">Working with display object containers</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e13.html" target="_blank">Loading display content dynamically</a>
	 *  <br>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="DisplayObject.html" target="">flash.display.DisplayObject</a>
	 * </div><br><hr>
	 */
	public class DisplayObjectContainer extends InteractiveObject {
		private const children:Vector.<DisplayObject> = new Vector.<DisplayObject>();
		private var _mouseChildren:Boolean;
		private var _tabChildren:Boolean;

		/**
		 * <p> Calling the <code>new DisplayObjectContainer()</code> constructor throws an <code>ArgumentError</code> exception. You <i>can</i>, however, call constructors for the following subclasses of DisplayObjectContainer: </p>
		 * <ul>
		 *  <li><code>new Loader()</code></li>
		 *  <li><code>new Sprite()</code></li>
		 *  <li><code>new MovieClip()</code></li>
		 * </ul>
		 */
		public function DisplayObjectContainer() {
			super();
			this.addEventListener(flash.events.Event.ADDED_TO_STAGE, this.addedToStage);
		}

		/**
		 * <p> Determines whether or not the children of the object are mouse, or user input device, enabled. If an object is enabled, a user can interact with it by using a mouse or user input device. The default is <code>true</code>. </p>
		 * <p>This property is useful when you create a button with an instance of the Sprite class (instead of using the SimpleButton class). When you use a Sprite instance to create a button, you can choose to decorate the button by using the <code>addChild()</code> method to add additional Sprite instances. This process can cause unexpected behavior with mouse events because the Sprite instances you add as children can become the target object of a mouse event when you expect the parent instance to be the target object. To ensure that the parent instance serves as the target objects for mouse events, you can set the <code>mouseChildren</code> property of the parent instance to <code>false</code>.</p>
		 * <p> No event is dispatched by setting this property. You must use the <code>addEventListener()</code> method to create interactive functionality.</p>
		 * 
		 * @return 
		 */
		public function get mouseChildren():Boolean {
			return _mouseChildren;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set mouseChildren(value:Boolean):void {
			_mouseChildren = value;
		}

		/**
		 * <p> Returns the number of children of this object. </p>
		 * 
		 * @return 
		 */
		public function get numChildren():int {
			return children.length;
		}

		/**
		 * <p> Determines whether the children of the object are tab enabled. Enables or disables tabbing for the children of the object. The default is <code>true</code>. </p>
		 * <p><b>Note:</b> Do not use the <code>tabChildren</code> property with Flex. Instead, use the <code>mx.core.UIComponent.hasFocusableChildren</code> property.</p>
		 * 
		 * @return 
		 */
		public function get tabChildren():Boolean {
			return _tabChildren;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set tabChildren(value:Boolean):void {
			_tabChildren = value;
		}

		/**
		 * <p> Returns a TextSnapshot object for this DisplayObjectContainer instance. </p>
		 * 
		 * @return 
		 */
		public function get textSnapshot():TextSnapshot {
			throw new Error("Not implemented");
		}
		
		override protected function createSVGElement():SVGElement {
			return document.createElementNS(SVG_NAMESPACE, "g") as SVGGElement;
		}

		/**
		 * <p> Adds a child DisplayObject instance to this DisplayObjectContainer instance. The child is added to the front (top) of all other children in this DisplayObjectContainer instance. (To add a child to a specific index position, use the <code>addChildAt()</code> method.) </p>
		 * <p>If you add a child object that already has a different display object container as a parent, the object is removed from the child list of the other display object container. </p>
		 * <p><b>Note:</b> The command <code>stage.addChild()</code> can cause problems with a published SWF file, including security problems and conflicts with other loaded SWF files. There is only one Stage within a Flash runtime instance, no matter how many SWF files you load into the runtime. So, generally, objects should not be added to the Stage, directly, at all. The only object the Stage should contain is the root object. Create a DisplayObjectContainer to contain all of the items on the display list. Then, if necessary, add that DisplayObjectContainer instance to the Stage.</p>
		 * 
		 * @param child  — The DisplayObject instance to add as a child of this DisplayObjectContainer instance. 
		 * @return  — The DisplayObject instance that you pass in the  parameter. 
		 */
		public function addChild(child:DisplayObject):DisplayObject {
			return addChildAt(child, numChildren);
		}

		/**
		 * <p> Adds a child DisplayObject instance to this DisplayObjectContainer instance. The child is added at the index position specified. An index of 0 represents the back (bottom) of the display list for this DisplayObjectContainer object. </p>
		 * <p>For example, the following example shows three display objects, labeled a, b, and c, at index positions 0, 2, and 1, respectively:</p>
		 * <p><img src="../../images/DisplayObjectContainer_layers.jpg" alt="b over c over a"></p>
		 * <p>If you add a child object that already has a different display object container as a parent, the object is removed from the child list of the other display object container. </p>
		 * 
		 * @param child  — The DisplayObject instance to add as a child of this DisplayObjectContainer instance. 
		 * @param index  — The index position to which the child is added. If you specify a currently occupied index position, the child object that exists at that position and all higher positions are moved up one position in the child list. 
		 * @return  — The DisplayObject instance that you pass in the  parameter. 
		 */
		public function addChildAt(child:DisplayObject, index:int):DisplayObject {
			if (child.parent != null)
				child.parent.removeChild(child);
			index = Math.min(index, numChildren);
			this.svgElement.appendChild(child.svgElement);
			children.splice(index, 0, child);
			child.setParent(this);
			child.dispatchEvent(new flash.events.Event(flash.events.Event.ADDED));
			if (child.stage)
				child.dispatchEvent(new flash.events.Event(flash.events.Event.ADDED_TO_STAGE));
			return child;
		}
		
		private function addedToStage(e:flash.events.Event):void {
			for each (var child:DisplayObject in children) {
				child.dispatchEvent(new flash.events.Event(flash.events.Event.ADDED_TO_STAGE));
			}
		}

		/**
		 * <p> Indicates whether the security restrictions would cause any display objects to be omitted from the list returned by calling the <code>DisplayObjectContainer.getObjectsUnderPoint()</code> method with the specified <code>point</code> point. By default, content from one domain cannot access objects from another domain unless they are permitted to do so with a call to the <code>Security.allowDomain()</code> method. For more information, related to security, see the Flash Player Developer Center Topic: <a href="http://www.adobe.com/go/devnet_security_en" target="external">Security</a>. </p>
		 * <p>The <code>point</code> parameter is in the coordinate space of the Stage, which may differ from the coordinate space of the display object container (unless the display object container is the Stage). You can use the <code>globalToLocal()</code> and the <code>localToGlobal()</code> methods to convert points between these coordinate spaces.</p>
		 * 
		 * @param point  — The point under which to look. 
		 * @return  —  if the point contains child display objects with security restrictions. 
		 */
		public function areInaccessibleObjectsUnderPoint(point:Point):Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Determines whether the specified display object is a child of the DisplayObjectContainer instance or the instance itself. The search includes the entire display list including this DisplayObjectContainer instance. Grandchildren, great-grandchildren, and so on each return <code>true</code>. </p>
		 * 
		 * @param child  — The child object to test. 
		 * @return  —  if the  object is a child of the DisplayObjectContainer or the container itself; otherwise . 
		 */
		public function contains(child:DisplayObject):Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the child display object instance that exists at the specified index. </p>
		 * 
		 * @param index  — The index position of the child object. 
		 * @return  — The child display object at the specified index position. 
		 */
		public function getChildAt(index:int):DisplayObject {
			return children[index];
		}

		/**
		 * <p> Returns the child display object that exists with the specified name. If more that one child display object has the specified name, the method returns the first object in the child list. </p>
		 * <p>The <code>getChildAt()</code> method is faster than the <code>getChildByName()</code> method. The <code>getChildAt()</code> method accesses a child from a cached array, whereas the <code>getChildByName()</code> method has to traverse a linked list to access a child.</p>
		 * 
		 * @param name  — The name of the child to return. 
		 * @return  — The child display object with the specified name. 
		 */
		public function getChildByName(name:String):DisplayObject {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the index position of a <code>child</code> DisplayObject instance. </p>
		 * 
		 * @param child  — The DisplayObject instance to identify. 
		 * @return  — The index position of the child display object to identify. 
		 */
		public function getChildIndex(child:DisplayObject):int {
			var pos:int = children.indexOf(child);
			if (pos ==-1) 
				throw new ArgumentError("Le DisplayObject indiqué doit être un enfant de l'appelant.");
			return pos;
		}

		/**
		 * <p> Returns an array of objects that lie under the specified point and are children (or grandchildren, and so on) of this DisplayObjectContainer instance. Any child objects that are inaccessible for security reasons are omitted from the returned array. To determine whether this security restriction affects the returned array, call the <code>areInaccessibleObjectsUnderPoint()</code> method. </p>
		 * <p>The <code>point</code> parameter is in the coordinate space of the Stage, which may differ from the coordinate space of the display object container (unless the display object container is the Stage). You can use the <code>globalToLocal()</code> and the <code>localToGlobal()</code> methods to convert points between these coordinate spaces.</p>
		 * 
		 * @param point  — The point under which to look. 
		 * @return  — An array of objects that lie under the specified point and are children (or grandchildren, and so on) of this DisplayObjectContainer instance. 
		 */
		public function getObjectsUnderPoint(point:Point):Array {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Removes the specified <code>child</code> DisplayObject instance from the child list of the DisplayObjectContainer instance. The <code>parent</code> property of the removed child is set to <code>null</code> , and the object is garbage collected if no other references to the child exist. The index positions of any display objects above the child in the DisplayObjectContainer are decreased by 1. </p>
		 * <p>The garbage collector reallocates unused memory space. When a variable or object is no longer actively referenced or stored somewhere, the garbage collector sweeps through and wipes out the memory space it used to occupy if no other references to it exist.</p>
		 * 
		 * @param child  — The DisplayObject instance to remove. 
		 * @return  — The DisplayObject instance that you pass in the  parameter. 
		 */
		public function removeChild(child:DisplayObject):DisplayObject {
			return removeChildAt(this.getChildIndex(child));
		}

		/**
		 * <p> Removes a child DisplayObject from the specified <code>index</code> position in the child list of the DisplayObjectContainer. The <code>parent</code> property of the removed child is set to <code>null</code>, and the object is garbage collected if no other references to the child exist. The index positions of any display objects above the child in the DisplayObjectContainer are decreased by 1. </p>
		 * <p>The garbage collector reallocates unused memory space. When a variable or object is no longer actively referenced or stored somewhere, the garbage collector sweeps through and wipes out the memory space it used to occupy if no other references to it exist.</p>
		 * 
		 * @param index  — The child index of the DisplayObject to remove. 
		 * @return  — The DisplayObject instance that was removed. 
		 */
		public function removeChildAt(index:int):DisplayObject {
			if (index<0 || index>=children.length)
				throw new RangeError("L'index indiqué sort des limites.");
			var child:DisplayObject = children[index];
			this.svgElement.removeChild(child.svgElement);
			children.splice(index, 1);
			var onStage:Boolean = child.stage;
			child.setParent(null);
			child.dispatchEvent(new flash.events.Event(flash.events.Event.REMOVED));
			if (onStage)
				child.dispatchEvent(new flash.events.Event(flash.events.Event.REMOVED_FROM_STAGE));
			return child;
		}

		/**
		 * <p> Removes all <code>child</code> DisplayObject instances from the child list of the DisplayObjectContainer instance. The <code>parent</code> property of the removed children is set to <code>null</code> , and the objects are garbage collected if no other references to the children exist. </p>
		 * <p>The garbage collector reallocates unused memory space. When a variable or object is no longer actively referenced or stored somewhere, the garbage collector sweeps through and wipes out the memory space it used to occupy if no other references to it exist.</p>
		 * 
		 * @param beginIndex  — The beginning position. A value smaller than 0 throws a <code>RangeError</code>. 
		 * @param endIndex  — The ending position. A value smaller than 0 throws a <code>RangeError</code>. 
		 */
		public function removeChildren(beginIndex:int = 0, endIndex:int = 0x7fffffff):void {
			for (var i:int = beginIndex; i < endIndex && children.length>beginIndex; i++)  {
				removeChildAt(beginIndex);
			}
		}

		/**
		 * <p> Changes the position of an existing child in the display object container. This affects the layering of child objects. For example, the following example shows three display objects, labeled a, b, and c, at index positions 0, 1, and 2, respectively: </p>
		 * <p><img src="../../images/DisplayObjectContainerSetChildIndex1.jpg" alt="c over b over a"></p>
		 * <p>When you use the <code>setChildIndex()</code> method and specify an index position that is already occupied, the only positions that change are those in between the display object's former and new position. All others will stay the same. If a child is moved to an index LOWER than its current index, all children in between will INCREASE by 1 for their index reference. If a child is moved to an index HIGHER than its current index, all children in between will DECREASE by 1 for their index reference. For example, if the display object container in the previous example is named <code>container</code>, you can swap the position of the display objects labeled a and b by calling the following code:</p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>container.setChildIndex(container.getChildAt(1), 0);</pre>
		 * </div>
		 * <p>This code results in the following arrangement of objects:</p>
		 * <p><img src="../../images/DisplayObjectContainerSetChildIndex2.jpg" alt="c over a over b"></p>
		 * 
		 * @param child  — The child DisplayObject instance for which you want to change the index number. 
		 * @param index  — The resulting index number for the <code>child</code> display object. 
		 */
		public function setChildIndex(child:DisplayObject, index:int):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Recursively stops the timeline execution of all MovieClips rooted at this object. </p>
		 * <p> Child display objects belonging to a sandbox to which the excuting code does not have access are ignored.</p>
		 * <p><b>Note: </b> Streaming media playback controlled via a NetStream object will not be stopped.</p>
		 */
		public function stopAllMovieClips():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Swaps the z-order (front-to-back order) of the two specified child objects. All other child objects in the display object container remain in the same index positions. </p>
		 * 
		 * @param child1  — The first child object. 
		 * @param child2  — The second child object. 
		 */
		public function swapChildren(child1:DisplayObject, child2:DisplayObject):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Swaps the z-order (front-to-back order) of the child objects at the two specified index positions in the child list. All other child objects in the display object container remain in the same index positions. </p>
		 * 
		 * @param index1  — The index position of the first child object. 
		 * @param index2  — The index position of the second child object. 
		 */
		public function swapChildrenAt(index1:int, index2:int):void {
			throw new Error("Not implemented");
		}
	}
}
