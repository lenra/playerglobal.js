package flash.display {
	/**
	 *  Defines codes for culling algorithms that determine which triangles not to render when drawing triangle paths. <p> The terms <code>POSITIVE</code> and <code>NEGATIVE</code> refer to the sign of a triangle's normal along the z-axis. The normal is a 3D vector that is perpendicular to the surface of the triangle. </p> <p> A triangle whose vertices 0, 1, and 2 are arranged in a clockwise order has a positive normal value. That is, its normal points in a positive z-axis direction, away from the current view point. When the <code>TriangleCulling.POSITIVE</code> algorithm is used, triangles with positive normals are not rendered. Another term for this is backface culling. </p> <p> A triangle whose vertices are arranged in a counter-clockwise order has a negative normal value. That is, its normal points in a negative z-axis direction, toward the current view point. When the <code>TriangleCulling.NEGATIVE</code> algorithm is used, triangles with negative normals will not be rendered. </p> <p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://www.gamedev.net/reference/articles/article1089.asp" target="_blank">Introduction to 3D Vectors</a>
	 *  <br>
	 *  <a href="http://www.gamedev.net/reference/articles/article1088.asp" target="_blank">3D Backface Culling</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Graphics.html#drawTriangles()" target="">flash.display.Graphics.drawTriangles()</a>
	 *  <br>
	 *  <a href="GraphicsTrianglePath.html" target="">flash.display.GraphicsTrianglePath</a>
	 * </div><br><hr>
	 */
	public class TriangleCulling {
		public static const NEGATIVE:String = "negative";
		public static const NONE:String = "none";
		public static const POSITIVE:String = "positive";
	}
}
