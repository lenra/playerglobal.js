package flash.display {
	//import flash.desktop.IFilePromise;
	import flash.events.UncaughtErrorEvents;
	import flash.net.URLRequest;
	import flash.system.LoaderContext;
	import flash.utils.ByteArray;

	/**
	 *  The Loader class is used to load SWF files or image (JPG, PNG, or GIF) files. Use the <code>load()</code> method to initiate loading. The loaded display object is added as a child of the Loader object. <p>Use the URLLoader class to load text or binary data.</p> <p>The Loader class overrides the following methods that it inherits, because a Loader object can only have one child display object—the display object that it loads. Calling the following methods throws an exception: <code>addChild()</code>, <code>addChildAt()</code>, <code>removeChild()</code>, <code>removeChildAt()</code>, and <code>setChildIndex()</code>. To remove a loaded display object, you must remove the <i>Loader</i> object from its parent DisplayObjectContainer child array. </p> <p> <b>iOS notes</b> </p> <p>In AIR applications on iOS, you can only load a SWF file containing ActionScript from the application package. This restriction includes any ActionScript, such as assets with class names exported for ActionScript. For loading any SWF file, you must load the SWF using the same application domain as the parent SWF, as shown in the following example:</p> <div class="listing">
	 *  <div class="clipcopy">
	 *   <a href="#" class="copyText">Copy</a>
	 *  </div>
	 *  <pre>
	 *  var loader:Loader = new Loader();
	 *  var url:URLRequest = new URLRequest("swfs/SecondarySwf.swf");
	 *  var loaderContext:LoaderContext = new LoaderContext(false, ApplicationDomain.currentDomain, null);
	 *  loader.load(url, loaderContext);</pre>
	 * </div> <p>In addition, on iOS you can't load a SWF file that contains any ActionScript ByteCode (ABC) then unload it and reload it. If you attempt to do this, the runtime throws error 3764.</p> <p>Prior to AIR 3.6, only SWF files that do not contain ActionScript bytecode can be loaded, regardless of whether they're loaded from the application package or over a network. As an alternative to using an external SWF file with ActionScript, create a SWC library and link it in to your main SWF.</p> <p>AIR 3.7 and higher supports loading of externally hosted secondary SWFs. The detailed description about this feature can be found <a href="http://blogs.adobe.com/airodynamics/2013/03/08/external-hosting-of-secondary-swfs-for-air-apps-on-ios/" target="_new">here</a>. </p> <p>These iOS restrictions restrictions do not apply when an application is running in the iOS Simulator (ipa-test-interpreter-simulator or ipa-debug-interpreter-simulator) or interpreter mode (ipa-test-interpreter or ipa-debug-interpreter.)</p> <p> <b>Loader security</b> </p> <p>When you use the Loader class, consider the Flash Player and Adobe AIR security model: </p> <ul> 
	 *  <li>You can load content from any accessible source. </li> 
	 *  <li>Loading is not allowed if the calling SWF file is in a network sandbox and the file to be loaded is local. </li> 
	 *  <li>If the loaded content is a SWF file written with ActionScript 3.0, it cannot be cross-scripted by a SWF file in another security sandbox unless that cross-scripting arrangement was approved through a call to the <code>System.allowDomain()</code> or the <code>System.allowInsecureDomain()</code> method in the loaded content file.</li> 
	 *  <li>If the loaded content is an AVM1 SWF file (written using ActionScript 1.0 or 2.0), it cannot be cross-scripted by an AVM2 SWF file (written using ActionScript 3.0). However, you can communicate between the two SWF files by using the LocalConnection class.</li> 
	 *  <li>If the loaded content is an image, its data cannot be accessed by a SWF file outside of the security sandbox, unless the domain of that SWF file was included in a URL policy file at the origin domain of the image.</li> 
	 *  <li>Movie clips in the local-with-file-system sandbox cannot script movie clips in the local-with-networking sandbox, and the reverse is also prevented. </li> 
	 *  <li>You cannot connect to commonly reserved ports. For a complete list of blocked ports, see "Restricting Networking APIs" in the <i>ActionScript 3.0 Developer's Guide</i>. </li> 
	 * </ul> <p>However, in AIR, content in the <code>application</code> security sandbox (content installed with the AIR application) are not restricted by these security limitations.</p> <p>For more information related to security, see the Flash Player Developer Center Topic: <a href="http://www.adobe.com/go/devnet_security_en" target="external">Security</a>.</p> <p>When loading a SWF file from an untrusted source (such as a domain other than that of the Loader object's root SWF file), you may want to define a mask for the Loader object, to prevent the loaded content (which is a child of the Loader object) from drawing to portions of the Stage outside of that mask, as shown in the following code:</p> <div class="listing">
	 *  <pre>
	 *  import flash.display.*;
	 *  import flash.net.URLRequest;
	 *  var rect:Shape = new Shape();
	 *  rect.graphics.beginFill(0xFFFFFF);
	 *  rect.graphics.drawRect(0, 0, 100, 100);
	 *  rect.graphics.endFill();
	 *  addChild(rect);
	 *  var ldr:Loader = new Loader();
	 *  ldr.mask = rect;
	 *  var url:String = "http://www.unknown.example.com/content.swf";
	 *  var urlReq:URLRequest = new URLRequest(url);
	 *  ldr.load(urlReq);
	 *  addChild(ldr);
	 *  </pre>
	 *  <div class="clipcopy">
	 *   <a href="#" class="copyText">Copy</a>
	 *  </div>
	 * </div> <p>Note: App Transport Security is being introduced from Apple in iOS9, which doesn’t allow unsecure connections between App and Web services. Due to this change all the connections which are made to Unsecure web sites via Loader, URLLoader will discontinue and not work due to App Transport Security. Please specify exceptions to the default behaviour by adding keys to Info.plist in your app.</p> <p>To turn off the feature completely you can add following in your Info.plist and it will work as before.</p> <pre>
	 *      &lt;key&gt;NSAppTransportSecurity&lt;/key&gt;
	 *                &lt;dict&gt;
	 *                    &lt;key&gt;NSAllowsArbitraryLoads&lt;/key&gt;&lt;true/&gt;
	 *                &lt;/dict&gt;
	 *   </pre> <p>Please specify exceptions to the default behavior by adding keys to InfoAdditions tag of application descriptor of your app.</p> <pre>
	 *   &lt;iPhone&gt;
	 *   &lt;InfoAdditions&gt;
	 *                    &lt;![CDATA[
	 *                           &lt;key&gt;NSAppTransportSecurity&lt;/key&gt;
	 *                               &lt;dict&gt;
	 *                                         &lt;key&gt;NSExceptionDomains&lt;/key&gt;
	 *                               &lt;dict&gt;
	 *                                        &lt;key&gt;www.example.com&lt;/key&gt;
	 *                               &lt;dict&gt;
	 *                                      &lt;!--Include to allow subdomains--&gt;
	 *                                      &lt;key&gt;NSIncludesSubdomains&lt;/key&gt;
	 *                                      &lt;true/&gt;
	 *                                      &lt;!--Include to allow HTTP requests--&gt;
	 *                                      &lt;key&gt;NSTemporaryExceptionAllowsInsecureHTTPLoads&lt;/key&gt;
	 *                                      &lt;true/&gt;
	 *                                       &lt;!--Include to specify minimum TLS version--&gt;
	 *                                       &lt;key&gt;NSTemporaryExceptionMinimumTLSVersion&lt;/key&gt;
	 *                                       &lt;string&gt;TLSv1.1&lt;/string&gt;
	 *                               &lt;/dict&gt;
	 *                               &lt;/dict&gt;
	 *                               &lt;/dict&gt;
	 *                   ]]&gt;
	 *          &lt;/InfoAdditions&gt;
	 *   &lt;/iPhone&gt;
	 *   </pre> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d9e.html" target="_blank">Loading an external SWF file</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS1EFE2EDA-026D-4d14-864E-79DFD56F87C6.html" target="_blank">Restricting networking APIs</a>
	 * </div><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e58.html" target="_blank">Display programming</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e3e.html" target="_blank">Basics of display programming</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e3c.html" target="_blank">Core display classes</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e13.html" target="_blank">Loading display content dynamically</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d5b.html" target="_blank">Taking advantage of mipmapping</a>
	 *  <br>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="LoaderInfo.html" target="">flash.display.LoaderInfo</a>
	 *  <br>
	 *  <a href="../../flash/net/URLLoader.html" target="">flash.net.URLLoader</a>
	 *  <br>
	 *  <a href="DisplayObject.html" target="">flash.display.DisplayObject</a>
	 * </div><br><hr>
	 */
	public class Loader extends DisplayObjectContainer {
		private var _content:DisplayObject;
		private var _contentLoaderInfo:LoaderInfo;
		private var _uncaughtErrorEvents:UncaughtErrorEvents;

		/**
		 * <p> Creates a Loader object that you can use to load files, such as SWF, JPEG, GIF, or PNG files. Call the <code>load()</code> method to load the asset as a child of the Loader instance. You can then add the Loader object to the display list (for instance, by using the <code>addChild()</code> method of a DisplayObjectContainer instance). The asset appears on the Stage as it loads. </p>
		 * <p>You can also use a Loader instance "offlist," that is without adding it to a display object container on the display list. In this mode, the Loader instance might be used to load a SWF file that contains additional modules of an application. </p>
		 * <p>To detect when the SWF file is finished loading, you can use the events of the LoaderInfo object associated with the <code>contentLoaderInfo</code> property of the Loader object. At that point, the code in the module SWF file can be executed to initialize and start the module. In the offlist mode, a Loader instance might also be used to load a SWF file that contains components or media assets. Again, you can use the LoaderInfo object event notifications to detect when the components are finished loading. At that point, the application can start using the components and media assets in the library of the SWF file by instantiating the ActionScript 3.0 classes that represent those components and assets.</p>
		 * <p>To determine the status of a Loader object, monitor the following events that the LoaderInfo object associated with the <code>contentLoaderInfo</code> property of the Loader object:</p>
		 * <ul>
		 *  <li>The <code>open</code> event is dispatched when loading begins.</li>
		 *  <li>The <code>ioError</code> or <code>securityError</code> event is dispatched if the file cannot be loaded or if an error occured during the load process. </li>
		 *  <li>The <code>progress</code> event fires continuously while the file is being loaded.</li>
		 *  <li>The <code>complete</code> event is dispatched when a file completes downloading, but before the loaded movie clip's methods and properties are available. </li>
		 *  <li>The <code>init</code> event is dispatched after the properties and methods of the loaded SWF file are accessible, so you can begin manipulating the loaded SWF file. This event is dispatched before the <code>complete</code> handler. In streaming SWF files, the <code>init</code> event can occur significantly earlier than the <code>complete</code> event. For most purposes, use the <code>init</code> handler.</li>
		 * </ul>
		 * <p><b>Notes (iOS only):</b> In AIR applications on iOS, you can only load a SWF file containing ActionScript from the application package. This restriction includes any ActionScript, such as assets with class names exported for ActionScript. For loading any SWF file, you must load the SWF using the same application domain as the parent SWF.</p>
		 * <p>Prior to AIR 3.6, only SWF files that do not contain ActionScript bytecode can be loaded, regardless of whether they're loaded from the application package or over a network. As an alternative to using an external SWF file with ActionScript, create a SWC library and link it in to your main SWF.</p>
		 * <p>These restrictions do not apply when an application is running in the iOS Simulator (ipa-test-interpreter-simulator or ipa-debug-interpreter-simulator) or interpreter mode (ipa-test-interpreter or ipa-debug-interpreter.)</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Loader.html#load()" target="">flash.display.Loader.load()</a>
		 *  <br>
		 *  <a href="LoaderInfo.html" target="">flash.display.LoaderInfo</a>
		 * </div>
		 */
		public function Loader() {
			
		}

		/**
		 * <p> Contains the root display object of the SWF file or image (JPG, PNG, or GIF) file that was loaded by using the <code>load()</code> or <code>loadBytes()</code> methods. </p>
		 * 
		 * @return 
		 */
		public function get content():DisplayObject {
			return _content;
		}

		/**
		 * <p> Returns a LoaderInfo object corresponding to the object being loaded. LoaderInfo objects are shared between the Loader object and the loaded content object. The LoaderInfo object supplies loading progress information and statistics about the loaded file. </p>
		 * <p>Events related to the load are dispatched by the LoaderInfo object referenced by the <code>contentLoaderInfo</code> property of the Loader object. The <code>contentLoaderInfo</code> property is set to a valid LoaderInfo object, even before the content is loaded, so that you can add event listeners to the object prior to the load.</p>
		 * <p>To detect uncaught errors that happen in a loaded SWF, use the <code>Loader.uncaughtErrorEvents</code> property, not the <code>Loader.contentLoaderInfo.uncaughtErrorEvents</code> property.</p>
		 * 
		 * @return 
		 */
		public function get contentLoaderInfo():LoaderInfo {
			return _contentLoaderInfo;
		}

		/**
		 * <p> An object that dispatches an <code>uncaughtError</code> event when an unhandled error occurs in the SWF that's loaded by this Loader object. An uncaught error happens when an error is thrown outside of any <code>try..catch</code> blocks or when an ErrorEvent object is dispatched with no registered listeners. </p>
		 * <p>Note that a Loader object's <code>uncaughtErrorEvents</code> property dispatches events that bubble through it, not events that it dispatches directly. It never dispatches an <code>uncaughtErrorEvent</code> in the target phase. It only dispatches the event in the capture and bubbling phases. To detect an uncaught error in the current SWF (the SWF in which the Loader object is defined) use the <code>LoaderInfo.uncaughtErrorEvents</code> property instead.</p>
		 * <p>If the content loaded by the Loader object is an AVM1 (ActionScript 2) SWF file, uncaught errors in the AVM1 SWF file do not result in an <code>uncaughtError</code> event.</p>
		 * 
		 * @return 
		 */
		public function get uncaughtErrorEvents():UncaughtErrorEvents {
			return _uncaughtErrorEvents;
		}

		/**
		 * <p> Cancels a <code>load()</code> method operation that is currently in progress for the Loader instance. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Loader.html#load()" target="">flash.display.Loader.load()</a>
		 * </div>
		 */
		public function close():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Loads a SWF, JPEG, progressive JPEG, unanimated GIF, or PNG file into an object that is a child of this Loader object. If you load an animated GIF file, only the first frame is displayed. As the Loader object can contain only a single child, issuing a subsequent <code>load()</code> request terminates the previous request, if still pending, and commences a new load. </p>
		 * <p><b>Note</b>: In AIR 1.5 and Flash Player 10, the maximum size for a loaded image is 8,191 pixels in width or height, and the total number of pixels cannot exceed 16,777,215 pixels. (So, if an loaded image is 8,191 pixels wide, it can only be 2,048 pixels high.) In Flash Player 9 and earlier and AIR 1.1 and earlier, the limitation is 2,880 pixels in height and 2,880 pixels in width.</p>
		 * <p>A SWF file or image loaded into a Loader object inherits the position, rotation, and scale properties of the parent display objects of the Loader object. </p>
		 * <p>Use the <code>unload()</code> method to remove movies or images loaded with this method, or to cancel a load operation that is in progress.</p>
		 * <p>You can prevent a SWF file from using this method by setting the <code>allowNetworking</code> parameter of the the <code>object</code> and <code>embed</code> tags in the HTML page that contains the SWF content.</p>
		 * <p><b>iOS notes</b></p>
		 * <p>In AIR applications on iOS, you can only load a SWF file containing ActionScript from the application package. This restriction includes any ActionScript, such as assets with class names exported for ActionScript. For loading any SWF file, you must load the SWF using the same application domain as the parent SWF, as shown in the following example:</p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *      var loader:Loader = new Loader();
		 *      var url:URLRequest = new URLRequest("swfs/SecondarySwf.swf");
		 *      var loaderContext:LoaderContext = new LoaderContext(false, ApplicationDomain.currentDomain, null);
		 *      loader.load(url, loaderContext);</pre>
		 * </div>
		 * <p>In addition, on iOS you can't load a SWF file that contains any ActionScript ByteCode (ABC) then unload it and reload it. If you attempt to do this, the runtime throws error 3764.</p>
		 * <p>Prior to AIR 3.6, only SWF files that do not contain ActionScript bytecode can be loaded, regardless of whether they're loaded from the application package or over a network. As an alternative to using an external SWF file with ActionScript, create a SWC library and link it in to your main SWF.</p>
		 * <p>These restrictions do not apply when an application is running in the iOS Simulator (ipa-test-interpreter-simulator or ipa-debug-interpreter-simulator) or interpreter mode (ipa-test-interpreter or ipa-debug-interpreter.)</p>
		 * <p><b>Loader security</b></p>
		 * <p>When you use this method, consider the Flash Player security model, which is described in the Loader class description. </p>
		 * <p> In Flash Player 10 and later, if you use a multipart Content-Type (for example "multipart/form-data") that contains an upload (indicated by a "filename" parameter in a "content-disposition" header within the POST body), the POST operation is subject to the security rules applied to uploads:</p>
		 * <ul>
		 *  <li>The POST operation must be performed in response to a user-initiated action, such as a mouse click or key press.</li>
		 *  <li>If the POST operation is cross-domain (the POST target is not on the same server as the SWF file that is sending the POST request), the target server must provide a URL policy file that permits cross-domain access.</li>
		 * </ul>
		 * <p>Also, for any multipart Content-Type, the syntax must be valid (according to the RFC2046 standard). If the syntax appears to be invalid, the POST operation is subject to the security rules applied to uploads.</p>
		 * <p>For more information related to security, see the Flash Player Developer Center Topic: <a href="http://www.adobe.com/go/devnet_security_en" target="external">Security</a>.</p>
		 * 
		 * @param request  — The absolute or relative URL of the SWF, JPEG, GIF, or PNG file to be loaded. A relative path must be relative to the main SWF file. Absolute URLs must include the protocol reference, such as http:// or file:///. Filenames cannot include disk drive specifications. 
		 * @param context  — A LoaderContext object, which has properties that define the following: <ul>
		 *  <li>Whether or not to check for the existence of a policy file upon loading the object</li>
		 *  <li>The ApplicationDomain for the loaded object</li>
		 *  <li>The SecurityDomain for the loaded object</li>
		 *  <li>The ImageDecodingPolicy for the loaded image object</li>
		 * </ul> <p>If the <code>context</code> parameter is not specified or refers to a null object, the loaded content remains in its own security domain.</p> <p><b>iOS only</b>: When calling the <code>load()</code> method in AIR for iOS, the LoaderContext instance must specify the main application domain (<code>ApplicationDomain.currentDomain</code>).</p> <p>For complete details, see the description of the properties in the <a href="../system/LoaderContext.html">LoaderContext</a> class.</p> 
		 */
		public function load(request:URLRequest, context:LoaderContext = null):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Loads from binary data stored in a ByteArray object. </p>
		 * <p>The <code>loadBytes()</code> method is asynchronous. You must wait for the "init" event before accessing the properties of a loaded object.</p>
		 * <p>When you use this method, consider the Flash Player security model, which is described in the Loader class description. </p>
		 * <p><b>Note (iOS only):</b> In AIR applications on iOS, you can only load a SWF file containing ActionScript from the application package. This restriction includes any ActionScript, such as assets with class names exported for ActionScript. For loading any SWF file, you must load the SWF using the same application domain as the parent SWF.</p>
		 * <p>Prior to AIR 3.6, calling this method has no effect on iOS.</p>
		 * 
		 * @param bytes  — A ByteArray object. The contents of the ByteArray can be any of the file formats supported by the Loader class: SWF, GIF, JPEG, or PNG. 
		 * @param context  — A LoaderContext object. Only the <code>applicationDomain</code> property of the LoaderContext object applies; the <code>checkPolicyFile</code> and <code>securityDomain</code> properties of the LoaderContext object do not apply. <p>If the <code>context</code> parameter is not specified or refers to a null object, the content is loaded into the current security domain— a process referred to as "import loading" in Flash Player security documentation. Specifically, if the loading SWF file trusts the remote SWF by incorporating the remote SWF into its code, then the loading SWF can import it directly into its own security domain.</p> <p>For more information related to security, see the Flash Player Developer Center Topic: <a href="http://www.adobe.com/go/devnet_security_en" target="external">Security</a>.</p> 
		 */
		public function loadBytes(bytes:ByteArray, context:LoaderContext = null):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Loads an IFilePromise instance. </p>
		 * <p>The <code>loadFilePromise</code> method takes an <code>IFilePromise</code> object and loads the binary data. If the data is a progressive stream, such as a video wait for the "init" or progress events before accessing the properties of the loaded object. Otherwise, wait for the complete event to make sure that the data is fully loaded.</p>
		 * <p>When you use this method, consider the Flash Player security model, which is described in the Loader class description. </p>
		 * 
		 * @param promise  — An IFilePromise object. The data source of the object can be any of the file formats supported by the Loader class: SWF, GIF, JPEG, or PNG. 
		 * @param context  — A LoaderContext object. Only the <code>applicationDomain</code> property of the LoaderContext object applies; the <code>checkPolicyFile</code> and <code>securityDomain</code> properties of the LoaderContext object do not apply. <p>If the <code>context</code> parameter is not specified or refers to a null object, the content is loaded into the current security domain— a process referred to as "import loading" in Flash Player security documentation. Specifically, if the loading SWF file trusts the remote SWF by incorporating the remote SWF into its code, then the loading SWF can import it directly into its own security domain.</p> <p>For more information related to security, see the Flash Player Developer Center Topic: <a href="http://www.adobe.com/go/devnet_security_en" target="external">Security</a>.</p> 
		 */
		/*public function loadFilePromise(promise:IFilePromise, context:LoaderContext = null):void {
			throw new Error("Not implemented");
		}*/

		/**
		 * <p> Removes a child of this Loader object that was loaded by using the <code>load()</code> method. The <code>property</code> of the associated LoaderInfo object is reset to <code>null</code>. The child is not necessarily destroyed because other objects might have references to it; however, it is no longer a child of the Loader object. </p>
		 * <p>When you call the <code>unload()</code> method, the Loader object's <code>contentLoaderInfo</code> property is set to <code>null</code>. Any visual assets that were loaded with the SWF are unloaded and removed from memory. ActionScript class definitions in the loaded SWF remain in memory, and code in the same application domain as the loaded SWF can access instances of those classes and create new instances.</p>
		 * <p><b>Note (iOS only)</b>: Prior to AIR 3.6, this method has no effect on iOS.</p>
		 * <p>As a best practice, before you unload a child SWF file, you should explicitly close any streams in the child SWF file's objects, such as LocalConnection, NetConnection, NetStream, and Sound objects. Otherwise, audio in the child SWF file might continue to play, even though the child SWF file was unloaded. To close streams in the child SWF file, add an event listener to the child that listens for the <code>unload</code> event. When the parent calls <code>Loader.unload()</code>, the <code>unload</code> event is dispatched to the child. The following code shows how you might do this:</p>
		 * <pre>
		 * function closeAllStreams(evt:Event) { 
		 *     myNetStream.close();
		 *     mySound.close();
		 *     myNetConnection.close();
		 *     myLocalConnection.close();
		 * }
		 * 
		 * myMovieClip.loaderInfo.addEventListener(Event.UNLOAD, closeAllStreams);</pre>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Loader.html#load()" target="">Loader.load()</a>
		 *  <br>
		 *  <a href="../../flash/media/Sound.html#close()" target="">flash.media.Sound.close()</a>
		 *  <br>
		 *  <a href="../../flash/net/LocalConnection.html#close()" target="">flash.net.LocalConnection.close()</a>
		 *  <br>
		 *  <a href="../../flash/net/NetConnection.html#close()" target="">flash.net.NetConnection.close()</a>
		 *  <br>
		 *  <a href="../../flash/net/NetStream.html#close()" target="">flash.net.NetStream.close()</a>
		 *  <br>
		 *  <a href="../../operators.html#delete" target="">delete operator</a>
		 * </div>
		 */
		public function unload():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Attempts to unload child SWF file contents and stops the execution of commands from loaded SWF files. This method attempts to unload SWF files that were loaded using <code>Loader.load()</code> or <code>Loader.loadBytes()</code> by removing references to EventDispatcher, NetConnection, Timer, Sound, or Video objects of the child SWF file. As a result, the following occurs for the child SWF file and the child SWF file's display list: </p>
		 * <ul>
		 *  <li>Sounds are stopped.</li>
		 *  <li>Stage event listeners are removed.</li>
		 *  <li>Event listeners for <code>enterFrame</code>, <code>frameConstructed</code>, <code>exitFrame</code>, <code>activate</code> and <code>deactivate</code> are removed.</li>
		 *  <li>Timers are stopped.</li>
		 *  <li>Camera and Microphone instances are detached</li>
		 *  <li>Movie clips are stopped.</li>
		 * </ul>
		 * <p>When you call the <code>unloadAndStop()</code> method, the Loader object's <code>contentLoaderInfo</code> property is set to <code>null</code>. Any visual assets that were loaded with the SWF are unloaded and removed from memory. ActionScript class definitions in the loaded SWF remain in memory, and code in the same application domain as the loaded SWF can access instances of those classes and create new instances.</p>
		 * <p><b>Note (iOS only)</b>: Prior to AIR 3.6, this method has no effect on iOS.</p>
		 * 
		 * @param gc  — Provides a hint to the garbage collector to run on the child SWF objects (<code>true</code>) or not (<code>false</code>). If you are unloading many objects asynchronously, setting the <code>gc</code> paramter to <code>false</code> might improve application performance. However, if the parameter is set to <code>false</code>, media and display objects of the child SWF file might persist in memory after running the <code>unloadAndStop()</code> command. 
		 */
		public function unloadAndStop(gc:Boolean = true):void {
			throw new Error("Not implemented");
		}
	}
}
