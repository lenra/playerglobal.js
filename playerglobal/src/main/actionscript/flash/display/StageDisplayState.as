package flash.display {
	/**
	 *  The StageDisplayState class provides values for the <code>Stage.displayState</code> property. <p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS2E9C7F3B-6A7C-4c5d-8ADD-5B23446FBEEB.html" target="_blank">Working with full-screen mode</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Stage.html#displayState" target="">flash.display.Stage.displayState</a>
	 * </div><br><hr>
	 */
	public class StageDisplayState {
		/**
		 * <p> Specifies that the Stage is in full-screen mode. In this mode, keyboard interactivity is enabled for mobile devices. </p>
		 */
		public static const FULL_SCREEN:String = "fullScreen";
		/**
		 * <p> Specifies that the Stage is in full-screen mode with keyboard interactivity enabled. <span>As of Flash Player 11.3, this capability is supported in both AIR applications and browser-based applications.</span> </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Stage.html#allowsFullScreenInteractive" target="">Stage.allowsFullScreenInteractive</a>
		 * </div>
		 */
		public static const FULL_SCREEN_INTERACTIVE:String = "fullScreenInteractive";
		/**
		 * <p> Specifies that the Stage is in normal mode. </p>
		 */
		public static const NORMAL:String = "normal";
	}
}
