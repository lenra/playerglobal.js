package flash.display {
	/**
	 *  The JPEGXREncoderOptions class defines a compression algorithm for the <code>flash.display.BitmapData.encode()</code> method. <p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS4768145595f94108-17913eb4136eaab51c7-8000.html" target="_blank">Compressing bitmap data</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="BitmapData.html" target="">flash.display.BitmapData</a>
	 *  <br>
	 *  <a href="BitmapData.html#encode()" target="">flash.display.BitmapData.encode()</a>
	 *  <br>
	 *  <a href="BitmapEncodingColorSpace.html" target="">flash.display.BitmapEncodingColorSpace</a>
	 * </div><br><hr>
	 */
	public class JPEGXREncoderOptions {
		private var _colorSpace:String;
		private var _quantization:uint;
		private var _trimFlexBits:uint;

		/**
		 * <p> Creates a JPEGEXREncoderOptions object with the specified settings. </p>
		 * 
		 * @param quantization  — The amount of lossy in the compression. 
		 * @param colorSpace  — Specifies how color channels are sampled. 
		 * @param trimFlexBits  — Determines the amount of extra entropy data that is cut after quantization. 
		 */
		public function JPEGXREncoderOptions(quantization:uint = 20, colorSpace:String = "auto", trimFlexBits:uint = 0) {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Specifies how color channels are sampled. For more information, see flash.display.BitmapEncodingColorSpace. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="BitmapEncodingColorSpace.html" target="">flash.display.BitmapEncodingColorSpace</a>
		 * </div>
		 * 
		 * @return 
		 */
		public function get colorSpace():String {
			return _colorSpace;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set colorSpace(value:String):void {
			_colorSpace = value;
		}

		/**
		 * <p> Specifies the amount of lossy in the compression. The range of values is 0 to 100, where a value of 0 means lossless compression. Larger values increase the lossy value and the resultant image becomes more grainy. A common value is 10. For values of 20 or larger, the image can become very grainy. </p>
		 * 
		 * @return 
		 */
		public function get quantization():uint {
			return _quantization;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set quantization(value:uint):void {
			_quantization = value;
		}

		/**
		 * <p> Determines the amount of extra entropy data that is cut after quantization. This property can affect image quality, and is typically left at the default value. </p>
		 * 
		 * @return 
		 */
		public function get trimFlexBits():uint {
			return _trimFlexBits;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set trimFlexBits(value:uint):void {
			_trimFlexBits = value;
		}
	}
}
