package flash.display {
	/**
	 *  The ColorCorrection class provides values for the <code>flash.display.Stage.colorCorrection</code> property. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Stage.html#colorCorrection" target="">flash.display.Stage.colorCorrection</a>
	 * </div><br><hr>
	 */
	public class ColorCorrection {
		/**
		 * <p> Uses the host's default color correction. For the web player the host is usually a browser, and Flash Player tries to use the same color correction as the web page hosting the SWF file. </p>
		 */
		public static const DEFAULT:String = "default";
		/**
		 * <p> Turns off color correction regardless of the player host environment. This setting provides faster performance. </p>
		 */
		public static const OFF:String = "off";
		/**
		 * <p> Turns on color correction regardless of the player host environment, if available. </p>
		 */
		public static const ON:String = "on";
	}
}
