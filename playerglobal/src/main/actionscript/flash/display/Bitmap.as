package flash.display {
	/**
	 *  The Bitmap class represents display objects that represent bitmap images. These can be images that you load with the flash.display.Loader class, or they can be images that you create with the <code>Bitmap()</code> constructor. <p>The <code>Bitmap()</code> constructor allows you to create a Bitmap object that contains a reference to a BitmapData object. After you create a Bitmap object, use the <code>addChild()</code> or <code>addChildAt()</code> method of the parent DisplayObjectContainer instance to place the bitmap on the display list.</p> <p>A Bitmap object can share its BitmapData reference among several Bitmap objects, independent of translation or rotation properties. Because you can create multiple Bitmap objects that reference the same BitmapData object, multiple display objects can use the same complex BitmapData object without incurring the memory overhead of a BitmapData object for each display object instance.</p> <p>A BitmapData object can be drawn to the screen by a Bitmap object in one of two ways: by using the vector renderer as a fill-bitmap shape, or by using a faster pixel-copying routine. The pixel-copying routine is substantially faster than the vector renderer, but the Bitmap object must meet certain conditions to use it:</p> <ul> 
	 *  <li> No stretching, rotation, or skewing can be applied to the Bitmap object.</li> 
	 *  <li> No color transform can be applied to the Bitmap object. </li> 
	 *  <li> No blend mode can be applied to the Bitmap object. </li> 
	 *  <li> No clipping can be done through mask layers or <code>setMask()</code> methods. </li> 
	 *  <li> The image itself cannot be a mask. </li> 
	 *  <li> The destination coordinates must be on a whole pixel boundary. </li> 
	 * </ul> <p>If you load a Bitmap object from a domain other than that of the Loader object used to load the image, and there is no URL policy file that permits access to the domain of the Loader object, then a script in that domain cannot access the Bitmap object or its properties and methods. For more information, see the Flash Player Developer Center Topic: <a href="http://www.adobe.com/go/devnet_security_en" target="external">Security</a>.</p> <p> <b>Note:</b> The Bitmap class is not a subclass of the InteractiveObject class, so it cannot dispatch mouse events. However, you can use the <code>addEventListener()</code> method of the display object container that contains the Bitmap object.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d64.html" target="_blank">Manipulating pixels</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d60.html" target="_blank">Copying bitmap data</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d63.html" target="_blank">Making textures with noise functions</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d62.html" target="_blank">Scrolling bitmaps</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d61.html" target="_blank">Bitmap example: Animated spinning moon</a>
	 * </div><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e58.html" target="_blank">Display programming</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e3e.html" target="_blank">Basics of display programming</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e3c.html" target="_blank">Core display classes</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7dfa.html" target="_blank">Choosing a DisplayObject subclass</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e1b.html" target="_blank">Working with bitmaps</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d66.html" target="_blank">Basics of working with bitmaps</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d65.html" target="_blank">The Bitmap and BitmapData classes</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS52621785137562065a8e668112d98c8c4df-8000.html" target="_blank">Asynchronous decoding of bitmap images</a>
	 *  <br>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Loader.html" target="">flash.display.Loader</a>
	 *  <br>
	 *  <a href="BitmapData.html" target="">flash.display.BitmapData</a>
	 * </div><br><hr>
	 */
	public class Bitmap extends DisplayObject {
		private var _bitmapData:BitmapData;
		private var _pixelSnapping:String;
		private var _smoothing:Boolean;

		/**
		 * <p> Initializes a Bitmap object to refer to the specified BitmapData object. </p>
		 * 
		 * @param bitmapData  — The BitmapData object being referenced. 
		 * @param pixelSnapping  — Whether or not the Bitmap object is snapped to the nearest pixel. 
		 * @param smoothing  — Whether or not the bitmap is smoothed when scaled. For example, the following examples show the same bitmap scaled by a factor of 3, with <code>smoothing</code> set to <code>false</code> (left) and <code>true</code> (right): <p> </p><table class="+ topic/table adobe-d/adobetable ">
		 *  <tbody>
		 *   <tr>
		 *    <td align="center"><img src="../../images/bitmap_smoothing_off.jpg" alt="A bitmap without smoothing."></td>
		 *    <td align="center"><img src="../../images/bitmap_smoothing_on.jpg" alt="A bitmap with smoothing."></td>
		 *   </tr>
		 *  </tbody>
		 * </table> <p></p> 
		 */
		public function Bitmap(bitmapData:BitmapData = null, pixelSnapping:String = "auto", smoothing:Boolean = false) {
			this.bitmapData = bitmapData;
			this.pixelSnapping = pixelSnapping;
			this.smoothing = smoothing;
		}

		/**
		 * <p> The BitmapData object being referenced. </p>
		 * 
		 * @return 
		 */
		public function get bitmapData():BitmapData {
			return _bitmapData;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set bitmapData(value:BitmapData):void {
			_bitmapData = value;
		}

		/**
		 * <p> Controls whether or not the Bitmap object is snapped to the nearest pixel. The PixelSnapping class includes possible values: </p>
		 * <ul>
		 *  <li><code>PixelSnapping.NEVER</code>—No pixel snapping occurs.</li>
		 *  <li><code>PixelSnapping.ALWAYS</code>—The image is always snapped to the nearest pixel, independent of transformation.</li>
		 *  <li><code>PixelSnapping.AUTO</code>—The image is snapped to the nearest pixel if it is drawn with no rotation or skew and it is drawn at a scale factor of 99.9% to 100.1%. If these conditions are satisfied, the bitmap image is drawn at 100% scale, snapped to the nearest pixel. Internally, this value allows the image to be drawn as fast as possible using the vector renderer.</li>
		 * </ul>
		 * 
		 * @return 
		 */
		public function get pixelSnapping():String {
			return _pixelSnapping;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set pixelSnapping(value:String):void {
			_pixelSnapping = value;
		}

		/**
		 * <p> Controls whether or not the bitmap is smoothed when scaled. If <code>true</code>, the bitmap is smoothed when scaled. If <code>false</code>, the bitmap is not smoothed when scaled. </p>
		 * 
		 * @return 
		 */
		public function get smoothing():Boolean {
			return _smoothing;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set smoothing(value:Boolean):void {
			_smoothing = value;
		}
	}
}
