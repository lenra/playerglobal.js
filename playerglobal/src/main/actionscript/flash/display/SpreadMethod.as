package flash.display {
	/**
	 *  The SpreadMethod class provides values for the <code>spreadMethod</code> parameter in the <code>beginGradientFill()</code> and <code>lineGradientStyle()</code> methods of the Graphics class. <p>The following example shows the same gradient fill using various spread methods:</p> <table class="+ topic/table adobe-d/adobetable ">
	 *  <tbody>
	 *   <tr>
	 *    <td align="center"> <img src="../../images/beginGradientFill_spread_pad.jpg" alt="linear gradient with SpreadMethod.PAD"> </td>
	 *    <td align="center"> <img src="../../images/beginGradientFill_spread_reflect.jpg" alt="linear gradient with SpreadMethod.REFLECT"> </td>
	 *    <td align="center"> <img src="../../images/beginGradientFill_spread_repeat.jpg" alt="linear gradient with SpreadMethod.REPEAT"> </td>
	 *   </tr>
	 *   <tr>
	 *    <td align="center"> <code>SpreadMethod.PAD</code> </td>
	 *    <td align="center"> <code>SpreadMethod.REFLECT</code> </td>
	 *    <td align="center"> <code>SpreadMethod.REPEAT</code> </td>
	 *   </tr>
	 *  </tbody>
	 * </table> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Graphics.html#beginGradientFill()" target="">flash.display.Graphics.beginGradientFill()</a>
	 *  <br>
	 *  <a href="Graphics.html#lineGradientStyle()" target="">flash.display.Graphics.lineGradientStyle()</a>
	 * </div><br><hr>
	 */
	public class SpreadMethod {
		/**
		 * <p> Specifies that the gradient use the <i>pad</i> spread method. </p>
		 */
		public static const PAD:String = "pad";
		/**
		 * <p> Specifies that the gradient use the <i>reflect</i> spread method. </p>
		 */
		public static const REFLECT:String = "reflect";
		/**
		 * <p> Specifies that the gradient use the <i>repeat</i> spread method. </p>
		 */
		public static const REPEAT:String = "repeat";
	}
}
