package flash.display {
	/**
	 *  A collection of drawing commands and the coordinate parameters for those commands. <p> Use a GraphicsPath object with the <code>Graphics.drawGraphicsData()</code> method. Drawing a GraphicsPath object is the equivalent of calling the <code>Graphics.drawPath()</code> method. </p> <p>The GraphicsPath class also has its own set of methods (<code>curveTo()</code>, <code>lineTo()</code>, <code>moveTo()</code> <code>wideLineTo()</code> and <code>wideMoveTo()</code>) similar to those in the Graphics class for making adjustments to the <code>GraphicsPath.commands</code> and <code>GraphicsPath.data</code> vector arrays.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Graphics.html#drawGraphicsData()" target="">flash.display.Graphics.drawGraphicsData()</a>
	 *  <br>
	 *  <a href="Graphics.html#drawPath()" target="">flash.display.Graphics.drawPath()</a>
	 * </div><br><hr>
	 */
	public class GraphicsPath implements IGraphicsPath, IGraphicsData {
		private var _commands:Vector.<int>;
		private var _data:Vector.<Number>;
		private var _winding:String;

		/**
		 * <p> Creates a new GraphicsPath object. </p>
		 * 
		 * @param commands  — A Vector of integers representing commands defined by the GraphicsPathCommand class. 
		 * @param data  — A Vector of Numbers where each pair of numbers is treated as a point (an x, y pair). 
		 * @param winding  — Specifies the winding rule using a value defined in the GraphicsPathWinding class. 
		 */
		public function GraphicsPath(commands:Vector.<int> = null, data:Vector.<Number> = null, winding:String = "evenOdd") {
			this.commands = commands;
			this.data = data;
			this.winding = winding;
		}

		/**
		 * <p> The Vector of drawing commands as integers representing the path. Each command can be one of the values defined by the GraphicsPathCommand class. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="GraphicsPathCommand.html" target="">flash.display.GraphicsPathCommand</a>
		 * </div>
		 * 
		 * @return 
		 */
		public function get commands():Vector.<int> {
			if (_commands == null)
				_commands = new Vector.<int>();
			return _commands;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set commands(value:Vector.<int>):void {
			_commands = value;
		}

		/**
		 * <p> The Vector of Numbers containing the parameters used with the drawing commands. </p>
		 * 
		 * @return 
		 */
		public function get data():Vector.<Number> {
			if (_data == null)
				_data = new Vector.<Number>();
			return _data;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set data(value:Vector.<Number>):void {
			_data = value;
		}

		/**
		 * <p> Specifies the winding rule using a value defined in the GraphicsPathWinding class. </p>
		 * 
		 * @return 
		 */
		public function get winding():String {
			return _winding;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set winding(value:String):void {
			_winding = value;
		}

		/**
		 * <p> Adds a new "cubicCurveTo" command to the <code>commands</code> vector and new coordinates to the <code>data</code> vector. </p>
		 * 
		 * @param controlX1  — A number that specifies the horizontal position of the first control point relative to the registration point of the parent display object. 
		 * @param controlY1  — A number that specifies the vertical position of the first control point relative to the registration point of the parent display object. 
		 * @param controlX2  — A number that specifies the horizontal position of the second control point relative to the registration point of the parent display object. 
		 * @param controlY2  — A number that specifies the vertical position of the second control point relative to the registration point of the parent display object. 
		 * @param anchorX  — A number that specifies the horizontal position of the next anchor point relative to the registration point of the parent display object. 
		 * @param anchorY  — A number that specifies the vertical position of the next anchor point relative to the registration point of the parent display object. 
		 */
		public function cubicCurveTo(controlX1:Number, controlY1:Number, controlX2:Number, controlY2:Number, anchorX:Number, anchorY:Number):void {
			commands.push(GraphicsPathCommand.CUBIC_CURVE_TO, GraphicsPathCommand.NO_OP, GraphicsPathCommand.NO_OP, GraphicsPathCommand.NO_OP, GraphicsPathCommand.NO_OP, GraphicsPathCommand.NO_OP);
			data.push(controlX1, controlY1, controlX2, controlY2, anchorX, anchorY);
		}

		/**
		 * <p> Adds a new "curveTo" command to the <code>commands</code> vector and new coordinates to the <code>data</code> vector. </p>
		 * 
		 * @param controlX  — A number that specifies the horizontal position of the control point relative to the registration point of the parent display object. 
		 * @param controlY  — A number that specifies the vertical position of the control point relative to the registration point of the parent display object. 
		 * @param anchorX  — A number that specifies the horizontal position of the next anchor point relative to the registration point of the parent display object. 
		 * @param anchorY  — A number that specifies the vertical position of the next anchor point relative to the registration point of the parent display object. 
		 */
		public function curveTo(controlX:Number, controlY:Number, anchorX:Number, anchorY:Number):void {
			commands.push(GraphicsPathCommand.CURVE_TO, GraphicsPathCommand.NO_OP, GraphicsPathCommand.NO_OP, GraphicsPathCommand.NO_OP);
			data.push(controlX, controlY, anchorX, anchorY);
		}
		
		public function draw(el:Element):void {
			var p:String = "";
			for (var i:int = 0; i < commands.length; i++) {
				switch (commands[i]) {
					case GraphicsPathCommand.CUBIC_CURVE_TO:
						p += "C"+data[i]+","+data[i + 1]+" "+data[i + 2]+","+data[i + 3]+" "+data[i + 4]+","+data[i + 5];
						break;
					case GraphicsPathCommand.CURVE_TO:
						p += "Q"+data[i]+","+data[i + 1]+" "+data[i + 2]+","+data[i + 3];
						break;
					case GraphicsPathCommand.LINE_TO:
						p += "L"+data[i]+","+data[i + 1];
						break;
					case GraphicsPathCommand.MOVE_TO:
						p += "M"+data[i]+","+data[i + 1];
						break;
					case GraphicsPathCommand.NO_OP:
						break;
					case GraphicsPathCommand.WIDE_LINE_TO:
						throw new Error("Not implemented");
						break;
					case GraphicsPathCommand.WIDE_MOVE_TO:
						throw new Error("Not implemented");
						break;
				}
				p += " ";
			}
			el.setAttribute("d", p);
		}

		/**
		 * <p> Adds a new "lineTo" command to the <code>commands</code> vector and new coordinates to the <code>data</code> vector. </p>
		 * 
		 * @param x  — The x coordinate of the destination point for the line. 
		 * @param y  — The y coordinate of the destination point for the line. 
		 */
		public function lineTo(x:Number, y:Number):void {
			commands.push(GraphicsPathCommand.LINE_TO, GraphicsPathCommand.NO_OP);
			data.push(x, y);
		}

		/**
		 * <p> Adds a new "moveTo" command to the <code>commands</code> vector and new coordinates to the <code>data</code> vector. </p>
		 * 
		 * @param x  — The x coordinate of the destination point. 
		 * @param y  — The y coordinate of the destination point. 
		 */
		public function moveTo(x:Number, y:Number):void {
			commands.push(GraphicsPathCommand.MOVE_TO, GraphicsPathCommand.NO_OP);
			data.push(x, y);
		}

		/**
		 * <p> Adds a new "wideLineTo" command to the <code>commands</code> vector and new coordinates to the <code>data</code> vector. </p>
		 * 
		 * @param x  — The x-coordinate of the destination point for the line. 
		 * @param y  — The y-coordinate of the destination point for the line. 
		 */
		public function wideLineTo(x:Number, y:Number):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Adds a new "wideMoveTo" command to the <code>commands</code> vector and new coordinates to the <code>data</code> vector. </p>
		 * 
		 * @param x  — The x-coordinate of the destination point. 
		 * @param y  — The y-coordinate of the destination point. 
		 */
		public function wideMoveTo(x:Number, y:Number):void {
			throw new Error("Not implemented");
		}
	}
}
