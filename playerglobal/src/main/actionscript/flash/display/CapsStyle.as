package flash.display {
	/**
	 *  The CapsStyle class is an enumeration of constant values that specify the caps style to use in drawing lines. The constants are provided for use as values in the <code>caps</code> parameter of the <code>flash.display.Graphics.lineStyle()</code> method. You can specify the following three types of caps: <p> <img src="../../images/linecap.jpg" alt="The three types of caps: NONE, ROUND, and SQUARE."> </p> <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Graphics.html#lineStyle()" target="">flash.display.Graphics.lineStyle()</a>
	 * </div><br><hr>
	 */
	public class CapsStyle {
		/**
		 * <p> Used to specify no caps in the <code>caps</code> parameter of the <code>flash.display.Graphics.lineStyle()</code> method. </p>
		 */
		public static const NONE:String = "none";
		/**
		 * <p> Used to specify round caps in the <code>caps</code> parameter of the <code>flash.display.Graphics.lineStyle()</code> method. </p>
		 */
		public static const ROUND:String = "round";
		/**
		 * <p> Used to specify square caps in the <code>caps</code> parameter of the <code>flash.display.Graphics.lineStyle()</code> method. </p>
		 */
		public static const SQUARE:String = "square";
	}
}
