package flash.display {
	/**
	 *  A ShaderInput instance represents a single input image for a shader kernel. A kernel can be defined to accept zero, one, or more source images that are used in the kernel execution. A ShaderInput instance provides a mechanism for specifying the input image that is used when the shader executes. To specify a value for the input, create a BitmapData, ByteArray, or Vector.&lt;Number&gt; instance containing the image data and assign it to the <code>input</code> property. <p>The ShaderInput instance representing a Shader instance's input image is accessed as a property of the Shader instance's <code>data</code> property. The ShaderInput property has the same name as the input's name in the shader code. For example, if a shader defines an input named <code>src</code>, the ShaderInput instance representing the <code>src</code> input is available as the <code>src</code> property, as this example shows:</p> <div class="listing">
	 *  <div class="clipcopy">
	 *   <a href="#" class="copyText">Copy</a>
	 *  </div>
	 *  <pre>myShader.data.src.input = new BitmapData(50, 50, true, 0xFF990000);</pre>
	 * </div> <p>For some uses of a Shader instance, you do not need to specify an input image, because it is automatically specified by the operation. You only need to specify an input when a Shader is used for the following:</p> <ul> 
	 *  <li>Shader fill</li> 
	 *  <li>ShaderFilter, only for the second or additional inputs if the shader is defined to use more than one input. (The object to which the filter is applied is automatically used as the first input.)</li> 
	 *  <li>Shader blend mode, only for the third or additional inputs if the shader is defined to use more than two inputs. (The objects being blended are automatically used as the first and second inputs.)</li> 
	 *  <li>ShaderJob background execution</li> 
	 * </ul> <p>If the shader is being executed using a ShaderJob instance to process a ByteArray containing a linear array of data, set the ShaderInput instance's <code>height</code> to 1 and <code>width</code> to the number of 32-bit floating point values in the ByteArray. In that case, the input in the shader must be defined with the <code>image1</code> data type.</p> <p>Generally, developer code does not create a ShaderInput instance directly. A ShaderInput instance is created for each of a shader's inputs when the Shader instance is created.</p> <p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS9FE4B9C5-45D4-481d-8FB5-C68F3AC8034C.html" target="_blank">Specifying shader input values</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="ShaderData.html" target="">flash.display.ShaderData</a>
	 *  <br>
	 *  <a href="Shader.html#data" target="">flash.display.Shader.data</a>
	 *  <br>
	 *  <a href="ShaderJob.html" target="">flash.display.ShaderJob</a>
	 * </div><br><hr>
	 */
	public class ShaderInput {
		private var _height:int;
		private var _input:Object;
		private var _width:int;

		private var _channels:int;
		private var _index:int;

		/**
		 * <p> Creates a ShaderInput instance. Developer code does not call the ShaderInput constructor directly. A ShaderInput instance is created for each of a shader's inputs when the Shader instance is created. </p>
		 */
		public function ShaderInput() {
			throw new Error("Not implemented");
		}

		/**
		 * <p> The number of channels that a shader input expects. This property must be accounted for when the input data is a ByteArray or Vector.&lt;Number&gt; instance. </p>
		 * 
		 * @return 
		 */
		public function get channels():int {
			return _channels;
		}

		/**
		 * <p> The height of the shader input. This property is only used when the input data is a ByteArray or Vector.&lt;Number&gt; instance. When the input is a BitmapData instance the height is automatically determined. </p>
		 * 
		 * @return 
		 */
		public function get height():int {
			return _height;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set height(value:int):void {
			_height = value;
		}

		/**
		 * <p> The zero-based index of the input in the shader, indicating the order of the input definitions in the shader. </p>
		 * 
		 * @return 
		 */
		public function get index():int {
			return _index;
		}

		/**
		 * <p> The input data that is used when the shader executes. This property can be a BitmapData instance, a ByteArray instance, or a Vector.&lt;Number&gt; instance. </p>
		 * <p>If a ByteArray value is assigned to the <code>input</code> property, the following conditions must be met:</p>
		 * <ul>
		 *  <li>The <code>height</code> and <code>width</code> properties must be set.</li>
		 *  <li>The byte array's contents must only consist of 32-bit floating-point values. These values can be written using the <code>ByteArray.writeFloat()</code> method.</li>
		 *  <li>The total length in bytes of the ByteArray must be exactly <code>width</code> times <code>height</code> times <code>channels</code> times 4.</li>
		 *  <li>The byte array's <code>endian</code> property must be <code>Endian.LITTLE_ENDIAN</code>.</li>
		 * </ul>
		 * <p>If a Vector.&lt;Number&gt; instance is assigned to the <code>input</code> property, the length of the Vector must be equal to <code>width</code> times <code>height</code> times <code>channels</code>.</p>
		 * 
		 * @return 
		 */
		public function get input():Object {
			return _input;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set input(value:Object):void {
			_input = value;
		}

		/**
		 * <p> The width of the shader input. This property is only used when the input data is a ByteArray or Vector.&lt;Number&gt; instance. When the input is a BitmapData instance the width is automatically determined. </p>
		 * 
		 * @return 
		 */
		public function get width():int {
			return _width;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set width(value:int):void {
			_width = value;
		}
	}
}
