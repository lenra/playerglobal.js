package flash.display {
	/**
	 *  This class is used to create lightweight shapes using the ActionScript drawing application program interface (API). The Shape class includes a <code>graphics</code> property, which lets you access methods from the Graphics class. <p>The Sprite class also includes a <code>graphics</code>property, and it includes other features not available to the Shape class. For example, a Sprite object is a display object container, whereas a Shape object is not (and cannot contain child display objects). For this reason, Shape objects consume less memory than Sprite objects that contain the same graphics. However, a Sprite object supports user input events, while a Shape object does not.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e58.html" target="_blank">Display programming</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e3e.html" target="_blank">Basics of display programming</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e3c.html" target="_blank">Core display classes</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7dce.html" target="_blank">Basics of the drawing API</a>
	 *  <br>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Graphics.html" target="">flash.display.Graphics</a>
	 *  <br>
	 *  <a href="Sprite.html" target="">flash.display.Sprite</a>
	 * </div><br><hr>
	 */
	public class Shape extends DisplayObject {
		private var _graphics:Graphics;

		/**
		 * <p> Creates a new Shape object. </p>
		 */
		public function Shape() {
			super();
		}
		
		override protected function createSVGElement():SVGElement {
			return document.createElementNS(SVG_NAMESPACE, "g") as SVGGElement;
		}

		/**
		 * <p> Specifies the Graphics object belonging to this Shape object, where vector drawing commands can occur. </p>
		 * 
		 * @return 
		 */
		public function get graphics():Graphics {
			if (!_graphics) {
				_graphics = new Graphics();
				_graphics.svgParent = this.svgElement;
			}
			return _graphics;
		}
	}
}
