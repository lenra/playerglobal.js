package flash.display {
	import flash.accessibility.AccessibilityImplementation;
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.IMEEvent;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	//import flash.events.NativeDragEvent;
	import flash.events.SoftKeyboardEvent;
	//import flash.events.TapGestureEvent;
	import flash.events.TextEvent;
	import flash.events.TouchEvent;
	import flash.events.TransformGestureEvent;
	import flash.geom.Rectangle;

	[Event(name="clear", type="flash.events.Event")]
	[Event(name="click", type="flash.events.MouseEvent")]
	[Event(name="contextMenu", type="flash.events.MouseEvent")]
	[Event(name="copy", type="flash.events.Event")]
	[Event(name="cut", type="flash.events.Event")]
	[Event(name="doubleClick", type="flash.events.MouseEvent")]
	[Event(name="focusIn", type="flash.events.FocusEvent")]
	[Event(name="focusOut", type="flash.events.FocusEvent")]
	[Event(name="gestureDirectionalTap", type="flash.events.TransformGestureEvent")]
	[Event(name="gestureLongPress", type="flash.events.TapGestureEvent")]
	[Event(name="gesturePan", type="flash.events.TransformGestureEvent")]
	[Event(name="gestureRotate", type="flash.events.TransformGestureEvent")]
	[Event(name="gestureSwipe", type="flash.events.TransformGestureEvent")]
	[Event(name="gestureTap", type="flash.events.TapGestureEvent")]
	[Event(name="gestureZoom", type="flash.events.TransformGestureEvent")]
	[Event(name="imeStartComposition", type="flash.events.IMEEvent")]
	[Event(name="keyDown", type="flash.events.KeyboardEvent")]
	[Event(name="keyFocusChange", type="flash.events.FocusEvent")]
	[Event(name="keyUp", type="flash.events.KeyboardEvent")]
	[Event(name="middleClick", type="flash.events.MouseEvent")]
	[Event(name="middleMouseDown", type="flash.events.MouseEvent")]
	[Event(name="middleMouseUp", type="flash.events.MouseEvent")]
	[Event(name="mouseDown", type="flash.events.MouseEvent")]
	[Event(name="mouseFocusChange", type="flash.events.FocusEvent")]
	[Event(name="mouseMove", type="flash.events.MouseEvent")]
	[Event(name="mouseOut", type="flash.events.MouseEvent")]
	[Event(name="mouseOver", type="flash.events.MouseEvent")]
	[Event(name="mouseUp", type="flash.events.MouseEvent")]
	[Event(name="mouseWheel", type="flash.events.MouseEvent")]
	[Event(name="nativeDragComplete", type="flash.events.NativeDragEvent")]
	[Event(name="nativeDragDrop", type="flash.events.NativeDragEvent")]
	[Event(name="nativeDragEnter", type="flash.events.NativeDragEvent")]
	[Event(name="nativeDragExit", type="flash.events.NativeDragEvent")]
	[Event(name="nativeDragOver", type="flash.events.NativeDragEvent")]
	[Event(name="nativeDragStart", type="flash.events.NativeDragEvent")]
	[Event(name="nativeDragUpdate", type="flash.events.NativeDragEvent")]
	[Event(name="paste", type="flash.events.Event")]
	[Event(name="proximityBegin", type="flash.events.TouchEvent")]
	[Event(name="proximityEnd", type="flash.events.TouchEvent")]
	[Event(name="proximityMove", type="flash.events.TouchEvent")]
	[Event(name="proximityOut", type="flash.events.TouchEvent")]
	[Event(name="proximityOver", type="flash.events.TouchEvent")]
	[Event(name="proximityRollOut", type="flash.events.TouchEvent")]
	[Event(name="proximityRollOver", type="flash.events.TouchEvent")]
	[Event(name="releaseOutside", type="flash.events.MouseEvent")]
	[Event(name="rightClick", type="flash.events.MouseEvent")]
	[Event(name="rightMouseDown", type="flash.events.MouseEvent")]
	[Event(name="rightMouseUp", type="flash.events.MouseEvent")]
	[Event(name="rollOut", type="flash.events.MouseEvent")]
	[Event(name="rollOver", type="flash.events.MouseEvent")]
	[Event(name="selectAll", type="flash.events.Event")]
	[Event(name="softKeyboardActivate", type="flash.events.SoftKeyboardEvent")]
	[Event(name="softKeyboardActivating", type="flash.events.SoftKeyboardEvent")]
	[Event(name="softKeyboardDeactivate", type="flash.events.SoftKeyboardEvent")]
	[Event(name="tabChildrenChange", type="flash.events.Event")]
	[Event(name="tabEnabledChange", type="flash.events.Event")]
	[Event(name="tabIndexChange", type="flash.events.Event")]
	[Event(name="textInput", type="flash.events.TextEvent")]
	[Event(name="touchBegin", type="flash.events.TouchEvent")]
	[Event(name="touchEnd", type="flash.events.TouchEvent")]
	[Event(name="touchMove", type="flash.events.TouchEvent")]
	[Event(name="touchOut", type="flash.events.TouchEvent")]
	[Event(name="touchOver", type="flash.events.TouchEvent")]
	[Event(name="touchRollOut", type="flash.events.TouchEvent")]
	[Event(name="touchRollOver", type="flash.events.TouchEvent")]
	[Event(name="touchTap", type="flash.events.TouchEvent")]
	/**
	 *  The InteractiveObject class is the abstract base class for all display objects with which the user can interact, using the mouse, keyboard, or other user input device. <p>You cannot instantiate the InteractiveObject class directly. A call to the <code>new InteractiveObject()</code> constructor throws an <code>ArgumentError</code> exception.</p> <p>The InteractiveObject class itself does not include any APIs for rendering content onscreen. To create a custom subclass of the InteractiveObject class, extend one of the subclasses that do have APIs for rendering content onscreen, such as the Sprite, SimpleButton, TextField, or MovieClip classes.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/mobileapps/WS19f279b149e7481c-4cb7e4a013321c24dab-8000.html" target="_blank">Open a soft keyboard in a mobile Flex application</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e58.html" target="_blank">Display programming</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e3e.html" target="_blank">Basics of display programming</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e3c.html" target="_blank">Core display classes</a>
	 *  <br>
	 * </div><br><hr>
	 */
	public class InteractiveObject extends DisplayObject {
		private var _accessibilityImplementation:AccessibilityImplementation;
		//private var _contextMenu:NativeMenu;
		private var _doubleClickEnabled:Boolean;
		private var _focusRect:Object;
		private var _mouseEnabled:Boolean;
		private var _needsSoftKeyboard:Boolean;
		private var _softKeyboard:String;
		private var _softKeyboardInputAreaOfInterest:Rectangle;
		private var _tabEnabled:Boolean;
		private var _tabIndex:int;

		/**
		 * <p> Calling the <code>new InteractiveObject()</code> constructor throws an <code>ArgumentError</code> exception. You can, however, call constructors for the following subclasses of InteractiveObject: </p>
		 * <ul>
		 *  <li><code>new SimpleButton()</code></li>
		 *  <li><code>new TextField()</code></li>
		 *  <li><code>new Loader()</code></li>
		 *  <li><code>new Sprite()</code></li>
		 *  <li><code>new MovieClip()</code></li>
		 * </ul>
		 */
		/*public function InteractiveObject() {
			super();
		}*/

		/**
		 * <p> The current accessibility implementation (AccessibilityImplementation) for this InteractiveObject instance. </p>
		 * 
		 * @return 
		 */
		public function get accessibilityImplementation():AccessibilityImplementation {
			return _accessibilityImplementation;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set accessibilityImplementation(value:AccessibilityImplementation):void {
			_accessibilityImplementation = value;
		}

		/**
		 * <p> Specifies the context menu associated with this object. </p>
		 * <p>For content running in Flash Player, this property is a ContextMenu object. In the AIR runtime, the ContextMenu class extends the NativeMenu class, however Flash Player only supports the ContextMenu class, not the NativeMenu class. </p>
		 * <p><b>Note:</b> TextField objects always include a clipboard menu in the context menu. The clipboard menu contains Cut, Copy, Paste, Clear, and Select All commands. You cannot remove these commands from the context menu for TextField objects. For TextField objects, selecting these commands (or their keyboard equivalents) does not generate <code>clear</code>, <code>copy</code>, <code>cut</code>, <code>paste</code>, or <code>selectAll</code> events.</p>
		 * 
		 * @return 
		 */
		/*public function get contextMenu():NativeMenu {
			return _contextMenu;
		}*/

		/**
		 * @param value
		 * @return 
		 */
		/*public function set contextMenu(value:NativeMenu):void {
			_contextMenu = value;
		}*/

		/**
		 * <p> Specifies whether the object receives <code>doubleClick</code> events. The default value is <code>false</code>, which means that by default an InteractiveObject instance does not receive <code>doubleClick</code> events. If the <code>doubleClickEnabled</code> property is set to <code>true</code>, the instance receives <code>doubleClick</code> events within its bounds. The <code>mouseEnabled</code> property of the InteractiveObject instance must also be set to <code>true</code> for the object to receive <code>doubleClick</code> events. </p>
		 * <p>No event is dispatched by setting this property. You must use the <code>addEventListener()</code> method to add an event listener for the <code>doubleClick</code> event.</p>
		 * 
		 * @return 
		 */
		public function get doubleClickEnabled():Boolean {
			return _doubleClickEnabled;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set doubleClickEnabled(value:Boolean):void {
			_doubleClickEnabled = value;
		}

		/**
		 * <p> Specifies whether this object displays a focus rectangle. It can take one of three values: <code>true</code>, <code>false</code>, or <code>null</code>. Values of <code>true</code> and <code>false</code> work as expected, specifying whether or not the focus rectangle appears. A value of <code>null</code> indicates that this object obeys the <code>stageFocusRect</code> property of the Stage. </p>
		 * 
		 * @return 
		 */
		public function get focusRect():Object {
			return _focusRect;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set focusRect(value:Object):void {
			_focusRect = value;
		}

		/**
		 * <p> Specifies whether this object receives mouse, or other user input, messages. The default value is <code>true</code>, which means that by default any InteractiveObject instance that is on the display list receives mouse events or other user input events. If <code>mouseEnabled</code> is set to <code>false</code>, the instance does not receive any mouse events (or other user input events like keyboard events). Any children of this instance on the display list are not affected. To change the <code>mouseEnabled</code> behavior for all children of an object on the display list, use <code>flash.display.DisplayObjectContainer.mouseChildren</code>. </p>
		 * <p> No event is dispatched by setting this property. You must use the <code>addEventListener()</code> method to create interactive functionality.</p>
		 * 
		 * @return 
		 */
		public function get mouseEnabled():Boolean {
			return _mouseEnabled;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set mouseEnabled(value:Boolean):void {
			_mouseEnabled = value;
		}

		/**
		 * <p> Specifies whether a virtual keyboard (an on-screen, software keyboard) should display when this InteractiveObject instance receives focus. </p>
		 * <p>By default, the value is <code>false</code> and focusing an InteractiveObject instance does not raise a soft keyboard. If the <code>needsSoftKeyboard</code> property is set to <code>true</code>, the runtime raises a soft keyboard when the InteractiveObject instance is ready to accept user input. An InteractiveObject instance is ready to accept user input after a programmatic call to set the Stage <code>focus</code> property or a user interaction, such as a "tap." If the client system has a hardware keyboard available or does not support virtual keyboards, then the soft keyboard is not raised.</p>
		 * <p>The InteractiveObject instance dispatches <code>softKeyboardActivating</code>, <code>softKeyboardActivate</code>, and <code>softKeyboardDeactivate</code> events when the soft keyboard raises and lowers.</p>
		 * <p><b>Note:</b> This property is not supported in AIR applications on iOS.</p>
		 * 
		 * @return 
		 */
		public function get needsSoftKeyboard():Boolean {
			return _needsSoftKeyboard;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set needsSoftKeyboard(value:Boolean):void {
			_needsSoftKeyboard = value;
		}

		/**
		 * <p> Controls the appearance of the soft keyboard. </p>
		 * <p> Devices with soft keyboards can customize the keyboard's buttons to match the type of input expected. For example, if numeric input is expected, a device can use <code>SoftKeyboardType.NUMBER</code> to display only numbers on the soft keyboard. Valid values are defined as constants in the SoftKeyboardType class: </p>
		 * <ul>
		 *  <li> "default" </li>
		 *  <li> "punctuation" </li>
		 *  <li> "url" </li>
		 *  <li> "number" </li>
		 *  <li> "contact" </li>
		 *  <li> "email" </li>
		 *  <li> "phone" </li>
		 *  <li> "decimalpad" </li>
		 * </ul>
		 * <p> These values serve as hints, to help a device display the best keyboard for the current operation. </p>
		 * <p> The default value is <code>SoftKeyboardType.DEFAULT.</code></p>
		 * 
		 * @return 
		 */
		public function get softKeyboard():String {
			return _softKeyboard;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set softKeyboard(value:String):void {
			_softKeyboard = value;
		}

		/**
		 * <p> Defines the area that should remain on-screen when a soft keyboard is displayed (not available on iOS). </p>
		 * <p>If the <code>needsSoftKeyboard</code> property of this InteractiveObject is <code>true</code>, then the runtime adjusts the display as needed to keep the object in view while the user types. Ordinarily, the runtime uses the object bounds obtained from the <code>DisplayObject.getBounds()</code> method. You can specify a different area using this <code>softKeyboardInputAreaOfInterest</code> property.</p>
		 * <p>Specify the <code>softKeyboardInputAreaOfInterest</code> in stage coordinates.</p>
		 * <p><b>Note:</b> On Android, the <code>softKeyboardInputAreaOfInterest</code> is not respected in landscape orientations.</p>
		 * <p><b>Note:</b> <code>softKeyboardInputAreaOfInterest</code> is not supported on iOS.</p>
		 * 
		 * @return 
		 */
		public function get softKeyboardInputAreaOfInterest():Rectangle {
			return _softKeyboardInputAreaOfInterest;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set softKeyboardInputAreaOfInterest(value:Rectangle):void {
			_softKeyboardInputAreaOfInterest = value;
		}

		/**
		 * <p> Specifies whether this object is in the tab order. If this object is in the tab order, the value is <code>true</code>; otherwise, the value is <code>false</code>. By default, the value is <code>false</code>, except for the following: </p>
		 * <ul>
		 *  <li>For a SimpleButton object, the value is <code>true</code>.</li>
		 *  <li>For a TextField object with <code>type = "input"</code>, the value is <code>true</code>.</li>
		 *  <li>For a Sprite object or MovieClip object with <code>buttonMode = true</code>, the value is <code>true</code>.</li>
		 * </ul>
		 * 
		 * @return 
		 */
		public function get tabEnabled():Boolean {
			return _tabEnabled;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set tabEnabled(value:Boolean):void {
			_tabEnabled = value;
		}

		/**
		 * <p> Specifies the tab ordering of objects in a SWF file. The <code>tabIndex</code> property is -1 by default, meaning no tab index is set for the object. </p>
		 * <p>If any currently displayed object in the SWF file contains a <code>tabIndex</code> property, automatic tab ordering is disabled, and the tab ordering is calculated from the <code>tabIndex</code> properties of objects in the SWF file. The custom tab ordering includes only objects that have <code>tabIndex</code> properties.</p>
		 * <p>The <code>tabIndex</code> property can be a non-negative integer. The objects are ordered according to their <code>tabIndex</code> properties, in ascending order. An object with a <code>tabIndex</code> value of 1 precedes an object with a <code>tabIndex</code> value of 2. Do not use the same <code>tabIndex</code> value for multiple objects.</p>
		 * <p>The custom tab ordering that the <code>tabIndex</code> property defines is <i>flat</i>. This means that no attention is paid to the hierarchical relationships of objects in the SWF file. All objects in the SWF file with <code>tabIndex</code> properties are placed in the tab order, and the tab order is determined by the order of the <code>tabIndex</code> values. </p>
		 * <p><b>Note:</b> To set the tab order for TLFTextField instances, cast the display object child of the TLFTextField as an InteractiveObject, then set the <code>tabIndex</code> property. For example: </p>
		 * <pre>
		 * 	 InteractiveObject(tlfInstance.getChildAt(1)).tabIndex = 3;
		 * 	 </pre>
		 * <code>tlfInstance1</code>
		 * <code>tlfInstance2</code>
		 * <code>tlfInstance3</code>
		 * <pre>
		 * 	 InteractiveObject(tlfInstance1.getChildAt(1)).tabIndex = 3;
		 * 	 InteractiveObject(tlfInstance2.getChildAt(1)).tabIndex = 2;
		 * 	 InteractiveObject(tlfInstance3.getChildAt(1)).tabIndex = 1;
		 * 	 </pre>
		 * 
		 * @return 
		 */
		public function get tabIndex():int {
			return _tabIndex;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set tabIndex(value:int):void {
			_tabIndex = value;
		}

		/**
		 * <p> Raises a virtual keyboard. </p>
		 * <p>Calling this method focuses the InteractiveObject instance and raises the soft keyboard, if necessary. The <code>needsSoftKeyboard</code> must also be <code>true</code>. A keyboard is not raised if a hardware keyboard is available, or if the client system does not support virtual keyboards.</p>
		 * <p><b>Note:</b> This method is not supported in AIR applications on iOS.</p>
		 * 
		 * @return  — A value of  means that the soft keyboard request was granted;  means that the soft keyboard was not raised. 
		 */
		public function requestSoftKeyboard():Boolean {
			throw new Error("Not implemented");
		}
	}
}
