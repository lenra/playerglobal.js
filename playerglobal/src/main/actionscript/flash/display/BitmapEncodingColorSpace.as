package flash.display {
	/**
	 *  The BitmapEncodingColorSpace class defines the constants that specify how color channels are sampled by the <code>flash.display.BitmapData.encode()</code> method when specifying the compressor as flash.display.JPEGXREncoderOptions. <p>For more information on these constants, see <a href="http://en.wikipedia.org/wiki/Chroma_subsampling">http://en.wikipedia.org/wiki/Chroma_subsampling</a>.</p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="BitmapData.html" target="">flash.display.BitmapData</a>
	 *  <br>
	 *  <a href="BitmapData.html#encode()" target="">flash.display.BitmapData.encode()</a>
	 *  <br>
	 *  <a href="JPEGXREncoderOptions.html" target="">flash.display.JPEGXREncoderOptions</a>
	 * </div><br><hr>
	 */
	public class BitmapEncodingColorSpace {
		/**
		 * <p> Specifies a subsampling scheme of 4:2:0. </p>
		 */
		public static const COLORSPACE_4_2_0:String = "4:2:0";
		/**
		 * <p> Specifies a subsampling scheme of 4:2:2. </p>
		 */
		public static const COLORSPACE_4_2_2:String = "4:2:2";
		/**
		 * <p> Specifies a subsampling scheme of 4:4:4. </p>
		 */
		public static const COLORSPACE_4_4_4:String = "4:4:4";
		/**
		 * <p> Specifies a subsampling scheme of auto. The auto scheme depends on the image that you are compressing, but has a default of 4:2:0. </p>
		 */
		public static const COLORSPACE_AUTO:String = "auto";
	}
}
