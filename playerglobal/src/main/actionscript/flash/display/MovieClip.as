package flash.display {
	/**
	 *  The MovieClip class inherits from the following classes: Sprite, DisplayObjectContainer, InteractiveObject, DisplayObject, and EventDispatcher. <p>Unlike the Sprite object, a MovieClip object has a timeline.</p> <p>&gt;In Flash Professional, the methods for the MovieClip class provide the same functionality as actions that target movie clips. Some additional methods do not have equivalent actions in the Actions toolbox in the Actions panel in the Flash authoring tool. </p> <p>Children instances placed on the Stage in Flash Professional cannot be accessed by code from within the constructor of a parent instance since they have not been created at that point in code execution. Before accessing the child, the parent must instead either create the child instance by code or delay access to a callback function that listens for the child to dispatch its <code>Event.ADDED_TO_STAGE</code> event.</p> <p>If you modify any of the following properties of a MovieClip object that contains a motion tween, the playhead is stopped in that MovieClip object: <code>alpha</code>, <code>blendMode</code>, <code>filters</code>, <code>height</code>, <code>opaqueBackground</code>, <code>rotation</code>, <code>scaleX</code>, <code>scaleY</code>, <code>scale9Grid</code>, <code>scrollRect</code>, <code>transform</code>, <code>visible</code>, <code>width</code>, <code>x</code>, or <code>y</code>. However, it does not stop the playhead in any child MovieClip objects of that MovieClip object.</p> <p> <b>Note:</b>Flash Lite 4 supports the MovieClip.opaqueBackground property only if FEATURE_BITMAPCACHE is defined. The default configuration of Flash Lite 4 does not define FEATURE_BITMAPCACHE. To enable the MovieClip.opaqueBackground property for a suitable device, define FEATURE_BITMAPCACHE in your project.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b8ea63-7ff3.html" target="_blank">Working with MovieClip objects</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d9d.html" target="_blank">Controlling movie clip playback</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b8ea63-7ffa.html" target="_blank">Playing movie clips and stopping playback</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b8ea63-7ffd.html" target="_blank">Fast-forwarding and rewinding</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b8ea63-7fe9.html" target="_blank">Jumping to a different frame and using frame labels</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b8ea63-7ff4.html" target="_blank">Working with scenes</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7fd2.html" target="_blank">Creating MovieClip objects with ActionScript</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d9e.html" target="_blank">Loading an external SWF file</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d9a.html" target="_blank">Movie clip example: RuntimeAssetsExplorer</a>
	 * </div><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e58.html" target="_blank">Display programming</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e3e.html" target="_blank">Basics of display programming</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e3c.html" target="_blank">Core display classes</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e1d.html" target="_blank">Working with movie clips</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d9c.html" target="_blank">Basics of movie clips</a>
	 *  <br>
	 * </div><br><hr>
	 */
	public class MovieClip extends Sprite {
		private var _enabled:Boolean;
		private var _trackAsMenu:Boolean;

		private var _currentFrame:int;
		private var _currentFrameLabel:String;
		private var _currentLabel:String;
		private var _currentLabels:Array;
		private var _currentScene:Scene;
		private var _framesLoaded:int;
		private var _isPlaying:Boolean;
		private var _scenes:Array;
		private var _totalFrames:int;

		/**
		 * <p> Creates a new MovieClip instance. After creating the MovieClip, call the <code>addChild()</code> or <code>addChildAt()</code> method of a display object container that is onstage. </p>
		 */
		public function MovieClip() {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Specifies the number of the frame in which the playhead is located in the timeline of the MovieClip instance. If the movie clip has multiple scenes, this value is the frame number in the current scene. </p>
		 * 
		 * @return 
		 */
		public function get currentFrame():int {
			return _currentFrame;
		}

		/**
		 * <p> The label at the current frame in the timeline of the MovieClip instance. If the current frame has no label, <code>currentLabel</code> is <code>null</code>. </p>
		 * 
		 * @return 
		 */
		public function get currentFrameLabel():String {
			return _currentFrameLabel;
		}

		/**
		 * <p> The current label in which the playhead is located in the timeline of the MovieClip instance. If the current frame has no label, <code>currentLabel</code> is set to the name of the previous frame that includes a label. If the current frame and previous frames do not include a label, <code>currentLabel</code> returns <code>null</code>. </p>
		 * 
		 * @return 
		 */
		public function get currentLabel():String {
			return _currentLabel;
		}

		/**
		 * <p> Returns an array of FrameLabel objects from the current scene. If the MovieClip instance does not use scenes, the array includes all frame labels from the entire MovieClip instance. </p>
		 * 
		 * @return 
		 */
		public function get currentLabels():Array {
			return _currentLabels;
		}

		/**
		 * <p> The current scene in which the playhead is located in the timeline of the MovieClip instance. </p>
		 * 
		 * @return 
		 */
		public function get currentScene():Scene {
			return _currentScene;
		}

		/**
		 * <p> A Boolean value that indicates whether a movie clip is enabled. The default value of <code>enabled</code> is <code>true</code>. If <code>enabled</code> is set to <code>false</code>, the movie clip's Over, Down, and Up frames are disabled. The movie clip continues to receive events (for example, <code>mouseDown</code>, <code>mouseUp</code>, <code>keyDown</code>, and <code>keyUp</code>). </p>
		 * <p>The <code>enabled</code> property governs only the button-like properties of a movie clip. You can change the <code>enabled</code> property at any time; the modified movie clip is immediately enabled or disabled. If <code>enabled</code> is set to <code>false</code>, the object is not included in automatic tab ordering.</p>
		 * 
		 * @return 
		 */
		public function get enabled():Boolean {
			return _enabled;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set enabled(value:Boolean):void {
			_enabled = value;
		}

		/**
		 * <p> The number of frames that are loaded from a streaming SWF file. You can use the <code>framesLoaded</code> property to determine whether the contents of a specific frame and all the frames before it loaded and are available locally in the browser. You can also use it to monitor the downloading of large SWF files. For example, you might want to display a message to users indicating that the SWF file is loading until a specified frame in the SWF file finishes loading. </p>
		 * <p>If the movie clip contains multiple scenes, the <code>framesLoaded</code> property returns the number of frames loaded for <i>all</i> scenes in the movie clip.</p>
		 * 
		 * @return 
		 */
		public function get framesLoaded():int {
			return _framesLoaded;
		}

		/**
		 * <p> A Boolean value that indicates whether a movie clip is curently playing. </p>
		 * 
		 * @return 
		 */
		public function get isPlaying():Boolean {
			return _isPlaying;
		}

		/**
		 * <p> An array of Scene objects, each listing the name, the number of frames, and the frame labels for a scene in the MovieClip instance. </p>
		 * 
		 * @return 
		 */
		public function get scenes():Array {
			return _scenes;
		}

		/**
		 * <p> The total number of frames in the MovieClip instance. </p>
		 * <p>If the movie clip contains multiple frames, the <code>totalFrames</code> property returns the total number of frames in <i>all</i> scenes in the movie clip.</p>
		 * 
		 * @return 
		 */
		public function get totalFrames():int {
			return _totalFrames;
		}

		/**
		 * <p> Indicates whether other display objects that are SimpleButton or MovieClip objects can receive mouse release events or other user input release events. The <code>trackAsMenu</code> property lets you create menus. You can set the <code>trackAsMenu</code> property on any SimpleButton or MovieClip object. The default value of the <code>trackAsMenu</code> property is <code>false</code>. </p>
		 * <p>You can change the <code>trackAsMenu</code> property at any time; the modified movie clip immediately uses the new behavior.</p>
		 * 
		 * @return 
		 */
		public function get trackAsMenu():Boolean {
			return _trackAsMenu;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set trackAsMenu(value:Boolean):void {
			_trackAsMenu = value;
		}

		/**
		 * <p> Starts playing the SWF file at the specified frame. This happens after all remaining actions in the frame have finished executing. To specify a scene as well as a frame, specify a value for the <code>scene</code> parameter. </p>
		 * 
		 * @param frame  — A number representing the frame number, or a string representing the label of the frame, to which the playhead is sent. If you specify a number, it is relative to the scene you specify. If you do not specify a scene, the current scene determines the global frame number to play. If you do specify a scene, the playhead jumps to the frame number in the specified scene. 
		 * @param scene  — The name of the scene to play. This parameter is optional. 
		 */
		public function gotoAndPlay(frame:Object, scene:String = null):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Brings the playhead to the specified frame of the movie clip and stops it there. This happens after all remaining actions in the frame have finished executing. If you want to specify a scene in addition to a frame, specify a <code>scene</code> parameter. </p>
		 * 
		 * @param frame  — A number representing the frame number, or a string representing the label of the frame, to which the playhead is sent. If you specify a number, it is relative to the scene you specify. If you do not specify a scene, the current scene determines the global frame number at which to go to and stop. If you do specify a scene, the playhead goes to the frame number in the specified scene and stops. 
		 * @param scene  — The name of the scene. This parameter is optional. 
		 */
		public function gotoAndStop(frame:Object, scene:String = null):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sends the playhead to the next frame and stops it. This happens after all remaining actions in the frame have finished executing. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="MovieClip.html#prevFrame()" target="">prevFrame()</a>
		 * </div>
		 * <span id="pageFilter"><br><span class="label"> Example &nbsp;( <span class="usage"><a href="http://www.adobe.com/go/learn_as3_usingexamples_en"> How to use this example </a></span>) </span><br><br>
		 *  <div class="detailBody">
		 *    In the following example, two SimpleButton objects control the timeline. The 
		 *   <code>prev</code> button moves the playhead to the previous frame, and the 
		 *   <code>nextBtn</code> button moves the playhead to the next frame: 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>
		 * 
		 * import flash.events.MouseEvent;
		 * 
		 * mc1.stop();
		 * prevBtn.addEventListener(MouseEvent.CLICK, goBack);
		 * nextBtn.addEventListener(MouseEvent.CLICK, goForward);
		 * 
		 * function goBack(event:MouseEvent):void {
		 *     mc1.prevFrame();
		 * }
		 * 
		 * function goForward(event:MouseEvent):void {
		 *     mc1.nextFrame();
		 * }
		 * </pre>
		 *   </div>
		 *  </div></span>
		 */
		public function nextFrame():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Moves the playhead to the next scene of the MovieClip instance. This happens after all remaining actions in the frame have finished executing. </p>
		 * <span id="pageFilter"><br><span class="label"> Example &nbsp;( <span class="usage"><a href="http://www.adobe.com/go/learn_as3_usingexamples_en"> How to use this example </a></span>) </span><br><br>
		 *  <div class="detailBody">
		 *    In the following example, two SimpleButton objects control the timeline. The 
		 *   <code>prevBtn</code> button moves the playhead to the previous scene, and the 
		 *   <code>nextBtn</code> button moves the playhead to the next scene: 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>
		 * 
		 * import flash.events.MouseEvent;
		 * 
		 * mc1.stop();
		 * prevBtn.addEventListener(MouseEvent.CLICK, goBack);
		 * nextBtn.addEventListener(MouseEvent.CLICK, goForward);
		 * 
		 * function goBack(event:MouseEvent):void {
		 *     mc1.prevScene();
		 * }
		 * 
		 * function goForward(event:MouseEvent):void {
		 *     mc1.nextScene();
		 * }
		 * </pre>
		 *   </div>
		 *  </div></span>
		 */
		public function nextScene():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Moves the playhead in the timeline of the movie clip. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="MovieClip.html#gotoAndPlay()" target="">gotoAndPlay()</a>
		 * </div>
		 * <span id="pageFilter"><br><span class="label"> Example &nbsp;( <span class="usage"><a href="http://www.adobe.com/go/learn_as3_usingexamples_en"> How to use this example </a></span>) </span><br><br>
		 *  <div class="detailBody">
		 *    The following code uses the 
		 *   <code>stop()</code> method to stop a movie clip named 
		 *   <code>mc1</code> and to resume playing when the user clicks the text field named 
		 *   <code>continueText</code>: 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>
		 * 
		 * import flash.text.TextField;
		 * import flash.events.MouseEvent;
		 * 
		 * var continueText:TextField = new TextField();
		 * continueText.text = "Play movie...";
		 * addChild(continueText);
		 * 
		 * mc1.stop();
		 * continueText.addEventListener(MouseEvent.CLICK, resumeMovie);
		 * 
		 * function resumeMovie(event:MouseEvent):void {
		 *     mc1.play();
		 * }
		 * </pre>
		 *   </div>
		 *  </div></span>
		 */
		public function play():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sends the playhead to the previous frame and stops it. This happens after all remaining actions in the frame have finished executing. </p>
		 * <span id="pageFilter"><br><span class="label"> Example &nbsp;( <span class="usage"><a href="http://www.adobe.com/go/learn_as3_usingexamples_en"> How to use this example </a></span>) </span><br><br>
		 *  <div class="detailBody">
		 *    In the following example, two SimpleButton objects control the timeline. The 
		 *   <code>prev</code> button moves the playhead to the previous frame, and the 
		 *   <code>nextBtn</code> button moves the playhead to the next frame: 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>
		 * 
		 * import flash.events.MouseEvent;
		 * 
		 * mc1.stop();
		 * prevBtn.addEventListener(MouseEvent.CLICK, goBack);
		 * nextBtn.addEventListener(MouseEvent.CLICK, goForward);
		 * 
		 * function goBack(event:MouseEvent):void {
		 *     mc1.prevFrame();
		 * }
		 * 
		 * function goForward(event:MouseEvent):void {
		 *     mc1.nextFrame();
		 * }
		 * </pre>
		 *   </div>
		 *  </div></span>
		 */
		public function prevFrame():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Moves the playhead to the previous scene of the MovieClip instance. This happens after all remaining actions in the frame have finished executing. </p>
		 * <span id="pageFilter"><br><span class="label"> Example &nbsp;( <span class="usage"><a href="http://www.adobe.com/go/learn_as3_usingexamples_en"> How to use this example </a></span>) </span><br><br>
		 *  <div class="detailBody">
		 *    In the following example, two SimpleButton objects control the timeline. The 
		 *   <code>prevBtn</code> button moves the playhead to the previous scene, and the 
		 *   <code>nextBtn</code> button moves the playhead to the next scene: 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>
		 * 
		 * import flash.events.MouseEvent;
		 * 
		 * mc1.stop();
		 * prevBtn.addEventListener(MouseEvent.CLICK, goBack);
		 * nextBtn.addEventListener(MouseEvent.CLICK, goForward);
		 * 
		 * function goBack(event:MouseEvent):void {
		 *     mc1.prevScene();
		 * }
		 * 
		 * function goForward(event:MouseEvent):void {
		 *     mc1.nextScene();
		 * }
		 * </pre>
		 *   </div>
		 *  </div></span>
		 */
		public function prevScene():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Stops the playhead in the movie clip. </p>
		 */
		public function stop():void {
			throw new Error("Not implemented");
		}
	}
}
