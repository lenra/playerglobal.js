package flash.display {
	/**
	 *  The InterpolationMethod class provides values for the <code>interpolationMethod</code> parameter in the <code>Graphics.beginGradientFill()</code> and <code>Graphics.lineGradientStyle()</code> methods. This parameter determines the RGB space to use when rendering the gradient. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Graphics.html#beginGradientFill()" target="">flash.display.Graphics.beginGradientFill()</a>
	 *  <br>
	 *  <a href="Graphics.html#lineGradientStyle()" target="">flash.display.Graphics.lineGradientStyle()</a>
	 * </div><br><hr>
	 */
	public class InterpolationMethod {
		/**
		 * <p> Specifies that the linear RGB interpolation method should be used. This means that an RGB color space based on a linear RGB color model is used. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="InterpolationMethod.html#RGB" target="">RGB</a>
		 * </div>
		 */
		public static const LINEAR_RGB:String = "linearRGB";
		/**
		 * <p> Specifies that the RGB interpolation method should be used. This means that the gradient is rendered with exponential sRGB (standard RGB) space. The sRGB space is a W3C-endorsed standard that defines a non-linear conversion between red, green, and blue component values and the actual intensity of the visible component color. </p>
		 * <p>For example, consider a simple linear gradient between two colors (with the <code>spreadMethod</code> parameter set to <code>SpreadMethod.REFLECT</code>). The different interpolation methods affect the appearance as follows: </p>
		 * <table class="+ topic/table adobe-d/adobetable ">
		 *  <tbody>
		 *   <tr>
		 *    <td align="center"><img src="../../images/beginGradientFill_interp_linearrgb.jpg" alt="linear gradient with InterpolationMethod.LINEAR_RGB"></td>
		 *    <td align="center"><img src="../../images/beginGradientFill_interp_rgb.jpg" alt="linear gradient with InterpolationMethod.RGB"></td>
		 *   </tr>
		 *   <tr>
		 *    <td align="center"><code>InterpolationMethod.LINEAR_RGB</code></td>
		 *    <td align="center"><code>InterpolationMethod.RGB</code></td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="InterpolationMethod.html#LINEAR_RGB" target="">LINEAR_RGB</a>
		 * </div>
		 */
		public static const RGB:String = "rgb";
	}
}
