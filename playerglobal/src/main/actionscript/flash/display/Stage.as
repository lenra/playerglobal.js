package flash.display {
	import flash.events.Event;
	import flash.events.FullScreenEvent;
	import flash.events.TimerEvent;
	import flash.geom.Matrix;
	import flash.utils.Timer;
	//import flash.events.StageOrientationEvent;
	import flash.events.StageVideoAvailabilityEvent;
	//import flash.events.VsyncStateChangeAvailabilityEvent;
	import flash.geom.Rectangle;
	import flash.media.StageVideo;
	import flash.text.TextSnapshot;

	[Event(name="VsyncStateChangeAvailability", type="flash.events.VsyncStateChangeAvailabilityEvent")]
	[Event(name="browserZoomChange", type="flash.events.Event")]
	[Event(name="fullScreen", type="flash.events.FullScreenEvent")]
	[Event(name="mouseLeave", type="flash.events.Event")]
	[Event(name="orientationChange", type="flash.events.StageOrientationEvent")]
	[Event(name="orientationChanging", type="flash.events.StageOrientationEvent")]
	[Event(name="resize", type="flash.events.Event")]
	[Event(name="stageVideoAvailability", type="flash.events.StageVideoAvailabilityEvent")]
	/**
	 *  The Stage class represents the main drawing area. <p>For SWF content running in the browser (in Flash<sup>®</sup> Player), the Stage represents the entire area where Flash content is shown. For content running in AIR on desktop operating systems, each NativeWindow object has a corresponding Stage object.</p> <p>The Stage object is not globally accessible. You need to access it through the <code>stage</code> property of a DisplayObject instance.</p> <p>The Stage class has several ancestor classes — DisplayObjectContainer, InteractiveObject, DisplayObject, and EventDispatcher — from which it inherits properties and methods. Many of these properties and methods are either inapplicable to Stage objects, or require security checks when called on a Stage object. The properties and methods that require security checks are documented as part of the Stage class.</p> <p>In addition, the following inherited properties are inapplicable to Stage objects. If you try to set them, an IllegalOperationError is thrown. These properties may always be read, but since they cannot be set, they will always contain default values.</p> <ul> 
	 *  <li> <code>accessibilityProperties</code> </li> 
	 *  <li> <code>alpha</code> </li> 
	 *  <li> <code>blendMode</code> </li> 
	 *  <li> <code>cacheAsBitmap</code> </li> 
	 *  <li> <code>contextMenu</code> </li> 
	 *  <li> <code>filters</code> </li> 
	 *  <li> <code>focusRect</code> </li> 
	 *  <li> <code>loaderInfo</code> </li> 
	 *  <li> <code>mask</code> </li> 
	 *  <li> <code>mouseEnabled</code> </li> 
	 *  <li> <code>name</code> </li> 
	 *  <li> <code>opaqueBackground</code> </li> 
	 *  <li> <code>rotation</code> </li> 
	 *  <li> <code>scale9Grid</code> </li> 
	 *  <li> <code>scaleX</code> </li> 
	 *  <li> <code>scaleY</code> </li> 
	 *  <li> <code>scrollRect</code> </li> 
	 *  <li> <code>tabEnabled</code> </li> 
	 *  <li> <code>tabIndex</code> </li> 
	 *  <li> <code>transform</code> </li> 
	 *  <li> <code>visible</code> </li> 
	 *  <li> <code>x</code> </li> 
	 *  <li> <code>y</code> </li> 
	 * </ul> <p>Some events that you might expect to be a part of the Stage class, such as <code>enterFrame</code>, <code>exitFrame</code>, <code>frameConstructed</code>, and <code>render</code>, cannot be Stage events because a reference to the Stage object cannot be guaranteed to exist in every situation where these events are used. Because these events cannot be dispatched by the Stage object, they are instead dispatched by every DisplayObject instance, which means that you can add an event listener to any DisplayObject instance to listen for these events. These events, which are part of the DisplayObject class, are called broadcast events to differentiate them from events that target a specific DisplayObject instance. Two other broadcast events, <code>activate</code> and <code>deactivate</code>, belong to DisplayObject's superclass, EventDispatcher. The <code>activate</code> and <code>deactivate</code> events behave similarly to the DisplayObject broadcast events, except that these two events are dispatched not only by all DisplayObject instances, but also by all EventDispatcher instances and instances of other EventDispatcher subclasses. For more information on broadcast events, see the DisplayObject class.</p> <p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7dff.html" target="_blank">Adding display objects to the display list</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e26.html" target="_blank">Traversing the display list </a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS0D75B487-23B9-402d-A52D-CB3C4CEB9EE4.html" target="_blank">Controlling Stage scaling</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS2E9C7F3B-6A7C-4c5d-8ADD-5B23446FBEEB.html" target="_blank">Working with full-screen mode</a>
	 * </div><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e4f.html" target="_blank">The event flow</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e58.html" target="_blank">Display programming</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e3e.html" target="_blank">Basics of display programming</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e3c.html" target="_blank">Core display classes</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e3d.html" target="_blank">Working with display objects</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e31.html" target="_blank">Setting Stage properties</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7dfb.html" target="_blank">Handling events for display objects</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS901d38e593cd1bac3bf97f12ca4e5c567-8000.html" target="_blank">Stage orientation</a>
	 *  <br>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="DisplayObject.html" target="">flash.display.DisplayObject</a>
	 * </div><br><hr>
	 */
	public class Stage extends DisplayObjectContainer {
		private static var _supportsOrientationChange:Boolean;
		private static var _stage:Stage;

		private var _align:String;
		private var _autoOrients:Boolean;
		private var _color:uint;
		private var _colorCorrection:String;
		private var _displayState:String;
		private var _focus:InteractiveObject;
		private var _frameRate:Number = 24;
		private var _fullScreenSourceRect:Rectangle;
		private var _height:Number;
		private var _mouseChildren:Boolean;
		private var _mouseLock:Boolean;
		private var _quality:String;
		private var _scaleMode:String;
		private var _showDefaultContextMenu:Boolean;
		private var _stageFocusRect:Boolean;
		private var _stageHeight:int;
		private var _stageWidth:int;
		private var _tabChildren:Boolean;
		private var _vsyncEnabled:Boolean;
		private var _width:Number;

		private var _allowsFullScreen:Boolean;
		private var _allowsFullScreenInteractive:Boolean;
		private var _browserZoomFactor:Number;
		private var _colorCorrectionSupport:String;
		private var _contentsScaleFactor:Number;
		private var _deviceOrientation:String;
		private var _fullScreenHeight:uint;
		private var _fullScreenWidth:uint;
		private var invalidated:Boolean = false;
		//private var _nativeWindow:NativeWindow;
		private var _numChildren:int;
		private var _orientation:String;
		private var _softKeyboardRect:Rectangle;
		private var _stage3Ds:Vector.<Stage3D>;
		private var _stageVideos:Vector.<StageVideo>;
		private var _supportedOrientations:Vector.<String>;
		private var _textSnapshot:TextSnapshot;
		private var _wmodeGPU:Boolean;
		
		private var frameTimer:Timer;

		public function Stage() {
			Stage._stage = this;
			window.addEventListener(Event.RESIZE, onResize, false);
			
			this.frameTimer = new Timer(1000 / frameRate);
			this.frameTimer.addEventListener(TimerEvent.TIMER, onFrame);
			this.frameTimer.start();
		}
		
		/**
		 * <p> A value from the StageAlign class that specifies the alignment of the stage in Flash Player or the browser. The following are valid values: </p>
		 * <p> </p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>Value</th>
		 *    <th>Vertical Alignment</th>
		 *    <th>Horizontal</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>StageAlign.TOP</code></td>
		 *    <td>Top</td>
		 *    <td>Center</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>StageAlign.BOTTOM</code></td>
		 *    <td>Bottom</td>
		 *    <td>Center</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>StageAlign.LEFT</code></td>
		 *    <td>Center</td>
		 *    <td>Left</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>StageAlign.RIGHT</code></td>
		 *    <td>Center</td>
		 *    <td>Right</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>StageAlign.TOP_LEFT</code></td>
		 *    <td>Top</td>
		 *    <td>Left</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>StageAlign.TOP_RIGHT</code></td>
		 *    <td>Top</td>
		 *    <td>Right</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>StageAlign.BOTTOM_LEFT</code></td>
		 *    <td>Bottom</td>
		 *    <td>Left</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>StageAlign.BOTTOM_RIGHT</code></td>
		 *    <td>Bottom</td>
		 *    <td>Right</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p>The <code>align</code> property is only available to an object that is in the same security sandbox as the Stage owner (the main SWF file). To avoid this, the Stage owner can grant permission to the domain of the calling object by calling the <code>Security.allowDomain()</code> method or the <code>Security.alowInsecureDomain()</code> method. For more information, see the "Security" chapter in the <i>ActionScript 3.0 Developer's Guide</i>.</p>
		 * 
		 * @return 
		 */
		public function get align():String {
			return _align;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set align(value:String):void {
			_align = value;
		}

		/**
		 * <p> Specifies whether this stage allows the use of the full screen mode </p>
		 * 
		 * @return 
		 */
		public function get allowsFullScreen():Boolean {
			return _allowsFullScreen;
		}

		/**
		 * <p> Specifies whether this stage allows the use of the full screen with text input mode </p>
		 * 
		 * @return 
		 */
		public function get allowsFullScreenInteractive():Boolean {
			return _allowsFullScreenInteractive;
		}

		/**
		 * <p> Specifies whether the stage automatically changes orientation when the device orientation changes. </p>
		 * <p>The initial value of this property is derived from the <code>autoOrients</code> element of the application descriptor and defaults to <code>false</code>. When changing the property to <code>false</code>, the behavior is not guaranteed. On some devices, the stage remains in the current orientation. On others, the stage orientation changes to a device-defined "standard" orientation, after which, no further stage orientation changes occur.</p>
		 * <p><i>AIR profile support:</i> This feature is supported on mobile devices, but it is not supported on desktop operating systems or AIR for TV devices. You can test for support at run time using the <code>Stage.supportsOrientantionChange</code> property. See <a href="http://help.adobe.com/en_US/air/build/WS144092a96ffef7cc16ddeea2126bb46b82f-8000.html"> AIR Profile Support</a> for more information regarding API support across multiple profiles.</p>
		 * 
		 * @return 
		 */
		public function get autoOrients():Boolean {
			return _autoOrients;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set autoOrients(value:Boolean):void {
			_autoOrients = value;
		}

		/**
		 * <p> Specifies the browser zoom factor. A change in browser zoom factor affects the scaling factor of the stage. </p>
		 * 
		 * @return 
		 */
		public function get browserZoomFactor():Number {
			return _browserZoomFactor;
		}

		/**
		 * <p> The SWF background color. </p>
		 * <p>Defaults to the stage background color, as set by the SWF tag SetBackgroundColor.</p>
		 * 
		 * @return 
		 */
		public function get color():uint {
			return _color;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set color(value:uint):void {
			_color = value;
		}

		/**
		 * <p> Controls Flash runtime color correction for displays. Color correction works only if the main monitor is assigned a valid ICC color profile, which specifies the device's particular color attributes. By default, the Flash runtime tries to match the color correction of its host (usually a browser). </p>
		 * <p>Use the <code>Stage.colorCorrectionSupport</code> property to determine if color correction is available on the current system and the default state. . If color correction is available, all colors on the stage are assumed to be in the sRGB color space, which is the most standard color space. Source profiles of input devices are not considered during color correction. No input color correction is applied; only the stage output is mapped to the main monitor's ICC color profile.</p>
		 * <p>In general, the benefits of activating color management include predictable and consistent color, better conversion, accurate proofing and more efficient cross-media output. Be aware, though, that color management does not provide perfect conversions due to devices having a different gamut from each other or original images. Nor does color management eliminate the need for custom or edited profiles. Color profiles are dependent on browsers, operating systems (OS), OS extensions, output devices, and application support.</p>
		 * <p>Applying color correction degrades the Flash runtime performance. A Flash runtime's color correction is document style color correction because all SWF movies are considered documents with implicit sRGB profiles. Use the <code>Stage.colorCorrectionSupport</code> property to tell the Flash runtime to correct colors when displaying the SWF file (document) to the display color space. Flash runtimes only compensates for differences between monitors, not for differences between input devices (camera/scanner/etc.). </p>
		 * <p>The three possible values are strings with corresponding constants in the flash.display.ColorCorrection class:</p>
		 * <ul>
		 *  <li><code>"default"</code>: Use the same color correction as the host system.</li>
		 *  <li><code>"on"</code>: Always perform color correction.</li>
		 *  <li><code>"off"</code>: Never perform color correction.</li>
		 * </ul>
		 * 
		 * @return 
		 */
		public function get colorCorrection():String {
			return _colorCorrection;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set colorCorrection(value:String):void {
			_colorCorrection = value;
		}

		/**
		 * <p> Specifies whether the Flash runtime is running on an operating system that supports color correction and whether the color profile of the main (primary) monitor can be read and understood by the Flash runtime. This property also returns the default state of color correction on the host system (usually the browser). Currently the return values can be: </p>
		 * <p>The three possible values are strings with corresponding constants in the flash.display.ColorCorrectionSupport class:</p>
		 * <ul>
		 *  <li><code>"unsupported"</code>: Color correction is not available.</li>
		 *  <li><code>"defaultOn"</code>: Always performs color correction.</li>
		 *  <li><code>"defaultOff"</code>: Never performs color correction.</li>
		 * </ul>
		 * 
		 * @return 
		 */
		public function get colorCorrectionSupport():String {
			return _colorCorrectionSupport;
		}

		/**
		 * <p> Specifies the effective pixel scaling factor of the stage. This value is 1 on standard screens and HiDPI (Retina display) screens. When the stage is rendered on HiDPI screens the pixel resolution is doubled; even if the stage scaling mode is set to StageScaleMode.NO_SCALE. Stage.stageWidth and Stage.stageHeight continue to be reported in classic pixel units. </p>
		 * 
		 * @return 
		 */
		public function get contentsScaleFactor():Number {
			return _contentsScaleFactor;
		}

		/**
		 * <p> The physical orientation of the device. </p>
		 * <p>On devices with slide-out keyboards, the state of the keyboard has a higher priority in determining the device orientation than the rotation detected by the accelerometer. Thus on a portrait-aspect device with a side-mounted keyboard, the <code>deviceOrientation</code> property will report <code>ROTATED_LEFT</code> when the keyboard is open no matter how the user is holding the device.</p>
		 * <p>Use the constants defined in the StageOrientation class when setting or comparing values for this property.</p>
		 * <p><i>AIR profile support:</i> This feature is supported on mobile devices, but it is not supported on desktop operating systems or AIR for TV devices. You can test for support at run time using the <code>Stage.supportsOrientationChange</code> property. See <a href="http://help.adobe.com/en_US/air/build/WS144092a96ffef7cc16ddeea2126bb46b82f-8000.html"> AIR Profile Support</a> for more information regarding API support across multiple profiles.</p>
		 * 
		 * @return 
		 */
		public function get deviceOrientation():String {
			return _deviceOrientation;
		}

		/**
		 * <p> A value from the StageDisplayState class that specifies which display state to use. The following are valid values: </p>
		 * <ul>
		 *  <li><code>StageDisplayState.FULL_SCREEN</code> Sets AIR application or content in Flash Player to expand the stage over the user's entire screen. Keyboard input is disabled, with the exception of a limited set of non-printing keys.</li>
		 *  <li><code>StageDisplayState.FULL_SCREEN_INTERACTIVE</code> Sets the application to expand the stage over the user's entire screen, with keyboard input allowed. <span>(Available in AIR and Flash Player, beginning with Flash Player 11.3.)</span></li>
		 *  <li><code>StageDisplayState.NORMAL</code> Sets the stage back to the standard stage display mode.</li>
		 * </ul>
		 * <p>The scaling behavior of the movie in full-screen mode is determined by the <code>scaleMode</code> setting (set using the <code>Stage.scaleMode</code> property or the SWF file's <code>embed</code> tag settings in the HTML file). If the <code>scaleMode</code> property is set to <code>noScale</code> while the application transitions to full-screen mode, the Stage <code>width</code> and <code>height</code> properties are updated, and the Stage dispatches a <code>resize</code> event. If any other scale mode is set, the stage and its contents are scaled to fill the new screen dimensions. The Stage object retains its original <code>width</code> and <code>height</code> values and does not dispatch a <code>resize</code> event.</p>
		 * <p>The following restrictions apply to SWF files that play within an HTML page (not those using the stand-alone Flash Player or AIR runtime):</p>
		 * <ul>
		 *  <li>To enable full-screen mode, add the <code>allowFullScreen</code> parameter to the <code>object</code> and <code>embed</code> tags in the HTML page that includes the SWF file, with <code>allowFullScreen</code> set to <code>"true"</code>, as shown in the following example: 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>&lt;param name="allowFullScreen" value="true" /&gt;
		 *           ...
		 *     &lt;embed src="example.swf" allowFullScreen="true" ... &gt;</pre>
		 *   </div> <p>An HTML page may also use a script to generate SWF-embedding tags. You need to alter the script so that it inserts the proper <code>allowFullScreen</code> settings. HTML pages generated by Flash Professional and Flash Builder use the <code>AC_FL_RunContent()</code> function to embed references to SWF files, and you need to add the <code>allowFullScreen</code> parameter settings, as in the following:</p> 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>AC_FL_RunContent( ... "allowFullScreen", "true", ... )</pre>
		 *   </div></li>
		 *  <li>Full-screen mode is initiated in response to a mouse click or key press by the user; the movie cannot change <code>Stage.displayState</code> without user input. Flash runtimes restrict keyboard input in full-screen mode. Acceptable keys include keyboard shortcuts that terminate full-screen mode and non-printing keys such as arrows, space, Shift, and Tab keys. (Use full-screen interactive mode to support input from additional keys.) Keyboard shortcuts that terminate full-screen mode are: Escape (Windows, Linux, and Mac), Control+W (Windows), Command+W (Mac), and Alt+F4.</li>
		 *  <li>Starting with Flash Player 9.0.115.0, full-screen works the same in windowless mode as it does in window mode. If you set the Window Mode (<code>wmode</code> in the HTML) to Opaque Windowless (<code>opaque</code>) or Transparent Windowless (<code>transparent</code>), full-screen can be initiated, but the full-screen window will always be opaque.</li>
		 *  <li>To enable full-screen interactive mode, which supports keyboard interactivity, add the <code>allowFullScreenInteractive</code> parameter to the <code>object</code> and <code>embed</code> tags in the HTML page that includes the SWF file, with <code>allowFullScreenInteractive</code> set to <code>"true"</code>, as shown in the following example: 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>&lt;param name="allowFullScreenInteractive" value="true" /&gt;
		 *           ...</pre>
		 *   </div></li>
		 * </ul>
		 * <p>These restrictions are <i>not</i> present for SWF content running in the stand-alone Flash Player or in AIR.</p>
		 * <p>When entering full-screen mode, the Flash runtime briefly displays a notification over the SWF content to inform the users they are in full-screen mode and that they can press the Escape key to end full-screen mode. </p>
		 * <p>When entering full-screen interactive mode, Flash Player displays a confirmation prompt over the SWF content, enabling users to allow access to extended keyboard input (including printing keys) and informing users that they can press the Escape key to end full-screen interactive mode. You should not make assumptions about the appearance/location of the confirmation prompt. You can determine if users have allowed access to extended keyboard input by listening for the <code>FullScreenEvent.FULL_SCREEN_INTERACTIVE_ACCEPTED</code> event. </p>
		 * <p>For <span>AIR</span> content running in full-screen mode, the system screen saver and power saving options are disabled while video content is playing and until either the video stops or full-screen mode is exited.</p>
		 * <p>On Linux, setting <code>displayState</code> to <code>StageDisplayState.FULL_SCREEN</code> or <code>StageDisplayState.FULL_SCREEN_INTERACTIVE</code> is an asynchronous operation.</p>
		 * 
		 * @return 
		 */
		public function get displayState():String {
			return _displayState;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set displayState(value:String):void {
			_displayState = value;
		}

		/**
		 * <p> The interactive object with keyboard focus; or <code>null</code> if focus is not set or if the focused object belongs to a security sandbox to which the calling object does not have access. </p>
		 * 
		 * @return 
		 */
		public function get focus():InteractiveObject {
			return _focus;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set focus(value:InteractiveObject):void {
			_focus = value;
		}

		/**
		 * <p> Gets and sets the frame rate of the stage. The frame rate is defined as frames per second. By default the rate is set to the frame rate of the first SWF file loaded. Valid range for the frame rate is from 0.01 to 1000 frames per second. </p>
		 * <p><b>Note:</b> An application might not be able to follow high frame rate settings, either because the target platform is not fast enough or the player is synchronized to the vertical blank timing of the display device (usually 60 Hz on LCD devices). In some cases, a target platform might also choose to lower the maximum frame rate if it anticipates high CPU usage.</p>
		 * <p>For content running in Adobe AIR, setting the <code>frameRate</code> property of one Stage object changes the frame rate for all Stage objects (used by different NativeWindow objects). </p>
		 * 
		 * @return 
		 */
		public function get frameRate():Number {
			return _frameRate;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set frameRate(value:Number):void {
			//TODO: changer le timer des frames
			_frameRate = value;
		}

		/**
		 * <p> Returns the height of the monitor that will be used when going to full screen size, if that state is entered immediately. If the user has multiple monitors, the monitor that's used is the monitor that most of the stage is on at the time. </p>
		 * <p><b>Note</b>: If the user has the opportunity to move the browser from one monitor to another between retrieving the value and going to full screen size, the value could be incorrect. If you retrieve the value in an event handler that sets <code>Stage.displayState</code> to <code>StageDisplayState.FULL_SCREEN</code>, the value will be correct.</p>
		 * <p>This is the pixel height of the monitor and is the same as the stage height would be if <code>Stage.align</code> is set to <code>StageAlign.TOP_LEFT</code> and <code>Stage.scaleMode</code> is set to <code>StageScaleMode.NO_SCALE</code>.</p>
		 * 
		 * @return 
		 */
		public function get fullScreenHeight():uint {
			return _fullScreenHeight;
		}

		/**
		 * <p> Sets the Flash runtime to scale a specific region of the stage to full-screen mode. If available, the Flash runtime scales in hardware, which uses the graphics and video card on a user's computer, and generally displays content more quickly than software scaling. </p>
		 * <p>When this property is set to a valid rectangle and the <code>displayState</code> property is set to full-screen mode, the Flash runtime scales the specified area. The actual Stage size in pixels within ActionScript does not change. The Flash runtime enforces a minimum limit for the size of the rectangle to accommodate the standard "Press Esc to exit full-screen mode" message. This limit is usually around 260 by 30 pixels but can vary on platform and Flash runtime version.</p>
		 * <p>This property can only be set when the Flash runtime is not in full-screen mode. To use this property correctly, set this property first, then set the <code>displayState</code> property to full-screen mode, as shown in the code examples. <b>Note:</b> In Flash Player 15 and later, this property can be set even when the Flash runtime is in full-screen mode.</p>
		 * <p>To enable scaling, set the <code>fullScreenSourceRect</code> property to a rectangle object:</p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>  
		 *      // valid, will enable hardware scaling
		 *      stage.fullScreenSourceRect = new Rectangle(0,0,320,240); 
		 *      </pre>
		 * </div>
		 * <p>To disable scaling, set the <code>fullScreenSourceRect=null</code> in ActionScript 3.0, and <code>undefined</code> in ActionScript 2.0.</p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *      stage.fullScreenSourceRect = null;
		 *      </pre>
		 * </div>
		 * <p>The end user also can select within Flash Player Display Settings to turn off hardware scaling, which is enabled by default. For more information, see <a href="http://www.adobe.com/go/display_settings" target="external">www.adobe.com/go/display_settings</a>.</p>
		 * 
		 * @return 
		 */
		public function get fullScreenSourceRect():Rectangle {
			return _fullScreenSourceRect;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set fullScreenSourceRect(value:Rectangle):void {
			_fullScreenSourceRect = value;
		}

		/**
		 * <p> Returns the width of the monitor that will be used when going to full screen size, if that state is entered immediately. If the user has multiple monitors, the monitor that's used is the monitor that most of the stage is on at the time. </p>
		 * <p><b>Note</b>: If the user has the opportunity to move the browser from one monitor to another between retrieving the value and going to full screen size, the value could be incorrect. If you retrieve the value in an event handler that sets <code>Stage.displayState</code> to <code>StageDisplayState.FULL_SCREEN</code>, the value will be correct.</p>
		 * <p>This is the pixel width of the monitor and is the same as the stage width would be if <code>Stage.align</code> is set to <code>StageAlign.TOP_LEFT</code> and <code>Stage.scaleMode</code> is set to <code>StageScaleMode.NO_SCALE</code>.</p>
		 * 
		 * @return 
		 */
		public function get fullScreenWidth():uint {
			return _fullScreenWidth;
		}

		/**
		 * <p> Indicates the height of the display object, in pixels. The height is calculated based on the bounds of the content of the display object. When you set the <code>height</code> property, the <code>scaleY</code> property is adjusted accordingly, as shown in the following code: </p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *     var rect:Shape = new Shape();
		 *     rect.graphics.beginFill(0xFF0000);
		 *     rect.graphics.drawRect(0, 0, 100, 100);
		 *     trace(rect.scaleY) // 1;
		 *     rect.height = 200;
		 *     trace(rect.scaleY) // 2;</pre>
		 * </div>
		 * <p>Except for TextField and Video objects, a display object with no content (such as an empty sprite) has a height of 0, even if you try to set <code>height</code> to a different value.</p>
		 * 
		 * @return 
		 */
		override public function get height():Number {
			return _height;
		}

		/**
		 * @param value
		 * @return 
		 */
		override public function set height(value:Number):void {
			_height = value;
		}

		/**
		 * <p> Determines whether or not the children of the object are mouse, or user input device, enabled. If an object is enabled, a user can interact with it by using a mouse or user input device. The default is <code>true</code>. </p>
		 * <p>This property is useful when you create a button with an instance of the Sprite class (instead of using the SimpleButton class). When you use a Sprite instance to create a button, you can choose to decorate the button by using the <code>addChild()</code> method to add additional Sprite instances. This process can cause unexpected behavior with mouse events because the Sprite instances you add as children can become the target object of a mouse event when you expect the parent instance to be the target object. To ensure that the parent instance serves as the target objects for mouse events, you can set the <code>mouseChildren</code> property of the parent instance to <code>false</code>.</p>
		 * <p> No event is dispatched by setting this property. You must use the <code>addEventListener()</code> method to create interactive functionality.</p>
		 * 
		 * @return 
		 */
		override public function get mouseChildren():Boolean {
			return _mouseChildren;
		}

		/**
		 * @param value
		 * @return 
		 */
		override public function set mouseChildren(value:Boolean):void {
			_mouseChildren = value;
		}

		/**
		 * <p> Set to <code>true</code> to enable mouse locking. Enabling mouse locking turns off the cursor, and allows mouse movement with no bounds. You can only enable mouse locking in full-screen mode for desktop applications. Setting it on applications not in full-screen mode, or for applications on mobile devices, throws an exception. </p>
		 * <p>Mouse locking is disabled automatically and the mouse cursor is made visible again when:</p>
		 * <ul>
		 *  <li>The user exits full-screen mode by using the Esc key (all platforms), Control-W (Windows), Command-W (Mac), or Alt-F4 (Windows).</li>
		 *  <li>The application window loses focus.</li>
		 *  <li>Any settings UI is visible, including all privacy dialog boxes.</li>
		 *  <li>A native dialog box is shown, such as a file upload dialog box.</li>
		 * </ul>
		 * <p>When exiting full screen mode, this property is automatically set to <code>false</code>.</p>
		 * <p>Events associated with mouse movement, such as the <code>mouseMove</code> event, use the MouseEvent class to represent the event object. When mouse locking is disabled, use the <code>MouseEvent.localX</code> and <code>MouseEvent.localY</code> properties to determine the location of the mouse. When mouse locking is enabled, use the <code>MouseEvent.movementX</code> and <code>MouseEvent.movementY</code> properties to determine the location of the mouse. The <code>movementX</code> and <code>movementY</code> properties contain changes in the position of the mouse since the last event, instead of absolute coordinates of the mouse location.</p>
		 * <p><b>Note:</b> When the application is in full-screen mode, mouse event listeners attached to display objects other than the Stage are not dispatched. Therefore, to receive mouse deltas and any other mouse events when <code>mouseLock</code> is <code>true</code>, attach the mouse event listeners to the Stage object.</p>
		 * 
		 * @return 
		 */
		public function get mouseLock():Boolean {
			return _mouseLock;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set mouseLock(value:Boolean):void {
			_mouseLock = value;
		}

		/**
		 * <p> A reference to the NativeWindow object containing this Stage. </p>
		 * <p>The window represents the native operating system window; the Stage represents the content contained by the window. This property is only valid for content running in AIR on platforms that support the NativeWindow class. On other platforms, this property will be <code>null</code>. In Flash Player (content running in a browser), this property will also be <code>null</code>.</p>
		 * 
		 * @return 
		 */
		/*public function get nativeWindow():NativeWindow {
			return _nativeWindow;
		}*/

		/**
		 * <p> Returns the number of children of this object. </p>
		 * 
		 * @return 
		 */
		/*override public function get numChildren():int {
			return _numChildren;
		}*/

		/**
		 * <p> The current orientation of the stage. This property is set to one of four values, defined as constants in the StageOrientation class: </p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>StageOrientation constant</th>
		 *    <th>Stage orientation</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>StageOrientation.DEFAULT</code></td>
		 *    <td>The screen is in the default orientation (right-side up).</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>StageOrientation.ROTATED_RIGHT</code></td>
		 *    <td>The screen is rotated right.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>StageOrientation.ROTATED_LEFT</code></td>
		 *    <td>The screen is rotated left.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>StageOrientation.UPSIDE_DOWN</code></td>
		 *    <td>The screen is rotated upside down.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>StageOrientation.UNKNOWN</code></td>
		 *    <td>The application has not yet determined the initial orientation of the screen. You can add an event listener for the <code>orientationChange</code> event </td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p>To set the stage orientation, use the <code>setOrientation()</code> method.</p>
		 * <p><b>Important:</b> orientation property is supported on Android devices from 2.6 namespace onwards.</p>
		 * 
		 * @return 
		 */
		public function get orientation():String {
			return _orientation;
		}

		/**
		 * <p> A value from the StageQuality class that specifies which rendering quality is used. The following are valid values: </p>
		 * <ul>
		 *  <li><code>StageQuality.LOW</code>—Low rendering quality. Graphics are not anti-aliased, and bitmaps are not smoothed, but runtimes still use mip-mapping.</li>
		 *  <li><code>StageQuality.MEDIUM</code>—Medium rendering quality. Graphics are anti-aliased using a 2 x 2 pixel grid, bitmap smoothing is dependent on the <code>Bitmap.smoothing</code> setting. Runtimes use mip-mapping. This setting is suitable for movies that do not contain text.</li>
		 *  <li><code>StageQuality.HIGH</code>—High rendering quality. Graphics are anti-aliased using a 4 x 4 pixel grid, and bitmap smoothing is dependent on the <code>Bitmap.smoothing</code> setting. Runtimes use mip-mapping. This is the default rendering quality setting that Flash Player uses.</li>
		 *  <li><code>StageQuality.BEST</code>—Very high rendering quality. Graphics are anti-aliased using a 4 x 4 pixel grid. If <code>Bitmap.smoothing</code> is <code>true</code> the runtime uses a high quality downscale algorithm that produces fewer artifacts (however, using <code>StageQuality.BEST</code> with <code>Bitmap.smoothing</code> set to <code>true</code> slows performance significantly and is not a recommended setting).</li>
		 * </ul>
		 * <p>Higher quality settings produce better rendering of scaled bitmaps. However, higher quality settings are computationally more expensive. In particular, when rendering scaled video, using higher quality settings can reduce the frame rate. </p>
		 * <p>The <code>BitmapData.draw()</code> method uses the value of the <code>Stage.quality</code> property. Alternatively, you can use the <code>BitmapData.drawWithQuality()</code> method, which lets you specify the <code>quality</code> parameter to the method, ignoring the current value of <code>Stage.quality</code>.</p>
		 * <p>In the desktop profile of Adobe AIR, <code>quality</code> can be set to <code>StageQuality.BEST</code> or <code>StageQuality.HIGH</code> (and the default value is <code>StageQuality.HIGH</code>). Attempting to set it to another value has no effect (and the property remains unchanged). In the moble profile of AIR, all four quality settings are available. The default value on mobile devices is <code>StageQuality.MEDIUM</code>.</p>
		 * <p>For content running in Adobe AIR, setting the <code>quality</code> property of one Stage object changes the rendering quality for all Stage objects (used by different NativeWindow objects). </p>
		 * <b><i>Note:</i></b>
		 * <code>quality</code>
		 * 
		 * @return 
		 */
		public function get quality():String {
			return _quality;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set quality(value:String):void {
			_quality = value;
		}

		/**
		 * <p> A value from the StageScaleMode class that specifies which scale mode to use. The following are valid values: </p>
		 * <ul>
		 *  <li><code>StageScaleMode.EXACT_FIT</code>—The entire application is visible in the specified area without trying to preserve the original aspect ratio. Distortion can occur, and the application may appear stretched or compressed. </li>
		 *  <li><code>StageScaleMode.SHOW_ALL</code>—The entire application is visible in the specified area without distortion while maintaining the original aspect ratio of the application. Borders can appear on two sides of the application. </li>
		 *  <li><code>StageScaleMode.NO_BORDER</code>—The entire application fills the specified area, without distortion but possibly with some cropping, while maintaining the original aspect ratio of the application. </li>
		 *  <li><code>StageScaleMode.NO_SCALE</code>—The entire application is fixed, so that it remains unchanged even as the size of the player window changes. Cropping might occur if the player window is smaller than the content. </li>
		 * </ul>
		 * 
		 * @return 
		 */
		public function get scaleMode():String {
			return _scaleMode;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set scaleMode(value:String):void {
			_scaleMode = value;
		}

		/**
		 * <p> Specifies whether to show or hide the default items in the Flash runtime context menu. </p>
		 * <p>If the <code>showDefaultContextMenu</code> property is set to <code>true</code> (the default), all context menu items appear. If the <code>showDefaultContextMenu</code> property is set to <code>false</code>, only the Settings and About... menu items appear.</p>
		 * 
		 * @return 
		 */
		public function get showDefaultContextMenu():Boolean {
			return _showDefaultContextMenu;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set showDefaultContextMenu(value:Boolean):void {
			_showDefaultContextMenu = value;
		}

		/**
		 * <p> A Rectangle specifying the area of the stage that is currently covered by a soft keyboard. The Rect's components are (0,0,0,0) when the soft keyboard isn't raised. </p>
		 * 
		 * @return 
		 */
		public function get softKeyboardRect():Rectangle {
			return _softKeyboardRect;
		}
		
		override public function get stage():Stage {
			return this;
		}

		/**
		 * <p> A list of Stage3D objects available for displaying 3-dimensional content. </p>
		 * <p>You can use only a limited number of Stage3D objects at a time. The number of available Stage3D objects depends on the platform and on the available hardware. </p>
		 * <p>A Stage3D object draws in front of a StageVideo object and behind the Flash display list.</p>
		 * 
		 * @return 
		 */
		public function get stage3Ds():Vector.<Stage3D> {
			return _stage3Ds;
		}

		/**
		 * <p> Specifies whether or not objects display a glowing border when they have focus. </p>
		 * 
		 * @return 
		 */
		public function get stageFocusRect():Boolean {
			return _stageFocusRect;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set stageFocusRect(value:Boolean):void {
			_stageFocusRect = value;
		}

		/**
		 * <p> The current height, in pixels, of the Stage. </p>
		 * <p>If the value of the <code>Stage.scaleMode</code> property is set to <code>StageScaleMode.NO_SCALE</code> when the user resizes the window, the Stage content maintains its size while the <code>stageHeight</code> property changes to reflect the new height size of the screen area occupied by the SWF file. (In the other scale modes, the <code>stageHeight</code> property always reflects the original height of the SWF file.) You can add an event listener for the <code>resize</code> event and then use the <code>stageHeight</code> property of the Stage class to determine the actual pixel dimension of the resized Flash runtime window. The event listener allows you to control how the screen content adjusts when the user resizes the window.</p>
		 * <p>Air for TV devices have slightly different behavior than desktop devices when you set the <code>stageHeight</code> property. If the <code>Stage.scaleMode</code> property is set to <code>StageScaleMode.NO_SCALE</code> and you set the <code>stageHeight</code> property, the stage height does not change until the next frame of the SWF.</p>
		 * <p><b>Note:</b> In an HTML page hosting the SWF file, both the <code>object</code> and <code>embed</code> tags' <code>height</code> attributes must be set to a percentage (such as <code>100%</code>), not pixels. If the settings are generated by JavaScript code, the <code>height</code> parameter of the <code>AC_FL_RunContent() </code> method must be set to a percentage, too. This percentage is applied to the <code>stageHeight</code> value.</p>
		 * 
		 * @return 
		 */
		public function get stageHeight():int {
			return window.innerHeight;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set stageHeight(value:int):void {
			_stageHeight = value;
		}

		/**
		 * <p> A list of StageVideo objects available for playing external videos. </p>
		 * <p>You can use only a limited number of StageVideo objects at a time. When a SWF begins to run, the number of available StageVideo objects depends on the platform and on available hardware. </p>
		 * <p>To use a StageVideo object, assign a member of the <code>stageVideos</code> Vector object to a StageVideo variable. </p>
		 * <p>All StageVideo objects are displayed on the stage behind any display objects. The StageVideo objects are displayed on the stage in the order they appear in the <code>stageVideos</code> Vector object. For example, if the <code>stageVideos</code> Vector object contains three entries:</p>
		 * <ol>
		 *  <li>The StageVideo object in the 0 index of the <code>stageVideos</code> Vector object is displayed behind all StageVideo objects.</li>
		 *  <li>The StageVideo object at index 1 is displayed in front of the StageVideo object at index 0.</li>
		 *  <li>The StageVideo object at index 2 is displayed in front of the StageVideo object at index 1.</li>
		 * </ol>
		 * <p>Use the <code>StageVideo.depth</code> property to change this ordering.</p>
		 * <p><b>Note:</b> AIR for TV devices support only one StageVideo object.</p>
		 * 
		 * @return 
		 */
		public function get stageVideos():Vector.<StageVideo> {
			return _stageVideos;
		}

		/**
		 * <p> Specifies the current width, in pixels, of the Stage. </p>
		 * <p>If the value of the <code>Stage.scaleMode</code> property is set to <code>StageScaleMode.NO_SCALE</code> when the user resizes the window, the Stage content maintains its defined size while the <code>stageWidth</code> property changes to reflect the new width size of the screen area occupied by the SWF file. (In the other scale modes, the <code>stageWidth</code> property always reflects the original width of the SWF file.) You can add an event listener for the <code>resize</code> event and then use the <code>stageWidth</code> property of the Stage class to determine the actual pixel dimension of the resized Flash runtime window. The event listener allows you to control how the screen content adjusts when the user resizes the window.</p>
		 * <p>Air for TV devices have slightly different behavior than desktop devices when you set the <code>stageWidth</code> property. If the <code>Stage.scaleMode</code> property is set to <code>StageScaleMode.NO_SCALE</code> and you set the <code>stageWidth</code> property, the stage width does not change until the next frame of the SWF.</p>
		 * <p><b>Note:</b> In an HTML page hosting the SWF file, both the <code>object</code> and <code>embed</code> tags' <code>width</code> attributes must be set to a percentage (such as <code>100%</code>), not pixels. If the settings are generated by JavaScript code, the <code>width</code> parameter of the <code>AC_FL_RunContent() </code> method must be set to a percentage, too. This percentage is applied to the <code>stageWidth</code> value.</p>
		 * 
		 * @return 
		 */
		public function get stageWidth():int {
			return window.innerWidth;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set stageWidth(value:int):void {
			_stageWidth = value;
		}

		/**
		 * <p> The orientations supported by the current device. </p>
		 * <p>You can use the orientation strings included in this list as parameters for the <code>setOrientation()</code> method. Setting an unsupported orientation fails without error.</p>
		 * <p>The possible orientations include:</p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>StageOrientation constant</th>
		 *    <th>Stage orientation</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>StageOrientation.DEFAULT</code></td>
		 *    <td>Set the stage orientation to the default orientation (right-side up).</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>StageOrientation.ROTATED_RIGHT</code></td>
		 *    <td>Set the stage orientation to be rotated right.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>StageOrientation.ROTATED_LEFT</code></td>
		 *    <td>Set the stage orientation to be rotated left.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>StageOrientation.UPSIDE_DOWN</code></td>
		 *    <td>Set the stage orientation to be rotated upside down.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * 
		 * @return 
		 */
		public function get supportedOrientations():Vector.<String> {
			return _supportedOrientations;
		}

		/**
		 * <p> Determines whether the children of the object are tab enabled. Enables or disables tabbing for the children of the object. The default is <code>true</code>. </p>
		 * <p><b>Note:</b> Do not use the <code>tabChildren</code> property with Flex. Instead, use the <code>mx.core.UIComponent.hasFocusableChildren</code> property.</p>
		 * 
		 * @return 
		 */
		override public function get tabChildren():Boolean {
			return _tabChildren;
		}

		/**
		 * @param value
		 * @return 
		 */
		override public function set tabChildren(value:Boolean):void {
			_tabChildren = value;
		}

		/**
		 * <p> Returns a TextSnapshot object for this DisplayObjectContainer instance. </p>
		 * 
		 * @return 
		 */
		override public function get textSnapshot():TextSnapshot {
			return _textSnapshot;
		}

		/**
		 * <p> This represents current state of vsync of underlying graphics solution We can enable/disable vsync, so we would immediately render without waiting for next VSYNC'ed state. Reading the property is valid at anytime. However, setting the property is valid only after the VsyncStateChangeAvailabilityEvent has fired. </p>
		 * 
		 * @return 
		 */
		public function get vsyncEnabled():Boolean {
			return _vsyncEnabled;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set vsyncEnabled(value:Boolean):void {
			_vsyncEnabled = value;
		}

		/**
		 * <p> Indicates the width of the display object, in pixels. The width is calculated based on the bounds of the content of the display object. When you set the <code>width</code> property, the <code>scaleX</code> property is adjusted accordingly, as shown in the following code: </p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *     var rect:Shape = new Shape();
		 *     rect.graphics.beginFill(0xFF0000);
		 *     rect.graphics.drawRect(0, 0, 100, 100);
		 *     trace(rect.scaleX) // 1;
		 *     rect.width = 200;
		 *     trace(rect.scaleX) // 2;</pre>
		 * </div>
		 * <p>Except for TextField and Video objects, a display object with no content (such as an empty sprite) has a width of 0, even if you try to set <code>width</code> to a different value.</p>
		 * 
		 * @return 
		 */
		override public function get width():Number {
			return stageWidth;
		}

		/**
		 * @param value
		 * @return 
		 */
		override public function set width(value:Number):void {
			_width = value;
		}

		/**
		 * <p> Indicates whether GPU compositing is available and in use. The <code>wmodeGPU</code> value is <code>true</code> <i>only</i> when all three of the following conditions exist: </p>
		 * <ul>
		 *  <li>GPU compositing has been requested.</li>
		 *  <li>GPU compositing is available.</li>
		 *  <li>GPU compositing is in use.</li>
		 * </ul>
		 * <p>Specifically, the <code>wmodeGPU</code> property indicates one of the following:</p>
		 * <ol>
		 *  <li>GPU compositing has not been requested or is unavailable. In this case, the <code>wmodeGPU</code> property value is <code>false</code>.</li>
		 *  <li>GPU compositing has been requested (if applicable and available), but the environment is operating in "fallback mode" (not optimal rendering) due to limitations of the content. In this case, the <code>wmodeGPU</code> property value is <code>true</code>.</li>
		 *  <li>GPU compositing has been requested (if applicable and available), and the environment is operating in the best mode. In this case, the <code>wmodeGPU</code> property value is also <code>true</code>.</li>
		 * </ol>
		 * <p>In other words, the <code>wmodeGPU</code> property identifies the capability and state of the rendering environment. For runtimes that do not support GPU compositing, such as AIR 1.5.2, the value is always <code>false</code>, because (as stated above) the value is <code>true</code> only when GPU compositing has been requested, is available, and is in use.</p>
		 * <p>The <code>wmodeGPU</code> property is useful to determine, at runtime, whether or not GPU compositing is in use. The value of <code>wmodeGPU</code> indicates if your content is going to be scaled by hardware, or not, so you can present graphics at the correct size. You can also determine if you're rendering in a fast path or not, so that you can adjust your content complexity accordingly.</p>
		 * <p>For Flash Player in a browser, GPU compositing can be requested by the value of <code>gpu</code> for the <code>wmode</code> HTML parameter in the page hosting the SWF file. For other configurations, GPU compositing can be requested in the header of a SWF file (set using SWF authoring tools).</p>
		 * <p>However, the <code>wmodeGPU</code> property does not identify the current rendering performance. Even if GPU compositing is "in use" the rendering process might not be operating in the best mode. To adjust your content for optimal rendering, use a Flash runtime debugger version, and set the <code>DisplayGPUBlendsetting</code> in your mm.cfg file.</p>
		 * <p><b>Note:</b> This property is always <code>false</code> when referenced from ActionScript that runs before the runtime performs its first rendering pass. For example, if you examine <code>wmodeGPU</code> from a script in Frame 1 of Adobe Flash Professional, and your SWF file is the first SWF file loaded in a new instance of the runtime, then the <code>wmodeGPU</code> value is <code>false</code>. To get an accurate value, wait until at least one rendering pass has occurred. If you write an event listener for the <code>exitFrame</code> event of any <code>DisplayObject</code>, the <code>wmodeGPU</code> value at is the correct value.</p>
		 * 
		 * @return 
		 */
		public function get wmodeGPU():Boolean {
			return _wmodeGPU;
		}
		
		override protected function createSVGElement():SVGElement {
			var ret:SVGElement = document.createElementNS(SVG_NAMESPACE, "svg") as SVGElement;
			document.body.appendChild(ret);
			ret.setAttribute("width", window.innerWidth);
			ret.setAttribute("height", window.innerHeight);
			return ret;
		}

		/**
		 * <p> Adds a child DisplayObject instance to this DisplayObjectContainer instance. The child is added to the front (top) of all other children in this DisplayObjectContainer instance. (To add a child to a specific index position, use the <code>addChildAt()</code> method.) </p>
		 * <p>If you add a child object that already has a different display object container as a parent, the object is removed from the child list of the other display object container. </p>
		 * <p><b>Note:</b> The command <code>stage.addChild()</code> can cause problems with a published SWF file, including security problems and conflicts with other loaded SWF files. There is only one Stage within a Flash runtime instance, no matter how many SWF files you load into the runtime. So, generally, objects should not be added to the Stage, directly, at all. The only object the Stage should contain is the root object. Create a DisplayObjectContainer to contain all of the items on the display list. Then, if necessary, add that DisplayObjectContainer instance to the Stage.</p>
		 * 
		 * @param child  — The DisplayObject instance to add as a child of this DisplayObjectContainer instance. 
		 * @return  — The DisplayObject instance that you pass in the  parameter. 
		 */
		/*override public function addChild(child:DisplayObject):DisplayObject {
			throw new Error("Not implemented");
		}*/

		/**
		 * <p> Adds a child DisplayObject instance to this DisplayObjectContainer instance. The child is added at the index position specified. An index of 0 represents the back (bottom) of the display list for this DisplayObjectContainer object. </p>
		 * <p>For example, the following example shows three display objects, labeled a, b, and c, at index positions 0, 2, and 1, respectively:</p>
		 * <p><img src="../../images/DisplayObjectContainer_layers.jpg" alt="b over c over a"></p>
		 * <p>If you add a child object that already has a different display object container as a parent, the object is removed from the child list of the other display object container. </p>
		 * 
		 * @param child  — The DisplayObject instance to add as a child of this DisplayObjectContainer instance. 
		 * @param index  — The index position to which the child is added. If you specify a currently occupied index position, the child object that exists at that position and all higher positions are moved up one position in the child list. 
		 * @return  — The DisplayObject instance that you pass in the  parameter. 
		 */
		/*override public function addChildAt(child:DisplayObject, index:int):DisplayObject {
			child = super.addChildAt(child, index);
			draw();
			return child;
		}*/

		/**
		 * <p> Registers an event listener object with an EventDispatcher object so that the listener receives notification of an event. <span>You can register event listeners on all nodes in the display list for a specific type of event, phase, and priority.</span> </p>
		 * <p>After you successfully register an event listener, you cannot change its priority through additional calls to <code>addEventListener()</code>. To change a listener's priority, you must first call <code>removeListener()</code>. Then you can register the listener again with the new priority level. </p>
		 * <p>Keep in mind that after the listener is registered, subsequent calls to <code>addEventListener()</code> with a different <code>type</code> or <code>useCapture</code> value result in the creation of a separate listener registration. <span>For example, if you first register a listener with <code>useCapture</code> set to <code>true</code>, it listens only during the capture phase. If you call <code>addEventListener()</code> again using the same listener object, but with <code>useCapture</code> set to <code>false</code>, you have two separate listeners: one that listens during the capture phase and another that listens during the target and bubbling phases.</span> </p>
		 * <p>You cannot register an event listener for only the target phase or the bubbling phase. Those phases are coupled during registration because bubbling applies only to the ancestors of the target node.</p>
		 * <p>If you no longer need an event listener, remove it by calling <code>removeEventListener()</code>, or memory problems could result. Event listeners are not automatically removed from memory because the garbage collector does not remove the listener as long as the dispatching object exists (unless the <code>useWeakReference</code> parameter is set to <code>true</code>).</p>
		 * <p>Copying an EventDispatcher instance does not copy the event listeners attached to it. (If your newly created node needs an event listener, you must attach the listener after creating the node.) However, if you move an EventDispatcher instance, the event listeners attached to it move along with it.</p>
		 * <p>If the event listener is being registered on a node while an event is being processed on this node, the event listener is not triggered during the current phase but can be triggered during a later phase in the event flow, such as the bubbling phase.</p>
		 * <p>If an event listener is removed from a node while an event is being processed on the node, it is still triggered by the current actions. After it is removed, the event listener is never invoked again (unless registered again for future processing). </p>
		 * 
		 * @param type  — The type of event. 
		 * @param listener  — The listener function that processes the event. This function must accept an Event object as its only parameter and must return nothing<span>, as this example shows:</span> <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>function(evt:Event):void</pre>
		 * </div> <p>The function can have any name.</p> 
		 * @param useCapture  — <span>Determines whether the listener works in the capture phase or the target and bubbling phases. If <code>useCapture</code> is set to <code>true</code>, the listener processes the event only during the capture phase and not in the target or bubbling phase. If <code>useCapture</code> is <code>false</code>, the listener processes the event only during the target or bubbling phase. To listen for the event in all three phases, call <code>addEventListener</code> twice, once with <code>useCapture</code> set to <code>true</code>, then again with <code>useCapture</code> set to <code>false</code>.</span> 
		 * @param priority  — The priority level of the event listener. The priority is designated by a signed 32-bit integer. The higher the number, the higher the priority. All listeners with priority <i>n</i> are processed before listeners of priority <i>n</i>-1. If two or more listeners share the same priority, they are processed in the order in which they were added. The default priority is 0. 
		 * @param useWeakReference  — Determines whether the reference to the listener is strong or weak. A strong reference (the default) prevents your listener from being garbage-collected. A weak reference does not. <p>Class-level member functions are not subject to garbage collection, so you can set <code>useWeakReference</code> to <code>true</code> for class-level member functions without subjecting them to garbage collection. If you set <code>useWeakReference</code> to <code>true</code> for a listener that is a nested inner function, the function will be garbage-collected and no longer persistent. If you create references to the inner function (save it in another variable) then it is not garbage-collected and stays persistent.</p> 
		 */
		/*override public function addEventListener(type:String, listener:Function, useCapture:Boolean = false, priority:int = 0, useWeakReference:Boolean = false):void {
			throw new Error("Not implemented");
		}*/

		/**
		 * <p> Sets keyboard focus to the interactive object specified by <code>objectToFocus</code>, with the focus direction specified by the <code>direction</code> parameter. </p>
		 * <p>The concept of focus direction must be defined by the application (or application framework). No intrinsic focus sorting of interactive objects exists, although you could use other available properties to establish an ordering principle. For example, you could sort interactive objects according to their positions on the Stage or in the display list. Calling <code>assignFocus()</code> is equivalent to setting the <code>Stage.focus</code> property, with the additional ability to indicate the direction from which the focus is being set.</p>
		 * <p>The <code>objectToFocus</code> will dispatch a <code>focusIn</code> event on receiving focus. The <code>direction</code> property of the FocusEvent object will report the setting of the <code>direction</code> parameter.</p>
		 * <p>If you assign an HTMLLoader object to the <code>objectToFocus</code> parameter, the HTMLLoader object selects the appropriate focusable object in the HTML DOM, based on the <code>direction</code> parameter value. If it is <code>FocusDirection.BOTTOM</code>, the focusable object in the HTML DOM at the end of the reading order is given focus. If it is <code>FocusDirection.TOP</code>, the focusable object in the HTML DOM at the beginning of the reading order is given focus. If it is <code>NONE</code>, the HTMLLoader object receives focus without changing its current focused element.</p>
		 * 
		 * @param objectToFocus  — The object to focus, or <code>null</code> to clear the focus from any element on the Stage. 
		 * @param direction  — The direction from which <code>objectToFocus</code> is being focused. Valid values are enumerated as constants in the FocusDirection class. 
		 */
		public function assignFocus(objectToFocus:InteractiveObject, direction:String):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Dispatches an event into the event flow. The event target is the EventDispatcher object upon which the <code>dispatchEvent()</code> method is called. </p>
		 * 
		 * @param event  — The Event object that is dispatched into the event flow. If the event is being redispatched, a clone of the event is created automatically. After an event is dispatched, its <code>target</code> property cannot be changed, so you must create a new copy of the event for redispatching to work. 
		 * @return  — A value of  if the event was successfully dispatched. A value of  indicates failure or that  was called on the event. 
		 */
		/*override public function dispatchEvent(event:flash.events.Event):Boolean {
			throw new Error("Not implemented");
		}*/

		/**
		 * <p> Checks whether the EventDispatcher object has any listeners registered for a specific type of event. This allows you to determine where an EventDispatcher object has altered handling of an event type in the event flow hierarchy. To determine whether a specific event type actually triggers an event listener, use <code>willTrigger()</code>. </p>
		 * <p>The difference between <code>hasEventListener()</code> and <code>willTrigger()</code> is that <code>hasEventListener()</code> examines only the object to which it belongs, whereas <code>willTrigger()</code> examines the entire event flow for the event specified by the <code>type</code> parameter. </p>
		 * <p>When <code>hasEventListener()</code> is called from a LoaderInfo object, only the listeners that the caller can access are considered.</p>
		 * 
		 * @param type  — The type of event. 
		 * @return  — A value of  if a listener of the specified type is registered;  otherwise. 
		 */
		/*override public function hasEventListener(type:String):Boolean {
			throw new Error("Not implemented");
		}*/

		/**
		 * <p> Calling the <code>invalidate()</code> method signals Flash runtimes to alert display objects on the next opportunity it has to render the display list (for example, when the playhead advances to a new frame). After you call the <code>invalidate()</code> method, when the display list is next rendered, the Flash runtime sends a <code>render</code> event to each display object that has registered to listen for the <code>render</code> event. You must call the <code>invalidate()</code> method each time you want the Flash runtime to send <code>render</code> events. </p>
		 * <p>The <code>render</code> event gives you an opportunity to make changes to the display list immediately before it is actually rendered. This lets you defer updates to the display list until the latest opportunity. This can increase performance by eliminating unnecessary screen updates.</p>
		 * <p>The <code>render</code> event is dispatched only to display objects that are in the same security domain as the code that calls the <code>stage.invalidate()</code> method, or to display objects from a security domain that has been granted permission via the <code>Security.allowDomain()</code> method.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/events/Event.html#RENDER" target="">flash.events.Event.RENDER</a>
		 * </div>
		 */
		public function invalidate():void {
			this.invalidated = true;
		}

		/**
		 * <p> Determines whether the <code>Stage.focus</code> property returns <code>null</code> for security reasons. In other words, <code>isFocusInaccessible</code> returns <code>true</code> if the object that has focus belongs to a security sandbox to which the SWF file does not have access. </p>
		 * 
		 * @return  —  if the object that has focus belongs to a security sandbox to which the SWF file does not have access. 
		 */
		public function isFocusInaccessible():Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Removes a child DisplayObject from the specified <code>index</code> position in the child list of the DisplayObjectContainer. The <code>parent</code> property of the removed child is set to <code>null</code>, and the object is garbage collected if no other references to the child exist. The index positions of any display objects above the child in the DisplayObjectContainer are decreased by 1. </p>
		 * <p>The garbage collector reallocates unused memory space. When a variable or object is no longer actively referenced or stored somewhere, the garbage collector sweeps through and wipes out the memory space it used to occupy if no other references to it exist.</p>
		 * 
		 * @param index  — The child index of the DisplayObject to remove. 
		 * @return  — The DisplayObject instance that was removed. 
		 */
		/*override public function removeChildAt(index:int):DisplayObject {
			throw new Error("Not implemented");
		}*/

		/**
		 * <p> Sets the stage to an orientation with the specified aspect ratio. </p>
		 * <p>If the stage orientation changes as a result of the method call, the Stage object dispatches an orientationChange event.</p>
		 * <p>To check whether device orientation is supported, check the value of the <code>Stage.supportsOrientantionChange</code> property.</p>
		 * <p><i>AIR profile support:</i> This feature is supported on mobile devices, but it is not supported on desktop operating systems or AIR for TV devices. You can test for support at run time using the <code>Stage.supportsOrientantionChange</code> property. See <a href="http://help.adobe.com/en_US/air/build/WS144092a96ffef7cc16ddeea2126bb46b82f-8000.html"> AIR Profile Support</a> for more information regarding API support across multiple profiles.</p>
		 * 
		 * @param newAspectRatio  — The type code for the desired aspect ratio (<code>StageAspectRatio.PORTRAIT</code>, <code>StageAspectRatio.LANDSCAPE</code>, or <code>StageAspectRatio.ANY</code>). 
		 */
		public function setAspectRatio(newAspectRatio:String):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Changes the position of an existing child in the display object container. This affects the layering of child objects. For example, the following example shows three display objects, labeled a, b, and c, at index positions 0, 1, and 2, respectively: </p>
		 * <p><img src="../../images/DisplayObjectContainerSetChildIndex1.jpg" alt="c over b over a"></p>
		 * <p>When you use the <code>setChildIndex()</code> method and specify an index position that is already occupied, the only positions that change are those in between the display object's former and new position. All others will stay the same. If a child is moved to an index LOWER than its current index, all children in between will INCREASE by 1 for their index reference. If a child is moved to an index HIGHER than its current index, all children in between will DECREASE by 1 for their index reference. For example, if the display object container in the previous example is named <code>container</code>, you can swap the position of the display objects labeled a and b by calling the following code:</p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>container.setChildIndex(container.getChildAt(1), 0);</pre>
		 * </div>
		 * <p>This code results in the following arrangement of objects:</p>
		 * <p><img src="../../images/DisplayObjectContainerSetChildIndex2.jpg" alt="c over a over b"></p>
		 * 
		 * @param child  — The child DisplayObject instance for which you want to change the index number. 
		 * @param index  — The resulting index number for the <code>child</code> display object. 
		 */
		override public function setChildIndex(child:DisplayObject, index:int):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets the stage to the specified orientation. </p>
		 * <p>Set the <code>newOrientation</code> parameter to one of the following four values defined as constants in the StageOrientation class:</p>
		 * <p> </p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>StageOrientation constant</th>
		 *    <th>Stage orientation</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>StageOrientation.DEFAULT</code></td>
		 *    <td>Set the stage orientation to the default orientation (right-side up).</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>StageOrientation.ROTATED_RIGHT</code></td>
		 *    <td>Set the stage orientation to be rotated right.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>StageOrientation.ROTATED_LEFT</code></td>
		 *    <td>Set the stage orientation to be rotated left.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>StageOrientation.UPSIDE_DOWN</code></td>
		 *    <td>Set the stage orientation to be rotated upside down.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p>Do not set the parameter to <code>StageOrientation.UNKNOWN</code> or any string value other than those listed in the table.</p>
		 * <p>To check whether changing device orientation is supported, check the value of the <code>Stage.supportsOrientantionChange</code> property. Check the list provided by the <code>supportedOrientations</code> property to determine which orientations are supported by the current device.</p>
		 * <p>Setting the orientation is an asynchronous operation. It is not guaranteed to be complete immediately after you call the <code>setOrientation()</code> method. Add an event listener for the <code>orientationChange</code> event to determine when the orientation change is complete.</p>
		 * <p><b>Important:</b> The <code>setOrientation()</code> method was not supported on Android devices before AIR 2.6.</p>
		 * <b>Note:</b>
		 * <code>setOrientation()</code>
		 * <i>not</i>
		 * <code>orientationChanging</code>
		 * 
		 * @param newOrientation  — The new orientation of the stage. 
		 */
		public function setOrientation(newOrientation:String):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Swaps the z-order (front-to-back order) of the child objects at the two specified index positions in the child list. All other child objects in the display object container remain in the same index positions. </p>
		 * 
		 * @param index1  — The index position of the first child object. 
		 * @param index2  — The index position of the second child object. 
		 */
		override public function swapChildrenAt(index1:int, index2:int):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Checks whether an event listener is registered with this EventDispatcher object or any of its ancestors for the specified event type. This method returns <code>true</code> if an event listener is triggered during any phase of the event flow when an event of the specified type is dispatched to this EventDispatcher object or any of its descendants. </p>
		 * <p>The difference between the <code>hasEventListener()</code> and the <code>willTrigger()</code> methods is that <code>hasEventListener()</code> examines only the object to which it belongs, whereas the <code>willTrigger()</code> method examines the entire event flow for the event specified by the <code>type</code> parameter. </p>
		 * <p>When <code>willTrigger()</code> is called from a LoaderInfo object, only the listeners that the caller can access are considered.</p>
		 * 
		 * @param type  — The type of event. 
		 * @return  — A value of  if a listener of the specified type will be triggered;  otherwise. 
		 */
		override public function willTrigger(type:String):Boolean {
			throw new Error("Not implemented");
		}
		
		private function onResize(e:*):void {
			svgElement.setAttribute("width", window.innerWidth);
			svgElement.setAttribute("height", window.innerHeight);
			this.dispatchEvent(new Event(Event.RESIZE));
		}
		
		
		private function onFrame(e:TimerEvent):void {
			DisplayObject.frameEvent(Event.ENTER_FRAME);
			DisplayObject.frameEvent(Event.FRAME_CONSTRUCTED);
			if (invalidated)
				DisplayObject.frameEvent(Event.RENDER);
			DisplayObject.frameEvent(Event.EXIT_FRAME);
		}


		/**
		 * <p> Whether the application supports changes in the stage orientation (and device rotation). Currently, this property is only <code>true</code> in AIR applications running on mobile devices. </p>
		 * 
		 * @return 
		 */
		public static function get supportsOrientationChange():Boolean {
			return _supportsOrientationChange;
		}
		
		public static function get stage():Stage {
			return _stage;
		}
	}
}
