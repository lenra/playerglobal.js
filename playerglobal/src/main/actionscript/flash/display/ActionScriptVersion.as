package flash.display {
	/**
	 *  The ActionScriptVersion class is an enumeration of constant values that indicate the language version of a loaded SWF file. The language version constants are provided for use in checking the <code>actionScriptVersion</code> property of a flash.display.LoaderInfo object. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="LoaderInfo.html#actionScriptVersion" target="">flash.display.LoaderInfo.actionScriptVersion</a>
	 * </div><br><hr>
	 */
	public class ActionScriptVersion {
		/**
		 * <p> ActionScript language version 2.0 and earlier. </p>
		 */
		public static const ACTIONSCRIPT2:uint = 2;
		/**
		 * <p> ActionScript language version 3.0. </p>
		 */
		public static const ACTIONSCRIPT3:uint = 3;
	}
}
