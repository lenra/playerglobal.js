package flash.display {
	import flash.geom.Matrix;

	/**
	 *  The Graphics class contains a set of methods that you can use to create a vector shape. Display objects that support drawing include Sprite and Shape objects. Each of these classes includes a <code>graphics</code> property that is a Graphics object. The following are among those helper functions provided for ease of use: <code>drawRect()</code>, <code>drawRoundRect()</code>, <code>drawCircle()</code>, and <code>drawEllipse()</code>. <p>You cannot create a Graphics object directly from ActionScript code. If you call <code>new Graphics()</code>, an exception is thrown.</p> <p>The Graphics class is final; it cannot be subclassed.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7dd9.html" target="_blank">Drawing lines and curves</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7dd8.html" target="_blank">Drawing shapes using built-in methods</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7dd7.html" target="_blank">Creating gradient lines and fills</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b8e525-7ffa.html" target="_blank">Defining a Matrix object for use with a gradient</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7dd6.html" target="_blank">Using the Math class with drawing methods</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7dcf.html" target="_blank">Drawing API example: Algorithmic Visual Generator</a>
	 * </div><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7dce.html" target="_blank">Basics of the drawing API</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7dda.html" target="_blank">The Graphics class</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WSE06FE962-09BE-4460-B020-5CDC2E54C499.html" target="_blank">Advanced use of the drawing API</a>
	 *  <br>
	 * </div><br><hr>
	 */
	public class Graphics {
		private const graphicsData:Vector.<IGraphicsData> = new Vector.<IGraphicsData>();
		private var currentPath:GraphicsPath = null;
		private var currentFill:IGraphicsFill = null;
		private var currentStroke:IGraphicsFill = null;

		private var _svgParent:SVGElement;
		private var _svgElement:SVGGElement;
		
		public function get svgParent():SVGElement {
			return _svgParent;
		}
		
		public function set svgParent(value:SVGElement):void {
			_svgParent = value;
		}
		
		/**
		 * <p> Fills a drawing area with a bitmap image. The bitmap can be repeated or tiled to fill the area. The fill remains in effect until you call the <code>beginFill()</code>, <code>beginBitmapFill()</code>, <code>beginGradientFill()</code>, or <code>beginShaderFill()</code> method. Calling the <code>clear()</code> method clears the fill. </p>
		 * <p>The application renders the fill whenever three or more points are drawn, or when the <code>endFill()</code> method is called. </p>
		 * 
		 * @param bitmap  — A transparent or opaque bitmap image that contains the bits to be displayed. 
		 * @param matrix  — A matrix object (of the flash.geom.Matrix class), which you can use to define transformations on the bitmap. For example, you can use the following matrix to rotate a bitmap by 45 degrees (pi/4 radians): <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *      matrix = new flash.geom.Matrix(); 
		 *      matrix.rotate(Math.PI / 4);
		 *      </pre>
		 * </div> 
		 * @param repeat  — If <code>true</code>, the bitmap image repeats in a tiled pattern. If <code>false</code>, the bitmap image does not repeat, and the edges of the bitmap are used for any fill area that extends beyond the bitmap. <p>For example, consider the following bitmap (a 20 x 20-pixel checkerboard pattern):</p> <p><img src="../../images/movieClip_beginBitmapFill_repeat_1.jpg" alt="20 by 20 pixel checkerboard"></p> <p>When <code>repeat</code> is set to <code>true</code> (as in the following example), the bitmap fill repeats the bitmap:</p> <p><img src="../../images/movieClip_beginBitmapFill_repeat_2.jpg" alt="60 by 60 pixel checkerboard"></p> <p>When <code>repeat</code> is set to <code>false</code>, the bitmap fill uses the edge pixels for the fill area outside the bitmap:</p> <p><img src="../../images/movieClip_beginBitmapFill_repeat_3.jpg" alt="60 by 60 pixel image with no repeating"></p> 
		 * @param smooth  — If <code>false</code>, upscaled bitmap images are rendered by using a nearest-neighbor algorithm and look pixelated. If <code>true</code>, upscaled bitmap images are rendered by using a bilinear algorithm. Rendering by using the nearest neighbor algorithm is faster. 
		 */
		public function beginBitmapFill(bitmap:BitmapData, matrix:Matrix = null, repeat:Boolean = true, smooth:Boolean = false):void {
			//closePath();
			currentFill = new GraphicsBitmapFill(bitmap, matrix, repeat, smooth);
			graphicsData.push(currentFill);
		}

		/**
		 * <p> Specifies a simple one-color fill that subsequent calls to other Graphics methods (such as <code>lineTo()</code> or <code>drawCircle()</code>) use when drawing. The fill remains in effect until you call the <code>beginFill()</code>, <code>beginBitmapFill()</code>, <code>beginGradientFill()</code>, or <code>beginShaderFill()</code> method. Calling the <code>clear()</code> method clears the fill. </p>
		 * <p>The application renders the fill whenever three or more points are drawn, or when the <code>endFill()</code> method is called.</p>
		 * 
		 * @param color  — The color of the fill (0xRRGGBB). 
		 * @param alpha  — The alpha value of the fill (0.0 to 1.0). 
		 */
		public function beginFill(color:uint, alpha:Number = 1.0):void {
			//closePath();
			currentFill = new GraphicsSolidFill(color, alpha);
			graphicsData.push(currentFill);
		}

		/**
		 * <p> Specifies a gradient fill used by subsequent calls to other Graphics methods (such as <code>lineTo()</code> or <code>drawCircle()</code>) for the object. The fill remains in effect until you call the <code>beginFill()</code>, <code>beginBitmapFill()</code>, <code>beginGradientFill()</code>, or <code>beginShaderFill()</code> method. Calling the <code>clear()</code> method clears the fill. </p>
		 * <p>The application renders the fill whenever three or more points are drawn, or when the <code>endFill()</code> method is called. </p>
		 * 
		 * @param type  — A value from the GradientType class that specifies which gradient type to use: <code>GradientType.LINEAR</code> or <code>GradientType.RADIAL</code>. 
		 * @param colors  — An array of RGB hexadecimal color values used in the gradient; for example, red is 0xFF0000, blue is 0x0000FF, and so on. You can specify up to 15 colors. For each color, specify a corresponding value in the alphas and ratios parameters. 
		 * @param alphas  — An array of alpha values for the corresponding colors in the colors array; valid values are 0 to 1. If the value is less than 0, the default is 0. If the value is greater than 1, the default is 1. 
		 * @param ratios  — An array of color distribution ratios; valid values are 0-255. This value defines the percentage of the width where the color is sampled at 100%. The value 0 represents the left position in the gradient box, and 255 represents the right position in the gradient box. <p><b>Note:</b> This value represents positions in the gradient box, not the coordinate space of the final gradient, which can be wider or thinner than the gradient box. Specify a value for each value in the <code>colors</code> parameter. </p> <p>For example, for a linear gradient that includes two colors, blue and green, the following example illustrates the placement of the colors in the gradient based on different values in the <code>ratios</code> array:</p> <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th><code>ratios</code></th>
		 *    <th>Gradient</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>[0, 127]</code></td>
		 *    <td><img src="../../images/gradient-ratios-1.jpg" alt="linear gradient blue to green with ratios 0 and 127"></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>[0, 255]</code></td>
		 *    <td><img src="../../images/gradient-ratios-2.jpg" alt="linear gradient blue to green with ratios 0 and 255"></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>[127, 255]</code></td>
		 *    <td><img src="../../images/gradient-ratios-3.jpg" alt="linear gradient blue to green with ratios 127 and 255"></td>
		 *   </tr>
		 *  </tbody>
		 * </table> <p>The values in the array must increase sequentially; for example, <code>[0, 63, 127, 190, 255]</code>. </p> 
		 * @param matrix  — A transformation matrix as defined by the flash.geom.Matrix class. The flash.geom.Matrix class includes a <code>createGradientBox()</code> method, which lets you conveniently set up the matrix for use with the <code>beginGradientFill()</code> method. 
		 * @param spreadMethod  — A value from the SpreadMethod class that specifies which spread method to use, either: <code>SpreadMethod.PAD</code>, <code>SpreadMethod.REFLECT</code>, or <code>SpreadMethod.REPEAT</code>. <p>For example, consider a simple linear gradient between two colors:</p> <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *      import flash.geom.*
		 *      import flash.display.*
		 *      var fillType:String = GradientType.LINEAR;
		 *      var colors:Array = [0xFF0000, 0x0000FF];
		 *      var alphas:Array = [1, 1];
		 *      var ratios:Array = [0x00, 0xFF];
		 *      var matr:Matrix = new Matrix();
		 *      matr.createGradientBox(20, 20, 0, 0, 0);
		 *      var spreadMethod:String = SpreadMethod.PAD;
		 *      this.graphics.beginGradientFill(fillType, colors, alphas, ratios, matr, spreadMethod);        
		 *      this.graphics.drawRect(0,0,100,100);
		 *      </pre>
		 * </div> <p>This example uses <code>SpreadMethod.PAD</code> for the spread method, and the gradient fill looks like the following:</p> <p><img src="../../images/beginGradientFill_spread_pad.jpg" alt="linear gradient with SpreadMethod.PAD"></p> <p>If you use <code>SpreadMethod.REFLECT</code> for the spread method, the gradient fill looks like the following:</p> <p><img src="../../images/beginGradientFill_spread_reflect.jpg" alt="linear gradient with SpreadMethod.REFLECT"></p> <p>If you use <code>SpreadMethod.REPEAT</code> for the spread method, the gradient fill looks like the following:</p> <p><img src="../../images/beginGradientFill_spread_repeat.jpg" alt="linear gradient with SpreadMethod.REPEAT"></p> 
		 * @param interpolationMethod  — A value from the InterpolationMethod class that specifies which value to use: <code>InterpolationMethod.LINEAR_RGB</code> or <code>InterpolationMethod.RGB</code> <p>For example, consider a simple linear gradient between two colors (with the <code>spreadMethod</code> parameter set to <code>SpreadMethod.REFLECT</code>). The different interpolation methods affect the appearance as follows: </p> <table class="+ topic/table adobe-d/adobetable ">
		 *  <tbody>
		 *   <tr>
		 *    <td align="center"> <img src="../../images/beginGradientFill_interp_linearrgb.jpg" alt="linear gradient with InterpolationMethod.LINEAR_RGB"> </td>
		 *    <td align="center"> <img src="../../images/beginGradientFill_interp_rgb.jpg" alt="linear gradient with InterpolationMethod.RGB"> </td>
		 *   </tr>
		 *   <tr>
		 *    <td align="center"><code>InterpolationMethod.LINEAR_RGB</code></td>
		 *    <td align="center"><code>InterpolationMethod.RGB</code></td>
		 *   </tr>
		 *  </tbody>
		 * </table> 
		 * @param focalPointRatio  — A number that controls the location of the focal point of the gradient. 0 means that the focal point is in the center. 1 means that the focal point is at one border of the gradient circle. -1 means that the focal point is at the other border of the gradient circle. A value less than -1 or greater than 1 is rounded to -1 or 1. For example, the following example shows a <code>focalPointRatio</code> set to 0.75: <p><img src="../../images/radial_sketch.jpg" alt="radial gradient with focalPointRatio set to 0.75"> </p> 
		 */
		public function beginGradientFill(type:String, colors:Array, alphas:Array, ratios:Array, matrix:Matrix = null, spreadMethod:String = "pad", interpolationMethod:String = "rgb", focalPointRatio:Number = 0):void {
			//closePath();
			currentFill = new GraphicsGradientFill(type, colors, alphas, ratios, matrix, spreadMethod, interpolationMethod);
			graphicsData.push(currentFill);
		}

		/**
		 * <p> Specifies a shader fill used by subsequent calls to other Graphics methods (such as <code>lineTo()</code> or <code>drawCircle()</code>) for the object. The fill remains in effect until you call the <code>beginFill()</code>, <code>beginBitmapFill()</code>, <code>beginGradientFill()</code>, or <code>beginShaderFill()</code> method. Calling the <code>clear()</code> method clears the fill. </p>
		 * <p>The application renders the fill whenever three or more points are drawn, or when the <code>endFill()</code> method is called.</p>
		 * <p>Shader fills are not supported under GPU rendering; filled areas will be colored cyan.</p>
		 * 
		 * @param shader  — The shader to use for the fill. This Shader instance is not required to specify an image input. However, if an image input is specified in the shader, the input must be provided manually. To specify the input, set the <code>input</code> property of the corresponding ShaderInput property of the <code>Shader.data</code> property. <p>When you pass a Shader instance as an argument the shader is copied internally. The drawing fill operation uses that internal copy, not a reference to the original shader. Any changes made to the shader, such as changing a parameter value, input, or bytecode, are not applied to the copied shader that's used for the fill.</p> 
		 * @param matrix  — A matrix object (of the flash.geom.Matrix class), which you can use to define transformations on the shader. For example, you can use the following matrix to rotate a shader by 45 degrees (pi/4 radians): <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *      matrix = new flash.geom.Matrix(); 
		 *      matrix.rotate(Math.PI / 4);
		 *      </pre>
		 * </div> <p>The coordinates received in the shader are based on the matrix that is specified for the <code>matrix</code> parameter. For a default (<code>null</code>) matrix, the coordinates in the shader are local pixel coordinates which can be used to sample an input.</p> 
		 */
		public function beginShaderFill(shader:Shader, matrix:Matrix = null):void {
			closePath();
			currentFill = new GraphicsShaderFill(shader, matrix);
			graphicsData.push(currentFill);
		}

		/**
		 * <p> Clears the graphics that were drawn to this Graphics object, and resets fill and line style settings. </p>
		 */
		public function clear():void {
			closePath();
			currentFill = null;
			currentStroke = null;
			graphicsData.length = 0
			if (_svgElement != null) {
				svgParent.removeChild(_svgElement);
				_svgElement = null;
			}
		}

		/**
		 * <p> Copies all of drawing commands from the source Graphics object into the calling Graphics object. </p>
		 * 
		 * @param sourceGraphics  — The Graphics object from which to copy the drawing commands. 
		 */
		public function copyFrom(sourceGraphics:Graphics):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Draws a cubic Bezier curve from the current drawing position to the specified anchor point. Cubic Bezier curves consist of two anchor points and two control points. The curve interpolates the two anchor points and curves toward the two control points. </p>
		 * <p><img src="../../images/cubic_bezier.png" alt="cubic bezier"></p>
		 * <p> The four points you use to draw a cubic Bezier curve with the <code>cubicCurveTo()</code> method are as follows: </p>
		 * <ul>
		 *  <li>The current drawing position is the first anchor point. </li>
		 *  <li>The <code>anchorX</code> and <code>anchorY</code> parameters specify the second anchor point.</li>
		 *  <li>The <code>controlX1</code> and <code>controlY1</code> parameters specify the first control point.</li>
		 *  <li>The <code>controlX2</code> and <code>controlY2</code> parameters specify the second control point.</li>
		 * </ul>
		 * <p> If you call the <code>cubicCurveTo()</code> method before calling the <code>moveTo()</code> method, your curve starts at position (0, 0). </p>
		 * <p> If the <code>cubicCurveTo()</code> method succeeds, the Flash runtime sets the current drawing position to (<code>anchorX</code>, <code>anchorY</code>). If the <code>cubicCurveTo()</code> method fails, the current drawing position remains unchanged. </p>
		 * <p> If your movie clip contains content created with the Flash drawing tools, the results of calls to the <code>cubicCurveTo()</code> method are drawn underneath that content. </p>
		 * 
		 * @param controlX1  — Specifies the horizontal position of the first control point relative to the registration point of the parent display object. 
		 * @param controlY1  — Specifies the vertical position of the first control point relative to the registration point of the parent display object. 
		 * @param controlX2  — Specifies the horizontal position of the second control point relative to the registration point of the parent display object. 
		 * @param controlY2  — Specifies the vertical position of the second control point relative to the registration point of the parent display object. 
		 * @param anchorX  — Specifies the horizontal position of the anchor point relative to the registration point of the parent display object. 
		 * @param anchorY  — Specifies the vertical position of the anchor point relative to the registration point of the parent display object. 
		 */
		public function cubicCurveTo(controlX1:Number, controlY1:Number, controlX2:Number, controlY2:Number, anchorX:Number, anchorY:Number):void {
			initPath();
			currentPath.cubicCurveTo(controlX1, controlY1, controlX2, controlY2, anchorX, anchorY);
		}

		/**
		 * <p> Draws a quadratic Bezier curve using the current line style from the current drawing position to (anchorX, anchorY) and using the control point that (<code>controlX</code>, <code>controlY</code>) specifies. The current drawing position is then set to (<code>anchorX</code>, <code>anchorY</code>). If the movie clip in which you are drawing contains content created with the Flash drawing tools, calls to the <code>curveTo()</code> method are drawn underneath this content. If you call the <code>curveTo()</code> method before any calls to the <code>moveTo()</code> method, the default of the current drawing position is (0, 0). If any of the parameters are missing, this method fails and the current drawing position is not changed. </p>
		 * <p>The curve drawn is a quadratic Bezier curve. Quadratic Bezier curves consist of two anchor points and one control point. The curve interpolates the two anchor points and curves toward the control point. </p>
		 * <p><img src="../../images/quad_bezier.png" alt="quadratic bezier"></p>
		 * 
		 * @param controlX  — A number that specifies the horizontal position of the control point relative to the registration point of the parent display object. 
		 * @param controlY  — A number that specifies the vertical position of the control point relative to the registration point of the parent display object. 
		 * @param anchorX  — A number that specifies the horizontal position of the next anchor point relative to the registration point of the parent display object. 
		 * @param anchorY  — A number that specifies the vertical position of the next anchor point relative to the registration point of the parent display object. 
		 */
		public function curveTo(controlX:Number, controlY:Number, anchorX:Number, anchorY:Number):void {
			initPath();
			currentPath.curveTo(controlX, controlY, anchorX, anchorY);
		}

		/**
		 * <p> Draws a circle. Set the line style, fill, or both before you call the <code>drawCircle()</code> method, by calling the <code>linestyle()</code>, <code>lineGradientStyle()</code>, <code>beginFill()</code>, <code>beginGradientFill()</code>, or <code>beginBitmapFill()</code> method. </p>
		 * 
		 * @param x  — The <i>x</i> location of the center of the circle relative to the registration point of the parent display object (in pixels). 
		 * @param y  — The <i>y</i> location of the center of the circle relative to the registration point of the parent display object (in pixels). 
		 * @param radius  — The radius of the circle (in pixels). 
		 */
		public function drawCircle(x:Number, y:Number, radius:Number):void {
			drawEllipse(x, y, radius, radius);
		}

		/**
		 * <p> Draws an ellipse. Set the line style, fill, or both before you call the <code>drawEllipse()</code> method, by calling the <code>linestyle()</code>, <code>lineGradientStyle()</code>, <code>beginFill()</code>, <code>beginGradientFill()</code>, or <code>beginBitmapFill()</code> method. </p>
		 * 
		 * @param x  — The <i>x</i> location of the top-left of the bounding-box of the ellipse relative to the registration point of the parent display object (in pixels). 
		 * @param y  — The <i>y</i> location of the top left of the bounding-box of the ellipse relative to the registration point of the parent display object (in pixels). 
		 * @param width  — The width of the ellipse (in pixels). 
		 * @param height  — The height of the ellipse (in pixels). 
		 */
		public function drawEllipse(x:Number, y:Number, width:Number, height:Number):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Submits a series of IGraphicsData instances for drawing. This method accepts a Vector containing objects including paths, fills, and strokes that implement the IGraphicsData interface. A Vector of IGraphicsData instances can refer to a part of a shape, or a complex fully defined set of data for rendering a complete shape. </p>
		 * <p> Graphics paths can contain other graphics paths. If the <code>graphicsData</code> Vector includes a path, that path and all its sub-paths are rendered during this operation. </p>
		 * 
		 * @param graphicsData  — A Vector containing graphics objects, each of which much implement the IGraphicsData interface. 
		 */
		public function drawGraphicsData(graphicsData:Vector.<IGraphicsData>):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Submits a series of commands for drawing. The <code>drawPath()</code> method accepts a Vector of individual <code>moveTo()</code>, <code>lineTo()</code>, and <code>curveTo()</code> drawing commands, combining them into a single call. The <code>drawPath()</code> method parameters combine drawing commands with x- and y-coordinate value pairs and a drawing direction. The drawing commands are integers, represented as constants defined in the GraphicsPathCommand class. The x- and y-coordinate value pairs are Numbers in an array where each pair defines a coordinate location. The drawing direction is a value from the GraphicsPathWinding class. </p>
		 * <p>Generally, drawings render faster with <code>drawPath()</code> than with a series of individual <code>lineTo()</code> and <code>curveTo()</code> method calls.</p>
		 * <p>The <code>drawPath()</code> method uses a uses a floating computation so rotation and scaling of shapes is more accurate and gives better results. However, curves submitted using the <code>drawPath()</code> method can have small sub-pixel alignment errors when used in conjunction with the <code>lineTo()</code> and <code>curveTo()</code> methods.</p>
		 * <p>The <code>drawPath()</code> method also uses slightly different rules for filling and drawing lines. They are:</p>
		 * <ul>
		 *  <li>When a fill is applied to rendering a path: 
		 *   <ul>
		 *    <li>A sub-path of less than 3 points is not rendered. (But note that the stroke rendering will still occur, consistent with the rules for strokes below.)</li>
		 *    <li>A sub-path that isn't closed (the end point is not equal to the begin point) is implicitly closed.</li>
		 *   </ul> </li>
		 *  <li>When a stroke is applied to rendering a path: 
		 *   <ul>
		 *    <li>The sub-paths can be composed of any number of points.</li>
		 *    <li>The sub-path is never implicitly closed.</li>
		 *   </ul> </li>
		 * </ul>
		 * 
		 * @param commands  — A Vector of integers representing drawing commands. The set of accepted values is defined by the constants in the GraphicsPathCommand class. 
		 * @param data  — A Vector of Number instances where each pair of numbers is treated as a coordinate location (an x, y pair). The x- and y-coordinate value pairs are not Point objects; the <code>data</code> vector is a series of numbers where each group of two numbers represents a coordinate location. 
		 * @param winding  — Specifies the winding rule using a value defined in the GraphicsPathWinding class. 
		 */
		public function drawPath(commands:Vector.<int>, data:Vector.<Number>, winding:String = "evenOdd"):void {
			closePath();
			graphicsData.push(new GraphicsPath(commands, data, winding));
		}

		/**
		 * <p> Draws a rectangle. Set the line style, fill, or both before you call the <code>drawRect()</code> method, by calling the <code>linestyle()</code>, <code>lineGradientStyle()</code>, <code>beginFill()</code>, <code>beginGradientFill()</code>, or <code>beginBitmapFill()</code> method. </p>
		 * 
		 * @param x  — A number indicating the horizontal position relative to the registration point of the parent display object (in pixels). 
		 * @param y  — A number indicating the vertical position relative to the registration point of the parent display object (in pixels). 
		 * @param width  — The width of the rectangle (in pixels). 
		 * @param height  — The height of the rectangle (in pixels). 
		 */
		public function drawRect(x:Number, y:Number, width:Number, height:Number):void {
			if (width == 0 || height == 0)
				return;
			initPath();
			currentPath.moveTo(x, y);
			currentPath.lineTo(x + width, y);
			currentPath.lineTo(x + width, y + height);
			currentPath.lineTo(x, y + height);
			currentPath.lineTo(x, y);
		}

		/**
		 * <p> Draws a rounded rectangle. Set the line style, fill, or both before you call the <code>drawRoundRect()</code> method, by calling the <code>linestyle()</code>, <code>lineGradientStyle()</code>, <code>beginFill()</code>, <code>beginGradientFill()</code>, or <code>beginBitmapFill()</code> method. </p>
		 * 
		 * @param x  — A number indicating the horizontal position relative to the registration point of the parent display object (in pixels). 
		 * @param y  — A number indicating the vertical position relative to the registration point of the parent display object (in pixels). 
		 * @param width  — The width of the round rectangle (in pixels). 
		 * @param height  — The height of the round rectangle (in pixels). 
		 * @param ellipseWidth  — The width of the ellipse used to draw the rounded corners (in pixels). 
		 * @param ellipseHeight  — The height of the ellipse used to draw the rounded corners (in pixels). Optional; if no value is specified, the default value matches that provided for the <code>ellipseWidth</code> parameter. 
		 */
		public function drawRoundRect(x:Number, y:Number, width:Number, height:Number, ellipseWidth:Number, ellipseHeight:Number = NaN):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Renders a set of triangles, typically to distort bitmaps and give them a three-dimensional appearance. The <code>drawTriangles()</code> method maps either the current fill, or a bitmap fill, to the triangle faces using a set of (u,v) coordinates. </p>
		 * <p> Any type of fill can be used, but if the fill has a transform matrix that transform matrix is ignored. </p>
		 * <p> A <code>uvtData</code> parameter improves texture mapping when a bitmap fill is used. </p>
		 * 
		 * @param vertices  — A Vector of Numbers where each pair of numbers is treated as a coordinate location (an x, y pair). The <code>vertices</code> parameter is required. 
		 * @param indices  — A Vector of integers or indexes, where every three indexes define a triangle. If the <code>indexes</code> parameter is null then every three vertices (six x,y pairs in the <code>vertices</code> Vector) defines a triangle. Otherwise each index refers to a vertex, which is a pair of numbers in the <code>vertices</code> Vector. For example <code>indexes[1]</code> refers to (<code>vertices[2]</code>, <code>vertices[3]</code>). The <code>indexes</code> parameter is optional, but indexes generally reduce the amount of data submitted and the amount of data computed. 
		 * @param uvtData  — A Vector of normalized coordinates used to apply texture mapping. Each coordinate refers to a point on the bitmap used for the fill. You must have one UV or one UVT coordinate per vertex. In UV coordinates, (0,0) is the upper left of the bitmap, and (1,1) is the lower right of the bitmap. <p>If the length of this vector is twice the length of the <code>vertices</code> vector then normalized coordinates are used without perspective correction. </p> <p>If the length of this vector is three times the length of the <code>vertices</code> vector then the third coordinate is interpreted as 't' (the distance from the eye to the texture in eye space). This helps the rendering engine correctly apply perspective when mapping textures in three dimensions.</p> <p>If the <code>uvtData</code> parameter is null, then normal fill rules (and any fill type) apply.</p> 
		 * @param culling  — Specifies whether to render triangles that face in a specified direction. This parameter prevents the rendering of triangles that cannot be seen in the current view. This parameter can be set to any value defined by the TriangleCulling class. 
		 */
		public function drawTriangles(vertices:Vector.<Number>, indices:Vector.<int> = null, uvtData:Vector.<Number> = null, culling:String = "none"):void {
			closePath();
			graphicsData.push(new GraphicsTrianglePath(vertices, indices, uvtData, culling));
		}

		/**
		 * <p> Applies a fill to the lines and curves that were added since the last call to the <code>beginFill()</code>, <code>beginGradientFill()</code>, or <code>beginBitmapFill()</code> method. Flash uses the fill that was specified in the previous call to the <code>beginFill()</code>, <code>beginGradientFill()</code>, or <code>beginBitmapFill()</code> method. If the current drawing position does not equal the previous position specified in a <code>moveTo()</code> method and a fill is defined, the path is closed with a line and then filled. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="Graphics.html#beginFill()" target="">beginFill()</a>
		 *  <br>
		 *  <a href="Graphics.html#beginBitmapFill()" target="">beginBitmapFill()</a>
		 *  <br>
		 *  <a href="Graphics.html#beginGradientFill()" target="">beginGradientFill()</a>
		 * </div>
		 */
		public function endFill():void {
			closePath();
			currentFill = null;
			graphicsData.push(new GraphicsEndFill());
		}

		/**
		 * <p> Specifies a bitmap to use for the line stroke when drawing lines. </p>
		 * <p>The bitmap line style is used for subsequent calls to Graphics methods such as the <code>lineTo()</code> method or the <code>drawCircle()</code> method. The line style remains in effect until you call the <code>lineStyle()</code> or <code>lineGradientStyle()</code> methods, or the <code>lineBitmapStyle()</code> method again with different parameters. </p>
		 * <p>You can call the <code>lineBitmapStyle()</code> method in the middle of drawing a path to specify different styles for different line segments within a path. </p>
		 * <p>Call the <code>lineStyle()</code> method before you call the <code>lineBitmapStyle()</code> method to enable a stroke, or else the value of the line style is <code>undefined</code>.</p>
		 * <p>Calls to the <code>clear()</code> method set the line style back to <code>undefined</code>. </p>
		 * 
		 * @param bitmap  — The bitmap to use for the line stroke. 
		 * @param matrix  — An optional transformation matrix as defined by the flash.geom.Matrix class. The matrix can be used to scale or otherwise manipulate the bitmap before applying it to the line style. 
		 * @param repeat  — Whether to repeat the bitmap in a tiled fashion. 
		 * @param smooth  — Whether smoothing should be applied to the bitmap. 
		 */
		public function lineBitmapStyle(bitmap:BitmapData, matrix:Matrix = null, repeat:Boolean = true, smooth:Boolean = false):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Specifies a gradient to use for the stroke when drawing lines. </p>
		 * <p>The gradient line style is used for subsequent calls to Graphics methods such as the <code>lineTo()</code> methods or the <code>drawCircle()</code> method. The line style remains in effect until you call the <code>lineStyle()</code> or <code>lineBitmapStyle()</code> methods, or the <code>lineGradientStyle()</code> method again with different parameters. </p>
		 * <p>You can call the <code>lineGradientStyle()</code> method in the middle of drawing a path to specify different styles for different line segments within a path. </p>
		 * <p>Call the <code>lineStyle()</code> method before you call the <code>lineGradientStyle()</code> method to enable a stroke, or else the value of the line style is <code>undefined</code>.</p>
		 * <p>Calls to the <code>clear()</code> method set the line style back to <code>undefined</code>. </p>
		 * 
		 * @param type  — A value from the GradientType class that specifies which gradient type to use, either GradientType.LINEAR or GradientType.RADIAL. 
		 * @param colors  — An array of RGB hex color values to be used in the gradient (for example, red is 0xFF0000, blue is 0x0000FF, and so on). 
		 * @param alphas  — An array of alpha values for the corresponding colors in the colors array; valid values are 0 to 1. If the value is less than 0, the default is 0. If the value is greater than 1, the default is 1. 
		 * @param ratios  — An array of color distribution ratios; valid values are from 0 to 255. This value defines the percentage of the width where the color is sampled at 100%. The value 0 represents the left position in the gradient box, and 255 represents the right position in the gradient box. This value represents positions in the gradient box, not the coordinate space of the final gradient, which can be wider or thinner than the gradient box. Specify a value for each value in the <code>colors</code> parameter. <p>For example, for a linear gradient that includes two colors, blue and green, the following figure illustrates the placement of the colors in the gradient based on different values in the <code>ratios</code> array:</p> <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th><code>ratios</code></th>
		 *    <th>Gradient</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>[0, 127]</code></td>
		 *    <td><img src="../../images/gradient-ratios-1.jpg" alt="linear gradient blue to green with ratios 0 and 127"></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>[0, 255]</code></td>
		 *    <td><img src="../../images/gradient-ratios-2.jpg" alt="linear gradient blue to green with ratios 0 and 255"></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>[127, 255]</code></td>
		 *    <td><img src="../../images/gradient-ratios-3.jpg" alt="linear gradient blue to green with ratios 127 and 255"></td>
		 *   </tr>
		 *  </tbody>
		 * </table> <p>The values in the array must increase, sequentially; for example, <code>[0, 63, 127, 190, 255]</code>. </p> 
		 * @param matrix  — A transformation matrix as defined by the flash.geom.Matrix class. The flash.geom.Matrix class includes a <code>createGradientBox()</code> method, which lets you conveniently set up the matrix for use with the <code>lineGradientStyle()</code> method. 
		 * @param spreadMethod  — A value from the SpreadMethod class that specifies which spread method to use: <p> </p><table class="+ topic/table adobe-d/adobetable ">
		 *  <tbody>
		 *   <tr>
		 *    <td align="center"><img src="../../images/beginGradientFill_spread_pad.jpg" alt="linear gradient with SpreadMethod.PAD"></td>
		 *    <td align="center"><img src="../../images/beginGradientFill_spread_reflect.jpg" alt="linear gradient with SpreadMethod.REFLECT"></td>
		 *    <td align="center"><img src="../../images/beginGradientFill_spread_repeat.jpg" alt="linear gradient with SpreadMethod.REPEAT"></td>
		 *   </tr>
		 *   <tr>
		 *    <td align="center"><code>SpreadMethod.PAD</code></td>
		 *    <td align="center"><code>SpreadMethod.REFLECT</code></td>
		 *    <td align="center"><code>SpreadMethod.REPEAT</code></td>
		 *   </tr>
		 *  </tbody>
		 * </table> <p></p> 
		 * @param interpolationMethod  — A value from the InterpolationMethod class that specifies which value to use. For example, consider a simple linear gradient between two colors (with the <code>spreadMethod</code> parameter set to <code>SpreadMethod.REFLECT</code>). The different interpolation methods affect the appearance as follows: <p> </p><table class="+ topic/table adobe-d/adobetable ">
		 *  <tbody>
		 *   <tr>
		 *    <td align="center"><img src="../../images/beginGradientFill_interp_linearrgb.jpg" alt="linear gradient with InterpolationMethod.LINEAR_RGB"></td>
		 *    <td align="center"><img src="../../images/beginGradientFill_interp_rgb.jpg" alt="linear gradient with InterpolationMethod.RGB"></td>
		 *   </tr>
		 *   <tr>
		 *    <td align="center"><code>InterpolationMethod.LINEAR_RGB</code></td>
		 *    <td align="center"><code>InterpolationMethod.RGB</code></td>
		 *   </tr>
		 *  </tbody>
		 * </table> <p></p> 
		 * @param focalPointRatio  — A number that controls the location of the focal point of the gradient. The value 0 means the focal point is in the center. The value 1 means the focal point is at one border of the gradient circle. The value -1 means that the focal point is at the other border of the gradient circle. Values less than -1 or greater than 1 are rounded to -1 or 1. The following image shows a gradient with a <code>focalPointRatio</code> of -0.75: <p><img src="../../images/radial_sketch.jpg" alt="radial gradient with focalPointRatio set to 0.75"> </p> 
		 */
		public function lineGradientStyle(type:String, colors:Array, alphas:Array, ratios:Array, matrix:Matrix = null, spreadMethod:String = "pad", interpolationMethod:String = "rgb", focalPointRatio:Number = 0):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Specifies a shader to use for the line stroke when drawing lines. </p>
		 * <p>The shader line style is used for subsequent calls to Graphics methods such as the <code>lineTo()</code> method or the <code>drawCircle()</code> method. The line style remains in effect until you call the <code>lineStyle()</code> or <code>lineGradientStyle()</code> methods, or the <code>lineBitmapStyle()</code> method again with different parameters. </p>
		 * <p>You can call the <code>lineShaderStyle()</code> method in the middle of drawing a path to specify different styles for different line segments within a path. </p>
		 * <p>Call the <code>lineStyle()</code> method before you call the <code>lineShaderStyle()</code> method to enable a stroke, or else the value of the line style is <code>undefined</code>.</p>
		 * <p>Calls to the <code>clear()</code> method set the line style back to <code>undefined</code>. </p>
		 * 
		 * @param shader  — The shader to use for the line stroke. 
		 * @param matrix  — An optional transformation matrix as defined by the flash.geom.Matrix class. The matrix can be used to scale or otherwise manipulate the bitmap before applying it to the line style. 
		 */
		public function lineShaderStyle(shader:Shader, matrix:Matrix = null):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Specifies a line style used for subsequent calls to Graphics methods such as the <code>lineTo()</code> method or the <code>drawCircle()</code> method. The line style remains in effect until you call the <code>lineGradientStyle()</code> method, the <code>lineBitmapStyle()</code> method, or the <code>lineStyle()</code> method with different parameters. </p>
		 * <p>You can call the <code>lineStyle()</code> method in the middle of drawing a path to specify different styles for different line segments within the path.</p>
		 * <p><b>Note: </b>Calls to the <code>clear()</code> method set the line style back to <code>undefined</code>.</p>
		 * <p><b>Note: </b>Flash Lite 4 supports only the first three parameters (<code>thickness</code>, <code>color</code>, and <code>alpha</code>).</p>
		 * 
		 * @param thickness  — An integer that indicates the thickness of the line in points; valid values are 0-255. If a number is not specified, or if the parameter is undefined, a line is not drawn. If a value of less than 0 is passed, the default is 0. The value 0 indicates hairline thickness; the maximum thickness is 255. If a value greater than 255 is passed, the default is 255. 
		 * @param color  — A hexadecimal color value of the line; for example, red is 0xFF0000, blue is 0x0000FF, and so on. If a value is not indicated, the default is 0x000000 (black). Optional. 
		 * @param alpha  — A number that indicates the alpha value of the color of the line; valid values are 0 to 1. If a value is not indicated, the default is 1 (solid). If the value is less than 0, the default is 0. If the value is greater than 1, the default is 1. 
		 * @param pixelHinting  — (Not supported in Flash Lite 4) A Boolean value that specifies whether to hint strokes to full pixels. This affects both the position of anchors of a curve and the line stroke size itself. With <code>pixelHinting</code> set to <code>true</code>, line widths are adjusted to full pixel widths. With <code>pixelHinting</code> set to <code>false</code>, disjoints can appear for curves and straight lines. For example, the following illustrations show how Flash Player or Adobe AIR renders two rounded rectangles that are identical, except that the <code>pixelHinting</code> parameter used in the <code>lineStyle()</code> method is set differently (the images are scaled by 200%, to emphasize the difference): <p><img src="../../images/lineStyle_pixelHinting.jpg" alt="pixelHinting false and pixelHinting true"></p> <p>If a value is not supplied, the line does not use pixel hinting.</p> 
		 * @param scaleMode  — (Not supported in Flash Lite 4) A value from the LineScaleMode class that specifies which scale mode to use: <ul>
		 *  <li> <code>LineScaleMode.NORMAL</code>—Always scale the line thickness when the object is scaled (the default). </li>
		 *  <li> <code>LineScaleMode.NONE</code>—Never scale the line thickness. </li>
		 *  <li> <code>LineScaleMode.VERTICAL</code>—Do not scale the line thickness if the object is scaled vertically <i>only</i>. For example, consider the following circles, drawn with a one-pixel line, and each with the <code>scaleMode</code> parameter set to <code>LineScaleMode.VERTICAL</code>. The circle on the left is scaled vertically only, and the circle on the right is scaled both vertically and horizontally: <p><img src="../../images/LineScaleMode_VERTICAL.jpg" alt="A circle scaled vertically, and a circle scaled both vertically and horizontally."></p> </li>
		 *  <li> <code>LineScaleMode.HORIZONTAL</code>—Do not scale the line thickness if the object is scaled horizontally <i>only</i>. For example, consider the following circles, drawn with a one-pixel line, and each with the <code>scaleMode</code> parameter set to <code>LineScaleMode.HORIZONTAL</code>. The circle on the left is scaled horizontally only, and the circle on the right is scaled both vertically and horizontally: <p><img src="../../images/LineScaleMode_HORIZONTAL.jpg" alt="A circle scaled horizontally, and a circle scaled both vertically and horizontally."></p> </li>
		 * </ul> 
		 * @param caps  — (Not supported in Flash Lite 4) A value from the CapsStyle class that specifies the type of caps at the end of lines. Valid values are: <code>CapsStyle.NONE</code>, <code>CapsStyle.ROUND</code>, and <code>CapsStyle.SQUARE</code>. If a value is not indicated, Flash uses round caps. <p>For example, the following illustrations show the different <code>capsStyle</code> settings. For each setting, the illustration shows a blue line with a thickness of 30 (for which the <code>capsStyle</code> applies), and a superimposed black line with a thickness of 1 (for which no <code>capsStyle</code> applies): </p> <p><img src="../../images/linecap.jpg" alt="NONE, ROUND, and SQUARE"></p> 
		 * @param joints  — (Not supported in Flash Lite 4) A value from the JointStyle class that specifies the type of joint appearance used at angles. Valid values are: <code>JointStyle.BEVEL</code>, <code>JointStyle.MITER</code>, and <code>JointStyle.ROUND</code>. If a value is not indicated, Flash uses round joints. <p>For example, the following illustrations show the different <code>joints</code> settings. For each setting, the illustration shows an angled blue line with a thickness of 30 (for which the <code>jointStyle</code> applies), and a superimposed angled black line with a thickness of 1 (for which no <code>jointStyle</code> applies): </p> <p><img src="../../images/linejoin.jpg" alt="MITER, ROUND, and BEVEL"></p> <p><b>Note:</b> For <code>joints</code> set to <code>JointStyle.MITER</code>, you can use the <code>miterLimit</code> parameter to limit the length of the miter.</p> 
		 * @param miterLimit  — (Not supported in Flash Lite 4) A number that indicates the limit at which a miter is cut off. Valid values range from 1 to 255 (and values outside that range are rounded to 1 or 255). This value is only used if the <code>jointStyle</code> is set to <code>"miter"</code>. The <code>miterLimit</code> value represents the length that a miter can extend beyond the point at which the lines meet to form a joint. The value expresses a factor of the line <code>thickness</code>. For example, with a <code>miterLimit</code> factor of 2.5 and a <code>thickness</code> of 10 pixels, the miter is cut off at 25 pixels. <p>For example, consider the following angled lines, each drawn with a <code>thickness</code> of 20, but with <code>miterLimit</code> set to 1, 2, and 4. Superimposed are black reference lines showing the meeting points of the joints:</p> <p><img src="../../images/miterLimit.jpg" alt="lines with miterLimit set to 1, 2, and 4"></p> <p>Notice that a given <code>miterLimit</code> value has a specific maximum angle for which the miter is cut off. The following table lists some examples:</p> <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th><code>miterLimit</code> value:</th>
		 *    <th>Angles smaller than this are cut off:</th>
		 *   </tr>
		 *   <tr>
		 *    <td>1.414</td>
		 *    <td>90 degrees</td>
		 *   </tr>
		 *   <tr>
		 *    <td>2</td>
		 *    <td>60 degrees</td>
		 *   </tr>
		 *   <tr>
		 *    <td>4</td>
		 *    <td>30 degrees</td>
		 *   </tr>
		 *   <tr>
		 *    <td>8</td>
		 *    <td>15 degrees</td>
		 *   </tr>
		 *  </tbody>
		 * </table> 
		 */
		public function lineStyle(thickness:Number = NaN, color:uint = 0, alpha:Number = 1.0, pixelHinting:Boolean = false, scaleMode:String = "normal", caps:String = null, joints:String = null, miterLimit:Number = 3):void {
			closePath();
			graphicsData.push(new GraphicsStroke(thickness, pixelHinting, scaleMode, caps, joints, miterLimit, new GraphicsSolidFill(color, alpha)));
		}

		/**
		 * <p> Draws a line using the current line style from the current drawing position to (<code>x</code>, <code>y</code>); the current drawing position is then set to (<code>x</code>, <code>y</code>). If the display object in which you are drawing contains content that was created with the Flash drawing tools, calls to the <code>lineTo()</code> method are drawn underneath the content. If you call <code>lineTo()</code> before any calls to the <code>moveTo()</code> method, the default position for the current drawing is (<i>0, 0</i>). If any of the parameters are missing, this method fails and the current drawing position is not changed. </p>
		 * 
		 * @param x  — A number that indicates the horizontal position relative to the registration point of the parent display object (in pixels). 
		 * @param y  — A number that indicates the vertical position relative to the registration point of the parent display object (in pixels). 
		 */
		public function lineTo(x:Number, y:Number):void {
			initPath();
			currentPath.lineTo(x, y);
		}

		/**
		 * <p> Moves the current drawing position to (<code>x</code>, <code>y</code>). If any of the parameters are missing, this method fails and the current drawing position is not changed. </p>
		 * 
		 * @param x  — A number that indicates the horizontal position relative to the registration point of the parent display object (in pixels). 
		 * @param y  — A number that indicates the vertical position relative to the registration point of the parent display object (in pixels). 
		 */
		public function moveTo(x:Number, y:Number):void {
			initPath();
			currentPath.moveTo(x, y);
		}

		/**
		 * <p> Queries a Sprite or Shape object (and optionally, its children) for its vector graphics content. The result is a Vector of IGraphicsData objects. Transformations are applied to the display object before the query, so the returned paths are all in the same coordinate space. Coordinates in the result data set are relative to the stage, not the display object being sampled. </p>
		 * <p>The result includes the following types of objects, with the specified limitations:</p>
		 * <ul>
		 *  <li>GraphicsSolidFill</li>
		 *  <li>GraphicsGradientFill 
		 *   <ul>
		 *    <li>All properties of the gradient fill are returned by <code>readGraphicsData()</code>.</li>
		 *    <li>The matrix returned is close to, but not exactly the same as, the input matrix.</li>
		 *   </ul> </li>
		 *  <li>GraphicsEndFill</li>
		 *  <li>GraphicsBitmapFill 
		 *   <ul>
		 *    <li>The matrix returned is close to, but not exactly the same as, the input matrix.</li>
		 *    <li><code>repeat</code> is always true.</li>
		 *    <li><code>smooth</code> is always false.</li>
		 *   </ul> </li>
		 *  <li>GraphicsStroke 
		 *   <ul>
		 *    <li><code>thickness</code> is supported.</li>
		 *    <li><code>fill</code> supports GraphicsSolidFill, GraphicsGradientFill, and GraphicsBitmapFill as described previously</li>
		 *    <li>All other properties have default values.</li>
		 *   </ul> </li>
		 *  <li>GraphicsPath 
		 *   <ul>
		 *    <li>The only supported commands are MOVE_TO, CURVE_TO, and LINE_TO.</li>
		 *   </ul> </li>
		 * </ul>
		 * <p>The following visual elements and transformations can't be represented and are not included in the result:</p>
		 * <ul>
		 *  <li>Masks</li>
		 *  <li>Text, with one exception: Static text that is defined with anti-alias type "anti-alias for animation" is rendered as vector shapes so it is included in the result.</li>
		 *  <li>Shader fills</li>
		 *  <li>Blend modes</li>
		 *  <li>9-slice scaling</li>
		 *  <li>Triangles (created with the <code>drawTriangles()</code> method)</li>
		 *  <li>Opaque background</li>
		 *  <li><code>scrollrect</code> settings</li>
		 *  <li>2.5D transformations</li>
		 *  <li>Non-visible objects (objects whose <code>visible</code> property is <code>false</code>)</li>
		 * </ul>
		 * 
		 * @param recurse  — whether the runtime should also query display object children of the current display object. A recursive query can take more time and memory to execute. The results are returned in a single flattened result set, not separated by display object. 
		 * @return  — A Vector of IGraphicsData objects representing the vector graphics content of the related display object 
		 */
		public function readGraphicsData(recurse:Boolean = true):Vector.<IGraphicsData> {
			return graphicsData;
		}
		
		private function initPath():void {
			if (currentPath == null) {
				currentPath = new GraphicsPath();
				graphicsData.push(currentPath);
			}
		}
		
		private function closePath():void {
			if (currentPath != null) {
				var el:Element = document.createElementNS(DisplayObject.SVG_NAMESPACE, "path");
				if (currentFill != null) {
					currentFill.draw(el);
					el.setAttribute("fill-rule", "evenodd");
				}
				if (currentStroke != null) {
					
				}
				currentPath.draw(el);
				if (_svgElement == null) {
					_svgElement = document.createElementNS(DisplayObject.SVG_NAMESPACE, "g") as SVGGElement;
					_svgParent.insertBefore(_svgElement, _svgParent.firstChild);
				}
				_svgElement.appendChild(el);
				currentPath = null;
			}
		}
	}
}
