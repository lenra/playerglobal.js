package flash.display {
	import flash.accessibility.AccessibilityProperties;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.events.KeyboardEvent;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.geom.Transform;
	import flash.geom.Vector3D;
	import flash.system.Worker;

	[Event(name="added", type="flash.events.Event")]
	[Event(name="addedToStage", type="flash.events.Event")]
	[Event(name="enterFrame", type="flash.events.Event")]
	[Event(name="exitFrame", type="flash.events.Event")]
	[Event(name="frameConstructed", type="flash.events.Event")]
	[Event(name="removed", type="flash.events.Event")]
	[Event(name="removedFromStage", type="flash.events.Event")]
	[Event(name="render", type="flash.events.Event")]
	/**
	 *  The DisplayObject class is the base class for all objects that can be placed on the display list. The display list manages all objects displayed in the Flash runtimes. Use the DisplayObjectContainer class to arrange the display objects in the display list. DisplayObjectContainer objects can have child display objects, while other display objects, such as Shape and TextField objects, are "leaf" nodes that have only parents and siblings, no children. <p>The DisplayObject class supports basic functionality like the <i>x</i> and <i>y</i> position of an object, as well as more advanced properties of the object such as its transformation matrix. </p> <p>DisplayObject is an abstract base class; therefore, you cannot call DisplayObject directly. Invoking <code>new DisplayObject()</code> throws an <code>ArgumentError</code> exception. </p> <p>All display objects inherit from the DisplayObject class.</p> <p>The DisplayObject class itself does not include any APIs for rendering content onscreen. For that reason, if you want create a custom subclass of the DisplayObject class, you will want to extend one of its subclasses that do have APIs for rendering content onscreen, such as the Shape, Sprite, Bitmap, SimpleButton, TextField, or MovieClip class.</p> <p>The DisplayObject class contains several broadcast events. Normally, the target of any particular event is a specific DisplayObject instance. For example, the target of an <code>added</code> event is the specific DisplayObject instance that was added to the display list. Having a single target restricts the placement of event listeners to that target and in some cases the target's ancestors on the display list. With broadcast events, however, the target is not a specific DisplayObject instance, but rather all DisplayObject instances, including those that are not on the display list. This means that you can add a listener to any DisplayObject instance to listen for broadcast events. In addition to the broadcast events listed in the DisplayObject class's Events table, the DisplayObject class also inherits two broadcast events from the EventDispatcher class: <code>activate</code> and <code>deactivate</code>.</p> <p>Some properties previously used in the ActionScript 1.0 and 2.0 MovieClip, TextField, and Button classes (such as <code>_alpha</code>, <code>_height</code>, <code>_name</code>, <code>_width</code>, <code>_x</code>, <code>_y</code>, and others) have equivalents in the ActionScript 3.0 DisplayObject class that are renamed so that they no longer begin with the underscore (_) character.</p> <p>For more information, see the "Display Programming" chapter of the <i>ActionScript 3.0 Developer's Guide</i>.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7dff.html" target="_blank">Adding display objects to the display list</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e26.html" target="_blank">Traversing the display list </a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e17.html" target="_blank">Panning and scrolling display objects</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e35.html" target="_blank">Caching display objects</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e15.html" target="_blank">Setting an opaque background color</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7df1.html" target="_blank">Applying blending modes</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e16.html" target="_blank">Adjusting DisplayObject colors</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7f37.html" target="_blank">Display object example: SpriteArranger</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7dcb.html" target="_blank">Geometry example: Applying a matrix transformation to a display object</a>
	 * </div><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e58.html" target="_blank">Display programming</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e3e.html" target="_blank">Basics of display programming</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e3c.html" target="_blank">Core display classes</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e3d.html" target="_blank">Working with display objects</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7dfb.html" target="_blank">Handling events for display objects</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7dfa.html" target="_blank">Choosing a DisplayObject subclass</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e0c.html" target="_blank">Manipulating display objects</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e0a.html" target="_blank">Animating objects</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e13.html" target="_blank">Loading display content dynamically</a>
	 *  <br>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="DisplayObjectContainer.html" target="">flash.display.DisplayObjectContainer</a>
	 * </div><br><hr>
	 */
	public class DisplayObject extends EventDispatcher implements IBitmapDrawable {
		public static const SVG_NAMESPACE:String = "http://www.w3.org/2000/svg";
		private static const SVG_RENDERER:SVGElement = (Worker.isSupported && Worker.current.isPrimordial) ? 
															document.createElementNS(DisplayObject.SVG_NAMESPACE, "svg") as SVGElement : null;
		private static const frameListeners:Object = {};
		private static var _loaderInfo:LoaderInfo;
		
		{
			if (SVG_RENDERER) {
				SVG_RENDERER.style.position = "absolute";
				SVG_RENDERER.style.top = "0";
				SVG_RENDERER.style.left = "0";
			}
		}
		
		private var _accessibilityProperties:AccessibilityProperties;
		private var _alpha:Number = 1;
		private var _blendMode:String;
		private var _blendShader:Shader;
		private var _cacheAsBitmap:Boolean;
		private var _cacheAsBitmapMatrix:Matrix;
		private var _filters:Array;
		private var _height:Number;
		private var _mask:DisplayObject;
		private var _metaData:Object;
		private var _name:String;
		private var _opaqueBackground:Object;
		private var _rotation:Number = 0;
		private var _rotationX:Number;
		private var _rotationY:Number;
		private var _rotationZ:Number;
		private var _scale9Grid:Rectangle;
		private var _scaleX:Number = 1;
		private var _scaleY:Number = 1;
		private var _scaleZ:Number;
		private var _scrollRect:Rectangle;
		private var _transform:Transform;
		private var _visible:Boolean;
		private var _width:Number;
		private var _x:Number = 0;
		private var _y:Number = 0;
		private var _z:Number = 0;

		private var _mouseX:Number;
		private var _mouseY:Number;
		private var _parent:DisplayObjectContainer;
		private var _root:DisplayObject;
		
		private var _svgElement:*;
		
		public function DisplayObject() {
			
		}
		
		/**
		 * <p> The current accessibility options for this display object. If you modify the <code>accessibilityProperties</code> property or any of the fields within <code>accessibilityProperties</code>, you must call the <code>Accessibility.updateProperties()</code> method to make your changes take effect. </p>
		 * <p><b>Note</b>: For an object created in the Flash authoring environment, the value of <code>accessibilityProperties</code> is prepopulated with any information you entered in the Accessibility panel for that object.</p>
		 * 
		 * @return 
		 */
		public function get accessibilityProperties():AccessibilityProperties {
			return _accessibilityProperties;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set accessibilityProperties(value:AccessibilityProperties):void {
			_accessibilityProperties = value;
		}

		/**
		 * <p> Indicates the alpha transparency value of the object specified. Valid values are 0 (fully transparent) to 1 (fully opaque). The default value is 1. Display objects with <code>alpha</code> set to 0 <i>are</i> active, even though they are invisible. </p>
		 * 
		 * @return 
		 */
		public function get alpha():Number {
			return _alpha;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set alpha(value:Number):void {
			value = Math.max(0, Math.min(1, value));
			_alpha = value;
			svgElement.setAttribute("opacity", _alpha);
		}

		/**
		 * <p> A value from the BlendMode class that specifies which blend mode to use. A bitmap can be drawn internally in two ways. If you have a blend mode enabled or an external clipping mask, the bitmap is drawn by adding a bitmap-filled square shape to the vector render. If you attempt to set this property to an invalid value, Flash runtimes set the value to <code>BlendMode.NORMAL</code>. </p>
		 * <p>The <code>blendMode</code> property affects each pixel of the display object. Each pixel is composed of three constituent colors (red, green, and blue), and each constituent color has a value between 0x00 and 0xFF. Flash Player or Adobe AIR compares each constituent color of one pixel in the movie clip with the corresponding color of the pixel in the background. For example, if <code>blendMode</code> is set to <code>BlendMode.LIGHTEN</code>, Flash Player or Adobe AIR compares the red value of the display object with the red value of the background, and uses the lighter of the two as the value for the red component of the displayed color.</p>
		 * <p>The following table describes the <code>blendMode</code> settings. The BlendMode class defines string values you can use. The illustrations in the table show <code>blendMode</code> values applied to a circular display object (2) superimposed on another display object (1).</p>
		 * <p> <img src="../../images/blendMode-0a.jpg" alt="Square Number 1"> <img src="../../images/blendMode-0b.jpg" alt="Circle Number 2"> </p>
		 * <table class="innertable">
		 *  <tbody>
		 *   <tr>
		 *    <th>BlendMode Constant</th>
		 *    <th>Illustration</th>
		 *    <th>Description</th>
		 *   </tr>
		 *   <tr>
		 *    <td><code>BlendMode.NORMAL</code></td>
		 *    <td><img src="../../images/blendMode-1.jpg" alt="blend mode NORMAL"></td>
		 *    <td>The display object appears in front of the background. Pixel values of the display object override those of the background. Where the display object is transparent, the background is visible.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>BlendMode.LAYER</code></td>
		 *    <td><img src="../../images/blendMode-2.jpg" alt="blend mode LAYER"></td>
		 *    <td>Forces the creation of a transparency group for the display object. This means that the display object is pre-composed in a temporary buffer before it is processed further. This is done automatically if the display object is pre-cached using bitmap caching or if the display object is a display object container with at least one child object with a <code>blendMode</code> setting other than <code>BlendMode.NORMAL</code>. Not supported under GPU rendering. </td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>BlendMode.MULTIPLY</code></td>
		 *    <td><img src="../../images/blendMode-3.jpg" alt="blend mode MULTIPLY"></td>
		 *    <td>Multiplies the values of the display object constituent colors by the colors of the background color, and then normalizes by dividing by 0xFF, resulting in darker colors. This setting is commonly used for shadows and depth effects. <p>For example, if a constituent color (such as red) of one pixel in the display object and the corresponding color of the pixel in the background both have the value 0x88, the multiplied result is 0x4840. Dividing by 0xFF yields a value of 0x48 for that constituent color, which is a darker shade than the color of the display object or the color of the background.</p></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>BlendMode.SCREEN</code></td>
		 *    <td><img src="../../images/blendMode-4.jpg" alt="blend mode SCREEN"></td>
		 *    <td>Multiplies the complement (inverse) of the display object color by the complement of the background color, resulting in a bleaching effect. This setting is commonly used for highlights or to remove black areas of the display object.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>BlendMode.LIGHTEN</code></td>
		 *    <td><img src="../../images/blendMode-5.jpg" alt="blend mode LIGHTEN"></td>
		 *    <td>Selects the lighter of the constituent colors of the display object and the color of the background (the colors with the larger values). This setting is commonly used for superimposing type. <p>For example, if the display object has a pixel with an RGB value of 0xFFCC33, and the background pixel has an RGB value of 0xDDF800, the resulting RGB value for the displayed pixel is 0xFFF833 (because 0xFF &gt; 0xDD, 0xCC &lt; 0xF8, and 0x33 &gt; 0x00 = 33). Not supported under GPU rendering.</p></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>BlendMode.DARKEN</code></td>
		 *    <td><img src="../../images/blendMode-6.jpg" alt="blend mode DARKEN"></td>
		 *    <td>Selects the darker of the constituent colors of the display object and the colors of the background (the colors with the smaller values). This setting is commonly used for superimposing type. <p>For example, if the display object has a pixel with an RGB value of 0xFFCC33, and the background pixel has an RGB value of 0xDDF800, the resulting RGB value for the displayed pixel is 0xDDCC00 (because 0xFF &gt; 0xDD, 0xCC &lt; 0xF8, and 0x33 &gt; 0x00 = 33). Not supported under GPU rendering.</p></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>BlendMode.DIFFERENCE</code></td>
		 *    <td><img src="../../images/blendMode-7.jpg" alt="blend mode DIFFERENCE"></td>
		 *    <td>Compares the constituent colors of the display object with the colors of its background, and subtracts the darker of the values of the two constituent colors from the lighter value. This setting is commonly used for more vibrant colors. <p>For example, if the display object has a pixel with an RGB value of 0xFFCC33, and the background pixel has an RGB value of 0xDDF800, the resulting RGB value for the displayed pixel is 0x222C33 (because 0xFF - 0xDD = 0x22, 0xF8 - 0xCC = 0x2C, and 0x33 - 0x00 = 0x33).</p></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>BlendMode.ADD</code></td>
		 *    <td><img src="../../images/blendMode-8.jpg" alt="blend mode ADD"></td>
		 *    <td>Adds the values of the constituent colors of the display object to the colors of its background, applying a ceiling of 0xFF. This setting is commonly used for animating a lightening dissolve between two objects. <p>For example, if the display object has a pixel with an RGB value of 0xAAA633, and the background pixel has an RGB value of 0xDD2200, the resulting RGB value for the displayed pixel is 0xFFC833 (because 0xAA + 0xDD &gt; 0xFF, 0xA6 + 0x22 = 0xC8, and 0x33 + 0x00 = 0x33).</p></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>BlendMode.SUBTRACT</code></td>
		 *    <td><img src="../../images/blendMode-9.jpg" alt="blend mode SUBTRACT"></td>
		 *    <td>Subtracts the values of the constituent colors in the display object from the values of the background color, applying a floor of 0. This setting is commonly used for animating a darkening dissolve between two objects. <p>For example, if the display object has a pixel with an RGB value of 0xAA2233, and the background pixel has an RGB value of 0xDDA600, the resulting RGB value for the displayed pixel is 0x338400 (because 0xDD - 0xAA = 0x33, 0xA6 - 0x22 = 0x84, and 0x00 - 0x33 &lt; 0x00).</p></td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>BlendMode.INVERT</code></td>
		 *    <td><img src="../../images/blendMode-10.jpg" alt="blend mode INVERT"></td>
		 *    <td>Inverts the background.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>BlendMode.ALPHA</code></td>
		 *    <td><img src="../../images/blendMode-11.jpg" alt="blend mode ALPHA"></td>
		 *    <td>Applies the alpha value of each pixel of the display object to the background. This requires the <code>blendMode</code> setting of the parent display object to be set to <code>BlendMode.LAYER</code>. For example, in the illustration, the parent display object, which is a white background, has <code>blendMode = BlendMode.LAYER</code>. Not supported under GPU rendering.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>BlendMode.ERASE</code></td>
		 *    <td><img src="../../images/blendMode-12.jpg" alt="blend mode ERASE"></td>
		 *    <td>Erases the background based on the alpha value of the display object. This requires the <code>blendMode</code> of the parent display object to be set to <code>BlendMode.LAYER</code>. For example, in the illustration, the parent display object, which is a white background, has <code>blendMode = BlendMode.LAYER</code>. Not supported under GPU rendering.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>BlendMode.OVERLAY</code></td>
		 *    <td><img src="../../images/blendMode-13.jpg" alt="blend mode OVERLAY"></td>
		 *    <td>Adjusts the color of each pixel based on the darkness of the background. If the background is lighter than 50% gray, the display object and background colors are screened, which results in a lighter color. If the background is darker than 50% gray, the colors are multiplied, which results in a darker color. This setting is commonly used for shading effects. Not supported under GPU rendering.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>BlendMode.HARDLIGHT</code></td>
		 *    <td><img src="../../images/blendMode-14.jpg" alt="blend mode HARDLIGHT"></td>
		 *    <td>Adjusts the color of each pixel based on the darkness of the display object. If the display object is lighter than 50% gray, the display object and background colors are screened, which results in a lighter color. If the display object is darker than 50% gray, the colors are multiplied, which results in a darker color. This setting is commonly used for shading effects. Not supported under GPU rendering.</td>
		 *   </tr>
		 *   <tr>
		 *    <td><code>BlendMode.SHADER</code></td>
		 *    <td align="center">N/A</td>
		 *    <td>Adjusts the color using a custom shader routine. The shader that is used is specified as the Shader instance assigned to the <code>blendShader</code> property. Setting the <code>blendShader</code> property of a display object to a Shader instance automatically sets the display object's <code>blendMode</code> property to <code>BlendMode.SHADER</code>. If the <code>blendMode</code> property is set to <code>BlendMode.SHADER</code> without first setting the <code>blendShader</code> property, the <code>blendMode</code> property is set to <code>BlendMode.NORMAL</code>. Not supported under GPU rendering.</td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * 
		 * @return 
		 */
		public function get blendMode():String {
			return _blendMode;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set blendMode(value:String):void {
			_blendMode = value;
		}

		/**
		 * <p> Sets a shader that is used for blending the foreground and background. When the <code>blendMode</code> property is set to <code>BlendMode.SHADER</code>, the specified Shader is used to create the blend mode output for the display object. </p>
		 * <p>Setting the <code>blendShader</code> property of a display object to a Shader instance automatically sets the display object's <code>blendMode</code> property to <code>BlendMode.SHADER</code>. If the <code>blendShader</code> property is set (which sets the <code>blendMode</code> property to <code>BlendMode.SHADER</code>), then the value of the <code>blendMode</code> property is changed, the blend mode can be reset to use the blend shader simply by setting the <code>blendMode</code> property to <code>BlendMode.SHADER</code>. The <code>blendShader</code> property does not need to be set again except to change the shader that's used for the blend mode.</p>
		 * <p>The Shader assigned to the <code>blendShader</code> property must specify at least two <code>image4</code> inputs. The inputs <b>do not</b> need to be specified in code using the associated ShaderInput objects' <code>input</code> properties. The background display object is automatically used as the first input (the input with <code>index</code> 0). The foreground display object is used as the second input (the input with <code>index</code> 1). A shader used as a blend shader can specify more than two inputs. In that case, any additional input must be specified by setting its ShaderInput instance's <code>input</code> property.</p>
		 * <p>When you assign a Shader instance to this property the shader is copied internally. The blend operation uses that internal copy, not a reference to the original shader. Any changes made to the shader, such as changing a parameter value, input, or bytecode, are not applied to the copied shader that's used for the blend mode.</p>
		 * 
		 * @return 
		 */
		public function get blendShader():Shader {
			return _blendShader;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set blendShader(value:Shader):void {
			_blendShader = value;
		}

		/**
		 * <p> If set to <code>true</code>, Flash runtimes cache an internal bitmap representation of the display object. This caching can increase performance for display objects that contain complex vector content. </p>
		 * <p>All vector data for a display object that has a cached bitmap is drawn to the bitmap instead of the main display. If <code>cacheAsBitmapMatrix</code> is null or unsupported, the bitmap is then copied to the main display as unstretched, unrotated pixels snapped to the nearest pixel boundaries. Pixels are mapped 1 to 1 with the parent object. If the bounds of the bitmap change, the bitmap is recreated instead of being stretched.</p>
		 * <p>If <code>cacheAsBitmapMatrix</code> is non-null and supported, the object is drawn to the off-screen bitmap using that matrix and the stretched and/or rotated results of that rendering are used to draw the object to the main display.</p>
		 * <p>No internal bitmap is created unless the <code>cacheAsBitmap</code> property is set to <code>true</code>.</p>
		 * <p>After you set the <code>cacheAsBitmap</code> property to <code>true</code>, the rendering does not change, however the display object performs pixel snapping automatically. The animation speed can be significantly faster depending on the complexity of the vector content. </p>
		 * <p>The <code>cacheAsBitmap</code> property is automatically set to <code>true</code> whenever you apply a filter to a display object (when its <code>filter</code> array is not empty), and if a display object has a filter applied to it, <code>cacheAsBitmap</code> is reported as <code>true</code> for that display object, even if you set the property to <code>false</code>. If you clear all filters for a display object, the <code>cacheAsBitmap</code> setting changes to what it was last set to.</p>
		 * <p>A display object does not use a bitmap even if the <code>cacheAsBitmap</code> property is set to <code>true</code> and instead renders from vector data in the following cases:</p>
		 * <ul>
		 *  <li>The bitmap is too large. In AIR 1.5 and Flash Player 10, the maximum size for a bitmap image is 8,191 pixels in width or height, and the total number of pixels cannot exceed 16,777,215 pixels. (So, if a bitmap image is 8,191 pixels wide, it can only be 2,048 pixels high.) In Flash Player 9 and earlier, the limitation is is 2880 pixels in height and 2,880 pixels in width.</li>
		 *  <li>The bitmap fails to allocate (out of memory error). </li>
		 * </ul>
		 * <p>The <code>cacheAsBitmap</code> property is best used with movie clips that have mostly static content and that do not scale and rotate frequently. With such movie clips, <code>cacheAsBitmap</code> can lead to performance increases when the movie clip is translated (when its <i>x</i> and <i>y</i> position is changed).</p>
		 * 
		 * @return 
		 */
		public function get cacheAsBitmap():Boolean {
			return _cacheAsBitmap;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set cacheAsBitmap(value:Boolean):void {
			_cacheAsBitmap = value;
		}

		/**
		 * <p> If non-null, this Matrix object defines how a display object is rendered when <code>cacheAsBitmap</code> is set to <code>true</code>. The application uses this matrix as a transformation matrix that is applied when rendering the bitmap version of the display object. </p>
		 * <p><i>AIR profile support:</i> This feature is supported on mobile devices, but it is not supported on desktop operating systems. It also has limited support on AIR for TV devices. Specifically, on AIR for TV devices, supported transformations include scaling and translation, but not rotation and skewing. See <a href="http://help.adobe.com/en_US/air/build/WS144092a96ffef7cc16ddeea2126bb46b82f-8000.html"> AIR Profile Support</a> for more information regarding API support across multiple profiles.</p>
		 * <p>With <code>cacheAsBitmapMatrix</code> set, the application retains a cached bitmap image across various 2D transformations, including translation, rotation, and scaling. If the application uses hardware acceleration, the object will be stored in video memory as a texture. This allows the GPU to apply the supported transformations to the object. The GPU can perform these transformations faster than the CPU.</p>
		 * <p>To use the hardware acceleration, set Rendering to GPU in the General tab of the iPhone Settings dialog box in Flash Professional CS5. Or set the <code>renderMode</code> property to <code>gpu</code> in the application descriptor file. Note that AIR for TV devices automatically use hardware acceleration if it is available.</p>
		 * <p>For example, the following code sends an untransformed bitmap representation of the display object to the GPU:</p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>matrix:Matrix = new Matrix(); // creates an identity matrix 
		 *      mySprite.cacheAsBitmapMatrix = matrix; 
		 *      mySprite.cacheAsBitmap = true;</pre>
		 * </div>
		 * <p>Usually, the identity matrix (<code>new Matrix()</code>) suffices. However, you can use another matrix, such as a scaled-down matrix, to upload a different bitmap to the GPU. For example, the following example applies a <code>cacheAsBitmapMatrix</code> matrix that is scaled by 0.5 on the x and y axes. The bitmap object that the GPU uses is smaller, however the GPU adjusts its size to match the transform.matrix property of the display object:</p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>matrix:Matrix = new Matrix(); // creates an identity matrix 
		 *      matrix.scale(0.5, 0.5); // scales the matrix 
		 *      mySprite.cacheAsBitmapMatrix = matrix; 
		 *      mySprite.cacheAsBitmap = true;</pre>
		 * </div>
		 * <p>Generally, you should choose to use a matrix that transforms the display object to the size that it will appear in the application. For example, if your application displays the bitmap version of the sprite scaled down by a half, use a matrix that scales down by a half. If you application will display the sprite larger than its current dimensions, use a matrix that scales up by that factor.</p>
		 * <p><b>Note:</b> The <code>cacheAsBitmapMatrix</code> property is suitable for 2D transformations. If you need to apply transformations in 3D, you may do so by setting a 3D property of the object and manipulating its <code>transform.matrix3D</code> property. If the application is packaged using GPU mode, this allows the 3D transforms to be applied to the object by the GPU. The <code>cacheAsBitmapMatrix</code> is ignored for 3D objects.</p>
		 * 
		 * @return 
		 */
		public function get cacheAsBitmapMatrix():Matrix {
			return _cacheAsBitmapMatrix;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set cacheAsBitmapMatrix(value:Matrix):void {
			_cacheAsBitmapMatrix = value;
		}

		/**
		 * <p> An indexed array that contains each filter object currently associated with the display object. The flash.filters package contains several classes that define specific filters you can use. </p>
		 * <p>Filters can be applied in Flash Professional at design time, or at run time by using ActionScript code. To apply a filter by using ActionScript, you must make a temporary copy of the entire <code>filters</code> array, modify the temporary array, then assign the value of the temporary array back to the <code>filters</code> array. You cannot directly add a new filter object to the <code>filters</code> array.</p>
		 * <p>To add a filter by using ActionScript, perform the following steps (assume that the target display object is named <code>myDisplayObject</code>):</p>
		 * <ol>
		 *  <li>Create a new filter object by using the constructor method of your chosen filter class.</li>
		 *  <li>Assign the value of the <code>myDisplayObject.filters</code> array to a temporary array, such as one named <code>myFilters</code>.</li>
		 *  <li>Add the new filter object to the <code>myFilters</code> temporary array.</li>
		 *  <li>Assign the value of the temporary array to the <code>myDisplayObject.filters</code> array.</li>
		 * </ol>
		 * <p>If the <code>filters</code> array is undefined, you do not need to use a temporary array. Instead, you can directly assign an array literal that contains one or more filter objects that you create. The first example in the Examples section adds a drop shadow filter by using code that handles both defined and undefined <code>filters</code> arrays.</p>
		 * <p>To modify an existing filter object, you must use the technique of modifying a copy of the <code>filters</code> array:</p>
		 * <ol>
		 *  <li>Assign the value of the <code>filters</code> array to a temporary array, such as one named <code>myFilters</code>.</li>
		 *  <li>Modify the property by using the temporary array, <code>myFilters</code>. For example, to set the quality property of the first filter in the array, you could use the following code: <code>myFilters[0].quality = 1;</code></li>
		 *  <li>Assign the value of the temporary array to the <code>filters</code> array.</li>
		 * </ol>
		 * <p>At load time, if a display object has an associated filter, it is marked to cache itself as a transparent bitmap. From this point forward, as long as the display object has a valid filter list, the player caches the display object as a bitmap. This source bitmap is used as a source image for the filter effects. Each display object usually has two bitmaps: one with the original unfiltered source display object and another for the final image after filtering. The final image is used when rendering. As long as the display object does not change, the final image does not need updating.</p>
		 * <p>The flash.filters package includes classes for filters. For example, to create a DropShadow filter, you would write:</p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *      import flash.filters.DropShadowFilter
		 *      var myFilter:DropShadowFilter = new DropShadowFilter (distance, angle, color, alpha, blurX, blurY, quality, inner, knockout)
		 *      </pre>
		 * </div>
		 * <p>You can use the <code>is</code> operator to determine the type of filter assigned to each index position in the <code>filter</code> array. For example, the following code shows how to determine the position of the first filter in the <code>filters</code> array that is a DropShadowFilter: </p>
		 * <div class="listing">
		 *  <pre>
		 *      import flash.text.TextField;
		 *      import flash.filters.*;
		 *      var tf:TextField = new TextField();
		 *      var filter1:DropShadowFilter = new DropShadowFilter();
		 *      var filter2:GradientGlowFilter = new GradientGlowFilter();
		 *      tf.filters = [filter1, filter2];
		 *      
		 *      tf.text = "DropShadow index: " + filterPosition(tf, DropShadowFilter).toString(); // 0
		 *      addChild(tf)
		 *      
		 *      function filterPosition(displayObject:DisplayObject, filterClass:Class):int {
		 *          for (var i:uint = 0; i &lt; displayObject.filters.length; i++) {
		 *              if (displayObject.filters[i] is filterClass) {
		 *                  return i;
		 *              }
		 *          }
		 *          return -1;
		 *      }
		 *      </pre>
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 * </div>
		 * <p><b>Note:</b> Since you cannot directly add a new filter object to the <code>DisplayObject.filters</code> array, the following code has no effect on the target display object, named <code>myDisplayObject</code>:</p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *      myDisplayObject.filters.push(myDropShadow);
		 *      </pre>
		 * </div>
		 * 
		 * @return 
		 */
		public function get filters():Array {
			return _filters;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set filters(value:Array):void {
			_filters = value;
		}

		/**
		 * <p> Indicates the height of the display object, in pixels. The height is calculated based on the bounds of the content of the display object. When you set the <code>height</code> property, the <code>scaleY</code> property is adjusted accordingly, as shown in the following code: </p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *     var rect:Shape = new Shape();
		 *     rect.graphics.beginFill(0xFF0000);
		 *     rect.graphics.drawRect(0, 0, 100, 100);
		 *     trace(rect.scaleY) // 1;
		 *     rect.height = 200;
		 *     trace(rect.scaleY) // 2;</pre>
		 * </div>
		 * <p>Except for TextField and Video objects, a display object with no content (such as an empty sprite) has a height of 0, even if you try to set <code>height</code> to a different value.</p>
		 * 
		 * @return 
		 */
		public function get height():Number {
			return getBoundingRect().height;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set height(value:Number):void {
			_height = value;
			this.scaleY = this.scaleY * value / this.height;
		}

		/**
		 * <p> Returns a LoaderInfo object containing information about loading the file to which this display object belongs. The <code>loaderInfo</code> property is defined only for the root display object of a SWF file or for a loaded Bitmap (not for a Bitmap that is drawn with ActionScript). To find the <code>loaderInfo</code> object associated with the SWF file that contains a display object named <code>myDisplayObject</code>, use <code>myDisplayObject.root.loaderInfo</code>. </p>
		 * <p>A large SWF file can monitor its download by calling <code>this.root.loaderInfo.addEventListener(Event.COMPLETE, func)</code>.</p>
		 * 
		 * @return 
		 */
		public function get loaderInfo():LoaderInfo {
			if (!_loaderInfo) 
				_loaderInfo = new LoaderInfo();
			return _loaderInfo;
		}

		/**
		 * <p> The calling display object is masked by the specified <code>mask</code> object. To ensure that masking works when the Stage is scaled, the <code>mask</code> display object must be in an active part of the display list. The <code>mask</code> object itself is not drawn. Set <code>mask</code> to <code>null</code> to remove the mask. </p>
		 * <p>To be able to scale a mask object, it must be on the display list. To be able to drag a mask Sprite object (by calling its <code>startDrag()</code> method), it must be on the display list. To call the <code>startDrag()</code> method for a mask sprite based on a <code>mouseDown</code> event being dispatched by the sprite, set the sprite's <code>buttonMode</code> property to <code>true</code>.</p>
		 * <p>When display objects are cached by setting the <code>cacheAsBitmap</code> property to <code>true</code> an the <code>cacheAsBitmapMatrix</code> property to a Matrix object, both the mask and the display object being masked must be part of the same cached bitmap. Thus, if the display object is cached, then the mask must be a child of the display object. If an ancestor of the display object on the display list is cached, then the mask must be a child of that ancestor or one of its descendents. If more than one ancestor of the masked object is cached, then the mask must be a descendent of the cached container closest to the masked object in the display list.</p>
		 * <p><b>Note:</b> A single <code>mask</code> object cannot be used to mask more than one calling display object. When the <code>mask</code> is assigned to a second display object, it is removed as the mask of the first object, and that object's <code>mask</code> property becomes <code>null</code>.</p>
		 * 
		 * @return 
		 */
		public function get mask():DisplayObject {
			return _mask;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set mask(value:DisplayObject):void {
			_mask = value;
		}

		/**
		 * @return 
		 */
		public function get metaData():Object {
			return _metaData;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set metaData(value:Object):void {
			_metaData = value;
		}

		/**
		 * <p> Indicates the x coordinate of the mouse or user input device position, in pixels. </p>
		 * <p><b>Note</b>: For a DisplayObject that has been rotated, the returned x coordinate will reflect the non-rotated object.</p>
		 * 
		 * @return 
		 */
		public function get mouseX():Number {
			return _mouseX;
		}

		/**
		 * <p> Indicates the y coordinate of the mouse or user input device position, in pixels. </p>
		 * <p><b>Note</b>: For a DisplayObject that has been rotated, the returned y coordinate will reflect the non-rotated object.</p>
		 * 
		 * @return 
		 */
		public function get mouseY():Number {
			return _mouseY;
		}

		/**
		 * <p> Indicates the instance name of the DisplayObject. The object can be identified in the child list of its parent display object container by calling the <code>getChildByName()</code> method of the display object container. </p>
		 * 
		 * @return 
		 */
		public function get name():String {
			return _name;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set name(value:String):void {
			_name = value;
		}

		/**
		 * <p> Specifies whether the display object is opaque with a certain background color. A transparent bitmap contains alpha channel data and is drawn transparently. An opaque bitmap has no alpha channel (and renders faster than a transparent bitmap). If the bitmap is opaque, you specify its own background color to use. </p>
		 * <p>If set to a number value, the surface is opaque (not transparent) with the RGB background color that the number specifies. If set to <code>null</code> (the default value), the display object has a transparent background.</p>
		 * <p>The <code>opaqueBackground</code> property is intended mainly for use with the <code>cacheAsBitmap</code> property, for rendering optimization. For display objects in which the <code>cacheAsBitmap</code> property is set to true, setting <code>opaqueBackground</code> can improve rendering performance.</p>
		 * <p>The opaque background region is <i>not</i> matched when calling the <code>hitTestPoint()</code> method with the <code>shapeFlag</code> parameter set to <code>true</code>.</p>
		 * <p>The opaque background region does not respond to mouse events.</p>
		 * 
		 * @return 
		 */
		public function get opaqueBackground():Object {
			return _opaqueBackground;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set opaqueBackground(value:Object):void {
			_opaqueBackground = value;
		}

		/**
		 * <p> Indicates the DisplayObjectContainer object that contains this display object. Use the <code>parent</code> property to specify a relative path to display objects that are above the current display object in the display list hierarchy. </p>
		 * <p>You can use <code>parent</code> to move up multiple levels in the display list as in the following:</p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *      this.parent.parent.alpha = 20;
		 *      </pre>
		 * </div>
		 * 
		 * @return 
		 */
		public function get parent():DisplayObjectContainer {
			return _parent;
		}
		
		internal function setParent(value:DisplayObjectContainer):void {
			_parent = value;
		}

		/**
		 * <p> For a display object in a loaded SWF file, the <code>root</code> property is the top-most display object in the portion of the display list's tree structure represented by that SWF file. For a Bitmap object representing a loaded image file, the <code>root</code> property is the Bitmap object itself. For the instance of the main class of the first SWF file loaded, the <code>root</code> property is the display object itself. The <code>root</code> property of the Stage object is the Stage object itself. The <code>root</code> property is set to <code>null</code> for any display object that has not been added to the display list, unless it has been added to a display object container that is off the display list but that is a child of the top-most display object in a loaded SWF file. </p>
		 * <p>For example, if you create a new Sprite object by calling the <code>Sprite()</code> constructor method, its <code>root</code> property is <code>null</code> until you add it to the display list (or to a display object container that is off the display list but that is a child of the top-most display object in a SWF file).</p>
		 * <p>For a loaded SWF file, even though the Loader object used to load the file may not be on the display list, the top-most display object in the SWF file has its <code>root</code> property set to itself. The Loader object does not have its <code>root</code> property set until it is added as a child of a display object for which the <code>root</code> property is set.</p>
		 * 
		 * @return 
		 */
		public function get root():DisplayObject {
			return _root;
		}

		/**
		 * <p> Indicates the rotation of the DisplayObject instance, in degrees, from its original orientation. Values from 0 to 180 represent clockwise rotation; values from 0 to -180 represent counterclockwise rotation. Values outside this range are added to or subtracted from 360 to obtain a value within the range. For example, the statement <code>my_video.rotation = 450</code> is the same as <code> my_video.rotation = 90</code>. </p>
		 * 
		 * @return 
		 */
		public function get rotation():Number {
			return _rotation;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set rotation(value:Number):void {
			_rotation = value;
			writeTransform();
		}

		/**
		 * <p> Indicates the x-axis rotation of the DisplayObject instance, in degrees, from its original orientation relative to the 3D parent container. Values from 0 to 180 represent clockwise rotation; values from 0 to -180 represent counterclockwise rotation. Values outside this range are added to or subtracted from 360 to obtain a value within the range. </p>
		 * 
		 * @return 
		 */
		public function get rotationX():Number {
			return _rotationX;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set rotationX(value:Number):void {
			_rotationX = value;
		}

		/**
		 * <p> Indicates the y-axis rotation of the DisplayObject instance, in degrees, from its original orientation relative to the 3D parent container. Values from 0 to 180 represent clockwise rotation; values from 0 to -180 represent counterclockwise rotation. Values outside this range are added to or subtracted from 360 to obtain a value within the range. </p>
		 * 
		 * @return 
		 */
		public function get rotationY():Number {
			return _rotationY;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set rotationY(value:Number):void {
			_rotationY = value;
		}

		/**
		 * <p> Indicates the z-axis rotation of the DisplayObject instance, in degrees, from its original orientation relative to the 3D parent container. Values from 0 to 180 represent clockwise rotation; values from 0 to -180 represent counterclockwise rotation. Values outside this range are added to or subtracted from 360 to obtain a value within the range. </p>
		 * 
		 * @return 
		 */
		public function get rotationZ():Number {
			return _rotationZ;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set rotationZ(value:Number):void {
			_rotationZ = value;
		}

		/**
		 * <p> The current scaling grid that is in effect. If set to <code>null</code>, the entire display object is scaled normally when any scale transformation is applied. </p>
		 * <p>When you define the <code>scale9Grid</code> property, the display object is divided into a grid with nine regions based on the <code>scale9Grid</code> rectangle, which defines the center region of the grid. The eight other regions of the grid are the following areas: </p>
		 * <ul>
		 *  <li>The upper-left corner outside of the rectangle</li>
		 *  <li>The area above the rectangle </li>
		 *  <li>The upper-right corner outside of the rectangle</li>
		 *  <li>The area to the left of the rectangle</li>
		 *  <li>The area to the right of the rectangle</li>
		 *  <li>The lower-left corner outside of the rectangle</li>
		 *  <li>The area below the rectangle</li>
		 *  <li>The lower-right corner outside of the rectangle</li>
		 * </ul>
		 * <p>You can think of the eight regions outside of the center (defined by the rectangle) as being like a picture frame that has special rules applied to it when scaled.</p>
		 * <p>When the <code>scale9Grid</code> property is set and a display object is scaled, all text and gradients are scaled normally; however, for other types of objects the following rules apply:</p>
		 * <ul>
		 *  <li>Content in the center region is scaled normally. </li>
		 *  <li>Content in the corners is not scaled. </li>
		 *  <li>Content in the top and bottom regions is scaled horizontally only. Content in the left and right regions is scaled vertically only.</li>
		 *  <li>All fills (including bitmaps, video, and gradients) are stretched to fit their shapes.</li>
		 * </ul>
		 * <p>If a display object is rotated, all subsequent scaling is normal (and the <code>scale9Grid</code> property is ignored).</p>
		 * <p>For example, consider the following display object and a rectangle that is applied as the display object's <code>scale9Grid</code>:</p>
		 * <table class="+ topic/table adobe-d/adobetable ">
		 *  <tbody>
		 *   <tr>
		 *    <td align="center"><img src="../../images/scale9Grid-a.jpg" alt="display object image"> <p>The display object.</p></td>
		 *    <td align="center"><img src="../../images/scale9Grid-b.jpg" alt="display object scale 9 region"> <p>The red rectangle shows the <code>scale9Grid</code>.</p></td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p>When the display object is scaled or stretched, the objects within the rectangle scale normally, but the objects outside of the rectangle scale according to the <code>scale9Grid</code> rules:</p>
		 * <table class="+ topic/table adobe-d/adobetable ">
		 *  <tbody>
		 *   <tr>
		 *    <td>Scaled to 75%:</td>
		 *    <td><img src="../../images/scale9Grid-c.jpg" alt="display object at 75%"></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Scaled to 50%:</td>
		 *    <td><img src="../../images/scale9Grid-d.jpg" alt="display object at 50%"></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Scaled to 25%:</td>
		 *    <td><img src="../../images/scale9Grid-e.jpg" alt="display object at 25%"></td>
		 *   </tr>
		 *   <tr>
		 *    <td>Stretched horizontally 150%: </td>
		 *    <td><img src="../../images/scale9Grid-f.jpg" alt="display stretched 150%"></td>
		 *   </tr>
		 *  </tbody>
		 * </table>
		 * <p>A common use for setting <code>scale9Grid</code> is to set up a display object to be used as a component, in which edge regions retain the same width when the component is scaled.</p>
		 * 
		 * @return 
		 */
		public function get scale9Grid():Rectangle {
			return _scale9Grid;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set scale9Grid(value:Rectangle):void {
			_scale9Grid = value;
		}

		/**
		 * <p> Indicates the horizontal scale (percentage) of the object as applied from the registration point. The default registration point is (0,0). 1.0 equals 100% scale. </p>
		 * <p>Scaling the local coordinate system changes the <code>x</code> and <code>y</code> property values, which are defined in whole pixels. </p>
		 * 
		 * @return 
		 */
		public function get scaleX():Number {
			return _scaleX;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set scaleX(value:Number):void {
			_scaleX = value;
			writeTransform();
		}

		/**
		 * <p> Indicates the vertical scale (percentage) of an object as applied from the registration point of the object. The default registration point is (0,0). 1.0 is 100% scale. </p>
		 * <p>Scaling the local coordinate system changes the <code>x</code> and <code>y</code> property values, which are defined in whole pixels. </p>
		 * 
		 * @return 
		 */
		public function get scaleY():Number {
			return _scaleY;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set scaleY(value:Number):void {
			_scaleY = value;
			writeTransform();
		}

		/**
		 * <p> Indicates the depth scale (percentage) of an object as applied from the registration point of the object. The default registration point is (0,0). 1.0 is 100% scale. </p>
		 * <p>Scaling the local coordinate system changes the <code>x</code>, <code>y</code> and <code>z</code> property values, which are defined in whole pixels. </p>
		 * 
		 * @return 
		 */
		public function get scaleZ():Number {
			return _scaleZ;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set scaleZ(value:Number):void {
			_scaleZ = value;
		}

		/**
		 * <p> The scroll rectangle bounds of the display object. The display object is cropped to the size defined by the rectangle, and it scrolls within the rectangle when you change the <code>x</code> and <code>y</code> properties of the <code>scrollRect</code> object. </p>
		 * <p>The properties of the <code>scrollRect</code> Rectangle object use the display object's coordinate space and are scaled just like the overall display object. The corner bounds of the cropped window on the scrolling display object are the origin of the display object (0,0) and the point defined by the width and height of the rectangle. They are not centered around the origin, but use the origin to define the upper-left corner of the area. A scrolled display object always scrolls in whole pixel increments. </p>
		 * <p>You can scroll an object left and right by setting the <code>x</code> property of the <code>scrollRect</code> Rectangle object. You can scroll an object up and down by setting the <code>y</code> property of the <code>scrollRect</code> Rectangle object. If the display object is rotated 90° and you scroll it left and right, the display object actually scrolls up and down.</p>
		 * <p>Note that changes to the <code>scrollRect</code> property are only processed when the object is rendered. Thus methods like <code>localToGlobal</code> may not produce the expected result if called immediately after modifying <code>scrollRect</code>.</p>
		 * <p><b>Note:</b> Starting with Flash Player 11.4/AIR 3.4, negative values for the width or the height of the rectangle are changed to 0.</p>
		 * 
		 * @return 
		 */
		public function get scrollRect():Rectangle {
			return _scrollRect;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set scrollRect(value:Rectangle):void {
			_scrollRect = value;
		}

		/**
		 * <p> The Stage of the display object. A Flash runtime application has only one Stage object. For example, you can create and load multiple display objects into the display list, and the <code>stage</code> property of each display object refers to the same Stage object (even if the display object belongs to a loaded SWF file). </p>
		 * <p>If a display object is not added to the display list, its <code>stage</code> property is set to <code>null</code>.</p>
		 * 
		 * @return 
		 */
		public function get stage():Stage {
			/*if (this is Stage)
				return this as Stage;
			*/if (parent == null)
				return null;
			return parent.stage;
		}
		
		public function get svgElement():SVGElement {
			if (_svgElement == null) {
				_svgElement = createSVGElement();
			}
			return _svgElement;
		}
		
		protected function createSVGElement():SVGElement {
			return null;
		}

		/**
		 * <p> An object with properties pertaining to a display object's matrix, color transform, and pixel bounds. The specific properties — matrix, colorTransform, and three read-only properties (<code>concatenatedMatrix</code>, <code>concatenatedColorTransform</code>, and <code>pixelBounds</code>) — are described in the entry for the Transform class. </p>
		 * <p>Each of the transform object's properties is itself an object. This concept is important because the only way to set new values for the matrix or colorTransform objects is to create a new object and copy that object into the transform.matrix or transform.colorTransform property.</p>
		 * <p>For example, to increase the <code>tx</code> value of a display object's matrix, you must make a copy of the entire matrix object, then copy the new object into the matrix property of the transform object:</p>
		 * <pre><code>
		 *     var myMatrix:Matrix = myDisplayObject.transform.matrix;  
		 *     myMatrix.tx += 10; 
		 *     myDisplayObject.transform.matrix = myMatrix;  
		 *     </code></pre>
		 * <p>You cannot directly set the <code>tx</code> property. The following code has no effect on <code>myDisplayObject</code>: </p>
		 * <pre><code>
		 *     myDisplayObject.transform.matrix.tx += 10;
		 *     </code></pre>
		 * <p>You can also copy an entire transform object and assign it to another display object's transform property. For example, the following code copies the entire transform object from <code>myOldDisplayObj</code> to <code>myNewDisplayObj</code>:</p>
		 * <code>myNewDisplayObj.transform = myOldDisplayObj.transform;</code>
		 * <p>The resulting display object, <code>myNewDisplayObj</code>, now has the same values for its matrix, color transform, and pixel bounds as the old display object, <code>myOldDisplayObj</code>.</p>
		 * <p>Note that AIR for TV devices use hardware acceleration, if it is available, for color transforms.</p>
		 * 
		 * @return 
		 */
		public function get transform():Transform {
			return _transform;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set transform(value:Transform):void {
			_transform = value;
		}

		/**
		 * <p> Whether or not the display object is visible. Display objects that are not visible are disabled. For example, if <code>visible=false</code> for an InteractiveObject instance, it cannot be clicked. </p>
		 * 
		 * @return 
		 */
		public function get visible():Boolean {
			return _visible;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set visible(value:Boolean):void {
			_visible = value;
		}

		/**
		 * <p> Indicates the width of the display object, in pixels. The width is calculated based on the bounds of the content of the display object. When you set the <code>width</code> property, the <code>scaleX</code> property is adjusted accordingly, as shown in the following code: </p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *     var rect:Shape = new Shape();
		 *     rect.graphics.beginFill(0xFF0000);
		 *     rect.graphics.drawRect(0, 0, 100, 100);
		 *     trace(rect.scaleX) // 1;
		 *     rect.width = 200;
		 *     trace(rect.scaleX) // 2;</pre>
		 * </div>
		 * <p>Except for TextField and Video objects, a display object with no content (such as an empty sprite) has a width of 0, even if you try to set <code>width</code> to a different value.</p>
		 * 
		 * @return 
		 */
		public function get width():Number {
			return getBoundingRect().width;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set width(value:Number):void {
			_width = value;
			this.scaleX = this.scaleX * value / this.width;
		}

		/**
		 * <p> Indicates the <i>x</i> coordinate of the DisplayObject instance relative to the local coordinates of the parent DisplayObjectContainer. If the object is inside a DisplayObjectContainer that has transformations, it is in the local coordinate system of the enclosing DisplayObjectContainer. Thus, for a DisplayObjectContainer rotated 90° counterclockwise, the DisplayObjectContainer's children inherit a coordinate system that is rotated 90° counterclockwise. The object's coordinates refer to the registration point position. </p>
		 * 
		 * @return 
		 */
		public function get x():Number {
			return _x;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set x(value:Number):void {
			_x = value;
			writeTransform();
		}

		/**
		 * <p> Indicates the <i>y</i> coordinate of the DisplayObject instance relative to the local coordinates of the parent DisplayObjectContainer. If the object is inside a DisplayObjectContainer that has transformations, it is in the local coordinate system of the enclosing DisplayObjectContainer. Thus, for a DisplayObjectContainer rotated 90° counterclockwise, the DisplayObjectContainer's children inherit a coordinate system that is rotated 90° counterclockwise. The object's coordinates refer to the registration point position. </p>
		 * 
		 * @return 
		 */
		public function get y():Number {
			return _y;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set y(value:Number):void {
			_y = value;
			writeTransform();
		}

		/**
		 * <p> Indicates the z coordinate position along the z-axis of the DisplayObject instance relative to the 3D parent container. The z property is used for 3D coordinates, not screen or pixel coordinates. </p>
		 * <p>When you set a <code>z</code> property for a display object to something other than the default value of <code>0</code>, a corresponding Matrix3D object is automatically created. for adjusting a display object's position and orientation in three dimensions. When working with the z-axis, the existing behavior of x and y properties changes from screen or pixel coordinates to positions relative to the 3D parent container.</p>
		 * <p>For example, a child of the <code>_root</code> at position x = 100, y = 100, z = 200 is not drawn at pixel location (100,100). The child is drawn wherever the 3D projection calculation puts it. The calculation is:</p>
		 * <p><code> (x*cameraFocalLength/cameraRelativeZPosition, y*cameraFocalLength/cameraRelativeZPosition)</code></p>
		 * 
		 * @return 
		 */
		public function get z():Number {
			return _z;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set z(value:Number):void {
			_z = value;
		}

		
		override public function addEventListener(type:String, listener:Function, useCapture:Boolean = false, priority:int = 0, useWeakReference:Boolean = false):void {
			if (type == null || listener == null)
				return;
			if (type == Event.ENTER_FRAME || type == Event.EXIT_FRAME || type == Event.FRAME_CONSTRUCTED || type == Event.RENDER) {
				var a:Vector.<DisplayObject> = frameListeners[type];
				if (a == null) {
					a = new Vector.<DisplayObject>();
					frameListeners[type] = a;
				}
				if (a.indexOf(this)==-1)
					a.push(this);
			}
			if (Event.getType(type, Vector.<Class>([MouseEvent, KeyboardEvent]))!=null) {
				if (this.svgElement!=null)
					this.svgElement.addEventListener(type, svgEvent);
			}
			super.addEventListener(type, listener, useCapture, priority, useWeakReference);
		}
		
		private function svgEvent(e:Event):void {
			var ev:flash.events.Event;
			if (e.type in flash.events.MouseEvent) {
				ev = new flash.events.MouseEvent(e.type);
			}
			else {
				trace("Event class not found for type ", e.type);
				ev = new flash.events.Event(e.type);
			}
			this.dispatchEvent(ev);
		}
		
		internal function getBoundingRect():Rectangle {
			//TODO: manage cache
			return render(svgElement, function():Rectangle {
				var box:* = _svgElement.getBBox();
				return new Rectangle(box.x, box.y, box.width, box.height);
			});
		}
		
		/**
		 * <p> Returns a rectangle that defines the area of the display object relative to the coordinate system of the <code>targetCoordinateSpace</code> object. Consider the following code, which shows how the rectangle returned can vary depending on the <code>targetCoordinateSpace</code> parameter that you pass to the method: </p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *      var container:Sprite = new Sprite();
		 *      container.x = 100;
		 *      container.y = 100;
		 *      this.addChild(container);
		 *      var contents:Shape = new Shape();
		 *      contents.graphics.drawCircle(0,0,100);
		 *      container.addChild(contents);
		 *      trace(contents.getBounds(container));
		 *       // (x=-100, y=-100, w=200, h=200)
		 *      trace(contents.getBounds(this));
		 *       // (x=0, y=0, w=200, h=200)
		 *      </pre>
		 * </div>
		 * <p><b>Note:</b> Use the <code>localToGlobal()</code> and <code>globalToLocal()</code> methods to convert the display object's local coordinates to display coordinates, or display coordinates to local coordinates, respectively.</p>
		 * <p>The <code>getBounds()</code> method is similar to the <code>getRect()</code> method; however, the Rectangle returned by the <code>getBounds()</code> method includes any strokes on shapes, whereas the Rectangle returned by the <code>getRect()</code> method does not. For an example, see the description of the <code>getRect()</code> method.</p>
		 * 
		 * @param targetCoordinateSpace  — The display object that defines the coordinate system to use. 
		 * @return  — The rectangle that defines the area of the display object relative to the  object's coordinate system. 
		 */
		public function getBounds(targetCoordinateSpace:DisplayObject):Rectangle {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a rectangle that defines the boundary of the display object, based on the coordinate system defined by the <code>targetCoordinateSpace</code> parameter, excluding any strokes on shapes. The values that the <code>getRect()</code> method returns are the same or smaller than those returned by the <code>getBounds()</code> method. </p>
		 * <p><b>Note:</b> Use <code>localToGlobal()</code> and <code>globalToLocal()</code> methods to convert the display object's local coordinates to Stage coordinates, or Stage coordinates to local coordinates, respectively.</p>
		 * 
		 * @param targetCoordinateSpace  — The display object that defines the coordinate system to use. 
		 * @return  — The rectangle that defines the area of the display object relative to the  object's coordinate system. 
		 */
		public function getRect(targetCoordinateSpace:DisplayObject):Rectangle {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Converts the <code>point</code> object from the Stage (global) coordinates to the display object's (local) coordinates. </p>
		 * <p>To use this method, first create an instance of the Point class. The <i>x</i> and <i>y</i> values that you assign represent global coordinates because they relate to the origin (0,0) of the main display area. Then pass the Point instance as the parameter to the <code>globalToLocal()</code> method. The method returns a new Point object with <i>x</i> and <i>y</i> values that relate to the origin of the display object instead of the origin of the Stage.</p>
		 * 
		 * @param point  — An object created with the Point class. The Point object specifies the <i>x</i> and <i>y</i> coordinates as properties. 
		 * @return  — A Point object with coordinates relative to the display object. 
		 */
		public function globalToLocal(point:Point):Point {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Converts a two-dimensional point from the Stage (global) coordinates to a three-dimensional display object's (local) coordinates. </p>
		 * <p>To use this method, first create an instance of the Point class. The x and y values that you assign to the Point object represent global coordinates because they are relative to the origin (0,0) of the main display area. Then pass the Point object to the <code>globalToLocal3D()</code> method as the <code>point</code> parameter. The method returns three-dimensional coordinates as a Vector3D object containing <code>x</code>, <code>y</code>, and <code>z</code> values that are relative to the origin of the three-dimensional display object.</p>
		 * 
		 * @param point  — A two dimensional Point object representing global x and y coordinates. 
		 * @return  — A Vector3D object with coordinates relative to the three-dimensional display object. 
		 */
		public function globalToLocal3D(point:Point):Vector3D {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Evaluates the bounding box of the display object to see if it overlaps or intersects with the bounding box of the <code>obj</code> display object. </p>
		 * 
		 * @param obj  — The display object to test against. 
		 * @return  —  if the bounding boxes of the display objects intersect;  if not. 
		 */
		public function hitTestObject(obj:DisplayObject):Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Evaluates the display object to see if it overlaps or intersects with the point specified by the <code>x</code> and <code>y</code> parameters. The <code>x</code> and <code>y</code> parameters specify a point in the coordinate space of the Stage, not the display object container that contains the display object (unless that display object container is the Stage). </p>
		 * 
		 * @param x  — The <i>x</i> coordinate to test against this object. 
		 * @param y  — The <i>y</i> coordinate to test against this object. 
		 * @param shapeFlag  — Whether to check against the actual pixels of the object (<code>true</code>) or the bounding box (<code>false</code>). 
		 * @return  —  if the display object overlaps or intersects with the specified point;  otherwise. 
		 */
		public function hitTestPoint(x:Number, y:Number, shapeFlag:Boolean = false):Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Converts a three-dimensional point of the three-dimensional display object's (local) coordinates to a two-dimensional point in the Stage (global) coordinates. </p>
		 * <p>For example, you can only use two-dimensional coordinates (x,y) to draw with the <code>display.Graphics</code> methods. To draw a three-dimensional object, you need to map the three-dimensional coordinates of a display object to two-dimensional coordinates. First, create an instance of the Vector3D class that holds the x-, y-, and z- coordinates of the three-dimensional display object. Then pass the Vector3D object to the <code>local3DToGlobal()</code> method as the <code>point3d</code> parameter. The method returns a two-dimensional Point object that can be used with the Graphics API to draw the three-dimensional object.</p>
		 * 
		 * @param point3d  — A Vector3D object containing either a three-dimensional point or the coordinates of the three-dimensional display object. 
		 * @return  — A two-dimensional point representing a three-dimensional point in two-dimensional space. 
		 */
		public function local3DToGlobal(point3d:Vector3D):Point {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Converts the <code>point</code> object from the display object's (local) coordinates to the Stage (global) coordinates. </p>
		 * <p>This method allows you to convert any given <i>x</i> and <i>y</i> coordinates from values that are relative to the origin (0,0) of a specific display object (local coordinates) to values that are relative to the origin of the Stage (global coordinates).</p>
		 * <p>To use this method, first create an instance of the Point class. The <i>x</i> and <i>y</i> values that you assign represent local coordinates because they relate to the origin of the display object.</p>
		 * <p>You then pass the Point instance that you created as the parameter to the <code>localToGlobal()</code> method. The method returns a new Point object with <i>x</i> and <i>y</i> values that relate to the origin of the Stage instead of the origin of the display object.</p>
		 * 
		 * @param point  — The name or identifier of a point created with the Point class, specifying the <i>x</i> and <i>y</i> coordinates as properties. 
		 * @return  — A Point object with coordinates relative to the Stage. 
		 */
		public function localToGlobal(point:Point):Point {
			throw new Error("Not implemented");
		}
		
		internal static function frameEvent(type:String):void {
			var e:Event = new Event(type);
			var a:Vector.<DisplayObject> = frameListeners[type];
			if (a == null)
				return;
			for each (var d:DisplayObject in a) {
				d.dispatchEvent(e);
			}
			if (type == Event.EXIT_FRAME && SVG_RENDERER.parentNode!=null) {
				document.body.removeChild(SVG_RENDERER);
			}
		}
		
		private function writeTransform():void {
			var t:String = "";
			if (x != 0 || y != 0)
				t += "translate(" + x + "," + y + ") ";
			if (rotation != 0)
				t += "rotate(" + rotation + ") ";
			if (scaleX != 1 || scaleY != 1)
				t += "scale(" + scaleX + "," + scaleY + ") ";
			svgElement.setAttribute("transform", t);
		}
		
		public static function render(el:SVGElement, fx:Function):* {
			//TODO: gérer du cache
			var prevParent:Element = null;
			var prevPosition:int;
			prevParent = el.parentElement;
			if (prevParent!=null)
				prevPosition = [].indexOf.apply(prevParent.childNodes, el);
			SVG_RENDERER.appendChild(el);
			if (SVG_RENDERER.parentNode==null)
				document.body.appendChild(SVG_RENDERER);
			var ret:* = fx();
			if (prevParent!=null)
				prevParent.insertBefore(el, prevPosition<prevParent.childNodes.length ? prevParent.childNodes[prevPosition] : null);
			return ret;
		}
	}
}
