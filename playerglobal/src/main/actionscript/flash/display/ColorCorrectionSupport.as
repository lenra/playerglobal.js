package flash.display {
	/**
	 *  The ColorCorrectionSupport class provides values for the <code>flash.display.Stage.colorCorrectionSupport</code> property. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Stage.html#colorCorrectionSupport" target="">flash.display.Stage.colorCorrectionSupport</a>
	 * </div><br><hr>
	 */
	public class ColorCorrectionSupport {
		/**
		 * <p> Color correction is supported, but off by default. </p>
		 */
		public static const DEFAULT_OFF:String = "defaultOff";
		/**
		 * <p> Color correction is supported, and on by default. </p>
		 */
		public static const DEFAULT_ON:String = "defaultOn";
		/**
		 * <p> Color correction is not supported by the host environment. </p>
		 */
		public static const UNSUPPORTED:String = "unsupported";
	}
}
