package flash.display {
	/**
	 *  The IBitmapDrawable interface is implemented by objects that can be passed as the <code>source</code> parameter of the <code>draw()</code> method of the BitmapData class. These objects are of type BitmapData or DisplayObject. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="BitmapData.html#draw()" target="">flash.display.BitmapData.draw()</a>
	 *  <br>
	 *  <a href="BitmapData.html" target="">flash.display.BitmapData</a>
	 *  <br>
	 *  <a href="DisplayObject.html" target="">flash.display.DisplayObject</a>
	 * </div><br><hr>
	 */
	public interface IBitmapDrawable {
	}
}
