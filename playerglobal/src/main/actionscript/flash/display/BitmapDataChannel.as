package flash.display {
	/**
	 *  The BitmapDataChannel class is an enumeration of constant values that indicate which channel to use: red, blue, green, or alpha transparency. <p>When you call some methods, you can use the bitwise OR operator (<code>|</code>) to combine BitmapDataChannel constants to indicate multiple color channels.</p> <p>The BitmapDataChannel constants are provided for use as values in the following:</p> <ul> 
	 *  <li>The <code>sourceChannel</code> and <code>destChannel</code> parameters of the <code>flash.display.BitmapData.copyChannel()</code> method</li> 
	 *  <li>The <code>channelOptions</code> parameter of the <code>flash.display.BitmapData.noise()</code> method</li> 
	 *  <li>The <code>flash.filters.DisplacementMapFilter.componentX</code> and <code>flash.filters.DisplacementMapFilter.componentY</code> properties</li> 
	 * </ul> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="BitmapData.html#copyChannel()" target="">flash.display.BitmapData.copyChannel()</a>
	 *  <br>
	 *  <a href="BitmapData.html#noise()" target="">flash.display.BitmapData.noise()</a>
	 *  <br>
	 *  <a href="../../flash/filters/DisplacementMapFilter.html#componentX" target="">flash.filters.DisplacementMapFilter.componentX</a>
	 *  <br>
	 *  <a href="../../flash/filters/DisplacementMapFilter.html#componentY" target="">flash.filters.DisplacementMapFilter.componentY</a>
	 * </div><br><hr>
	 */
	public class BitmapDataChannel {
		/**
		 * <p> The alpha channel. </p>
		 */
		public static const ALPHA:uint = 8;
		/**
		 * <p> The blue channel. </p>
		 */
		public static const BLUE:uint = 4;
		/**
		 * <p> The green channel. </p>
		 */
		public static const GREEN:uint = 2;
		/**
		 * <p> The red channel. </p>
		 */
		public static const RED:uint = 1;
	}
}
