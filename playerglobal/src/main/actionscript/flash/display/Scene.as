package flash.display {
	/**
	 *  The Scene class includes properties for identifying the name, labels, and number of frames in a scene. A Scene object instance is created in Flash Professional, not by writing ActionScript code. The MovieClip class includes a <code>currentScene</code> property, which is a Scene object that identifies the scene in which the playhead is located in the timeline of the MovieClip instance. The <code>scenes</code> property of the MovieClip class is an array of Scene objects. Also, the <code>gotoAndPlay()</code> and <code>gotoAndStop()</code> methods of the MovieClip class use Scene objects as parameters. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="MovieClip.html#currentScene" target="">MovieClip.currentScene</a>
	 *  <br>
	 *  <a href="MovieClip.html#scenes" target="">MovieClip.scenes</a>
	 *  <br>
	 *  <a href="MovieClip.html#gotoAndPlay()" target="">MovieClip.gotoAndPlay()</a>
	 *  <br>
	 *  <a href="MovieClip.html#gotoAndStop()" target="">MovieClip.gotoAndStop()</a>
	 * </div><br><hr>
	 */
	public class Scene {
		private var _labels:Array;
		private var _name:String;
		private var _numFrames:int;

		/**
		 * <p> An array of FrameLabel objects for the scene. Each FrameLabel object contains a <code>frame</code> property, which specifies the frame number corresponding to the label, and a <code>name</code> property. </p>
		 * 
		 * @return 
		 */
		public function get labels():Array {
			return _labels;
		}

		/**
		 * <p> The name of the scene. </p>
		 * 
		 * @return 
		 */
		public function get name():String {
			return _name;
		}

		/**
		 * <p> The number of frames in the scene. </p>
		 * 
		 * @return 
		 */
		public function get numFrames():int {
			return _numFrames;
		}
	}
}
