package flash.display {
	/**
	 *  The PNGEncoderOptions class defines a compression algorithm for the <code>flash.display.BitmapData.encode()</code> method. <p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS4768145595f94108-17913eb4136eaab51c7-8000.html" target="_blank">Compressing bitmap data</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="BitmapData.html" target="">flash.display.BitmapData</a>
	 *  <br>
	 *  <a href="BitmapData.html#encode()" target="">flash.display.BitmapData.encode()</a>
	 * </div><br><hr>
	 */
	public class PNGEncoderOptions {
		private var _fastCompression:Boolean;

		/**
		 * <p> Creates a PNGEncoderOptions object, optionally specifying compression settings. </p>
		 * 
		 * @param fastCompression  — The initial compression mode. 
		 */
		public function PNGEncoderOptions(fastCompression:Boolean = false) {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Chooses compression speed over file size. Setting this property to true improves compression speed but creates larger files. </p>
		 * 
		 * @return 
		 */
		public function get fastCompression():Boolean {
			return _fastCompression;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set fastCompression(value:Boolean):void {
			_fastCompression = value;
		}
	}
}
