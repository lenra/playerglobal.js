package flash.display {
	import flash.events.Event;
	import flash.events.EventDispatcher;

	[Event(name="frameLabel", type="flash.events.Event")]
	/**
	 *  The FrameLabel object contains properties that specify a frame number and the corresponding label name. <p>The MovieClip class includes a <code>currentLabels</code> property, which is an Array of FrameLabel objects for the current scene. If the MovieClip instance does not use scenes, the Array includes all frame labels from the entire MovieClip instance.</p> <p>The Scene class includes a <code>labels</code> property, which is an Array of FrameLabel objects for the scene. </p> <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Scene.html#labels" target="">Scene.labels</a>
	 *  <br>
	 *  <a href="MovieClip.html#currentLabel" target="">MovieClip.currentLabel</a>
	 *  <br>
	 *  <a href="MovieClip.html#currentLabels" target="">MovieClip.currentLabels</a>
	 *  <br>
	 *  <a href="MovieClip.html#currentScene" target="">MovieClip.currentScene</a>
	 *  <br>
	 *  <a href="MovieClip.html#scenes" target="">MovieClip.scenes</a>
	 *  <br>
	 *  <a href="MovieClip.html#gotoAndPlay()" target="">MovieClip.gotoAndPlay()</a>
	 *  <br>
	 *  <a href="MovieClip.html#gotoAndStop()" target="">MovieClip.gotoAndStop()</a>
	 * </div><br><hr>
	 */
	public class FrameLabel extends EventDispatcher {
		private var _frame:int;
		private var _name:String;

		/**
		 * <p> Constructor. </p>
		 * 
		 * @param name  — The label name. 
		 * @param frame  — The frame number associated with the label name. 
		 */
		public function FrameLabel(name:String, frame:int) {
			super();
			throw new Error("Not implemented");
		}

		/**
		 * <p> The frame number containing the label. </p>
		 * 
		 * @return 
		 */
		public function get frame():int {
			return _frame;
		}

		/**
		 * <p> The name of the label. </p>
		 * 
		 * @return 
		 */
		public function get name():String {
			return _name;
		}
	}
}
