package flash.display {
	/**
	 *  The StageScaleMode class provides values for the <code>Stage.scaleMode</code> property. <p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS0D75B487-23B9-402d-A52D-CB3C4CEB9EE4.html" target="_blank">Controlling Stage scaling</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Stage.html#scaleMode" target="">flash.display.Stage.scaleMode</a>
	 * </div><br><hr>
	 */
	public class StageScaleMode {
		/**
		 * <p> Specifies that the entire application be visible in the specified area without trying to preserve the original aspect ratio. Distortion can occur. </p>
		 */
		public static const EXACT_FIT:String = "exactFit";
		/**
		 * <p> Specifies that the entire application fill the specified area, without distortion but possibly with some cropping, while maintaining the original aspect ratio of the application. </p>
		 */
		public static const NO_BORDER:String = "noBorder";
		/**
		 * <p> Specifies that the size of the application be fixed, so that it remains unchanged even as the size of the player window changes. Cropping might occur if the player window is smaller than the content. </p>
		 */
		public static const NO_SCALE:String = "noScale";
		/**
		 * <p> Specifies that the entire application be visible in the specified area without distortion while maintaining the original aspect ratio of the application. Borders can appear on two sides of the application. </p>
		 */
		public static const SHOW_ALL:String = "showAll";
	}
}
