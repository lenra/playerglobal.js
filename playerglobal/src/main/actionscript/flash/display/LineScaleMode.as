package flash.display {
	/**
	 *  The LineScaleMode class provides values for the <code>scaleMode</code> parameter in the <code>Graphics.lineStyle()</code> method. <p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Graphics.html#lineStyle()" target="">flash.display.Graphics.lineStyle()</a>
	 * </div><br><hr>
	 */
	public class LineScaleMode {
		/**
		 * <p> With this setting used as the <code>scaleMode</code> parameter of the <code>lineStyle()</code> method, the thickness of the line scales <i>only</i> horizontally. For example, consider the following circles, drawn with a one-pixel line, and each with the <code>scaleMode</code> parameter set to <code>LineScaleMode.HORIZONTAL</code>. The circle on the left is scaled only horizontally, and the circle on the right is scaled both vertically and horizontally. </p>
		 * <p><img src="../../images/LineScaleMode_HORIZONTAL.jpg" alt="A circle scaled horizontally, and a circle scaled both vertically and horizontally."></p>
		 */
		public static const HORIZONTAL:String = "horizontal";
		/**
		 * <p> With this setting used as the <code>scaleMode</code> parameter of the <code>lineStyle()</code> method, the thickness of the line never scales. </p>
		 */
		public static const NONE:String = "none";
		/**
		 * <p> With this setting used as the <code>scaleMode</code> parameter of the <code>lineStyle()</code> method, the thickness of the line always scales when the object is scaled (the default). </p>
		 */
		public static const NORMAL:String = "normal";
		/**
		 * <p> With this setting used as the <code>scaleMode</code> parameter of the <code>lineStyle()</code> method, the thickness of the line scales <i>only</i> vertically. For example, consider the following circles, drawn with a one-pixel line, and each with the <code>scaleMode</code> parameter set to <code>LineScaleMode.VERTICAL</code>. The circle on the left is scaled only vertically, and the circle on the right is scaled both vertically and horizontally. </p>
		 * <p><img src="../../images/LineScaleMode_VERTICAL.jpg" alt="A circle scaled vertically, and a circle scaled both vertically and horizontally."></p>
		 */
		public static const VERTICAL:String = "vertical";
	}
}
