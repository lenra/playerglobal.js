package flash.display {
	import flash.filters.BitmapFilter;
	import flash.geom.ColorTransform;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.ByteArray;

	/**
	 *  The BitmapData class lets you work with the data (pixels) of a <span>Bitmap object</span> . You can use the methods of the BitmapData class to create arbitrarily sized transparent or opaque bitmap images and manipulate them in various ways at runtime. <span>You can also access the BitmapData for a bitmap image that you load with the <code>flash.display.Loader</code> class.</span> <p>This class lets you separate bitmap rendering operations from the internal display updating routines of Flash Player. By manipulating a BitmapData object directly, you can create complex images without incurring the per-frame overhead of constantly redrawing the content from vector data.</p> <p>The methods of the BitmapData class support effects that are not available through the filters available to non-bitmap display objects.</p> <p>A BitmapData object contains an array of pixel data. This data can represent either a fully opaque bitmap or a transparent bitmap that contains alpha channel data. Either type of BitmapData object is stored as a buffer of 32-bit integers. Each 32-bit integer determines the properties of a single pixel in the bitmap.</p> <p>Each 32-bit integer is a combination of four 8-bit channel values (from 0 to 255) that describe the alpha transparency and the red, green, and blue (ARGB) values of the pixel. (For ARGB values, the most significant byte represents the alpha channel value, followed by red, green, and blue.)</p> <p>The four channels (alpha, red, green, and blue) are represented as numbers when you use them with the <code>BitmapData.copyChannel()</code> method or the <code>DisplacementMapFilter.componentX</code> and <code>DisplacementMapFilter.componentY</code> properties, and these numbers are represented by the following constants in the BitmapDataChannel class:</p> <ul> 
	 *  <li> <code>BitmapDataChannel.ALPHA</code> </li> 
	 *  <li> <code>BitmapDataChannel.RED</code> </li> 
	 *  <li> <code>BitmapDataChannel.GREEN</code> </li> 
	 *  <li> <code>BitmapDataChannel.BLUE</code> </li> 
	 * </ul> <p>You can attach BitmapData objects to a Bitmap object by using the <code>bitmapData</code> property of the Bitmap object.</p> <p>You can use a BitmapData object to fill a Graphics object by using the <code>Graphics.beginBitmapFill()</code> method.</p> <p>In the AIR runtime, the DockIcon, Icon, InteractiveIcon, and SystemTrayIcon classes each include a <code>bitmaps</code> property that is an array of BitmapData objects that define the bitmap images for an icon.</p> <p>In AIR 1.5 and Flash Player 10, the maximum size for a BitmapData object is 8,191 pixels in width or height, and the total number of pixels cannot exceed 16,777,215 pixels. (So, if a BitmapData object is 8,191 pixels wide, it can only be 2,048 pixels high.) In Flash Player 9 and earlier and AIR 1.1 and earlier, the limitation is 2,880 pixels in height and 2,880 in width.</p> <p>Starting with AIR 3 and Flash player 11, the size limits for a BitmapData object have been removed. The maximum size of a bitmap is now dependent on the operating system.</p> <p>Calls to any method or property of a BitmapData object throw an ArgumentError error if the BitmapData object is invalid (for example, if it has <code>height == 0</code> and <code>width == 0</code>) or it has been disposed of via dispose(). </p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d64.html" target="_blank">Manipulating pixels</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d60.html" target="_blank">Copying bitmap data</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS4768145595f94108-17913eb4136eaab51c7-8000.html" target="_blank">Compressing bitmap data</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d63.html" target="_blank">Making textures with noise functions</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d62.html" target="_blank">Scrolling bitmaps</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d61.html" target="_blank">Bitmap example: Animated spinning moon</a>
	 * </div><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e1b.html" target="_blank">Working with bitmaps</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d66.html" target="_blank">Basics of working with bitmaps</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d65.html" target="_blank">The Bitmap and BitmapData classes</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7d5b.html" target="_blank">Taking advantage of mipmapping</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS52621785137562065a8e668112d98c8c4df-8000.html" target="_blank">Asynchronous decoding of bitmap images</a>
	 *  <br>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Bitmap.html#bitmapData" target="">flash.display.Bitmap.bitmapData</a>
	 *  <br>
	 *  <a href="../../flash/desktop/DockIcon.html#bitmaps" target="">flash.desktop.DockIcon.bitmaps</a>
	 *  <br>
	 *  <a href="Graphics.html#beginBitmapFill()" target="">flash.display.Graphics.beginBitmapFill()</a>
	 *  <br>
	 *  <a href="../../flash/desktop/Icon.html#bitmaps" target="">flash.desktop.Icon.bitmaps</a>
	 *  <br>
	 *  <a href="../../flash/desktop/InteractiveIcon.html#bitmaps" target="">flash.desktop.InteractiveIcon.bitmaps</a>
	 *  <br>
	 *  <a href="Loader.html" target="">flash.display.Loader</a>
	 *  <br>
	 *  <a href="../../flash/desktop/SystemTrayIcon.html#bitmaps" target="">flash.desktop.SystemTrayIcon.bitmaps</a>
	 * </div><br><hr>
	 */
	public class BitmapData implements IBitmapDrawable {
		private var _height:int;
		private var _rect:Rectangle;
		private var _transparent:Boolean;
		private var _width:int;

		/**
		 * <p> Creates a BitmapData object with a specified width and height. If you specify a value for the <code>fillColor</code> parameter, every pixel in the bitmap is set to that color. </p>
		 * <p>By default, the bitmap is created as transparent, unless you pass the value <code>false</code> for the <code>transparent</code> parameter. After you create an opaque bitmap, you cannot change it to a transparent bitmap. Every pixel in an opaque bitmap uses only 24 bits of color channel information. If you define the bitmap as transparent, every pixel uses 32 bits of color channel information, including an alpha transparency channel.</p>
		 * <p>In AIR 1.5 and Flash Player 10, the maximum size for a BitmapData object is 8,191 pixels in width or height, and the total number of pixels cannot exceed 16,777,215 pixels. (So, if a BitmapData object is 8,191 pixels wide, it can only be 2,048 pixels high.) In Flash Player 9 and earlier and AIR 1.1 and earlier, the limitation is 2,880 pixels in height and 2,880 pixels in width. If you specify a width or height value that is greater than 2880, a new instance is not created.</p>
		 * 
		 * @param width  — The width of the bitmap image in pixels. 
		 * @param height  — The height of the bitmap image in pixels. 
		 * @param transparent  — Specifies whether the bitmap image supports per-pixel transparency. The default value is <code>true</code> (transparent). To create a fully transparent bitmap, set the value of the <code>transparent</code> parameter to <code>true</code> and the value of the <code>fillColor</code> parameter to 0x00000000 (or to 0). Setting the <code>transparent</code> property to <code>false</code> can result in minor improvements in rendering performance. 
		 * @param fillColor  — A 32-bit ARGB color value that you use to fill the bitmap image area. The default value is 0xFFFFFFFF (solid white). 
		 */
		public function BitmapData(width:int, height:int, transparent:Boolean = true, fillColor:uint = 0xFFFFFFFF) {
			throw new Error("Not implemented");
		}

		/**
		 * <p> The height of the bitmap image in pixels. </p>
		 * 
		 * @return 
		 */
		public function get height():int {
			return _height;
		}

		/**
		 * <p> The rectangle that defines the size and location of the bitmap image. The top and left of the rectangle are 0; the width and height are equal to the width and height in pixels of the BitmapData object. </p>
		 * 
		 * @return 
		 */
		public function get rect():Rectangle {
			return _rect;
		}

		/**
		 * <p> Defines whether the bitmap image supports per-pixel transparency. You can set this value only when you construct a BitmapData object by passing in <code>true</code> for the <code>transparent</code> parameter of the constructor. Then, after you create a BitmapData object, you can check whether it supports per-pixel transparency by determining if the value of the <code>transparent</code> property is <code>true</code>. </p>
		 * 
		 * @return 
		 */
		public function get transparent():Boolean {
			return _transparent;
		}

		/**
		 * <p> The width of the bitmap image in pixels. </p>
		 * 
		 * @return 
		 */
		public function get width():int {
			return _width;
		}

		/**
		 * <p> Takes a source image and a filter object and generates the filtered image. </p>
		 * <p>This method relies on the behavior of built-in filter objects, which determine the destination rectangle that is affected by an input source rectangle.</p>
		 * <p>After a filter is applied, the resulting image can be larger than the input image. For example, if you use a BlurFilter class to blur a source rectangle of (50,50,100,100) and a destination point of (10,10), the area that changes in the destination image is larger than (10,10,60,60) because of the blurring. This happens internally during the <code>applyFilter()</code> call.</p>
		 * <p>If the <code>sourceRect</code> parameter of the <code>sourceBitmapData</code> parameter is an interior region, such as (50,50,100,100) in a 200 x 200 image, the filter uses the source pixels outside the <code>sourceRect</code> parameter to generate the destination rectangle.</p>
		 * <p>If the BitmapData object and the object specified as the <code>sourceBitmapData</code> parameter are the same object, the application uses a temporary copy of the object to perform the filter. For best performance, avoid this situation.</p>
		 * 
		 * @param sourceBitmapData  — The input bitmap image to use. The source image can be a different BitmapData object or it can refer to the current BitmapData instance. 
		 * @param sourceRect  — A rectangle that defines the area of the source image to use as input. 
		 * @param destPoint  — The point within the destination image (the current BitmapData instance) that corresponds to the upper-left corner of the source rectangle. 
		 * @param filter  — The filter object that you use to perform the filtering operation. Each type of filter has certain requirements, as follows: <ul>
		 *  <li><b>BlurFilter</b> — This filter can use source and destination images that are either opaque or transparent. If the formats of the images do not match, the copy of the source image that is made during the filtering matches the format of the destination image.</li>
		 *  <li><b>BevelFilter, DropShadowFilter, GlowFilter </b> — The destination image of these filters must be a transparent image. Calling DropShadowFilter or GlowFilter creates an image that contains the alpha channel data of the drop shadow or glow. It does not create the drop shadow onto the destination image. If you use any of these filters with an opaque destination image, an exception is thrown.</li>
		 *  <li><b>ConvolutionFilter</b> — This filter can use source and destination images that are either opaque or transparent.</li>
		 *  <li><b>ColorMatrixFilter</b> — This filter can use source and destination images that are either opaque or transparent.</li>
		 *  <li><b>DisplacementMapFilter</b> — This filter can use source and destination images that are either opaque or transparent, but the source and destination image formats must be the same.</li>
		 * </ul> 
		 */
		public function applyFilter(sourceBitmapData:BitmapData, sourceRect:Rectangle, destPoint:Point, filter:BitmapFilter):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a new BitmapData object that is a clone of the original instance with an exact copy of the contained bitmap. </p>
		 * 
		 * @return  — A new BitmapData object that is identical to the original. 
		 */
		public function clone():BitmapData {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Adjusts the color values in a specified area of a bitmap image by using a <code>ColorTransform</code> object. If the rectangle matches the boundaries of the bitmap image, this method transforms the color values of the entire image. </p>
		 * 
		 * @param rect  — A Rectangle object that defines the area of the image in which the ColorTransform object is applied. 
		 * @param colorTransform  — A ColorTransform object that describes the color transformation values to apply. 
		 */
		public function colorTransform(rect:Rectangle, colorTransform:ColorTransform):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Compares two BitmapData objects. If the two BitmapData objects have the same dimensions (width and height), the method returns a new BitmapData object, in which each pixel is the "difference" between the pixels in the two source objects: </p>
		 * <ul>
		 *  <li>If two pixels are equal, the difference pixel is 0x00000000. </li>
		 *  <li>If two pixels have different RGB values (ignoring the alpha value), the difference pixel is 0xRRGGBB where RR/GG/BB are the individual difference values between red, green, and blue channels (the pixel value in the source object minus the pixel value in the <code>otherBitmapData</code> object). Alpha channel differences are ignored in this case. </li>
		 *  <li>If only the alpha channel value is different, the pixel value is 0x<i>ZZ</i>FFFFFF, where <i>ZZ</i> is the difference in the alpha values (the alpha value in the source object minus the alpha value in the <code>otherBitmapData</code> object).</li>
		 * </ul>
		 * <p>For example, consider the following two BitmapData objects:</p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *      var bmd1:BitmapData = new BitmapData(50, 50, true, 0xFFFF8800);
		 *      var bmd2:BitmapData = new BitmapData(50, 50, true, 0xCCCC6600);
		 *      var diffBmpData:BitmapData = bmd1.compare(bmd2) as BitmapData;
		 *      trace ("0x" + diffBmpData.getPixel(0,0).toString(16); // 0x332200
		 *      </pre>
		 * </div>
		 * <p><b>Note:</b> The colors used to fill the two BitmapData objects have slightly different RGB values (0xFF0000 and 0xFFAA00). The result of the <code>compare()</code> method is a new BitmapData object with each pixel showing the difference in the RGB values between the two bitmaps.</p>
		 * <p>Consider the following two BitmapData objects, in which the RGB colors are the same, but the alpha values are different:</p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *      var bmd1:BitmapData = new BitmapData(50, 50, true, 0xFFFFAA00);
		 *      var bmd2:BitmapData = new BitmapData(50, 50, true, 0xCCFFAA00);
		 *      var diffBmpData:BitmapData = bmd1.compare(bmd2) as BitmapData;
		 *      trace ("0x" + diffBmpData.getPixel32(0,0).toString(16); // 0x33ffffff
		 *      </pre>
		 * </div>
		 * <p>The result of the <code>compare()</code> method is a new BitmapData object with each pixel showing the difference in the alpha values between the two bitmaps.</p>
		 * <p>If the BitmapData objects are equivalent (with the same width, height, and identical pixel values), the method returns the number 0.</p>
		 * <p>If the widths of the BitmapData objects are not equal, the method returns the number -3. </p>
		 * <p>If the heights of the BitmapData objects are not equal, but the widths are the same, the method returns the number -4.</p>
		 * <p>The following example compares two Bitmap objects with different widths (50 and 60):</p>
		 * <div class="listing">
		 *  <div class="clipcopy">
		 *   <a href="#" class="copyText">Copy</a>
		 *  </div>
		 *  <pre>
		 *      var bmd1:BitmapData = new BitmapData(100, 50, false, 0xFFFF0000);
		 *      var bmd2:BitmapData = new BitmapData(100, 60, false, 0xFFFFAA00);
		 *      trace(bmd1.compare(bmd2)); // -4
		 *      </pre>
		 * </div>
		 * 
		 * @param otherBitmapData  — The BitmapData object to compare with the source BitmapData object. 
		 * @return  — If the two BitmapData objects have the same dimensions (width and height), the method returns a new BitmapData object that has the difference between the two objects (see the main discussion). If the BitmapData objects are equivalent, the method returns the number 0. If the widths of the BitmapData objects are not equal, the method returns the number -3. If the heights of the BitmapData objects are not equal, the method returns the number -4. 
		 */
		public function compare(otherBitmapData:BitmapData):Object {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Transfers data from one channel of another BitmapData object or the current BitmapData object into a channel of the current BitmapData object. All of the data in the other channels in the destination BitmapData object are preserved. </p>
		 * <p>The source channel value and destination channel value can be one of following values: </p>
		 * <ul>
		 *  <li><code>BitmapDataChannel.RED</code></li>
		 *  <li><code>BitmapDataChannel.GREEN</code></li>
		 *  <li><code>BitmapDataChannel.BLUE</code></li>
		 *  <li><code>BitmapDataChannel.ALPHA</code></li>
		 * </ul>
		 * 
		 * @param sourceBitmapData  — The input bitmap image to use. The source image can be a different BitmapData object or it can refer to the current BitmapData object. 
		 * @param sourceRect  — The source Rectangle object. To copy only channel data from a smaller area within the bitmap, specify a source rectangle that is smaller than the overall size of the BitmapData object. 
		 * @param destPoint  — The destination Point object that represents the upper-left corner of the rectangular area where the new channel data is placed. To copy only channel data from one area to a different area in the destination image, specify a point other than (0,0). 
		 * @param sourceChannel  — The source channel. Use a value from the BitmapDataChannel class (<code>BitmapDataChannel.RED</code>, <code>BitmapDataChannel.BLUE</code>, <code>BitmapDataChannel.GREEN</code>, <code>BitmapDataChannel.ALPHA</code>). 
		 * @param destChannel  — The destination channel. Use a value from the BitmapDataChannel class (<code>BitmapDataChannel.RED</code>, <code>BitmapDataChannel.BLUE</code>, <code>BitmapDataChannel.GREEN</code>, <code>BitmapDataChannel.ALPHA</code>). 
		 */
		public function copyChannel(sourceBitmapData:BitmapData, sourceRect:Rectangle, destPoint:Point, sourceChannel:uint, destChannel:uint):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Provides a fast routine to perform pixel manipulation between images with no stretching, rotation, or color effects. This method copies a rectangular area of a source image to a rectangular area of the same size at the destination point of the destination BitmapData object. </p>
		 * <p>If you include the <code>alphaBitmap</code> and <code>alphaPoint</code> parameters, you can use a secondary image as an alpha source for the source image. If the source image has alpha data, both sets of alpha data are used to composite pixels from the source image to the destination image. The <code>alphaPoint</code> parameter is the point in the alpha image that corresponds to the upper-left corner of the source rectangle. Any pixels outside the intersection of the source image and alpha image are not copied to the destination image.</p>
		 * <p>The <code>mergeAlpha</code> property controls whether or not the alpha channel is used when a transparent image is copied onto another transparent image. To copy pixels with the alpha channel data, set the <code>mergeAlpha</code> property to <code>true</code>. By default, the <code>mergeAlpha</code> property is <code>false</code>.</p>
		 * 
		 * @param sourceBitmapData  — The input bitmap image from which to copy pixels. The source image can be a different BitmapData instance, or it can refer to the current BitmapData instance. 
		 * @param sourceRect  — A rectangle that defines the area of the source image to use as input. 
		 * @param destPoint  — The destination point that represents the upper-left corner of the rectangular area where the new pixels are placed. 
		 * @param alphaBitmapData  — A secondary, alpha BitmapData object source. 
		 * @param alphaPoint  — The point in the alpha BitmapData object source that corresponds to the upper-left corner of the <code>sourceRect</code> parameter. 
		 * @param mergeAlpha  — To use the alpha channel, set the value to <code>true</code>. To copy pixels with no alpha channel, set the value to <code>false</code>. 
		 */
		public function copyPixels(sourceBitmapData:BitmapData, sourceRect:Rectangle, destPoint:Point, alphaBitmapData:BitmapData = null, alphaPoint:Point = null, mergeAlpha:Boolean = false):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Fills a byte array from a rectangular region of pixel data. Starting at the <code>position</code> index of the ByteArray, this method writes an unsigned integer (a 32-bit unmultiplied pixel value) for each pixel into the byte array. If necessary, the byte array's size is increased to the necessary number of bytes to hold all the pixel data. </p>
		 * 
		 * @param rect  — A rectangular area in the current BitmapData object 
		 * @param data  — the destination ByteArray object 
		 */
		public function copyPixelsToByteArray(rect:Rectangle, data:ByteArray):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Frees memory that is used to store the BitmapData object. </p>
		 * <p>When the <code>dispose()</code> method is called on an image, the width and height of the image are set to 0. All subsequent calls to methods or properties of this BitmapData instance fail, and an exception is thrown. </p>
		 * <p><code>BitmapData.dispose()</code> releases the memory occupied by the actual bitmap data, immediately (a bitmap can consume up to 64 MB of memory). After using <code>BitmapData.dispose()</code>, the BitmapData object is no longer usable and the Flash runtime throws an exception if you call functions on the BitmapData object. However, <code>BitmapData.dispose()</code> does not garbage collect the BitmapData object (approximately 128 bytes); the memory occupied by the actual BitmapData object is released at the time the BitmapData object is collected by the garbage collector.</p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="../../flash/system/System.html#gc()" target="">flash.system.System.gc()</a>
		 * </div>
		 * <span id="pageFilter"><br><span class="label"> Example &nbsp;( <span class="usage"><a href="http://www.adobe.com/go/learn_as3_usingexamples_en"> How to use this example </a></span>) </span><br><br>
		 *  <div class="detailBody">
		 *    The following example shows the effect of calling a method of a BitmapData object after a call to the 
		 *   <code>dispose()</code> method (an exception is thrown): 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>
		 * import flash.display.BitmapData;
		 * 
		 * var myBitmapData:BitmapData = new BitmapData(100, 80, false, 0x000000FF);
		 * trace(myBitmapData.getPixel(1, 1)); // 255 == 0xFF
		 * 
		 * myBitmapData.dispose();
		 * try {
		 *     trace(myBitmapData.getPixel(1, 1));
		 * } catch (error:Error) {
		 *     trace(error); // ArgumentError
		 * }
		 * </pre>
		 *   </div>
		 *  </div></span>
		 */
		public function dispose():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Draws the <code>source</code> display object onto the bitmap image, using the Flash runtime vector renderer. You can specify <code>matrix</code>, <code>colorTransform</code>, <code>blendMode</code>, and a destination <code>clipRect</code> parameter to control how the rendering performs. Optionally, you can specify whether the bitmap should be smoothed when scaled (this works only if the source object is a BitmapData object). </p>
		 * <p><b>Note: </b>The <code>drawWithQuality()</code> method works exactly like the <code>draw()</code> method, but instead of using the <code>Stage.quality</code> property to determine the quality of vector rendering, you specify the <code>quality</code> parameter to the <code>drawWithQuality()</code> method.</p>
		 * <p>This method directly corresponds to how objects are drawn with the standard vector renderer for objects in the authoring tool interface.</p>
		 * <p>The source display object does not use any of its applied transformations for this call. It is treated as it exists in the library or file, with no matrix transform, no color transform, and no blend mode. To draw a display object (such as a movie clip) by using its own transform properties, you can copy its <code>transform</code> property object to the <code>transform</code> property of the Bitmap object that uses the BitmapData object.</p>
		 * <p>This method is supported over RTMP in <span>Flash Player 9.0.115.0 and later and in</span> Adobe AIR. You can control access to streams on Flash Media Server in a server-side script. For more information, see the <code>Client.audioSampleAccess</code> and <code>Client.videoSampleAccess</code> properties in <a href="http://www.adobe.com/go/learn_flash_ss_as_en" target="external"> <i>Server-Side ActionScript Language Reference for Adobe Flash Media Server</i></a>.</p>
		 * <p>If the source object and (in the case of a Sprite or MovieClip object) all of its child objects do not come from the same domain as the caller, or are not in a content that is accessible to the caller by having called the <code>Security.allowDomain()</code> method, a call to the <code>draw()</code> throws a SecurityError exception. This restriction does not apply to AIR content in the application security sandbox.</p>
		 * <p>There are also restrictions on using a loaded bitmap image as the <code>source</code>. A call to the <code>draw()</code> method is successful if the loaded image comes from the same domain as the caller. Also, a cross-domain policy file on the image's server can grant permission to the domain of the SWF content calling the <code>draw()</code> method. In this case, you must set the <code>checkPolicyFile</code> property of a LoaderContext object, and use this object as the <code>context</code> parameter when calling the <code>load()</code> method of the Loader object used to load the image. These restrictions do not apply to AIR content in the application security sandbox.</p>
		 * <p>On Windows, the <code>draw()</code> method cannot capture SWF content embedded in an HTML page<span> in an HTMLLoader object in Adobe AIR</span>.</p>
		 * <p>The <code>draw()</code> method cannot capture PDF content<span> in Adobe AIR</span>. Nor can it capture or SWF content embedded in HTML in which the <code>wmode</code> attribute is set to <code>"window"</code><span> in Adobe AIR</span>.</p>
		 * 
		 * @param source  — The display object or BitmapData object to draw to the BitmapData object. (The DisplayObject and BitmapData classes implement the IBitmapDrawable interface.) 
		 * @param matrix  — A Matrix object used to scale, rotate, or translate the coordinates of the bitmap. If you do not want to apply a matrix transformation to the image, set this parameter to an identity matrix, created with the default <code>new Matrix()</code> constructor, or pass a <code>null</code> value. 
		 * @param colorTransform  — A ColorTransform object that you use to adjust the color values of the bitmap. If no object is supplied, the bitmap image's colors are not transformed. If you must pass this parameter but you do not want to transform the image, set this parameter to a ColorTransform object created with the default <code>new ColorTransform()</code> constructor. 
		 * @param blendMode  — A string value, from the flash.display.BlendMode class, specifying the blend mode to be applied to the resulting bitmap. 
		 * @param clipRect  — A Rectangle object that defines the area of the source object to draw. If you do not supply this value, no clipping occurs and the entire source object is drawn. 
		 * @param smoothing  — A Boolean value that determines whether a BitmapData object is smoothed when scaled or rotated, due to a scaling or rotation in the <code>matrix</code> parameter. The <code>smoothing</code> parameter only applies if the <code>source</code> parameter is a BitmapData object. With <code>smoothing</code> set to <code>false</code>, the rotated or scaled BitmapData image can appear pixelated or jagged. For example, the following two images use the same BitmapData object for the <code>source</code> parameter, but the <code>smoothing</code> parameter is set to <code>true</code> on the left and <code>false</code> on the right: <p><img src="../../images/bitmapData_draw_smoothing.jpg" alt="Two images: the left one with smoothing and the right one without smoothing."></p> <p>Drawing a bitmap with <code>smoothing</code> set to <code>true</code> takes longer than doing so with <code>smoothing</code> set to <code>false</code>.</p> 
		 */
		public function draw(source:IBitmapDrawable, matrix:Matrix = null, colorTransform:ColorTransform = null, blendMode:String = null, clipRect:Rectangle = null, smoothing:Boolean = false):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Draws the <code>source</code> display object onto the bitmap image, using the Flash runtime vector renderer. You can specify <code>matrix</code>, <code>colorTransform</code>, <code>blendMode</code>, and a destination <code>clipRect</code> parameter to control how the rendering performs. Optionally, you can specify whether the bitmap should be smoothed when scaled (this works only if the source object is a BitmapData object). </p>
		 * <p><b>Note: </b>The <code>drawWithQuality()</code> method works exactly like the <code>draw()</code> method, but instead of using the <code>Stage.quality</code> property to determine the quality of vector rendering, you specify the <code>quality</code> parameter to the <code>drawWithQuality()</code> method.</p>
		 * <p>This method directly corresponds to how objects are drawn with the standard vector renderer for objects in the authoring tool interface.</p>
		 * <p>The source display object does not use any of its applied transformations for this call. It is treated as it exists in the library or file, with no matrix transform, no color transform, and no blend mode. To draw a display object (such as a movie clip) by using its own transform properties, you can copy its <code>transform</code> property object to the <code>transform</code> property of the Bitmap object that uses the BitmapData object.</p>
		 * <p>This method is supported over RTMP in <span>Flash Player 9.0.115.0 and later and in</span> Adobe AIR. You can control access to streams on Flash Media Server in a server-side script. For more information, see the <code>Client.audioSampleAccess</code> and <code>Client.videoSampleAccess</code> properties in <a href="http://www.adobe.com/go/documentation" target="external"> <i>Server-Side ActionScript Language Reference for Adobe Flash Media Server</i></a>.</p>
		 * <p>If the source object and (in the case of a Sprite or MovieClip object) all of its child objects do not come from the same domain as the caller, or are not in a content that is accessible to the caller by having called the <code>Security.allowDomain()</code> method, a call to the <code>drawWithQuality()</code> throws a SecurityError exception. This restriction does not apply to AIR content in the application security sandbox.</p>
		 * <p>There are also restrictions on using a loaded bitmap image as the <code>source</code>. A call to the <code>drawWithQuality()</code> method is successful if the loaded image comes from the same domain as the caller. Also, a cross-domain policy file on the image's server can grant permission to the domain of the SWF content calling the <code>drawWithQuality()</code> method. In this case, you must set the <code>checkPolicyFile</code> property of a LoaderContext object, and use this object as the <code>context</code> parameter when calling the <code>load()</code> method of the Loader object used to load the image. These restrictions do not apply to AIR content in the application security sandbox.</p>
		 * <p>On Windows, the <code>drawWithQuality()</code> method cannot capture SWF content embedded in an HTML page<span> in an HTMLLoader object in Adobe AIR</span>.</p>
		 * <p>The <code>drawWithQuality()</code> method cannot capture PDF content<span> in Adobe AIR</span>. Nor can it capture or SWF content embedded in HTML in which the <code>wmode</code> attribute is set to <code>"window"</code><span> in Adobe AIR</span>.</p>
		 * 
		 * @param source  — The display object or BitmapData object to draw to the BitmapData object. (The DisplayObject and BitmapData classes implement the IBitmapDrawable interface.) 
		 * @param matrix  — A Matrix object used to scale, rotate, or translate the coordinates of the bitmap. If you do not want to apply a matrix transformation to the image, set this parameter to an identity matrix, created with the default <code>new Matrix()</code> constructor, or pass a <code>null</code> value. 
		 * @param colorTransform  — A ColorTransform object that you use to adjust the color values of the bitmap. If no object is supplied, the bitmap image's colors are not transformed. If you must pass this parameter but you do not want to transform the image, set this parameter to a ColorTransform object created with the default <code>new ColorTransform()</code> constructor. 
		 * @param blendMode  — A string value, from the flash.display.BlendMode class, specifying the blend mode to be applied to the resulting bitmap. 
		 * @param clipRect  — A Rectangle object that defines the area of the source object to draw. If you do not supply this value, no clipping occurs and the entire source object is drawn. 
		 * @param smoothing  — A Boolean value that determines whether a BitmapData object is smoothed when scaled or rotated, due to a scaling or rotation in the <code>matrix</code> parameter. The <code>smoothing</code> parameter only applies if the <code>source</code> parameter is a BitmapData object. With <code>smoothing</code> set to <code>false</code>, the rotated or scaled BitmapData image can appear pixelated or jagged. For example, the following two images use the same BitmapData object for the <code>source</code> parameter, but the <code>smoothing</code> parameter is set to <code>true</code> on the left and <code>false</code> on the right: <p><img src="../../images/bitmapData_draw_smoothing.jpg" alt="Two images: the left one with smoothing and the right one without smoothing."></p> <p>Drawing a bitmap with <code>smoothing</code> set to <code>true</code> takes longer than doing so with <code>smoothing</code> set to <code>false</code>.</p> 
		 * @param quality  — Any of one of the StageQuality values. Selects the antialiasing quality to be used when drawing vectors graphics. 
		 */
		public function drawWithQuality(source:IBitmapDrawable, matrix:Matrix = null, colorTransform:ColorTransform = null, blendMode:String = null, clipRect:Rectangle = null, smoothing:Boolean = false, quality:String = null):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Compresses this BitmapData object using the selected compressor algorithm and returns a new ByteArray object. Optionally, writes the resulting data to the specified ByteArray. The <code>compressor</code> argument specifies the encoding algorithm, and can be PNGEncoderOptions, JPEGEncoderOptions, or JPEGXREncoderOptions. </p>
		 * <p>The following example compresses a BitmapData object using the JPEGEncoderOptions:</p>
		 * <pre>
		 *      // Compress a BitmapData object as a JPEG file.
		 *      var bitmapData:BitmapData = new BitmapData(640,480,false,0x00FF00);
		 *      var byteArray:ByteArray = new ByteArray();
		 *      bitmapData.encode(new Rectangle(0,0,640,480), new flash.display.JPEGEncoderOptions(), byteArray); </pre>
		 * 
		 * @param rect  — The area of the BitmapData object to compress. 
		 * @param compressor  — The compressor type to use. Valid values are: <code>flash.display.PNGEncoderOptions</code>, <code>flash.display.JPEGEncoderOptions</code>, and <code>flash.display.JPEGXREncoderOptions</code>. 
		 * @param byteArray  — The output ByteArray to hold the encoded image. 
		 * @return  — A ByteArray containing the encoded image. 
		 */
		public function encode(rect:Rectangle, compressor:Object, byteArray:ByteArray = null):ByteArray {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Fills a rectangular area of pixels with a specified ARGB color. </p>
		 * 
		 * @param rect  — The rectangular area to fill. 
		 * @param color  — The ARGB color value that fills the area. ARGB colors are often specified in hexadecimal format; for example, 0xFF336699. 
		 */
		public function fillRect(rect:Rectangle, color:uint):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Performs a flood fill operation on an image starting at an (<i>x</i>, <i>y</i>) coordinate and filling with a certain color. The <code>floodFill()</code> method is similar to the paint bucket tool in various paint programs. The color is an ARGB color that contains alpha information and color information. </p>
		 * 
		 * @param x  — The <i>x</i> coordinate of the image. 
		 * @param y  — The <i>y</i> coordinate of the image. 
		 * @param color  — The ARGB color to use as a fill. 
		 */
		public function floodFill(x:int, y:int, color:uint):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Determines the destination rectangle that the <code>applyFilter()</code> method call affects, given a BitmapData object, a source rectangle, and a filter object. </p>
		 * <p>For example, a blur filter normally affects an area larger than the size of the original image. A 100 x 200 pixel image that is being filtered by a default BlurFilter instance, where <code>blurX = blurY = 4</code> generates a destination rectangle of <code>(-2,-2,104,204)</code>. The <code>generateFilterRect()</code> method lets you find out the size of this destination rectangle in advance so that you can size the destination image appropriately before you perform a filter operation.</p>
		 * <p>Some filters clip their destination rectangle based on the source image size. For example, an inner <code>DropShadow</code> does not generate a larger result than its source image. In this API, the BitmapData object is used as the source bounds and not the source <code>rect</code> parameter.</p>
		 * 
		 * @param sourceRect  — A rectangle defining the area of the source image to use as input. 
		 * @param filter  — A filter object that you use to calculate the destination rectangle. 
		 * @return  — A destination rectangle computed by using an image, the  parameter, and a filter. 
		 */
		public function generateFilterRect(sourceRect:Rectangle, filter:BitmapFilter):Rectangle {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Determines a rectangular region that either fully encloses all pixels of a specified color within the bitmap image (if the <code>findColor</code> parameter is set to <code>true</code>) or fully encloses all pixels that do not include the specified color (if the <code>findColor</code> parameter is set to <code>false</code>). </p>
		 * <p>For example, if you have a source image and you want to determine the rectangle of the image that contains a nonzero alpha channel, pass <code>{mask: 0xFF000000, color: 0x00000000}</code> as parameters. If the <code>findColor</code> parameter is set to <code>true</code>, the entire image is searched for the bounds of pixels for which <code>(value &amp; mask) == color</code> (where <code>value</code> is the color value of the pixel). If the <code>findColor</code> parameter is set to <code>false</code>, the entire image is searched for the bounds of pixels for which <code>(value &amp; mask) != color</code> (where <code>value</code> is the color value of the pixel). To determine white space around an image, pass <code>{mask: 0xFFFFFFFF, color: 0xFFFFFFFF}</code> to find the bounds of nonwhite pixels.</p>
		 * 
		 * @param mask  — A hexadecimal value, specifying the bits of the ARGB color to consider. The color value is combined with this hexadecimal value, by using the <code>&amp;</code> (bitwise AND) operator. 
		 * @param color  — A hexadecimal value, specifying the ARGB color to match (if <code>findColor</code> is set to <code>true</code>) or <i>not</i> to match (if <code>findColor</code> is set to <code>false</code>). 
		 * @param findColor  — If the value is set to <code>true</code>, returns the bounds of a color value in an image. If the value is set to <code>false</code>, returns the bounds of where this color doesn't exist in an image. 
		 * @return  — The region of the image that is the specified color. 
		 */
		public function getColorBoundsRect(mask:uint, color:uint, findColor:Boolean = true):Rectangle {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns an integer that represents an RGB pixel value from a BitmapData object at a specific point (<i>x</i>, <i>y</i>). The <code>getPixel()</code> method returns an unmultiplied pixel value. No alpha information is returned. </p>
		 * <p>All pixels in a BitmapData object are stored as premultiplied color values. A premultiplied image pixel has the red, green, and blue color channel values already multiplied by the alpha data. For example, if the alpha value is 0, the values for the RGB channels are also 0, independent of their unmultiplied values. This loss of data can cause some problems when you perform operations. All BitmapData methods take and return unmultiplied values. The internal pixel representation is converted from premultiplied to unmultiplied before it is returned as a value. During a set operation, the pixel value is premultiplied before the raw image pixel is set.</p>
		 * 
		 * @param x  — The <i>x</i> position of the pixel. 
		 * @param y  — The <i>y</i> position of the pixel. 
		 * @return  — A number that represents an RGB pixel value. If the (<i>x</i>, <i>y</i>) coordinates are outside the bounds of the image, the method returns 0. 
		 */
		public function getPixel(x:int, y:int):uint {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns an ARGB color value that contains alpha channel data and RGB data. This method is similar to the <code>getPixel()</code> method, which returns an RGB color without alpha channel data. </p>
		 * <p>All pixels in a BitmapData object are stored as premultiplied color values. A premultiplied image pixel has the red, green, and blue color channel values already multiplied by the alpha data. For example, if the alpha value is 0, the values for the RGB channels are also 0, independent of their unmultiplied values. This loss of data can cause some problems when you perform operations. All BitmapData methods take and return unmultiplied values. The internal pixel representation is converted from premultiplied to unmultiplied before it is returned as a value. During a set operation, the pixel value is premultiplied before the raw image pixel is set.</p>
		 * 
		 * @param x  — The <i>x</i> position of the pixel. 
		 * @param y  — The <i>y</i> position of the pixel. 
		 * @return  — A number representing an ARGB pixel value. If the (<i>x</i>, <i>y</i>) coordinates are outside the bounds of the image, 0 is returned. 
		 */
		public function getPixel32(x:int, y:int):uint {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Generates a byte array from a rectangular region of pixel data. Writes an unsigned integer (a 32-bit unmultiplied pixel value) for each pixel into the byte array. </p>
		 * 
		 * @param rect  — A rectangular area in the current BitmapData object. 
		 * @return  — A ByteArray representing the pixels in the given Rectangle. 
		 */
		public function getPixels(rect:Rectangle):ByteArray {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Generates a vector array from a rectangular region of pixel data. Returns a Vector object of unsigned integers (a 32-bit unmultiplied pixel value) for the specified rectangle. </p>
		 * 
		 * @param rect  — A rectangular area in the current BitmapData object. 
		 * @return  — A Vector representing the given Rectangle. 
		 */
		public function getVector(rect:Rectangle):Vector.<uint> {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Computes a 256-value binary number histogram of a BitmapData object. This method returns a Vector object containing four Vector.&lt;Number&gt; instances (four Vector objects that contain Number objects). The four Vector instances represent the red, green, blue and alpha components in order. Each Vector instance contains 256 values that represent the population count of an individual component value, from 0 to 255. </p>
		 * 
		 * @param hRect  — The area of the BitmapData object to use. 
		 * @return 
		 */
		public function histogram(hRect:Rectangle = null):Vector.<Vector.<Number>> {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Performs pixel-level hit detection between one bitmap image and a point, rectangle, or other bitmap image. A hit is defined as an overlap of a point or rectangle over an opaque pixel, or two overlapping opaque pixels. No stretching, rotation, or other transformation of either object is considered when the hit test is performed. </p>
		 * <p>If an image is an opaque image, it is considered a fully opaque rectangle for this method. Both images must be transparent images to perform pixel-level hit testing that considers transparency. When you are testing two transparent images, the alpha threshold parameters control what alpha channel values, from 0 to 255, are considered opaque.</p>
		 * 
		 * @param firstPoint  — A position of the upper-left corner of the BitmapData image in an arbitrary coordinate space. The same coordinate space is used in defining the <code>secondBitmapPoint</code> parameter. 
		 * @param firstAlphaThreshold  — The smallest alpha channel value that is considered opaque for this hit test. 
		 * @param secondObject  — A Rectangle, Point, Bitmap, or BitmapData object. 
		 * @param secondBitmapDataPoint  — A point that defines a pixel location in the second BitmapData object. Use this parameter only when the value of <code>secondObject</code> is a BitmapData object. 
		 * @param secondAlphaThreshold  — The smallest alpha channel value that is considered opaque in the second BitmapData object. Use this parameter only when the value of <code>secondObject</code> is a BitmapData object and both BitmapData objects are transparent. 
		 * @return  — A value of  if a hit occurs; otherwise, . 
		 */
		public function hitTest(firstPoint:Point, firstAlphaThreshold:uint, secondObject:Object, secondBitmapDataPoint:Point = null, secondAlphaThreshold:uint = 1):Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Locks an image so that any objects that reference the BitmapData object, such as Bitmap objects, are not updated when this BitmapData object changes. To improve performance, use this method along with the <code>unlock()</code> method before and after numerous calls to the <code>setPixel()</code> or <code>setPixel32()</code> method. </p>
		 * <p><span class="label">Related API Elements</span></p>
		 * <div class="seeAlso">
		 *  <a href="BitmapData.html#setPixel()" target="">setPixel()</a>
		 *  <br>
		 *  <a href="BitmapData.html#setPixel32()" target="">setPixel32()</a>
		 *  <br>
		 *  <a href="BitmapData.html#unlock()" target="">unlock()</a>
		 * </div>
		 * <span id="pageFilter"><br><span class="label"> Example &nbsp;( <span class="usage"><a href="http://www.adobe.com/go/learn_as3_usingexamples_en"> How to use this example </a></span>) </span><br><br>
		 *  <div class="detailBody">
		 *    The following example creates a BitmapData object based on the 
		 *   <code>bitmapData</code> property of a Bitmap object, 
		 *   <code>picture</code>. It then calls the 
		 *   <code>lock()</code> method before calling a complicated custom function, 
		 *   <code>complexTransformation()</code>, that modifies the BitmapData object. (The 
		 *   <code>picture</code> object and the 
		 *   <code>complexTransformation()</code> function are not defined in this example.) Even if the 
		 *   <code>complexTransformation()</code> function updates the 
		 *   <code>bitmapData</code> property of the 
		 *   <code>picture</code> object, changes are not reflected until the code calls the 
		 *   <code>unlock()</code> method on the 
		 *   <code>bitmapData</code> object: 
		 *   <div class="listing">
		 *    <div class="clipcopy">
		 *     <a href="#" class="copyText">Copy</a>
		 *    </div>
		 *    <pre>
		 * import flash.display.BitmapData;
		 * 
		 * var bitmapData:BitmapData = picture.bitmapData;
		 * bitmapData.lock();
		 * bitmapData = complexTransformation(bitmapData);
		 * bitmapData.unlock();
		 * picture.bitmapData = bitmapData;
		 * </pre>
		 *   </div>
		 *  </div></span>
		 */
		public function lock():void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Performs per-channel blending from a source image to a destination image. For each channel and each pixel, a new value is computed based on the channel values of the source and destination pixels. For example, in the red channel, the new value is computed as follows (where <code>redSrc</code> is the red channel value for a pixel in the source image and <code>redDest</code> is the red channel value at the corresponding pixel of the destination image): </p>
		 * <p> <code> new redDest = [(redSrc * redMultiplier) + (redDest * (256 - redMultiplier))] / 256; </code> </p>
		 * <p>The <code>redMultiplier</code>, <code>greenMultiplier</code>, <code>blueMultiplier</code>, and <code>alphaMultiplier</code> values are the multipliers used for each color channel. Use a hexadecimal value ranging from <code>0</code> to <code>0x100</code> (256) where <code>0</code> specifies the full value from the destination is used in the result, <code>0x100</code> specifies the full value from the source is used, and numbers in between specify a blend is used (such as <code>0x80</code> for 50%).</p>
		 * 
		 * @param sourceBitmapData  — The input bitmap image to use. The source image can be a different BitmapData object, or it can refer to the current BitmapData object. 
		 * @param sourceRect  — A rectangle that defines the area of the source image to use as input. 
		 * @param destPoint  — The point within the destination image (the current BitmapData instance) that corresponds to the upper-left corner of the source rectangle. 
		 * @param redMultiplier  — A hexadecimal uint value by which to multiply the red channel value. 
		 * @param greenMultiplier  — A hexadecimal uint value by which to multiply the green channel value. 
		 * @param blueMultiplier  — A hexadecimal uint value by which to multiply the blue channel value. 
		 * @param alphaMultiplier  — A hexadecimal uint value by which to multiply the alpha transparency value. 
		 */
		public function merge(sourceBitmapData:BitmapData, sourceRect:Rectangle, destPoint:Point, redMultiplier:uint, greenMultiplier:uint, blueMultiplier:uint, alphaMultiplier:uint):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Fills an image with pixels representing random noise. </p>
		 * 
		 * @param randomSeed  — The random seed number to use. If you keep all other parameters the same, you can generate different pseudo-random results by varying the random seed value. The noise function is a mapping function, not a true random-number generation function, so it creates the same results each time from the same random seed. 
		 * @param low  — The lowest value to generate for each channel (0 to 255). 
		 * @param high  — The highest value to generate for each channel (0 to 255). 
		 * @param channelOptions  — A number that can be a combination of any of the four color channel values (<code>BitmapDataChannel.RED</code>, <code>BitmapDataChannel.BLUE</code>, <code>BitmapDataChannel.GREEN</code>, and <code>BitmapDataChannel.ALPHA</code>). You can use the logical OR operator (<code>|</code>) to combine channel values. 
		 * @param grayScale  — A Boolean value. If the value is <code>true</code>, a grayscale image is created by setting all of the color channels to the same value. The alpha channel selection is not affected by setting this parameter to <code>true</code>. 
		 */
		public function noise(randomSeed:int, low:uint = 0, high:uint = 255, channelOptions:uint = 7, grayScale:Boolean = false):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Remaps the color channel values in an image that has up to four arrays of color palette data, one for each channel. </p>
		 * <p>Flash runtimes use the following steps to generate the resulting image:</p>
		 * <ol>
		 *  <li>After the red, green, blue, and alpha values are computed, they are added together using standard 32-bit-integer arithmetic. </li>
		 *  <li>The red, green, blue, and alpha channel values of each pixel are extracted into separate 0 to 255 values. These values are used to look up new color values in the appropriate array: <code>redArray</code>, <code>greenArray</code>, <code>blueArray</code>, and <code>alphaArray</code>. Each of these four arrays should contain 256 values. </li>
		 *  <li>After all four of the new channel values are retrieved, they are combined into a standard ARGB value that is applied to the pixel.</li>
		 * </ol>
		 * <p>Cross-channel effects can be supported with this method. Each input array can contain full 32-bit values, and no shifting occurs when the values are added together. This routine does not support per-channel clamping. </p>
		 * <p>If no array is specified for a channel, the color channel is copied from the source image to the destination image.</p>
		 * <p>You can use this method for a variety of effects such as general palette mapping (taking one channel and converting it to a false color image). You can also use this method for a variety of advanced color manipulation algorithms, such as gamma, curves, levels, and quantizing.</p>
		 * 
		 * @param sourceBitmapData  — The input bitmap image to use. The source image can be a different BitmapData object, or it can refer to the current BitmapData instance. 
		 * @param sourceRect  — A rectangle that defines the area of the source image to use as input. 
		 * @param destPoint  — The point within the destination image (the current BitmapData object) that corresponds to the upper-left corner of the source rectangle. 
		 * @param redArray  — If <code>redArray</code> is not <code>null</code>, <code>red = redArray[source red value] else red = source rect value</code>. 
		 * @param greenArray  — If <code>greenArray</code> is not <code>null</code>, <code>green = greenArray[source green value] else green = source green value.</code> 
		 * @param blueArray  — If <code>blueArray</code> is not <code>null</code>, <code>blue = blueArray[source blue value] else blue = source blue value</code>. 
		 * @param alphaArray  — If <code>alphaArray</code> is not <code>null</code>, <code>alpha = alphaArray[source alpha value] else alpha = source alpha value</code>. 
		 */
		public function paletteMap(sourceBitmapData:BitmapData, sourceRect:Rectangle, destPoint:Point, redArray:Array = null, greenArray:Array = null, blueArray:Array = null, alphaArray:Array = null):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Generates a Perlin noise image. </p>
		 * <p>The Perlin noise generation algorithm interpolates and combines individual random noise functions (called octaves) into a single function that generates more natural-seeming random noise. Like musical octaves, each octave function is twice the frequency of the one before it. Perlin noise has been described as a "fractal sum of noise" because it combines multiple sets of noise data with different levels of detail.</p>
		 * <p>You can use Perlin noise functions to simulate natural phenomena and landscapes, such as wood grain, clouds, and mountain ranges. In most cases, the output of a Perlin noise function is not displayed directly but is used to enhance other images and give them pseudo-random variations.</p>
		 * <p>Simple digital random noise functions often produce images with harsh, contrasting points. This kind of harsh contrast is not often found in nature. The Perlin noise algorithm blends multiple noise functions that operate at different levels of detail. This algorithm results in smaller variations among neighboring pixel values.</p>
		 * 
		 * @param baseX  — Frequency to use in the <i>x</i> direction. For example, to generate a noise that is sized for a 64 x 128 image, pass 64 for the <code>baseX</code> value. 
		 * @param baseY  — Frequency to use in the <i>y</i> direction. For example, to generate a noise that is sized for a 64 x 128 image, pass 128 for the <code>baseY</code> value. 
		 * @param numOctaves  — Number of octaves or individual noise functions to combine to create this noise. Larger numbers of octaves create images with greater detail. Larger numbers of octaves also require more processing time. 
		 * @param randomSeed  — The random seed number to use. If you keep all other parameters the same, you can generate different pseudo-random results by varying the random seed value. The Perlin noise function is a mapping function, not a true random-number generation function, so it creates the same results each time from the same random seed. 
		 * @param stitch  — A Boolean value. If the value is <code>true</code>, the method attempts to smooth the transition edges of the image to create seamless textures for tiling as a bitmap fill. 
		 * @param fractalNoise  — A Boolean value. If the value is <code>true</code>, the method generates fractal noise; otherwise, it generates turbulence. An image with turbulence has visible discontinuities in the gradient that can make it better approximate sharper visual effects like flames and ocean waves. 
		 * @param channelOptions  — A number that can be a combination of any of the four color channel values (<code>BitmapDataChannel.RED</code>, <code>BitmapDataChannel.BLUE</code>, <code>BitmapDataChannel.GREEN</code>, and <code>BitmapDataChannel.ALPHA</code>). You can use the logical OR operator (<code>|</code>) to combine channel values. 
		 * @param grayScale  — A Boolean value. If the value is <code>true</code>, a grayscale image is created by setting each of the red, green, and blue color channels to identical values. The alpha channel value is not affected if this value is set to <code>true</code>. 
		 * @param offsets  — An array of points that correspond to <i>x</i> and <i>y</i> offsets for each octave. By manipulating the offset values you can smoothly scroll the layers of a perlinNoise image. Each point in the offset array affects a specific octave noise function. 
		 */
		public function perlinNoise(baseX:Number, baseY:Number, numOctaves:uint, randomSeed:int, stitch:Boolean, fractalNoise:Boolean, channelOptions:uint = 7, grayScale:Boolean = false, offsets:Array = null):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Performs a pixel dissolve either from a source image to a destination image or by using the same image. Flash runtimes use a <code>randomSeed</code> value to generate a random pixel dissolve. The return value of the function must be passed in on subsequent calls to continue the pixel dissolve until it is finished. </p>
		 * <p>If the source image does not equal the destination image, pixels are copied from the source to the destination by using all of the properties. This process allows dissolving from a blank image into a fully populated image.</p>
		 * <p>If the source and destination images are equal, pixels are filled with the <code>color</code> parameter. This process allows dissolving away from a fully populated image. In this mode, the destination <code>point</code> parameter is ignored.</p>
		 * 
		 * @param sourceBitmapData  — The input bitmap image to use. The source image can be a different BitmapData object, or it can refer to the current BitmapData instance. 
		 * @param sourceRect  — A rectangle that defines the area of the source image to use as input. 
		 * @param destPoint  — The point within the destination image (the current BitmapData instance) that corresponds to the upper-left corner of the source rectangle. 
		 * @param randomSeed  — The random seed to use to start the pixel dissolve. 
		 * @param numPixels  — The default is 1/30 of the source area (width x height). 
		 * @param fillColor  — An ARGB color value that you use to fill pixels whose source value equals its destination value. 
		 * @return  — The new random seed value to use for subsequent calls. 
		 */
		public function pixelDissolve(sourceBitmapData:BitmapData, sourceRect:Rectangle, destPoint:Point, randomSeed:int = 0, numPixels:int = 0, fillColor:uint = 0):int {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Scrolls an image by a certain (<i>x</i>, <i>y</i>) pixel amount. Edge regions outside the scrolling area are left unchanged. </p>
		 * 
		 * @param x  — The amount by which to scroll horizontally. 
		 * @param y  — The amount by which to scroll vertically. 
		 */
		public function scroll(x:int, y:int):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets a single pixel of a BitmapData object. The current alpha channel value of the image pixel is preserved during this operation. The value of the RGB color parameter is treated as an unmultiplied color value. </p>
		 * <p><b>Note:</b> To increase performance, when you use the <code>setPixel()</code> or <code>setPixel32()</code> method repeatedly, call the <code>lock()</code> method before you call the <code>setPixel()</code> or <code>setPixel32()</code> method, and then call the <code>unlock()</code> method when you have made all pixel changes. This process prevents objects that reference this BitmapData instance from updating until you finish making the pixel changes.</p>
		 * 
		 * @param x  — The <i>x</i> position of the pixel whose value changes. 
		 * @param y  — The <i>y</i> position of the pixel whose value changes. 
		 * @param color  — The resulting RGB color for the pixel. 
		 */
		public function setPixel(x:int, y:int, color:uint):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Sets the color and alpha transparency values of a single pixel of a BitmapData object. This method is similar to the <code>setPixel()</code> method; the main difference is that the <code>setPixel32()</code> method takes an ARGB color value that contains alpha channel information. </p>
		 * <p>All pixels in a BitmapData object are stored as premultiplied color values. A premultiplied image pixel has the red, green, and blue color channel values already multiplied by the alpha data. For example, if the alpha value is 0, the values for the RGB channels are also 0, independent of their unmultiplied values. This loss of data can cause some problems when you perform operations. All BitmapData methods take and return unmultiplied values. The internal pixel representation is converted from premultiplied to unmultiplied before it is returned as a value. During a set operation, the pixel value is premultiplied before the raw image pixel is set.</p>
		 * <p><b>Note:</b> To increase performance, when you use the <code>setPixel()</code> or <code>setPixel32()</code> method repeatedly, call the <code>lock()</code> method before you call the <code>setPixel()</code> or <code>setPixel32()</code> method, and then call the <code>unlock()</code> method when you have made all pixel changes. This process prevents objects that reference this BitmapData instance from updating until you finish making the pixel changes.</p>
		 * 
		 * @param x  — The <i>x</i> position of the pixel whose value changes. 
		 * @param y  — The <i>y</i> position of the pixel whose value changes. 
		 * @param color  — The resulting ARGB color for the pixel. If the bitmap is opaque (not transparent), the alpha transparency portion of this color value is ignored. 
		 */
		public function setPixel32(x:int, y:int, color:uint):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Converts a byte array into a rectangular region of pixel data. For each pixel, the <code>ByteArray.readUnsignedInt()</code> method is called and the return value is written into the pixel. If the byte array ends before the full rectangle is written, the function returns. The data in the byte array is expected to be 32-bit ARGB pixel values. No seeking is performed on the byte array before or after the pixels are read. </p>
		 * 
		 * @param rect  — Specifies the rectangular region of the BitmapData object. 
		 * @param inputByteArray  — A ByteArray object that consists of 32-bit unmultiplied pixel values to be used in the rectangular region. 
		 */
		public function setPixels(rect:Rectangle, inputByteArray:ByteArray):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Converts a Vector into a rectangular region of pixel data. For each pixel, a Vector element is read and written into the BitmapData pixel. The data in the Vector is expected to be 32-bit ARGB pixel values. </p>
		 * 
		 * @param rect  — Specifies the rectangular region of the BitmapData object. 
		 * @param inputVector  — A Vector object that consists of 32-bit unmultiplied pixel values to be used in the rectangular region. 
		 */
		public function setVector(rect:Rectangle, inputVector:Vector.<uint>):void {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Tests pixel values in an image against a specified threshold and sets pixels that pass the test to new color values. Using the <code>threshold()</code> method, you can isolate and replace color ranges in an image and perform other logical operations on image pixels. </p>
		 * <p>The <code>threshold()</code> method's test logic is as follows:</p>
		 * <ol>
		 *  <li>If <code>((pixelValue &amp; mask) operation (threshold &amp; mask))</code>, then set the pixel to <code>color</code>;</li>
		 *  <li>Otherwise, if <code>copySource == true</code>, then set the pixel to corresponding pixel value from <code>sourceBitmap</code>.</li>
		 * </ol>
		 * <p>The <code>operation</code> parameter specifies the comparison operator to use for the threshold test. For example, by using "==" as the <code>operation</code> parameter, you can isolate a specific color value in an image. Or by using <code>{operation: "&lt;", mask: 0xFF000000, threshold: 0x7F000000, color: 0x00000000}</code>, you can set all destination pixels to be fully transparent when the source image pixel's alpha is less than 0x7F. You can use this technique for animated transitions and other effects.</p>
		 * 
		 * @param sourceBitmapData  — The input bitmap image to use. The source image can be a different BitmapData object or it can refer to the current BitmapData instance. 
		 * @param sourceRect  — A rectangle that defines the area of the source image to use as input. 
		 * @param destPoint  — The point within the destination image (the current BitmapData instance) that corresponds to the upper-left corner of the source rectangle. 
		 * @param operation  — One of the following comparison operators, passed as a String: "&lt;", "&lt;=", "&gt;", "&gt;=", "==", "!=" 
		 * @param threshold  — The value that each pixel is tested against to see if it meets or exceeds the threshhold. 
		 * @param color  — The color value that a pixel is set to if the threshold test succeeds. The default value is 0x00000000. 
		 * @param mask  — The mask to use to isolate a color component. 
		 * @param copySource  — If the value is <code>true</code>, pixel values from the source image are copied to the destination when the threshold test fails. If the value is <code>false</code>, the source image is not copied when the threshold test fails. 
		 * @return  — The number of pixels that were changed. 
		 */
		public function threshold(sourceBitmapData:BitmapData, sourceRect:Rectangle, destPoint:Point, operation:String, threshold:uint, color:uint = 0, mask:uint = 0xFFFFFFFF, copySource:Boolean = false):uint {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Unlocks an image so that any objects that reference the BitmapData object, such as Bitmap objects, are updated when this BitmapData object changes. To improve performance, use this method along with the <code>lock()</code> method before and after numerous calls to the <code>setPixel()</code> or <code>setPixel32()</code> method. </p>
		 * 
		 * @param changeRect  — The area of the BitmapData object that has changed. If you do not specify a value for this parameter, the entire area of the BitmapData object is considered changed. This parameter requires Flash Player version 9.0.115.0 or later. 
		 */
		public function unlock(changeRect:Rectangle = null):void {
			throw new Error("Not implemented");
		}
	}
}
