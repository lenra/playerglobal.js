package flash.display {
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.UncaughtErrorEvents;
	import flash.system.ApplicationDomain;
	import flash.utils.ByteArray;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;

	[Event(name="complete", type="flash.events.Event")]
	[Event(name="httpStatus", type="flash.events.HTTPStatusEvent")]
	[Event(name="init", type="flash.events.Event")]
	[Event(name="ioError", type="flash.events.IOErrorEvent")]
	[Event(name="open", type="flash.events.Event")]
	[Event(name="progress", type="flash.events.ProgressEvent")]
	[Event(name="unload", type="flash.events.Event")]
	/**
	 *  The LoaderInfo class provides information about a loaded SWF file or a loaded image file (JPEG, GIF, or PNG). LoaderInfo objects are available for any display object. The information provided includes load progress, the URLs of the loader and loaded content, the number of bytes total for the media, and the nominal height and width of the media. <p>You can access LoaderInfo objects in two ways: </p> <ul> 
	 *  <li>The <code>contentLoaderInfo</code> property of a flash.display.Loader object— The <code>contentLoaderInfo</code> property is always available for any Loader object. For a Loader object that has not called the <code>load()</code> or <code>loadBytes()</code> method, or that has not sufficiently loaded, attempting to access many of the properties of the <code>contentLoaderInfo</code> property throws an error.</li> 
	 *  <li>The <code>loaderInfo</code> property of a display object. </li> 
	 * </ul> <p>The <code>contentLoaderInfo</code> property of a Loader object provides information about the content that the Loader object is loading, whereas the <code>loaderInfo</code> property of a DisplayObject provides information about the root SWF file for that display object. </p> <p>When you use a Loader object to load a display object (such as a SWF file or a bitmap), the <code>loaderInfo</code> property of the display object is the same as the <code>contentLoaderInfo</code> property of the Loader object (<code>DisplayObject.loaderInfo = Loader.contentLoaderInfo</code>). Because the instance of the main class of the SWF file has no Loader object, the <code>loaderInfo</code> property is the only way to access the LoaderInfo for the instance of the main class of the SWF file.</p> <p>The following diagram shows the different uses of the LoaderInfo object—for the instance of the main class of the SWF file, for the <code>contentLoaderInfo</code> property of a Loader object, and for the <code>loaderInfo</code> property of a loaded object:</p> <p> <img src="../../images/loaderInfo_object.jpg" alt="An image of different LoaderInfo situations"> </p> <p>When a loading operation is not complete, some properties of the <code>contentLoaderInfo</code> property of a Loader object are not available. You can obtain some properties, such as <code>bytesLoaded</code>, <code>bytesTotal</code>, <code>url</code>, <code>loaderURL</code>, and <code>applicationDomain</code>. When the <code>loaderInfo</code> object dispatches the <code>init</code> event, you can access all properties of the <code>loaderInfo</code> object and the loaded image or SWF file.</p> <p> <b>Note:</b> All properties of LoaderInfo objects are read-only.</p> <p>The <code>EventDispatcher.dispatchEvent()</code> method is not applicable to LoaderInfo objects. If you call <code>dispatchEvent()</code> on a LoaderInfo object, an IllegalOperationError exception is thrown.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7de1.html" target="_blank">Monitoring loading progress</a>
	 * </div><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e13.html" target="_blank">Loading display content dynamically</a>
	 *  <br>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Loader.html" target="">flash.display.Loader</a>
	 *  <br>
	 *  <a href="Loader.html#content" target="">flash.display.Loader.content</a>
	 *  <br>
	 *  <a href="DisplayObject.html" target="">flash.display.DisplayObject</a>
	 *  <br>
	 *  <a href="DisplayObject.html#loaderInfo" target="">flash.display.DisplayObject.loaderInfo</a>
	 * </div><br><hr>
	 */
	public class LoaderInfo extends EventDispatcher {
		private var _childSandboxBridge:Object;
		private var _parentSandboxBridge:Object;

		private var _actionScriptVersion:uint;
		private var _applicationDomain:ApplicationDomain;
		private var _bytes:ByteArray;
		private var _bytesLoaded:uint;
		private var _bytesTotal:uint;
		private var _childAllowsParent:Boolean;
		private var _content:DisplayObject;
		private var _contentType:String;
		private var _frameRate:Number;
		private var _height:int;
		private var _isURLInaccessible:Boolean;
		private var _loader:Loader;
		private var _loaderURL:String;
		private var _parameters:Object;
		private var _parentAllowsChild:Boolean;
		private var _sameDomain:Boolean;
		private var _sharedEvents:EventDispatcher;
		private var _swfVersion:uint;
		private var _uncaughtErrorEvents:UncaughtErrorEvents;
		private var _url:String;
		private var _width:int;

		public function LoaderInfo() {
			var global:* = (function():*{}.constructor)("return self")();
			this._url = global["loaderInfos"].url;
			var l:URLLoader = new URLLoader();
			l.dataFormat = URLLoaderDataFormat.BINARY;
			l.addEventListener(Event.COMPLETE, function(e:Event):void {
				_bytes = e.target.data as ByteArray;
			});
			l.load(new URLRequest(this._url));
		}

		/**
		 * <p> The ActionScript version of the loaded SWF file. The language version is specified by using the enumerations in the ActionScriptVersion class, such as <code>ActionScriptVersion.ACTIONSCRIPT2</code> and <code>ActionScriptVersion.ACTIONSCRIPT3</code>. </p>
		 * <p><b>Note:</b> This property always has a value of either <code>ActionScriptVersion.ACTIONSCRIPT2</code> or <code>ActionScriptVersion.ACTIONSCRIPT3</code>. ActionScript 1.0 and 2.0 are both reported as <code>ActionScriptVersion.ACTIONSCRIPT2</code> (version 2.0). This property only distinguishes ActionScript 1.0 and 2.0 from ActionScript 3.0.</p>
		 * 
		 * @return 
		 */
		public function get actionScriptVersion():uint {
			return _actionScriptVersion;
		}

		/**
		 * <p> When an external SWF file is loaded, all ActionScript 3.0 definitions contained in the loaded class are stored in the <code>applicationDomain</code> property. </p>
		 * <p>All code in a SWF file is defined to exist in an application domain. The current application domain is where your main application runs. The system domain contains all application domains, including the current domain and all classes used by Flash Player or Adobe AIR.</p>
		 * <p>All application domains, except the system domain, have an associated parent domain. The parent domain of your main application's <code>applicationDomain</code> is the system domain. Loaded classes are defined only when their parent doesn't already define them. You cannot override a loaded class definition with a newer definition.</p>
		 * <p>For usage examples of application domains, see the "Client System Environment" chapter in the <i>ActionScript 3.0 Developer's Guide</i>.</p>
		 * 
		 * @return 
		 */
		public function get applicationDomain():ApplicationDomain {
			return _applicationDomain;
		}

		/**
		 * <p> The bytes associated with a LoaderInfo object. </p>
		 * 
		 * @return 
		 */
		public function get bytes():ByteArray {
			return _bytes;
		}

		/**
		 * <p> The number of bytes that are loaded for the media. When this number equals the value of <code>bytesTotal</code>, all of the bytes are loaded. </p>
		 * 
		 * @return 
		 */
		public function get bytesLoaded():uint {
			return _bytesLoaded;
		}

		/**
		 * <p> The number of compressed bytes in the entire media file. </p>
		 * <p>Before the first <code>progress</code> event is dispatched by this LoaderInfo object's corresponding Loader object, <code>bytesTotal</code> is 0. After the first <code>progress</code> event from the Loader object, <code>bytesTotal</code> reflects the actual number of bytes to be downloaded.</p>
		 * <p><b>Note (iOS only)</b>: When running an application on iOS, the value returned is not the same as on other platforms. </p>
		 * 
		 * @return 
		 */
		public function get bytesTotal():uint {
			return _bytesTotal;
		}

		/**
		 * <p> Expresses the trust relationship from content (child) to the Loader (parent). If the child has allowed the parent access, <code>true</code>; otherwise, <code>false</code>. This property is set to <code>true</code> if the child object has called the <code>allowDomain()</code> method to grant permission to the parent domain or if a URL policy is loaded at the child domain that grants permission to the parent domain. If child and parent are in the same domain, this property is set to <code>true</code>. </p>
		 * <p>For more information related to security, see the Flash Player Developer Center Topic: <a href="http://www.adobe.com/go/devnet_security_en" target="external">Security</a>.</p>
		 * 
		 * @return 
		 */
		public function get childAllowsParent():Boolean {
			return _childAllowsParent;
		}

		/**
		 * <p> A object that can be set by the loaded content's code to expose properties and methods that can be accessed by code in the Loader object's sandbox. This <i>sandbox bridge</i> lets content from a non-application domain have controlled access to scripts in the application sandbox, and vice versa. The sandbox bridge serves as a gateway between the sandboxes, providing explicit interaction between application and non-application security sandboxes. </p>
		 * 
		 * @return 
		 */
		public function get childSandboxBridge():Object {
			return _childSandboxBridge;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set childSandboxBridge(value:Object):void {
			_childSandboxBridge = value;
		}

		/**
		 * <p> The loaded object associated with this LoaderInfo object. </p>
		 * 
		 * @return 
		 */
		public function get content():DisplayObject {
			return _content;
		}

		/**
		 * <p> The MIME type of the loaded file. The value is <code>null</code> if not enough of the file has loaded in order to determine the type. The following list gives the possible values: </p>
		 * <ul>
		 *  <li><code>"application/x-shockwave-flash"</code></li>
		 *  <li><code>"image/jpeg"</code></li>
		 *  <li><code>"image/gif"</code></li>
		 *  <li><code>"image/png"</code></li>
		 * </ul>
		 * 
		 * @return 
		 */
		public function get contentType():String {
			return _contentType;
		}

		/**
		 * <p> The nominal frame rate, in frames per second, of the loaded SWF file. This number is often an integer, but need not be. </p>
		 * <p>This value may differ from the actual frame rate in use. Flash Player or Adobe AIR only uses a single frame rate for all loaded SWF files at any one time, and this frame rate is determined by the nominal frame rate of the main SWF file. Also, the main frame rate may not be able to be achieved, depending on hardware, sound synchronization, and other factors.</p>
		 * 
		 * @return 
		 */
		public function get frameRate():Number {
			return _frameRate;
		}

		/**
		 * <p> The nominal height of the loaded file. This value might differ from the actual height at which the content is displayed, since the loaded content or its parent display objects might be scaled. </p>
		 * 
		 * @return 
		 */
		public function get height():int {
			return _height;
		}

		/**
		 * <p> Indicates if the <code>LoaderInfo.url</code> property has been truncated. When the <code>isURLInaccessible</code> value is <code>true</code> the <code>LoaderInfo.url</code> value is only the domain of the final URL from which the content loaded. For example, the property is truncated if the content is loaded from <code>http://www.adobe.com/assets/hello.swf</code>, and the <code>LoaderInfo.url</code> property has the value <code>http://www.adobe.com</code>. The <code>isURLInaccessible</code> value is <code>true</code> only when all of the following are also true: </p>
		 * <ul>
		 *  <li>An HTTP redirect occurred while loading the content.</li>
		 *  <li>The SWF file calling <code>Loader.load()</code> is from a different domain than the content's final URL.</li>
		 *  <li>The SWF file calling <code>Loader.load()</code> does not have permission to access the content. Permission is granted to access the content the same way permission is granted for <code>BitmapData.draw()</code>: call <code>Security.allowDomain()</code> to access a SWF file (or for non-SWF file content, establish a policy file and use the <code>LoaderContext.checkPolicyFile</code> property).</li>
		 * </ul>
		 * <p><b>Note:</b> The <code>isURLInaccessible</code> property was added for Flash Player 10.1 and AIR 2.0. However, this property is made available to SWF files of all versions when the Flash runtime supports it. So, using some authoring tools in "strict mode" causes a compilation error. To work around the error use the indirect syntax <code>myLoaderInfo["isURLInaccessible"]</code>, or disable strict mode. If you are using Flash Professional CS5 or Flex SDK 4.1, you can use and compile this API for runtimes released before Flash Player 10.1 and AIR 2.</p>
		 * <p>For application content in AIR, the value of this property is always <code>false</code>.</p>
		 * 
		 * @return 
		 */
		public function get isURLInaccessible():Boolean {
			return _isURLInaccessible;
		}

		/**
		 * <p> The Loader object associated with this LoaderInfo object. If this LoaderInfo object is the <code>loaderInfo</code> property of the instance of the main class of the SWF file, no Loader object is associated. </p>
		 * 
		 * @return 
		 */
		public function get loader():Loader {
			return _loader;
		}

		/**
		 * <p> The URL of the SWF file that initiated the loading of the media described by this LoaderInfo object. For the instance of the main class of the SWF file, this URL is the same as the SWF file's own URL. </p>
		 * 
		 * @return 
		 */
		public function get loaderURL():String {
			return _loaderURL;
		}

		/**
		 * <p> An object that contains name-value pairs that represent the parameters provided to the loaded SWF file. </p>
		 * <p>You can use a <code>for-in</code> loop to extract all the names and values from the <code>parameters</code> object.</p>
		 * <p>The two sources of parameters are: the query string in the URL of the main SWF file, and the value of the <code>FlashVars</code> HTML parameter (this affects only the main SWF file).</p>
		 * <p>The <code>parameters</code> property replaces the ActionScript 1.0 and 2.0 technique of providing SWF file parameters as properties of the main timeline.</p>
		 * <p>The value of the <code>parameters</code> property is null for Loader objects that contain SWF files that use ActionScript 1.0 or 2.0. It is only non-null for Loader objects that contain SWF files that use ActionScript 3.0.</p>
		 * 
		 * @return 
		 */
		public function get parameters():Object {
			return _parameters;
		}

		/**
		 * <p> Expresses the trust relationship from Loader (parent) to the content (child). If the parent has allowed the child access, <code>true</code>; otherwise, <code>false</code>. This property is set to <code>true</code> if the parent object called the <code>allowDomain()</code> method to grant permission to the child domain or if a URL policy file is loaded at the parent domain granting permission to the child domain. If child and parent are in the same domain, this property is set to <code>true</code>. </p>
		 * <p>For more information related to security, see the Flash Player Developer Center Topic: <a href="http://www.adobe.com/go/devnet_security_en" target="external">Security</a>.</p>
		 * 
		 * @return 
		 */
		public function get parentAllowsChild():Boolean {
			return _parentAllowsChild;
		}

		/**
		 * <p> A object that can be set by code in the Loader object's sandbox to expose properties and methods that can be accessed by the loaded content's code. This <i>sandbox bridge</i> lets content from a non-application domain have controlled access to scripts in the application sandbox, and vice versa. The sandbox bridge serves as a gateway between the sandboxes, providing explicit interaction between application and non-application security sandboxes. </p>
		 * 
		 * @return 
		 */
		public function get parentSandboxBridge():Object {
			return _parentSandboxBridge;
		}

		/**
		 * @param value
		 * @return 
		 */
		public function set parentSandboxBridge(value:Object):void {
			_parentSandboxBridge = value;
		}

		/**
		 * <p> Expresses the domain relationship between the loader and the content: <code>true</code> if they have the same origin domain; <code>false</code> otherwise. </p>
		 * 
		 * @return 
		 */
		public function get sameDomain():Boolean {
			return _sameDomain;
		}

		/**
		 * <p> An EventDispatcher instance that can be used to exchange events across security boundaries. Even when the Loader object and the loaded content originate from security domains that do not trust one another, both can access <code>sharedEvents</code> and send and receive events via this object. </p>
		 * 
		 * @return 
		 */
		public function get sharedEvents():EventDispatcher {
			return _sharedEvents;
		}

		/**
		 * <p> The file format version of the loaded SWF file. The file format is specified using the enumerations in the SWFVersion class, such as <code>SWFVersion.FLASH7</code> and <code>SWFVersion.FLASH9</code>. </p>
		 * 
		 * @return 
		 */
		public function get swfVersion():uint {
			return _swfVersion;
		}

		/**
		 * <p> An object that dispatches an <code>uncaughtError</code> event when an unhandled error occurs in code in this LoaderInfo object's SWF file. An uncaught error happens when an error is thrown outside of any <code>try..catch</code> blocks or when an ErrorEvent object is dispatched with no registered listeners. </p>
		 * <p>For example, if in a try block, there is an event dispatcher that calls its event handler, the catch block doesn't catch the error if thrown in event handler. Any error thrown thereafter can be caught by listening to <code>LoaderInfo.uncaughtErrorEvents</code></p>
		 * <p>This property is created when the SWF associated with this LoaderInfo has finished loading. Until then the <code>uncaughtErrorEvents</code> property is <code>null</code>. In an ActionScript-only project, you can access this property during or after the execution of the constructor function of the main class of the SWF file. For a Flex project, the <code>uncaughtErrorEvents</code> property is available after the <code>applicationComplete</code> event is dispatched.</p>
		 * 
		 * @return 
		 */
		public function get uncaughtErrorEvents():UncaughtErrorEvents {
			return _uncaughtErrorEvents;
		}

		/**
		 * <p> The URL of the media being loaded. </p>
		 * <p>Before the first <code>progress</code> event is dispatched by this LoaderInfo object's corresponding Loader object, the value of the <code>url</code> property might reflect only the initial URL specified in the call to the <code>load()</code> method of the Loader object. After the first <code>progress</code> event, the <code>url</code> property reflects the media's final URL, after any redirects and relative URLs are resolved.</p>
		 * <p>In some cases, the value of the <code>url</code> property is truncated; see the <code>isURLInaccessible</code> property for details.</p>
		 * 
		 * @return 
		 */
		public function get url():String {
			return _url;
		}

		/**
		 * <p> The nominal width of the loaded content. This value might differ from the actual width at which the content is displayed, since the loaded content or its parent display objects might be scaled. </p>
		 * 
		 * @return 
		 */
		public function get width():int {
			return _width;
		}


		/**
		 * <p> Returns the LoaderInfo object associated with a SWF file defined as an object. </p>
		 * 
		 * @param object  — The object for which you want to get an associated LoaderInfo object. 
		 * @return  — The associated LoaderInfo object. Returns  when called in non-debugger builds (or when debugging is not enabled) or if the referenced  does not have an associated LoaderInfo object (such as some objects used by the AIR runtime). 
		 */
		public static function getLoaderInfoByDefinition(object:Object):LoaderInfo {
			throw new Error("Not implemented");
		}
	}
}
