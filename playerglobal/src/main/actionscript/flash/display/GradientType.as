package flash.display {
	/**
	 *  The GradientType class provides values for the <code>type</code> parameter in the <code>beginGradientFill()</code> and <code>lineGradientStyle()</code> methods of the flash.display.Graphics class. <br><hr>
	 */
	public class GradientType {
		/**
		 * <p> Value used to specify a linear gradient fill. </p>
		 */
		public static const LINEAR:String = "linear";
		/**
		 * <p> Value used to specify a radial gradient fill. </p>
		 */
		public static const RADIAL:String = "radial";
	}
}
