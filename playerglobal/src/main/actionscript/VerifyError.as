package {
	/**
	 *  The VerifyError class represents an error that occurs when a malformed or corrupted SWF file is encountered. <p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7ecf.html" target="_blank">Responding to error events and status</a>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="flash/display/Loader.html" target="">Loader class</a>
	 * </div><br><hr>
	 */
	public class VerifyError extends Error {

		/**
		 * <p> Creates a new VerifyError object. </p>
		 * 
		 * @param message  — Contains the message associated with the VerifyError object. 
		 */
		public function VerifyError(message:String = "") {
			super(message);
			throw new Error("Not implemented");
		}
	}
}
