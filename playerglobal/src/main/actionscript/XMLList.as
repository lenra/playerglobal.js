package {
	/**
	 *  The XMLList class contains methods for working with one or more XML elements. An XMLList object can represent one or more XML objects or elements (including multiple nodes or attributes), so you can call methods on the elements as a group or on the individual elements in the collection. <p>If an XMLList object has only one XML element, you can use the XML class methods on the XMLList object directly. In the following example, <code>example.two</code> is an XMLList object of length 1, so you can call any XML method on it.</p> <div class="listing">
	 *  <div class="clipcopy">
	 *   <a href="#" class="copyText">Copy</a>
	 *  </div>
	 *  <pre>
	 *  var example2 = &lt;example&gt;&lt;two&gt;2&lt;/two&gt;&lt;/example&gt;;</pre>
	 * </div> <p>If you attempt to use XML class methods with an XMLList object containing more than one XML object, an exception is thrown; instead, iterate over the XMLList collection (using a <code>for each..in</code> statement, for example) and apply the methods to each XML object in the collection.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7f95.html" target="_blank">Initializing XML variables</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e68.html" target="_blank">Assembling and transforming XML objects</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e6b.html" target="_blank">Traversing XML structures</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e6c.html" target="_blank">Using XML namespaces</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e6d.html" target="_blank">XML type conversion</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e5e.html" target="_blank">Converting XML and XMLList objects to strings</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e6a.html" target="_blank">Reading external XML documents</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e69.html" target="_blank">XML in ActionScript example: Loading RSS data from the Internet</a>
	 * </div><p id="learnMore"><span class="classHeaderTableLabel">Learn more</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/Flex/4.6/UsingSDK/WS2db454920e96a9e51e63e3d11c0bf69084-7b69.html" target="_blank">Hierarchical data objects</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7ff5.html" target="_blank">Working with XML</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e65.html" target="_blank">Basics of XML</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e72.html" target="_blank">The E4X approach to XML processing</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e71.html" target="_blank">XML objects</a>
	 *  <br>
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7e70.html" target="_blank">XMLList objects</a>
	 *  <br>
	 * </div><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="XML.html" target="">XML</a>
	 *  <br>
	 *  <a href="statements.html#for_each..in" target="">for each..in</a>
	 *  <br>
	 *  <a href="Namespace.html" target="">Namespace</a>
	 *  <br>
	 *  <a href="QName.html" target="">QName</a>
	 * </div><br><hr>
	 */
	public class XMLList {

		/**
		 * <p> Creates a new XMLList object. </p>
		 * 
		 * @param value  — Any object that can be converted to an XMLList object by using the top-level <code>XMLList()</code> function. 
		 */
		public function XMLList(value:Object) {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Calls the <code>attribute()</code> method of each XML object and returns an XMLList object of the results. The results match the given <code>attributeName</code> parameter. If there is no match, the <code>attribute()</code> method returns an empty XMLList object. </p>
		 * 
		 * @param attributeName  — The name of the attribute that you want to include in an XMLList object. 
		 * @return  — An XMLList object of matching XML objects or an empty XMLList object. 
		 */
		public function attribute(attributeName:*):XMLList {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Calls the <code>attributes()</code> method of each XML object and returns an XMLList object of attributes for each XML object. </p>
		 * 
		 * @return  — An XMLList object of attributes for each XML object. 
		 */
		public function attributes():XMLList {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Calls the <code>child()</code> method of each XML object and returns an XMLList object that contains the results in order. </p>
		 * 
		 * @param propertyName  — The element name or integer of the XML child. 
		 * @return  — An XMLList object of child nodes that match the input parameter. 
		 */
		public function child(propertyName:Object):XMLList {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Calls the <code>children()</code> method of each XML object and returns an XMLList object that contains the results. </p>
		 * 
		 * @return  — An XMLList object of the children in the XML objects. 
		 */
		public function children():XMLList {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Calls the <code>comments()</code> method of each XML object and returns an XMLList of comments. </p>
		 * 
		 * @return  — An XMLList of the comments in the XML objects. 
		 */
		public function comments():XMLList {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Checks whether the XMLList object contains an XML object that is equal to the given <code>value</code> parameter. </p>
		 * 
		 * @param value  — An XML object to compare against the current XMLList object. 
		 * @return  — If the XMLList contains the XML object declared in the  parameter, then ; otherwise . 
		 */
		public function contains(value:XML):Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a copy of the given XMLList object. The copy is a duplicate of the entire tree of nodes. The copied XML object has no parent and returns <code>null</code> if you attempt to call the <code>parent()</code> method. </p>
		 * 
		 * @return  — The copy of the XMLList object. 
		 */
		public function copy():XMLList {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns all descendants (children, grandchildren, great-grandchildren, and so on) of the XML object that have the given <code>name</code> parameter. The <code>name</code> parameter can be a QName object, a String data type, or any other data type that is then converted to a String data type. </p>
		 * <p>To return all descendants, use the asterisk (*) parameter. If no parameter is passed, the string "*" is passed and returns all descendants of the XML object.</p>
		 * 
		 * @param name  — The name of the element to match. 
		 * @return  — An XMLList object of the matching descendants (children, grandchildren, and so on) of the XML objects in the original list. If there are no descendants, returns an empty XMLList object. 
		 */
		public function descendants(name:Object = "*"):XMLList {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Calls the <code>elements()</code> method of each XML object. The <code>name</code> parameter is passed to the <code>descendants()</code> method. If no parameter is passed, the string "*" is passed to the <code>descendants()</code> method. </p>
		 * 
		 * @param name  — The name of the elements to match. 
		 * @return  — An XMLList object of the matching child elements of the XML objects. 
		 */
		public function elements(name:Object = "*"):XMLList {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Checks whether the XMLList object contains complex content. An XMLList object is considered to contain complex content if it is not empty and either of the following conditions is true: </p>
		 * <ul>
		 *  <li>The XMLList object contains a single XML item with complex content.</li>
		 *  <li>The XMLList object contains elements.</li>
		 * </ul>
		 * 
		 * @return  — If the XMLList object contains complex content, then ; otherwise . 
		 */
		public function hasComplexContent():Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Checks for the property specified by <code>p</code>. </p>
		 * 
		 * @param p  — The property to match. 
		 * @return  — If the parameter exists, then ; otherwise . 
		 */
		/*public function hasOwnProperty(p:*):Boolean {
			throw new Error("Not implemented");
		}*/

		/**
		 * <p> Checks whether the XMLList object contains simple content. An XMLList object is considered to contain simple content if one or more of the following conditions is true: </p>
		 * <ul>
		 *  <li>The XMLList object is empty</li>
		 *  <li>The XMLList object contains a single XML item with simple content</li>
		 *  <li>The XMLList object contains no elements</li>
		 * </ul>
		 * 
		 * @return  — If the XMLList contains simple content, then ; otherwise . 
		 */
		public function hasSimpleContent():Boolean {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the number of properties in the XMLList object. </p>
		 * 
		 * @return  — The number of properties in the XMLList object. 
		 */
		public function length():int {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Merges adjacent text nodes and eliminates empty text nodes for each of the following: all text nodes in the XMLList, all the XML objects contained in the XMLList, and the descendants of all the XML objects in the XMLList. </p>
		 * 
		 * @return  — The normalized XMLList object. 
		 */
		public function normalize():XMLList {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the parent of the XMLList object if all items in the XMLList object have the same parent. If the XMLList object has no parent or different parents, the method returns <code>undefined</code>. </p>
		 * 
		 * @return  — Returns the parent XML object. 
		 */
		public function parent():Object {
			throw new Error("Not implemented");
		}

		/**
		 * <p> If a <code>name</code> parameter is provided, lists all the children of the XMLList object that contain processing instructions with that name. With no parameters, the method lists all the children of the XMLList object that contain any processing instructions. </p>
		 * 
		 * @param name  — The name of the processing instructions to match. 
		 * @return  — An XMLList object that contains the processing instructions for each XML object. 
		 */
		public function processingInstructions(name:String = "*"):XMLList {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Checks whether the property <code>p</code> is in the set of properties that can be iterated in a <code>for..in</code> statement applied to the XMLList object. This is <code>true</code> only if <code>toNumber(p)</code> is greater than or equal to 0 and less than the length of the XMLList object. </p>
		 * 
		 * @param p  — The index of a property to check. 
		 * @return  — If the property can be iterated in a  statement, then ; otherwise . 
		 */
		/*override public function propertyIsEnumerable(p:*):Boolean {
			throw new Error("Not implemented");
		}*/

		/**
		 * <p> Calls the <code>text()</code> method of each XML object and returns an XMLList object that contains the results. </p>
		 * 
		 * @return  — An XMLList object of all XML properties of the XMLList object that represent XML text nodes. 
		 */
		public function text():XMLList {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a string representation of all the XML objects in an XMLList object. The rules for this conversion depend on whether the XML object has simple content or complex content: </p>
		 * <ul>
		 *  <li>If the XML object has simple content, <code>toString()</code> returns the string contents of the XML object with the following stripped out: the start tag, attributes, namespace declarations, and end tag.</li>
		 * </ul>
		 * <ul>
		 *  <li> If the XML object has complex content, <code>toString()</code> returns an XML encoded string representing the entire XML object, including the start tag, attributes, namespace declarations, and end tag.</li>
		 * </ul>
		 * <p>To return the entire XML object every time, use the <code>toXMLString()</code> method.</p>
		 * 
		 * @return  — The string representation of the XML object. 
		 */
		public function toString():String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns a string representation of all the XML objects in an XMLList object. Unlike the <code>toString()</code> method, the <code>toXMLString()</code> method always returns the start tag, attributes, and end tag of the XML object, regardless of whether the XML object has simple content or complex content. (The <code>toString()</code> method strips out these items for XML objects that contain simple content.) </p>
		 * 
		 * @return  — The string representation of the XML object. 
		 */
		public function toXMLString():String {
			throw new Error("Not implemented");
		}

		/**
		 * <p> Returns the XMLList object. </p>
		 * 
		 * @return  — Returns the current XMLList object. 
		 */
		/*public function valueOf():* {
			throw new Error("Not implemented");
		}*/
	}
}
