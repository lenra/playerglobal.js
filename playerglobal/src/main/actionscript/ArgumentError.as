package {
	/**
	 *  The ArgumentError class represents an error that occurs when the arguments supplied in a function do not match the arguments defined for that function. This error occurs, for example, when a function is called with the wrong number of arguments, an argument of the incorrect type, or an invalid argument. <p><a href="#includeExamplesSummary">View the examples</a></p><p id="moreExamples"><span class="classHeaderTableLabel">More examples</span></p><div class="seeAlso">
	 *  <a href="http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118a9b90204-7ecf.html" target="_blank">Responding to error events and status</a>
	 * </div><br><hr>
	 */
	public class ArgumentError extends Error {

		/**
		 * <p> Creates an ArgumentError object. </p>
		 * 
		 * @param message  — A string associated with the error. 
		 */
		public function ArgumentError(message:String = "") {
			super(message);
		}
	}
}
