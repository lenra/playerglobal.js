package {
	/**
	 *  A Class object is created for each class definition in a program. Every Class object is an instance of the Class class. The Class object contains the static properties and methods of the class. The class object creates instances of the class when invoked using the <code>new</code> operator. <p>Some methods, such as <code>flash.net.getClassByAlias()</code>, return an object of type Class. Other methods may have a parameter of type Class, such as <code>flash.net.registerClassAlias()</code>. </p> <p>The class name is the reference to the Class object, as this example shows:</p> <pre> 
	 *  class Foo {
	 *  }
	 *  </pre> <p>The <code>class Foo{}</code> statement is the class definition that creates the Class object Foo. Additionally, the statement <code>new Foo()</code> will create a new instance of class Foo, and the result will be of type Foo.</p> <p>Use the <code>class</code> statement to declare your classes. Class objects are useful for advanced techniques, such as assigning classes to an existing instance object at runtime, as shown in the "Examples" section below.</p> <p>Any static properties and methods of a class live on the class's Class object. Class, itself, declares <code>prototype</code>.</p> <p>Generally, you do not need to declare or create variables of type Class manually. However, in the following code, a class is assigned as a public Class property <code>circleClass</code>, and you can refer to this Class property as a property of the main Library class:</p> <div class="listing">
	 *  <div class="clipcopy">
	 *   <a href="#" class="copyText">Copy</a>
	 *  </div>
	 *  <pre>
	 *  package {
	 *   import flash.display.Sprite;
	 *   public class Library extends Sprite {
	 *       
	 *       public var circleClass:Class = Circle;
	 *       public function Library() {
	 *       }
	 *   }
	 *  }
	 *   
	 *  import flash.display.Shape;
	 *  class Circle extends Shape {
	 *   public function Circle(color:uint = 0xFFCC00, radius:Number = 10) {
	 *       graphics.beginFill(color);
	 *       graphics.drawCircle(radius, radius, radius);
	 *   }
	 *  }
	 *  </pre>
	 * </div> <p>Another SWF file can load the resulting Library.swf file and then instantiate objects of type Circle. The following example shows one way to get access to a child SWF file's assets. (Other techniques include using <code>flash.utils.getDefnitionByName()</code> or importing stub definitions of the child SWF file.)</p> <div class="listing">
	 *  <div class="clipcopy">
	 *   <a href="#" class="copyText">Copy</a>
	 *  </div>
	 *  <pre>
	 *  package {
	 *   import flash.display.Sprite;
	 *   import flash.display.Shape;
	 *   import flash.display.Loader;
	 *   import flash.net.URLRequest;
	 *   import flash.events.Event;
	 *   public class LibaryLoader extends Sprite {
	 *       public function LibaryLoader() {
	 *           var ldr:Loader = new Loader();
	 *           var urlReq:URLRequest = new URLRequest("Library.swf");
	 *           ldr.load(urlReq);
	 *           ldr.contentLoaderInfo.addEventListener(Event.COMPLETE, loaded);
	 *       }
	 *       private function loaded(event:Event):void {
	 *           var library:Object = event.target.content;
	 *           var circle:Shape = new library.circleClass();
	 *           addChild(circle);
	 *       }
	 *   }
	 *  }
	 *  </pre>
	 * </div> <p>In ActionScript 3.0, you can create embedded classes for external assets (such as images, sounds, or fonts) that are compiled into SWF files. In earlier versions of ActionScript, you associated those assets using a linkage ID with the <code>MovieClip.attachMovie()</code> method. In ActionScript 3.0, each embedded asset is represented by a unique embedded asset class. Therefore, you can use the <code>new</code> operator to instantiate the asset's associated class and call methods and properties on that asset.</p> <p><a href="#includeExamplesSummary">View the examples</a></p><p><span class="classHeaderTableLabel">Related API Elements</span></p><div class="seeAlso">
	 *  <a href="Object.html#prototype" target="">Object.prototype</a>
	 *  <br>
	 *  <a href="operators.html#new" target="">new operator</a>
	 * </div><br><hr>
	 */
	public class Class {
	}
}
